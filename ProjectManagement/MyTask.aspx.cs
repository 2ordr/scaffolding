﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using AjaxControlToolkit;

namespace ProjectManagement
{
    public partial class MyTask : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                    DateTime orddate = DateTime.Now;

                    txtTimeDate.Text = Convert.ToString(orddate);

                    int taskid;
                    int subtskid;
                    SqlConnection con1 = new SqlConnection(cs);
                    con1.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con1;

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
                    taskid = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
                    cmd.Parameters.Clear();
                    cmd.CommandText = "Delete FROM TEMPTASK";
                    cmd.ExecuteNonQuery();

                    cmd.Parameters.Clear();
                    cmd.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
                    subtskid = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Parameters.Clear();

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_InsertTempTask";
                    cmd.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
                    cmd.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
                    cmd.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
                    cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
                    cmd.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
                    cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT * FROM TEMPTASK";
                    SqlDataAdapter dtask = new SqlDataAdapter(cmd);
                    DataTable dttask = new DataTable();
                    dtask.Fill(dttask);
                    gridTask.DataSource = dttask;
                    gridTask.DataBind();

                    con1.Close();

                    lbltaskid.Text = Convert.ToInt32(taskid).ToString();
                    TaskId.Text = lbltaskid.Text;
                    //SetInitialRow();
                    BtnStop.Visible = false;
                    Label1.Text = Session["username"].ToString();

                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    com.Connection = con;
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "select Distinct EmpId,SkillSets from EMPLOYEE Where SkillSets!='" + null + "'";
                    SqlDataAdapter adp1 = new SqlDataAdapter(com);
                    DataSet dtt1 = new DataSet();
                    adp1.Fill(dtt1);
                    DDLSkillSet.DataSource = dtt1;
                    DDLSkillSet.DataTextField = "SkillSets";
                    DDLSkillSet.DataValueField = "EmpId";
                    DDLSkillSet.DataBind();
                    //DDLSkillSet.Items.Insert(0, new ListItem("-Select Skillset-", "0"));
                    con.Close();

                    PopulateHours();
                    PopulateMinutes();
                    PopulateSeconds();
                }
            }

        }

        private void PopulateHours()
        {
            for (int i = 0; i <= 24; i++)
            {
                ddlHours.Items.Add(i.ToString("D2"));
            }
        }

        private void PopulateMinutes()
        {
            for (int i = 0; i < 60; i++)
            {
                ddlMinutes.Items.Add(i.ToString("D2"));
            }
        }

        private void PopulateSeconds()
        {
            for (int i = 0; i < 60; i++)
            {
                ddlSeconds.Items.Add(i.ToString("D2"));
            }
        }

        protected void SaveTask_Click(object sender, EventArgs e)
        {
            string hours = "00:00:00";

            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubtask = (rows.Cells[0].FindControl("lblSubtask") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[1].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLSkillSet = (rows.Cells[2].FindControl("DDLSkillSet") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtStatus = (rows.Cells[4].FindControl("txtStatus") as System.Web.UI.WebControls.TextBox);

                    if (txtTask.Text == string.Empty || txtMTask.Text == string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                        return;
                    }
                    else
                    {
                        hours = ddlHours.Text + ":" + ddlMinutes.Text + ":" + ddlSeconds.Text;

                        SqlConnection con = new SqlConnection(cs);
                        con.Open();
                        com.Connection = con;
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertTaskDetails";
                        com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                        com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtStatus.Text;
                        com.Parameters.Add("@ENDDATE", System.Data.SqlDbType.VarChar).Value = "";
                        com.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = txtMTask.Text;
                        com.Parameters.Add("@FILEPATH", System.Data.SqlDbType.VarChar).Value = FileUpload1.FileName;
                        // com.Parameters.Add("@EXPECTEDHOURS", System.Data.SqlDbType.Time).Value = txtExpectedHrs.Text;
                        com.Parameters.Add("@EXPECTEDHOURS", System.Data.SqlDbType.Time).Value = hours;
                        com.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = DDLSkillSet.SelectedItem.Text;
                        com.Parameters.Add("@SUBTASK", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                        com.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = lblSubtask.Text;

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                        con.Close();

                    }
                }
            }
            resetall();
            ViewTaskList();
        }
        public void resetall()
        {
            TaskId.Text = "";
            txttask_0.Text = "";
            txtMTask.Text = "";
            int taskid, subtskid;
            SqlConnection con11 = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con11;
            con11.Open();

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
            taskid = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
            lbltaskid.Text = taskid.ToString();

            cmd.Parameters.Clear();
            cmd.CommandText = "Delete FROM TEMPTASK";
            cmd.ExecuteNonQuery();

            cmd.Parameters.Clear();
            cmd.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
            subtskid = Convert.ToInt32(cmd.ExecuteScalar());
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_InsertTempTask";
            cmd.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
            cmd.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
            cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
            cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(cmd);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();

            cmd.Parameters.Clear();

            con11.Close();

            lbltaskid.Text = Convert.ToInt32(taskid).ToString();
            TaskId.Text = lbltaskid.Text;
            //txtExpectedHrs.Text = "00:00";
            ddlHours.Text = "00";
            ddlMinutes.Text = "00";
            ddlSeconds.Text = "00";
        }
        protected void AddTask_Click(object sender, EventArgs e)
        {
            //Response.Redirect("MyTask.aspx");
            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubtask = (rows.Cells[0].FindControl("lblSubtask") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[1].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLSkillSet = (rows.Cells[2].FindControl("DDLSkillSet") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtStatus = (rows.Cells[4].FindControl("txtStatus") as System.Web.UI.WebControls.TextBox);

                    //if (txtTask.Text == "")
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                    //    return;
                    //}

                    //else
                    //{
                    com.Parameters.Clear();
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    com.Connection = con;
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_UpdateTempTask";
                    com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                    com.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                    com.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = DDLSkillSet.SelectedItem.Text;
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtStatus.Text;
                    com.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = lblSubtask.Text;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = DDLSkillSet.SelectedValue;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    //com.CommandType = System.Data.CommandType.Text;
                    //com.CommandText = "SELECT * FROM TEMPTASK";
                    //SqlDataAdapter dtask = new SqlDataAdapter(com);
                    //DataTable dttask = new DataTable();
                    //dtask.Fill(dttask);
                    //gridTask.DataSource = dttask;
                    //gridTask.DataBind();

                    con.Close();
                    // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details Added SuccessFully');", true);

                    //resetall();
                    //}
                }
            }
            resetTask();
        }
        public void resetTask()
        {

            int taskid;
            int subtskid;
            SqlConnection con11 = new SqlConnection(cs);
            SqlCommand cmd11 = new SqlCommand();
            cmd11.Connection = con11;
            con11.Open();

            cmd11.CommandType = System.Data.CommandType.Text;
            cmd11.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TEMPTASK";
            taskid = Convert.ToInt32(cmd11.ExecuteScalar());
            lbltaskid.Text = taskid.ToString();
            cmd11.Parameters.Clear();

            if (taskid.ToString() == "0")
            {
                cmd11.CommandType = System.Data.CommandType.Text;
                cmd11.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
                taskid = Convert.ToInt32(cmd11.ExecuteScalar()) + 1;
                lbltaskid.Text = taskid.ToString();
                cmd11.Parameters.Clear();
                cmd11.CommandText = "Delete FROM TEMPTASK";
                cmd11.ExecuteNonQuery();

                cmd11.Parameters.Clear();
                cmd11.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
                subtskid = Convert.ToInt32(cmd11.ExecuteScalar());
                cmd11.Parameters.Clear();

                cmd11.CommandType = System.Data.CommandType.StoredProcedure;
                cmd11.CommandText = "sp_InsertTempTask";
                cmd11.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
                cmd11.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
                cmd11.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
                cmd11.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
                cmd11.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
                cmd11.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

                cmd11.ExecuteNonQuery();
                cmd11.Parameters.Clear();
            }
            else
            {
                cmd11.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
                subtskid = Convert.ToInt32(cmd11.ExecuteScalar());

                cmd11.CommandType = System.Data.CommandType.StoredProcedure;
                cmd11.CommandText = "sp_InsertTempTask";
                cmd11.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
                cmd11.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
                cmd11.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
                cmd11.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
                cmd11.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
                cmd11.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

                cmd11.ExecuteNonQuery();
                cmd11.Parameters.Clear();
            }

            cmd11.CommandType = System.Data.CommandType.Text;
            cmd11.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(cmd11);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();

            cmd11.Parameters.Clear();
            con11.Close();

            lbltaskid.Text = Convert.ToInt32(taskid).ToString();
            TaskId.Text = lbltaskid.Text;
        }
        protected void gridTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            com.Connection = conn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList DDLSkillSet = (DropDownList)e.Row.FindControl("DDLSkillSet");
                Label lbltaskid = (Label)e.Row.FindControl("lbltaskid");
                Label lblSubtask = (Label)e.Row.FindControl("lblSubtask");

                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "select Distinct EmpId,SkillSets from EMPLOYEE Where SkillSets!='" + null + "'"; // Change Code//
                SqlDataAdapter adp1 = new SqlDataAdapter(com);
                DataSet dtt1 = new DataSet();
                adp1.Fill(dtt1);
                DDLSkillSet.DataSource = dtt1;
                DDLSkillSet.DataTextField = "SkillSets";
                DDLSkillSet.DataValueField = "EmpId";
                DDLSkillSet.DataBind();
                //DDLSkillSet.Items.Insert(0, new ListItem("-Select SkillSet-", "0"));
                com.Parameters.Clear();

                dtt1.Tables[0].Rows.Clear();
                com.CommandText = "SELECT SkillSet,Empid,TaskName FROM TEMPTASK WHERE TASKID=" + lbltaskid.Text + "AND SubTask=" + lblSubtask.Text + "";
                adp1.SelectCommand = com;
                adp1.Fill(dtt1);
                if (dtt1.Tables[0].Rows.Count > 0)
                {
                    if (dtt1.Tables[0].Rows[0]["TaskName"] != "")
                    {
                        DDLSkillSet.SelectedItem.Text = dtt1.Tables[0].Rows[0]["Skillset"].ToString();
                        DDLSkillSet.SelectedValue = dtt1.Tables[0].Rows[0]["Empid"].ToString();
                    }
                    else
                    {
                        DDLSkillSet.Items.Insert(0, new ListItem("-Vælg SkillSet-", "0"));
                    }


                }
                com.Parameters.Clear();
                conn.Close();
            }

        }

        protected void lnkDeletetask_Click(object sender, EventArgs e)
        {
            LinkButton lnkDeletetask = (LinkButton)sender;
            GridViewRow grdRow = ((GridViewRow)lnkDeletetask.Parent.Parent);
            Label lbltaskid = (Label)grdRow.FindControl("lbltaskid");
            Label lblSubtask = (Label)grdRow.FindControl("lblSubtask");

            com.Parameters.Clear();
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            com.Connection = conn;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "Delete from TempTask where TASKID=" + lbltaskid.Text + "AND SubTask=" + lblSubtask.Text + "";
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(com);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();
            com.Parameters.Clear();

            conn.Close();
        }
        //private void SetInitialRow()
        //{

        //    DataTable dt = new DataTable();

        //    DataRow dr = null;

        //    dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));

        //    dt.Columns.Add(new DataColumn("Checkbox1", typeof(string)));

        //    dt.Columns.Add(new DataColumn("Column1", typeof(string)));

        //    dt.Columns.Add(new DataColumn("Column2", typeof(string)));

        //    dt.Columns.Add(new DataColumn("Checkbox3", typeof(string)));

        //    dt.Columns.Add(new DataColumn("Column4", typeof(string)));

        //    dt.Columns.Add(new DataColumn("Column5", typeof(string)));


        //    dr = dt.NewRow();

        //    dr["RowNumber"] = 1;

        //    dr["Checkbox1"] = 1;

        //    dr["Column1"] = string.Empty;

        //    dr["Column2"] = string.Empty;

        //    dr["Checkbox3"] = string.Empty;

        //    dr["Column4"] = string.Empty;

        //    dr["Column5"] = string.Empty;



        //    dt.Rows.Add(dr);

        //    //dr = dt.NewRow();



        //    //Store the DataTable in ViewState

        //    ViewState["CurrentTable"] = dt;



        //    Gridview1.DataSource = dt;

        //    Gridview1.DataBind();

        //}

        //private void AddNewRowToGrid()
        //{

        //    int rowIndex = 0;



        //    if (ViewState["CurrentTable"] != null)
        //    {

        //        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

        //        DataRow drCurrentRow = null;

        //        if (dtCurrentTable.Rows.Count > 0)
        //        {

        //            for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
        //            {

        //                //extract the TextBox values

        //                TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("TextBox1");

        //                TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("TextBox2");

        //                //TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("TextBox3");

        //                TextBox box4 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("TextBox4");

        //                TextBox box5 = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("TextBox5");





        //                drCurrentRow = dtCurrentTable.NewRow();

        //                drCurrentRow["RowNumber"] = i + 1;
        //                //drCurrentRow["Checkbox"] = i + 1;


        //                dtCurrentTable.Rows[i - 1]["Column1"] = box1.Text;

        //                dtCurrentTable.Rows[i - 1]["Column2"] = box2.Text;

        //                //dtCurrentTable.Rows[i - 1]["Column3"] = box3.Text;

        //                dtCurrentTable.Rows[i - 1]["Column4"] = box4.Text;

        //                dtCurrentTable.Rows[i - 1]["Column5"] = box5.Text;



        //                rowIndex++;

        //            }

        //            dtCurrentTable.Rows.Add(drCurrentRow);

        //            ViewState["CurrentTable"] = dtCurrentTable;



        //            Gridview1.DataSource = dtCurrentTable;

        //            Gridview1.DataBind();

        //        }

        //    }

        //    else
        //    {

        //        Response.Write("ViewState is null");

        //    }



        //    //Set Previous Data on Postbacks

        //    SetPreviousData();

        //}


        //private void SetPreviousData()
        //{

        //    int rowIndex = 0;

        //    if (ViewState["CurrentTable"] != null)
        //    {

        //        DataTable dt = (DataTable)ViewState["CurrentTable"];

        //        if (dt.Rows.Count > 0)
        //        {

        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {

        //                TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("TextBox1");

        //                TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("TextBox2");

        //                //TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("TextBox3");

        //                TextBox box4 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("TextBox4");

        //                TextBox box5 = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("TextBox5");



        //                box1.Text = dt.Rows[i]["Column1"].ToString();

        //                box2.Text = dt.Rows[i]["Column2"].ToString();

        //                //box3.Text = dt.Rows[i]["Column3"].ToString();

        //                box4.Text = dt.Rows[i]["Column4"].ToString();

        //                box5.Text = dt.Rows[i]["Column5"].ToString();



        //                rowIndex++;

        //            }

        //        }

        //    }

        //}



        //protected void ButtonAdd_Click1(object sender, EventArgs e)
        //{

        //    AddNewRowToGrid();

        //}

        //protected void AjaxFileUpload1_UploadComplete(object sender, AjaxFileUploadEventArgs e)
        //{
        //    //string fileName = Path.GetFileName(e.FileName);
        //    //AjaxFileUpload1.SaveAs(Server.MapPath("~/Uploads/" + fileName));

        //    string path = Server.MapPath("~/Uploads/" + e.FileName);
        //    //AjaxFileUpload1.SaveAs(path);
        //}

        protected void BtnStart_Click(object sender, EventArgs e)
        {

            Timer1.Enabled = true;
            BtnStart.Visible = false;
            BtnStop.Visible = true;
            Label5.Text = lbltimedisplay.Text;
        }

        protected void BtnStop_Click(object sender, EventArgs e)
        {

            Timer1.Enabled = false;
            BtnStart.Visible = true;
            BtnStop.Visible = false;
            Label6.Text = lbltimedisplay.Text;

            DateTime inTime = Convert.ToDateTime(Label5.Text);
            DateTime outTime = Convert.ToDateTime(Label6.Text);
            TimeSpan i = outTime.Subtract(inTime);
            this.txtHrs.Text = i.ToString();
        }

        protected void Timer1_Tick1(object sender, EventArgs e)
        {
            lbltimedisplay.Text = DateTime.Now.ToString();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
            if (filename == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select File To Upload');", true);
                return;
            }
            else
            {
                string filePath = @"~\Uploads\" + filename.Trim();
                string newfile = Server.MapPath(filePath);
                FileUpload1.SaveAs(newfile);
                //Stream str = FileUpload1.PostedFile.InputStream;

                //BinaryReader br = new BinaryReader(str);

                //Byte[] size = br.ReadBytes((int)str.Length);

                using (SqlConnection con = new SqlConnection(cs))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "insert into TaskFIleData(TaskId,FileName,FilePath) values(@id,@Name,@FilePath)";

                        cmd.Parameters.AddWithValue("@id", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;

                        cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarChar).Value = filename;

                        cmd.Parameters.AddWithValue("@FilePath", System.Data.SqlDbType.VarChar).Value = newfile;

                        cmd.Connection = con;

                        con.Open();

                        cmd.ExecuteNonQuery();

                        con.Close();
                    }

                } gvFileUpload1();
                Idfiledetails.Visible = true;

            }
        }
        private void gvFileUpload1()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from TaskFIleData where TaskId=" + lbltaskid.Text;

                    cmd.Connection = con;

                    con.Open();

                    gvFileUpload.DataSource = cmd.ExecuteReader();

                    gvFileUpload.DataBind();

                    con.Close();
                }
            }
        }

        protected void btnVIewTask_Click(object sender, EventArgs e)
        {
            ViewTaskList();
        }

        public void ViewTaskList()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand amdd = new SqlCommand();
            con.Open();
            amdd.Connection = con;
            amdd.CommandType = System.Data.CommandType.StoredProcedure;
            amdd.CommandText = "sp_getTaskDetails";
            SqlDataAdapter adp1 = new SqlDataAdapter(amdd);
            DataSet dtt1 = new DataSet();
            adp1.Fill(dtt1);
            gvTaskView.DataSource = dtt1;
            gvTaskView.DataBind();
            IdTaskDetails.Visible = true;
        }
        protected void lbtnViewImage_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //string FilePath = grdrow.Cells[2].Text;
            Label lblFilePath = (Label)grdrow.Cells[2].FindControl("lblFilePath");
            string FilePath = lblFilePath.Text;

            if (FilePath.Contains(".doc") || FilePath.Contains(".rtf") || FilePath.Contains(".html") || FilePath.Contains(".log") || FilePath.Contains(".jpeg") || FilePath.Contains(".tif") || FilePath.Contains(".tiff") || FilePath.Contains(".gif") || FilePath.Contains(".bmp") || FilePath.Contains(".asf"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            //view pdf files 
            if (FilePath.Contains(".pdf") || FilePath.Contains(".avi") || FilePath.Contains(".zip") || FilePath.Contains(".xls") || FilePath.Contains(".csv") || FilePath.Contains(".wav") || FilePath.Contains(".mp3"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }


            if (FilePath.Contains(".htm") || FilePath.Contains(".mpg") || FilePath.Contains(".mpeg") || FilePath.Contains(".fdf") || FilePath.Contains(".ppt") || FilePath.Contains(".dwg") || FilePath.Contains(".msg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;

            }

            if (FilePath.Contains(".txt") || FilePath.Contains(".xml") || FilePath.Contains(".sdxl") || FilePath.Contains(".xdp") || FilePath.Contains(".jar"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".jpg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".png"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }
        }
    }
}
