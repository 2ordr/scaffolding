﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalaryDetails.aspx.cs" Inherits="ProjectManagement.SalaryDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>


    <style>
        .clienthead {
            font-size: x-large;
            font-weight: bold;
        }

        .td {
            font-size: small;
            font-weight: 700;
        }
    </style>
    <script type="text/javascript">
        function PrintGridDataAll() {


            <%--   var prtGrid = document.getElementById('<%# SearchResultGrid.ClientID %>');tbcontain_tb1_SearchResultGrid--%>
            var prtGrid = document.getElementById("MiainDiv");
            prtGrid.border = 0;
            var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=800,tollbar=0,scrollbars=1,status=0,resizable=1');
            prtwin.document.writeln("<div style='text-align:center; margin-top:20px;'></div>");
            prtwin.document.write(prtGrid.outerHTML);
            prtwin.document.close();
            prtwin.focus();
            prtwin.print();
            prtwin.close();
        }

        $(document).ready(function () {

            $('.modal').fadeOut();

            $(".searchbtnclick").click(function () {
                $(".modal").fadeIn();


            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 900px; width: 100%; font-size: xx-large; background-color: #FAFAFA;">

            <div id="MiainDiv" runat="server" style="margin: 0px; left: 195px; top: 0px; width: 100%; height: 90%; float: left; background-color: #FAFAFA;">

                <div style="width: 100%; height: 80px; background-color: silver; text-align: center">
                    <asp:Label ID="Label1" runat="server" CssClass="clienthead" Text="Company Name"></asp:Label><br />
                    <asp:Label ID="Label3" runat="server" CssClass="clienthead" Text="Salary Slip"></asp:Label>
                </div>
                <fieldset style="border: double">
                    <legend></legend>
                    <div>
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend></legend>
                                        <table style="width: 100%; height: 200px">

                                            <tr style="width: 30%; height: 31px">
                                                <td style="height: 31px; width: 30%"></td>
                                                <td class="td" style="width: 10%;">User Name:</td>
                                                <td class="td" style="width: 15%;">
                                                    <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td class="td" style="width: 10%;">User Id:                                               
                                                </td>
                                                <td class="td">
                                                    <asp:Label ID="lblUserId" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td style="width: 20%"></td>
                                            </tr>
                                            <tr style="height: 31px">
                                                <td></td>
                                                <td class="td">Branch Name:</td>
                                                <td></td>
                                                <td class="td"></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr style="height: 31px">
                                                <td></td>
                                                <td class="td">Department Name:</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr style="height: 31px">
                                                <td></td>
                                                <td class="td">Occupation:</td>
                                                <td class="td">
                                                    <asp:Label ID="lblOccupation" runat="server" Text=""></asp:Label></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr style="height: 31px">
                                                <td></td>
                                                <td class="td">Present Days:</td>
                                                <td class="td">
                                                    <asp:Label ID="lblPresentDay" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td class="td">Salary Month:</td>
                                                <td class="td">
                                                    <asp:Label ID="lblSalaryMonth" runat="server" Text=""></asp:Label></td>
                                                <td></td>
                                            </tr>
                                        </table>

                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%; height: 10px" colspan="6"></td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend></legend>
                                        <table style="width: 100%; height: 250px">
                                            <tr>
                                                <td style="height: 31px; width: 30%"></td>
                                                <td class="td" style="width: 10%;">Earning:</td>
                                                <td class="td" style="width: 15%;"></td>
                                                <td class="td" style="width: 10%;"></td>

                                                <td class="td">DeDuction:</td>
                                                <td style="width: 20%"></td>
                                            </tr>
                                            <tr class="td" style="height: 31px">
                                                <td></td>
                                                <td class="td">Salary:</td>
                                                <td>
                                                    <asp:Label ID="lblsalary" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td class="td">Other:</td>
                                                <td>00:00</td>
                                                <td></td>
                                            </tr>
                                            <tr class="td" style="height: 31px">
                                                <td></td>
                                                <td class="td">OT:</td>
                                                <td>
                                                    <asp:Label ID="lblOT" runat="server" Text="00:00"></asp:Label>
                                                </td>
                                                <td class="td">Advance:</td>
                                                <td>00:00</td>
                                                <td></td>
                                            </tr>
                                            <tr class="td" style="height: 31px">
                                                <td></td>
                                                <td class="td">Bonus:</td>
                                                <td>00:00</td>
                                                <td class="td"></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="td" style="height: 31px">
                                                <td></td>
                                                <td class="td">Accomodation:</td>
                                                <td>00:00</td>
                                                <td class="td"></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="td" style="height: 31px">
                                                <td></td>
                                                <td class="td">Allowance:</td>
                                                <td>00:00</td>
                                                <td class="td"></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <tr class="td" style="height: 31px">
                                                <td></td>
                                                <td class="td">Total Earning::</td>
                                                <td>
                                                    <asp:Label ID="lbltotalerning" runat="server" Text="00:00"></asp:Label>
                                                </td>
                                                <td class="td">Total Deduction::</td>
                                                <td>
                                                    <asp:Label ID="lbltotaldeduction" runat="server" Text="00:00"></asp:Label>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="td" style="height: 31px">
                                                <td></td>
                                                <td class="td"></td>
                                                <td></td>
                                                <td class="td">Net Pay Amount:</td>
                                                <td>
                                                    <asp:Label ID="lblnetpayment" runat="server" Text="00:00"></asp:Label>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>


                        </table>
                        <table style="width: 100%; border-bottom-width: 10px; border-bottom-color: black;">
                            <tr>
                                <td style="width: 100%; height: 20px" colspan="6"></td>
                            </tr>
                            <tr style="height: 31px">
                                <td style="height: 31px; width: 30%"></td>
                                <td class="td" style="width: 10%;">Prepared By:</td>
                                <td class="td" style="width: 15%;">Checked By:</td>
                                <td class="td" style="width: 10%;">Received By:                                               
                                </td>
                                <td class="td"></td>
                                <td style="width: 20%"></td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
            </div>
            <div style="height: 20px; width: 100%">
                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="buttonn" OnClick="btnPrint_Click" />
            </div>
            <asp:GridView ID="GvSalaryDetails" runat="server" AutoGenerateDeleteButton="false" Height="100px" Visible="false"></asp:GridView>
        </div>
    </form>
</body>
</html>
