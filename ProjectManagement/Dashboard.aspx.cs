﻿using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Data.SqlClient;
using System.Configuration;


namespace ProjectManagement
{
    public partial class Dashboard : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            Chart();

            if (!IsPostBack)
            {
                TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    Label1.Text = Session["username"].ToString();
                }
            }
        }
        protected void Chart()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            DataSet ds = new DataSet();
            string query = "select DATEPART(MM,AssignedDate) AS OrderMonth,EstimatedHours from ASSIGNED";
            SqlCommand cmd = new SqlCommand(query, con);

            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            da.Fill(ds, "PremiumData");

            if (ds.Tables["PremiumData"].Rows.Count > 0)
            {

                Chart1.DataSource = ds.Tables["PremiumData"];
                // Set series members names for the X and Y values

                Chart1.Series["Count"].XValueMember = "OrderMonth";

                Chart1.Series["Count"].YValueMembers = "EstimatedHours";

                Chart1.Series["Count"].IsValueShownAsLabel = true;

                if (ChartType.SelectedItem.ToString() == "Bar")
                {

                    Chart1.Series["Count"].ChartType = SeriesChartType.Bar;

                    Chart1.Titles[1].Text = "Bar Chart";

                }

                //else if (ChartType.SelectedItem.ToString() == "Pie")
                //{

                //Chart1.Series["Count"]["3DLabelLineSize"] = "100";

                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;

                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Inclination = 15;

                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Rotation = 10;

                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Perspective = 10;

                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.IsRightAngleAxes = false;

                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 0;

                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.IsClustered = false;

                //Chart1.Series[0]["PieDrawingStyle"] = "SoftEdge";

                //Chart1.Series["Count"].ChartType = SeriesChartType.Pie;

                //Chart1.Series["Count"]["PieLabelStyle"] = "OutsideInColumn";

                //Chart1.Series["Count"]["PieOutsideLabelPlacement"] = "Right";

                //Chart1.Titles[1].Text = "Pie Chart";

                //}

                //else if (ChartType.SelectedItem.ToString() == "Line")
                //{

                //Chart1.Series["Count"].ChartType = SeriesChartType.Line;

                //Chart1.Titles[1].Text = "Line Chart";

                //}

                else if (ChartType.SelectedItem.ToString() == "Funnel")
                {

                    Chart1.Series["Count"]["FunnelStyle"] = "YIsHeight";

                    Chart1.Series["Count"]["FunnelLabelStyle"] = "OutsideInColumn";

                    Chart1.Series["Count"]["FunnelOutsideLabelPlacement"] = "Right";

                    Chart1.Series["Count"]["FunnelPointGap"] = "1";

                    Chart1.Series["Count"]["Funnel3DDrawingStyle"] = "SoftEdge";

                    Chart1.Series["Count"].ChartType = SeriesChartType.Funnel;

                    Chart1.Series["Count"]["Funnel3DRotationAngle"] = "5";

                    Chart1.Series["Count"]["Funnel3DDrawingStyle"] = "CircularBase";

                    Chart1.Series["Count"]["FunnelMinPointHeight"] = "0";

                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Inclination = 15;

                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Rotation = 10;

                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Perspective = 10;

                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.IsRightAngleAxes = false;

                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 0;

                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.IsClustered = false;

                    Chart1.Titles[1].Text = "Funnel Chart";

                }

                //else if (ChartType.SelectedItem.ToString() == "Area")
                //{

                //Chart1.Series["Count"].ChartType = SeriesChartType.Area;

                //Chart1.Titles[1].Text = "Area Chart";

                //}

                else if (ChartType.SelectedItem.ToString() == "Pyramid")
                {

                    Chart1.Series["Count"]["PyramidLabelStyle"] = "OutsideInColumn";

                    Chart1.Series["Count"]["PyramidPointGap"] = "1";

                    Chart1.Series["Count"]["PyramidMinPointHeight"] = "0";

                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

                    Chart1.Series["Count"]["Pyramid3DRotationAngle"] = "5";

                    Chart1.Series["Count"]["Pyramid3DDrawingStyle"] = "SquareBase";

                    Chart1.Series["Count"]["PyramidValueType"] = "Linear";

                    Chart1.Series["Count"].ChartType = SeriesChartType.Pyramid;

                    Chart1.Titles[1].Text = "Pyramid Chart";

                }

                else
                {

                    Chart1.Series["Count"].ChartType = SeriesChartType.Column;

                    Chart1.Titles[1].Text = "Column Chart";

                }

                // Data bind to the selected data source

                Chart1.DataBind();


            }
            //Pie Chart
            DataSet ds1 = new DataSet();
            string query1 = "select DATEPART(dd,StartDateTime) AS Day,TotalWage from HoursWorked";
            SqlCommand cmd1 = new SqlCommand(query1, con);

            cmd1.ExecuteNonQuery();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);

            da1.Fill(ds1, "PremiumData");

            if (ds1.Tables["PremiumData"].Rows.Count > 0)
            {
                Chart2.DataSource = ds1.Tables["PremiumData"];
                // Set series members names for the X and Y values

                Chart2.Series["Count"].XValueMember = "Day";

                Chart2.Series["Count"].YValueMembers = "TotalWage";

                Chart2.Series["Count"].IsValueShownAsLabel = true;

                Chart2.Series["Count"]["3DLabelLineSize"] = "100";

                Chart2.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;

                Chart2.ChartAreas["ChartArea1"].Area3DStyle.Inclination = 15;

                Chart2.ChartAreas["ChartArea1"].Area3DStyle.Rotation = 10;

                Chart2.ChartAreas["ChartArea1"].Area3DStyle.Perspective = 10;

                Chart2.ChartAreas["ChartArea1"].Area3DStyle.IsRightAngleAxes = false;

                Chart2.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 0;

                Chart2.ChartAreas["ChartArea1"].Area3DStyle.IsClustered = false;

                Chart2.Series[0]["PieDrawingStyle"] = "SoftEdge";

                Chart2.Series["Count"].ChartType = SeriesChartType.Pie;

                Chart2.Series["Count"]["PieLabelStyle"] = "OutsideInColumn";

                Chart2.Series["Count"]["PieOutsideLabelPlacement"] = "Right";

                Chart2.Titles[1].Text = "Pie Chart";
                Chart2.DataBind();

            }


            //Line Chart
            DataSet ds2 = new DataSet();
            string query2 = "select DATEPART(dd,ExpectedTime) AS Days,TotalCost from REQUESTEDQUOTE";
            SqlCommand cmd2 = new SqlCommand(query2, con);

            cmd2.ExecuteNonQuery();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);

            da2.Fill(ds2, "PremiumData");

            if (ds2.Tables["PremiumData"].Rows.Count > 0)
            {
                Chart3.DataSource = ds2.Tables["PremiumData"];
                // Set series members names for the X and Y values

                Chart3.Series["Count"].XValueMember = "Days";

                Chart3.Series["Count"].YValueMembers = "TotalCost";

                Chart3.Series["Count"].IsValueShownAsLabel = true;
                Chart3.Series["Count"].ChartType = SeriesChartType.Line;

                Chart3.Titles[1].Text = "Line Chart";
                Chart3.DataBind();
            }
            //Area Chart
            DataSet ds3 = new DataSet();
            string query3 = "select DATEPART(dd,ExpectedTime) AS Days,TotalCost from REQUESTEDQUOTE";
            SqlCommand cmd3 = new SqlCommand(query3, con);

            cmd3.ExecuteNonQuery();
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);

            da3.Fill(ds3, "PremiumData");

            if (ds3.Tables["PremiumData"].Rows.Count > 0)
            {
                Chart4.DataSource = ds3.Tables["PremiumData"];
                // Set series members names for the X and Y values

                Chart4.Series["Count"].XValueMember = "Days";

                Chart4.Series["Count"].YValueMembers = "TotalCost";

                Chart4.Series["Count"].IsValueShownAsLabel = true;

                Chart4.Series["Count"].ChartType = SeriesChartType.Area;

                Chart4.Titles[1].Text = "Area Chart";
            }
        }
    }


    // protected void ShieldChart1_TakeDataSource(object sender, Shield.Web.UI.ChartTakeDataSourceEventArgs e)
    // {
    //     ShieldChart1.DataSource = new object[]
    //{
    //    new {Quarter = "Q1", Sales = 312 },
    //    new {Quarter = "Q2", Sales = 212 },
    //    new {Quarter = "Q3", Sales = 322 },
    //    new {Quarter = "Q4", Sales = 128 }
    //};
    // }
    // protected void ShieldChart1_SelectionChanged(object sender, Shield.Web.UI.ChartSelectionChangedEventArgs e)
    // {
    //     if (e.Selected)
    //     {
    //         //SelectedQuarter = e.Name;
    //         //DataPerformance = GetPerformanceData().Where(d => d.Quarter == SelectedQuarter).ToList();
    //     }
    // }


}