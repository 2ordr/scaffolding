﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;

namespace ProjectManagement
{
    public partial class CreateUser : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetall();
                Label1.Text = Session["username"].ToString();
            }
        }
        public void resetall()
        {

            txtName.Text = "";
            txtUName.Text = "";
            txtPassword.Text = "";
            txtMobile.Text = "";
            txtEmail.Text = "";
            txtWage.Text = "";

        }
        protected void Btnview_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewUserVertical.aspx?");
        }
        protected void Btnadd_Click(object sender, EventArgs e)
        {
            if (txtName.Text == String.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('User Not Created');", true);
            }

            else
            {
                try
                {
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    com.Connection = con;
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_InsertUserDetails";
                    com.Parameters.Add("@NAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;
                    com.Parameters.Add("@USERNAME", System.Data.SqlDbType.VarChar).Value = txtUName.Text;
                    com.Parameters.Add("@PASSWORD", System.Data.SqlDbType.VarChar).Value = txtPassword.Text;
                    com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
                    com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
                    com.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = txtWage.Text;
                    com.Parameters.Add("@USERTYPE", System.Data.SqlDbType.VarChar).Value = ddlUserType.SelectedItem.Text;
                    com.ExecuteNonQuery();
                    com.Parameters.Clear();
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('User  Created');", true);
                    resetall();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('User Not Created');", true);
                }
            }
        }


        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}