USE [ScaffOldingManagementSystem]
GO
/****** Object:  StoredProcedure [dbo].[sp_getOrderByDate]    Script Date: 11/01/2015 16:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getOrderById] 
	@ORDERTOTAL NUMERIC(18,2)
AS
	SELECT distinct *
	FROM ORDERS,HAS,CUSTOMER,STOCKSITEM
	WHERE ORDERS.OrderTotal=@ORDERTOTAL AND 
	ORDERS.OrderDate=HAS.DateTime and
	CUSTOMER.CustId=HAS.CustId 
	AND ORDERS.OrderId=HAS.OrderId
	AND HAS.ItemCode=STOCKSITEM.ItemCode