﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace ProjectManagement
{
    public partial class SalaryDetails : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;


                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_getSalaryDetails";

                    cmd.Parameters.Add("@I_SID", System.Data.SqlDbType.BigInt).Value = Request.QueryString["SalaryId"];
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        GvSalaryDetails.DataSource = dt;
                        GvSalaryDetails.DataBind();

                        lblUserName.Text = dt.Tables[0].Rows[0]["FName"].ToString();
                        lblUserId.Text = dt.Tables[0].Rows[0]["EmpId"].ToString();
                        lblOccupation.Text = dt.Tables[0].Rows[0]["Occupation"].ToString();
                        lbltotalerning.Text = dt.Tables[0].Rows[0]["TOTALSALARY"].ToString();
                        lblnetpayment.Text = dt.Tables[0].Rows[0]["TOTALSALARY"].ToString();
                        lblsalary.Text = dt.Tables[0].Rows[0]["TOTALSALARY"].ToString();
                    }
                    con.Close();
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {

            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>PrintGridDataAll();</script>");
        }
    }
}