﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace ProjectManagement
{
    public partial class customer_overview : System.Web.UI.Page
    {

        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //    this.LblCustName.Text = Request.QueryString["CustName"];
                //    this.LblCustName.Text = Request.QueryString["CustId"];

                //if (Session["SELECTEDVALUES"] != null)
                //{
                //    ArrayList selectedValues = (ArrayList)Session["SELECTEDVALUES"];
                //    for (int i = 0; i < selectedValues.Count; i++)
                //    {

                SqlConnection con = new SqlConnection(cs);
                con.Open();

                //String str = "select * from CUSTOMER where CustId=" + selectedValues[i].ToString();

                SqlCommand cmd = new SqlCommand("select * from CUSTOMER where CustId = @CustId", con);
                String var = Request.QueryString["CustId"];
                cmd.Parameters.Add("@CustId", System.Data.SqlDbType.VarChar).Value = var;

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                DataSet dt = new DataSet();

                adpt.Fill(dt);

                GridView1.DataSource = dt;

                GridView1.DataBind();
                LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                LblCustId.Text = dt.Tables[0].Rows[0]["CustId"].ToString();


                //Session["labeltext"] = LblCustName.Text;
                Session["labelid"] = LblCustId.Text;

            }
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Customer.aspx");
        }
    }
}


