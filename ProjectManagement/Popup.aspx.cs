﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using System.Data;

namespace ProjectManagement
{
    public partial class Popup : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {

            //LblCustId.Text = Session["labelid"].ToString();


            if (!IsPostBack)
            {
                resetall();

                if (Global.flag == 0)
                {

                    LblCustId.Text = Request.QueryString["CustId"];
                    BtnUpdate.Visible = false;
                }
                if (Global.flag == 1)
                {
                    BtnSaveContact.Visible = false;
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();

                    SqlCommand cmd = new SqlCommand("select * from CONTACTS where ContId = @I_CONTID", con);

                    String var = Request.QueryString["ContId"];
                    cmd.Parameters.Add("@I_CONTID", System.Data.SqlDbType.VarChar).Value = var;

                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                    DataSet dt = new DataSet();

                    adpt.Fill(dt);

                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        GridViewPopup.DataSource = dt;

                        GridViewPopup.DataBind();

                        LblContId.Text = dt.Tables[0].Rows[0]["CONTID"].ToString();
                        txtName.Text = dt.Tables[0].Rows[0]["Name"].ToString();
                        LblCustId.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                        txtEmail.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                        txtAddress.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                        txtcity.Text = dt.Tables[0].Rows[0]["City"].ToString();
                        txtCountry.Text = dt.Tables[0].Rows[0]["Country"].ToString();
                        txtZipCode.Text = dt.Tables[0].Rows[0]["ZipCode"].ToString();
                        txtMobile.Text = dt.Tables[0].Rows[0]["Mobile"].ToString();
                        txtphone.Text = dt.Tables[0].Rows[0]["Phone"].ToString();
                        txtProvince.Text = dt.Tables[0].Rows[0]["Province"].ToString();
                        txtRemark.Text = dt.Tables[0].Rows[0]["Remark"].ToString();
                    }
                    con.Close();
                }
            }
        }

        protected void BtnSaveContact_Click(object sender, EventArgs e)
        {
            int id = 0;

            id = Convert.ToInt32(LblCustId.Text);
            if (txtName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertContactsDetails";
                com.Parameters.Add("@NAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;
                com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = id;
                com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
                com.Parameters.Add("@PHONE", System.Data.SqlDbType.VarChar).Value = txtphone.Text;
                com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
                com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress.Text;
                com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtcity.Text;
                com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince.Text;
                com.Parameters.Add("@REMARK", System.Data.SqlDbType.VarChar).Value = txtRemark.Text;
                com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.VarChar).Value = txtZipCode.Text;
                com.Parameters.Add("@COUNTRY", System.Data.SqlDbType.VarChar).Value = txtCountry.Text;



                //com.Parameters.Add("@DELETESTATUS", System.Data.SqlDbType.BigInt).Value =false;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }
            resetall();
            //Response.Redirect("customer contacts.aspx");
            string display = "Contact Saved";

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + display + "');", true);//this will dispaly the alert box


            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close_Window", "self.close();", true);

        }

        public void resetall()
        {

            txtName.Text = "";
            txtMobile.Text = "";
            txtphone.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtcity.Text = "";
            txtProvince.Text = "";
            txtZipCode.Text = "";
            txtCountry.Text = "";

        }

        protected void BtnUpdate_Click(object sender, EventArgs e)
        {

            int id1 = 0;

            id1 = Convert.ToInt32(LblCustId.Text);

            if (txtName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_UpdateContactsDetails";

                com.Parameters.Add("@I_CONTID", System.Data.SqlDbType.VarChar).Value = LblContId.Text;
                com.Parameters.Add("@NAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;
                com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = id1;//LblCustId.Text;
                com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
                com.Parameters.Add("@PHONE", System.Data.SqlDbType.VarChar).Value = txtphone.Text;
                com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
                com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress.Text;
                com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtcity.Text;
                com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince.Text;
                com.Parameters.Add("@REMARK", System.Data.SqlDbType.VarChar).Value = txtRemark.Text;
                com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.VarChar).Value = txtZipCode.Text;
                com.Parameters.Add("@COUNTRY", System.Data.SqlDbType.VarChar).Value = txtCountry.Text;
                //com.Parameters.Add("@DELETESTATUS", System.Data.SqlDbType.BigInt).Value =false;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }
            string display = "Contact Updated";

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + display + "');", true);//this will dispaly the alert box


            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close_Window", "self.close();", true);
        }


    }
}