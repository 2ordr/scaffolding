﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Quatation request form.aspx.cs" Inherits="ProjectManagement.Quatation_request_form" EnableEventValidation="false" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>New - Projects - Scaffolding</title>

    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
     <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>


    <style>
        .divWaiting {
            position: absolute;
            background-color: #fafafa;
            z-index: 2147483647;
            opacity: 0.8;
            overflow: hidden;
            text-align: center;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            padding-top: 20%;
        }

        headform1 {
            color: #383838;
        }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .auto-style2 {
            height: 15px;
        }

        .Itemalign {
            text-align: center;
        }

        *.MyCalendar1 .ajax__calendar_container {
            border: 1px solid #646464;
            background-color: #3A4F63;
            color: white;
            position: absolute;
            left: 1px;
            top: 3px;
        }

        .MyCalendar1 .ajax__calendar_footer {
            font-size: 0px;
            height: 0px;
        }

        .MyCalendar1 .ajax__calendar_other .ajax__calendar_day, .MyCalendar1 .ajax__calendar_other .ajax__calendar_year {
            color: black;
        }

        .MyCalendar1 .ajax__calendar_hover .ajax__calendar_day, .MyCalendar1 .ajax__calendar_hover .ajax__calendar_month, .MyCalendar1 .ajax__calendar_hover .ajax__calendar_year {
            color: black;
        }

        .MyCalendar1 .ajax__calendar_active .ajax__calendar_day, .MyCalendar1 .ajax__calendar_active .ajax__calendar_month, .MyCalendar1 .ajax__calendar_active .ajax__calendar_year {
            color: black;
            font-weight: bold;
        }
    </style>


    <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>

    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward()
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>


</head>

<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;" onload="noBack();" onpageshow="if(event.persisted) noBack();" onunload="">
    <form id="form1" runat="server">

       <%-- <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>--%>

        <div style="height: 1000px; width: 1920px;">
            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox1" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150"></asp:TextBox>

                                <%--<div class="x-panel-body x-panel-body-default x-box-layout-ct x-panel-body-default x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="searchBox-body" role="presentation" style="left: 0px; top: 0px; width: 177px; height: 28px;">
                                    <div class="x-box-inner " id="searchBox-innerCt" role="presentation" style="width: 177px; height: 28px;">
                                        <div class="x-box-target" id="searchBox-targetEl" role="presentation" style="width: 177px; height: 19px;">
                                            <table class="x-field x-table-plain x-form-item x-form-type-text x-box-item x-field-default x-hbox-form-item" id="combobox-1022" role="presentation" style="margin: 0px; left: 0px; top: 1px; width: 139px; right: auto; table-layout: fixed;" cellpadding="0">
                                                <tbody>
                                                    <tr class="x-form-item-input-row" id="combobox-1022-inputRow" role="presentation">
                                                        <td width="105" class="x-field-label-cell" id="combobox-1022-labelCell" role="presentation" valign="top" style="display: none;" halign="left">
                                                            <label class="x-form-item-label x-unselectable x-form-item-label-left" id="combobox-1022-labelEl" style="width: 100px; margin-right: 5px;" for="combobox-1022-inputEl" unselectable="on"></label>
                                                        </td>
                                                        <td class="x-form-item-body  " id="combobox-1022-bodyEl" role="presentation" style="width: 100%;" colspan="3">
                                                            <table class="x-form-trigger-wrap" id="combobox-1022-triggerWrap" role="presentation" style="width: 100%; table-layout: fixed;" cellspacing="0" cellpadding="0">
                                                                <tbody role="presentation">
                                                                    <tr role="presentation">
                                                                        <td class="x-form-trigger-input-cell" id="combobox-1022-inputCell" role="presentation" style="width: 100%;">
                                                                            <div class="x-hide-display x-form-data-hidden" id="ext-gen1091" role="presentation"></div>
                                                                            <input name="combobox-1022-inputEl" class="x-field-without-trigger x-form-empty-field x-form-text " id="combobox-1022-inputEl" style="width: 100%;" type="text" placeholder="Search (Ctrl+/)" autocomplete="off" /><span class="icon-search"></span></td>
                                                                        <td class=" x-trigger-cell x-unselectable" id="ext-gen1090" role="presentation" valign="top" style="width: 23px; display: none;">
                                                                            <div class="x-trigger-index-0 x-form-trigger x-form-arrow-trigger x-form-trigger-first" id="ext-gen1089" role="presentation"></div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="x-form-invalid-under" id="combobox-1022-errorEl" role="alert" aria-live="polite" style="display: none;" colspan="2"></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="x-splitter x-box-item x-splitter-default x-splitter-vertical x-unselectable" id="splitter-1023" style="margin: 0px; left: 134px; top: 0px; width: 5px; height: 28px; right: auto; display: none;"></div>
                                            <a tabindex="0" class="x-btn btn-collapse x-unselectable x-box-item x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon btn-expanded" id="button-1024" style="border-width: 0px; margin: 0px; left: 149px; top: 2px; right: auto;" hidefocus="on" unselectable="on"><span class="x-btn-wrap" id="button-1024-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1024-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1024-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-collapse " id="button-1024-btnIconEl" role="presentation" unselectable="on">&nbsp;</span></span></span></a>
                                        </div>
                                    </div>
                                </div>--%>
                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Dashboard</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Customer</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-5" data-index="5"><a class="menu-link" href="ViewQuatation.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Quatation</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Order</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="View Project.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projects</span></a></li>


                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Setting</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <ul class="menu-group" id="ext-gen3448">

                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="ViewUserVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">My Tasks</span></a></li>



                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>--%>

                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <div class="allSides" style="width: 100%; height: 70px;">
                    <div class="divider"></div>
                    <asp:Label ID="Label10" runat="server" CssClass="clienthead" Text="New Quatation"></asp:Label>
                </div>
                <div style="width: 1%; float: left; height: 800px; border: solid 0px black;">
                </div>
                <div style="height: 50px">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/image/Step1.jpg" Height="50" Width="100" />
                </div>

                <div style="width: 82%; float: left; height: 800px; font-family: Calibri;">

                    <div style="width: 100%; float: left; height: 800px;">

                        <table style="width: 1725px;">
                            <tbody>
                                <tr>
                                    <td style="width: 40%;">
                                        <fieldset>

                                            <legend style="color: black; font-weight: bold; font-size: large">Basic Info</legend>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="text-align: right; width: 250px">Date                                           
                                                    </td>
                                                    <td style="width: 5px"></td>
                                                    <td style="">

                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDate" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                       
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Customer Name                                            
                                                    </td>
                                                    <td style="width: 5px"></td>
                                                    <td style="">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName" runat="server" Width="300px" Height="31px" AutoPostBack="true" OnTextChanged="txtName_TextChanged"></asp:TextBox>
                                                        <asp:Label ID="lblName" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>


                                                <%--   <tr>
                                                <td style="text-align: right;">Expected Time&nbsp;&nbsp;&nbsp;
                                                </td>
                                                <td colspan="2">
                                                    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
                                                    <asp:TextBox ID="TxtExTime" runat="server" Width="300px" Height="21px"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtExTime"></asp:CalendarExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right;">Expected Cost&nbsp;&nbsp;&nbsp;</td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="TxtExCost" runat="server" Width="300px" Height="21px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>--%>
                                                <tr>
                                                    <td style="text-align: right;">Responsed Time</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtResTime" runat="server" Width="300px" Height="31px"></asp:TextBox>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Total Cost</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtResCost" runat="server" Width="300px" Height="31px" TextMode="Number"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Customer Ack</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:DropDownList CssClass="search_categories" ID="ddlCustAck" runat="server" Width="300px" Height="36px">
                                                            <asp:ListItem>Yes</asp:ListItem>
                                                            <asp:ListItem>No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Admin Approve</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">

                                                        <asp:DropDownList CssClass="search_categories" ID="ddlAdminApp" runat="server" Width="300px" Height="36px">
                                                            <asp:ListItem>Yes</asp:ListItem>
                                                            <asp:ListItem>No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Quate Status</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">

                                                        <asp:DropDownList CssClass="search_categories" ID="ddquotestatus" runat="server" Width="300px" Height="36px">
                                                            <asp:ListItem>Draft</asp:ListItem>
                                                            <asp:ListItem>Send</asp:ListItem>
                                                            <asp:ListItem>Accepted</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Quote Possibility</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuoteposs" runat="server" Width="300px" Height="31px" TextMode="Number"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>

                                    <td style="width: 50%; vertical-align: top;">
                                        <div style="height: 450px">
                                            <fieldset style="height: 456px; width: 97%">

                                                <legend style="color: black; font-weight: bold; font-size: large">Basic Info</legend>

                                                <table style="width: 100%; font-family: Calibri; margin-top: 1px;">
                                                    <tbody>

                                                        <tr>
                                                            <td>
                                                                <div id="div1" runat="server" visible="true">
                                                                    <table style="margin-left: 4px;">
                                                                        <tr style="background-color: #F2F2F2; border-width: 5px;">
                                                                            <td style="border-color: white">Customer Name</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="divCustlist" runat="server" style="overflow: auto; height: 400px" visible="false">

                                                                    <table style="width: 100%; font-family: Calibri; margin-top: 2px;">
                                                                        <tbody>
                                                                            <%--<tr>
                                                                        <td style="width: 25%;"></td>
                                                                        <td style="width: 25%;"></td>
                                                                        <td style="width: 50%;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 10px;" colspan="3"></td>
                                                                    </tr>--%>
                                                                            <tr>
                                                                                <td style="width: 100%;" colspan="3">
                                                                                    <asp:GridView ID="GridView1" runat="server" ShowHeader="false" DataKeyNames="CustId" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                        <Columns>
                                                                                            <%--<asp:BoundField DataField="CustId" HeaderText="CustId" HeaderStyle-Width="50px" Visible="false" />--%>
                                                                                            <asp:BoundField DataField="CustName" HeaderText="Customer Name" HeaderStyle-Width="300px" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                                            <asp:CommandField SelectText="Select" ShowSelectButton="true" Visible="false" />
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </td>

                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 10px;" colspan="3"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </div>

                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div style="height: 25px;">
                            <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="10" runat="server" AssociatedUpdatePanelID="uptest">
                                <ProgressTemplate>
                                    <span>
                                        <asp:Image runat="server" ID="imgwait" ImageAlign="Middle" ImageUrl="~/image/22222.png"></asp:Image></span>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>
                        </div>
                        <div id="headerdiv" runat="server" style="width: 1700px;" visible="false">
                            <table border="1" id="tblheader" style="width: 1700px; border-width: 5px; border-color: white">
                                <tr style="background-color: #F2F2F2;">
                                    <td style="width: 20px; text-align: center; border-width: 5px; border-color: white"></td>

                                    <td style="width: 250px; text-align: center; border-width: 5px; border-color: white">Item Name</td>

                                    <td style="width: 250px; text-align: center; border-width: 5px; border-color: white">@Price</td>

                                    <td style="width: 250px; text-align: center; border-width: 5px; border-color: white">Quantity</td>

                                    <td style="width: 250px; text-align: center; border-width: 5px; border-color: white">Requested Quantity</td>

                                    <td style="width: 250px; text-align: center; border-width: 5px; border-color: white">Amount</td>

                                    <%-- <td style="width: 40px; text-align: center;"></td>--%>
                                </tr>
                            </table>
                        </div>
                        <div id="divstk" runat="server" style="overflow: auto; width: 1700px; height: 270px;" visible="false">
                            <table style="width: 1725px;">

                                <tr>
                                    <td style="width: 100%;">
                                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" OnRowDataBound="GridView2_RowDataBound1" ShowHeader="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="lblItem" runat="server" Checked="true" Visible="false" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="20px" />
                                                    <ItemStyle Width="20px" CssClass="Itemalign" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Item Name" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemName1" runat="server" Height="36px" AutoPostBack="true" DataTextField="ItemName" DataValueField="ItemCode" OnSelectedIndexChanged="ddlItemName1_SelectedIndexChanged"></asp:DropDownList>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                    <ItemStyle Width="200px" CssClass="Itemalign" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="@Price" HeaderStyle-Width="100px" HeaderStyle-VerticalAlign="Middle" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="200px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                    <ItemStyle Width="200px" CssClass="Itemalign" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Quantity" HeaderStyle-Width="100px" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQuantity" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                    <ItemStyle Width="200px" CssClass="Itemalign" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Requested Quantity" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQuantity" runat="server" AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" BackColor="#F2F2F2"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                    <ItemStyle Width="200px" CssClass="Itemalign" />

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cost" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTot" runat="server" Text="" Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                    <ItemStyle Width="200px" CssClass="Itemalign" />

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" ItemStyle-Width="150px">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="250px" />
                                                    <ItemStyle Width="200px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div id="divStockdv1" runat="server" style="overflow: auto;" visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="height: 20px;">
                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/image/Step2.jpg" Height="50" Width="100" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;"></td>
                                </tr>
                                <tr>

                                    <td style="text-align: right; width: 8%;">Item Name:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 18%;">
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname1" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname1_SelectedIndexChanged"></asp:DropDownList>
                                    </td>
                                    <td style="text-align: right; width: 5%;">Price:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 12%;">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice1" runat="server" Height="21px" Width="140px"></asp:TextBox>
                                    </td>
                                    <td style="text-align: right; width: 5%;">Quantity:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 13%;">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity1" runat="server" Height="21px" Width="140px"></asp:TextBox>
                                    </td>
                                    <td style="text-align: right; width: 8%;">Quated Quantity:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 12%;">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty1" runat="server" Height="21px" Width="140px" TextMode="Number" AutoPostBack="true" OnTextChanged="txtQPrice1_TextChanged" Text="0"></asp:TextBox>
                                    </td>
                                    <td style="text-align: right; width: 8%;">Quated Price:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 13%;">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice1" runat="server" Height="21px" Width="140px" TextMode="Number" Text="0"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname2" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname2_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice2" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity2" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty2" runat="server" Height="21px" Width="140px" TextMode="Number" AutoPostBack="true" OnTextChanged="txtQPrice2_TextChanged" Text="0"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice2" runat="server" Height="21px" Width="140px" TextMode="Number" Text="0"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname3" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname3_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice3" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>

                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity3" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty3" runat="server" Height="21px" Width="140px" TextMode="Number" AutoPostBack="true" OnTextChanged="txtQPrice3_TextChanged" Text="0"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice3" runat="server" Height="21px" Width="140px" TextMode="Number" Text="0"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname4" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname4_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice4" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity4" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty4" runat="server" Height="21px" Width="140px" TextMode="Number" AutoPostBack="true" OnTextChanged="txtQPrice4_TextChanged" Text="0"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice4" runat="server" Height="21px" Width="140px" TextMode="Number" Text="0"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname5" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname5_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice5" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity5" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty5" runat="server" Height="21px" Width="140px" TextMode="Number" Text="0" AutoPostBack="true" OnTextChanged="txtQPrice5_TextChanged"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice5" runat="server" Height="21px" Width="140px" TextMode="Number" Text="0"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 5px;" colspan="10"></td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="9"></td>
                                    <td>
                                        <asp:Button CssClass="button" ID="btnAddmore" Style="background-color: #009900;" runat="server" Text="+Add More" Height="25px" OnClick="btnAddmore_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divStockdv2" runat="server" style="overflow-y: auto; height: 138px;" visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>

                                    <td style="text-align: right; width: 8%;">Item Name:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 18%;">
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname6" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname6_SelectedIndexChanged"></asp:DropDownList>
                                    </td>
                                    <td style="text-align: right; width: 5%;">Price:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 12%;">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice6" runat="server" Width="140px" Height="21px"></asp:TextBox>
                                    </td>
                                    <td style="text-align: right; width: 5%;">Quantity:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 12%;">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity6" runat="server" Height="21px" Width="140px"></asp:TextBox>
                                    </td>
                                    <td style="text-align: right; width: 9%;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td style="width: 12%;">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty6" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right; width: 8%;">Quated Price:&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 13%;">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice6" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice6_TextChanged" TextMode="Number"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname7" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname7_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice7" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity7" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty7" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice7" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice7_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname8" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname8_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice8" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity8" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty8" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice8" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice8_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname9" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname9_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice9" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity9" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty9" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice9" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice9_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname10" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname10_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice10" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity10" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty10" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice10" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice10_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname11" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname11_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice11" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity11" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty11" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice11" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice11_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname12" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname12_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice12" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity12" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty12" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice12" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice12_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname13" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname13_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice13" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity13" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty13" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice13" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice13_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname14" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname14_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice14" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity14" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty14" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice14" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice14_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Item Name:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemname15" runat="server" Width="300px" Height="33px" AutoPostBack="true" DataTextField="ItemName" DataValueField="Itemcode" OnSelectedIndexChanged="ddlItemname15_SelectedIndexChanged"></asp:DropDownList></td>
                                    <td style="text-align: right;">Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice15" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity15" runat="server" Height="21px" Width="140px"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Quantity:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQQty15" runat="server" Height="21px" Width="140px" TextMode="Number"></asp:TextBox></td>
                                    <td style="text-align: right;">Quated Price:&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQPrice15" runat="server" AutoPostBack="true" Height="21px" Width="140px" OnTextChanged="txtQPrice15_TextChanged" TextMode="Number"></asp:TextBox></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="shadowdiv" style="width: 100%; float: left; height: 100px; text-align: center; border: solid 0px black;">
                    <table>
                        <tbody>
                            <tr>

                                <td>

                                    <asp:Button CssClass="buttonn" ID="Button1" runat="server" Text="+ADD " Width="101px" Style="background-color: #009900;" OnClick="Button1_Click" />

                                </td>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="Button2" runat="server" Text="VIEW" Width="120px" Style="background-color: #009933" OnClick="ViewButton_Click" />

                                </td>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="Button4" runat="server" Text="DELETE" Width="120px" Style="background-color: #009933" />
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>



            </div>

        </div>
    </form>
</body>
</html>
