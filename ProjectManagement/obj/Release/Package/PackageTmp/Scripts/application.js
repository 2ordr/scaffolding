﻿(function () {
    (function (window, undefined) {
        var document = window.document, navigator = window.navigator, location = window.location; var jQuery = (function () {
            var jQuery = function (selector, context) { return new jQuery.fn.init(selector, context, rootjQuery); }, _jQuery = window.jQuery, _$ = window.$, rootjQuery, quickExpr = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/, rnotwhite = /\S/, trimLeft = /^\s+/, trimRight = /\s+$/, rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>)?$/, rvalidchars = /^[\],:{}\s]*$/, rvalidescape = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, rvalidtokens = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g, rwebkit = /(webkit)[ \/]([\w.]+)/, ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/, rmsie = /(msie) ([\w.]+)/, rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/, rdashAlpha = /-([a-z]|[0-9])/ig, rmsPrefix = /^-ms-/, fcamelCase = function (all, letter) { return (letter + "").toUpperCase(); }, userAgent = navigator.userAgent, browserMatch, readyList, DOMContentLoaded, toString = Object.prototype.toString, hasOwn = Object.prototype.hasOwnProperty, push = Array.prototype.push, slice = Array.prototype.slice, trim = String.prototype.trim, indexOf = Array.prototype.indexOf, class2type = {}; jQuery.fn = jQuery.prototype = {
                constructor: jQuery, init: function (selector, context, rootjQuery) {
                    var match, elem, ret, doc; if (!selector) { return this; }
                    if (selector.nodeType) { this.context = this[0] = selector; this.length = 1; return this; }
                    if (selector === "body" && !context && document.body) { this.context = document; this[0] = document.body; this.selector = selector; this.length = 1; return this; }
                    if (typeof selector === "string") {
                        if (selector.charAt(0) === "<" && selector.charAt(selector.length - 1) === ">" && selector.length >= 3) { match = [null, selector, null]; } else { match = quickExpr.exec(selector); }
                        if (match && (match[1] || !context)) {
                            if (match[1]) {
                                context = context instanceof jQuery ? context[0] : context; doc = (context ? context.ownerDocument || context : document); ret = rsingleTag.exec(selector); if (ret) { if (jQuery.isPlainObject(context)) { selector = [document.createElement(ret[1])]; jQuery.fn.attr.call(selector, context, true); } else { selector = [doc.createElement(ret[1])]; } } else { ret = jQuery.buildFragment([match[1]], [doc]); selector = (ret.cacheable ? jQuery.clone(ret.fragment) : ret.fragment).childNodes; }
                                return jQuery.merge(this, selector);
                            } else {
                                elem = document.getElementById(match[2]); if (elem && elem.parentNode) {
                                    if (elem.id !== match[2]) { return rootjQuery.find(selector); }
                                    this.length = 1; this[0] = elem;
                                }
                                this.context = document; this.selector = selector; return this;
                            }
                        } else if (!context || context.jquery) {
                            return (context || rootjQuery).find(selector);
                        } else { return this.constructor(context).find(selector); }
                    } else if (jQuery.isFunction(selector)) { return rootjQuery.ready(selector); }
                    if (selector.selector !== undefined) { this.selector = selector.selector; this.context = selector.context; }
                    return jQuery.makeArray(selector, this);
                }, selector: "", jquery: "1.7.1", length: 0, size: function () { return this.length; }, toArray: function () { return slice.call(this, 0); }, get: function (num) { return num == null ? this.toArray() : (num < 0 ? this[this.length + num] : this[num]); }, pushStack: function (elems, name, selector) {
                    var ret = this.constructor(); if (jQuery.isArray(elems)) { push.apply(ret, elems); } else { jQuery.merge(ret, elems); }
                    ret.prevObject = this; ret.context = this.context; if (name === "find") { ret.selector = this.selector + (this.selector ? " " : "") + selector; } else if (name) { ret.selector = this.selector + "." + name + "(" + selector + ")"; }
                    return ret;
                }, each: function (callback, args) { return jQuery.each(this, callback, args); }, ready: function (fn) { jQuery.bindReady(); readyList.add(fn); return this; }, eq: function (i) { i = +i; return i === -1 ? this.slice(i) : this.slice(i, i + 1); }, first: function () { return this.eq(0); }, last: function () { return this.eq(-1); }, slice: function () { return this.pushStack(slice.apply(this, arguments), "slice", slice.call(arguments).join(",")); }, map: function (callback) { return this.pushStack(jQuery.map(this, function (elem, i) { return callback.call(elem, i, elem); })); }, end: function () { return this.prevObject || this.constructor(null); }, push: push, sort: [].sort, splice: [].splice
            }; jQuery.fn.init.prototype = jQuery.fn; jQuery.extend = jQuery.fn.extend = function () {
                var options, name, src, copy, copyIsArray, clone, target = arguments[0] || {}, i = 1, length = arguments.length, deep = false; if (typeof target === "boolean") { deep = target; target = arguments[1] || {}; i = 2; }
                if (typeof target !== "object" && !jQuery.isFunction(target)) { target = {}; }
                if (length === i) { target = this; --i; }
                for (; i < length; i++) {
                    if ((options = arguments[i]) != null) {
                        for (name in options) {
                            src = target[name]; copy = options[name]; if (target === copy) { continue; }
                            if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)))) {
                                if (copyIsArray) { copyIsArray = false; clone = src && jQuery.isArray(src) ? src : []; } else { clone = src && jQuery.isPlainObject(src) ? src : {}; }
                                target[name] = jQuery.extend(deep, clone, copy);
                            } else if (copy !== undefined) { target[name] = copy; }
                        }
                    }
                }
                return target;
            }; jQuery.extend({
                noConflict: function (deep) {
                    if (window.$ === jQuery) { window.$ = _$; }
                    if (deep && window.jQuery === jQuery) { window.jQuery = _jQuery; }
                    return jQuery;
                }, isReady: false, readyWait: 1, holdReady: function (hold) { if (hold) { jQuery.readyWait++; } else { jQuery.ready(true); } }, ready: function (wait) {
                    if ((wait === true && !--jQuery.readyWait) || (wait !== true && !jQuery.isReady)) {
                        if (!document.body) { return setTimeout(jQuery.ready, 1); }
                        jQuery.isReady = true; if (wait !== true && --jQuery.readyWait > 0) { return; }
                        readyList.fireWith(document, [jQuery]); if (jQuery.fn.trigger) { jQuery(document).trigger("ready").off("ready"); }
                    }
                }, bindReady: function () {
                    if (readyList) { return; }
                    readyList = jQuery.Callbacks("once memory"); if (document.readyState === "complete") { return setTimeout(jQuery.ready, 1); }
                    if (document.addEventListener) { document.addEventListener("DOMContentLoaded", DOMContentLoaded, false); window.addEventListener("load", jQuery.ready, false); } else if (document.attachEvent) {
                        document.attachEvent("onreadystatechange", DOMContentLoaded); window.attachEvent("onload", jQuery.ready); var toplevel = false; try { toplevel = window.frameElement == null; } catch (e) { }
                        if (document.documentElement.doScroll && toplevel) { doScrollCheck(); }
                    }
                }, isFunction: function (obj) { return jQuery.type(obj) === "function"; }, isArray: Array.isArray || function (obj) { return jQuery.type(obj) === "array"; }, isWindow: function (obj) { return obj && typeof obj === "object" && "setInterval" in obj; }, isNumeric: function (obj) { return !isNaN(parseFloat(obj)) && isFinite(obj); }, type: function (obj) { return obj == null ? String(obj) : class2type[toString.call(obj)] || "object"; }, isPlainObject: function (obj) {
                    if (!obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow(obj)) { return false; }
                    try { if (obj.constructor && !hasOwn.call(obj, "constructor") && !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) { return false; } } catch (e) { return false; }
                    var key; for (key in obj) { }
                    return key === undefined || hasOwn.call(obj, key);
                }, isEmptyObject: function (obj) {
                    for (var name in obj) { return false; }
                    return true;
                }, error: function (msg) { throw new Error(msg); }, parseJSON: function (data) {
                    if (typeof data !== "string" || !data) { return null; }
                    data = jQuery.trim(data); if (window.JSON && window.JSON.parse) { return window.JSON.parse(data); }
                    if (rvalidchars.test(data.replace(rvalidescape, "@").replace(rvalidtokens, "]").replace(rvalidbraces, ""))) { return (new Function("return " + data))(); }
                    jQuery.error("Invalid JSON: " + data);
                }, parseXML: function (data) {
                    var xml, tmp; try { if (window.DOMParser) { tmp = new DOMParser(); xml = tmp.parseFromString(data, "text/xml"); } else { xml = new ActiveXObject("Microsoft.XMLDOM"); xml.async = "false"; xml.loadXML(data); } } catch (e) { xml = undefined; }
                    if (!xml || !xml.documentElement || xml.getElementsByTagName("parsererror").length) { jQuery.error("Invalid XML: " + data); }
                    return xml;
                }, noop: function () { }, globalEval: function (data) { if (data && rnotwhite.test(data)) { (window.execScript || function (data) { window["eval"].call(window, data); })(data); } }, camelCase: function (string) { return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase); }, nodeName: function (elem, name) { return elem.nodeName && elem.nodeName.toUpperCase() === name.toUpperCase(); }, each: function (object, callback, args) {
                    var name, i = 0, length = object.length, isObj = length === undefined || jQuery.isFunction(object); if (args) {
                        if (isObj) { for (name in object) { if (callback.apply(object[name], args) === false) { break; } } } else { for (; i < length;) { if (callback.apply(object[i++], args) === false) { break; } } }
                    } else { if (isObj) { for (name in object) { if (callback.call(object[name], name, object[name]) === false) { break; } } } else { for (; i < length;) { if (callback.call(object[i], i, object[i++]) === false) { break; } } } }
                    return object;
                }, trim: trim ? function (text) { return text == null ? "" : trim.call(text); } : function (text) { return text == null ? "" : text.toString().replace(trimLeft, "").replace(trimRight, ""); }, makeArray: function (array, results) {
                    var ret = results || []; if (array != null) { var type = jQuery.type(array); if (array.length == null || type === "string" || type === "function" || type === "regexp" || jQuery.isWindow(array)) { push.call(ret, array); } else { jQuery.merge(ret, array); } }
                    return ret;
                }, inArray: function (elem, array, i) {
                    var len; if (array) {
                        if (indexOf) { return indexOf.call(array, elem, i); }
                        len = array.length; i = i ? i < 0 ? Math.max(0, len + i) : i : 0; for (; i < len; i++) { if (i in array && array[i] === elem) { return i; } }
                    }
                    return -1;
                }, merge: function (first, second) {
                    var i = first.length, j = 0; if (typeof second.length === "number") { for (var l = second.length; j < l; j++) { first[i++] = second[j]; } } else { while (second[j] !== undefined) { first[i++] = second[j++]; } }
                    first.length = i; return first;
                }, grep: function (elems, callback, inv) {
                    var ret = [], retVal; inv = !!inv; for (var i = 0, length = elems.length; i < length; i++) { retVal = !!callback(elems[i], i); if (inv !== retVal) { ret.push(elems[i]); } }
                    return ret;
                }, map: function (elems, callback, arg) {
                    var value, key, ret = [], i = 0, length = elems.length, isArray = elems instanceof jQuery || length !== undefined && typeof length === "number" && ((length > 0 && elems[0] && elems[length - 1]) || length === 0 || jQuery.isArray(elems)); if (isArray) {
                        for (; i < length; i++) { value = callback(elems[i], i, arg); if (value != null) { ret[ret.length] = value; } }
                    } else { for (key in elems) { value = callback(elems[key], key, arg); if (value != null) { ret[ret.length] = value; } } }
                    return ret.concat.apply([], ret);
                }, guid: 1, proxy: function (fn, context) {
                    if (typeof context === "string") { var tmp = fn[context]; context = fn; fn = tmp; }
                    if (!jQuery.isFunction(fn)) { return undefined; }
                    var args = slice.call(arguments, 2), proxy = function () { return fn.apply(context, args.concat(slice.call(arguments))); }; proxy.guid = fn.guid = fn.guid || proxy.guid || jQuery.guid++; return proxy;
                }, access: function (elems, key, value, exec, fn, pass) {
                    var length = elems.length; if (typeof key === "object") {
                        for (var k in key) { jQuery.access(elems, k, key[k], exec, fn, value); }
                        return elems;
                    }
                    if (value !== undefined) {
                        exec = !pass && exec && jQuery.isFunction(value); for (var i = 0; i < length; i++) { fn(elems[i], key, exec ? value.call(elems[i], i, fn(elems[i], key)) : value, pass); }
                        return elems;
                    }
                    return length ? fn(elems[0], key) : undefined;
                }, now: function () { return (new Date()).getTime(); }, uaMatch: function (ua) { ua = ua.toLowerCase(); var match = rwebkit.exec(ua) || ropera.exec(ua) || rmsie.exec(ua) || ua.indexOf("compatible") < 0 && rmozilla.exec(ua) || []; return { browser: match[1] || "", version: match[2] || "0" }; }, sub: function () {
                    function jQuerySub(selector, context) { return new jQuerySub.fn.init(selector, context); }
                    jQuery.extend(true, jQuerySub, this); jQuerySub.superclass = this; jQuerySub.fn = jQuerySub.prototype = this(); jQuerySub.fn.constructor = jQuerySub; jQuerySub.sub = this.sub; jQuerySub.fn.init = function init(selector, context) {
                        if (context && context instanceof jQuery && !(context instanceof jQuerySub)) { context = jQuerySub(context); }
                        return jQuery.fn.init.call(this, selector, context, rootjQuerySub);
                    }; jQuerySub.fn.init.prototype = jQuerySub.fn; var rootjQuerySub = jQuerySub(document); return jQuerySub;
                }, browser: {}
            }); jQuery.each("Boolean Number String Function Array Date RegExp Object".split(" "), function (i, name) { class2type["[object " + name + "]"] = name.toLowerCase(); }); browserMatch = jQuery.uaMatch(userAgent); if (browserMatch.browser) { jQuery.browser[browserMatch.browser] = true; jQuery.browser.version = browserMatch.version; }
            if (jQuery.browser.webkit) { jQuery.browser.safari = true; }
            if (rnotwhite.test("\xA0")) { trimLeft = /^[\s\xA0]+/; trimRight = /[\s\xA0]+$/; }
            rootjQuery = jQuery(document); if (document.addEventListener) { DOMContentLoaded = function () { document.removeEventListener("DOMContentLoaded", DOMContentLoaded, false); jQuery.ready(); }; } else if (document.attachEvent) { DOMContentLoaded = function () { if (document.readyState === "complete") { document.detachEvent("onreadystatechange", DOMContentLoaded); jQuery.ready(); } }; }
            function doScrollCheck() {
                if (jQuery.isReady) { return; }
                try { document.documentElement.doScroll("left"); } catch (e) { setTimeout(doScrollCheck, 1); return; }
                jQuery.ready();
            }
            return jQuery;
        })(); var flagsCache = {}; function createFlags(flags) {
            var object = flagsCache[flags] = {}, i, length; flags = flags.split(/\s+/); for (i = 0, length = flags.length; i < length; i++) { object[flags[i]] = true; }
            return object;
        }
        jQuery.Callbacks = function (flags) {
            flags = flags ? (flagsCache[flags] || createFlags(flags)) : {}; var list = [], stack = [], memory, firing, firingStart, firingLength, firingIndex, add = function (args) { var i, length, elem, type, actual; for (i = 0, length = args.length; i < length; i++) { elem = args[i]; type = jQuery.type(elem); if (type === "array") { add(elem); } else if (type === "function") { if (!flags.unique || !self.has(elem)) { list.push(elem); } } } }, fire = function (context, args) {
                args = args || []; memory = !flags.memory || [context, args]; firing = true; firingIndex = firingStart || 0; firingStart = 0; firingLength = list.length; for (; list && firingIndex < firingLength; firingIndex++) { if (list[firingIndex].apply(context, args) === false && flags.stopOnFalse) { memory = true; break; } }
                firing = false; if (list) { if (!flags.once) { if (stack && stack.length) { memory = stack.shift(); self.fireWith(memory[0], memory[1]); } } else if (memory === true) { self.disable(); } else { list = []; } }
            }, self = {
                add: function () {
                    if (list) { var length = list.length; add(arguments); if (firing) { firingLength = list.length; } else if (memory && memory !== true) { firingStart = length; fire(memory[0], memory[1]); } }
                    return this;
                }, remove: function () {
                    if (list) {
                        var args = arguments, argIndex = 0, argLength = args.length; for (; argIndex < argLength; argIndex++) {
                            for (var i = 0; i < list.length; i++) {
                                if (args[argIndex] === list[i]) {
                                    if (firing) { if (i <= firingLength) { firingLength--; if (i <= firingIndex) { firingIndex--; } } }
                                    list.splice(i--, 1); if (flags.unique) { break; }
                                }
                            }
                        }
                    }
                    return this;
                }, has: function (fn) {
                    if (list) { var i = 0, length = list.length; for (; i < length; i++) { if (fn === list[i]) { return true; } } }
                    return false;
                }, empty: function () { list = []; return this; }, disable: function () { list = stack = memory = undefined; return this; }, disabled: function () { return !list; }, lock: function () {
                    stack = undefined; if (!memory || memory === true) { self.disable(); }
                    return this;
                }, locked: function () { return !stack; }, fireWith: function (context, args) {
                    if (stack) { if (firing) { if (!flags.once) { stack.push([context, args]); } } else if (!(flags.once && memory)) { fire(context, args); } }
                    return this;
                }, fire: function () { self.fireWith(this, arguments); return this; }, fired: function () { return !!memory; }
            }; return self;
        }; var sliceDeferred = [].slice; jQuery.extend({
            Deferred: function (func) {
                var doneList = jQuery.Callbacks("once memory"), failList = jQuery.Callbacks("once memory"), progressList = jQuery.Callbacks("memory"), state = "pending", lists = { resolve: doneList, reject: failList, notify: progressList }, promise = {
                    done: doneList.add, fail: failList.add, progress: progressList.add, state: function () { return state; }, isResolved: doneList.fired, isRejected: failList.fired, then: function (doneCallbacks, failCallbacks, progressCallbacks) { deferred.done(doneCallbacks).fail(failCallbacks).progress(progressCallbacks); return this; }, always: function () { deferred.done.apply(deferred, arguments).fail.apply(deferred, arguments); return this; }, pipe: function (fnDone, fnFail, fnProgress) { return jQuery.Deferred(function (newDefer) { jQuery.each({ done: [fnDone, "resolve"], fail: [fnFail, "reject"], progress: [fnProgress, "notify"] }, function (handler, data) { var fn = data[0], action = data[1], returned; if (jQuery.isFunction(fn)) { deferred[handler](function () { returned = fn.apply(this, arguments); if (returned && jQuery.isFunction(returned.promise)) { returned.promise().then(newDefer.resolve, newDefer.reject, newDefer.notify); } else { newDefer[action + "With"](this === deferred ? newDefer : this, [returned]); } }); } else { deferred[handler](newDefer[action]); } }); }).promise(); }, promise: function (obj) {
                        if (obj == null) { obj = promise; } else { for (var key in promise) { obj[key] = promise[key]; } }
                        return obj;
                    }
                }, deferred = promise.promise({}), key; for (key in lists) { deferred[key] = lists[key].fire; deferred[key + "With"] = lists[key].fireWith; }
                deferred.done(function () { state = "resolved"; }, failList.disable, progressList.lock).fail(function () { state = "rejected"; }, doneList.disable, progressList.lock); if (func) { func.call(deferred, deferred); }
                return deferred;
            }, when: function (firstParam) {
                var args = sliceDeferred.call(arguments, 0), i = 0, length = args.length, pValues = new Array(length), count = length, pCount = length, deferred = length <= 1 && firstParam && jQuery.isFunction(firstParam.promise) ? firstParam : jQuery.Deferred(), promise = deferred.promise(); function resolveFunc(i) { return function (value) { args[i] = arguments.length > 1 ? sliceDeferred.call(arguments, 0) : value; if (!(--count)) { deferred.resolveWith(deferred, args); } }; }
                function progressFunc(i) { return function (value) { pValues[i] = arguments.length > 1 ? sliceDeferred.call(arguments, 0) : value; deferred.notifyWith(promise, pValues); }; }
                if (length > 1) {
                    for (; i < length; i++) { if (args[i] && args[i].promise && jQuery.isFunction(args[i].promise)) { args[i].promise().then(resolveFunc(i), deferred.reject, progressFunc(i)); } else { --count; } }
                    if (!count) { deferred.resolveWith(deferred, args); }
                } else if (deferred !== firstParam) { deferred.resolveWith(deferred, length ? [firstParam] : []); }
                return promise;
            }
        }); jQuery.support = (function () {
            var support, all, a, select, opt, input, marginDiv, fragment, tds, events, eventName, i, isSupported, div = document.createElement("div"), documentElement = document.documentElement; div.setAttribute("className", "t"); div.innerHTML = "   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>"; all = div.getElementsByTagName("*"); a = div.getElementsByTagName("a")[0]; if (!all || !all.length || !a) { return {}; }
            select = document.createElement("select"); opt = select.appendChild(document.createElement("option")); input = div.getElementsByTagName("input")[0]; support = {
                leadingWhitespace: (div.firstChild.nodeType === 3), tbody: !div.getElementsByTagName("tbody").length, htmlSerialize: !!div.getElementsByTagName("link").length, style: /top/.test(a.getAttribute("style")), hrefNormalized: (a.getAttribute("href") === "/a"),
                opacity: /^0.55/.test(a.style.opacity), cssFloat: !!a.style.cssFloat, checkOn: (input.value === "on"), optSelected: opt.selected, getSetAttribute: div.className !== "t", enctype: !!document.createElement("form").enctype, html5Clone: document.createElement("nav").cloneNode(true).outerHTML !== "<:nav></:nav>", submitBubbles: true, changeBubbles: true, focusinBubbles: false, deleteExpando: true, noCloneEvent: true, inlineBlockNeedsLayout: false, shrinkWrapBlocks: false, reliableMarginRight: true
            }; input.checked = true; support.noCloneChecked = input.cloneNode(true).checked; select.disabled = true; support.optDisabled = !opt.disabled; try { delete div.test; } catch (e) { support.deleteExpando = false; }
            if (!div.addEventListener && div.attachEvent && div.fireEvent) { div.attachEvent("onclick", function () { support.noCloneEvent = false; }); div.cloneNode(true).fireEvent("onclick"); }
            input = document.createElement("input"); input.value = "t"; input.setAttribute("type", "radio"); support.radioValue = input.value === "t"; input.setAttribute("checked", "checked"); div.appendChild(input); fragment = document.createDocumentFragment(); fragment.appendChild(div.lastChild); support.checkClone = fragment.cloneNode(true).cloneNode(true).lastChild.checked; support.appendChecked = input.checked; fragment.removeChild(input); fragment.appendChild(div); div.innerHTML = ""; if (window.getComputedStyle) { marginDiv = document.createElement("div"); marginDiv.style.width = "0"; marginDiv.style.marginRight = "0"; div.style.width = "2px"; div.appendChild(marginDiv); support.reliableMarginRight = (parseInt((window.getComputedStyle(marginDiv, null) || { marginRight: 0 }).marginRight, 10) || 0) === 0; }
            if (div.attachEvent) {
                for (i in { submit: 1, change: 1, focusin: 1 }) {
                    eventName = "on" + i; isSupported = (eventName in div); if (!isSupported) { div.setAttribute(eventName, "return;"); isSupported = (typeof div[eventName] === "function"); }
                    support[i + "Bubbles"] = isSupported;
                }
            }
            fragment.removeChild(div); fragment = select = opt = marginDiv = div = input = null; jQuery(function () {
                var container, outer, inner, table, td, offsetSupport, conMarginTop, ptlm, vb, style, html, body = document.getElementsByTagName("body")[0]; if (!body) { return; }
                conMarginTop = 1; ptlm = "position:absolute;top:0;left:0;width:1px;height:1px;margin:0;"; vb = "visibility:hidden;border:0;"; style = "style='" + ptlm + "border:5px solid #000;padding:0;'"; html = "<div " + style + "><div></div></div>" + "<table " + style + " cellpadding='0' cellspacing='0'>" + "<tr><td></td></tr></table>"; container = document.createElement("div"); container.style.cssText = vb + "width:0;height:0;position:static;top:0;margin-top:" + conMarginTop + "px"; body.insertBefore(container, body.firstChild); div = document.createElement("div"); container.appendChild(div); div.innerHTML = "<table><tr><td style='padding:0;border:0;display:none'></td><td>t</td></tr></table>"; tds = div.getElementsByTagName("td"); isSupported = (tds[0].offsetHeight === 0); tds[0].style.display = ""; tds[1].style.display = "none"; support.reliableHiddenOffsets = isSupported && (tds[0].offsetHeight === 0); div.innerHTML = ""; div.style.width = div.style.paddingLeft = "1px"; jQuery.boxModel = support.boxModel = div.offsetWidth === 2; if (typeof div.style.zoom !== "undefined") { div.style.display = "inline"; div.style.zoom = 1; support.inlineBlockNeedsLayout = (div.offsetWidth === 2); div.style.display = ""; div.innerHTML = "<div style='width:4px;'></div>"; support.shrinkWrapBlocks = (div.offsetWidth !== 2); }
                div.style.cssText = ptlm + vb; div.innerHTML = html; outer = div.firstChild; inner = outer.firstChild; td = outer.nextSibling.firstChild.firstChild; offsetSupport = { doesNotAddBorder: (inner.offsetTop !== 5), doesAddBorderForTableAndCells: (td.offsetTop === 5) }; inner.style.position = "fixed"; inner.style.top = "20px"; offsetSupport.fixedPosition = (inner.offsetTop === 20 || inner.offsetTop === 15); inner.style.position = inner.style.top = ""; outer.style.overflow = "hidden"; outer.style.position = "relative"; offsetSupport.subtractsBorderForOverflowNotVisible = (inner.offsetTop === -5); offsetSupport.doesNotIncludeMarginInBodyOffset = (body.offsetTop !== conMarginTop); body.removeChild(container); div = container = null; jQuery.extend(support, offsetSupport);
            }); return support;
        })(); var rbrace = /^(?:\{.*\}|\[.*\])$/, rmultiDash = /([A-Z])/g; jQuery.extend({
            cache: {}, uuid: 0, expando: "jQuery" + (jQuery.fn.jquery + Math.random()).replace(/\D/g, ""), noData: { "embed": true, "object": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000", "applet": true }, hasData: function (elem) { elem = elem.nodeType ? jQuery.cache[elem[jQuery.expando]] : elem[jQuery.expando]; return !!elem && !isEmptyDataObject(elem); }, data: function (elem, name, data, pvt) {
                if (!jQuery.acceptData(elem)) { return; }
                var privateCache, thisCache, ret, internalKey = jQuery.expando, getByName = typeof name === "string", isNode = elem.nodeType, cache = isNode ? jQuery.cache : elem, id = isNode ? elem[internalKey] : elem[internalKey] && internalKey, isEvents = name === "events"; if ((!id || !cache[id] || (!isEvents && !pvt && !cache[id].data)) && getByName && data === undefined) { return; }
                if (!id) { if (isNode) { elem[internalKey] = id = ++jQuery.uuid; } else { id = internalKey; } }
                if (!cache[id]) { cache[id] = {}; if (!isNode) { cache[id].toJSON = jQuery.noop; } }
                if (typeof name === "object" || typeof name === "function") { if (pvt) { cache[id] = jQuery.extend(cache[id], name); } else { cache[id].data = jQuery.extend(cache[id].data, name); } }
                privateCache = thisCache = cache[id]; if (!pvt) {
                    if (!thisCache.data) { thisCache.data = {}; }
                    thisCache = thisCache.data;
                }
                if (data !== undefined) { thisCache[jQuery.camelCase(name)] = data; }
                if (isEvents && !thisCache[name]) { return privateCache.events; }
                if (getByName) { ret = thisCache[name]; if (ret == null) { ret = thisCache[jQuery.camelCase(name)]; } } else { ret = thisCache; }
                return ret;
            }, removeData: function (elem, name, pvt) {
                if (!jQuery.acceptData(elem)) { return; }
                var thisCache, i, l, internalKey = jQuery.expando, isNode = elem.nodeType, cache = isNode ? jQuery.cache : elem, id = isNode ? elem[internalKey] : internalKey; if (!cache[id]) { return; }
                if (name) {
                    thisCache = pvt ? cache[id] : cache[id].data; if (thisCache) {
                        if (!jQuery.isArray(name)) { if (name in thisCache) { name = [name]; } else { name = jQuery.camelCase(name); if (name in thisCache) { name = [name]; } else { name = name.split(" "); } } }
                        for (i = 0, l = name.length; i < l; i++) { delete thisCache[name[i]]; }
                        if (!(pvt ? isEmptyDataObject : jQuery.isEmptyObject)(thisCache)) { return; }
                    }
                }
                if (!pvt) { delete cache[id].data; if (!isEmptyDataObject(cache[id])) { return; } }
                if (jQuery.support.deleteExpando || !cache.setInterval) { delete cache[id]; } else { cache[id] = null; }
                if (isNode) { if (jQuery.support.deleteExpando) { delete elem[internalKey]; } else if (elem.removeAttribute) { elem.removeAttribute(internalKey); } else { elem[internalKey] = null; } }
            }, _data: function (elem, name, data) { return jQuery.data(elem, name, data, true); }, acceptData: function (elem) {
                if (elem.nodeName) { var match = jQuery.noData[elem.nodeName.toLowerCase()]; if (match) { return !(match === true || elem.getAttribute("classid") !== match); } }
                return true;
            }
        }); jQuery.fn.extend({
            data: function (key, value) {
                var parts, attr, name, data = null; if (typeof key === "undefined") {
                    if (this.length) {
                        data = jQuery.data(this[0]); if (this[0].nodeType === 1 && !jQuery._data(this[0], "parsedAttrs")) {
                            attr = this[0].attributes; for (var i = 0, l = attr.length; i < l; i++) { name = attr[i].name; if (name.indexOf("data-") === 0) { name = jQuery.camelCase(name.substring(5)); dataAttr(this[0], name, data[name]); } }
                            jQuery._data(this[0], "parsedAttrs", true);
                        }
                    }
                    return data;
                } else if (typeof key === "object") { return this.each(function () { jQuery.data(this, key); }); }
                parts = key.split("."); parts[1] = parts[1] ? "." + parts[1] : ""; if (value === undefined) {
                    data = this.triggerHandler("getData" + parts[1] + "!", [parts[0]]); if (data === undefined && this.length) { data = jQuery.data(this[0], key); data = dataAttr(this[0], key, data); }
                    return data === undefined && parts[1] ? this.data(parts[0]) : data;
                } else { return this.each(function () { var self = jQuery(this), args = [parts[0], value]; self.triggerHandler("setData" + parts[1] + "!", args); jQuery.data(this, key, value); self.triggerHandler("changeData" + parts[1] + "!", args); }); }
            }, removeData: function (key) { return this.each(function () { jQuery.removeData(this, key); }); }
        }); function dataAttr(elem, key, data) {
            if (data === undefined && elem.nodeType === 1) {
                var name = "data-" + key.replace(rmultiDash, "-$1").toLowerCase(); data = elem.getAttribute(name); if (typeof data === "string") {
                    try { data = data === "true" ? true : data === "false" ? false : data === "null" ? null : jQuery.isNumeric(data) ? parseFloat(data) : rbrace.test(data) ? jQuery.parseJSON(data) : data; } catch (e) { }
                    jQuery.data(elem, key, data);
                } else { data = undefined; }
            }
            return data;
        }
        function isEmptyDataObject(obj) {
            for (var name in obj) {
                if (name === "data" && jQuery.isEmptyObject(obj[name])) { continue; }
                if (name !== "toJSON") { return false; }
            }
            return true;
        }
        function handleQueueMarkDefer(elem, type, src) { var deferDataKey = type + "defer", queueDataKey = type + "queue", markDataKey = type + "mark", defer = jQuery._data(elem, deferDataKey); if (defer && (src === "queue" || !jQuery._data(elem, queueDataKey)) && (src === "mark" || !jQuery._data(elem, markDataKey))) { setTimeout(function () { if (!jQuery._data(elem, queueDataKey) && !jQuery._data(elem, markDataKey)) { jQuery.removeData(elem, deferDataKey, true); defer.fire(); } }, 0); } }
        jQuery.extend({
            _mark: function (elem, type) { if (elem) { type = (type || "fx") + "mark"; jQuery._data(elem, type, (jQuery._data(elem, type) || 0) + 1); } }, _unmark: function (force, elem, type) {
                if (force !== true) { type = elem; elem = force; force = false; }
                if (elem) { type = type || "fx"; var key = type + "mark", count = force ? 0 : ((jQuery._data(elem, key) || 1) - 1); if (count) { jQuery._data(elem, key, count); } else { jQuery.removeData(elem, key, true); handleQueueMarkDefer(elem, type, "mark"); } }
            }, queue: function (elem, type, data) {
                var q; if (elem) {
                    type = (type || "fx") + "queue"; q = jQuery._data(elem, type); if (data) { if (!q || jQuery.isArray(data)) { q = jQuery._data(elem, type, jQuery.makeArray(data)); } else { q.push(data); } }
                    return q || [];
                }
            }, dequeue: function (elem, type) {
                type = type || "fx"; var queue = jQuery.queue(elem, type), fn = queue.shift(), hooks = {}; if (fn === "inprogress") { fn = queue.shift(); }
                if (fn) {
                    if (type === "fx") { queue.unshift("inprogress"); }
                    jQuery._data(elem, type + ".run", hooks); fn.call(elem, function () { jQuery.dequeue(elem, type); }, hooks);
                }
                if (!queue.length) { jQuery.removeData(elem, type + "queue " + type + ".run", true); handleQueueMarkDefer(elem, type, "queue"); }
            }
        }); jQuery.fn.extend({
            queue: function (type, data) {
                if (typeof type !== "string") { data = type; type = "fx"; }
                if (data === undefined) { return jQuery.queue(this[0], type); }
                return this.each(function () { var queue = jQuery.queue(this, type, data); if (type === "fx" && queue[0] !== "inprogress") { jQuery.dequeue(this, type); } });
            }, dequeue: function (type) { return this.each(function () { jQuery.dequeue(this, type); }); }, delay: function (time, type) { time = jQuery.fx ? jQuery.fx.speeds[time] || time : time; type = type || "fx"; return this.queue(type, function (next, hooks) { var timeout = setTimeout(next, time); hooks.stop = function () { clearTimeout(timeout); }; }); }, clearQueue: function (type) { return this.queue(type || "fx", []); }, promise: function (type, object) {
                if (typeof type !== "string") { object = type; type = undefined; }
                type = type || "fx"; var defer = jQuery.Deferred(), elements = this, i = elements.length, count = 1, deferDataKey = type + "defer", queueDataKey = type + "queue", markDataKey = type + "mark", tmp; function resolve() { if (!(--count)) { defer.resolveWith(elements, [elements]); } }
                while (i--) { if ((tmp = jQuery.data(elements[i], deferDataKey, undefined, true) || (jQuery.data(elements[i], queueDataKey, undefined, true) || jQuery.data(elements[i], markDataKey, undefined, true)) && jQuery.data(elements[i], deferDataKey, jQuery.Callbacks("once memory"), true))) { count++; tmp.add(resolve); } }
                resolve(); return defer.promise();
            }
        }); var rclass = /[\n\t\r]/g, rspace = /\s+/, rreturn = /\r/g, rtype = /^(?:button|input)$/i, rfocusable = /^(?:button|input|object|select|textarea)$/i, rclickable = /^a(?:rea)?$/i, rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i, getSetAttribute = jQuery.support.getSetAttribute, nodeHook, boolHook, fixSpecified; jQuery.fn.extend({
            attr: function (name, value) { return jQuery.access(this, name, value, true, jQuery.attr); }, removeAttr: function (name) { return this.each(function () { jQuery.removeAttr(this, name); }); }, prop: function (name, value) { return jQuery.access(this, name, value, true, jQuery.prop); }, removeProp: function (name) { name = jQuery.propFix[name] || name; return this.each(function () { try { this[name] = undefined; delete this[name]; } catch (e) { } }); }, addClass: function (value) {
                var classNames, i, l, elem, setClass, c, cl; if (jQuery.isFunction(value)) { return this.each(function (j) { jQuery(this).addClass(value.call(this, j, this.className)); }); }
                if (value && typeof value === "string") {
                    classNames = value.split(rspace); for (i = 0, l = this.length; i < l; i++) {
                        elem = this[i]; if (elem.nodeType === 1) {
                            if (!elem.className && classNames.length === 1) { elem.className = value; } else {
                                setClass = " " + elem.className + " "; for (c = 0, cl = classNames.length; c < cl; c++) { if (!~setClass.indexOf(" " + classNames[c] + " ")) { setClass += classNames[c] + " "; } }
                                elem.className = jQuery.trim(setClass);
                            }
                        }
                    }
                }
                return this;
            }, removeClass: function (value) {
                var classNames, i, l, elem, className, c, cl; if (jQuery.isFunction(value)) { return this.each(function (j) { jQuery(this).removeClass(value.call(this, j, this.className)); }); }
                if ((value && typeof value === "string") || value === undefined) {
                    classNames = (value || "").split(rspace); for (i = 0, l = this.length; i < l; i++) {
                        elem = this[i]; if (elem.nodeType === 1 && elem.className) {
                            if (value) {
                                className = (" " + elem.className + " ").replace(rclass, " "); for (c = 0, cl = classNames.length; c < cl; c++) { className = className.replace(" " + classNames[c] + " ", " "); }
                                elem.className = jQuery.trim(className);
                            } else { elem.className = ""; }
                        }
                    }
                }
                return this;
            }, toggleClass: function (value, stateVal) {
                var type = typeof value, isBool = typeof stateVal === "boolean"; if (jQuery.isFunction(value)) { return this.each(function (i) { jQuery(this).toggleClass(value.call(this, i, this.className, stateVal), stateVal); }); }
                return this.each(function () {
                    if (type === "string") { var className, i = 0, self = jQuery(this), state = stateVal, classNames = value.split(rspace); while ((className = classNames[i++])) { state = isBool ? state : !self.hasClass(className); self[state ? "addClass" : "removeClass"](className); } } else if (type === "undefined" || type === "boolean") {
                        if (this.className) { jQuery._data(this, "__className__", this.className); }
                        this.className = this.className || value === false ? "" : jQuery._data(this, "__className__") || "";
                    }
                });
            }, hasClass: function (selector) {
                var className = " " + selector + " ", i = 0, l = this.length; for (; i < l; i++) { if (this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf(className) > -1) { return true; } }
                return false;
            }, val: function (value) {
                var hooks, ret, isFunction, elem = this[0]; if (!arguments.length) {
                    if (elem) {
                        hooks = jQuery.valHooks[elem.nodeName.toLowerCase()] || jQuery.valHooks[elem.type]; if (hooks && "get" in hooks && (ret = hooks.get(elem, "value")) !== undefined) { return ret; }
                        ret = elem.value; return typeof ret === "string" ? ret.replace(rreturn, "") : ret == null ? "" : ret;
                    }
                    return;
                }
                isFunction = jQuery.isFunction(value); return this.each(function (i) {
                    var self = jQuery(this), val; if (this.nodeType !== 1) { return; }
                    if (isFunction) { val = value.call(this, i, self.val()); } else { val = value; }
                    if (val == null) { val = ""; } else if (typeof val === "number") { val += ""; } else if (jQuery.isArray(val)) { val = jQuery.map(val, function (value) { return value == null ? "" : value + ""; }); }
                    hooks = jQuery.valHooks[this.nodeName.toLowerCase()] || jQuery.valHooks[this.type]; if (!hooks || !("set" in hooks) || hooks.set(this, val, "value") === undefined) { this.value = val; }
                });
            }
        }); jQuery.extend({
            valHooks: {
                option: { get: function (elem) { var val = elem.attributes.value; return !val || val.specified ? elem.value : elem.text; } }, select: {
                    get: function (elem) {
                        var value, i, max, option, index = elem.selectedIndex, values = [], options = elem.options, one = elem.type === "select-one"; if (index < 0) { return null; }
                        i = one ? index : 0; max = one ? index + 1 : options.length; for (; i < max; i++) {
                            option = options[i]; if (option.selected && (jQuery.support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null) && (!option.parentNode.disabled || !jQuery.nodeName(option.parentNode, "optgroup"))) {
                                value = jQuery(option).val(); if (one) { return value; }
                                values.push(value);
                            }
                        }
                        if (one && !values.length && options.length) { return jQuery(options[index]).val(); }
                        return values;
                    }, set: function (elem, value) {
                        var values = jQuery.makeArray(value); jQuery(elem).find("option").each(function () { this.selected = jQuery.inArray(jQuery(this).val(), values) >= 0; }); if (!values.length) { elem.selectedIndex = -1; }
                        return values;
                    }
                }
            }, attrFn: { val: true, css: true, html: true, text: true, data: true, width: true, height: true, offset: true }, attr: function (elem, name, value, pass) {
                var ret, hooks, notxml, nType = elem.nodeType; if (!elem || nType === 3 || nType === 8 || nType === 2) { return; }
                if (pass && name in jQuery.attrFn) { return jQuery(elem)[name](value); }
                if (typeof elem.getAttribute === "undefined") { return jQuery.prop(elem, name, value); }
                notxml = nType !== 1 || !jQuery.isXMLDoc(elem); if (notxml) { name = name.toLowerCase(); hooks = jQuery.attrHooks[name] || (rboolean.test(name) ? boolHook : nodeHook); }
                if (value !== undefined) { if (value === null) { jQuery.removeAttr(elem, name); return; } else if (hooks && "set" in hooks && notxml && (ret = hooks.set(elem, value, name)) !== undefined) { return ret; } else { elem.setAttribute(name, "" + value); return value; } } else if (hooks && "get" in hooks && notxml && (ret = hooks.get(elem, name)) !== null) { return ret; } else { ret = elem.getAttribute(name); return ret === null ? undefined : ret; }
            }, removeAttr: function (elem, value) { var propName, attrNames, name, l, i = 0; if (value && elem.nodeType === 1) { attrNames = value.toLowerCase().split(rspace); l = attrNames.length; for (; i < l; i++) { name = attrNames[i]; if (name) { propName = jQuery.propFix[name] || name; jQuery.attr(elem, name, ""); elem.removeAttribute(getSetAttribute ? name : propName); if (rboolean.test(name) && propName in elem) { elem[propName] = false; } } } } }, attrHooks: {
                type: {
                    set: function (elem, value) {
                        if (rtype.test(elem.nodeName) && elem.parentNode) { jQuery.error("type property can't be changed"); } else if (!jQuery.support.radioValue && value === "radio" && jQuery.nodeName(elem, "input")) {
                            var val = elem.value; elem.setAttribute("type", value); if (val) { elem.value = val; }
                            return value;
                        }
                    }
                }, value: {
                    get: function (elem, name) {
                        if (nodeHook && jQuery.nodeName(elem, "button")) { return nodeHook.get(elem, name); }
                        return name in elem ? elem.value : null;
                    }, set: function (elem, value, name) {
                        if (nodeHook && jQuery.nodeName(elem, "button")) { return nodeHook.set(elem, value, name); }
                        elem.value = value;
                    }
                }
            }, propFix: { tabindex: "tabIndex", readonly: "readOnly", "for": "htmlFor", "class": "className", maxlength: "maxLength", cellspacing: "cellSpacing", cellpadding: "cellPadding", rowspan: "rowSpan", colspan: "colSpan", usemap: "useMap", frameborder: "frameBorder", contenteditable: "contentEditable" }, prop: function (elem, name, value) {
                var ret, hooks, notxml, nType = elem.nodeType; if (!elem || nType === 3 || nType === 8 || nType === 2) { return; }
                notxml = nType !== 1 || !jQuery.isXMLDoc(elem); if (notxml) { name = jQuery.propFix[name] || name; hooks = jQuery.propHooks[name]; }
                if (value !== undefined) { if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) { return ret; } else { return (elem[name] = value); } } else { if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) { return ret; } else { return elem[name]; } }
            }, propHooks: { tabIndex: { get: function (elem) { var attributeNode = elem.getAttributeNode("tabindex"); return attributeNode && attributeNode.specified ? parseInt(attributeNode.value, 10) : rfocusable.test(elem.nodeName) || rclickable.test(elem.nodeName) && elem.href ? 0 : undefined; } } }
        }); jQuery.attrHooks.tabindex = jQuery.propHooks.tabIndex; boolHook = {
            get: function (elem, name) { var attrNode, property = jQuery.prop(elem, name); return property === true || typeof property !== "boolean" && (attrNode = elem.getAttributeNode(name)) && attrNode.nodeValue !== false ? name.toLowerCase() : undefined; }, set: function (elem, value, name) {
                var propName; if (value === false) { jQuery.removeAttr(elem, name); } else {
                    propName = jQuery.propFix[name] || name; if (propName in elem) { elem[propName] = true; }
                    elem.setAttribute(name, name.toLowerCase());
                }
                return name;
            }
        }; if (!getSetAttribute) {
            fixSpecified = { name: true, id: true }; nodeHook = jQuery.valHooks.button = {
                get: function (elem, name) { var ret; ret = elem.getAttributeNode(name); return ret && (fixSpecified[name] ? ret.nodeValue !== "" : ret.specified) ? ret.nodeValue : undefined; }, set: function (elem, value, name) {
                    var ret = elem.getAttributeNode(name); if (!ret) { ret = document.createAttribute(name); elem.setAttributeNode(ret); }
                    return (ret.nodeValue = value + "");
                }
            }; jQuery.attrHooks.tabindex.set = nodeHook.set;
            jQuery.each(["width", "height"], function (i, name) { jQuery.attrHooks[name] = jQuery.extend(jQuery.attrHooks[name], { set: function (elem, value) { if (value === "") { elem.setAttribute(name, "auto"); return value; } } }); });
            jQuery.attrHooks.contenteditable = {
                get: nodeHook.get, set: function (elem, value, name) {
                    if (value === "") { value = "false"; }
                    nodeHook.set(elem, value, name);
                }
            };
        }
        if (!jQuery.support.hrefNormalized) { jQuery.each(["href", "src", "width", "height"], function (i, name) { jQuery.attrHooks[name] = jQuery.extend(jQuery.attrHooks[name], { get: function (elem) { var ret = elem.getAttribute(name, 2); return ret === null ? undefined : ret; } }); }); }
        if (!jQuery.support.style) { jQuery.attrHooks.style = { get: function (elem) { return elem.style.cssText.toLowerCase() || undefined; }, set: function (elem, value) { return (elem.style.cssText = "" + value); } }; }
        if (!jQuery.support.optSelected) {
            jQuery.propHooks.selected = jQuery.extend(jQuery.propHooks.selected, {
                get: function (elem) {
                    var parent = elem.parentNode; if (parent) { parent.selectedIndex; if (parent.parentNode) { parent.parentNode.selectedIndex; } }
                    return null;
                }
            });
        }
        if (!jQuery.support.enctype) { jQuery.propFix.enctype = "encoding"; }
        if (!jQuery.support.checkOn) { jQuery.each(["radio", "checkbox"], function () { jQuery.valHooks[this] = { get: function (elem) { return elem.getAttribute("value") === null ? "on" : elem.value; } }; }); }
        jQuery.each(["radio", "checkbox"], function () { jQuery.valHooks[this] = jQuery.extend(jQuery.valHooks[this], { set: function (elem, value) { if (jQuery.isArray(value)) { return (elem.checked = jQuery.inArray(jQuery(elem).val(), value) >= 0); } } }); }); var rformElems = /^(?:textarea|input|select)$/i, rtypenamespace = /^([^\.]*)?(?:\.(.+))?$/, rhoverHack = /\bhover(\.\S+)?\b/, rkeyEvent = /^key/, rmouseEvent = /^(?:mouse|contextmenu)|click/, rfocusMorph = /^(?:focusinfocus|focusoutblur)$/, rquickIs = /^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/, quickParse = function (selector) {
            var quick = rquickIs.exec(selector); if (quick) { quick[1] = (quick[1] || "").toLowerCase(); quick[3] = quick[3] && new RegExp("(?:^|\\s)" + quick[3] + "(?:\\s|$)"); }
            return quick;
        }, quickIs = function (elem, m) { var attrs = elem.attributes || {}; return ((!m[1] || elem.nodeName.toLowerCase() === m[1]) && (!m[2] || (attrs.id || {}).value === m[2]) && (!m[3] || m[3].test((attrs["class"] || {}).value))); }, hoverHack = function (events) { return jQuery.event.special.hover ? events : events.replace(rhoverHack, "mouseenter$1 mouseleave$1"); }; jQuery.event = {
            add: function (elem, types, handler, data, selector) {
                var elemData, eventHandle, events, t, tns, type, namespaces, handleObj, handleObjIn, quick, handlers, special; if (elem.nodeType === 3 || elem.nodeType === 8 || !types || !handler || !(elemData = jQuery._data(elem))) { return; }
                if (handler.handler) { handleObjIn = handler; handler = handleObjIn.handler; }
                if (!handler.guid) { handler.guid = jQuery.guid++; }
                events = elemData.events; if (!events) { elemData.events = events = {}; }
                eventHandle = elemData.handle; if (!eventHandle) { elemData.handle = eventHandle = function (e) { return typeof jQuery !== "undefined" && (!e || jQuery.event.triggered !== e.type) ? jQuery.event.dispatch.apply(eventHandle.elem, arguments) : undefined; }; eventHandle.elem = elem; }
                types = jQuery.trim(hoverHack(types)).split(" "); for (t = 0; t < types.length; t++) {
                    tns = rtypenamespace.exec(types[t]) || []; type = tns[1]; namespaces = (tns[2] || "").split(".").sort(); special = jQuery.event.special[type] || {}; type = (selector ? special.delegateType : special.bindType) || type; special = jQuery.event.special[type] || {}; handleObj = jQuery.extend({ type: type, origType: tns[1], data: data, handler: handler, guid: handler.guid, selector: selector, quick: quickParse(selector), namespace: namespaces.join(".") }, handleObjIn); handlers = events[type]; if (!handlers) { handlers = events[type] = []; handlers.delegateCount = 0; if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) { if (elem.addEventListener) { elem.addEventListener(type, eventHandle, false); } else if (elem.attachEvent) { elem.attachEvent("on" + type, eventHandle); } } }
                    if (special.add) { special.add.call(elem, handleObj); if (!handleObj.handler.guid) { handleObj.handler.guid = handler.guid; } }
                    if (selector) { handlers.splice(handlers.delegateCount++, 0, handleObj); } else { handlers.push(handleObj); }
                    jQuery.event.global[type] = true;
                }
                elem = null;
            }, global: {}, remove: function (elem, types, handler, selector, mappedTypes) {
                var elemData = jQuery.hasData(elem) && jQuery._data(elem), t, tns, type, origType, namespaces, origCount, j, events, special, handle, eventType, handleObj; if (!elemData || !(events = elemData.events)) { return; }
                types = jQuery.trim(hoverHack(types || "")).split(" "); for (t = 0; t < types.length; t++) {
                    tns = rtypenamespace.exec(types[t]) || []; type = origType = tns[1]; namespaces = tns[2]; if (!type) {
                        for (type in events) { jQuery.event.remove(elem, type + types[t], handler, selector, true); }
                        continue;
                    }
                    special = jQuery.event.special[type] || {}; type = (selector ? special.delegateType : special.bindType) || type; eventType = events[type] || []; origCount = eventType.length; namespaces = namespaces ? new RegExp("(^|\\.)" + namespaces.split(".").sort().join("\\.(?:.*\\.)?") + "(\\.|$)") : null; for (j = 0; j < eventType.length; j++) {
                        handleObj = eventType[j]; if ((mappedTypes || origType === handleObj.origType) && (!handler || handler.guid === handleObj.guid) && (!namespaces || namespaces.test(handleObj.namespace)) && (!selector || selector === handleObj.selector || selector === "**" && handleObj.selector)) {
                            eventType.splice(j--, 1); if (handleObj.selector) { eventType.delegateCount--; }
                            if (special.remove) { special.remove.call(elem, handleObj); }
                        }
                    }
                    if (eventType.length === 0 && origCount !== eventType.length) {
                        if (!special.teardown || special.teardown.call(elem, namespaces) === false) { jQuery.removeEvent(elem, type, elemData.handle); }
                        delete events[type];
                    }
                }
                if (jQuery.isEmptyObject(events)) {
                    handle = elemData.handle; if (handle) { handle.elem = null; }
                    jQuery.removeData(elem, ["events", "handle"], true);
                }
            }, customEvent: { "getData": true, "setData": true, "changeData": true }, trigger: function (event, data, elem, onlyHandlers) {
                if (elem && (elem.nodeType === 3 || elem.nodeType === 8)) { return; }
                var type = event.type || event, namespaces = [], cache, exclusive, i, cur, old, ontype, special, handle, eventPath, bubbleType; if (rfocusMorph.test(type + jQuery.event.triggered)) { return; }
                if (type.indexOf("!") >= 0) { type = type.slice(0, -1); exclusive = true; }
                if (type.indexOf(".") >= 0) { namespaces = type.split("."); type = namespaces.shift(); namespaces.sort(); }
                if ((!elem || jQuery.event.customEvent[type]) && !jQuery.event.global[type]) { return; }
                event = typeof event === "object" ? event[jQuery.expando] ? event : new jQuery.Event(type, event) : new jQuery.Event(type); event.type = type; event.isTrigger = true; event.exclusive = exclusive; event.namespace = namespaces.join("."); event.namespace_re = event.namespace ? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.)?") + "(\\.|$)") : null; ontype = type.indexOf(":") < 0 ? "on" + type : ""; if (!elem) {
                    cache = jQuery.cache; for (i in cache) { if (cache[i].events && cache[i].events[type]) { jQuery.event.trigger(event, data, cache[i].handle.elem, true); } }
                    return;
                }
                event.result = undefined; if (!event.target) { event.target = elem; }
                data = data != null ? jQuery.makeArray(data) : []; data.unshift(event); special = jQuery.event.special[type] || {}; if (special.trigger && special.trigger.apply(elem, data) === false) { return; }
                eventPath = [[elem, special.bindType || type]]; if (!onlyHandlers && !special.noBubble && !jQuery.isWindow(elem)) {
                    bubbleType = special.delegateType || type; cur = rfocusMorph.test(bubbleType + type) ? elem : elem.parentNode; old = null; for (; cur; cur = cur.parentNode) { eventPath.push([cur, bubbleType]); old = cur; }
                    if (old && old === elem.ownerDocument) { eventPath.push([old.defaultView || old.parentWindow || window, bubbleType]); }
                }
                for (i = 0; i < eventPath.length && !event.isPropagationStopped() ; i++) {
                    cur = eventPath[i][0]; event.type = eventPath[i][1]; handle = (jQuery._data(cur, "events") || {})[event.type] && jQuery._data(cur, "handle"); if (handle) { handle.apply(cur, data); }
                    handle = ontype && cur[ontype]; if (handle && jQuery.acceptData(cur) && handle.apply(cur, data) === false) { event.preventDefault(); }
                }
                event.type = type; if (!onlyHandlers && !event.isDefaultPrevented()) {
                    if ((!special._default || special._default.apply(elem.ownerDocument, data) === false) && !(type === "click" && jQuery.nodeName(elem, "a")) && jQuery.acceptData(elem)) {
                        if (ontype && elem[type] && ((type !== "focus" && type !== "blur") || event.target.offsetWidth !== 0) && !jQuery.isWindow(elem)) {
                            old = elem[ontype]; if (old) { elem[ontype] = null; }
                            jQuery.event.triggered = type; elem[type](); jQuery.event.triggered = undefined; if (old) { elem[ontype] = old; }
                        }
                    }
                }
                return event.result;
            }, dispatch: function (event) {
                event = jQuery.event.fix(event || window.event); var handlers = ((jQuery._data(this, "events") || {})[event.type] || []), delegateCount = handlers.delegateCount, args = [].slice.call(arguments, 0), run_all = !event.exclusive && !event.namespace, handlerQueue = [], i, j, cur, jqcur, ret, selMatch, matched, matches, handleObj, sel, related; args[0] = event; event.delegateTarget = this; if (delegateCount && !event.target.disabled && !(event.button && event.type === "click")) {
                    jqcur = jQuery(this); jqcur.context = this.ownerDocument || this; for (cur = event.target; cur != this; cur = cur.parentNode || this) {
                        selMatch = {}; matches = []; jqcur[0] = cur; for (i = 0; i < delegateCount; i++) {
                            handleObj = handlers[i]; sel = handleObj.selector; if (selMatch[sel] === undefined) { selMatch[sel] = (handleObj.quick ? quickIs(cur, handleObj.quick) : jqcur.is(sel)); }
                            if (selMatch[sel]) { matches.push(handleObj); }
                        }
                        if (matches.length) { handlerQueue.push({ elem: cur, matches: matches }); }
                    }
                }
                if (handlers.length > delegateCount) { handlerQueue.push({ elem: this, matches: handlers.slice(delegateCount) }); }
                for (i = 0; i < handlerQueue.length && !event.isPropagationStopped() ; i++) { matched = handlerQueue[i]; event.currentTarget = matched.elem; for (j = 0; j < matched.matches.length && !event.isImmediatePropagationStopped() ; j++) { handleObj = matched.matches[j]; if (run_all || (!event.namespace && !handleObj.namespace) || event.namespace_re && event.namespace_re.test(handleObj.namespace)) { event.data = handleObj.data; event.handleObj = handleObj; ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args); if (ret !== undefined) { event.result = ret; if (ret === false) { event.preventDefault(); event.stopPropagation(); } } } } }
                return event.result;
            }, props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "), fixHooks: {}, keyHooks: {
                props: "char charCode key keyCode".split(" "), filter: function (event, original) {
                    if (event.which == null) { event.which = original.charCode != null ? original.charCode : original.keyCode; }
                    return event;
                }
            }, mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "), filter: function (event, original) {
                    var eventDoc, doc, body, button = original.button, fromElement = original.fromElement; if (event.pageX == null && original.clientX != null) { eventDoc = event.target.ownerDocument || document; doc = eventDoc.documentElement; body = eventDoc.body; event.pageX = original.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0); event.pageY = original.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0); }
                    if (!event.relatedTarget && fromElement) { event.relatedTarget = fromElement === event.target ? original.toElement : fromElement; }
                    if (!event.which && button !== undefined) { event.which = (button & 1 ? 1 : (button & 2 ? 3 : (button & 4 ? 2 : 0))); }
                    return event;
                }
            }, fix: function (event) {
                if (event[jQuery.expando]) { return event; }
                var i, prop, originalEvent = event, fixHook = jQuery.event.fixHooks[event.type] || {}, copy = fixHook.props ? this.props.concat(fixHook.props) : this.props; event = jQuery.Event(originalEvent); for (i = copy.length; i;) { prop = copy[--i]; event[prop] = originalEvent[prop]; }
                if (!event.target) { event.target = originalEvent.srcElement || document; }
                if (event.target.nodeType === 3) { event.target = event.target.parentNode; }
                if (event.metaKey === undefined) { event.metaKey = event.ctrlKey; }
                return fixHook.filter ? fixHook.filter(event, originalEvent) : event;
            }, special: { ready: { setup: jQuery.bindReady }, load: { noBubble: true }, focus: { delegateType: "focusin" }, blur: { delegateType: "focusout" }, beforeunload: { setup: function (data, namespaces, eventHandle) { if (jQuery.isWindow(this)) { this.onbeforeunload = eventHandle; } }, teardown: function (namespaces, eventHandle) { if (this.onbeforeunload === eventHandle) { this.onbeforeunload = null; } } } }, simulate: function (type, elem, event, bubble) {
                var e = jQuery.extend(new jQuery.Event(), event, { type: type, isSimulated: true, originalEvent: {} }); if (bubble) { jQuery.event.trigger(e, null, elem); } else { jQuery.event.dispatch.call(elem, e); }
                if (e.isDefaultPrevented()) { event.preventDefault(); }
            }
        }; jQuery.event.handle = jQuery.event.dispatch; jQuery.removeEvent = document.removeEventListener ? function (elem, type, handle) { if (elem.removeEventListener) { elem.removeEventListener(type, handle, false); } } : function (elem, type, handle) { if (elem.detachEvent) { elem.detachEvent("on" + type, handle); } }; jQuery.Event = function (src, props) {
            if (!(this instanceof jQuery.Event)) { return new jQuery.Event(src, props); }
            if (src && src.type) { this.originalEvent = src; this.type = src.type; this.isDefaultPrevented = (src.defaultPrevented || src.returnValue === false || src.getPreventDefault && src.getPreventDefault()) ? returnTrue : returnFalse; } else { this.type = src; }
            if (props) { jQuery.extend(this, props); }
            this.timeStamp = src && src.timeStamp || jQuery.now(); this[jQuery.expando] = true;
        }; function returnFalse() { return false; }
        function returnTrue() { return true; }
        jQuery.Event.prototype = {
            preventDefault: function () {
                this.isDefaultPrevented = returnTrue; var e = this.originalEvent; if (!e) { return; }
                if (e.preventDefault) { e.preventDefault(); } else { e.returnValue = false; }
            }, stopPropagation: function () {
                this.isPropagationStopped = returnTrue; var e = this.originalEvent; if (!e) { return; }
                if (e.stopPropagation) { e.stopPropagation(); }
                e.cancelBubble = true;
            }, stopImmediatePropagation: function () { this.isImmediatePropagationStopped = returnTrue; this.stopPropagation(); }, isDefaultPrevented: returnFalse, isPropagationStopped: returnFalse, isImmediatePropagationStopped: returnFalse
        }; jQuery.each({ mouseenter: "mouseover", mouseleave: "mouseout" }, function (orig, fix) {
            jQuery.event.special[orig] = {
                delegateType: fix, bindType: fix, handle: function (event) {
                    var target = this, related = event.relatedTarget, handleObj = event.handleObj, selector = handleObj.selector, ret; if (!related || (related !== target && !jQuery.contains(target, related))) { event.type = handleObj.origType; ret = handleObj.handler.apply(this, arguments); event.type = fix; }
                    return ret;
                }
            };
        }); if (!jQuery.support.submitBubbles) {
            jQuery.event.special.submit = {
                setup: function () {
                    if (jQuery.nodeName(this, "form")) { return false; }
                    jQuery.event.add(this, "click._submit keypress._submit", function (e) { var elem = e.target, form = jQuery.nodeName(elem, "input") || jQuery.nodeName(elem, "button") ? elem.form : undefined; if (form && !form._submit_attached) { jQuery.event.add(form, "submit._submit", function (event) { if (this.parentNode && !event.isTrigger) { jQuery.event.simulate("submit", this.parentNode, event, true); } }); form._submit_attached = true; } });
                }, teardown: function () {
                    if (jQuery.nodeName(this, "form")) { return false; }
                    jQuery.event.remove(this, "._submit");
                }
            };
        }
        if (!jQuery.support.changeBubbles) {
            jQuery.event.special.change = {
                setup: function () {
                    if (rformElems.test(this.nodeName)) {
                        if (this.type === "checkbox" || this.type === "radio") { jQuery.event.add(this, "propertychange._change", function (event) { if (event.originalEvent.propertyName === "checked") { this._just_changed = true; } }); jQuery.event.add(this, "click._change", function (event) { if (this._just_changed && !event.isTrigger) { this._just_changed = false; jQuery.event.simulate("change", this, event, true); } }); }
                        return false;
                    }
                    jQuery.event.add(this, "beforeactivate._change", function (e) { var elem = e.target; if (rformElems.test(elem.nodeName) && !elem._change_attached) { jQuery.event.add(elem, "change._change", function (event) { if (this.parentNode && !event.isSimulated && !event.isTrigger) { jQuery.event.simulate("change", this.parentNode, event, true); } }); elem._change_attached = true; } });
                }, handle: function (event) { var elem = event.target; if (this !== elem || event.isSimulated || event.isTrigger || (elem.type !== "radio" && elem.type !== "checkbox")) { return event.handleObj.handler.apply(this, arguments); } }, teardown: function () { jQuery.event.remove(this, "._change"); return rformElems.test(this.nodeName); }
            };
        }
        if (!jQuery.support.focusinBubbles) { jQuery.each({ focus: "focusin", blur: "focusout" }, function (orig, fix) { var attaches = 0, handler = function (event) { jQuery.event.simulate(fix, event.target, jQuery.event.fix(event), true); }; jQuery.event.special[fix] = { setup: function () { if (attaches++ === 0) { document.addEventListener(orig, handler, true); } }, teardown: function () { if (--attaches === 0) { document.removeEventListener(orig, handler, true); } } }; }); }
        jQuery.fn.extend({
            on: function (types, selector, data, fn, one) {
                var origFn, type; if (typeof types === "object") {
                    if (typeof selector !== "string") { data = selector; selector = undefined; }
                    for (type in types) { this.on(type, selector, data, types[type], one); }
                    return this;
                }
                if (data == null && fn == null) { fn = selector; data = selector = undefined; } else if (fn == null) { if (typeof selector === "string") { fn = data; data = undefined; } else { fn = data; data = selector; selector = undefined; } }
                if (fn === false) { fn = returnFalse; } else if (!fn) { return this; }
                if (one === 1) { origFn = fn; fn = function (event) { jQuery().off(event); return origFn.apply(this, arguments); }; fn.guid = origFn.guid || (origFn.guid = jQuery.guid++); }
                return this.each(function () { jQuery.event.add(this, types, fn, data, selector); });
            }, one: function (types, selector, data, fn) { return this.on.call(this, types, selector, data, fn, 1); }, off: function (types, selector, fn) {
                if (types && types.preventDefault && types.handleObj) { var handleObj = types.handleObj; jQuery(types.delegateTarget).off(handleObj.namespace ? handleObj.type + "." + handleObj.namespace : handleObj.type, handleObj.selector, handleObj.handler); return this; }
                if (typeof types === "object") {
                    for (var type in types) { this.off(type, selector, types[type]); }
                    return this;
                }
                if (selector === false || typeof selector === "function") { fn = selector; selector = undefined; }
                if (fn === false) { fn = returnFalse; }
                return this.each(function () { jQuery.event.remove(this, types, fn, selector); });
            }, bind: function (types, data, fn) { return this.on(types, null, data, fn); }, unbind: function (types, fn) { return this.off(types, null, fn); }, live: function (types, data, fn) { jQuery(this.context).on(types, this.selector, data, fn); return this; }, die: function (types, fn) { jQuery(this.context).off(types, this.selector || "**", fn); return this; }, delegate: function (selector, types, data, fn) { return this.on(types, selector, data, fn); }, undelegate: function (selector, types, fn) { return arguments.length == 1 ? this.off(selector, "**") : this.off(types, selector, fn); }, trigger: function (type, data) { return this.each(function () { jQuery.event.trigger(type, data, this); }); }, triggerHandler: function (type, data) { if (this[0]) { return jQuery.event.trigger(type, data, this[0], true); } }, toggle: function (fn) {
                var args = arguments, guid = fn.guid || jQuery.guid++, i = 0, toggler = function (event) { var lastToggle = (jQuery._data(this, "lastToggle" + fn.guid) || 0) % i; jQuery._data(this, "lastToggle" + fn.guid, lastToggle + 1); event.preventDefault(); return args[lastToggle].apply(this, arguments) || false; }; toggler.guid = guid; while (i < args.length) { args[i++].guid = guid; }
                return this.click(toggler);
            }, hover: function (fnOver, fnOut) { return this.mouseenter(fnOver).mouseleave(fnOut || fnOver); }
        }); jQuery.each(("blur focus focusin focusout load resize scroll unload click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup error contextmenu").split(" "), function (i, name) {
            jQuery.fn[name] = function (data, fn) {
                if (fn == null) { fn = data; data = null; }
                return arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
            }; if (jQuery.attrFn) { jQuery.attrFn[name] = true; }
            if (rkeyEvent.test(name)) { jQuery.event.fixHooks[name] = jQuery.event.keyHooks; }
            if (rmouseEvent.test(name)) { jQuery.event.fixHooks[name] = jQuery.event.mouseHooks; }
        }); (function () {
            var chunker = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g, expando = "sizcache" + (Math.random() + '').replace('.', ''), done = 0, toString = Object.prototype.toString, hasDuplicate = false, baseHasDuplicate = true, rBackslash = /\\/g, rReturn = /\r\n/g, rNonWord = /\W/;[0, 0].sort(function () { baseHasDuplicate = false; return 0; }); var Sizzle = function (selector, context, results, seed) {
                results = results || []; context = context || document; var origContext = context; if (context.nodeType !== 1 && context.nodeType !== 9) { return []; }
                if (!selector || typeof selector !== "string") { return results; }
                var m, set, checkSet, extra, ret, cur, pop, i, prune = true, contextXML = Sizzle.isXML(context), parts = [], soFar = selector; do { chunker.exec(""); m = chunker.exec(soFar); if (m) { soFar = m[3]; parts.push(m[1]); if (m[2]) { extra = m[3]; break; } } } while (m); if (parts.length > 1 && origPOS.exec(selector)) {
                    if (parts.length === 2 && Expr.relative[parts[0]]) { set = posProcess(parts[0] + parts[1], context, seed); } else {
                        set = Expr.relative[parts[0]] ? [context] : Sizzle(parts.shift(), context); while (parts.length) {
                            selector = parts.shift(); if (Expr.relative[selector]) { selector += parts.shift(); }
                            set = posProcess(selector, set, seed);
                        }
                    }
                } else {
                    if (!seed && parts.length > 1 && context.nodeType === 9 && !contextXML && Expr.match.ID.test(parts[0]) && !Expr.match.ID.test(parts[parts.length - 1])) { ret = Sizzle.find(parts.shift(), context, contextXML); context = ret.expr ? Sizzle.filter(ret.expr, ret.set)[0] : ret.set[0]; }
                    if (context) {
                        ret = seed ? { expr: parts.pop(), set: makeArray(seed) } : Sizzle.find(parts.pop(), parts.length === 1 && (parts[0] === "~" || parts[0] === "+") && context.parentNode ? context.parentNode : context, contextXML); set = ret.expr ? Sizzle.filter(ret.expr, ret.set) : ret.set; if (parts.length > 0) { checkSet = makeArray(set); } else { prune = false; }
                        while (parts.length) {
                            cur = parts.pop(); pop = cur; if (!Expr.relative[cur]) { cur = ""; } else { pop = parts.pop(); }
                            if (pop == null) { pop = context; }
                            Expr.relative[cur](checkSet, pop, contextXML);
                        }
                    } else { checkSet = parts = []; }
                }
                if (!checkSet) { checkSet = set; }
                if (!checkSet) { Sizzle.error(cur || selector); }
                if (toString.call(checkSet) === "[object Array]") { if (!prune) { results.push.apply(results, checkSet); } else if (context && context.nodeType === 1) { for (i = 0; checkSet[i] != null; i++) { if (checkSet[i] && (checkSet[i] === true || checkSet[i].nodeType === 1 && Sizzle.contains(context, checkSet[i]))) { results.push(set[i]); } } } else { for (i = 0; checkSet[i] != null; i++) { if (checkSet[i] && checkSet[i].nodeType === 1) { results.push(set[i]); } } } } else { makeArray(checkSet, results); }
                if (extra) { Sizzle(extra, origContext, results, seed); Sizzle.uniqueSort(results); }
                return results;
            }; Sizzle.uniqueSort = function (results) {
                if (sortOrder) { hasDuplicate = baseHasDuplicate; results.sort(sortOrder); if (hasDuplicate) { for (var i = 1; i < results.length; i++) { if (results[i] === results[i - 1]) { results.splice(i--, 1); } } } }
                return results;
            }; Sizzle.matches = function (expr, set) { return Sizzle(expr, null, null, set); }; Sizzle.matchesSelector = function (node, expr) { return Sizzle(expr, null, null, [node]).length > 0; }; Sizzle.find = function (expr, context, isXML) {
                var set, i, len, match, type, left; if (!expr) { return []; }
                for (i = 0, len = Expr.order.length; i < len; i++) { type = Expr.order[i]; if ((match = Expr.leftMatch[type].exec(expr))) { left = match[1]; match.splice(1, 1); if (left.substr(left.length - 1) !== "\\") { match[1] = (match[1] || "").replace(rBackslash, ""); set = Expr.find[type](match, context, isXML); if (set != null) { expr = expr.replace(Expr.match[type], ""); break; } } } }
                if (!set) { set = typeof context.getElementsByTagName !== "undefined" ? context.getElementsByTagName("*") : []; }
                return { set: set, expr: expr };
            }; Sizzle.filter = function (expr, set, inplace, not) {
                var match, anyFound, type, found, item, filter, left, i, pass, old = expr, result = [], curLoop = set, isXMLFilter = set && set[0] && Sizzle.isXML(set[0]); while (expr && set.length) {
                    for (type in Expr.filter) {
                        if ((match = Expr.leftMatch[type].exec(expr)) != null && match[2]) {
                            filter = Expr.filter[type]; left = match[1]; anyFound = false; match.splice(1, 1); if (left.substr(left.length - 1) === "\\") { continue; }
                            if (curLoop === result) { result = []; }
                            if (Expr.preFilter[type]) { match = Expr.preFilter[type](match, curLoop, inplace, result, not, isXMLFilter); if (!match) { anyFound = found = true; } else if (match === true) { continue; } }
                            if (match) { for (i = 0; (item = curLoop[i]) != null; i++) { if (item) { found = filter(item, match, i, curLoop); pass = not ^ found; if (inplace && found != null) { if (pass) { anyFound = true; } else { curLoop[i] = false; } } else if (pass) { result.push(item); anyFound = true; } } } }
                            if (found !== undefined) {
                                if (!inplace) { curLoop = result; }
                                expr = expr.replace(Expr.match[type], ""); if (!anyFound) { return []; }
                                break;
                            }
                        }
                    }
                    if (expr === old) { if (anyFound == null) { Sizzle.error(expr); } else { break; } }
                    old = expr;
                }
                return curLoop;
            }; Sizzle.error = function (msg) { throw new Error("Syntax error, unrecognized expression: " + msg); }; var getText = Sizzle.getText = function (elem) {
                var i, node, nodeType = elem.nodeType, ret = ""; if (nodeType) { if (nodeType === 1 || nodeType === 9) { if (typeof elem.textContent === 'string') { return elem.textContent; } else if (typeof elem.innerText === 'string') { return elem.innerText.replace(rReturn, ''); } else { for (elem = elem.firstChild; elem; elem = elem.nextSibling) { ret += getText(elem); } } } else if (nodeType === 3 || nodeType === 4) { return elem.nodeValue; } } else { for (i = 0; (node = elem[i]) ; i++) { if (node.nodeType !== 8) { ret += getText(node); } } }
                return ret;
            }; var Expr = Sizzle.selectors = {
                order: ["ID", "NAME", "TAG"], match: { ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/, CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/, NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/, ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/, TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/, CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/, POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/, PSEUDO: /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/ }, leftMatch: {}, attrMap: { "class": "className", "for": "htmlFor" }, attrHandle: { href: function (elem) { return elem.getAttribute("href"); }, type: function (elem) { return elem.getAttribute("type"); } }, relative: {
                    "+": function (checkSet, part) {
                        var isPartStr = typeof part === "string", isTag = isPartStr && !rNonWord.test(part), isPartStrNotTag = isPartStr && !isTag; if (isTag) { part = part.toLowerCase(); }
                        for (var i = 0, l = checkSet.length, elem; i < l; i++) {
                            if ((elem = checkSet[i])) {
                                while ((elem = elem.previousSibling) && elem.nodeType !== 1) { }
                                checkSet[i] = isPartStrNotTag || elem && elem.nodeName.toLowerCase() === part ? elem || false : elem === part;
                            }
                        }
                        if (isPartStrNotTag) { Sizzle.filter(part, checkSet, true); }
                    }, ">": function (checkSet, part) {
                        var elem, isPartStr = typeof part === "string", i = 0, l = checkSet.length; if (isPartStr && !rNonWord.test(part)) { part = part.toLowerCase(); for (; i < l; i++) { elem = checkSet[i]; if (elem) { var parent = elem.parentNode; checkSet[i] = parent.nodeName.toLowerCase() === part ? parent : false; } } } else {
                            for (; i < l; i++) { elem = checkSet[i]; if (elem) { checkSet[i] = isPartStr ? elem.parentNode : elem.parentNode === part; } }
                            if (isPartStr) { Sizzle.filter(part, checkSet, true); }
                        }
                    }, "": function (checkSet, part, isXML) {
                        var nodeCheck, doneName = done++, checkFn = dirCheck; if (typeof part === "string" && !rNonWord.test(part)) { part = part.toLowerCase(); nodeCheck = part; checkFn = dirNodeCheck; }
                        checkFn("parentNode", part, doneName, checkSet, nodeCheck, isXML);
                    }, "~": function (checkSet, part, isXML) {
                        var nodeCheck, doneName = done++, checkFn = dirCheck; if (typeof part === "string" && !rNonWord.test(part)) { part = part.toLowerCase(); nodeCheck = part; checkFn = dirNodeCheck; }
                        checkFn("previousSibling", part, doneName, checkSet, nodeCheck, isXML);
                    }
                }, find: {
                    ID: function (match, context, isXML) { if (typeof context.getElementById !== "undefined" && !isXML) { var m = context.getElementById(match[1]); return m && m.parentNode ? [m] : []; } }, NAME: function (match, context) {
                        if (typeof context.getElementsByName !== "undefined") {
                            var ret = [], results = context.getElementsByName(match[1]); for (var i = 0, l = results.length; i < l; i++) { if (results[i].getAttribute("name") === match[1]) { ret.push(results[i]); } }
                            return ret.length === 0 ? null : ret;
                        }
                    }, TAG: function (match, context) { if (typeof context.getElementsByTagName !== "undefined") { return context.getElementsByTagName(match[1]); } }
                }, preFilter: {
                    CLASS: function (match, curLoop, inplace, result, not, isXML) {
                        match = " " + match[1].replace(rBackslash, "") + " "; if (isXML) { return match; }
                        for (var i = 0, elem; (elem = curLoop[i]) != null; i++) { if (elem) { if (not ^ (elem.className && (" " + elem.className + " ").replace(/[\t\n\r]/g, " ").indexOf(match) >= 0)) { if (!inplace) { result.push(elem); } } else if (inplace) { curLoop[i] = false; } } }
                        return false;
                    }, ID: function (match) { return match[1].replace(rBackslash, ""); }, TAG: function (match, curLoop) { return match[1].replace(rBackslash, "").toLowerCase(); }, CHILD: function (match) {
                        if (match[1] === "nth") {
                            if (!match[2]) { Sizzle.error(match[0]); }
                            match[2] = match[2].replace(/^\+|\s*/g, ''); var test = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(match[2] === "even" && "2n" || match[2] === "odd" && "2n+1" || !/\D/.test(match[2]) && "0n+" + match[2] || match[2]); match[2] = (test[1] + (test[2] || 1)) - 0; match[3] = test[3] - 0;
                        }
                        else if (match[2]) { Sizzle.error(match[0]); }
                        match[0] = done++; return match;
                    }, ATTR: function (match, curLoop, inplace, result, not, isXML) {
                        var name = match[1] = match[1].replace(rBackslash, ""); if (!isXML && Expr.attrMap[name]) { match[1] = Expr.attrMap[name]; }
                        match[4] = (match[4] || match[5] || "").replace(rBackslash, ""); if (match[2] === "~=") { match[4] = " " + match[4] + " "; }
                        return match;
                    }, PSEUDO: function (match, curLoop, inplace, result, not) {
                        if (match[1] === "not") {
                            if ((chunker.exec(match[3]) || "").length > 1 || /^\w/.test(match[3])) { match[3] = Sizzle(match[3], null, null, curLoop); } else {
                                var ret = Sizzle.filter(match[3], curLoop, inplace, true ^ not); if (!inplace) { result.push.apply(result, ret); }
                                return false;
                            }
                        } else if (Expr.match.POS.test(match[0]) || Expr.match.CHILD.test(match[0])) { return true; }
                        return match;
                    }, POS: function (match) { match.unshift(true); return match; }
                }, filters: {
                    enabled: function (elem) { return elem.disabled === false && elem.type !== "hidden"; }, disabled: function (elem) { return elem.disabled === true; }, checked: function (elem) { return elem.checked === true; }, selected: function (elem) {
                        if (elem.parentNode) { elem.parentNode.selectedIndex; }
                        return elem.selected === true;
                    }, parent: function (elem) { return !!elem.firstChild; }, empty: function (elem) { return !elem.firstChild; }, has: function (elem, i, match) { return !!Sizzle(match[3], elem).length; }, header: function (elem) { return (/h\d/i).test(elem.nodeName); }, text: function (elem) {
                        var attr = elem.getAttribute("type"), type = elem.type;
                        return elem.nodeName.toLowerCase() === "input" && "text" === type && (attr === type || attr === null);
                    }, radio: function (elem) { return elem.nodeName.toLowerCase() === "input" && "radio" === elem.type; }, checkbox: function (elem) { return elem.nodeName.toLowerCase() === "input" && "checkbox" === elem.type; }, file: function (elem) { return elem.nodeName.toLowerCase() === "input" && "file" === elem.type; }, password: function (elem) { return elem.nodeName.toLowerCase() === "input" && "password" === elem.type; }, submit: function (elem) { var name = elem.nodeName.toLowerCase(); return (name === "input" || name === "button") && "submit" === elem.type; }, image: function (elem) { return elem.nodeName.toLowerCase() === "input" && "image" === elem.type; }, reset: function (elem) { var name = elem.nodeName.toLowerCase(); return (name === "input" || name === "button") && "reset" === elem.type; }, button: function (elem) { var name = elem.nodeName.toLowerCase(); return name === "input" && "button" === elem.type || name === "button"; }, input: function (elem) { return (/input|select|textarea|button/i).test(elem.nodeName); }, focus: function (elem) { return elem === elem.ownerDocument.activeElement; }
                }, setFilters: { first: function (elem, i) { return i === 0; }, last: function (elem, i, match, array) { return i === array.length - 1; }, even: function (elem, i) { return i % 2 === 0; }, odd: function (elem, i) { return i % 2 === 1; }, lt: function (elem, i, match) { return i < match[3] - 0; }, gt: function (elem, i, match) { return i > match[3] - 0; }, nth: function (elem, i, match) { return match[3] - 0 === i; }, eq: function (elem, i, match) { return match[3] - 0 === i; } }, filter: {
                    PSEUDO: function (elem, match, i, array) {
                        var name = match[1], filter = Expr.filters[name]; if (filter) { return filter(elem, i, match, array); } else if (name === "contains") { return (elem.textContent || elem.innerText || getText([elem]) || "").indexOf(match[3]) >= 0; } else if (name === "not") {
                            var not = match[3]; for (var j = 0, l = not.length; j < l; j++) { if (not[j] === elem) { return false; } }
                            return true;
                        } else { Sizzle.error(name); }
                    }, CHILD: function (elem, match) {
                        var first, last, doneName, parent, cache, count, diff, type = match[1], node = elem; switch (type) {
                            case "only": case "first": while ((node = node.previousSibling)) { if (node.nodeType === 1) { return false; } }
                                if (type === "first") { return true; }
                                node = elem; case "last": while ((node = node.nextSibling)) { if (node.nodeType === 1) { return false; } }
                                    return true; case "nth": first = match[2]; last = match[3]; if (first === 1 && last === 0) { return true; }
                                        doneName = match[0]; parent = elem.parentNode; if (parent && (parent[expando] !== doneName || !elem.nodeIndex)) {
                                            count = 0; for (node = parent.firstChild; node; node = node.nextSibling) { if (node.nodeType === 1) { node.nodeIndex = ++count; } }
                                            parent[expando] = doneName;
                                        }
                                        diff = elem.nodeIndex - last; if (first === 0) { return diff === 0; } else { return (diff % first === 0 && diff / first >= 0); }
                        }
                    }, ID: function (elem, match) { return elem.nodeType === 1 && elem.getAttribute("id") === match; }, TAG: function (elem, match) { return (match === "*" && elem.nodeType === 1) || !!elem.nodeName && elem.nodeName.toLowerCase() === match; }, CLASS: function (elem, match) { return (" " + (elem.className || elem.getAttribute("class")) + " ").indexOf(match) > -1; }, ATTR: function (elem, match) { var name = match[1], result = Sizzle.attr ? Sizzle.attr(elem, name) : Expr.attrHandle[name] ? Expr.attrHandle[name](elem) : elem[name] != null ? elem[name] : elem.getAttribute(name), value = result + "", type = match[2], check = match[4]; return result == null ? type === "!=" : !type && Sizzle.attr ? result != null : type === "=" ? value === check : type === "*=" ? value.indexOf(check) >= 0 : type === "~=" ? (" " + value + " ").indexOf(check) >= 0 : !check ? value && result !== false : type === "!=" ? value !== check : type === "^=" ? value.indexOf(check) === 0 : type === "$=" ? value.substr(value.length - check.length) === check : type === "|=" ? value === check || value.substr(0, check.length + 1) === check + "-" : false; }, POS: function (elem, match, i, array) { var name = match[2], filter = Expr.setFilters[name]; if (filter) { return filter(elem, i, match, array); } }
                }
            }; var origPOS = Expr.match.POS, fescape = function (all, num) { return "\\" + (num - 0 + 1); }; for (var type in Expr.match) { Expr.match[type] = new RegExp(Expr.match[type].source + (/(?![^\[]*\])(?![^\(]*\))/.source)); Expr.leftMatch[type] = new RegExp(/(^(?:.|\r|\n)*?)/.source + Expr.match[type].source.replace(/\\(\d+)/g, fescape)); }
            var makeArray = function (array, results) {
                array = Array.prototype.slice.call(array, 0); if (results) { results.push.apply(results, array); return results; }
                return array;
            }; try { Array.prototype.slice.call(document.documentElement.childNodes, 0)[0].nodeType; } catch (e) {
                makeArray = function (array, results) {
                    var i = 0, ret = results || []; if (toString.call(array) === "[object Array]") { Array.prototype.push.apply(ret, array); } else { if (typeof array.length === "number") { for (var l = array.length; i < l; i++) { ret.push(array[i]); } } else { for (; array[i]; i++) { ret.push(array[i]); } } }
                    return ret;
                };
            }
            var sortOrder, siblingCheck; if (document.documentElement.compareDocumentPosition) {
                sortOrder = function (a, b) {
                    if (a === b) { hasDuplicate = true; return 0; }
                    if (!a.compareDocumentPosition || !b.compareDocumentPosition) { return a.compareDocumentPosition ? -1 : 1; }
                    return a.compareDocumentPosition(b) & 4 ? -1 : 1;
                };
            } else {
                sortOrder = function (a, b) {
                    if (a === b) { hasDuplicate = true; return 0; } else if (a.sourceIndex && b.sourceIndex) { return a.sourceIndex - b.sourceIndex; }
                    var al, bl, ap = [], bp = [], aup = a.parentNode, bup = b.parentNode, cur = aup; if (aup === bup) { return siblingCheck(a, b); } else if (!aup) { return -1; } else if (!bup) { return 1; }
                    while (cur) { ap.unshift(cur); cur = cur.parentNode; }
                    cur = bup; while (cur) { bp.unshift(cur); cur = cur.parentNode; }
                    al = ap.length; bl = bp.length; for (var i = 0; i < al && i < bl; i++) { if (ap[i] !== bp[i]) { return siblingCheck(ap[i], bp[i]); } }
                    return i === al ? siblingCheck(a, bp[i], -1) : siblingCheck(ap[i], b, 1);
                }; siblingCheck = function (a, b, ret) {
                    if (a === b) { return ret; }
                    var cur = a.nextSibling; while (cur) {
                        if (cur === b) { return -1; }
                        cur = cur.nextSibling;
                    }
                    return 1;
                };
            }
            (function () {
                var form = document.createElement("div"), id = "script" + (new Date()).getTime(), root = document.documentElement; form.innerHTML = "<a name='" + id + "'/>"; root.insertBefore(form, root.firstChild); if (document.getElementById(id)) { Expr.find.ID = function (match, context, isXML) { if (typeof context.getElementById !== "undefined" && !isXML) { var m = context.getElementById(match[1]); return m ? m.id === match[1] || typeof m.getAttributeNode !== "undefined" && m.getAttributeNode("id").nodeValue === match[1] ? [m] : undefined : []; } }; Expr.filter.ID = function (elem, match) { var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id"); return elem.nodeType === 1 && node && node.nodeValue === match; }; }
                root.removeChild(form); root = form = null;
            })(); (function () {
                var div = document.createElement("div"); div.appendChild(document.createComment("")); if (div.getElementsByTagName("*").length > 0) {
                    Expr.find.TAG = function (match, context) {
                        var results = context.getElementsByTagName(match[1]); if (match[1] === "*") {
                            var tmp = []; for (var i = 0; results[i]; i++) { if (results[i].nodeType === 1) { tmp.push(results[i]); } }
                            results = tmp;
                        }
                        return results;
                    };
                }
                div.innerHTML = "<a href='#'></a>"; if (div.firstChild && typeof div.firstChild.getAttribute !== "undefined" && div.firstChild.getAttribute("href") !== "#") { Expr.attrHandle.href = function (elem) { return elem.getAttribute("href", 2); }; }
                div = null;
            })(); if (document.querySelectorAll) {
                (function () {
                    var oldSizzle = Sizzle, div = document.createElement("div"), id = "__sizzle__"; div.innerHTML = "<p class='TEST'></p>"; if (div.querySelectorAll && div.querySelectorAll(".TEST").length === 0) { return; }
                    Sizzle = function (query, context, extra, seed) {
                        context = context || document; if (!seed && !Sizzle.isXML(context)) {
                            var match = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(query); if (match && (context.nodeType === 1 || context.nodeType === 9)) { if (match[1]) { return makeArray(context.getElementsByTagName(query), extra); } else if (match[2] && Expr.find.CLASS && context.getElementsByClassName) { return makeArray(context.getElementsByClassName(match[2]), extra); } }
                            if (context.nodeType === 9) {
                                if (query === "body" && context.body) { return makeArray([context.body], extra); } else if (match && match[3]) { var elem = context.getElementById(match[3]); if (elem && elem.parentNode) { if (elem.id === match[3]) { return makeArray([elem], extra); } } else { return makeArray([], extra); } }
                                try { return makeArray(context.querySelectorAll(query), extra); } catch (qsaError) { }
                            } else if (context.nodeType === 1 && context.nodeName.toLowerCase() !== "object") {
                                var oldContext = context, old = context.getAttribute("id"), nid = old || id, hasParent = context.parentNode, relativeHierarchySelector = /^\s*[+~]/.test(query); if (!old) { context.setAttribute("id", nid); } else { nid = nid.replace(/'/g, "\\$&"); }
                                if (relativeHierarchySelector && hasParent) { context = context.parentNode; }
                                try { if (!relativeHierarchySelector || hasParent) { return makeArray(context.querySelectorAll("[id='" + nid + "'] " + query), extra); } } catch (pseudoError) { } finally { if (!old) { oldContext.removeAttribute("id"); } }
                            }
                        }
                        return oldSizzle(query, context, extra, seed);
                    }; for (var prop in oldSizzle) { Sizzle[prop] = oldSizzle[prop]; }
                    div = null;
                })();
            }
            (function () {
                var html = document.documentElement, matches = html.matchesSelector || html.mozMatchesSelector || html.webkitMatchesSelector || html.msMatchesSelector; if (matches) {
                    var disconnectedMatch = !matches.call(document.createElement("div"), "div"), pseudoWorks = false; try { matches.call(document.documentElement, "[test!='']:sizzle"); } catch (pseudoError) { pseudoWorks = true; }
                    Sizzle.matchesSelector = function (node, expr) {
                        expr = expr.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']"); if (!Sizzle.isXML(node)) { try { if (pseudoWorks || !Expr.match.PSEUDO.test(expr) && !/!=/.test(expr)) { var ret = matches.call(node, expr); if (ret || !disconnectedMatch || node.document && node.document.nodeType !== 11) { return ret; } } } catch (e) { } }
                        return Sizzle(expr, null, null, [node]).length > 0;
                    };
                }
            })(); (function () {
                var div = document.createElement("div"); div.innerHTML = "<div class='test e'></div><div class='test'></div>";
                if (!div.getElementsByClassName || div.getElementsByClassName("e").length === 0) { return; }
                div.lastChild.className = "e"; if (div.getElementsByClassName("e").length === 1) { return; }
                Expr.order.splice(1, 0, "CLASS"); Expr.find.CLASS = function (match, context, isXML) { if (typeof context.getElementsByClassName !== "undefined" && !isXML) { return context.getElementsByClassName(match[1]); } }; div = null;
            })(); function dirNodeCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
                for (var i = 0, l = checkSet.length; i < l; i++) {
                    var elem = checkSet[i]; if (elem) {
                        var match = false; elem = elem[dir]; while (elem) {
                            if (elem[expando] === doneName) { match = checkSet[elem.sizset]; break; }
                            if (elem.nodeType === 1 && !isXML) { elem[expando] = doneName; elem.sizset = i; }
                            if (elem.nodeName.toLowerCase() === cur) { match = elem; break; }
                            elem = elem[dir];
                        }
                        checkSet[i] = match;
                    }
                }
            }
            function dirCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
                for (var i = 0, l = checkSet.length; i < l; i++) {
                    var elem = checkSet[i]; if (elem) {
                        var match = false; elem = elem[dir]; while (elem) {
                            if (elem[expando] === doneName) { match = checkSet[elem.sizset]; break; }
                            if (elem.nodeType === 1) {
                                if (!isXML) { elem[expando] = doneName; elem.sizset = i; }
                                if (typeof cur !== "string") { if (elem === cur) { match = true; break; } } else if (Sizzle.filter(cur, [elem]).length > 0) { match = elem; break; }
                            }
                            elem = elem[dir];
                        }
                        checkSet[i] = match;
                    }
                }
            }
            if (document.documentElement.contains) { Sizzle.contains = function (a, b) { return a !== b && (a.contains ? a.contains(b) : true); }; } else if (document.documentElement.compareDocumentPosition) { Sizzle.contains = function (a, b) { return !!(a.compareDocumentPosition(b) & 16); }; } else { Sizzle.contains = function () { return false; }; }
            Sizzle.isXML = function (elem) { var documentElement = (elem ? elem.ownerDocument || elem : 0).documentElement; return documentElement ? documentElement.nodeName !== "HTML" : false; }; var posProcess = function (selector, context, seed) {
                var match, tmpSet = [], later = "", root = context.nodeType ? [context] : context; while ((match = Expr.match.PSEUDO.exec(selector))) { later += match[0]; selector = selector.replace(Expr.match.PSEUDO, ""); }
                selector = Expr.relative[selector] ? selector + "*" : selector; for (var i = 0, l = root.length; i < l; i++) { Sizzle(selector, root[i], tmpSet, seed); }
                return Sizzle.filter(later, tmpSet);
            }; Sizzle.attr = jQuery.attr; Sizzle.selectors.attrMap = {}; jQuery.find = Sizzle; jQuery.expr = Sizzle.selectors; jQuery.expr[":"] = jQuery.expr.filters; jQuery.unique = Sizzle.uniqueSort; jQuery.text = Sizzle.getText; jQuery.isXMLDoc = Sizzle.isXML; jQuery.contains = Sizzle.contains;
        })(); var runtil = /Until$/, rparentsprev = /^(?:parents|prevUntil|prevAll)/, rmultiselector = /,/, isSimple = /^.[^:#\[\.,]*$/, slice = Array.prototype.slice, POS = jQuery.expr.match.POS, guaranteedUnique = { children: true, contents: true, next: true, prev: true }; jQuery.fn.extend({
            find: function (selector) {
                var self = this, i, l; if (typeof selector !== "string") { return jQuery(selector).filter(function () { for (i = 0, l = self.length; i < l; i++) { if (jQuery.contains(self[i], this)) { return true; } } }); }
                var ret = this.pushStack("", "find", selector), length, n, r; for (i = 0, l = this.length; i < l; i++) { length = ret.length; jQuery.find(selector, this[i], ret); if (i > 0) { for (n = length; n < ret.length; n++) { for (r = 0; r < length; r++) { if (ret[r] === ret[n]) { ret.splice(n--, 1); break; } } } } }
                return ret;
            }, has: function (target) { var targets = jQuery(target); return this.filter(function () { for (var i = 0, l = targets.length; i < l; i++) { if (jQuery.contains(this, targets[i])) { return true; } } }); }, not: function (selector) { return this.pushStack(winnow(this, selector, false), "not", selector); }, filter: function (selector) { return this.pushStack(winnow(this, selector, true), "filter", selector); }, is: function (selector) { return !!selector && (typeof selector === "string" ? POS.test(selector) ? jQuery(selector, this.context).index(this[0]) >= 0 : jQuery.filter(selector, this).length > 0 : this.filter(selector).length > 0); }, closest: function (selectors, context) {
                var ret = [], i, l, cur = this[0]; if (jQuery.isArray(selectors)) {
                    var level = 1; while (cur && cur.ownerDocument && cur !== context) {
                        for (i = 0; i < selectors.length; i++) { if (jQuery(cur).is(selectors[i])) { ret.push({ selector: selectors[i], elem: cur, level: level }); } }
                        cur = cur.parentNode; level++;
                    }
                    return ret;
                }
                var pos = POS.test(selectors) || typeof selectors !== "string" ? jQuery(selectors, context || this.context) : 0; for (i = 0, l = this.length; i < l; i++) { cur = this[i]; while (cur) { if (pos ? pos.index(cur) > -1 : jQuery.find.matchesSelector(cur, selectors)) { ret.push(cur); break; } else { cur = cur.parentNode; if (!cur || !cur.ownerDocument || cur === context || cur.nodeType === 11) { break; } } } }
                ret = ret.length > 1 ? jQuery.unique(ret) : ret; return this.pushStack(ret, "closest", selectors);
            }, index: function (elem) {
                if (!elem) { return (this[0] && this[0].parentNode) ? this.prevAll().length : -1; }
                if (typeof elem === "string") { return jQuery.inArray(this[0], jQuery(elem)); }
                return jQuery.inArray(elem.jquery ? elem[0] : elem, this);
            }, add: function (selector, context) { var set = typeof selector === "string" ? jQuery(selector, context) : jQuery.makeArray(selector && selector.nodeType ? [selector] : selector), all = jQuery.merge(this.get(), set); return this.pushStack(isDisconnected(set[0]) || isDisconnected(all[0]) ? all : jQuery.unique(all)); }, andSelf: function () { return this.add(this.prevObject); }
        }); function isDisconnected(node) { return !node || !node.parentNode || node.parentNode.nodeType === 11; }
        jQuery.each({ parent: function (elem) { var parent = elem.parentNode; return parent && parent.nodeType !== 11 ? parent : null; }, parents: function (elem) { return jQuery.dir(elem, "parentNode"); }, parentsUntil: function (elem, i, until) { return jQuery.dir(elem, "parentNode", until); }, next: function (elem) { return jQuery.nth(elem, 2, "nextSibling"); }, prev: function (elem) { return jQuery.nth(elem, 2, "previousSibling"); }, nextAll: function (elem) { return jQuery.dir(elem, "nextSibling"); }, prevAll: function (elem) { return jQuery.dir(elem, "previousSibling"); }, nextUntil: function (elem, i, until) { return jQuery.dir(elem, "nextSibling", until); }, prevUntil: function (elem, i, until) { return jQuery.dir(elem, "previousSibling", until); }, siblings: function (elem) { return jQuery.sibling(elem.parentNode.firstChild, elem); }, children: function (elem) { return jQuery.sibling(elem.firstChild); }, contents: function (elem) { return jQuery.nodeName(elem, "iframe") ? elem.contentDocument || elem.contentWindow.document : jQuery.makeArray(elem.childNodes); } }, function (name, fn) {
            jQuery.fn[name] = function (until, selector) {
                var ret = jQuery.map(this, fn, until); if (!runtil.test(name)) { selector = until; }
                if (selector && typeof selector === "string") { ret = jQuery.filter(selector, ret); }
                ret = this.length > 1 && !guaranteedUnique[name] ? jQuery.unique(ret) : ret; if ((this.length > 1 || rmultiselector.test(selector)) && rparentsprev.test(name)) { ret = ret.reverse(); }
                return this.pushStack(ret, name, slice.call(arguments).join(","));
            };
        }); jQuery.extend({
            filter: function (expr, elems, not) {
                if (not) { expr = ":not(" + expr + ")"; }
                return elems.length === 1 ? jQuery.find.matchesSelector(elems[0], expr) ? [elems[0]] : [] : jQuery.find.matches(expr, elems);
            }, dir: function (elem, dir, until) {
                var matched = [], cur = elem[dir]; while (cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery(cur).is(until))) {
                    if (cur.nodeType === 1) { matched.push(cur); }
                    cur = cur[dir];
                }
                return matched;
            }, nth: function (cur, result, dir, elem) {
                result = result || 1; var num = 0; for (; cur; cur = cur[dir]) { if (cur.nodeType === 1 && ++num === result) { break; } }
                return cur;
            }, sibling: function (n, elem) {
                var r = []; for (; n; n = n.nextSibling) { if (n.nodeType === 1 && n !== elem) { r.push(n); } }
                return r;
            }
        }); function winnow(elements, qualifier, keep) {
            qualifier = qualifier || 0; if (jQuery.isFunction(qualifier)) { return jQuery.grep(elements, function (elem, i) { var retVal = !!qualifier.call(elem, i, elem); return retVal === keep; }); } else if (qualifier.nodeType) { return jQuery.grep(elements, function (elem, i) { return (elem === qualifier) === keep; }); } else if (typeof qualifier === "string") { var filtered = jQuery.grep(elements, function (elem) { return elem.nodeType === 1; }); if (isSimple.test(qualifier)) { return jQuery.filter(qualifier, filtered, !keep); } else { qualifier = jQuery.filter(qualifier, filtered); } }
            return jQuery.grep(elements, function (elem, i) { return (jQuery.inArray(elem, qualifier) >= 0) === keep; });
        }
        function createSafeFragment(document) {
            var list = nodeNames.split("|"), safeFrag = document.createDocumentFragment(); if (safeFrag.createElement) { while (list.length) { safeFrag.createElement(list.pop()); } }
            return safeFrag;
        }
        var nodeNames = "abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|" + "header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", rinlinejQuery = / jQuery\d+="(?:\d+|null)"/g, rleadingWhitespace = /^\s+/, rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig, rtagName = /<([\w:]+)/, rtbody = /<tbody/i, rhtml = /<|&#?\w+;/, rnoInnerhtml = /<(?:script|style)/i, rnocache = /<(?:script|object|embed|option|style)/i, rnoshimcache = new RegExp("<(?:" + nodeNames + ")", "i"), rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i, rscriptType = /\/(java|ecma)script/i, rcleanScript = /^\s*<!(?:\[CDATA\[|\-\-)/, wrapMap = { option: [1, "<select multiple='multiple'>", "</select>"], legend: [1, "<fieldset>", "</fieldset>"], thead: [1, "<table>", "</table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"], area: [1, "<map>", "</map>"], _default: [0, "", ""] }, safeFragment = createSafeFragment(document); wrapMap.optgroup = wrapMap.option; wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead; wrapMap.th = wrapMap.td; if (!jQuery.support.htmlSerialize) { wrapMap._default = [1, "div<div>", "</div>"]; }
        jQuery.fn.extend({
            text: function (text) {
                if (jQuery.isFunction(text)) { return this.each(function (i) { var self = jQuery(this); self.text(text.call(this, i, self.text())); }); }
                if (typeof text !== "object" && text !== undefined) { return this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode(text)); }
                return jQuery.text(this);
            }, wrapAll: function (html) {
                if (jQuery.isFunction(html)) { return this.each(function (i) { jQuery(this).wrapAll(html.call(this, i)); }); }
                if (this[0]) {
                    var wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(true); if (this[0].parentNode) { wrap.insertBefore(this[0]); }
                    wrap.map(function () {
                        var elem = this; while (elem.firstChild && elem.firstChild.nodeType === 1) { elem = elem.firstChild; }
                        return elem;
                    }).append(this);
                }
                return this;
            }, wrapInner: function (html) {
                if (jQuery.isFunction(html)) { return this.each(function (i) { jQuery(this).wrapInner(html.call(this, i)); }); }
                return this.each(function () { var self = jQuery(this), contents = self.contents(); if (contents.length) { contents.wrapAll(html); } else { self.append(html); } });
            }, wrap: function (html) { var isFunction = jQuery.isFunction(html); return this.each(function (i) { jQuery(this).wrapAll(isFunction ? html.call(this, i) : html); }); }, unwrap: function () { return this.parent().each(function () { if (!jQuery.nodeName(this, "body")) { jQuery(this).replaceWith(this.childNodes); } }).end(); }, append: function () { return this.domManip(arguments, true, function (elem) { if (this.nodeType === 1) { this.appendChild(elem); } }); }, prepend: function () { return this.domManip(arguments, true, function (elem) { if (this.nodeType === 1) { this.insertBefore(elem, this.firstChild); } }); }, before: function () { if (this[0] && this[0].parentNode) { return this.domManip(arguments, false, function (elem) { this.parentNode.insertBefore(elem, this); }); } else if (arguments.length) { var set = jQuery.clean(arguments); set.push.apply(set, this.toArray()); return this.pushStack(set, "before", arguments); } }, after: function () { if (this[0] && this[0].parentNode) { return this.domManip(arguments, false, function (elem) { this.parentNode.insertBefore(elem, this.nextSibling); }); } else if (arguments.length) { var set = this.pushStack(this, "after", arguments); set.push.apply(set, jQuery.clean(arguments)); return set; } }, remove: function (selector, keepData) {
                for (var i = 0, elem; (elem = this[i]) != null; i++) {
                    if (!selector || jQuery.filter(selector, [elem]).length) {
                        if (!keepData && elem.nodeType === 1) { jQuery.cleanData(elem.getElementsByTagName("*")); jQuery.cleanData([elem]); }
                        if (elem.parentNode) { elem.parentNode.removeChild(elem); }
                    }
                }
                return this;
            }, empty: function () {
                for (var i = 0, elem; (elem = this[i]) != null; i++) {
                    if (elem.nodeType === 1) { jQuery.cleanData(elem.getElementsByTagName("*")); }
                    while (elem.firstChild) { elem.removeChild(elem.firstChild); }
                }
                return this;
            }, clone: function (dataAndEvents, deepDataAndEvents) { dataAndEvents = dataAndEvents == null ? false : dataAndEvents; deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents; return this.map(function () { return jQuery.clone(this, dataAndEvents, deepDataAndEvents); }); }, html: function (value) {
                if (value === undefined) { return this[0] && this[0].nodeType === 1 ? this[0].innerHTML.replace(rinlinejQuery, "") : null; } else if (typeof value === "string" && !rnoInnerhtml.test(value) && (jQuery.support.leadingWhitespace || !rleadingWhitespace.test(value)) && !wrapMap[(rtagName.exec(value) || ["", ""])[1].toLowerCase()]) {
                    value = value.replace(rxhtmlTag, "<$1></$2>"); try {
                        for (var i = 0, l = this.length; i < l; i++) { if (this[i].nodeType === 1) { jQuery.cleanData(this[i].getElementsByTagName("*")); this[i].innerHTML = value; } }
                    } catch (e) { this.empty().append(value); }
                } else if (jQuery.isFunction(value)) { this.each(function (i) { var self = jQuery(this); self.html(value.call(this, i, self.html())); }); } else { this.empty().append(value); }
                return this;
            }, replaceWith: function (value) {
                if (this[0] && this[0].parentNode) {
                    if (jQuery.isFunction(value)) { return this.each(function (i) { var self = jQuery(this), old = self.html(); self.replaceWith(value.call(this, i, old)); }); }
                    if (typeof value !== "string") { value = jQuery(value).detach(); }
                    return this.each(function () { var next = this.nextSibling, parent = this.parentNode; jQuery(this).remove(); if (next) { jQuery(next).before(value); } else { jQuery(parent).append(value); } });
                } else { return this.length ? this.pushStack(jQuery(jQuery.isFunction(value) ? value() : value), "replaceWith", value) : this; }
            }, detach: function (selector) { return this.remove(selector, true); }, domManip: function (args, table, callback) {
                var results, first, fragment, parent, value = args[0], scripts = []; if (!jQuery.support.checkClone && arguments.length === 3 && typeof value === "string" && rchecked.test(value)) { return this.each(function () { jQuery(this).domManip(args, table, callback, true); }); }
                if (jQuery.isFunction(value)) { return this.each(function (i) { var self = jQuery(this); args[0] = value.call(this, i, table ? self.html() : undefined); self.domManip(args, table, callback); }); }
                if (this[0]) {
                    parent = value && value.parentNode; if (jQuery.support.parentNode && parent && parent.nodeType === 11 && parent.childNodes.length === this.length) { results = { fragment: parent }; } else { results = jQuery.buildFragment(args, this, scripts); }
                    fragment = results.fragment; if (fragment.childNodes.length === 1) { first = fragment = fragment.firstChild; } else { first = fragment.firstChild; }
                    if (first) { table = table && jQuery.nodeName(first, "tr"); for (var i = 0, l = this.length, lastIndex = l - 1; i < l; i++) { callback.call(table ? root(this[i], first) : this[i], results.cacheable || (l > 1 && i < lastIndex) ? jQuery.clone(fragment, true, true) : fragment); } }
                    if (scripts.length) { jQuery.each(scripts, evalScript); }
                }
                return this;
            }
        }); function root(elem, cur) { return jQuery.nodeName(elem, "table") ? (elem.getElementsByTagName("tbody")[0] || elem.appendChild(elem.ownerDocument.createElement("tbody"))) : elem; }
        function cloneCopyEvent(src, dest) {
            if (dest.nodeType !== 1 || !jQuery.hasData(src)) { return; }
            var type, i, l, oldData = jQuery._data(src), curData = jQuery._data(dest, oldData), events = oldData.events; if (events) { delete curData.handle; curData.events = {}; for (type in events) { for (i = 0, l = events[type].length; i < l; i++) { jQuery.event.add(dest, type + (events[type][i].namespace ? "." : "") + events[type][i].namespace, events[type][i], events[type][i].data); } } }
            if (curData.data) { curData.data = jQuery.extend({}, curData.data); }
        }
        function cloneFixAttributes(src, dest) {
            var nodeName; if (dest.nodeType !== 1) { return; }
            if (dest.clearAttributes) { dest.clearAttributes(); }
            if (dest.mergeAttributes) { dest.mergeAttributes(src); }
            nodeName = dest.nodeName.toLowerCase(); if (nodeName === "object") { dest.outerHTML = src.outerHTML; } else if (nodeName === "input" && (src.type === "checkbox" || src.type === "radio")) {
                if (src.checked) { dest.defaultChecked = dest.checked = src.checked; }
                if (dest.value !== src.value) { dest.value = src.value; }
            } else if (nodeName === "option") { dest.selected = src.defaultSelected; } else if (nodeName === "input" || nodeName === "textarea") { dest.defaultValue = src.defaultValue; }
            dest.removeAttribute(jQuery.expando);
        }
        jQuery.buildFragment = function (args, nodes, scripts) {
            var fragment, cacheable, cacheresults, doc, first = args[0]; if (nodes && nodes[0]) { doc = nodes[0].ownerDocument || nodes[0]; }
            if (!doc.createDocumentFragment) { doc = document; }
            if (args.length === 1 && typeof first === "string" && first.length < 512 && doc === document && first.charAt(0) === "<" && !rnocache.test(first) && (jQuery.support.checkClone || !rchecked.test(first)) && (jQuery.support.html5Clone || !rnoshimcache.test(first))) { cacheable = true; cacheresults = jQuery.fragments[first]; if (cacheresults && cacheresults !== 1) { fragment = cacheresults; } }
            if (!fragment) { fragment = doc.createDocumentFragment(); jQuery.clean(args, doc, fragment, scripts); }
            if (cacheable) { jQuery.fragments[first] = cacheresults ? fragment : 1; }
            return { fragment: fragment, cacheable: cacheable };
        }; jQuery.fragments = {}; jQuery.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (name, original) {
            jQuery.fn[name] = function (selector) {
                var ret = [], insert = jQuery(selector), parent = this.length === 1 && this[0].parentNode; if (parent && parent.nodeType === 11 && parent.childNodes.length === 1 && insert.length === 1) { insert[original](this[0]); return this; } else {
                    for (var i = 0, l = insert.length; i < l; i++) { var elems = (i > 0 ? this.clone(true) : this).get(); jQuery(insert[i])[original](elems); ret = ret.concat(elems); }
                    return this.pushStack(ret, name, insert.selector);
                }
            };
        }); function getAll(elem) { if (typeof elem.getElementsByTagName !== "undefined") { return elem.getElementsByTagName("*"); } else if (typeof elem.querySelectorAll !== "undefined") { return elem.querySelectorAll("*"); } else { return []; } }
        function fixDefaultChecked(elem) { if (elem.type === "checkbox" || elem.type === "radio") { elem.defaultChecked = elem.checked; } }
        function findInputs(elem) { var nodeName = (elem.nodeName || "").toLowerCase(); if (nodeName === "input") { fixDefaultChecked(elem); } else if (nodeName !== "script" && typeof elem.getElementsByTagName !== "undefined") { jQuery.grep(elem.getElementsByTagName("input"), fixDefaultChecked); } }
        function shimCloneNode(elem) { var div = document.createElement("div"); safeFragment.appendChild(div); div.innerHTML = elem.outerHTML; return div.firstChild; }
        jQuery.extend({
            clone: function (elem, dataAndEvents, deepDataAndEvents) {
                var srcElements, destElements, i, clone = jQuery.support.html5Clone || !rnoshimcache.test("<" + elem.nodeName) ? elem.cloneNode(true) : shimCloneNode(elem); if ((!jQuery.support.noCloneEvent || !jQuery.support.noCloneChecked) && (elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem)) { cloneFixAttributes(elem, clone); srcElements = getAll(elem); destElements = getAll(clone); for (i = 0; srcElements[i]; ++i) { if (destElements[i]) { cloneFixAttributes(srcElements[i], destElements[i]); } } }
                if (dataAndEvents) { cloneCopyEvent(elem, clone); if (deepDataAndEvents) { srcElements = getAll(elem); destElements = getAll(clone); for (i = 0; srcElements[i]; ++i) { cloneCopyEvent(srcElements[i], destElements[i]); } } }
                srcElements = destElements = null; return clone;
            }, clean: function (elems, context, fragment, scripts) {
                var checkScriptType; context = context || document; if (typeof context.createElement === "undefined") { context = context.ownerDocument || context[0] && context[0].ownerDocument || document; }
                var ret = [], j; for (var i = 0, elem; (elem = elems[i]) != null; i++) {
                    if (typeof elem === "number") { elem += ""; }
                    if (!elem) { continue; }
                    if (typeof elem === "string") {
                        if (!rhtml.test(elem)) { elem = context.createTextNode(elem); } else {
                            elem = elem.replace(rxhtmlTag, "<$1></$2>"); var tag = (rtagName.exec(elem) || ["", ""])[1].toLowerCase(), wrap = wrapMap[tag] || wrapMap._default, depth = wrap[0], div = context.createElement("div"); if (context === document) { safeFragment.appendChild(div); } else { createSafeFragment(context).appendChild(div); }
                            div.innerHTML = wrap[1] + elem + wrap[2]; while (depth--) { div = div.lastChild; }
                            if (!jQuery.support.tbody) { var hasBody = rtbody.test(elem), tbody = tag === "table" && !hasBody ? div.firstChild && div.firstChild.childNodes : wrap[1] === "<table>" && !hasBody ? div.childNodes : []; for (j = tbody.length - 1; j >= 0; --j) { if (jQuery.nodeName(tbody[j], "tbody") && !tbody[j].childNodes.length) { tbody[j].parentNode.removeChild(tbody[j]); } } }
                            if (!jQuery.support.leadingWhitespace && rleadingWhitespace.test(elem)) { div.insertBefore(context.createTextNode(rleadingWhitespace.exec(elem)[0]), div.firstChild); }
                            elem = div.childNodes;
                        }
                    }
                    var len; if (!jQuery.support.appendChecked) { if (elem[0] && typeof (len = elem.length) === "number") { for (j = 0; j < len; j++) { findInputs(elem[j]); } } else { findInputs(elem); } }
                    if (elem.nodeType) { ret.push(elem); } else { ret = jQuery.merge(ret, elem); }
                }
                if (fragment) {
                    checkScriptType = function (elem) { return !elem.type || rscriptType.test(elem.type); }; for (i = 0; ret[i]; i++) {
                        if (scripts && jQuery.nodeName(ret[i], "script") && (!ret[i].type || ret[i].type.toLowerCase() === "text/javascript")) { scripts.push(ret[i].parentNode ? ret[i].parentNode.removeChild(ret[i]) : ret[i]); } else {
                            if (ret[i].nodeType === 1) { var jsTags = jQuery.grep(ret[i].getElementsByTagName("script"), checkScriptType); ret.splice.apply(ret, [i + 1, 0].concat(jsTags)); }
                            fragment.appendChild(ret[i]);
                        }
                    }
                }
                return ret;
            }, cleanData: function (elems) {
                var data, id, cache = jQuery.cache, special = jQuery.event.special, deleteExpando = jQuery.support.deleteExpando; for (var i = 0, elem; (elem = elems[i]) != null; i++) {
                    if (elem.nodeName && jQuery.noData[elem.nodeName.toLowerCase()]) { continue; }
                    id = elem[jQuery.expando]; if (id) {
                        data = cache[id]; if (data && data.events) {
                            for (var type in data.events) { if (special[type]) { jQuery.event.remove(elem, type); } else { jQuery.removeEvent(elem, type, data.handle); } }
                            if (data.handle) { data.handle.elem = null; }
                        }
                        if (deleteExpando) { delete elem[jQuery.expando]; } else if (elem.removeAttribute) { elem.removeAttribute(jQuery.expando); }
                        delete cache[id];
                    }
                }
            }
        }); function evalScript(i, elem) {
            if (elem.src) { jQuery.ajax({ url: elem.src, async: false, dataType: "script" }); } else { jQuery.globalEval((elem.text || elem.textContent || elem.innerHTML || "").replace(rcleanScript, "/*$0*/")); }
            if (elem.parentNode) { elem.parentNode.removeChild(elem); }
        }
        var ralpha = /alpha\([^)]*\)/i, ropacity = /opacity=([^)]*)/, rupper = /([A-Z]|^ms)/g, rnumpx = /^-?\d+(?:px)?$/i, rnum = /^-?\d/, rrelNum = /^([\-+])=([\-+.\de]+)/, cssShow = { position: "absolute", visibility: "hidden", display: "block" }, cssWidth = ["Left", "Right"], cssHeight = ["Top", "Bottom"], curCSS, getComputedStyle, currentStyle; jQuery.fn.css = function (name, value) {
            if (arguments.length === 2 && value === undefined) { return this; }
            return jQuery.access(this, name, value, true, function (elem, name, value) { return value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name); });
        }; jQuery.extend({
            cssHooks: { opacity: { get: function (elem, computed) { if (computed) { var ret = curCSS(elem, "opacity", "opacity"); return ret === "" ? "1" : ret; } else { return elem.style.opacity; } } } }, cssNumber: { "fillOpacity": true, "fontWeight": true, "lineHeight": true, "opacity": true, "orphans": true, "widows": true, "zIndex": true, "zoom": true }, cssProps: { "float": jQuery.support.cssFloat ? "cssFloat" : "styleFloat" }, style: function (elem, name, value, extra) {
                if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) { return; }
                var ret, type, origName = jQuery.camelCase(name), style = elem.style, hooks = jQuery.cssHooks[origName]; name = jQuery.cssProps[origName] || origName; if (value !== undefined) {
                    type = typeof value; if (type === "string" && (ret = rrelNum.exec(value))) { value = (+(ret[1] + 1) * +ret[2]) + parseFloat(jQuery.css(elem, name)); type = "number"; }
                    if (value == null || type === "number" && isNaN(value)) { return; }
                    if (type === "number" && !jQuery.cssNumber[origName]) { value += "px"; }
                    if (!hooks || !("set" in hooks) || (value = hooks.set(elem, value)) !== undefined) { try { style[name] = value; } catch (e) { } }
                } else {
                    if (hooks && "get" in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) { return ret; }
                    return style[name];
                }
            }, css: function (elem, name, extra) {
                var ret, hooks; name = jQuery.camelCase(name); hooks = jQuery.cssHooks[name]; name = jQuery.cssProps[name] || name; if (name === "cssFloat") { name = "float"; }
                if (hooks && "get" in hooks && (ret = hooks.get(elem, true, extra)) !== undefined) { return ret; } else if (curCSS) { return curCSS(elem, name); }
            }, swap: function (elem, options, callback) {
                var old = {}; for (var name in options) { old[name] = elem.style[name]; elem.style[name] = options[name]; }
                callback.call(elem); for (name in options) { elem.style[name] = old[name]; }
            }
        }); jQuery.curCSS = jQuery.css; jQuery.each(["height", "width"], function (i, name) {
            jQuery.cssHooks[name] = {
                get: function (elem, computed, extra) {
                    var val; if (computed) {
                        if (elem.offsetWidth !== 0) { return getWH(elem, name, extra); } else { jQuery.swap(elem, cssShow, function () { val = getWH(elem, name, extra); }); }
                        return val;
                    }
                }, set: function (elem, value) { if (rnumpx.test(value)) { value = parseFloat(value); if (value >= 0) { return value + "px"; } } else { return value; } }
            };
        }); if (!jQuery.support.opacity) {
            jQuery.cssHooks.opacity = {
                get: function (elem, computed) { return ropacity.test((computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "") ? (parseFloat(RegExp.$1) / 100) + "" : computed ? "1" : ""; }, set: function (elem, value) {
                    var style = elem.style, currentStyle = elem.currentStyle, opacity = jQuery.isNumeric(value) ? "alpha(opacity=" + value * 100 + ")" : "", filter = currentStyle && currentStyle.filter || style.filter || ""; style.zoom = 1; if (value >= 1 && jQuery.trim(filter.replace(ralpha, "")) === "") { style.removeAttribute("filter"); if (currentStyle && !currentStyle.filter) { return; } }
                    style.filter = ralpha.test(filter) ? filter.replace(ralpha, opacity) : filter + " " + opacity;
                }
            };
        }
        jQuery(function () { if (!jQuery.support.reliableMarginRight) { jQuery.cssHooks.marginRight = { get: function (elem, computed) { var ret; jQuery.swap(elem, { "display": "inline-block" }, function () { if (computed) { ret = curCSS(elem, "margin-right", "marginRight"); } else { ret = elem.style.marginRight; } }); return ret; } }; } }); if (document.defaultView && document.defaultView.getComputedStyle) {
            getComputedStyle = function (elem, name) {
                var ret, defaultView, computedStyle; name = name.replace(rupper, "-$1").toLowerCase(); if ((defaultView = elem.ownerDocument.defaultView) && (computedStyle = defaultView.getComputedStyle(elem, null))) { ret = computedStyle.getPropertyValue(name); if (ret === "" && !jQuery.contains(elem.ownerDocument.documentElement, elem)) { ret = jQuery.style(elem, name); } }
                return ret;
            };
        }
        if (document.documentElement.currentStyle) {
            currentStyle = function (elem, name) {
                var left, rsLeft, uncomputed, ret = elem.currentStyle && elem.currentStyle[name], style = elem.style; if (ret === null && style && (uncomputed = style[name])) { ret = uncomputed; }
                if (!rnumpx.test(ret) && rnum.test(ret)) {
                    left = style.left; rsLeft = elem.runtimeStyle && elem.runtimeStyle.left; if (rsLeft) { elem.runtimeStyle.left = elem.currentStyle.left; }
                    style.left = name === "fontSize" ? "1em" : (ret || 0); ret = style.pixelLeft + "px"; style.left = left; if (rsLeft) { elem.runtimeStyle.left = rsLeft; }
                }
                return ret === "" ? "auto" : ret;
            };
        }
        curCSS = getComputedStyle || currentStyle; function getWH(elem, name, extra) {
            var val = name === "width" ? elem.offsetWidth : elem.offsetHeight, which = name === "width" ? cssWidth : cssHeight, i = 0, len = which.length; if (val > 0) {
                if (extra !== "border") {
                    for (; i < len; i++) {
                        if (!extra) { val -= parseFloat(jQuery.css(elem, "padding" + which[i])) || 0; }
                        if (extra === "margin") { val += parseFloat(jQuery.css(elem, extra + which[i])) || 0; } else { val -= parseFloat(jQuery.css(elem, "border" + which[i] + "Width")) || 0; }
                    }
                }
                return val + "px";
            }
            val = curCSS(elem, name, name); if (val < 0 || val == null) { val = elem.style[name] || 0; }
            val = parseFloat(val) || 0; if (extra) {
                for (; i < len; i++) {
                    val += parseFloat(jQuery.css(elem, "padding" + which[i])) || 0; if (extra !== "padding") { val += parseFloat(jQuery.css(elem, "border" + which[i] + "Width")) || 0; }
                    if (extra === "margin") { val += parseFloat(jQuery.css(elem, extra + which[i])) || 0; }
                }
            }
            return val + "px";
        }
        if (jQuery.expr && jQuery.expr.filters) { jQuery.expr.filters.hidden = function (elem) { var width = elem.offsetWidth, height = elem.offsetHeight; return (width === 0 && height === 0) || (!jQuery.support.reliableHiddenOffsets && ((elem.style && elem.style.display) || jQuery.css(elem, "display")) === "none"); }; jQuery.expr.filters.visible = function (elem) { return !jQuery.expr.filters.hidden(elem); }; }
        var r20 = /%20/g, rbracket = /\[\]$/, rCRLF = /\r?\n/g, rhash = /#.*$/, rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg, rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i, rlocalProtocol = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/, rnoContent = /^(?:GET|HEAD)$/, rprotocol = /^\/\//, rquery = /\?/, rscript = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, rselectTextarea = /^(?:select|textarea)/i, rspacesAjax = /\s+/, rts = /([?&])_=[^&]*/, rurl = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/, _load = jQuery.fn.load, prefilters = {}, transports = {}, ajaxLocation, ajaxLocParts, allTypes = ["*/"] + ["*"]; try { ajaxLocation = location.href; } catch (e) { ajaxLocation = document.createElement("a"); ajaxLocation.href = ""; ajaxLocation = ajaxLocation.href; }
        ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || []; function addToPrefiltersOrTransports(structure) {
            return function (dataTypeExpression, func) {
                if (typeof dataTypeExpression !== "string") { func = dataTypeExpression; dataTypeExpression = "*"; }
                if (jQuery.isFunction(func)) {
                    var dataTypes = dataTypeExpression.toLowerCase().split(rspacesAjax), i = 0, length = dataTypes.length, dataType, list, placeBefore; for (; i < length; i++) {
                        dataType = dataTypes[i]; placeBefore = /^\+/.test(dataType); if (placeBefore) { dataType = dataType.substr(1) || "*"; }
                        list = structure[dataType] = structure[dataType] || []; list[placeBefore ? "unshift" : "push"](func);
                    }
                }
            };
        }
        function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR, dataType, inspected) {
            dataType = dataType || options.dataTypes[0]; inspected = inspected || {}; inspected[dataType] = true; var list = structure[dataType], i = 0, length = list ? list.length : 0, executeOnly = (structure === prefilters), selection; for (; i < length && (executeOnly || !selection) ; i++) { selection = list[i](options, originalOptions, jqXHR); if (typeof selection === "string") { if (!executeOnly || inspected[selection]) { selection = undefined; } else { options.dataTypes.unshift(selection); selection = inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR, selection, inspected); } } }
            if ((executeOnly || !selection) && !inspected["*"]) { selection = inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR, "*", inspected); }
            return selection;
        }
        function ajaxExtend(target, src) {
            var key, deep, flatOptions = jQuery.ajaxSettings.flatOptions || {}; for (key in src) { if (src[key] !== undefined) { (flatOptions[key] ? target : (deep || (deep = {})))[key] = src[key]; } }
            if (deep) { jQuery.extend(true, target, deep); }
        }
        jQuery.fn.extend({
            load: function (url, params, callback) {
                if (typeof url !== "string" && _load) { return _load.apply(this, arguments); } else if (!this.length) { return this; }
                var off = url.indexOf(" "); if (off >= 0) { var selector = url.slice(off, url.length); url = url.slice(0, off); }
                var type = "GET"; if (params) { if (jQuery.isFunction(params)) { callback = params; params = undefined; } else if (typeof params === "object") { params = jQuery.param(params, jQuery.ajaxSettings.traditional); type = "POST"; } }
                var self = this; jQuery.ajax({
                    url: url, type: type, dataType: "html", data: params, complete: function (jqXHR, status, responseText) {
                        responseText = jqXHR.responseText; if (jqXHR.isResolved()) {
                            jqXHR.done(function (r) { responseText = r; }); self.html(selector ? jQuery("<div>")
                            .append(responseText.replace(rscript, ""))
                            .find(selector) : responseText);
                        }
                        if (callback) { self.each(callback, [responseText, status, jqXHR]); }
                    }
                }); return this;
            }, serialize: function () { return jQuery.param(this.serializeArray()); }, serializeArray: function () { return this.map(function () { return this.elements ? jQuery.makeArray(this.elements) : this; }).filter(function () { return this.name && !this.disabled && (this.checked || rselectTextarea.test(this.nodeName) || rinput.test(this.type)); }).map(function (i, elem) { var val = jQuery(this).val(); return val == null ? null : jQuery.isArray(val) ? jQuery.map(val, function (val, i) { return { name: elem.name, value: val.replace(rCRLF, "\r\n") }; }) : { name: elem.name, value: val.replace(rCRLF, "\r\n") }; }).get(); }
        }); jQuery.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function (i, o) { jQuery.fn[o] = function (f) { return this.on(o, f); }; }); jQuery.each(["get", "post"], function (i, method) {
            jQuery[method] = function (url, data, callback, type) {
                if (jQuery.isFunction(data)) { type = type || callback; callback = data; data = undefined; }
                return jQuery.ajax({ type: method, url: url, data: data, success: callback, dataType: type });
            };
        }); jQuery.extend({
            getScript: function (url, callback) { return jQuery.get(url, undefined, callback, "script"); }, getJSON: function (url, data, callback) { return jQuery.get(url, data, callback, "json"); }, ajaxSetup: function (target, settings) {
                if (settings) { ajaxExtend(target, jQuery.ajaxSettings); } else { settings = target; target = jQuery.ajaxSettings; }
                ajaxExtend(target, settings); return target;
            }, ajaxSettings: {
                url: ajaxLocation, isLocal: rlocalProtocol.test(ajaxLocParts[1]), global: true, type: "GET", contentType: "application/x-www-form-urlencoded", processData: true, async: true, accepts: { xml: "application/xml, text/xml", html: "text/html", text: "text/plain", json: "application/json, text/javascript", "*": allTypes }, contents: { xml: /xml/, html: /html/, json: /json/ }, responseFields: { xml: "responseXML", text: "responseText" },
                converters: { "* text": window.String, "text html": true, "text json": jQuery.parseJSON, "text xml": jQuery.parseXML }, flatOptions: { context: true, url: true }
            }, ajaxPrefilter: addToPrefiltersOrTransports(prefilters), ajaxTransport: addToPrefiltersOrTransports(transports), ajax: function (url, options) {
                if (typeof url === "object") { options = url; url = undefined; }
                options = options || {}; var s = jQuery.ajaxSetup({}, options), callbackContext = s.context || s, globalEventContext = callbackContext !== s && (callbackContext.nodeType || callbackContext instanceof jQuery) ? jQuery(callbackContext) : jQuery.event, deferred = jQuery.Deferred(), completeDeferred = jQuery.Callbacks("once memory"), statusCode = s.statusCode || {}, ifModifiedKey, requestHeaders = {}, requestHeadersNames = {}, responseHeadersString, responseHeaders, transport, timeoutTimer, parts, state = 0, fireGlobals, i, jqXHR = {
                    readyState: 0, setRequestHeader: function (name, value) {
                        if (!state) { var lname = name.toLowerCase(); name = requestHeadersNames[lname] = requestHeadersNames[lname] || name; requestHeaders[name] = value; }
                        return this;
                    }, getAllResponseHeaders: function () { return state === 2 ? responseHeadersString : null; }, getResponseHeader: function (key) {
                        var match; if (state === 2) {
                            if (!responseHeaders) { responseHeaders = {}; while ((match = rheaders.exec(responseHeadersString))) { responseHeaders[match[1].toLowerCase()] = match[2]; } }
                            match = responseHeaders[key.toLowerCase()];
                        }
                        return match === undefined ? null : match;
                    }, overrideMimeType: function (type) {
                        if (!state) { s.mimeType = type; }
                        return this;
                    }, abort: function (statusText) {
                        statusText = statusText || "abort"; if (transport) { transport.abort(statusText); }
                        done(0, statusText); return this;
                    }
                }; function done(status, nativeStatusText, responses, headers) {
                    if (state === 2) { return; }
                    state = 2; if (timeoutTimer) { clearTimeout(timeoutTimer); }
                    transport = undefined; responseHeadersString = headers || ""; jqXHR.readyState = status > 0 ? 4 : 0; var isSuccess, success, error, statusText = nativeStatusText, response = responses ? ajaxHandleResponses(s, jqXHR, responses) : undefined, lastModified, etag; if (status >= 200 && status < 300 || status === 304) {
                        if (s.ifModified) {
                            if ((lastModified = jqXHR.getResponseHeader("Last-Modified"))) { jQuery.lastModified[ifModifiedKey] = lastModified; }
                            if ((etag = jqXHR.getResponseHeader("Etag"))) { jQuery.etag[ifModifiedKey] = etag; }
                        }
                        if (status === 304) { statusText = "notmodified"; isSuccess = true; } else { try { success = ajaxConvert(s, response); statusText = "success"; isSuccess = true; } catch (e) { statusText = "parsererror"; error = e; } }
                    } else { error = statusText; if (!statusText || status) { statusText = "error"; if (status < 0) { status = 0; } } }
                    jqXHR.status = status; jqXHR.statusText = "" + (nativeStatusText || statusText); if (isSuccess) { deferred.resolveWith(callbackContext, [success, statusText, jqXHR]); } else { deferred.rejectWith(callbackContext, [jqXHR, statusText, error]); }
                    jqXHR.statusCode(statusCode); statusCode = undefined; if (fireGlobals) { globalEventContext.trigger("ajax" + (isSuccess ? "Success" : "Error"), [jqXHR, s, isSuccess ? success : error]); }
                    completeDeferred.fireWith(callbackContext, [jqXHR, statusText]); if (fireGlobals) { globalEventContext.trigger("ajaxComplete", [jqXHR, s]); if (!(--jQuery.active)) { jQuery.event.trigger("ajaxStop"); } }
                }
                deferred.promise(jqXHR); jqXHR.success = jqXHR.done; jqXHR.error = jqXHR.fail; jqXHR.complete = completeDeferred.add; jqXHR.statusCode = function (map) {
                    if (map) { var tmp; if (state < 2) { for (tmp in map) { statusCode[tmp] = [statusCode[tmp], map[tmp]]; } } else { tmp = map[jqXHR.status]; jqXHR.then(tmp, tmp); } }
                    return this;
                };
                s.url = ((url || s.url) + "").replace(rhash, "").replace(rprotocol, ajaxLocParts[1] + "//"); s.dataTypes = jQuery.trim(s.dataType || "*").toLowerCase().split(rspacesAjax); if (s.crossDomain == null) { parts = rurl.exec(s.url.toLowerCase()); s.crossDomain = !!(parts && (parts[1] != ajaxLocParts[1] || parts[2] != ajaxLocParts[2] || (parts[3] || (parts[1] === "http:" ? 80 : 443)) != (ajaxLocParts[3] || (ajaxLocParts[1] === "http:" ? 80 : 443)))); }
                if (s.data && s.processData && typeof s.data !== "string") { s.data = jQuery.param(s.data, s.traditional); }
                inspectPrefiltersOrTransports(prefilters, s, options, jqXHR); if (state === 2) { return false; }
                fireGlobals = s.global; s.type = s.type.toUpperCase(); s.hasContent = !rnoContent.test(s.type); if (fireGlobals && jQuery.active++ === 0) { jQuery.event.trigger("ajaxStart"); }
                if (!s.hasContent) {
                    if (s.data) { s.url += (rquery.test(s.url) ? "&" : "?") + s.data; delete s.data; }
                    ifModifiedKey = s.url; if (s.cache === false) { var ts = jQuery.now(), ret = s.url.replace(rts, "$1_=" + ts); s.url = ret + ((ret === s.url) ? (rquery.test(s.url) ? "&" : "?") + "_=" + ts : ""); }
                }
                if (s.data && s.hasContent && s.contentType !== false || options.contentType) { jqXHR.setRequestHeader("Content-Type", s.contentType); }
                if (s.ifModified) {
                    ifModifiedKey = ifModifiedKey || s.url; if (jQuery.lastModified[ifModifiedKey]) { jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[ifModifiedKey]); }
                    if (jQuery.etag[ifModifiedKey]) { jqXHR.setRequestHeader("If-None-Match", jQuery.etag[ifModifiedKey]); }
                }
                jqXHR.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== "*" ? ", " + allTypes + "; q=0.01" : "") : s.accepts["*"]); for (i in s.headers) { jqXHR.setRequestHeader(i, s.headers[i]); }
                if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || state === 2)) { jqXHR.abort(); return false; }
                for (i in { success: 1, error: 1, complete: 1 }) { jqXHR[i](s[i]); }
                transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR); if (!transport) { done(-1, "No Transport"); } else {
                    jqXHR.readyState = 1; if (fireGlobals) { globalEventContext.trigger("ajaxSend", [jqXHR, s]); }
                    if (s.async && s.timeout > 0) { timeoutTimer = setTimeout(function () { jqXHR.abort("timeout"); }, s.timeout); }
                    try { state = 1; transport.send(requestHeaders, done); } catch (e) { if (state < 2) { done(-1, e); } else { throw e; } }
                }
                return jqXHR;
            }, param: function (a, traditional) {
                var s = [], add = function (key, value) { value = jQuery.isFunction(value) ? value() : value; s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value); }; if (traditional === undefined) { traditional = jQuery.ajaxSettings.traditional; }
                if (jQuery.isArray(a) || (a.jquery && !jQuery.isPlainObject(a))) { jQuery.each(a, function () { add(this.name, this.value); }); } else { for (var prefix in a) { buildParams(prefix, a[prefix], traditional, add); } }
                return s.join("&").replace(r20, "+");
            }
        }); function buildParams(prefix, obj, traditional, add) { if (jQuery.isArray(obj)) { jQuery.each(obj, function (i, v) { if (traditional || rbracket.test(prefix)) { add(prefix, v); } else { buildParams(prefix + "[" + (typeof v === "object" || jQuery.isArray(v) ? i : "") + "]", v, traditional, add); } }); } else if (!traditional && obj != null && typeof obj === "object") { for (var name in obj) { buildParams(prefix + "[" + name + "]", obj[name], traditional, add); } } else { add(prefix, obj); } }
        jQuery.extend({ active: 0, lastModified: {}, etag: {} }); function ajaxHandleResponses(s, jqXHR, responses) {
            var contents = s.contents, dataTypes = s.dataTypes, responseFields = s.responseFields, ct, type, finalDataType, firstDataType; for (type in responseFields) { if (type in responses) { jqXHR[responseFields[type]] = responses[type]; } }
            while (dataTypes[0] === "*") { dataTypes.shift(); if (ct === undefined) { ct = s.mimeType || jqXHR.getResponseHeader("content-type"); } }
            if (ct) { for (type in contents) { if (contents[type] && contents[type].test(ct)) { dataTypes.unshift(type); break; } } }
            if (dataTypes[0] in responses) { finalDataType = dataTypes[0]; } else {
                for (type in responses) {
                    if (!dataTypes[0] || s.converters[type + " " + dataTypes[0]]) { finalDataType = type; break; }
                    if (!firstDataType) { firstDataType = type; }
                }
                finalDataType = finalDataType || firstDataType;
            }
            if (finalDataType) {
                if (finalDataType !== dataTypes[0]) { dataTypes.unshift(finalDataType); }
                return responses[finalDataType];
            }
        }
        function ajaxConvert(s, response) {
            if (s.dataFilter) { response = s.dataFilter(response, s.dataType); }
            var dataTypes = s.dataTypes, converters = {}, i, key, length = dataTypes.length, tmp, current = dataTypes[0], prev, conversion, conv, conv1, conv2; for (i = 1; i < length; i++) {
                if (i === 1) { for (key in s.converters) { if (typeof key === "string") { converters[key.toLowerCase()] = s.converters[key]; } } }
                prev = current; current = dataTypes[i]; if (current === "*") { current = prev; } else if (prev !== "*" && prev !== current) {
                    conversion = prev + " " + current; conv = converters[conversion] || converters["* " + current]; if (!conv) {
                        conv2 = undefined; for (conv1 in converters) {
                            tmp = conv1.split(" "); if (tmp[0] === prev || tmp[0] === "*") {
                                conv2 = converters[tmp[1] + " " + current]; if (conv2) {
                                    conv1 = converters[conv1]; if (conv1 === true) { conv = conv2; } else if (conv2 === true) { conv = conv1; }
                                    break;
                                }
                            }
                        }
                    }
                    if (!(conv || conv2)) { jQuery.error("No conversion from " + conversion.replace(" ", " to ")); }
                    if (conv !== true) { response = conv ? conv(response) : conv2(conv1(response)); }
                }
            }
            return response;
        }
        var jsc = jQuery.now(), jsre = /(\=)\?(&|$)|\?\?/i; jQuery.ajaxSetup({ jsonp: "callback", jsonpCallback: function () { return jQuery.expando + "_" + (jsc++); } }); jQuery.ajaxPrefilter("json jsonp", function (s, originalSettings, jqXHR) {
            var inspectData = s.contentType === "application/x-www-form-urlencoded" && (typeof s.data === "string"); if (s.dataTypes[0] === "jsonp" || s.jsonp !== false && (jsre.test(s.url) || inspectData && jsre.test(s.data))) {
                var responseContainer, jsonpCallback = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback, previous = window[jsonpCallback], url = s.url, data = s.data, replace = "$1" + jsonpCallback + "$2"; if (s.jsonp !== false) {
                    url = url.replace(jsre, replace); if (s.url === url) {
                        if (inspectData) { data = data.replace(jsre, replace); }
                        if (s.data === data) { url += (/\?/.test(url) ? "&" : "?") + s.jsonp + "=" + jsonpCallback; }
                    }
                }
                s.url = url; s.data = data; window[jsonpCallback] = function (response) { responseContainer = [response]; }; jqXHR.always(function () { window[jsonpCallback] = previous; if (responseContainer && jQuery.isFunction(previous)) { window[jsonpCallback](responseContainer[0]); } }); s.converters["script json"] = function () {
                    if (!responseContainer) { jQuery.error(jsonpCallback + " was not called"); }
                    return responseContainer[0];
                }; s.dataTypes[0] = "json"; return "script";
            }
        }); jQuery.ajaxSetup({ accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" }, contents: { script: /javascript|ecmascript/ }, converters: { "text script": function (text) { jQuery.globalEval(text); return text; } } }); jQuery.ajaxPrefilter("script", function (s) {
            if (s.cache === undefined) { s.cache = false; }
            if (s.crossDomain) { s.type = "GET"; s.global = false; }
        }); jQuery.ajaxTransport("script", function (s) {
            if (s.crossDomain) {
                var script, head = document.head || document.getElementsByTagName("head")[0] || document.documentElement; return {
                    send: function (_, callback) {
                        script = document.createElement("script"); script.async = "async"; if (s.scriptCharset) { script.charset = s.scriptCharset; }
                        script.src = s.url; script.onload = script.onreadystatechange = function (_, isAbort) {
                            if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
                                script.onload = script.onreadystatechange = null; if (head && script.parentNode) { head.removeChild(script); }
                                script = undefined; if (!isAbort) { callback(200, "success"); }
                            }
                        }; head.insertBefore(script, head.firstChild);
                    }, abort: function () { if (script) { script.onload(0, 1); } }
                };
            }
        }); var xhrOnUnloadAbort = window.ActiveXObject ? function () { for (var key in xhrCallbacks) { xhrCallbacks[key](0, 1); } } : false, xhrId = 0, xhrCallbacks; function createStandardXHR() { try { return new window.XMLHttpRequest(); } catch (e) { } }
        function createActiveXHR() { try { return new window.ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { } }
        jQuery.ajaxSettings.xhr = window.ActiveXObject ? function () { return !this.isLocal && createStandardXHR() || createActiveXHR(); } : createStandardXHR; (function (xhr) { jQuery.extend(jQuery.support, { ajax: !!xhr, cors: !!xhr && ("withCredentials" in xhr) }); })(jQuery.ajaxSettings.xhr()); if (jQuery.support.ajax) {
            jQuery.ajaxTransport(function (s) {
                if (!s.crossDomain || jQuery.support.cors) {
                    var callback; return {
                        send: function (headers, complete) {
                            var xhr = s.xhr(), handle, i; if (s.username) { xhr.open(s.type, s.url, s.async, s.username, s.password); } else { xhr.open(s.type, s.url, s.async); }
                            if (s.xhrFields) { for (i in s.xhrFields) { xhr[i] = s.xhrFields[i]; } }
                            if (s.mimeType && xhr.overrideMimeType) { xhr.overrideMimeType(s.mimeType); }
                            if (!s.crossDomain && !headers["X-Requested-With"]) { headers["X-Requested-With"] = "XMLHttpRequest"; }
                            try { for (i in headers) { xhr.setRequestHeader(i, headers[i]); } } catch (_) { }
                            xhr.send((s.hasContent && s.data) || null); callback = function (_, isAbort) {
                                var status, statusText, responseHeaders, responses, xml; try {
                                    if (callback && (isAbort || xhr.readyState === 4)) {
                                        callback = undefined; if (handle) { xhr.onreadystatechange = jQuery.noop; if (xhrOnUnloadAbort) { delete xhrCallbacks[handle]; } }
                                        if (isAbort) { if (xhr.readyState !== 4) { xhr.abort(); } } else {
                                            status = xhr.status; responseHeaders = xhr.getAllResponseHeaders(); responses = {}; xml = xhr.responseXML; if (xml && xml.documentElement) { responses.xml = xml; }
                                            responses.text = xhr.responseText; try { statusText = xhr.statusText; } catch (e) { statusText = ""; }
                                            if (!status && s.isLocal && !s.crossDomain) { status = responses.text ? 200 : 404; } else if (status === 1223) { status = 204; }
                                        }
                                    }
                                } catch (firefoxAccessException) { if (!isAbort) { complete(-1, firefoxAccessException); } }
                                if (responses) { complete(status, statusText, responses, responseHeaders); }
                            };
                            if (!s.async || xhr.readyState === 4) { callback(); } else {
                                handle = ++xhrId; if (xhrOnUnloadAbort) {
                                    if (!xhrCallbacks) { xhrCallbacks = {}; jQuery(window).unload(xhrOnUnloadAbort); }
                                    xhrCallbacks[handle] = callback;
                                }
                                xhr.onreadystatechange = callback;
                            }
                        }, abort: function () { if (callback) { callback(0, 1); } }
                    };
                }
            });
        }
        var elemdisplay = {}, iframe, iframeDoc, rfxtypes = /^(?:toggle|show|hide)$/, rfxnum = /^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i, timerId, fxAttrs = [["height", "marginTop", "marginBottom", "paddingTop", "paddingBottom"], ["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"], ["opacity"]], fxNow; jQuery.fn.extend({
            show: function (speed, easing, callback) {
                var elem, display; if (speed || speed === 0) { return this.animate(genFx("show", 3), speed, easing, callback); } else {
                    for (var i = 0, j = this.length; i < j; i++) {
                        elem = this[i]; if (elem.style) {
                            display = elem.style.display; if (!jQuery._data(elem, "olddisplay") && display === "none") { display = elem.style.display = ""; }
                            if (display === "" && jQuery.css(elem, "display") === "none") { jQuery._data(elem, "olddisplay", defaultDisplay(elem.nodeName)); }
                        }
                    }
                    for (i = 0; i < j; i++) { elem = this[i]; if (elem.style) { display = elem.style.display; if (display === "" || display === "none") { elem.style.display = jQuery._data(elem, "olddisplay") || ""; } } }
                    return this;
                }
            }, hide: function (speed, easing, callback) {
                if (speed || speed === 0) { return this.animate(genFx("hide", 3), speed, easing, callback); } else {
                    var elem, display, i = 0, j = this.length; for (; i < j; i++) { elem = this[i]; if (elem.style) { display = jQuery.css(elem, "display"); if (display !== "none" && !jQuery._data(elem, "olddisplay")) { jQuery._data(elem, "olddisplay", display); } } }
                    for (i = 0; i < j; i++) { if (this[i].style) { this[i].style.display = "none"; } }
                    return this;
                }
            }, _toggle: jQuery.fn.toggle, toggle: function (fn, fn2, callback) {
                var bool = typeof fn === "boolean"; if (jQuery.isFunction(fn) && jQuery.isFunction(fn2)) { this._toggle.apply(this, arguments); } else if (fn == null || bool) { this.each(function () { var state = bool ? fn : jQuery(this).is(":hidden"); jQuery(this)[state ? "show" : "hide"](); }); } else { this.animate(genFx("toggle", 3), fn, fn2, callback); }
                return this;
            }, fadeTo: function (speed, to, easing, callback) { return this.filter(":hidden").css("opacity", 0).show().end().animate({ opacity: to }, speed, easing, callback); }, animate: function (prop, speed, easing, callback) {
                var optall = jQuery.speed(speed, easing, callback); if (jQuery.isEmptyObject(prop)) { return this.each(optall.complete, [false]); }
                prop = jQuery.extend({}, prop); function doAnimation() {
                    if (optall.queue === false) { jQuery._mark(this); }
                    var opt = jQuery.extend({}, optall), isElement = this.nodeType === 1, hidden = isElement && jQuery(this).is(":hidden"), name, val, p, e, parts, start, end, unit, method; opt.animatedProperties = {}; for (p in prop) {
                        name = jQuery.camelCase(p); if (p !== name) { prop[name] = prop[p]; delete prop[p]; }
                        val = prop[name]; if (jQuery.isArray(val)) { opt.animatedProperties[name] = val[1]; val = prop[name] = val[0]; } else { opt.animatedProperties[name] = opt.specialEasing && opt.specialEasing[name] || opt.easing || 'swing'; }
                        if (val === "hide" && hidden || val === "show" && !hidden) { return opt.complete.call(this); }
                        if (isElement && (name === "height" || name === "width")) { opt.overflow = [this.style.overflow, this.style.overflowX, this.style.overflowY]; if (jQuery.css(this, "display") === "inline" && jQuery.css(this, "float") === "none") { if (!jQuery.support.inlineBlockNeedsLayout || defaultDisplay(this.nodeName) === "inline") { this.style.display = "inline-block"; } else { this.style.zoom = 1; } } }
                    }
                    if (opt.overflow != null) { this.style.overflow = "hidden"; }
                    for (p in prop) {
                        e = new jQuery.fx(this, opt, p); val = prop[p]; if (rfxtypes.test(val)) { method = jQuery._data(this, "toggle" + p) || (val === "toggle" ? hidden ? "show" : "hide" : 0); if (method) { jQuery._data(this, "toggle" + p, method === "show" ? "hide" : "show"); e[method](); } else { e[val](); } } else {
                            parts = rfxnum.exec(val); start = e.cur(); if (parts) {
                                end = parseFloat(parts[2]); unit = parts[3] || (jQuery.cssNumber[p] ? "" : "px"); if (unit !== "px") { jQuery.style(this, p, (end || 1) + unit); start = ((end || 1) / e.cur()) * start; jQuery.style(this, p, start + unit); }
                                if (parts[1]) { end = ((parts[1] === "-=" ? -1 : 1) * end) + start; }
                                e.custom(start, end, unit);
                            } else { e.custom(start, val, ""); }
                        }
                    }
                    return true;
                }
                return optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
            }, stop: function (type, clearQueue, gotoEnd) {
                if (typeof type !== "string") { gotoEnd = clearQueue; clearQueue = type; type = undefined; }
                if (clearQueue && type !== false) { this.queue(type || "fx", []); }
                return this.each(function () {
                    var index, hadTimers = false, timers = jQuery.timers, data = jQuery._data(this); if (!gotoEnd) { jQuery._unmark(true, this); }
                    function stopQueue(elem, data, index) { var hooks = data[index]; jQuery.removeData(elem, index, true); hooks.stop(gotoEnd); }
                    if (type == null) { for (index in data) { if (data[index] && data[index].stop && index.indexOf(".run") === index.length - 4) { stopQueue(this, data, index); } } } else if (data[index = type + ".run"] && data[index].stop) { stopQueue(this, data, index); }
                    for (index = timers.length; index--;) {
                        if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
                            if (gotoEnd) { timers[index](true); } else { timers[index].saveState(); }
                            hadTimers = true; timers.splice(index, 1);
                        }
                    }
                    if (!(gotoEnd && hadTimers)) { jQuery.dequeue(this, type); }
                });
            }
        }); function createFxNow() { setTimeout(clearFxNow, 0); return (fxNow = jQuery.now()); }
        function clearFxNow() { fxNow = undefined; }
        function genFx(type, num) { var obj = {}; jQuery.each(fxAttrs.concat.apply([], fxAttrs.slice(0, num)), function () { obj[this] = type; }); return obj; }
        jQuery.each({ slideDown: genFx("show", 1), slideUp: genFx("hide", 1), slideToggle: genFx("toggle", 1), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function (name, props) { jQuery.fn[name] = function (speed, easing, callback) { return this.animate(props, speed, easing, callback); }; }); jQuery.extend({
            speed: function (speed, easing, fn) {
                var opt = speed && typeof speed === "object" ? jQuery.extend({}, speed) : { complete: fn || !fn && easing || jQuery.isFunction(speed) && speed, duration: speed, easing: fn && easing || easing && !jQuery.isFunction(easing) && easing }; opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration : opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[opt.duration] : jQuery.fx.speeds._default; if (opt.queue == null || opt.queue === true) { opt.queue = "fx"; }
                opt.old = opt.complete; opt.complete = function (noUnmark) {
                    if (jQuery.isFunction(opt.old)) { opt.old.call(this); }
                    if (opt.queue) { jQuery.dequeue(this, opt.queue); } else if (noUnmark !== false) { jQuery._unmark(this); }
                }; return opt;
            }, easing: { linear: function (p, n, firstNum, diff) { return firstNum + diff * p; }, swing: function (p, n, firstNum, diff) { return ((-Math.cos(p * Math.PI) / 2) + 0.5) * diff + firstNum; } }, timers: [], fx: function (elem, options, prop) { this.options = options; this.elem = elem; this.prop = prop; options.orig = options.orig || {}; }
        }); jQuery.fx.prototype = {
            update: function () {
                if (this.options.step) { this.options.step.call(this.elem, this.now, this); }
                (jQuery.fx.step[this.prop] || jQuery.fx.step._default)(this);
            }, cur: function () {
                if (this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null)) { return this.elem[this.prop]; }
                var parsed, r = jQuery.css(this.elem, this.prop); return isNaN(parsed = parseFloat(r)) ? !r || r === "auto" ? 0 : r : parsed;
            }, custom: function (from, to, unit) {
                var self = this, fx = jQuery.fx; this.startTime = fxNow || createFxNow(); this.end = to; this.now = this.start = from; this.pos = this.state = 0; this.unit = unit || this.unit || (jQuery.cssNumber[this.prop] ? "" : "px"); function t(gotoEnd) { return self.step(gotoEnd); }
                t.queue = this.options.queue; t.elem = this.elem; t.saveState = function () { if (self.options.hide && jQuery._data(self.elem, "fxshow" + self.prop) === undefined) { jQuery._data(self.elem, "fxshow" + self.prop, self.start); } }; if (t() && jQuery.timers.push(t) && !timerId) { timerId = setInterval(fx.tick, fx.interval); }
            }, show: function () {
                var dataShow = jQuery._data(this.elem, "fxshow" + this.prop); this.options.orig[this.prop] = dataShow || jQuery.style(this.elem, this.prop); this.options.show = true; if (dataShow !== undefined) { this.custom(this.cur(), dataShow); } else { this.custom(this.prop === "width" || this.prop === "height" ? 1 : 0, this.cur()); }
                jQuery(this.elem).show();
            }, hide: function () { this.options.orig[this.prop] = jQuery._data(this.elem, "fxshow" + this.prop) || jQuery.style(this.elem, this.prop); this.options.hide = true; this.custom(this.cur(), 0); }, step: function (gotoEnd) {
                var p, n, complete, t = fxNow || createFxNow(), done = true, elem = this.elem, options = this.options; if (gotoEnd || t >= options.duration + this.startTime) {
                    this.now = this.end; this.pos = this.state = 1; this.update(); options.animatedProperties[this.prop] = true; for (p in options.animatedProperties) { if (options.animatedProperties[p] !== true) { done = false; } }
                    if (done) {
                        if (options.overflow != null && !jQuery.support.shrinkWrapBlocks) { jQuery.each(["", "X", "Y"], function (index, value) { elem.style["overflow" + value] = options.overflow[index]; }); }
                        if (options.hide) { jQuery(elem).hide(); }
                        if (options.hide || options.show) { for (p in options.animatedProperties) { jQuery.style(elem, p, options.orig[p]); jQuery.removeData(elem, "fxshow" + p, true); jQuery.removeData(elem, "toggle" + p, true); } }
                        complete = options.complete; if (complete) { options.complete = false; complete.call(elem); }
                    }
                    return false;
                } else {
                    if (options.duration == Infinity) { this.now = t; } else { n = t - this.startTime; this.state = n / options.duration; this.pos = jQuery.easing[options.animatedProperties[this.prop]](this.state, n, 0, 1, options.duration); this.now = this.start + ((this.end - this.start) * this.pos); }
                    this.update();
                }
                return true;
            }
        }; jQuery.extend(jQuery.fx, {
            tick: function () {
                var timer, timers = jQuery.timers, i = 0; for (; i < timers.length; i++) { timer = timers[i]; if (!timer() && timers[i] === timer) { timers.splice(i--, 1); } }
                if (!timers.length) { jQuery.fx.stop(); }
            }, interval: 13, stop: function () { clearInterval(timerId); timerId = null; }, speeds: { slow: 600, fast: 200, _default: 400 }, step: { opacity: function (fx) { jQuery.style(fx.elem, "opacity", fx.now); }, _default: function (fx) { if (fx.elem.style && fx.elem.style[fx.prop] != null) { fx.elem.style[fx.prop] = fx.now + fx.unit; } else { fx.elem[fx.prop] = fx.now; } } }
        }); jQuery.each(["width", "height"], function (i, prop) { jQuery.fx.step[prop] = function (fx) { jQuery.style(fx.elem, prop, Math.max(0, fx.now) + fx.unit); }; }); if (jQuery.expr && jQuery.expr.filters) { jQuery.expr.filters.animated = function (elem) { return jQuery.grep(jQuery.timers, function (fn) { return elem === fn.elem; }).length; }; }
        function defaultDisplay(nodeName) {
            if (!elemdisplay[nodeName]) {
                var body = document.body, elem = jQuery("<" + nodeName + ">").appendTo(body), display = elem.css("display"); elem.remove(); if (display === "none" || display === "") {
                    if (!iframe) { iframe = document.createElement("iframe"); iframe.frameBorder = iframe.width = iframe.height = 0; }
                    body.appendChild(iframe); if (!iframeDoc || !iframe.createElement) { iframeDoc = (iframe.contentWindow || iframe.contentDocument).document; iframeDoc.write((document.compatMode === "CSS1Compat" ? "<!doctype html>" : "") + "<html><body>"); iframeDoc.close(); }
                    elem = iframeDoc.createElement(nodeName); iframeDoc.body.appendChild(elem); display = jQuery.css(elem, "display"); body.removeChild(iframe);
                }
                elemdisplay[nodeName] = display;
            }
            return elemdisplay[nodeName];
        }
        var rtable = /^t(?:able|d|h)$/i, rroot = /^(?:body|html)$/i; if ("getBoundingClientRect" in document.documentElement) {
            jQuery.fn.offset = function (options) {
                var elem = this[0], box; if (options) { return this.each(function (i) { jQuery.offset.setOffset(this, options, i); }); }
                if (!elem || !elem.ownerDocument) { return null; }
                if (elem === elem.ownerDocument.body) { return jQuery.offset.bodyOffset(elem); }
                try { box = elem.getBoundingClientRect(); } catch (e) { }
                var doc = elem.ownerDocument, docElem = doc.documentElement; if (!box || !jQuery.contains(docElem, elem)) { return box ? { top: box.top, left: box.left } : { top: 0, left: 0 }; }
                var body = doc.body, win = getWindow(doc), clientTop = docElem.clientTop || body.clientTop || 0, clientLeft = docElem.clientLeft || body.clientLeft || 0, scrollTop = win.pageYOffset || jQuery.support.boxModel && docElem.scrollTop || body.scrollTop, scrollLeft = win.pageXOffset || jQuery.support.boxModel && docElem.scrollLeft || body.scrollLeft, top = box.top + scrollTop - clientTop, left = box.left + scrollLeft - clientLeft; return { top: top, left: left };
            };
        } else {
            jQuery.fn.offset = function (options) {
                var elem = this[0]; if (options) { return this.each(function (i) { jQuery.offset.setOffset(this, options, i); }); }
                if (!elem || !elem.ownerDocument) { return null; }
                if (elem === elem.ownerDocument.body) { return jQuery.offset.bodyOffset(elem); }
                var computedStyle, offsetParent = elem.offsetParent, prevOffsetParent = elem, doc = elem.ownerDocument, docElem = doc.documentElement, body = doc.body, defaultView = doc.defaultView, prevComputedStyle = defaultView ? defaultView.getComputedStyle(elem, null) : elem.currentStyle, top = elem.offsetTop, left = elem.offsetLeft; while ((elem = elem.parentNode) && elem !== body && elem !== docElem) {
                    if (jQuery.support.fixedPosition && prevComputedStyle.position === "fixed") { break; }
                    computedStyle = defaultView ? defaultView.getComputedStyle(elem, null) : elem.currentStyle; top -= elem.scrollTop; left -= elem.scrollLeft; if (elem === offsetParent) {
                        top += elem.offsetTop; left += elem.offsetLeft; if (jQuery.support.doesNotAddBorder && !(jQuery.support.doesAddBorderForTableAndCells && rtable.test(elem.nodeName))) { top += parseFloat(computedStyle.borderTopWidth) || 0; left += parseFloat(computedStyle.borderLeftWidth) || 0; }
                        prevOffsetParent = offsetParent; offsetParent = elem.offsetParent;
                    }
                    if (jQuery.support.subtractsBorderForOverflowNotVisible && computedStyle.overflow !== "visible") { top += parseFloat(computedStyle.borderTopWidth) || 0; left += parseFloat(computedStyle.borderLeftWidth) || 0; }
                    prevComputedStyle = computedStyle;
                }
                if (prevComputedStyle.position === "relative" || prevComputedStyle.position === "static") { top += body.offsetTop; left += body.offsetLeft; }
                if (jQuery.support.fixedPosition && prevComputedStyle.position === "fixed") { top += Math.max(docElem.scrollTop, body.scrollTop); left += Math.max(docElem.scrollLeft, body.scrollLeft); }
                return { top: top, left: left };
            };
        }
        jQuery.offset = {
            bodyOffset: function (body) {
                var top = body.offsetTop, left = body.offsetLeft; if (jQuery.support.doesNotIncludeMarginInBodyOffset) { top += parseFloat(jQuery.css(body, "marginTop")) || 0; left += parseFloat(jQuery.css(body, "marginLeft")) || 0; }
                return { top: top, left: left };
            }, setOffset: function (elem, options, i) {
                var position = jQuery.css(elem, "position"); if (position === "static") { elem.style.position = "relative"; }
                var curElem = jQuery(elem), curOffset = curElem.offset(), curCSSTop = jQuery.css(elem, "top"), curCSSLeft = jQuery.css(elem, "left"), calculatePosition = (position === "absolute" || position === "fixed") && jQuery.inArray("auto", [curCSSTop, curCSSLeft]) > -1, props = {}, curPosition = {}, curTop, curLeft; if (calculatePosition) { curPosition = curElem.position(); curTop = curPosition.top; curLeft = curPosition.left; } else { curTop = parseFloat(curCSSTop) || 0; curLeft = parseFloat(curCSSLeft) || 0; }
                if (jQuery.isFunction(options)) { options = options.call(elem, i, curOffset); }
                if (options.top != null) { props.top = (options.top - curOffset.top) + curTop; }
                if (options.left != null) { props.left = (options.left - curOffset.left) + curLeft; }
                if ("using" in options) { options.using.call(elem, props); } else { curElem.css(props); }
            }
        }; jQuery.fn.extend({
            position: function () {
                if (!this[0]) { return null; }
                var elem = this[0], offsetParent = this.offsetParent(), offset = this.offset(), parentOffset = rroot.test(offsetParent[0].nodeName) ? { top: 0, left: 0 } : offsetParent.offset(); offset.top -= parseFloat(jQuery.css(elem, "marginTop")) || 0; offset.left -= parseFloat(jQuery.css(elem, "marginLeft")) || 0; parentOffset.top += parseFloat(jQuery.css(offsetParent[0], "borderTopWidth")) || 0; parentOffset.left += parseFloat(jQuery.css(offsetParent[0], "borderLeftWidth")) || 0; return { top: offset.top - parentOffset.top, left: offset.left - parentOffset.left };
            }, offsetParent: function () {
                return this.map(function () {
                    var offsetParent = this.offsetParent || document.body; while (offsetParent && (!rroot.test(offsetParent.nodeName) && jQuery.css(offsetParent, "position") === "static")) { offsetParent = offsetParent.offsetParent; }
                    return offsetParent;
                });
            }
        }); jQuery.each(["Left", "Top"], function (i, name) {
            var method = "scroll" + name; jQuery.fn[method] = function (val) {
                var elem, win; if (val === undefined) {
                    elem = this[0]; if (!elem) { return null; }
                    win = getWindow(elem); return win ? ("pageXOffset" in win) ? win[i ? "pageYOffset" : "pageXOffset"] : jQuery.support.boxModel && win.document.documentElement[method] || win.document.body[method] : elem[method];
                }
                return this.each(function () { win = getWindow(this); if (win) { win.scrollTo(!i ? val : jQuery(win).scrollLeft(), i ? val : jQuery(win).scrollTop()); } else { this[method] = val; } });
            };
        }); function getWindow(elem) { return jQuery.isWindow(elem) ? elem : elem.nodeType === 9 ? elem.defaultView || elem.parentWindow : false; }
        jQuery.each(["Height", "Width"], function (i, name) {
            var type = name.toLowerCase(); jQuery.fn["inner" + name] = function () { var elem = this[0]; return elem ? elem.style ? parseFloat(jQuery.css(elem, type, "padding")) : this[type]() : null; }; jQuery.fn["outer" + name] = function (margin) { var elem = this[0]; return elem ? elem.style ? parseFloat(jQuery.css(elem, type, margin ? "margin" : "border")) : this[type]() : null; }; jQuery.fn[type] = function (size) {
                var elem = this[0]; if (!elem) { return size == null ? null : this; }
                if (jQuery.isFunction(size)) { return this.each(function (i) { var self = jQuery(this); self[type](size.call(this, i, self[type]())); }); }
                if (jQuery.isWindow(elem)) { var docElemProp = elem.document.documentElement["client" + name], body = elem.document.body; return elem.document.compatMode === "CSS1Compat" && docElemProp || body && body["client" + name] || docElemProp; } else if (elem.nodeType === 9) { return Math.max(elem.documentElement["client" + name], elem.body["scroll" + name], elem.documentElement["scroll" + name], elem.body["offset" + name], elem.documentElement["offset" + name]); } else if (size === undefined) { var orig = jQuery.css(elem, type), ret = parseFloat(orig); return jQuery.isNumeric(ret) ? ret : orig; } else { return this.css(type, typeof size === "string" ? size : size + "px"); }
            };
        }); window.jQuery = window.$ = jQuery; if (typeof define === "function" && define.amd && define.amd.jQuery) { define("jquery", [], function () { return jQuery; }); }
    })(window); (function ($, undefined) {
        $.ui = $.ui || {}; if ($.ui.version) { return; }
        $.extend($.ui, { version: "1.8.16", keyCode: { ALT: 18, BACKSPACE: 8, CAPS_LOCK: 20, COMMA: 188, COMMAND: 91, COMMAND_LEFT: 91, COMMAND_RIGHT: 93, CONTROL: 17, DELETE: 46, DOWN: 40, END: 35, ENTER: 13, ESCAPE: 27, HOME: 36, INSERT: 45, LEFT: 37, MENU: 93, NUMPAD_ADD: 107, NUMPAD_DECIMAL: 110, NUMPAD_DIVIDE: 111, NUMPAD_ENTER: 108, NUMPAD_MULTIPLY: 106, NUMPAD_SUBTRACT: 109, PAGE_DOWN: 34, PAGE_UP: 33, PERIOD: 190, RIGHT: 39, SHIFT: 16, SPACE: 32, TAB: 9, UP: 38, WINDOWS: 91 } }); $.fn.extend({
            propAttr: $.fn.prop || $.fn.attr, _focus: $.fn.focus, focus: function (delay, fn) { return typeof delay === "number" ? this.each(function () { var elem = this; setTimeout(function () { $(elem).focus(); if (fn) { fn.call(elem); } }, delay); }) : this._focus.apply(this, arguments); }, scrollParent: function () {
                var scrollParent; if (($.browser.msie && (/(static|relative)/).test(this.css('position'))) || (/absolute/).test(this.css('position'))) { scrollParent = this.parents().filter(function () { return (/(relative|absolute|fixed)/).test($.curCSS(this, 'position', 1)) && (/(auto|scroll)/).test($.curCSS(this, 'overflow', 1) + $.curCSS(this, 'overflow-y', 1) + $.curCSS(this, 'overflow-x', 1)); }).eq(0); } else { scrollParent = this.parents().filter(function () { return (/(auto|scroll)/).test($.curCSS(this, 'overflow', 1) + $.curCSS(this, 'overflow-y', 1) + $.curCSS(this, 'overflow-x', 1)); }).eq(0); }
                return (/fixed/).test(this.css('position')) || !scrollParent.length ? $(document) : scrollParent;
            }, zIndex: function (zIndex) {
                if (zIndex !== undefined) { return this.css("zIndex", zIndex); }
                if (this.length) {
                    var elem = $(this[0]), position, value; while (elem.length && elem[0] !== document) {
                        position = elem.css("position"); if (position === "absolute" || position === "relative" || position === "fixed") { value = parseInt(elem.css("zIndex"), 10); if (!isNaN(value) && value !== 0) { return value; } }
                        elem = elem.parent();
                    }
                }
                return 0;
            }, disableSelection: function () { return this.bind(($.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function (event) { event.preventDefault(); }); }, enableSelection: function () { return this.unbind(".ui-disableSelection"); }
        }); $.each(["Width", "Height"], function (i, name) {
            var side = name === "Width" ? ["Left", "Right"] : ["Top", "Bottom"], type = name.toLowerCase(), orig = { innerWidth: $.fn.innerWidth, innerHeight: $.fn.innerHeight, outerWidth: $.fn.outerWidth, outerHeight: $.fn.outerHeight }; function reduce(elem, size, border, margin) {
                $.each(side, function () {
                    size -= parseFloat($.curCSS(elem, "padding" + this, true)) || 0; if (border) { size -= parseFloat($.curCSS(elem, "border" + this + "Width", true)) || 0; }
                    if (margin) { size -= parseFloat($.curCSS(elem, "margin" + this, true)) || 0; }
                }); return size;
            }
            $.fn["inner" + name] = function (size) {
                if (size === undefined) { return orig["inner" + name].call(this); }
                return this.each(function () { $(this).css(type, reduce(this, size) + "px"); });
            }; $.fn["outer" + name] = function (size, margin) {
                if (typeof size !== "number") { return orig["outer" + name].call(this, size); }
                return this.each(function () { $(this).css(type, reduce(this, size, true, margin) + "px"); });
            };
        }); function focusable(element, isTabIndexNotNaN) {
            var nodeName = element.nodeName.toLowerCase(); if ("area" === nodeName) {
                var map = element.parentNode, mapName = map.name, img; if (!element.href || !mapName || map.nodeName.toLowerCase() !== "map") { return false; }
                img = $("img[usemap=#" + mapName + "]")[0]; return !!img && visible(img);
            }
            return (/input|select|textarea|button|object/.test(nodeName) ? !element.disabled : "a" == nodeName ? element.href || isTabIndexNotNaN : isTabIndexNotNaN)
            && visible(element);
        }
        function visible(element) { return !$(element).parents().andSelf().filter(function () { return $.curCSS(this, "visibility") === "hidden" || $.expr.filters.hidden(this); }).length; }
        $.extend($.expr[":"], { data: function (elem, i, match) { return !!$.data(elem, match[3]); }, focusable: function (element) { return focusable(element, !isNaN($.attr(element, "tabindex"))); }, tabbable: function (element) { var tabIndex = $.attr(element, "tabindex"), isTabIndexNaN = isNaN(tabIndex); return (isTabIndexNaN || tabIndex >= 0) && focusable(element, !isTabIndexNaN); } }); $(function () { var body = document.body, div = body.appendChild(div = document.createElement("div")); $.extend(div.style, { minHeight: "100px", height: "auto", padding: 0, borderWidth: 0 }); $.support.minHeight = div.offsetHeight === 100; $.support.selectstart = "onselectstart" in div; body.removeChild(div).style.display = "none"; }); $.extend($.ui, {
            plugin: {
                add: function (module, option, set) { var proto = $.ui[module].prototype; for (var i in set) { proto.plugins[i] = proto.plugins[i] || []; proto.plugins[i].push([option, set[i]]); } }, call: function (instance, name, args) {
                    var set = instance.plugins[name]; if (!set || !instance.element[0].parentNode) { return; }
                    for (var i = 0; i < set.length; i++) { if (instance.options[set[i][0]]) { set[i][1].apply(instance.element, args); } }
                }
            }, contains: function (a, b) { return document.compareDocumentPosition ? a.compareDocumentPosition(b) & 16 : a !== b && a.contains(b); }, hasScroll: function (el, a) {
                if ($(el).css("overflow") === "hidden") { return false; }
                var scroll = (a && a === "left") ? "scrollLeft" : "scrollTop", has = false; if (el[scroll] > 0) { return true; }
                el[scroll] = 1; has = (el[scroll] > 0); el[scroll] = 0; return has;
            }, isOverAxis: function (x, reference, size) { return (x > reference) && (x < (reference + size)); }, isOver: function (y, x, top, left, height, width) { return $.ui.isOverAxis(y, top, height) && $.ui.isOverAxis(x, left, width); }
        });
    })(jQuery); (function ($, undefined) {
        if ($.cleanData) {
            var _cleanData = $.cleanData; $.cleanData = function (elems) {
                for (var i = 0, elem; (elem = elems[i]) != null; i++) { try { $(elem).triggerHandler("remove"); } catch (e) { } }
                _cleanData(elems);
            };
        } else {
            var _remove = $.fn.remove; $.fn.remove = function (selector, keepData) {
                return this.each(function () {
                    if (!keepData) { if (!selector || $.filter(selector, [this]).length) { $("*", this).add([this]).each(function () { try { $(this).triggerHandler("remove"); } catch (e) { } }); } }
                    return _remove.call($(this), selector, keepData);
                });
            };
        }
        $.widget = function (name, base, prototype) {
            var namespace = name.split(".")[0], fullName; name = name.split(".")[1]; fullName = namespace + "-" + name; if (!prototype) { prototype = base; base = $.Widget; }
            $.expr[":"][fullName] = function (elem) { return !!$.data(elem, name); }; $[namespace] = $[namespace] || {}; $[namespace][name] = function (options, element) { if (arguments.length) { this._createWidget(options, element); } }; var basePrototype = new base();
            basePrototype.options = $.extend(true, {}, basePrototype.options); $[namespace][name].prototype = $.extend(true, basePrototype, { namespace: namespace, widgetName: name, widgetEventPrefix: $[namespace][name].prototype.widgetEventPrefix || name, widgetBaseClass: fullName }, prototype); $.widget.bridge(name, $[namespace][name]);
        }; $.widget.bridge = function (name, object) {
            $.fn[name] = function (options) {
                var isMethodCall = typeof options === "string", args = Array.prototype.slice.call(arguments, 1), returnValue = this; options = !isMethodCall && args.length ? $.extend.apply(null, [true, options].concat(args)) : options; if (isMethodCall && options.charAt(0) === "_") { return returnValue; }
                if (isMethodCall) {
                    this.each(function () {
                        var instance = $.data(this, name), methodValue = instance && $.isFunction(instance[options]) ? instance[options].apply(instance, args) : instance;
                        if (methodValue !== instance && methodValue !== undefined) { returnValue = methodValue; return false; }
                    });
                } else { this.each(function () { var instance = $.data(this, name); if (instance) { instance.option(options || {})._init(); } else { $.data(this, name, new object(options, this)); } }); }
                return returnValue;
            };
        }; $.Widget = function (options, element) { if (arguments.length) { this._createWidget(options, element); } }; $.Widget.prototype = {
            widgetName: "widget", widgetEventPrefix: "", options: { disabled: false }, _createWidget: function (options, element) { $.data(element, this.widgetName, this); this.element = $(element); this.options = $.extend(true, {}, this.options, this._getCreateOptions(), options); var self = this; this.element.bind("remove." + this.widgetName, function () { self.destroy(); }); this._create(); this._trigger("create"); this._init(); }, _getCreateOptions: function () { return $.metadata && $.metadata.get(this.element[0])[this.widgetName]; }, _create: function () { }, _init: function () { }, destroy: function () { this.element.unbind("." + this.widgetName).removeData(this.widgetName); this.widget().unbind("." + this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass + "-disabled " + "ui-state-disabled"); }, widget: function () { return this.element; }, option: function (key, value) {
                var options = key; if (arguments.length === 0) { return $.extend({}, this.options); }
                if (typeof key === "string") {
                    if (value === undefined) { return this.options[key]; }
                    options = {}; options[key] = value;
                }
                this._setOptions(options); return this;
            }, _setOptions: function (options) { var self = this; $.each(options, function (key, value) { self._setOption(key, value); }); return this; }, _setOption: function (key, value) {
                this.options[key] = value; if (key === "disabled") {
                    this.widget()
                    [value ? "addClass" : "removeClass"](this.widgetBaseClass + "-disabled" + " " + "ui-state-disabled").attr("aria-disabled", value);
                }
                return this;
            }, enable: function () { return this._setOption("disabled", false); }, disable: function () { return this._setOption("disabled", true); }, _trigger: function (type, event, data) {
                var callback = this.options[type]; event = $.Event(event); event.type = (type === this.widgetEventPrefix ? type : this.widgetEventPrefix + type).toLowerCase(); data = data || {}; if (event.originalEvent) { for (var i = $.event.props.length, prop; i;) { prop = $.event.props[--i]; event[prop] = event.originalEvent[prop]; } }
                this.element.trigger(event, data); return !($.isFunction(callback) && callback.call(this.element[0], event, data) === false || event.isDefaultPrevented());
            }
        };
    })(jQuery); (function ($, undefined) {
        var mouseHandled = false; $(document).mouseup(function (e) { mouseHandled = false; }); $.widget("ui.mouse", {
            options: { cancel: ':input,option', distance: 1, delay: 0 }, _mouseInit: function () { var self = this; this.element.bind('mousedown.' + this.widgetName, function (event) { return self._mouseDown(event); }).bind('click.' + this.widgetName, function (event) { if (true === $.data(event.target, self.widgetName + '.preventClickEvent')) { $.removeData(event.target, self.widgetName + '.preventClickEvent'); event.stopImmediatePropagation(); return false; } }); this.started = false; }, _mouseDestroy: function () { this.element.unbind('.' + this.widgetName); }, _mouseDown: function (event) {
                if (mouseHandled) { return }; (this._mouseStarted && this._mouseUp(event)); this._mouseDownEvent = event; var self = this, btnIsLeft = (event.which == 1), elIsCancel = (typeof this.options.cancel == "string" && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : false); if (!btnIsLeft || elIsCancel || !this._mouseCapture(event)) { return true; }
                this.mouseDelayMet = !this.options.delay; if (!this.mouseDelayMet) { this._mouseDelayTimer = setTimeout(function () { self.mouseDelayMet = true; }, this.options.delay); }
                if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) { this._mouseStarted = (this._mouseStart(event) !== false); if (!this._mouseStarted) { event.preventDefault(); return true; } }
                if (true === $.data(event.target, this.widgetName + '.preventClickEvent')) { $.removeData(event.target, this.widgetName + '.preventClickEvent'); }
                this._mouseMoveDelegate = function (event) { return self._mouseMove(event); }; this._mouseUpDelegate = function (event) { return self._mouseUp(event); }; $(document).bind('mousemove.' + this.widgetName, this._mouseMoveDelegate).bind('mouseup.' + this.widgetName, this._mouseUpDelegate); event.preventDefault(); mouseHandled = true; return true;
            }, _mouseMove: function (event) {
                if ($.browser.msie && !(document.documentMode >= 9) && !event.button) { return this._mouseUp(event); }
                if (this._mouseStarted) { this._mouseDrag(event); return event.preventDefault(); }
                if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) { this._mouseStarted = (this._mouseStart(this._mouseDownEvent, event) !== false); (this._mouseStarted ? this._mouseDrag(event) : this._mouseUp(event)); }
                return !this._mouseStarted;
            }, _mouseUp: function (event) {
                $(document).unbind('mousemove.' + this.widgetName, this._mouseMoveDelegate).unbind('mouseup.' + this.widgetName, this._mouseUpDelegate); if (this._mouseStarted) {
                    this._mouseStarted = false; if (event.target == this._mouseDownEvent.target) { $.data(event.target, this.widgetName + '.preventClickEvent', true); }
                    this._mouseStop(event);
                }
                return false;
            }, _mouseDistanceMet: function (event) { return (Math.max(Math.abs(this._mouseDownEvent.pageX - event.pageX), Math.abs(this._mouseDownEvent.pageY - event.pageY)) >= this.options.distance); }, _mouseDelayMet: function (event) { return this.mouseDelayMet; }, _mouseStart: function (event) { }, _mouseDrag: function (event) { }, _mouseStop: function (event) { }, _mouseCapture: function (event) { return true; }
        });
    })(jQuery); (function ($, undefined) {
        $.ui = $.ui || {}; var horizontalPositions = /left|center|right/, verticalPositions = /top|center|bottom/, center = "center", _position = $.fn.position, _offset = $.fn.offset; $.fn.position = function (options) {
            if (!options || !options.of) { return _position.apply(this, arguments); }
            options = $.extend({}, options); var target = $(options.of), targetElem = target[0], collision = (options.collision || "flip").split(" "), offset = options.offset ? options.offset.split(" ") : [0, 0], targetWidth, targetHeight, basePosition; if (targetElem.nodeType === 9) { targetWidth = target.width(); targetHeight = target.height(); basePosition = { top: 0, left: 0 }; } else if (targetElem.setTimeout) { targetWidth = target.width(); targetHeight = target.height(); basePosition = { top: target.scrollTop(), left: target.scrollLeft() }; } else if (targetElem.preventDefault) { options.at = "left top"; targetWidth = targetHeight = 0; basePosition = { top: options.of.pageY, left: options.of.pageX }; } else { targetWidth = target.outerWidth(); targetHeight = target.outerHeight(); basePosition = target.offset(); }
            $.each(["my", "at"], function () {
                var pos = (options[this] || "").split(" "); if (pos.length === 1) { pos = horizontalPositions.test(pos[0]) ? pos.concat([center]) : verticalPositions.test(pos[0]) ? [center].concat(pos) : [center, center]; }
                pos[0] = horizontalPositions.test(pos[0]) ? pos[0] : center; pos[1] = verticalPositions.test(pos[1]) ? pos[1] : center; options[this] = pos;
            }); if (collision.length === 1) { collision[1] = collision[0]; }
            offset[0] = parseInt(offset[0], 10) || 0; if (offset.length === 1) { offset[1] = offset[0]; }
            offset[1] = parseInt(offset[1], 10) || 0; if (options.at[0] === "right") { basePosition.left += targetWidth; } else if (options.at[0] === center) { basePosition.left += targetWidth / 2; }
            if (options.at[1] === "bottom") { basePosition.top += targetHeight; } else if (options.at[1] === center) { basePosition.top += targetHeight / 2; }
            basePosition.left += offset[0]; basePosition.top += offset[1]; return this.each(function () {
                var elem = $(this), elemWidth = elem.outerWidth(), elemHeight = elem.outerHeight(), marginLeft = parseInt($.curCSS(this, "marginLeft", true)) || 0, marginTop = parseInt($.curCSS(this, "marginTop", true)) || 0, collisionWidth = elemWidth + marginLeft +
                (parseInt($.curCSS(this, "marginRight", true)) || 0), collisionHeight = elemHeight + marginTop +
                (parseInt($.curCSS(this, "marginBottom", true)) || 0), position = $.extend({}, basePosition), collisionPosition; if (options.my[0] === "right") { position.left -= elemWidth; } else if (options.my[0] === center) { position.left -= elemWidth / 2; }
                if (options.my[1] === "bottom") { position.top -= elemHeight; } else if (options.my[1] === center) { position.top -= elemHeight / 2; }
                position.left = Math.round(position.left); position.top = Math.round(position.top); collisionPosition = { left: position.left - marginLeft, top: position.top - marginTop }; $.each(["left", "top"], function (i, dir) { if ($.ui.position[collision[i]]) { $.ui.position[collision[i]][dir](position, { targetWidth: targetWidth, targetHeight: targetHeight, elemWidth: elemWidth, elemHeight: elemHeight, collisionPosition: collisionPosition, collisionWidth: collisionWidth, collisionHeight: collisionHeight, offset: offset, my: options.my, at: options.at }); } }); if ($.fn.bgiframe) { elem.bgiframe(); }
                elem.offset($.extend(position, { using: options.using }));
            });
        }; $.ui.position = {
            fit: { left: function (position, data) { var win = $(window), over = data.collisionPosition.left + data.collisionWidth - win.width() - win.scrollLeft(); position.left = over > 0 ? position.left - over : Math.max(position.left - data.collisionPosition.left, position.left); }, top: function (position, data) { var win = $(window), over = data.collisionPosition.top + data.collisionHeight - win.height() - win.scrollTop(); position.top = over > 0 ? position.top - over : Math.max(position.top - data.collisionPosition.top, position.top); } }, flip: {
                left: function (position, data) {
                    if (data.at[0] === center) { return; }
                    var win = $(window), over = data.collisionPosition.left + data.collisionWidth - win.width() - win.scrollLeft(), myOffset = data.my[0] === "left" ? -data.elemWidth : data.my[0] === "right" ? data.elemWidth : 0, atOffset = data.at[0] === "left" ? data.targetWidth : -data.targetWidth, offset = -2 * data.offset[0]; position.left += data.collisionPosition.left < 0 ? myOffset + atOffset + offset : over > 0 ? myOffset + atOffset + offset : 0;
                }, top: function (position, data) {
                    if (data.at[1] === center) { return; }
                    var win = $(window), over = data.collisionPosition.top + data.collisionHeight - win.height() - win.scrollTop(), myOffset = data.my[1] === "top" ? -data.elemHeight : data.my[1] === "bottom" ? data.elemHeight : 0, atOffset = data.at[1] === "top" ? data.targetHeight : -data.targetHeight, offset = -2 * data.offset[1]; position.top += data.collisionPosition.top < 0 ? myOffset + atOffset + offset : over > 0 ? myOffset + atOffset + offset : 0;
                }
            }
        }; if (!$.offset.setOffset) {
            $.offset.setOffset = function (elem, options) {
                if (/static/.test($.curCSS(elem, "position"))) { elem.style.position = "relative"; }
                var curElem = $(elem), curOffset = curElem.offset(), curTop = parseInt($.curCSS(elem, "top", true), 10) || 0, curLeft = parseInt($.curCSS(elem, "left", true), 10) || 0, props = { top: (options.top - curOffset.top) + curTop, left: (options.left - curOffset.left) + curLeft }; if ('using' in options) { options.using.call(elem, props); } else { curElem.css(props); }
            }; $.fn.offset = function (options) {
                var elem = this[0]; if (!elem || !elem.ownerDocument) { return null; }
                if (options) { return this.each(function () { $.offset.setOffset(this, options); }); }
                return _offset.call(this);
            };
        }
    }(jQuery)); (function ($, undefined) {
        $.widget("ui.draggable", $.ui.mouse, {
            widgetEventPrefix: "drag", options: { addClasses: true, appendTo: "parent", axis: false, connectToSortable: false, containment: false, cursor: "auto", cursorAt: false, grid: false, handle: false, helper: "original", iframeFix: false, opacity: false, refreshPositions: false, revert: false, revertDuration: 500, scope: "default", scroll: true, scrollSensitivity: 20, scrollSpeed: 20, snap: false, snapMode: "both", snapTolerance: 20, stack: false, zIndex: false }, _create: function () {
                if (this.options.helper == 'original' && !(/^(?:r|a|f)/).test(this.element.css("position")))
                    this.element[0].style.position = 'relative'; (this.options.addClasses && this.element.addClass("ui-draggable")); (this.options.disabled && this.element.addClass("ui-draggable-disabled")); this._mouseInit();
            }, destroy: function () {
                if (!this.element.data('draggable')) return; this.element.removeData("draggable").unbind(".draggable").removeClass("ui-draggable"
                + " ui-draggable-dragging"
                + " ui-draggable-disabled"); this._mouseDestroy(); return this;
            }, _mouseCapture: function (event) {
                var o = this.options; if (this.helper || o.disabled || $(event.target).is('.ui-resizable-handle'))
                    return false; this.handle = this._getHandle(event); if (!this.handle)
                        return false; if (o.iframeFix) { $(o.iframeFix === true ? "iframe" : o.iframeFix).each(function () { $('<div class="ui-draggable-iframeFix" style="background: #fff;"></div>').css({ width: this.offsetWidth + "px", height: this.offsetHeight + "px", position: "absolute", opacity: "0.001", zIndex: 1000 }).css($(this).offset()).appendTo("body"); }); }
                return true;
            }, _mouseStart: function (event) {
                var o = this.options; this.helper = this._createHelper(event); this._cacheHelperProportions(); if ($.ui.ddmanager)
                    $.ui.ddmanager.current = this; this._cacheMargins(); this.cssPosition = this.helper.css("position"); this.scrollParent = this.helper.scrollParent(); this.offset = this.positionAbs = this.element.offset(); this.offset = { top: this.offset.top - this.margins.top, left: this.offset.left - this.margins.left }; $.extend(this.offset, {
                        click: { left: event.pageX - this.offset.left, top: event.pageY - this.offset.top }, parent: this._getParentOffset(), relative: this._getRelativeOffset()
                    }); this.originalPosition = this.position = this._generatePosition(event); this.originalPageX = event.pageX; this.originalPageY = event.pageY; (o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt)); if (o.containment)
                        this._setContainment(); if (this._trigger("start", event) === false) { this._clear(); return false; }
                this._cacheHelperProportions(); if ($.ui.ddmanager && !o.dropBehaviour)
                    $.ui.ddmanager.prepareOffsets(this, event); this.helper.addClass("ui-draggable-dragging"); this._mouseDrag(event, true);
                if ($.ui.ddmanager) $.ui.ddmanager.dragStart(this, event); return true;
            }, _mouseDrag: function (event, noPropagation) {
                this.position = this._generatePosition(event); this.positionAbs = this._convertPositionTo("absolute"); if (!noPropagation) {
                    var ui = this._uiHash(); if (this._trigger('drag', event, ui) === false) { this._mouseUp({}); return false; }
                    this.position = ui.position;
                }
                if (!this.options.axis || this.options.axis != "y") this.helper[0].style.left = this.position.left + 'px'; if (!this.options.axis || this.options.axis != "x") this.helper[0].style.top = this.position.top + 'px'; if ($.ui.ddmanager) $.ui.ddmanager.drag(this, event); return false;
            }, _mouseStop: function (event) {
                var dropped = false; if ($.ui.ddmanager && !this.options.dropBehaviour)
                    dropped = $.ui.ddmanager.drop(this, event); if (this.dropped) { dropped = this.dropped; this.dropped = false; }
                if ((!this.element[0] || !this.element[0].parentNode) && this.options.helper == "original")
                    return false; if ((this.options.revert == "invalid" && !dropped) || (this.options.revert == "valid" && dropped) || this.options.revert === true || ($.isFunction(this.options.revert) && this.options.revert.call(this.element, dropped))) { var self = this; $(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () { if (self._trigger("stop", event) !== false) { self._clear(); } }); } else { if (this._trigger("stop", event) !== false) { this._clear(); } }
                return false;
            }, _mouseUp: function (event) {
                if (this.options.iframeFix === true) { $("div.ui-draggable-iframeFix").each(function () { this.parentNode.removeChild(this); }); }
                if ($.ui.ddmanager) $.ui.ddmanager.dragStop(this, event); return $.ui.mouse.prototype._mouseUp.call(this, event);
            }, cancel: function () {
                if (this.helper.is(".ui-draggable-dragging")) { this._mouseUp({}); } else { this._clear(); }
                return this;
            }, _getHandle: function (event) { var handle = !this.options.handle || !$(this.options.handle, this.element).length ? true : false; $(this.options.handle, this.element).find("*").andSelf().each(function () { if (this == event.target) handle = true; }); return handle; }, _createHelper: function (event) {
                var o = this.options; var helper = $.isFunction(o.helper) ? $(o.helper.apply(this.element[0], [event])) : (o.helper == 'clone' ? this.element.clone().removeAttr('id') : this.element); if (!helper.parents('body').length)
                    helper.appendTo((o.appendTo == 'parent' ? this.element[0].parentNode : o.appendTo)); if (helper[0] != this.element[0] && !(/(fixed|absolute)/).test(helper.css("position")))
                        helper.css("position", "absolute"); return helper;
            }, _adjustOffsetFromHelper: function (obj) {
                if (typeof obj == 'string') { obj = obj.split(' '); }
                if ($.isArray(obj)) { obj = { left: +obj[0], top: +obj[1] || 0 }; }
                if ('left' in obj) { this.offset.click.left = obj.left + this.margins.left; }
                if ('right' in obj) { this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left; }
                if ('top' in obj) { this.offset.click.top = obj.top + this.margins.top; }
                if ('bottom' in obj) { this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top; }
            }, _getParentOffset: function () {
                this.offsetParent = this.helper.offsetParent(); var po = this.offsetParent.offset(); if (this.cssPosition == 'absolute' && this.scrollParent[0] != document && $.ui.contains(this.scrollParent[0], this.offsetParent[0])) { po.left += this.scrollParent.scrollLeft(); po.top += this.scrollParent.scrollTop(); }
                if ((this.offsetParent[0] == document.body)
                || (this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() == 'html' && $.browser.msie))
                    po = { top: 0, left: 0 }; return { top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0), left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0) };
            }, _getRelativeOffset: function () { if (this.cssPosition == "relative") { var p = this.element.position(); return { top: p.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(), left: p.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft() }; } else { return { top: 0, left: 0 }; } }, _cacheMargins: function () { this.margins = { left: (parseInt(this.element.css("marginLeft"), 10) || 0), top: (parseInt(this.element.css("marginTop"), 10) || 0), right: (parseInt(this.element.css("marginRight"), 10) || 0), bottom: (parseInt(this.element.css("marginBottom"), 10) || 0) }; }, _cacheHelperProportions: function () { this.helperProportions = { width: this.helper.outerWidth(), height: this.helper.outerHeight() }; }, _setContainment: function () { var o = this.options; if (o.containment == 'parent') o.containment = this.helper[0].parentNode; if (o.containment == 'document' || o.containment == 'window') this.containment = [o.containment == 'document' ? 0 : $(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, o.containment == 'document' ? 0 : $(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, (o.containment == 'document' ? 0 : $(window).scrollLeft()) + $(o.containment == 'document' ? document : window).width() - this.helperProportions.width - this.margins.left, (o.containment == 'document' ? 0 : $(window).scrollTop()) + ($(o.containment == 'document' ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]; if (!(/^(document|window|parent)$/).test(o.containment) && o.containment.constructor != Array) { var c = $(o.containment); var ce = c[0]; if (!ce) return; var co = c.offset(); var over = ($(ce).css("overflow") != 'hidden'); this.containment = [(parseInt($(ce).css("borderLeftWidth"), 10) || 0) + (parseInt($(ce).css("paddingLeft"), 10) || 0), (parseInt($(ce).css("borderTopWidth"), 10) || 0) + (parseInt($(ce).css("paddingTop"), 10) || 0), (over ? Math.max(ce.scrollWidth, ce.offsetWidth) : ce.offsetWidth) - (parseInt($(ce).css("borderLeftWidth"), 10) || 0) - (parseInt($(ce).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (over ? Math.max(ce.scrollHeight, ce.offsetHeight) : ce.offsetHeight) - (parseInt($(ce).css("borderTopWidth"), 10) || 0) - (parseInt($(ce).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom]; this.relative_container = c; } else if (o.containment.constructor == Array) { this.containment = o.containment; } }, _convertPositionTo: function (d, pos) { if (!pos) pos = this.position; var mod = d == "absolute" ? 1 : -1; var o = this.options, scroll = this.cssPosition == 'absolute' && !(this.scrollParent[0] != document && $.ui.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, scrollIsRootNode = (/(html|body)/i).test(scroll[0].tagName); return { top: (pos.top + this.offset.relative.top * mod + this.offset.parent.top * mod - ($.browser.safari && $.browser.version < 526 && this.cssPosition == 'fixed' ? 0 : (this.cssPosition == 'fixed' ? -this.scrollParent.scrollTop() : (scrollIsRootNode ? 0 : scroll.scrollTop())) * mod)), left: (pos.left + this.offset.relative.left * mod + this.offset.parent.left * mod - ($.browser.safari && $.browser.version < 526 && this.cssPosition == 'fixed' ? 0 : (this.cssPosition == 'fixed' ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft()) * mod)) }; }, _generatePosition: function (event) {
                var o = this.options, scroll = this.cssPosition == 'absolute' && !(this.scrollParent[0] != document && $.ui.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, scrollIsRootNode = (/(html|body)/i).test(scroll[0].tagName); var pageX = event.pageX; var pageY = event.pageY; if (this.originalPosition) {
                    var containment; if (this.containment) {
                        if (this.relative_container) { var co = this.relative_container.offset(); containment = [this.containment[0] + co.left, this.containment[1] + co.top, this.containment[2] + co.left, this.containment[3] + co.top]; }
                        else { containment = this.containment; }
                        if (event.pageX - this.offset.click.left < containment[0]) pageX = containment[0] + this.offset.click.left; if (event.pageY - this.offset.click.top < containment[1]) pageY = containment[1] + this.offset.click.top; if (event.pageX - this.offset.click.left > containment[2]) pageX = containment[2] + this.offset.click.left; if (event.pageY - this.offset.click.top > containment[3]) pageY = containment[3] + this.offset.click.top;
                    }
                    if (o.grid) { var top = o.grid[1] ? this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1] : this.originalPageY; pageY = containment ? (!(top - this.offset.click.top < containment[1] || top - this.offset.click.top > containment[3]) ? top : (!(top - this.offset.click.top < containment[1]) ? top - o.grid[1] : top + o.grid[1])) : top; var left = o.grid[0] ? this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0] : this.originalPageX; pageX = containment ? (!(left - this.offset.click.left < containment[0] || left - this.offset.click.left > containment[2]) ? left : (!(left - this.offset.click.left < containment[0]) ? left - o.grid[0] : left + o.grid[0])) : left; }
                }
                return { top: (pageY - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ($.browser.safari && $.browser.version < 526 && this.cssPosition == 'fixed' ? 0 : (this.cssPosition == 'fixed' ? -this.scrollParent.scrollTop() : (scrollIsRootNode ? 0 : scroll.scrollTop())))), left: (pageX - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ($.browser.safari && $.browser.version < 526 && this.cssPosition == 'fixed' ? 0 : (this.cssPosition == 'fixed' ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft()))) };
            }, _clear: function () { this.helper.removeClass("ui-draggable-dragging"); if (this.helper[0] != this.element[0] && !this.cancelHelperRemoval) this.helper.remove(); this.helper = null; this.cancelHelperRemoval = false; }, _trigger: function (type, event, ui) { ui = ui || this._uiHash(); $.ui.plugin.call(this, type, [event, ui]); if (type == "drag") this.positionAbs = this._convertPositionTo("absolute"); return $.Widget.prototype._trigger.call(this, type, event, ui); }, plugins: {}, _uiHash: function (event) { return { helper: this.helper, position: this.position, originalPosition: this.originalPosition, offset: this.positionAbs }; }
        }); $.extend($.ui.draggable, { version: "1.8.16" }); $.ui.plugin.add("draggable", "connectToSortable", {
            start: function (event, ui) { var inst = $(this).data("draggable"), o = inst.options, uiSortable = $.extend({}, ui, { item: inst.element }); inst.sortables = []; $(o.connectToSortable).each(function () { var sortable = $.data(this, 'sortable'); if (sortable && !sortable.options.disabled) { inst.sortables.push({ instance: sortable, shouldRevert: sortable.options.revert }); sortable.refreshPositions(); sortable._trigger("activate", event, uiSortable); } }); }, stop: function (event, ui) {
                var inst = $(this).data("draggable"), uiSortable = $.extend({}, ui, { item: inst.element }); $.each(inst.sortables, function () {
                    if (this.instance.isOver) {
                        this.instance.isOver = 0; inst.cancelHelperRemoval = true; this.instance.cancelHelperRemoval = false;
                        if (this.shouldRevert) this.instance.options.revert = true; this.instance._mouseStop(event); this.instance.options.helper = this.instance.options._helper; if (inst.options.helper == 'original')
                            this.instance.currentItem.css({ top: 'auto', left: 'auto' });
                    } else { this.instance.cancelHelperRemoval = false; this.instance._trigger("deactivate", event, uiSortable); }
                });
            }, drag: function (event, ui) {
                var inst = $(this).data("draggable"), self = this; var checkPos = function (o) { var dyClick = this.offset.click.top, dxClick = this.offset.click.left; var helperTop = this.positionAbs.top, helperLeft = this.positionAbs.left; var itemHeight = o.height, itemWidth = o.width; var itemTop = o.top, itemLeft = o.left; return $.ui.isOver(helperTop + dyClick, helperLeft + dxClick, itemTop, itemLeft, itemHeight, itemWidth); }; $.each(inst.sortables, function (i) {
                    this.instance.positionAbs = inst.positionAbs; this.instance.helperProportions = inst.helperProportions; this.instance.offset.click = inst.offset.click; if (this.instance._intersectsWith(this.instance.containerCache)) {
                        if (!this.instance.isOver) { this.instance.isOver = 1; this.instance.currentItem = $(self).clone().removeAttr('id').appendTo(this.instance.element).data("sortable-item", true); this.instance.options._helper = this.instance.options.helper; this.instance.options.helper = function () { return ui.helper[0]; }; event.target = this.instance.currentItem[0]; this.instance._mouseCapture(event, true); this.instance._mouseStart(event, true, true); this.instance.offset.click.top = inst.offset.click.top; this.instance.offset.click.left = inst.offset.click.left; this.instance.offset.parent.left -= inst.offset.parent.left - this.instance.offset.parent.left; this.instance.offset.parent.top -= inst.offset.parent.top - this.instance.offset.parent.top; inst._trigger("toSortable", event); inst.dropped = this.instance.element; inst.currentItem = inst.element; this.instance.fromOutside = inst; }
                        if (this.instance.currentItem) this.instance._mouseDrag(event);
                    } else { if (this.instance.isOver) { this.instance.isOver = 0; this.instance.cancelHelperRemoval = true; this.instance.options.revert = false; this.instance._trigger('out', event, this.instance._uiHash(this.instance)); this.instance._mouseStop(event, true); this.instance.options.helper = this.instance.options._helper; this.instance.currentItem.remove(); if (this.instance.placeholder) this.instance.placeholder.remove(); inst._trigger("fromSortable", event); inst.dropped = false; } };
                });
            }
        }); $.ui.plugin.add("draggable", "cursor", { start: function (event, ui) { var t = $('body'), o = $(this).data('draggable').options; if (t.css("cursor")) o._cursor = t.css("cursor"); t.css("cursor", o.cursor); }, stop: function (event, ui) { var o = $(this).data('draggable').options; if (o._cursor) $('body').css("cursor", o._cursor); } }); $.ui.plugin.add("draggable", "opacity", { start: function (event, ui) { var t = $(ui.helper), o = $(this).data('draggable').options; if (t.css("opacity")) o._opacity = t.css("opacity"); t.css('opacity', o.opacity); }, stop: function (event, ui) { var o = $(this).data('draggable').options; if (o._opacity) $(ui.helper).css('opacity', o._opacity); } }); $.ui.plugin.add("draggable", "scroll", {
            start: function (event, ui) { var i = $(this).data("draggable"); if (i.scrollParent[0] != document && i.scrollParent[0].tagName != 'HTML') i.overflowOffset = i.scrollParent.offset(); }, drag: function (event, ui) {
                var i = $(this).data("draggable"), o = i.options, scrolled = false; if (i.scrollParent[0] != document && i.scrollParent[0].tagName != 'HTML') {
                    if (!o.axis || o.axis != 'x') {
                        if ((i.overflowOffset.top + i.scrollParent[0].offsetHeight) - event.pageY < o.scrollSensitivity)
                            i.scrollParent[0].scrollTop = scrolled = i.scrollParent[0].scrollTop + o.scrollSpeed; else if (event.pageY - i.overflowOffset.top < o.scrollSensitivity)
                                i.scrollParent[0].scrollTop = scrolled = i.scrollParent[0].scrollTop - o.scrollSpeed;
                    }
                    if (!o.axis || o.axis != 'y') {
                        if ((i.overflowOffset.left + i.scrollParent[0].offsetWidth) - event.pageX < o.scrollSensitivity)
                            i.scrollParent[0].scrollLeft = scrolled = i.scrollParent[0].scrollLeft + o.scrollSpeed; else if (event.pageX - i.overflowOffset.left < o.scrollSensitivity)
                                i.scrollParent[0].scrollLeft = scrolled = i.scrollParent[0].scrollLeft - o.scrollSpeed;
                    }
                } else {
                    if (!o.axis || o.axis != 'x') {
                        if (event.pageY - $(document).scrollTop() < o.scrollSensitivity)
                            scrolled = $(document).scrollTop($(document).scrollTop() - o.scrollSpeed); else if ($(window).height() - (event.pageY - $(document).scrollTop()) < o.scrollSensitivity)
                                scrolled = $(document).scrollTop($(document).scrollTop() + o.scrollSpeed);
                    }
                    if (!o.axis || o.axis != 'y') {
                        if (event.pageX - $(document).scrollLeft() < o.scrollSensitivity)
                            scrolled = $(document).scrollLeft($(document).scrollLeft() - o.scrollSpeed); else if ($(window).width() - (event.pageX - $(document).scrollLeft()) < o.scrollSensitivity)
                                scrolled = $(document).scrollLeft($(document).scrollLeft() + o.scrollSpeed);
                    }
                }
                if (scrolled !== false && $.ui.ddmanager && !o.dropBehaviour)
                    $.ui.ddmanager.prepareOffsets(i, event);
            }
        }); $.ui.plugin.add("draggable", "snap", {
            start: function (event, ui) { var i = $(this).data("draggable"), o = i.options; i.snapElements = []; $(o.snap.constructor != String ? (o.snap.items || ':data(draggable)') : o.snap).each(function () { var $t = $(this); var $o = $t.offset(); if (this != i.element[0]) i.snapElements.push({ item: this, width: $t.outerWidth(), height: $t.outerHeight(), top: $o.top, left: $o.left }); }); }, drag: function (event, ui) {
                var inst = $(this).data("draggable"), o = inst.options; var d = o.snapTolerance; var x1 = ui.offset.left, x2 = x1 + inst.helperProportions.width, y1 = ui.offset.top, y2 = y1 + inst.helperProportions.height; for (var i = inst.snapElements.length - 1; i >= 0; i--) {
                    var l = inst.snapElements[i].left, r = l + inst.snapElements[i].width, t = inst.snapElements[i].top, b = t + inst.snapElements[i].height; if (!((l - d < x1 && x1 < r + d && t - d < y1 && y1 < b + d) || (l - d < x1 && x1 < r + d && t - d < y2 && y2 < b + d) || (l - d < x2 && x2 < r + d && t - d < y1 && y1 < b + d) || (l - d < x2 && x2 < r + d && t - d < y2 && y2 < b + d))) { if (inst.snapElements[i].snapping) (inst.options.snap.release && inst.options.snap.release.call(inst.element, event, $.extend(inst._uiHash(), { snapItem: inst.snapElements[i].item }))); inst.snapElements[i].snapping = false; continue; }
                    if (o.snapMode != 'inner') { var ts = Math.abs(t - y2) <= d; var bs = Math.abs(b - y1) <= d; var ls = Math.abs(l - x2) <= d; var rs = Math.abs(r - x1) <= d; if (ts) ui.position.top = inst._convertPositionTo("relative", { top: t - inst.helperProportions.height, left: 0 }).top - inst.margins.top; if (bs) ui.position.top = inst._convertPositionTo("relative", { top: b, left: 0 }).top - inst.margins.top; if (ls) ui.position.left = inst._convertPositionTo("relative", { top: 0, left: l - inst.helperProportions.width }).left - inst.margins.left; if (rs) ui.position.left = inst._convertPositionTo("relative", { top: 0, left: r }).left - inst.margins.left; }
                    var first = (ts || bs || ls || rs); if (o.snapMode != 'outer') { var ts = Math.abs(t - y1) <= d; var bs = Math.abs(b - y2) <= d; var ls = Math.abs(l - x1) <= d; var rs = Math.abs(r - x2) <= d; if (ts) ui.position.top = inst._convertPositionTo("relative", { top: t, left: 0 }).top - inst.margins.top; if (bs) ui.position.top = inst._convertPositionTo("relative", { top: b - inst.helperProportions.height, left: 0 }).top - inst.margins.top; if (ls) ui.position.left = inst._convertPositionTo("relative", { top: 0, left: l }).left - inst.margins.left; if (rs) ui.position.left = inst._convertPositionTo("relative", { top: 0, left: r - inst.helperProportions.width }).left - inst.margins.left; }
                    if (!inst.snapElements[i].snapping && (ts || bs || ls || rs || first))
                        (inst.options.snap.snap && inst.options.snap.snap.call(inst.element, event, $.extend(inst._uiHash(), { snapItem: inst.snapElements[i].item }))); inst.snapElements[i].snapping = (ts || bs || ls || rs || first);
                };
            }
        }); $.ui.plugin.add("draggable", "stack", {
            start: function (event, ui) {
                var o = $(this).data("draggable").options; var group = $.makeArray($(o.stack)).sort(function (a, b) { return (parseInt($(a).css("zIndex"), 10) || 0) - (parseInt($(b).css("zIndex"), 10) || 0); }); if (!group.length) { return; }
                var min = parseInt(group[0].style.zIndex) || 0; $(group).each(function (i) { this.style.zIndex = min + i; }); this[0].style.zIndex = min + group.length;
            }
        }); $.ui.plugin.add("draggable", "zIndex", { start: function (event, ui) { var t = $(ui.helper), o = $(this).data("draggable").options; if (t.css("zIndex")) o._zIndex = t.css("zIndex"); t.css('zIndex', o.zIndex); }, stop: function (event, ui) { var o = $(this).data("draggable").options; if (o._zIndex) $(ui.helper).css('zIndex', o._zIndex); } });
    })(jQuery); (function ($, undefined) {
        $.widget("ui.droppable", {
            widgetEventPrefix: "drop", options: { accept: '*', activeClass: false, addClasses: true, greedy: false, hoverClass: false, scope: 'default', tolerance: 'intersect' }, _create: function () { var o = this.options, accept = o.accept; this.isover = 0; this.isout = 1; this.accept = $.isFunction(accept) ? accept : function (d) { return d.is(accept); }; this.proportions = { width: this.element[0].offsetWidth, height: this.element[0].offsetHeight }; $.ui.ddmanager.droppables[o.scope] = $.ui.ddmanager.droppables[o.scope] || []; $.ui.ddmanager.droppables[o.scope].push(this); (o.addClasses && this.element.addClass("ui-droppable")); }, destroy: function () {
                var drop = $.ui.ddmanager.droppables[this.options.scope]; for (var i = 0; i < drop.length; i++)
                    if (drop[i] == this)
                        drop.splice(i, 1); this.element.removeClass("ui-droppable ui-droppable-disabled").removeData("droppable").unbind(".droppable"); return this;
            }, _setOption: function (key, value) {
                if (key == 'accept') { this.accept = $.isFunction(value) ? value : function (d) { return d.is(value); }; }
                $.Widget.prototype._setOption.apply(this, arguments);
            }, _activate: function (event) { var draggable = $.ui.ddmanager.current; if (this.options.activeClass) this.element.addClass(this.options.activeClass); (draggable && this._trigger('activate', event, this.ui(draggable))); }, _deactivate: function (event) { var draggable = $.ui.ddmanager.current; if (this.options.activeClass) this.element.removeClass(this.options.activeClass); (draggable && this._trigger('deactivate', event, this.ui(draggable))); }, _over: function (event) { var draggable = $.ui.ddmanager.current; if (!draggable || (draggable.currentItem || draggable.element)[0] == this.element[0]) return; if (this.accept.call(this.element[0], (draggable.currentItem || draggable.element))) { if (this.options.hoverClass) this.element.addClass(this.options.hoverClass); this._trigger('over', event, this.ui(draggable)); } }, _out: function (event) { var draggable = $.ui.ddmanager.current; if (!draggable || (draggable.currentItem || draggable.element)[0] == this.element[0]) return; if (this.accept.call(this.element[0], (draggable.currentItem || draggable.element))) { if (this.options.hoverClass) this.element.removeClass(this.options.hoverClass); this._trigger('out', event, this.ui(draggable)); } }, _drop: function (event, custom) {
                var draggable = custom || $.ui.ddmanager.current; if (!draggable || (draggable.currentItem || draggable.element)[0] == this.element[0]) return false; var childrenIntersection = false; this.element.find(":data(droppable)").not(".ui-draggable-dragging").each(function () { var inst = $.data(this, 'droppable'); if (inst.options.greedy && !inst.options.disabled && inst.options.scope == draggable.options.scope && inst.accept.call(inst.element[0], (draggable.currentItem || draggable.element)) && $.ui.intersect(draggable, $.extend(inst, { offset: inst.element.offset() }), inst.options.tolerance)) { childrenIntersection = true; return false; } }); if (childrenIntersection) return false; if (this.accept.call(this.element[0], (draggable.currentItem || draggable.element))) { if (this.options.activeClass) this.element.removeClass(this.options.activeClass); if (this.options.hoverClass) this.element.removeClass(this.options.hoverClass); this._trigger('drop', event, this.ui(draggable)); return this.element; }
                return false;
            }, ui: function (c) { return { draggable: (c.currentItem || c.element), helper: c.helper, position: c.position, offset: c.positionAbs }; }
        }); $.extend($.ui.droppable, { version: "1.8.16" }); $.ui.intersect = function (draggable, droppable, toleranceMode) {
            if (!droppable.offset) return false; var x1 = (draggable.positionAbs || draggable.position.absolute).left, x2 = x1 + draggable.helperProportions.width, y1 = (draggable.positionAbs || draggable.position.absolute).top, y2 = y1 + draggable.helperProportions.height; var l = droppable.offset.left, r = l + droppable.proportions.width, t = droppable.offset.top, b = t + droppable.proportions.height; switch (toleranceMode) {
                case 'fit': return (l <= x1 && x2 <= r && t <= y1 && y2 <= b); break; case 'intersect': return (l < x1 + (draggable.helperProportions.width / 2)
                && x2 - (draggable.helperProportions.width / 2) < r && t < y1 + (draggable.helperProportions.height / 2)
                && y2 - (draggable.helperProportions.height / 2) < b); break; case 'pointer': var draggableLeft = ((draggable.positionAbs || draggable.position.absolute).left + (draggable.clickOffset || draggable.offset.click).left), draggableTop = ((draggable.positionAbs || draggable.position.absolute).top + (draggable.clickOffset || draggable.offset.click).top), isOver = $.ui.isOver(draggableTop, draggableLeft, t, l, droppable.proportions.height, droppable.proportions.width); return isOver; break; case 'touch': return ((y1 >= t && y1 <= b) || (y2 >= t && y2 <= b) || (y1 < t && y2 > b)
                ) && ((x1 >= l && x1 <= r) || (x2 >= l && x2 <= r) || (x1 < l && x2 > r)
                ); break; default: return false; break;
            }
        }; $.ui.ddmanager = {
            current: null, droppables: { 'default': [] }, prepareOffsets: function (t, event) { var m = $.ui.ddmanager.droppables[t.options.scope] || []; var type = event ? event.type : null; var list = (t.currentItem || t.element).find(":data(droppable)").andSelf(); droppablesLoop: for (var i = 0; i < m.length; i++) { if (m[i].options.disabled || (t && !m[i].accept.call(m[i].element[0], (t.currentItem || t.element)))) continue; for (var j = 0; j < list.length; j++) { if (list[j] == m[i].element[0]) { m[i].proportions.height = 0; continue droppablesLoop; } }; m[i].visible = m[i].element.css("display") != "none"; if (!m[i].visible) continue; if (type == "mousedown") m[i]._activate.call(m[i], event); m[i].offset = m[i].element.offset(); m[i].proportions = { width: m[i].element[0].offsetWidth, height: m[i].element[0].offsetHeight }; } }, drop: function (draggable, event) {
                var dropped = false; $.each($.ui.ddmanager.droppables[draggable.options.scope] || [], function () {
                    if (!this.options) return; if (!this.options.disabled && this.visible && $.ui.intersect(draggable, this, this.options.tolerance))
                        dropped = dropped || this._drop.call(this, event); if (!this.options.disabled && this.visible && this.accept.call(this.element[0], (draggable.currentItem || draggable.element))) { this.isout = 1; this.isover = 0; this._deactivate.call(this, event); }
                }); return dropped;
            }, dragStart: function (draggable, event) { draggable.element.parents(":not(body,html)").bind("scroll.droppable", function () { if (!draggable.options.refreshPositions) $.ui.ddmanager.prepareOffsets(draggable, event); }); }, drag: function (draggable, event) {
                if (draggable.options.refreshPositions) $.ui.ddmanager.prepareOffsets(draggable, event); $.each($.ui.ddmanager.droppables[draggable.options.scope] || [], function () {
                    if (this.options.disabled || this.greedyChild || !this.visible) return; var intersects = $.ui.intersect(draggable, this, this.options.tolerance); var c = !intersects && this.isover == 1 ? 'isout' : (intersects && this.isover == 0 ? 'isover' : null); if (!c) return; var parentInstance; if (this.options.greedy) { var parent = this.element.parents(':data(droppable):eq(0)'); if (parent.length) { parentInstance = $.data(parent[0], 'droppable'); parentInstance.greedyChild = (c == 'isover' ? 1 : 0); } }
                    if (parentInstance && c == 'isover') { parentInstance['isover'] = 0; parentInstance['isout'] = 1; parentInstance._out.call(parentInstance, event); }
                    this[c] = 1; this[c == 'isout' ? 'isover' : 'isout'] = 0; this[c == "isover" ? "_over" : "_out"].call(this, event); if (parentInstance && c == 'isout') { parentInstance['isout'] = 0; parentInstance['isover'] = 1; parentInstance._over.call(parentInstance, event); }
                });
            }, dragStop: function (draggable, event) { draggable.element.parents(":not(body,html)").unbind("scroll.droppable"); if (!draggable.options.refreshPositions) $.ui.ddmanager.prepareOffsets(draggable, event); }
        };
    })(jQuery); (function ($, undefined) {
        $.widget("ui.resizable", $.ui.mouse, {
            widgetEventPrefix: "resize", options: { alsoResize: false, animate: false, animateDuration: "slow", animateEasing: "swing", aspectRatio: false, autoHide: false, containment: false, ghost: false, grid: false, handles: "e,s,se", helper: false, maxHeight: null, maxWidth: null, minHeight: 10, minWidth: 10, zIndex: 1000 }, _create: function () {
                var self = this, o = this.options; this.element.addClass("ui-resizable"); $.extend(this, { _aspectRatio: !!(o.aspectRatio), aspectRatio: o.aspectRatio, originalElement: this.element, _proportionallyResizeElements: [], _helper: o.helper || o.ghost || o.animate ? o.helper || 'ui-resizable-helper' : null }); if (this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)) {
                    if (/relative/.test(this.element.css('position')) && $.browser.opera)
                        this.element.css({ position: 'relative', top: 'auto', left: 'auto' }); this.element.wrap($('<div class="ui-wrapper" style="overflow: hidden;"></div>').css({ position: this.element.css('position'), width: this.element.outerWidth(), height: this.element.outerHeight(), top: this.element.css('top'), left: this.element.css('left') })); this.element = this.element.parent().data("resizable", this.element.data('resizable')); this.elementIsWrapper = true; this.element.css({ marginLeft: this.originalElement.css("marginLeft"), marginTop: this.originalElement.css("marginTop"), marginRight: this.originalElement.css("marginRight"), marginBottom: this.originalElement.css("marginBottom") }); this.originalElement.css({ marginLeft: 0, marginTop: 0, marginRight: 0, marginBottom: 0 }); this.originalResizeStyle = this.originalElement.css('resize'); this.originalElement.css('resize', 'none'); this._proportionallyResizeElements.push(this.originalElement.css({ position: 'static', zoom: 1, display: 'block' })); this.originalElement.css({ margin: this.originalElement.css('margin') }); this._proportionallyResize();
                }
                this.handles = o.handles || (!$('.ui-resizable-handle', this.element).length ? "e,s,se" : { n: '.ui-resizable-n', e: '.ui-resizable-e', s: '.ui-resizable-s', w: '.ui-resizable-w', se: '.ui-resizable-se', sw: '.ui-resizable-sw', ne: '.ui-resizable-ne', nw: '.ui-resizable-nw' }); if (this.handles.constructor == String) { if (this.handles == 'all') this.handles = 'n,e,s,w,se,sw,ne,nw'; var n = this.handles.split(","); this.handles = {}; for (var i = 0; i < n.length; i++) { var handle = $.trim(n[i]), hname = 'ui-resizable-' + handle; var axis = $('<div class="ui-resizable-handle ' + hname + '"></div>'); if (/sw|se|ne|nw/.test(handle)) axis.css({ zIndex: ++o.zIndex }); if ('se' == handle) { axis.addClass('ui-icon ui-icon-gripsmall-diagonal-se'); }; this.handles[handle] = '.ui-resizable-' + handle; this.element.append(axis); } }
                this._renderAxis = function (target) {
                    target = target || this.element; for (var i in this.handles) {
                        if (this.handles[i].constructor == String)
                            this.handles[i] = $(this.handles[i], this.element).show(); if (this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i)) { var axis = $(this.handles[i], this.element), padWrapper = 0; padWrapper = /sw|ne|nw|se|n|s/.test(i) ? axis.outerHeight() : axis.outerWidth(); var padPos = ['padding', /ne|nw|n/.test(i) ? 'Top' : /se|sw|s/.test(i) ? 'Bottom' : /^e$/.test(i) ? 'Right' : 'Left'].join(""); target.css(padPos, padWrapper); this._proportionallyResize(); }
                        if (!$(this.handles[i]).length)
                            continue;
                    }
                }; this._renderAxis(this.element); this._handles = $('.ui-resizable-handle', this.element).disableSelection(); this._handles.mouseover(function () {
                    if (!self.resizing) {
                        if (this.className)
                            var axis = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i); self.axis = axis && axis[1] ? axis[1] : 'se';
                    }
                }); if (o.autoHide) { this._handles.hide(); $(this.element).addClass("ui-resizable-autohide").hover(function () { if (o.disabled) return; $(this).removeClass("ui-resizable-autohide"); self._handles.show(); }, function () { if (o.disabled) return; if (!self.resizing) { $(this).addClass("ui-resizable-autohide"); self._handles.hide(); } }); }
                this._mouseInit();
            }, destroy: function () {
                this._mouseDestroy(); var _destroy = function (exp) { $(exp).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").unbind(".resizable").find('.ui-resizable-handle').remove(); }; if (this.elementIsWrapper) { _destroy(this.element); var wrapper = this.element; wrapper.after(this.originalElement.css({ position: wrapper.css('position'), width: wrapper.outerWidth(), height: wrapper.outerHeight(), top: wrapper.css('top'), left: wrapper.css('left') })).remove(); }
                this.originalElement.css('resize', this.originalResizeStyle); _destroy(this.originalElement); return this;
            }, _mouseCapture: function (event) {
                var handle = false; for (var i in this.handles) { if ($(this.handles[i])[0] == event.target) { handle = true; } }
                return !this.options.disabled && handle;
            }, _mouseStart: function (event) {
                var o = this.options, iniPos = this.element.position(), el = this.element; this.resizing = true; this.documentScroll = { top: $(document).scrollTop(), left: $(document).scrollLeft() }; if (el.is('.ui-draggable') || (/absolute/).test(el.css('position'))) { el.css({ position: 'absolute', top: iniPos.top, left: iniPos.left }); }
                if ($.browser.opera && (/relative/).test(el.css('position')))
                    el.css({ position: 'relative', top: 'auto', left: 'auto' }); this._renderProxy(); var curleft = num(this.helper.css('left')), curtop = num(this.helper.css('top')); if (o.containment) { curleft += $(o.containment).scrollLeft() || 0; curtop += $(o.containment).scrollTop() || 0; }
                this.offset = this.helper.offset(); this.position = { left: curleft, top: curtop }; this.size = this._helper ? { width: el.outerWidth(), height: el.outerHeight() } : { width: el.width(), height: el.height() }; this.originalSize = this._helper ? { width: el.outerWidth(), height: el.outerHeight() } : { width: el.width(), height: el.height() }; this.originalPosition = { left: curleft, top: curtop }; this.sizeDiff = { width: el.outerWidth() - el.width(), height: el.outerHeight() - el.height() }; this.originalMousePosition = { left: event.pageX, top: event.pageY }; this.aspectRatio = (typeof o.aspectRatio == 'number') ? o.aspectRatio : ((this.originalSize.width / this.originalSize.height) || 1); var cursor = $('.ui-resizable-' + this.axis).css('cursor'); $('body').css('cursor', cursor == 'auto' ? this.axis + '-resize' : cursor); el.addClass("ui-resizable-resizing"); this._propagate("start", event); return true;
            }, _mouseDrag: function (event) {
                var el = this.helper, o = this.options, props = {}, self = this, smp = this.originalMousePosition, a = this.axis; var dx = (event.pageX - smp.left) || 0, dy = (event.pageY - smp.top) || 0; var trigger = this._change[a]; if (!trigger) return false; var data = trigger.apply(this, [event, dx, dy]), ie6 = $.browser.msie && $.browser.version < 7, csdif = this.sizeDiff; this._updateVirtualBoundaries(event.shiftKey); if (this._aspectRatio || event.shiftKey)
                    data = this._updateRatio(data, event); data = this._respectSize(data, event); this._propagate("resize", event); el.css({ top: this.position.top + "px", left: this.position.left + "px", width: this.size.width + "px", height: this.size.height + "px" }); if (!this._helper && this._proportionallyResizeElements.length)
                        this._proportionallyResize(); this._updateCache(data); this._trigger('resize', event, this.ui()); return false;
            }, _mouseStop: function (event) {
                this.resizing = false; var o = this.options, self = this; if (this._helper) {
                    var pr = this._proportionallyResizeElements, ista = pr.length && (/textarea/i).test(pr[0].nodeName), soffseth = ista && $.ui.hasScroll(pr[0], 'left') ? 0 : self.sizeDiff.height, soffsetw = ista ? 0 : self.sizeDiff.width; var s = { width: (self.helper.width() - soffsetw), height: (self.helper.height() - soffseth) }, left = (parseInt(self.element.css('left'), 10) + (self.position.left - self.originalPosition.left)) || null, top = (parseInt(self.element.css('top'), 10) + (self.position.top - self.originalPosition.top)) || null; if (!o.animate)
                        this.element.css($.extend(s, { top: top, left: left })); self.helper.height(self.size.height); self.helper.width(self.size.width); if (this._helper && !o.animate) this._proportionallyResize();
                }
                $('body').css('cursor', 'auto'); this.element.removeClass("ui-resizable-resizing"); this._propagate("stop", event); if (this._helper) this.helper.remove(); return false;
            }, _updateVirtualBoundaries: function (forceAspectRatio) {
                var o = this.options, pMinWidth, pMaxWidth, pMinHeight, pMaxHeight, b; b = { minWidth: isNumber(o.minWidth) ? o.minWidth : 0, maxWidth: isNumber(o.maxWidth) ? o.maxWidth : Infinity, minHeight: isNumber(o.minHeight) ? o.minHeight : 0, maxHeight: isNumber(o.maxHeight) ? o.maxHeight : Infinity }; if (this._aspectRatio || forceAspectRatio) { pMinWidth = b.minHeight * this.aspectRatio; pMinHeight = b.minWidth / this.aspectRatio; pMaxWidth = b.maxHeight * this.aspectRatio; pMaxHeight = b.maxWidth / this.aspectRatio; if (pMinWidth > b.minWidth) b.minWidth = pMinWidth; if (pMinHeight > b.minHeight) b.minHeight = pMinHeight; if (pMaxWidth < b.maxWidth) b.maxWidth = pMaxWidth; if (pMaxHeight < b.maxHeight) b.maxHeight = pMaxHeight; }
                this._vBoundaries = b;
            }, _updateCache: function (data) { var o = this.options; this.offset = this.helper.offset(); if (isNumber(data.left)) this.position.left = data.left; if (isNumber(data.top)) this.position.top = data.top; if (isNumber(data.height)) this.size.height = data.height; if (isNumber(data.width)) this.size.width = data.width; }, _updateRatio: function (data, event) {
                var o = this.options, cpos = this.position, csize = this.size, a = this.axis; if (isNumber(data.height)) data.width = (data.height * this.aspectRatio); else if (isNumber(data.width)) data.height = (data.width / this.aspectRatio); if (a == 'sw') { data.left = cpos.left + (csize.width - data.width); data.top = null; }
                if (a == 'nw') { data.top = cpos.top + (csize.height - data.height); data.left = cpos.left + (csize.width - data.width); }
                return data;
            }, _respectSize: function (data, event) { var el = this.helper, o = this._vBoundaries, pRatio = this._aspectRatio || event.shiftKey, a = this.axis, ismaxw = isNumber(data.width) && o.maxWidth && (o.maxWidth < data.width), ismaxh = isNumber(data.height) && o.maxHeight && (o.maxHeight < data.height), isminw = isNumber(data.width) && o.minWidth && (o.minWidth > data.width), isminh = isNumber(data.height) && o.minHeight && (o.minHeight > data.height); if (isminw) data.width = o.minWidth; if (isminh) data.height = o.minHeight; if (ismaxw) data.width = o.maxWidth; if (ismaxh) data.height = o.maxHeight; var dw = this.originalPosition.left + this.originalSize.width, dh = this.position.top + this.size.height; var cw = /sw|nw|w/.test(a), ch = /nw|ne|n/.test(a); if (isminw && cw) data.left = dw - o.minWidth; if (ismaxw && cw) data.left = dw - o.maxWidth; if (isminh && ch) data.top = dh - o.minHeight; if (ismaxh && ch) data.top = dh - o.maxHeight; var isNotwh = !data.width && !data.height; if (isNotwh && !data.left && data.top) data.top = null; else if (isNotwh && !data.top && data.left) data.left = null; return data; }, _proportionallyResize: function () {
                var o = this.options; if (!this._proportionallyResizeElements.length) return; var element = this.helper || this.element; for (var i = 0; i < this._proportionallyResizeElements.length; i++) {
                    var prel = this._proportionallyResizeElements[i]; if (!this.borderDif) { var b = [prel.css('borderTopWidth'), prel.css('borderRightWidth'), prel.css('borderBottomWidth'), prel.css('borderLeftWidth')], p = [prel.css('paddingTop'), prel.css('paddingRight'), prel.css('paddingBottom'), prel.css('paddingLeft')]; this.borderDif = $.map(b, function (v, i) { var border = parseInt(v, 10) || 0, padding = parseInt(p[i], 10) || 0; return border + padding; }); }
                    if ($.browser.msie && !(!($(element).is(':hidden') || $(element).parents(':hidden').length)))
                        continue; prel.css({ height: (element.height() - this.borderDif[0] - this.borderDif[2]) || 0, width: (element.width() - this.borderDif[1] - this.borderDif[3]) || 0 });
                };
            }, _renderProxy: function () { var el = this.element, o = this.options; this.elementOffset = el.offset(); if (this._helper) { this.helper = this.helper || $('<div style="overflow:hidden;"></div>'); var ie6 = $.browser.msie && $.browser.version < 7, ie6offset = (ie6 ? 1 : 0), pxyoffset = (ie6 ? 2 : -1); this.helper.addClass(this._helper).css({ width: this.element.outerWidth() + pxyoffset, height: this.element.outerHeight() + pxyoffset, position: 'absolute', left: this.elementOffset.left - ie6offset + 'px', top: this.elementOffset.top - ie6offset + 'px', zIndex: ++o.zIndex }); this.helper.appendTo("body").disableSelection(); } else { this.helper = this.element; } }, _change: { e: function (event, dx, dy) { return { width: this.originalSize.width + dx }; }, w: function (event, dx, dy) { var o = this.options, cs = this.originalSize, sp = this.originalPosition; return { left: sp.left + dx, width: cs.width - dx }; }, n: function (event, dx, dy) { var o = this.options, cs = this.originalSize, sp = this.originalPosition; return { top: sp.top + dy, height: cs.height - dy }; }, s: function (event, dx, dy) { return { height: this.originalSize.height + dy }; }, se: function (event, dx, dy) { return $.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [event, dx, dy])); }, sw: function (event, dx, dy) { return $.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [event, dx, dy])); }, ne: function (event, dx, dy) { return $.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [event, dx, dy])); }, nw: function (event, dx, dy) { return $.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [event, dx, dy])); } }, _propagate: function (n, event) { $.ui.plugin.call(this, n, [event, this.ui()]); (n != "resize" && this._trigger(n, event, this.ui())); }, plugins: {}, ui: function () { return { originalElement: this.originalElement, element: this.element, helper: this.helper, position: this.position, size: this.size, originalSize: this.originalSize, originalPosition: this.originalPosition }; }
        }); $.extend($.ui.resizable, { version: "1.8.16" }); $.ui.plugin.add("resizable", "alsoResize", {
            start: function (event, ui) {
                var self = $(this).data("resizable"), o = self.options; var _store = function (exp) {
                    $(exp).each(function () {
                        var el = $(this); el.data("resizable-alsoresize", {
                            width: parseInt(el.width(), 10), height: parseInt(el.height(), 10), left: parseInt(el.css('left'), 10), top: parseInt(el.css('top'), 10), position: el.css('position')
                        });
                    });
                }; if (typeof (o.alsoResize) == 'object' && !o.alsoResize.parentNode) {
                    if (o.alsoResize.length) { o.alsoResize = o.alsoResize[0]; _store(o.alsoResize); }
                    else { $.each(o.alsoResize, function (exp) { _store(exp); }); }
                } else { _store(o.alsoResize); }
            }, resize: function (event, ui) {
                var self = $(this).data("resizable"), o = self.options, os = self.originalSize, op = self.originalPosition; var delta = { height: (self.size.height - os.height) || 0, width: (self.size.width - os.width) || 0, top: (self.position.top - op.top) || 0, left: (self.position.left - op.left) || 0 }, _alsoResize = function (exp, c) {
                    $(exp).each(function () {
                        var el = $(this), start = $(this).data("resizable-alsoresize"), style = {}, css = c && c.length ? c : el.parents(ui.originalElement[0]).length ? ['width', 'height'] : ['width', 'height', 'top', 'left']; $.each(css, function (i, prop) {
                            var sum = (start[prop] || 0) + (delta[prop] || 0); if (sum && sum >= 0)
                                style[prop] = sum || null;
                        }); if ($.browser.opera && /relative/.test(el.css('position'))) { self._revertToRelativePosition = true; el.css({ position: 'absolute', top: 'auto', left: 'auto' }); }
                        el.css(style);
                    });
                }; if (typeof (o.alsoResize) == 'object' && !o.alsoResize.nodeType) { $.each(o.alsoResize, function (exp, c) { _alsoResize(exp, c); }); } else { _alsoResize(o.alsoResize); }
            }, stop: function (event, ui) {
                var self = $(this).data("resizable"), o = self.options; var _reset = function (exp) { $(exp).each(function () { var el = $(this); el.css({ position: el.data("resizable-alsoresize").position }); }); }; if (self._revertToRelativePosition) { self._revertToRelativePosition = false; if (typeof (o.alsoResize) == 'object' && !o.alsoResize.nodeType) { $.each(o.alsoResize, function (exp) { _reset(exp); }); } else { _reset(o.alsoResize); } }
                $(this).removeData("resizable-alsoresize");
            }
        }); $.ui.plugin.add("resizable", "animate", { stop: function (event, ui) { var self = $(this).data("resizable"), o = self.options; var pr = self._proportionallyResizeElements, ista = pr.length && (/textarea/i).test(pr[0].nodeName), soffseth = ista && $.ui.hasScroll(pr[0], 'left') ? 0 : self.sizeDiff.height, soffsetw = ista ? 0 : self.sizeDiff.width; var style = { width: (self.size.width - soffsetw), height: (self.size.height - soffseth) }, left = (parseInt(self.element.css('left'), 10) + (self.position.left - self.originalPosition.left)) || null, top = (parseInt(self.element.css('top'), 10) + (self.position.top - self.originalPosition.top)) || null; self.element.animate($.extend(style, top && left ? { top: top, left: left } : {}), { duration: o.animateDuration, easing: o.animateEasing, step: function () { var data = { width: parseInt(self.element.css('width'), 10), height: parseInt(self.element.css('height'), 10), top: parseInt(self.element.css('top'), 10), left: parseInt(self.element.css('left'), 10) }; if (pr && pr.length) $(pr[0]).css({ width: data.width, height: data.height }); self._updateCache(data); self._propagate("resize", event); } }); } }); $.ui.plugin.add("resizable", "containment", {
            start: function (event, ui) {
                var self = $(this).data("resizable"), o = self.options, el = self.element; var oc = o.containment, ce = (oc instanceof $) ? oc.get(0) : (/parent/.test(oc)) ? el.parent().get(0) : oc; if (!ce) return; self.containerElement = $(ce); if (/document/.test(oc) || oc == document) { self.containerOffset = { left: 0, top: 0 }; self.containerPosition = { left: 0, top: 0 }; self.parentData = { element: $(document), left: 0, top: 0, width: $(document).width(), height: $(document).height() || document.body.parentNode.scrollHeight }; }
                else { var element = $(ce), p = []; $(["Top", "Right", "Left", "Bottom"]).each(function (i, name) { p[i] = num(element.css("padding" + name)); }); self.containerOffset = element.offset(); self.containerPosition = element.position(); self.containerSize = { height: (element.innerHeight() - p[3]), width: (element.innerWidth() - p[1]) }; var co = self.containerOffset, ch = self.containerSize.height, cw = self.containerSize.width, width = ($.ui.hasScroll(ce, "left") ? ce.scrollWidth : cw), height = ($.ui.hasScroll(ce) ? ce.scrollHeight : ch); self.parentData = { element: ce, left: co.left, top: co.top, width: width, height: height }; }
            }, resize: function (event, ui) {
                var self = $(this).data("resizable"), o = self.options, ps = self.containerSize, co = self.containerOffset, cs = self.size, cp = self.position, pRatio = self._aspectRatio || event.shiftKey, cop = { top: 0, left: 0 }, ce = self.containerElement; if (ce[0] != document && (/static/).test(ce.css('position'))) cop = co; if (cp.left < (self._helper ? co.left : 0)) { self.size.width = self.size.width + (self._helper ? (self.position.left - co.left) : (self.position.left - cop.left)); if (pRatio) self.size.height = self.size.width / o.aspectRatio; self.position.left = o.helper ? co.left : 0; }
                if (cp.top < (self._helper ? co.top : 0)) { self.size.height = self.size.height + (self._helper ? (self.position.top - co.top) : self.position.top); if (pRatio) self.size.width = self.size.height * o.aspectRatio; self.position.top = self._helper ? co.top : 0; }
                self.offset.left = self.parentData.left + self.position.left; self.offset.top = self.parentData.top + self.position.top; var woset = Math.abs((self._helper ? self.offset.left - cop.left : (self.offset.left - cop.left)) + self.sizeDiff.width), hoset = Math.abs((self._helper ? self.offset.top - cop.top : (self.offset.top - co.top)) + self.sizeDiff.height); var isParent = self.containerElement.get(0) == self.element.parent().get(0), isOffsetRelative = /relative|absolute/.test(self.containerElement.css('position')); if (isParent && isOffsetRelative) woset -= self.parentData.left; if (woset + self.size.width >= self.parentData.width) { self.size.width = self.parentData.width - woset; if (pRatio) self.size.height = self.size.width / self.aspectRatio; }
                if (hoset + self.size.height >= self.parentData.height) { self.size.height = self.parentData.height - hoset; if (pRatio) self.size.width = self.size.height * self.aspectRatio; }
            }, stop: function (event, ui) {
                var self = $(this).data("resizable"), o = self.options, cp = self.position, co = self.containerOffset, cop = self.containerPosition, ce = self.containerElement; var helper = $(self.helper), ho = helper.offset(), w = helper.outerWidth() - self.sizeDiff.width, h = helper.outerHeight() - self.sizeDiff.height; if (self._helper && !o.animate && (/relative/).test(ce.css('position')))
                    $(this).css({ left: ho.left - cop.left - co.left, width: w, height: h }); if (self._helper && !o.animate && (/static/).test(ce.css('position')))
                        $(this).css({ left: ho.left - cop.left - co.left, width: w, height: h });
            }
        }); $.ui.plugin.add("resizable", "ghost", { start: function (event, ui) { var self = $(this).data("resizable"), o = self.options, cs = self.size; self.ghost = self.originalElement.clone(); self.ghost.css({ opacity: .25, display: 'block', position: 'relative', height: cs.height, width: cs.width, margin: 0, left: 0, top: 0 }).addClass('ui-resizable-ghost').addClass(typeof o.ghost == 'string' ? o.ghost : ''); self.ghost.appendTo(self.helper); }, resize: function (event, ui) { var self = $(this).data("resizable"), o = self.options; if (self.ghost) self.ghost.css({ position: 'relative', height: self.size.height, width: self.size.width }); }, stop: function (event, ui) { var self = $(this).data("resizable"), o = self.options; if (self.ghost && self.helper) self.helper.get(0).removeChild(self.ghost.get(0)); } }); $.ui.plugin.add("resizable", "grid", {
            resize: function (event, ui) {
                var self = $(this).data("resizable"), o = self.options, cs = self.size, os = self.originalSize, op = self.originalPosition, a = self.axis, ratio = o._aspectRatio || event.shiftKey; o.grid = typeof o.grid == "number" ? [o.grid, o.grid] : o.grid; var ox = Math.round((cs.width - os.width) / (o.grid[0] || 1)) * (o.grid[0] || 1), oy = Math.round((cs.height - os.height) / (o.grid[1] || 1)) * (o.grid[1] || 1); if (/^(se|s|e)$/.test(a)) { self.size.width = os.width + ox; self.size.height = os.height + oy; }
                else if (/^(ne)$/.test(a)) { self.size.width = os.width + ox; self.size.height = os.height + oy; self.position.top = op.top - oy; }
                else if (/^(sw)$/.test(a)) { self.size.width = os.width + ox; self.size.height = os.height + oy; self.position.left = op.left - ox; }
                else { self.size.width = os.width + ox; self.size.height = os.height + oy; self.position.top = op.top - oy; self.position.left = op.left - ox; }
            }
        }); var num = function (v) { return parseInt(v, 10) || 0; }; var isNumber = function (value) { return !isNaN(parseInt(value, 10)); };
    })(jQuery); (function ($, undefined) {
        $.widget("ui.selectable", $.ui.mouse, {
            options: { appendTo: 'body', autoRefresh: true, distance: 0, filter: '*', tolerance: 'touch' }, _create: function () { var self = this; this.element.addClass("ui-selectable"); this.dragged = false; var selectees; this.refresh = function () { selectees = $(self.options.filter, self.element[0]); selectees.each(function () { var $this = $(this); var pos = $this.offset(); $.data(this, "selectable-item", { element: this, $element: $this, left: pos.left, top: pos.top, right: pos.left + $this.outerWidth(), bottom: pos.top + $this.outerHeight(), startselected: false, selected: $this.hasClass('ui-selected'), selecting: $this.hasClass('ui-selecting'), unselecting: $this.hasClass('ui-unselecting') }); }); }; this.refresh(); this.selectees = selectees.addClass("ui-selectee"); this._mouseInit(); this.helper = $("<div class='ui-selectable-helper'></div>"); }, destroy: function () { this.selectees.removeClass("ui-selectee").removeData("selectable-item"); this.element.removeClass("ui-selectable ui-selectable-disabled").removeData("selectable").unbind(".selectable"); this._mouseDestroy(); return this; }, _mouseStart: function (event) {
                var self = this; this.opos = [event.pageX, event.pageY]; if (this.options.disabled)
                    return; var options = this.options; this.selectees = $(options.filter, this.element[0]); this._trigger("start", event); $(options.appendTo).append(this.helper); this.helper.css({ "left": event.clientX, "top": event.clientY, "width": 0, "height": 0 }); if (options.autoRefresh) { this.refresh(); }
                this.selectees.filter('.ui-selected').each(function () { var selectee = $.data(this, "selectable-item"); selectee.startselected = true; if (!event.metaKey) { selectee.$element.removeClass('ui-selected'); selectee.selected = false; selectee.$element.addClass('ui-unselecting'); selectee.unselecting = true; self._trigger("unselecting", event, { unselecting: selectee.element }); } }); $(event.target).parents().andSelf().each(function () {
                    var selectee = $.data(this, "selectable-item"); if (selectee) {
                        var doSelect = !event.metaKey || !selectee.$element.hasClass('ui-selected'); selectee.$element.removeClass(doSelect ? "ui-unselecting" : "ui-selected").addClass(doSelect ? "ui-selecting" : "ui-unselecting"); selectee.unselecting = !doSelect; selectee.selecting = doSelect; selectee.selected = doSelect; if (doSelect) { self._trigger("selecting", event, { selecting: selectee.element }); } else { self._trigger("unselecting", event, { unselecting: selectee.element }); }
                        return false;
                    }
                });
            }, _mouseDrag: function (event) {
                var self = this; this.dragged = true; if (this.options.disabled)
                    return; var options = this.options; var x1 = this.opos[0], y1 = this.opos[1], x2 = event.pageX, y2 = event.pageY; if (x1 > x2) { var tmp = x2; x2 = x1; x1 = tmp; }
                if (y1 > y2) { var tmp = y2; y2 = y1; y1 = tmp; }
                this.helper.css({ left: x1, top: y1, width: x2 - x1, height: y2 - y1 }); this.selectees.each(function () {
                    var selectee = $.data(this, "selectable-item"); if (!selectee || selectee.element == self.element[0])
                        return; var hit = false; if (options.tolerance == 'touch') { hit = (!(selectee.left > x2 || selectee.right < x1 || selectee.top > y2 || selectee.bottom < y1)); } else if (options.tolerance == 'fit') { hit = (selectee.left > x1 && selectee.right < x2 && selectee.top > y1 && selectee.bottom < y2); }
                    if (hit) {
                        if (selectee.selected) { selectee.$element.removeClass('ui-selected'); selectee.selected = false; }
                        if (selectee.unselecting) { selectee.$element.removeClass('ui-unselecting'); selectee.unselecting = false; }
                        if (!selectee.selecting) { selectee.$element.addClass('ui-selecting'); selectee.selecting = true; self._trigger("selecting", event, { selecting: selectee.element }); }
                    } else {
                        if (selectee.selecting) {
                            if (event.metaKey && selectee.startselected) { selectee.$element.removeClass('ui-selecting'); selectee.selecting = false; selectee.$element.addClass('ui-selected'); selectee.selected = true; } else {
                                selectee.$element.removeClass('ui-selecting'); selectee.selecting = false; if (selectee.startselected) { selectee.$element.addClass('ui-unselecting'); selectee.unselecting = true; }
                                self._trigger("unselecting", event, { unselecting: selectee.element });
                            }
                        }
                        if (selectee.selected) { if (!event.metaKey && !selectee.startselected) { selectee.$element.removeClass('ui-selected'); selectee.selected = false; selectee.$element.addClass('ui-unselecting'); selectee.unselecting = true; self._trigger("unselecting", event, { unselecting: selectee.element }); } }
                    }
                }); return false;
            }, _mouseStop: function (event) { var self = this; this.dragged = false; var options = this.options; $('.ui-unselecting', this.element[0]).each(function () { var selectee = $.data(this, "selectable-item"); selectee.$element.removeClass('ui-unselecting'); selectee.unselecting = false; selectee.startselected = false; self._trigger("unselected", event, { unselected: selectee.element }); }); $('.ui-selecting', this.element[0]).each(function () { var selectee = $.data(this, "selectable-item"); selectee.$element.removeClass('ui-selecting').addClass('ui-selected'); selectee.selecting = false; selectee.selected = true; selectee.startselected = true; self._trigger("selected", event, { selected: selectee.element }); }); this._trigger("stop", event); this.helper.remove(); return false; }
        }); $.extend($.ui.selectable, { version: "1.8.16" });
    })(jQuery); (function ($, undefined) {
        $.widget("ui.sortable", $.ui.mouse, {
            widgetEventPrefix: "sort", options: { appendTo: "parent", axis: false, connectWith: false, containment: false, cursor: 'auto', cursorAt: false, dropOnEmpty: true, forcePlaceholderSize: false, forceHelperSize: false, grid: false, handle: false, helper: "original", items: '> *', opacity: false, placeholder: false, revert: false, scroll: true, scrollSensitivity: 20, scrollSpeed: 20, scope: "default", tolerance: "intersect", zIndex: 1000 }, _create: function () { var o = this.options; this.containerCache = {}; this.element.addClass("ui-sortable"); this.refresh(); this.floating = this.items.length ? o.axis === 'x' || (/left|right/).test(this.items[0].item.css('float')) || (/inline|table-cell/).test(this.items[0].item.css('display')) : false; this.offset = this.element.offset(); this._mouseInit(); }, destroy: function () {
                this.element.removeClass("ui-sortable ui-sortable-disabled").removeData("sortable").unbind(".sortable"); this._mouseDestroy(); for (var i = this.items.length - 1; i >= 0; i--)
                    this.items[i].item.removeData("sortable-item"); return this;
            }, _setOption: function (key, value) {
                if (key === "disabled") {
                    this.options[key] = value; this.widget()
                    [value ? "addClass" : "removeClass"]("ui-sortable-disabled");
                } else { $.Widget.prototype._setOption.apply(this, arguments); }
            }, _mouseCapture: function (event, overrideHandle) {
                if (this.reverting) { return false; }
                if (this.options.disabled || this.options.type == 'static') return false; this._refreshItems(event); var currentItem = null, self = this, nodes = $(event.target).parents().each(function () { if ($.data(this, 'sortable-item') == self) { currentItem = $(this); return false; } }); if ($.data(event.target, 'sortable-item') == self) currentItem = $(event.target); if (!currentItem) return false; if (this.options.handle && !overrideHandle) { var validHandle = false; $(this.options.handle, currentItem).find("*").andSelf().each(function () { if (this == event.target) validHandle = true; }); if (!validHandle) return false; }
                this.currentItem = currentItem; this._removeCurrentsFromItems(); return true;
            }, _mouseStart: function (event, overrideHandle, noActivation) {
                var o = this.options, self = this; this.currentContainer = this; this.refreshPositions(); this.helper = this._createHelper(event); this._cacheHelperProportions(); this._cacheMargins(); this.scrollParent = this.helper.scrollParent(); this.offset = this.currentItem.offset(); this.offset = { top: this.offset.top - this.margins.top, left: this.offset.left - this.margins.left }; this.helper.css("position", "absolute"); this.cssPosition = this.helper.css("position"); $.extend(this.offset, {
                    click: { left: event.pageX - this.offset.left, top: event.pageY - this.offset.top }, parent: this._getParentOffset(), relative: this._getRelativeOffset()
                }); this.originalPosition = this._generatePosition(event); this.originalPageX = event.pageX; this.originalPageY = event.pageY; (o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt)); this.domPosition = { prev: this.currentItem.prev()[0], parent: this.currentItem.parent()[0] }; if (this.helper[0] != this.currentItem[0]) { this.currentItem.hide(); }
                this._createPlaceholder(); if (o.containment)
                    this._setContainment(); if (o.cursor) { if ($('body').css("cursor")) this._storedCursor = $('body').css("cursor"); $('body').css("cursor", o.cursor); }
                if (o.opacity) { if (this.helper.css("opacity")) this._storedOpacity = this.helper.css("opacity"); this.helper.css("opacity", o.opacity); }
                if (o.zIndex) { if (this.helper.css("zIndex")) this._storedZIndex = this.helper.css("zIndex"); this.helper.css("zIndex", o.zIndex); }
                if (this.scrollParent[0] != document && this.scrollParent[0].tagName != 'HTML')
                    this.overflowOffset = this.scrollParent.offset(); this._trigger("start", event, this._uiHash()); if (!this._preserveHelperProportions)
                        this._cacheHelperProportions(); if (!noActivation) { for (var i = this.containers.length - 1; i >= 0; i--) { this.containers[i]._trigger("activate", event, self._uiHash(this)); } }
                if ($.ui.ddmanager)
                    $.ui.ddmanager.current = this; if ($.ui.ddmanager && !o.dropBehaviour)
                        $.ui.ddmanager.prepareOffsets(this, event); this.dragging = true; this.helper.addClass("ui-sortable-helper"); this._mouseDrag(event); return true;
            }, _mouseDrag: function (event) {
                this.position = this._generatePosition(event); this.positionAbs = this._convertPositionTo("absolute"); if (!this.lastPositionAbs) { this.lastPositionAbs = this.positionAbs; }
                if (this.options.scroll) {
                    var o = this.options, scrolled = false; if (this.scrollParent[0] != document && this.scrollParent[0].tagName != 'HTML') {
                        if ((this.overflowOffset.top + this.scrollParent[0].offsetHeight) - event.pageY < o.scrollSensitivity)
                            this.scrollParent[0].scrollTop = scrolled = this.scrollParent[0].scrollTop + o.scrollSpeed; else if (event.pageY - this.overflowOffset.top < o.scrollSensitivity)
                                this.scrollParent[0].scrollTop = scrolled = this.scrollParent[0].scrollTop - o.scrollSpeed; if ((this.overflowOffset.left + this.scrollParent[0].offsetWidth) - event.pageX < o.scrollSensitivity)
                                    this.scrollParent[0].scrollLeft = scrolled = this.scrollParent[0].scrollLeft + o.scrollSpeed; else if (event.pageX - this.overflowOffset.left < o.scrollSensitivity)
                                        this.scrollParent[0].scrollLeft = scrolled = this.scrollParent[0].scrollLeft - o.scrollSpeed;
                    } else {
                        if (event.pageY - $(document).scrollTop() < o.scrollSensitivity)
                            scrolled = $(document).scrollTop($(document).scrollTop() - o.scrollSpeed); else if ($(window).height() - (event.pageY - $(document).scrollTop()) < o.scrollSensitivity)
                                scrolled = $(document).scrollTop($(document).scrollTop() + o.scrollSpeed); if (event.pageX - $(document).scrollLeft() < o.scrollSensitivity)
                                    scrolled = $(document).scrollLeft($(document).scrollLeft() - o.scrollSpeed); else if ($(window).width() - (event.pageX - $(document).scrollLeft()) < o.scrollSensitivity)
                                        scrolled = $(document).scrollLeft($(document).scrollLeft() + o.scrollSpeed);
                    }
                    if (scrolled !== false && $.ui.ddmanager && !o.dropBehaviour)
                        $.ui.ddmanager.prepareOffsets(this, event);
                }
                this.positionAbs = this._convertPositionTo("absolute"); if (!this.options.axis || this.options.axis != "y") this.helper[0].style.left = this.position.left + 'px'; if (!this.options.axis || this.options.axis != "x") this.helper[0].style.top = this.position.top + 'px'; for (var i = this.items.length - 1; i >= 0; i--) {
                    var item = this.items[i], itemElement = item.item[0], intersection = this._intersectsWithPointer(item); if (!intersection) continue; if (itemElement != this.currentItem[0]
                    && this.placeholder[intersection == 1 ? "next" : "prev"]()[0] != itemElement && !$.ui.contains(this.placeholder[0], itemElement)
                    && (this.options.type == 'semi-dynamic' ? !$.ui.contains(this.element[0], itemElement) : true)
                    ) {
                        this.direction = intersection == 1 ? "down" : "up"; if (this.options.tolerance == "pointer" || this._intersectsWithSides(item)) { this._rearrange(event, item); } else { break; }
                        this._trigger("change", event, this._uiHash()); break;
                    }
                }
                this._contactContainers(event); if ($.ui.ddmanager) $.ui.ddmanager.drag(this, event); this._trigger('sort', event, this._uiHash()); this.lastPositionAbs = this.positionAbs; return false;
            }, _mouseStop: function (event, noPropagation) {
                if (!event) return; if ($.ui.ddmanager && !this.options.dropBehaviour)
                    $.ui.ddmanager.drop(this, event); if (this.options.revert) { var self = this; var cur = self.placeholder.offset(); self.reverting = true; $(this.helper).animate({ left: cur.left - this.offset.parent.left - self.margins.left + (this.offsetParent[0] == document.body ? 0 : this.offsetParent[0].scrollLeft), top: cur.top - this.offset.parent.top - self.margins.top + (this.offsetParent[0] == document.body ? 0 : this.offsetParent[0].scrollTop) }, parseInt(this.options.revert, 10) || 500, function () { self._clear(event); }); } else { this._clear(event, noPropagation); }
                return false;
            }, cancel: function () {
                var self = this; if (this.dragging) {
                    this._mouseUp({ target: null }); if (this.options.helper == "original")
                        this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper"); else
                        this.currentItem.show(); for (var i = this.containers.length - 1; i >= 0; i--) { this.containers[i]._trigger("deactivate", null, self._uiHash(this)); if (this.containers[i].containerCache.over) { this.containers[i]._trigger("out", null, self._uiHash(this)); this.containers[i].containerCache.over = 0; } }
                }
                if (this.placeholder) { if (this.placeholder[0].parentNode) this.placeholder[0].parentNode.removeChild(this.placeholder[0]); if (this.options.helper != "original" && this.helper && this.helper[0].parentNode) this.helper.remove(); $.extend(this, { helper: null, dragging: false, reverting: false, _noFinalSort: null }); if (this.domPosition.prev) { $(this.domPosition.prev).after(this.currentItem); } else { $(this.domPosition.parent).prepend(this.currentItem); } }
                return this;
            }, serialize: function (o) {
                var items = this._getItemsAsjQuery(o && o.connected); var str = []; o = o || {}; $(items).each(function () { var res = ($(o.item || this).attr(o.attribute || 'id') || '').match(o.expression || (/(.+)[-=_](.+)/)); if (res) str.push((o.key || res[1] + '[]') + '=' + (o.key && o.expression ? res[1] : res[2])); }); if (!str.length && o.key) { str.push(o.key + '='); }
                return str.join('&');
            }, toArray: function (o) { var items = this._getItemsAsjQuery(o && o.connected); var ret = []; o = o || {}; items.each(function () { ret.push($(o.item || this).attr(o.attribute || 'id') || ''); }); return ret; }, _intersectsWith: function (item) {
                var x1 = this.positionAbs.left, x2 = x1 + this.helperProportions.width, y1 = this.positionAbs.top, y2 = y1 + this.helperProportions.height; var l = item.left, r = l + item.width, t = item.top, b = t + item.height; var dyClick = this.offset.click.top, dxClick = this.offset.click.left; var isOverElement = (y1 + dyClick) > t && (y1 + dyClick) < b && (x1 + dxClick) > l && (x1 + dxClick) < r; if (this.options.tolerance == "pointer" || this.options.forcePointerForContainers || (this.options.tolerance != "pointer" && this.helperProportions[this.floating ? 'width' : 'height'] > item[this.floating ? 'width' : 'height'])) { return isOverElement; } else {
                    return (l < x1 + (this.helperProportions.width / 2)
                    && x2 - (this.helperProportions.width / 2) < r && t < y1 + (this.helperProportions.height / 2)
                    && y2 - (this.helperProportions.height / 2) < b);
                }
            }, _intersectsWithPointer: function (item) {
                var isOverElementHeight = $.ui.isOverAxis(this.positionAbs.top + this.offset.click.top, item.top, item.height), isOverElementWidth = $.ui.isOverAxis(this.positionAbs.left + this.offset.click.left, item.left, item.width), isOverElement = isOverElementHeight && isOverElementWidth, verticalDirection = this._getDragVerticalDirection(), horizontalDirection = this._getDragHorizontalDirection(); if (!isOverElement)
                    return false; return this.floating ? (((horizontalDirection && horizontalDirection == "right") || verticalDirection == "down") ? 2 : 1) : (verticalDirection && (verticalDirection == "down" ? 2 : 1));
            }, _intersectsWithSides: function (item) { var isOverBottomHalf = $.ui.isOverAxis(this.positionAbs.top + this.offset.click.top, item.top + (item.height / 2), item.height), isOverRightHalf = $.ui.isOverAxis(this.positionAbs.left + this.offset.click.left, item.left + (item.width / 2), item.width), verticalDirection = this._getDragVerticalDirection(), horizontalDirection = this._getDragHorizontalDirection(); if (this.floating && horizontalDirection) { return ((horizontalDirection == "right" && isOverRightHalf) || (horizontalDirection == "left" && !isOverRightHalf)); } else { return verticalDirection && ((verticalDirection == "down" && isOverBottomHalf) || (verticalDirection == "up" && !isOverBottomHalf)); } }, _getDragVerticalDirection: function () { var delta = this.positionAbs.top - this.lastPositionAbs.top; return delta != 0 && (delta > 0 ? "down" : "up"); }, _getDragHorizontalDirection: function () { var delta = this.positionAbs.left - this.lastPositionAbs.left; return delta != 0 && (delta > 0 ? "right" : "left"); }, refresh: function (event) { this._refreshItems(event); this.refreshPositions(); return this; }, _connectWith: function () { var options = this.options; return options.connectWith.constructor == String ? [options.connectWith] : options.connectWith; }, _getItemsAsjQuery: function (connected) {
                var self = this; var items = []; var queries = []; var connectWith = this._connectWith(); if (connectWith && connected) { for (var i = connectWith.length - 1; i >= 0; i--) { var cur = $(connectWith[i]); for (var j = cur.length - 1; j >= 0; j--) { var inst = $.data(cur[j], 'sortable'); if (inst && inst != this && !inst.options.disabled) { queries.push([$.isFunction(inst.options.items) ? inst.options.items.call(inst.element) : $(inst.options.items, inst.element).not(".ui-sortable-helper").not('.ui-sortable-placeholder'), inst]); } }; }; }
                queries.push([$.isFunction(this.options.items) ? this.options.items.call(this.element, null, { options: this.options, item: this.currentItem }) : $(this.options.items, this.element).not(".ui-sortable-helper").not('.ui-sortable-placeholder'), this]); for (var i = queries.length - 1; i >= 0; i--) { queries[i][0].each(function () { items.push(this); }); }; return $(items);
            }, _removeCurrentsFromItems: function () {
                var list = this.currentItem.find(":data(sortable-item)"); for (var i = 0; i < this.items.length; i++) {
                    for (var j = 0; j < list.length; j++) {
                        if (list[j] == this.items[i].item[0])
                            this.items.splice(i, 1);
                    };
                };
            }, _refreshItems: function (event) {
                this.items = []; this.containers = [this]; var items = this.items; var self = this; var queries = [[$.isFunction(this.options.items) ? this.options.items.call(this.element[0], event, { item: this.currentItem }) : $(this.options.items, this.element), this]]; var connectWith = this._connectWith(); if (connectWith) { for (var i = connectWith.length - 1; i >= 0; i--) { var cur = $(connectWith[i]); for (var j = cur.length - 1; j >= 0; j--) { var inst = $.data(cur[j], 'sortable'); if (inst && inst != this && !inst.options.disabled) { queries.push([$.isFunction(inst.options.items) ? inst.options.items.call(inst.element[0], event, { item: this.currentItem }) : $(inst.options.items, inst.element), inst]); this.containers.push(inst); } }; }; }
                for (var i = queries.length - 1; i >= 0; i--) { var targetData = queries[i][1]; var _queries = queries[i][0]; for (var j = 0, queriesLength = _queries.length; j < queriesLength; j++) { var item = $(_queries[j]); item.data('sortable-item', targetData); items.push({ item: item, instance: targetData, width: 0, height: 0, left: 0, top: 0 }); }; };
            }, refreshPositions: function (fast) {
                if (this.offsetParent && this.helper) { this.offset.parent = this._getParentOffset(); }
                for (var i = this.items.length - 1; i >= 0; i--) {
                    var item = this.items[i]; if (item.instance != this.currentContainer && this.currentContainer && item.item[0] != this.currentItem[0])
                        continue; var t = this.options.toleranceElement ? $(this.options.toleranceElement, item.item) : item.item; if (!fast) { item.width = t.outerWidth(); item.height = t.outerHeight(); }
                    var p = t.offset(); item.left = p.left; item.top = p.top;
                }; if (this.options.custom && this.options.custom.refreshContainers) { this.options.custom.refreshContainers.call(this); } else { for (var i = this.containers.length - 1; i >= 0; i--) { var p = this.containers[i].element.offset(); this.containers[i].containerCache.left = p.left; this.containers[i].containerCache.top = p.top; this.containers[i].containerCache.width = this.containers[i].element.outerWidth(); this.containers[i].containerCache.height = this.containers[i].element.outerHeight(); }; }
                return this;
            }, _createPlaceholder: function (that) {
                var self = that || this, o = self.options; if (!o.placeholder || o.placeholder.constructor == String) {
                    var className = o.placeholder; o.placeholder = {
                        element: function () {
                            var el = $(document.createElement(self.currentItem[0].nodeName)).addClass(className || self.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper")[0]; if (!className)
                                el.style.visibility = "hidden"; return el;
                        }, update: function (container, p) { if (className && !o.forcePlaceholderSize) return; if (!p.height()) { p.height(self.currentItem.innerHeight() - parseInt(self.currentItem.css('paddingTop') || 0, 10) - parseInt(self.currentItem.css('paddingBottom') || 0, 10)); }; if (!p.width()) { p.width(self.currentItem.innerWidth() - parseInt(self.currentItem.css('paddingLeft') || 0, 10) - parseInt(self.currentItem.css('paddingRight') || 0, 10)); }; }
                    };
                }
                self.placeholder = $(o.placeholder.element.call(self.element, self.currentItem)); self.currentItem.after(self.placeholder); o.placeholder.update(self, self.placeholder);
            }, _contactContainers: function (event) {
                var innermostContainer = null, innermostIndex = null; for (var i = this.containers.length - 1; i >= 0; i--) {
                    if ($.ui.contains(this.currentItem[0], this.containers[i].element[0]))
                        continue; if (this._intersectsWith(this.containers[i].containerCache)) {
                            if (innermostContainer && $.ui.contains(this.containers[i].element[0], innermostContainer.element[0]))
                                continue; innermostContainer = this.containers[i]; innermostIndex = i;
                        } else { if (this.containers[i].containerCache.over) { this.containers[i]._trigger("out", event, this._uiHash(this)); this.containers[i].containerCache.over = 0; } }
                }
                if (!innermostContainer) return; if (this.containers.length === 1) { this.containers[innermostIndex]._trigger("over", event, this._uiHash(this)); this.containers[innermostIndex].containerCache.over = 1; } else if (this.currentContainer != this.containers[innermostIndex]) {
                    var dist = 10000; var itemWithLeastDistance = null; var base = this.positionAbs[this.containers[innermostIndex].floating ? 'left' : 'top']; for (var j = this.items.length - 1; j >= 0; j--) { if (!$.ui.contains(this.containers[innermostIndex].element[0], this.items[j].item[0])) continue; var cur = this.items[j][this.containers[innermostIndex].floating ? 'left' : 'top']; if (Math.abs(cur - base) < dist) { dist = Math.abs(cur - base); itemWithLeastDistance = this.items[j]; } }
                    if (!itemWithLeastDistance && !this.options.dropOnEmpty)
                        return; this.currentContainer = this.containers[innermostIndex]; itemWithLeastDistance ? this._rearrange(event, itemWithLeastDistance, null, true) : this._rearrange(event, null, this.containers[innermostIndex].element, true); this._trigger("change", event, this._uiHash()); this.containers[innermostIndex]._trigger("change", event, this._uiHash(this)); this.options.placeholder.update(this.currentContainer, this.placeholder); this.containers[innermostIndex]._trigger("over", event, this._uiHash(this)); this.containers[innermostIndex].containerCache.over = 1;
                }
            }, _createHelper: function (event) {
                var o = this.options; var helper = $.isFunction(o.helper) ? $(o.helper.apply(this.element[0], [event, this.currentItem])) : (o.helper == 'clone' ? this.currentItem.clone() : this.currentItem); if (!helper.parents('body').length)
                    $(o.appendTo != 'parent' ? o.appendTo : this.currentItem[0].parentNode)[0].appendChild(helper[0]); if (helper[0] == this.currentItem[0])
                        this._storedCSS = { width: this.currentItem[0].style.width, height: this.currentItem[0].style.height, position: this.currentItem.css("position"), top: this.currentItem.css("top"), left: this.currentItem.css("left") }; if (helper[0].style.width == '' || o.forceHelperSize) helper.width(this.currentItem.width()); if (helper[0].style.height == '' || o.forceHelperSize) helper.height(this.currentItem.height()); return helper;
            }, _adjustOffsetFromHelper: function (obj) {
                if (typeof obj == 'string') { obj = obj.split(' '); }
                if ($.isArray(obj)) { obj = { left: +obj[0], top: +obj[1] || 0 }; }
                if ('left' in obj) { this.offset.click.left = obj.left + this.margins.left; }
                if ('right' in obj) { this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left; }
                if ('top' in obj) { this.offset.click.top = obj.top + this.margins.top; }
                if ('bottom' in obj) { this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top; }
            }, _getParentOffset: function () {
                this.offsetParent = this.helper.offsetParent(); var po = this.offsetParent.offset(); if (this.cssPosition == 'absolute' && this.scrollParent[0] != document && $.ui.contains(this.scrollParent[0], this.offsetParent[0])) { po.left += this.scrollParent.scrollLeft(); po.top += this.scrollParent.scrollTop(); }
                if ((this.offsetParent[0] == document.body)
                || (this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() == 'html' && $.browser.msie))
                    po = { top: 0, left: 0 }; return { top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0), left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0) };
            }, _getRelativeOffset: function () { if (this.cssPosition == "relative") { var p = this.currentItem.position(); return { top: p.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(), left: p.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft() }; } else { return { top: 0, left: 0 }; } }, _cacheMargins: function () { this.margins = { left: (parseInt(this.currentItem.css("marginLeft"), 10) || 0), top: (parseInt(this.currentItem.css("marginTop"), 10) || 0) }; }, _cacheHelperProportions: function () { this.helperProportions = { width: this.helper.outerWidth(), height: this.helper.outerHeight() }; }, _setContainment: function () { var o = this.options; if (o.containment == 'parent') o.containment = this.helper[0].parentNode; if (o.containment == 'document' || o.containment == 'window') this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, $(o.containment == 'document' ? document : window).width() - this.helperProportions.width - this.margins.left, ($(o.containment == 'document' ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]; if (!(/^(document|window|parent)$/).test(o.containment)) { var ce = $(o.containment)[0]; var co = $(o.containment).offset(); var over = ($(ce).css("overflow") != 'hidden'); this.containment = [co.left + (parseInt($(ce).css("borderLeftWidth"), 10) || 0) + (parseInt($(ce).css("paddingLeft"), 10) || 0) - this.margins.left, co.top + (parseInt($(ce).css("borderTopWidth"), 10) || 0) + (parseInt($(ce).css("paddingTop"), 10) || 0) - this.margins.top, co.left + (over ? Math.max(ce.scrollWidth, ce.offsetWidth) : ce.offsetWidth) - (parseInt($(ce).css("borderLeftWidth"), 10) || 0) - (parseInt($(ce).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, co.top + (over ? Math.max(ce.scrollHeight, ce.offsetHeight) : ce.offsetHeight) - (parseInt($(ce).css("borderTopWidth"), 10) || 0) - (parseInt($(ce).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top]; } }, _convertPositionTo: function (d, pos) { if (!pos) pos = this.position; var mod = d == "absolute" ? 1 : -1; var o = this.options, scroll = this.cssPosition == 'absolute' && !(this.scrollParent[0] != document && $.ui.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, scrollIsRootNode = (/(html|body)/i).test(scroll[0].tagName); return { top: (pos.top + this.offset.relative.top * mod + this.offset.parent.top * mod - ($.browser.safari && this.cssPosition == 'fixed' ? 0 : (this.cssPosition == 'fixed' ? -this.scrollParent.scrollTop() : (scrollIsRootNode ? 0 : scroll.scrollTop())) * mod)), left: (pos.left + this.offset.relative.left * mod + this.offset.parent.left * mod - ($.browser.safari && this.cssPosition == 'fixed' ? 0 : (this.cssPosition == 'fixed' ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft()) * mod)) }; }, _generatePosition: function (event) {
                var o = this.options, scroll = this.cssPosition == 'absolute' && !(this.scrollParent[0] != document && $.ui.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, scrollIsRootNode = (/(html|body)/i).test(scroll[0].tagName); if (this.cssPosition == 'relative' && !(this.scrollParent[0] != document && this.scrollParent[0] != this.offsetParent[0])) { this.offset.relative = this._getRelativeOffset(); }
                var pageX = event.pageX; var pageY = event.pageY; if (this.originalPosition) {
                    if (this.containment) { if (event.pageX - this.offset.click.left < this.containment[0]) pageX = this.containment[0] + this.offset.click.left; if (event.pageY - this.offset.click.top < this.containment[1]) pageY = this.containment[1] + this.offset.click.top; if (event.pageX - this.offset.click.left > this.containment[2]) pageX = this.containment[2] + this.offset.click.left; if (event.pageY - this.offset.click.top > this.containment[3]) pageY = this.containment[3] + this.offset.click.top; }
                    if (o.grid) { var top = this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1]; pageY = this.containment ? (!(top - this.offset.click.top < this.containment[1] || top - this.offset.click.top > this.containment[3]) ? top : (!(top - this.offset.click.top < this.containment[1]) ? top - o.grid[1] : top + o.grid[1])) : top; var left = this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0]; pageX = this.containment ? (!(left - this.offset.click.left < this.containment[0] || left - this.offset.click.left > this.containment[2]) ? left : (!(left - this.offset.click.left < this.containment[0]) ? left - o.grid[0] : left + o.grid[0])) : left; }
                }
                return { top: (pageY - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ($.browser.safari && this.cssPosition == 'fixed' ? 0 : (this.cssPosition == 'fixed' ? -this.scrollParent.scrollTop() : (scrollIsRootNode ? 0 : scroll.scrollTop())))), left: (pageX - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ($.browser.safari && this.cssPosition == 'fixed' ? 0 : (this.cssPosition == 'fixed' ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft()))) };
            }, _rearrange: function (event, i, a, hardRefresh) { a ? a[0].appendChild(this.placeholder[0]) : i.item[0].parentNode.insertBefore(this.placeholder[0], (this.direction == 'down' ? i.item[0] : i.item[0].nextSibling)); this.counter = this.counter ? ++this.counter : 1; var self = this, counter = this.counter; window.setTimeout(function () { if (counter == self.counter) self.refreshPositions(!hardRefresh); }, 0); }, _clear: function (event, noPropagation) {
                this.reverting = false; var delayedTriggers = [], self = this; if (!this._noFinalSort && this.currentItem.parent().length) this.placeholder.before(this.currentItem); this._noFinalSort = null; if (this.helper[0] == this.currentItem[0]) {
                    for (var i in this._storedCSS) { if (this._storedCSS[i] == 'auto' || this._storedCSS[i] == 'static') this._storedCSS[i] = ''; }
                    this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper");
                } else { this.currentItem.show(); }
                if (this.fromOutside && !noPropagation) delayedTriggers.push(function (event) { this._trigger("receive", event, this._uiHash(this.fromOutside)); }); if ((this.fromOutside || this.domPosition.prev != this.currentItem.prev().not(".ui-sortable-helper")[0] || this.domPosition.parent != this.currentItem.parent()[0]) && !noPropagation) delayedTriggers.push(function (event) { this._trigger("update", event, this._uiHash()); }); if (!$.ui.contains(this.element[0], this.currentItem[0])) { if (!noPropagation) delayedTriggers.push(function (event) { this._trigger("remove", event, this._uiHash()); }); for (var i = this.containers.length - 1; i >= 0; i--) { if ($.ui.contains(this.containers[i].element[0], this.currentItem[0]) && !noPropagation) { delayedTriggers.push((function (c) { return function (event) { c._trigger("receive", event, this._uiHash(this)); }; }).call(this, this.containers[i])); delayedTriggers.push((function (c) { return function (event) { c._trigger("update", event, this._uiHash(this)); }; }).call(this, this.containers[i])); } }; }; for (var i = this.containers.length - 1; i >= 0; i--) { if (!noPropagation) delayedTriggers.push((function (c) { return function (event) { c._trigger("deactivate", event, this._uiHash(this)); }; }).call(this, this.containers[i])); if (this.containers[i].containerCache.over) { delayedTriggers.push((function (c) { return function (event) { c._trigger("out", event, this._uiHash(this)); }; }).call(this, this.containers[i])); this.containers[i].containerCache.over = 0; } }
                if (this._storedCursor) $('body').css("cursor", this._storedCursor); if (this._storedOpacity) this.helper.css("opacity", this._storedOpacity); if (this._storedZIndex) this.helper.css("zIndex", this._storedZIndex == 'auto' ? '' : this._storedZIndex); this.dragging = false; if (this.cancelHelperRemoval) {
                    if (!noPropagation) { this._trigger("beforeStop", event, this._uiHash()); for (var i = 0; i < delayedTriggers.length; i++) { delayedTriggers[i].call(this, event); }; this._trigger("stop", event, this._uiHash()); }
                    return false;
                }
                if (!noPropagation) this._trigger("beforeStop", event, this._uiHash()); this.placeholder[0].parentNode.removeChild(this.placeholder[0]); if (this.helper[0] != this.currentItem[0]) this.helper.remove(); this.helper = null; if (!noPropagation) { for (var i = 0; i < delayedTriggers.length; i++) { delayedTriggers[i].call(this, event); }; this._trigger("stop", event, this._uiHash()); }
                this.fromOutside = false; return true;
            }, _trigger: function () { if ($.Widget.prototype._trigger.apply(this, arguments) === false) { this.cancel(); } }, _uiHash: function (inst) { var self = inst || this; return { helper: self.helper, placeholder: self.placeholder || $([]), position: self.position, originalPosition: self.originalPosition, offset: self.positionAbs, item: self.currentItem, sender: inst ? inst.element : null }; }
        }); $.extend($.ui.sortable, { version: "1.8.16" });
    })(jQuery); (function ($, undefined) {
        $.widget("ui.accordion", {
            options: { active: 0, animated: "slide", autoHeight: true, clearStyle: false, collapsible: false, event: "click", fillSpace: false, header: "> li > :first-child,> :not(li):even", icons: { header: "ui-icon-triangle-1-e", headerSelected: "ui-icon-triangle-1-s" }, navigation: false, navigationFilter: function () { return this.href.toLowerCase() === location.href.toLowerCase(); } }, _create: function () {
                var self = this, options = self.options; self.running = 0; self.element.addClass("ui-accordion ui-widget ui-helper-reset")
                .children("li").addClass("ui-accordion-li-fix"); self.headers = self.element.find(options.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all").bind("mouseenter.accordion", function () {
                    if (options.disabled) { return; }
                    $(this).addClass("ui-state-hover");
                }).bind("mouseleave.accordion", function () {
                    if (options.disabled) { return; }
                    $(this).removeClass("ui-state-hover");
                }).bind("focus.accordion", function () {
                    if (options.disabled) { return; }
                    $(this).addClass("ui-state-focus");
                }).bind("blur.accordion", function () {
                    if (options.disabled) { return; }
                    $(this).removeClass("ui-state-focus");
                }); self.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"); if (options.navigation) { var current = self.element.find("a").filter(options.navigationFilter).eq(0); if (current.length) { var header = current.closest(".ui-accordion-header"); if (header.length) { self.active = header; } else { self.active = current.closest(".ui-accordion-content").prev(); } } }
                self.active = self._findActive(self.active || options.active).addClass("ui-state-default ui-state-active").toggleClass("ui-corner-all").toggleClass("ui-corner-top"); self.active.next().addClass("ui-accordion-content-active"); self._createIcons(); self.resize(); self.element.attr("role", "tablist"); self.headers.attr("role", "tab").bind("keydown.accordion", function (event) { return self._keydown(event); }).next().attr("role", "tabpanel"); self.headers.not(self.active || "").attr({ "aria-expanded": "false", "aria-selected": "false", tabIndex: -1 }).next().hide(); if (!self.active.length) { self.headers.eq(0).attr("tabIndex", 0); } else { self.active.attr({ "aria-expanded": "true", "aria-selected": "true", tabIndex: 0 }); }
                if (!$.browser.safari) { self.headers.find("a").attr("tabIndex", -1); }
                if (options.event) { self.headers.bind(options.event.split(" ").join(".accordion ") + ".accordion", function (event) { self._clickHandler.call(self, event, this); event.preventDefault(); }); }
            }, _createIcons: function () { var options = this.options; if (options.icons) { $("<span></span>").addClass("ui-icon " + options.icons.header).prependTo(this.headers); this.active.children(".ui-icon").toggleClass(options.icons.header).toggleClass(options.icons.headerSelected); this.element.addClass("ui-accordion-icons"); } }, _destroyIcons: function () { this.headers.children(".ui-icon").remove(); this.element.removeClass("ui-accordion-icons"); }, destroy: function () {
                var options = this.options; this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"); this.headers.unbind(".accordion").removeClass("ui-accordion-header ui-accordion-disabled ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("tabIndex"); this.headers.find("a").removeAttr("tabIndex"); this._destroyIcons(); var contents = this.headers.next().css("display", "").removeAttr("role").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-accordion-disabled ui-state-disabled"); if (options.autoHeight || options.fillHeight) { contents.css("height", ""); }
                return $.Widget.prototype.destroy.call(this);
            }, _setOption: function (key, value) {
                $.Widget.prototype._setOption.apply(this, arguments); if (key == "active") { this.activate(value); }
                if (key == "icons") { this._destroyIcons(); if (value) { this._createIcons(); } }
                if (key == "disabled") {
                    this.headers.add(this.headers.next())
                    [value ? "addClass" : "removeClass"]("ui-accordion-disabled ui-state-disabled");
                }
            }, _keydown: function (event) {
                if (this.options.disabled || event.altKey || event.ctrlKey) { return; }
                var keyCode = $.ui.keyCode, length = this.headers.length, currentIndex = this.headers.index(event.target), toFocus = false; switch (event.keyCode) { case keyCode.RIGHT: case keyCode.DOWN: toFocus = this.headers[(currentIndex + 1) % length]; break; case keyCode.LEFT: case keyCode.UP: toFocus = this.headers[(currentIndex - 1 + length) % length]; break; case keyCode.SPACE: case keyCode.ENTER: this._clickHandler({ target: event.target }, event.target); event.preventDefault(); }
                if (toFocus) { $(event.target).attr("tabIndex", -1); $(toFocus).attr("tabIndex", 0); toFocus.focus(); return false; }
                return true;
            }, resize: function () {
                var options = this.options, maxHeight; if (options.fillSpace) {
                    if ($.browser.msie) { var defOverflow = this.element.parent().css("overflow"); this.element.parent().css("overflow", "hidden"); }
                    maxHeight = this.element.parent().height(); if ($.browser.msie) { this.element.parent().css("overflow", defOverflow); }
                    this.headers.each(function () { maxHeight -= $(this).outerHeight(true); }); this.headers.next().each(function () {
                        $(this).height(Math.max(0, maxHeight -
                        $(this).innerHeight() + $(this).height()));
                    }).css("overflow", "auto");
                } else if (options.autoHeight) { maxHeight = 0; this.headers.next().each(function () { maxHeight = Math.max(maxHeight, $(this).height("").height()); }).height(maxHeight); }
                return this;
            }, activate: function (index) { this.options.active = index; var active = this._findActive(index)[0]; this._clickHandler({ target: active }, active); return this; }, _findActive: function (selector) { return selector ? typeof selector === "number" ? this.headers.filter(":eq(" + selector + ")") : this.headers.not(this.headers.not(selector)) : selector === false ? $([]) : this.headers.filter(":eq(0)"); }, _clickHandler: function (event, target) {
                var options = this.options; if (options.disabled) { return; }
                if (!event.target) {
                    if (!options.collapsible) { return; }
                    this.active.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").children(".ui-icon").removeClass(options.icons.headerSelected).addClass(options.icons.header); this.active.next().addClass("ui-accordion-content-active"); var toHide = this.active.next(), data = { options: options, newHeader: $([]), oldHeader: options.active, newContent: $([]), oldContent: toHide }, toShow = (this.active = $([])); this._toggle(toShow, toHide, data); return;
                }
                var clicked = $(event.currentTarget || target), clickedIsActive = clicked[0] === this.active[0]; options.active = options.collapsible && clickedIsActive ? false : this.headers.index(clicked); if (this.running || (!options.collapsible && clickedIsActive)) { return; }
                var active = this.active, toShow = clicked.next(), toHide = this.active.next(), data = { options: options, newHeader: clickedIsActive && options.collapsible ? $([]) : clicked, oldHeader: this.active, newContent: clickedIsActive && options.collapsible ? $([]) : toShow, oldContent: toHide }, down = this.headers.index(this.active[0]) > this.headers.index(clicked[0]); this.active = clickedIsActive ? $([]) : clicked; this._toggle(toShow, toHide, data, clickedIsActive, down); active.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").children(".ui-icon").removeClass(options.icons.headerSelected).addClass(options.icons.header); if (!clickedIsActive) { clicked.removeClass("ui-state-default ui-corner-all").addClass("ui-state-active ui-corner-top").children(".ui-icon").removeClass(options.icons.header).addClass(options.icons.headerSelected); clicked.next().addClass("ui-accordion-content-active"); }
                return;
            }, _toggle: function (toShow, toHide, data, clickedIsActive, down) {
                var self = this, options = self.options; self.toShow = toShow; self.toHide = toHide; self.data = data; var complete = function () {
                    if (!self) { return; }
                    return self._completed.apply(self, arguments);
                }; self._trigger("changestart", null, self.data); self.running = toHide.size() === 0 ? toShow.size() : toHide.size(); if (options.animated) {
                    var animOptions = {}; if (options.collapsible && clickedIsActive) { animOptions = { toShow: $([]), toHide: toHide, complete: complete, down: down, autoHeight: options.autoHeight || options.fillSpace }; } else { animOptions = { toShow: toShow, toHide: toHide, complete: complete, down: down, autoHeight: options.autoHeight || options.fillSpace }; }
                    if (!options.proxied) { options.proxied = options.animated; }
                    if (!options.proxiedDuration) { options.proxiedDuration = options.duration; }
                    options.animated = $.isFunction(options.proxied) ? options.proxied(animOptions) : options.proxied; options.duration = $.isFunction(options.proxiedDuration) ? options.proxiedDuration(animOptions) : options.proxiedDuration; var animations = $.ui.accordion.animations, duration = options.duration, easing = options.animated; if (easing && !animations[easing] && !$.easing[easing]) { easing = "slide"; }
                    if (!animations[easing]) { animations[easing] = function (options) { this.slide(options, { easing: easing, duration: duration || 700 }); }; }
                    animations[easing](animOptions);
                } else {
                    if (options.collapsible && clickedIsActive) { toShow.toggle(); } else { toHide.hide(); toShow.show(); }
                    complete(true);
                }
                toHide.prev().attr({ "aria-expanded": "false", "aria-selected": "false", tabIndex: -1 }).blur(); toShow.prev().attr({ "aria-expanded": "true", "aria-selected": "true", tabIndex: 0 }).focus();
            }, _completed: function (cancel) {
                this.running = cancel ? 0 : --this.running; if (this.running) { return; }
                if (this.options.clearStyle) { this.toShow.add(this.toHide).css({ height: "", overflow: "" }); }
                this.toHide.removeClass("ui-accordion-content-active"); if (this.toHide.length) { this.toHide.parent()[0].className = this.toHide.parent()[0].className; }
                this._trigger("change", null, this.data);
            }
        }); $.extend($.ui.accordion, {
            version: "1.8.16", animations: {
                slide: function (options, additions) {
                    options = $.extend({ easing: "swing", duration: 300 }, options, additions); if (!options.toHide.size()) { options.toShow.animate({ height: "show", paddingTop: "show", paddingBottom: "show" }, options); return; }
                    if (!options.toShow.size()) { options.toHide.animate({ height: "hide", paddingTop: "hide", paddingBottom: "hide" }, options); return; }
                    var overflow = options.toShow.css("overflow"), percentDone = 0, showProps = {}, hideProps = {}, fxAttrs = ["height", "paddingTop", "paddingBottom"], originalWidth; var s = options.toShow; originalWidth = s[0].style.width; s.width(parseInt(s.parent().width(), 10)
                    - parseInt(s.css("paddingLeft"), 10)
                    - parseInt(s.css("paddingRight"), 10)
                    - (parseInt(s.css("borderLeftWidth"), 10) || 0)
                    - (parseInt(s.css("borderRightWidth"), 10) || 0)); $.each(fxAttrs, function (i, prop) { hideProps[prop] = "hide"; var parts = ("" + $.css(options.toShow[0], prop)).match(/^([\d+-.]+)(.*)$/); showProps[prop] = { value: parts[1], unit: parts[2] || "px" }; }); options.toShow.css({ height: 0, overflow: "hidden" }).show(); options.toHide.filter(":hidden").each(options.complete).end().filter(":visible").animate(hideProps, {
                        step: function (now, settings) {
                            if (settings.prop == "height") { percentDone = (settings.end - settings.start === 0) ? 0 : (settings.now - settings.start) / (settings.end - settings.start); }
                            options.toShow[0].style[settings.prop] = (percentDone * showProps[settings.prop].value)
                            + showProps[settings.prop].unit;
                        }, duration: options.duration, easing: options.easing, complete: function () {
                            if (!options.autoHeight) { options.toShow.css("height", ""); }
                            options.toShow.css({ width: originalWidth, overflow: overflow }); options.complete();
                        }
                    });
                }, bounceslide: function (options) { this.slide(options, { easing: options.down ? "easeOutBounce" : "swing", duration: options.down ? 1000 : 200 }); }
            }
        });
    })(jQuery); (function ($, undefined) {
        var requestIndex = 0; $.widget("ui.autocomplete", {
            options: { appendTo: "body", autoFocus: false, delay: 300, minLength: 1, position: { my: "left top", at: "left bottom", collision: "none" }, source: null }, pending: 0, _create: function () {
                var self = this, doc = this.element[0].ownerDocument, suppressKeyPress; this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off")
                .attr({ role: "textbox", "aria-autocomplete": "list", "aria-haspopup": "true" }).bind("keydown.autocomplete", function (event) {
                    if (self.options.disabled || self.element.propAttr("readOnly")) { return; }
                    suppressKeyPress = false; var keyCode = $.ui.keyCode; switch (event.keyCode) {
                        case keyCode.PAGE_UP: self._move("previousPage", event); break; case keyCode.PAGE_DOWN: self._move("nextPage", event); break; case keyCode.UP: self._move("previous", event); event.preventDefault(); break; case keyCode.DOWN: self._move("next", event); event.preventDefault(); break; case keyCode.ENTER: case keyCode.NUMPAD_ENTER: if (self.menu.active) { suppressKeyPress = true; event.preventDefault(); }
                        case keyCode.TAB: if (!self.menu.active) { return; }
                            self.menu.select(event); break; case keyCode.ESCAPE: self.element.val(self.term); self.close(event); break; default: clearTimeout(self.searching); self.searching = setTimeout(function () { if (self.term != self.element.val()) { self.selectedItem = null; self.search(null, event); } }, self.options.delay); break;
                    }
                }).bind("keypress.autocomplete", function (event) { if (suppressKeyPress) { suppressKeyPress = false; event.preventDefault(); } }).bind("focus.autocomplete", function () {
                    if (self.options.disabled) { return; }
                    self.selectedItem = null; self.previous = self.element.val();
                }).bind("blur.autocomplete", function (event) {
                    if (self.options.disabled) { return; }
                    clearTimeout(self.searching); self.closing = setTimeout(function () { self.close(event); self._change(event); }, 150);
                }); this._initSource(); this.response = function () { return self._response.apply(self, arguments); }; this.menu = $("<ul></ul>").addClass("ui-autocomplete").appendTo($(this.options.appendTo || "body", doc)[0])
                .mousedown(function (event) {
                    var menuElement = self.menu.element[0]; if (!$(event.target).closest(".ui-menu-item").length) { setTimeout(function () { $(document).one('mousedown', function (event) { if (event.target !== self.element[0] && event.target !== menuElement && !$.ui.contains(menuElement, event.target)) { self.close(); } }); }, 1); }
                    setTimeout(function () { clearTimeout(self.closing); }, 13);
                }).menu({
                    focus: function (event, ui) { var item = ui.item.data("item.autocomplete"); if (false !== self._trigger("focus", event, { item: item })) { if (/^key/.test(event.originalEvent.type)) { self.element.val(item.value); } } }, selected: function (event, ui) {
                        var item = ui.item.data("item.autocomplete"), previous = self.previous; if (self.element[0] !== doc.activeElement) { self.element.focus(); self.previous = previous; setTimeout(function () { self.previous = previous; self.selectedItem = item; }, 1); }
                        if (false !== self._trigger("select", event, { item: item })) { self.element.val(item.value); }
                        self.term = self.element.val(); self.close(event); self.selectedItem = item;
                    }, blur: function (event, ui) { if (self.menu.element.is(":visible") && (self.element.val() !== self.term)) { self.element.val(self.term); } }
                }).zIndex(this.element.zIndex() + 1)
                .css({ top: 0, left: 0 }).hide().data("menu"); if ($.fn.bgiframe) { this.menu.element.bgiframe(); }
            }, destroy: function () { this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete").removeAttr("role").removeAttr("aria-autocomplete").removeAttr("aria-haspopup"); this.menu.element.remove(); $.Widget.prototype.destroy.call(this); }, _setOption: function (key, value) {
                $.Widget.prototype._setOption.apply(this, arguments); if (key === "source") { this._initSource(); }
                if (key === "appendTo") { this.menu.element.appendTo($(value || "body", this.element[0].ownerDocument)[0]) }
                if (key === "disabled" && value && this.xhr) { this.xhr.abort(); }
            }, _initSource: function () {
                var self = this, array, url; if ($.isArray(this.options.source)) { array = this.options.source; this.source = function (request, response) { response($.ui.autocomplete.filter(array, request.term)); }; } else if (typeof this.options.source === "string") {
                    url = this.options.source; this.source = function (request, response) {
                        if (self.xhr) { self.xhr.abort(); }
                        self.xhr = $.ajax({ url: url, data: request, dataType: "json", autocompleteRequest: ++requestIndex, success: function (data, status) { if (this.autocompleteRequest === requestIndex) { response(data); } }, error: function () { if (this.autocompleteRequest === requestIndex) { response([]); } } });
                    };
                } else { this.source = this.options.source; }
            }, search: function (value, event) {
                value = value != null ? value : this.element.val(); this.term = this.element.val(); if (value.length < this.options.minLength) { return this.close(event); }
                clearTimeout(this.closing); if (this._trigger("search", event) === false) { return; }
                return this._search(value);
            }, _search: function (value) { this.pending++; this.element.addClass("ui-autocomplete-loading"); this.source({ term: value }, this.response); }, _response: function (content) {
                if (!this.options.disabled && content && content.length) { content = this._normalize(content); this._suggest(content); this._trigger("open"); } else { this.close(); }
                this.pending--; if (!this.pending) { this.element.removeClass("ui-autocomplete-loading"); }
            }, close: function (event) { clearTimeout(this.closing); if (this.menu.element.is(":visible")) { this.menu.element.hide(); this.menu.deactivate(); this._trigger("close", event); } }, _change: function (event) { if (this.previous !== this.element.val()) { this._trigger("change", event, { item: this.selectedItem }); } }, _normalize: function (items) {
                if (items.length && items[0].label && items[0].value) { return items; }
                return $.map(items, function (item) {
                    if (typeof item === "string") { return { label: item, value: item }; }
                    return $.extend({ label: item.label || item.value, value: item.value || item.label }, item);
                });
            }, _suggest: function (items) { var ul = this.menu.element.empty().zIndex(this.element.zIndex() + 1); this._renderMenu(ul, items); this.menu.deactivate(); this.menu.refresh(); ul.show(); this._resizeMenu(); ul.position($.extend({ of: this.element }, this.options.position)); if (this.options.autoFocus) { this.menu.next(new $.Event("mouseover")); } }, _resizeMenu: function () { var ul = this.menu.element; ul.outerWidth(Math.max(ul.width("").outerWidth(), this.element.outerWidth())); }, _renderMenu: function (ul, items) { var self = this; $.each(items, function (index, item) { self._renderItem(ul, item); }); }, _renderItem: function (ul, item) { return $("<li></li>").data("item.autocomplete", item).append($("<a></a>").text(item.label)).appendTo(ul); }, _move: function (direction, event) {
                if (!this.menu.element.is(":visible")) { this.search(null, event); return; }
                if (this.menu.first() && /^previous/.test(direction) || this.menu.last() && /^next/.test(direction)) { this.element.val(this.term); this.menu.deactivate(); return; }
                this.menu[direction](event);
            }, widget: function () { return this.menu.element; }
        }); $.extend($.ui.autocomplete, { escapeRegex: function (value) { return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"); }, filter: function (array, term) { var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i"); return $.grep(array, function (value) { return matcher.test(value.label || value.value || value); }); } });
    }(jQuery)); (function ($) {
        $.widget("ui.menu", {
            _create: function () {
                var self = this; this.element.addClass("ui-menu ui-widget ui-widget-content ui-corner-all").attr({ role: "listbox", "aria-activedescendant": "ui-active-menuitem" }).click(function (event) {
                    if (!$(event.target).closest(".ui-menu-item a").length) { return; }
                    event.preventDefault(); self.select(event);
                }); this.refresh();
            }, refresh: function () {
                var self = this; var items = this.element.children("li:not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role", "menuitem"); items.children("a").addClass("ui-corner-all").attr("tabindex", -1)
                .mouseenter(function (event) { self.activate(event, $(this).parent()); }).mouseleave(function () { self.deactivate(); });
            }, activate: function (event, item) {
                this.deactivate(); if (this.hasScroll()) { var offset = item.offset().top - this.element.offset().top, scroll = this.element.scrollTop(), elementHeight = this.element.height(); if (offset < 0) { this.element.scrollTop(scroll + offset); } else if (offset >= elementHeight) { this.element.scrollTop(scroll + offset - elementHeight + item.height()); } }
                this.active = item.eq(0).children("a").addClass("ui-state-hover").attr("id", "ui-active-menuitem").end(); this._trigger("focus", event, { item: item });
            }, deactivate: function () {
                if (!this.active) { return; }
                this.active.children("a").removeClass("ui-state-hover").removeAttr("id"); this._trigger("blur"); this.active = null;
            }, next: function (event) { this.move("next", ".ui-menu-item:first", event); }, previous: function (event) { this.move("prev", ".ui-menu-item:last", event); }, first: function () { return this.active && !this.active.prevAll(".ui-menu-item").length; }, last: function () { return this.active && !this.active.nextAll(".ui-menu-item").length; }, move: function (direction, edge, event) {
                if (!this.active) { this.activate(event, this.element.children(edge)); return; }
                var next = this.active[direction + "All"](".ui-menu-item").eq(0); if (next.length) { this.activate(event, next); } else { this.activate(event, this.element.children(edge)); }
            }, nextPage: function (event) {
                if (this.hasScroll()) {
                    if (!this.active || this.last()) { this.activate(event, this.element.children(".ui-menu-item:first")); return; }
                    var base = this.active.offset().top, height = this.element.height(), result = this.element.children(".ui-menu-item").filter(function () { var close = $(this).offset().top - base - height + $(this).height(); return close < 10 && close > -10; }); if (!result.length) { result = this.element.children(".ui-menu-item:last"); }
                    this.activate(event, result);
                } else { this.activate(event, this.element.children(".ui-menu-item").filter(!this.active || this.last() ? ":first" : ":last")); }
            }, previousPage: function (event) {
                if (this.hasScroll()) {
                    if (!this.active || this.first()) { this.activate(event, this.element.children(".ui-menu-item:last")); return; }
                    var base = this.active.offset().top, height = this.element.height(); result = this.element.children(".ui-menu-item").filter(function () { var close = $(this).offset().top - base + height - $(this).height(); return close < 10 && close > -10; }); if (!result.length) { result = this.element.children(".ui-menu-item:first"); }
                    this.activate(event, result);
                } else { this.activate(event, this.element.children(".ui-menu-item").filter(!this.active || this.first() ? ":last" : ":first")); }
            }, hasScroll: function () { return this.element.height() < this.element[$.fn.prop ? "prop" : "attr"]("scrollHeight"); }, select: function (event) { this._trigger("selected", event, { item: this.active }); }
        });
    }(jQuery)); (function ($, undefined) {
        var lastActive, startXPos, startYPos, clickDragged, baseClasses = "ui-button ui-widget ui-state-default ui-corner-all", stateClasses = "ui-state-hover ui-state-active ", typeClasses = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only", formResetHandler = function () { var buttons = $(this).find(":ui-button"); setTimeout(function () { buttons.button("refresh"); }, 1); }, radioGroup = function (radio) {
            var name = radio.name, form = radio.form, radios = $([]); if (name) { if (form) { radios = $(form).find("[name='" + name + "']"); } else { radios = $("[name='" + name + "']", radio.ownerDocument).filter(function () { return !this.form; }); } }
            return radios;
        }; $.widget("ui.button", {
            options: { disabled: null, text: true, label: null, icons: { primary: null, secondary: null } }, _create: function () {
                this.element.closest("form").unbind("reset.button").bind("reset.button", formResetHandler); if (typeof this.options.disabled !== "boolean") { this.options.disabled = this.element.propAttr("disabled"); }
                this._determineButtonType(); this.hasTitle = !!this.buttonElement.attr("title"); var self = this, options = this.options, toggleButton = this.type === "checkbox" || this.type === "radio", hoverClass = "ui-state-hover" + (!toggleButton ? " ui-state-active" : ""), focusClass = "ui-state-focus"; if (options.label === null) { options.label = this.buttonElement.html(); }
                if (this.element.is(":disabled")) { options.disabled = true; }
                this.buttonElement.addClass(baseClasses).attr("role", "button").bind("mouseenter.button", function () {
                    if (options.disabled) { return; }
                    $(this).addClass("ui-state-hover"); if (this === lastActive) { $(this).addClass("ui-state-active"); }
                }).bind("mouseleave.button", function () {
                    if (options.disabled) { return; }
                    $(this).removeClass(hoverClass);
                }).bind("click.button", function (event) { if (options.disabled) { event.preventDefault(); event.stopImmediatePropagation(); } }); this.element.bind("focus.button", function () { self.buttonElement.addClass(focusClass); }).bind("blur.button", function () { self.buttonElement.removeClass(focusClass); }); if (toggleButton) {
                    this.element.bind("change.button", function () {
                        if (clickDragged) { return; }
                        self.refresh();
                    }); this.buttonElement.bind("mousedown.button", function (event) {
                        if (options.disabled) { return; }
                        clickDragged = false; startXPos = event.pageX; startYPos = event.pageY;
                    }).bind("mouseup.button", function (event) {
                        if (options.disabled) { return; }
                        if (startXPos !== event.pageX || startYPos !== event.pageY) { clickDragged = true; }
                    });
                }
                if (this.type === "checkbox") {
                    this.buttonElement.bind("click.button", function () {
                        if (options.disabled || clickDragged) { return false; }
                        $(this).toggleClass("ui-state-active"); self.buttonElement.attr("aria-pressed", self.element[0].checked);
                    });
                } else if (this.type === "radio") {
                    this.buttonElement.bind("click.button", function () {
                        if (options.disabled || clickDragged) { return false; }
                        $(this).addClass("ui-state-active"); self.buttonElement.attr("aria-pressed", "true"); var radio = self.element[0]; radioGroup(radio).not(radio).map(function () { return $(this).button("widget")[0]; }).removeClass("ui-state-active").attr("aria-pressed", "false");
                    });
                } else {
                    this.buttonElement.bind("mousedown.button", function () {
                        if (options.disabled) { return false; }
                        $(this).addClass("ui-state-active"); lastActive = this; $(document).one("mouseup", function () { lastActive = null; });
                    }).bind("mouseup.button", function () {
                        if (options.disabled) { return false; }
                        $(this).removeClass("ui-state-active");
                    }).bind("keydown.button", function (event) {
                        if (options.disabled) { return false; }
                        if (event.keyCode == $.ui.keyCode.SPACE || event.keyCode == $.ui.keyCode.ENTER) { $(this).addClass("ui-state-active"); }
                    }).bind("keyup.button", function () { $(this).removeClass("ui-state-active"); }); if (this.buttonElement.is("a")) { this.buttonElement.keyup(function (event) { if (event.keyCode === $.ui.keyCode.SPACE) { $(this).click(); } }); }
                }
                this._setOption("disabled", options.disabled); this._resetButton();
            }, _determineButtonType: function () {
                if (this.element.is(":checkbox")) { this.type = "checkbox"; } else if (this.element.is(":radio")) { this.type = "radio"; } else if (this.element.is("input")) { this.type = "input"; } else { this.type = "button"; }
                if (this.type === "checkbox" || this.type === "radio") {
                    var ancestor = this.element.parents().filter(":last"), labelSelector = "label[for='" + this.element.attr("id") + "']"; this.buttonElement = ancestor.find(labelSelector); if (!this.buttonElement.length) { ancestor = ancestor.length ? ancestor.siblings() : this.element.siblings(); this.buttonElement = ancestor.filter(labelSelector); if (!this.buttonElement.length) { this.buttonElement = ancestor.find(labelSelector); } }
                    this.element.addClass("ui-helper-hidden-accessible"); var checked = this.element.is(":checked"); if (checked) { this.buttonElement.addClass("ui-state-active"); }
                    this.buttonElement.attr("aria-pressed", checked);
                } else { this.buttonElement = this.element; }
            }, widget: function () { return this.buttonElement; }, destroy: function () {
                this.element.removeClass("ui-helper-hidden-accessible"); this.buttonElement.removeClass(baseClasses + " " + stateClasses + " " + typeClasses).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()); if (!this.hasTitle) { this.buttonElement.removeAttr("title"); }
                $.Widget.prototype.destroy.call(this);
            }, _setOption: function (key, value) {
                $.Widget.prototype._setOption.apply(this, arguments); if (key === "disabled") {
                    if (value) { this.element.propAttr("disabled", true); } else { this.element.propAttr("disabled", false); }
                    return;
                }
                this._resetButton();
            }, refresh: function () {
                var isDisabled = this.element.is(":disabled"); if (isDisabled !== this.options.disabled) { this._setOption("disabled", isDisabled); }
                if (this.type === "radio") { radioGroup(this.element[0]).each(function () { if ($(this).is(":checked")) { $(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true"); } else { $(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false"); } }); } else if (this.type === "checkbox") { if (this.element.is(":checked")) { this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true"); } else { this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false"); } }
            }, _resetButton: function () {
                if (this.type === "input") {
                    if (this.options.label) { this.element.val(this.options.label); }
                    return;
                }
                var buttonElement = this.buttonElement.removeClass(typeClasses), buttonText = $("<span></span>").addClass("ui-button-text").html(this.options.label).appendTo(buttonElement.empty()).text(), icons = this.options.icons, multipleIcons = icons.primary && icons.secondary, buttonClasses = []; if (icons.primary || icons.secondary) {
                    if (this.options.text) { buttonClasses.push("ui-button-text-icon" + (multipleIcons ? "s" : (icons.primary ? "-primary" : "-secondary"))); }
                    if (icons.primary) { buttonElement.prepend("<span class='ui-button-icon-primary ui-icon " + icons.primary + "'></span>"); }
                    if (icons.secondary) { buttonElement.append("<span class='ui-button-icon-secondary ui-icon " + icons.secondary + "'></span>"); }
                    if (!this.options.text) { buttonClasses.push(multipleIcons ? "ui-button-icons-only" : "ui-button-icon-only"); if (!this.hasTitle) { buttonElement.attr("title", buttonText); } }
                } else { buttonClasses.push("ui-button-text-only"); }
                buttonElement.addClass(buttonClasses.join(" "));
            }
        }); $.widget("ui.buttonset", {
            options: { items: ":button, :submit, :reset, :checkbox, :radio, a, :data(button)" }, _create: function () { this.element.addClass("ui-buttonset"); }, _init: function () { this.refresh(); }, _setOption: function (key, value) {
                if (key === "disabled") { this.buttons.button("option", key, value); }
                $.Widget.prototype._setOption.apply(this, arguments);
            }, refresh: function () { var ltr = this.element.css("direction") === "ltr"; this.buttons = this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function () { return $(this).button("widget")[0]; }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(ltr ? "ui-corner-left" : "ui-corner-right").end().filter(":last").addClass(ltr ? "ui-corner-right" : "ui-corner-left").end().end(); }, destroy: function () { this.element.removeClass("ui-buttonset"); this.buttons.map(function () { return $(this).button("widget")[0]; }).removeClass("ui-corner-left ui-corner-right").end().button("destroy"); $.Widget.prototype.destroy.call(this); }
        });
    }(jQuery)); (function ($, undefined) {
        var uiDialogClasses = 'ui-dialog ' + 'ui-widget ' + 'ui-widget-content ' + 'ui-corner-all ', sizeRelatedOptions = { buttons: true, height: true, maxHeight: true, maxWidth: true, minHeight: true, minWidth: true, width: true }, resizableRelatedOptions = { maxHeight: true, maxWidth: true, minHeight: true, minWidth: true }, attrFn = $.attrFn || { val: true, css: true, html: true, text: true, data: true, width: true, height: true, offset: true, click: true }; $.widget("ui.dialog", {
            options: { autoOpen: true, buttons: {}, closeOnEscape: true, closeText: 'close', dialogClass: '', draggable: true, hide: null, height: 'auto', maxHeight: false, maxWidth: false, minHeight: 150, minWidth: 150, modal: false, position: { my: 'center', at: 'center', collision: 'fit', using: function (pos) { var topOffset = $(this).css(pos).offset().top; if (topOffset < 0) { $(this).css('top', pos.top - topOffset); } } }, resizable: true, show: null, stack: true, title: '', width: 300, zIndex: 1000 }, _create: function () {
                this.originalTitle = this.element.attr('title'); if (typeof this.originalTitle !== "string") { this.originalTitle = ""; }
                this.options.title = this.options.title || this.originalTitle; var self = this, options = self.options, title = options.title || '&#160;', titleId = $.ui.dialog.getTitleId(self.element), uiDialog = (self.uiDialog = $('<div></div>')).appendTo(document.body).hide().addClass(uiDialogClasses + options.dialogClass).css({ zIndex: options.zIndex })
                .attr('tabIndex', -1).css('outline', 0).keydown(function (event) { if (options.closeOnEscape && !event.isDefaultPrevented() && event.keyCode && event.keyCode === $.ui.keyCode.ESCAPE) { self.close(event); event.preventDefault(); } }).attr({ role: 'dialog', 'aria-labelledby': titleId }).mousedown(function (event) { self.moveToTop(false, event); }), uiDialogContent = self.element.show().removeAttr('title').addClass('ui-dialog-content ' + 'ui-widget-content').appendTo(uiDialog), uiDialogTitlebar = (self.uiDialogTitlebar = $('<div></div>')).addClass('ui-dialog-titlebar ' + 'ui-widget-header ' + 'ui-corner-all ' + 'ui-helper-clearfix').prependTo(uiDialog), uiDialogTitlebarClose = $('<a href="#"></a>').addClass('ui-dialog-titlebar-close ' + 'ui-corner-all').attr('role', 'button').hover(function () { uiDialogTitlebarClose.addClass('ui-state-hover'); }, function () { uiDialogTitlebarClose.removeClass('ui-state-hover'); }).focus(function () { uiDialogTitlebarClose.addClass('ui-state-focus'); }).blur(function () { uiDialogTitlebarClose.removeClass('ui-state-focus'); }).click(function (event) { self.close(event); return false; }).appendTo(uiDialogTitlebar), uiDialogTitlebarCloseText = (self.uiDialogTitlebarCloseText = $('<span></span>')).addClass('ui-icon ' + 'ui-icon-closethick').text(options.closeText).appendTo(uiDialogTitlebarClose), uiDialogTitle = $('<span></span>').addClass('ui-dialog-title').attr('id', titleId).html(title).prependTo(uiDialogTitlebar); if ($.isFunction(options.beforeclose) && !$.isFunction(options.beforeClose)) { options.beforeClose = options.beforeclose; }
                uiDialogTitlebar.find("*").add(uiDialogTitlebar).disableSelection(); if (options.draggable && $.fn.draggable) { self._makeDraggable(); }
                if (options.resizable && $.fn.resizable) { self._makeResizable(); }
                self._createButtons(options.buttons); self._isOpen = false; if ($.fn.bgiframe) { uiDialog.bgiframe(); }
            }, _init: function () { if (this.options.autoOpen) { this.open(); } }, destroy: function () {
                var self = this; if (self.overlay) { self.overlay.destroy(); }
                self.uiDialog.hide(); self.element.unbind('.dialog').removeData('dialog').removeClass('ui-dialog-content ui-widget-content').hide().appendTo('body'); self.uiDialog.remove(); if (self.originalTitle) { self.element.attr('title', self.originalTitle); }
                return self;
            }, widget: function () { return this.uiDialog; }, close: function (event) {
                var self = this, maxZ, thisZ; if (false === self._trigger('beforeClose', event)) { return; }
                if (self.overlay) { self.overlay.destroy(); }
                self.uiDialog.unbind('keypress.ui-dialog'); self._isOpen = false; if (self.options.hide) { self.uiDialog.hide(self.options.hide, function () { self._trigger('close', event); }); } else { self.uiDialog.hide(); self._trigger('close', event); }
                $.ui.dialog.overlay.resize(); if (self.options.modal) { maxZ = 0; $('.ui-dialog').each(function () { if (this !== self.uiDialog[0]) { thisZ = $(this).css('z-index'); if (!isNaN(thisZ)) { maxZ = Math.max(maxZ, thisZ); } } }); $.ui.dialog.maxZ = maxZ; }
                return self;
            }, isOpen: function () { return this._isOpen; }, moveToTop: function (force, event) {
                var self = this, options = self.options, saveScroll; if ((options.modal && !force) || (!options.stack && !options.modal)) { return self._trigger('focus', event); }
                if (options.zIndex > $.ui.dialog.maxZ) { $.ui.dialog.maxZ = options.zIndex; }
                if (self.overlay) { $.ui.dialog.maxZ += 1; self.overlay.$el.css('z-index', $.ui.dialog.overlay.maxZ = $.ui.dialog.maxZ); }
                saveScroll = { scrollTop: self.element.scrollTop(), scrollLeft: self.element.scrollLeft() }; $.ui.dialog.maxZ += 1; self.uiDialog.css('z-index', $.ui.dialog.maxZ); self.element.attr(saveScroll); self._trigger('focus', event); return self;
            }, open: function () {
                if (this._isOpen) { return; }
                var self = this, options = self.options, uiDialog = self.uiDialog; self.overlay = options.modal ? new $.ui.dialog.overlay(self) : null; self._size(); self._position(options.position); uiDialog.show(options.show); self.moveToTop(true); if (options.modal) {
                    uiDialog.bind('keypress.ui-dialog', function (event) {
                        if (event.keyCode !== $.ui.keyCode.TAB) { return; }
                        var tabbables = $(':tabbable', this), first = tabbables.filter(':first'), last = tabbables.filter(':last'); if (event.target === last[0] && !event.shiftKey) { first.focus(1); return false; } else if (event.target === first[0] && event.shiftKey) { last.focus(1); return false; }
                    });
                }
                $(self.element.find(':tabbable').get().concat(uiDialog.find('.ui-dialog-buttonpane :tabbable').get().concat(uiDialog.get()))).eq(0).focus(); self._isOpen = true; self._trigger('open'); return self;
            }, _createButtons: function (buttons) {
                var self = this, hasButtons = false, uiDialogButtonPane = $('<div></div>').addClass('ui-dialog-buttonpane ' + 'ui-widget-content ' + 'ui-helper-clearfix'), uiButtonSet = $("<div></div>").addClass("ui-dialog-buttonset").appendTo(uiDialogButtonPane); self.uiDialog.find('.ui-dialog-buttonpane').remove(); if (typeof buttons === 'object' && buttons !== null) { $.each(buttons, function () { return !(hasButtons = true); }); }
                if (hasButtons) {
                    $.each(buttons, function (name, props) {
                        props = $.isFunction(props) ? { click: props, text: name } : props; var button = $('<button type="button"></button>').click(function () { props.click.apply(self.element[0], arguments); }).appendTo(uiButtonSet); $.each(props, function (key, value) {
                            if (key === "click") { return; }
                            if (key in attrFn) { button[key](value); } else { button.attr(key, value); }
                        }); if ($.fn.button) { button.button(); }
                    }); uiDialogButtonPane.appendTo(self.uiDialog);
                }
            }, _makeDraggable: function () {
                var self = this, options = self.options, doc = $(document), heightBeforeDrag; function filteredUi(ui) { return { position: ui.position, offset: ui.offset }; }
                self.uiDialog.draggable({ cancel: '.ui-dialog-content, .ui-dialog-titlebar-close', handle: '.ui-dialog-titlebar', containment: 'document', start: function (event, ui) { heightBeforeDrag = options.height === "auto" ? "auto" : $(this).height(); $(this).height($(this).height()).addClass("ui-dialog-dragging"); self._trigger('dragStart', event, filteredUi(ui)); }, drag: function (event, ui) { self._trigger('drag', event, filteredUi(ui)); }, stop: function (event, ui) { options.position = [ui.position.left - doc.scrollLeft(), ui.position.top - doc.scrollTop()]; $(this).removeClass("ui-dialog-dragging").height(heightBeforeDrag); self._trigger('dragStop', event, filteredUi(ui)); $.ui.dialog.overlay.resize(); } });
            }, _makeResizable: function (handles) {
                handles = (handles === undefined ? this.options.resizable : handles); var self = this, options = self.options, position = self.uiDialog.css('position'), resizeHandles = (typeof handles === 'string' ? handles : 'n,e,s,w,se,sw,ne,nw'); function filteredUi(ui) { return { originalPosition: ui.originalPosition, originalSize: ui.originalSize, position: ui.position, size: ui.size }; }
                self.uiDialog.resizable({ cancel: '.ui-dialog-content', containment: 'document', alsoResize: self.element, maxWidth: options.maxWidth, maxHeight: options.maxHeight, minWidth: options.minWidth, minHeight: self._minHeight(), handles: resizeHandles, start: function (event, ui) { $(this).addClass("ui-dialog-resizing"); self._trigger('resizeStart', event, filteredUi(ui)); }, resize: function (event, ui) { self._trigger('resize', event, filteredUi(ui)); }, stop: function (event, ui) { $(this).removeClass("ui-dialog-resizing"); options.height = $(this).height(); options.width = $(this).width(); self._trigger('resizeStop', event, filteredUi(ui)); $.ui.dialog.overlay.resize(); } }).css('position', position).find('.ui-resizable-se').addClass('ui-icon ui-icon-grip-diagonal-se');
            }, _minHeight: function () { var options = this.options; if (options.height === 'auto') { return options.minHeight; } else { return Math.min(options.minHeight, options.height); } }, _position: function (position) {
                var myAt = [], offset = [0, 0], isVisible; if (position) {
                    if (typeof position === 'string' || (typeof position === 'object' && '0' in position)) {
                        myAt = position.split ? position.split(' ') : [position[0], position[1]]; if (myAt.length === 1) { myAt[1] = myAt[0]; }
                        $.each(['left', 'top'], function (i, offsetPosition) { if (+myAt[i] === myAt[i]) { offset[i] = myAt[i]; myAt[i] = offsetPosition; } }); position = { my: myAt.join(" "), at: myAt.join(" "), offset: offset.join(" ") };
                    }
                    position = $.extend({}, $.ui.dialog.prototype.options.position, position);
                } else { position = $.ui.dialog.prototype.options.position; }
                isVisible = this.uiDialog.is(':visible'); if (!isVisible) { this.uiDialog.show(); }
                this.uiDialog
                .css({ top: 0, left: 0 }).position($.extend({ of: window }, position)); if (!isVisible) { this.uiDialog.hide(); }
            }, _setOptions: function (options) {
                var self = this, resizableOptions = {}, resize = false; $.each(options, function (key, value) {
                    self._setOption(key, value); if (key in sizeRelatedOptions) { resize = true; }
                    if (key in resizableRelatedOptions) { resizableOptions[key] = value; }
                }); if (resize) { this._size(); }
                if (this.uiDialog.is(":data(resizable)")) { this.uiDialog.resizable("option", resizableOptions); }
            }, _setOption: function (key, value) {
                var self = this, uiDialog = self.uiDialog; switch (key) {
                    case "beforeclose": key = "beforeClose"; break; case "buttons": self._createButtons(value); break; case "closeText": self.uiDialogTitlebarCloseText.text("" + value); break; case "dialogClass": uiDialog.removeClass(self.options.dialogClass).addClass(uiDialogClasses + value); break; case "disabled": if (value) { uiDialog.addClass('ui-dialog-disabled'); } else { uiDialog.removeClass('ui-dialog-disabled'); }
                        break; case "draggable": var isDraggable = uiDialog.is(":data(draggable)"); if (isDraggable && !value) { uiDialog.draggable("destroy"); }
                            if (!isDraggable && value) { self._makeDraggable(); }
                            break; case "position": self._position(value); break; case "resizable": var isResizable = uiDialog.is(":data(resizable)"); if (isResizable && !value) { uiDialog.resizable('destroy'); }
                                if (isResizable && typeof value === 'string') { uiDialog.resizable('option', 'handles', value); }
                                if (!isResizable && value !== false) { self._makeResizable(value); }
                                break; case "title": $(".ui-dialog-title", self.uiDialogTitlebar).html("" + (value || '&#160;')); break;
                }
                $.Widget.prototype._setOption.apply(self, arguments);
            }, _size: function () {
                var options = this.options, nonContentHeight, minContentHeight, isVisible = this.uiDialog.is(":visible"); this.element.show().css({ width: 'auto', minHeight: 0, height: 0 }); if (options.minWidth > options.width) { options.width = options.minWidth; }
                nonContentHeight = this.uiDialog.css({ height: 'auto', width: options.width }).height(); minContentHeight = Math.max(0, options.minHeight - nonContentHeight); if (options.height === "auto") {
                    if ($.support.minHeight) { this.element.css({ minHeight: minContentHeight, height: "auto" }); } else {
                        this.uiDialog.show(); var autoHeight = this.element.css("height", "auto").height(); if (!isVisible) { this.uiDialog.hide(); }
                        this.element.height(Math.max(autoHeight, minContentHeight));
                    }
                } else { this.element.height(Math.max(options.height - nonContentHeight, 0)); }
                if (this.uiDialog.is(':data(resizable)')) { this.uiDialog.resizable('option', 'minHeight', this._minHeight()); }
            }
        }); $.extend($.ui.dialog, {
            version: "1.8.16", uuid: 0, maxZ: 0, getTitleId: function ($el) {
                var id = $el.attr('id'); if (!id) { this.uuid += 1; id = this.uuid; }
                return 'ui-dialog-title-' + id;
            }, overlay: function (dialog) { this.$el = $.ui.dialog.overlay.create(dialog); }
        }); $.extend($.ui.dialog.overlay, {
            instances: [], oldInstances: [], maxZ: 0, events: $.map('focus,mousedown,mouseup,keydown,keypress,click'.split(','), function (event) { return event + '.dialog-overlay'; }).join(' '), create: function (dialog) {
                if (this.instances.length === 0) { setTimeout(function () { if ($.ui.dialog.overlay.instances.length) { $(document).bind($.ui.dialog.overlay.events, function (event) { if ($(event.target).zIndex() < $.ui.dialog.overlay.maxZ) { return false; } }); } }, 1); $(document).bind('keydown.dialog-overlay', function (event) { if (dialog.options.closeOnEscape && !event.isDefaultPrevented() && event.keyCode && event.keyCode === $.ui.keyCode.ESCAPE) { dialog.close(event); event.preventDefault(); } }); $(window).bind('resize.dialog-overlay', $.ui.dialog.overlay.resize); }
                var $el = (this.oldInstances.pop() || $('<div></div>').addClass('ui-widget-overlay')).appendTo(document.body).css({ width: this.width(), height: this.height() }); if ($.fn.bgiframe) { $el.bgiframe(); }
                this.instances.push($el); return $el;
            }, destroy: function ($el) {
                var indexOf = $.inArray($el, this.instances); if (indexOf != -1) { this.oldInstances.push(this.instances.splice(indexOf, 1)[0]); }
                if (this.instances.length === 0) { $([document, window]).unbind('.dialog-overlay'); }
                $el.remove(); var maxZ = 0; $.each(this.instances, function () { maxZ = Math.max(maxZ, this.css('z-index')); }); this.maxZ = maxZ;
            }, height: function () {
                var scrollHeight, offsetHeight; if ($.browser.msie && $.browser.version < 7) {
                    scrollHeight = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight); offsetHeight = Math.max(document.documentElement.offsetHeight, document.body.offsetHeight); if (scrollHeight < offsetHeight) { return $(window).height() + 'px'; } else { return scrollHeight + 'px'; }
                } else { return $(document).height() + 'px'; }
            }, width: function () {
                var scrollWidth, offsetWidth; if ($.browser.msie) {
                    scrollWidth = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth); offsetWidth = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth); if (scrollWidth < offsetWidth) { return $(window).width() + 'px'; } else { return scrollWidth + 'px'; }
                } else { return $(document).width() + 'px'; }
            }, resize: function () { var $overlays = $([]); $.each($.ui.dialog.overlay.instances, function () { $overlays = $overlays.add(this); }); $overlays.css({ width: 0, height: 0 }).css({ width: $.ui.dialog.overlay.width(), height: $.ui.dialog.overlay.height() }); }
        }); $.extend($.ui.dialog.overlay.prototype, { destroy: function () { $.ui.dialog.overlay.destroy(this.$el); } });
    }(jQuery)); (function ($, undefined) {
        var numPages = 5; $.widget("ui.slider", $.ui.mouse, {
            widgetEventPrefix: "slide", options: { animate: false, distance: 0, max: 100, min: 0, orientation: "horizontal", range: false, step: 1, value: 0, values: null }, _create: function () {
                var self = this, o = this.options, existingHandles = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"), handle = "<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>", handleCount = (o.values && o.values.length) || 1, handles = []; this._keySliding = false; this._mouseSliding = false; this._animateOff = true; this._handleIndex = null; this._detectOrientation(); this._mouseInit(); this.element.addClass("ui-slider" + " ui-slider-" + this.orientation + " ui-widget" + " ui-widget-content" + " ui-corner-all" +
                (o.disabled ? " ui-slider-disabled ui-disabled" : "")); this.range = $([]); if (o.range) {
                    if (o.range === true) {
                        if (!o.values) { o.values = [this._valueMin(), this._valueMin()]; }
                        if (o.values.length && o.values.length !== 2) { o.values = [o.values[0], o.values[0]]; }
                    }
                    this.range = $("<div></div>").appendTo(this.element).addClass("ui-slider-range" +
                    " ui-widget-header" +
                    ((o.range === "min" || o.range === "max") ? " ui-slider-range-" + o.range : ""));
                }
                for (var i = existingHandles.length; i < handleCount; i += 1) { handles.push(handle); }
                this.handles = existingHandles.add($(handles.join("")).appendTo(self.element)); this.handle = this.handles.eq(0); this.handles.add(this.range).filter("a").click(function (event) { event.preventDefault(); }).hover(function () { if (!o.disabled) { $(this).addClass("ui-state-hover"); } }, function () { $(this).removeClass("ui-state-hover"); }).focus(function () { if (!o.disabled) { $(".ui-slider .ui-state-focus").removeClass("ui-state-focus"); $(this).addClass("ui-state-focus"); } else { $(this).blur(); } }).blur(function () { $(this).removeClass("ui-state-focus"); }); this.handles.each(function (i) { $(this).data("index.ui-slider-handle", i); }); this.handles.keydown(function (event) {
                    var ret = true, index = $(this).data("index.ui-slider-handle"), allowed, curVal, newVal, step; if (self.options.disabled) { return; }
                    switch (event.keyCode) {
                        case $.ui.keyCode.HOME: case $.ui.keyCode.END: case $.ui.keyCode.PAGE_UP: case $.ui.keyCode.PAGE_DOWN: case $.ui.keyCode.UP: case $.ui.keyCode.RIGHT: case $.ui.keyCode.DOWN: case $.ui.keyCode.LEFT: ret = false; if (!self._keySliding) { self._keySliding = true; $(this).addClass("ui-state-active"); allowed = self._start(event, index); if (allowed === false) { return; } }
                            break;
                    }
                    step = self.options.step; if (self.options.values && self.options.values.length) { curVal = newVal = self.values(index); } else { curVal = newVal = self.value(); }
                    switch (event.keyCode) {
                        case $.ui.keyCode.HOME: newVal = self._valueMin(); break; case $.ui.keyCode.END: newVal = self._valueMax(); break; case $.ui.keyCode.PAGE_UP: newVal = self._trimAlignValue(curVal + ((self._valueMax() - self._valueMin()) / numPages)); break; case $.ui.keyCode.PAGE_DOWN: newVal = self._trimAlignValue(curVal - ((self._valueMax() - self._valueMin()) / numPages)); break; case $.ui.keyCode.UP: case $.ui.keyCode.RIGHT: if (curVal === self._valueMax()) { return; }
                            newVal = self._trimAlignValue(curVal + step); break; case $.ui.keyCode.DOWN: case $.ui.keyCode.LEFT: if (curVal === self._valueMin()) { return; }
                                newVal = self._trimAlignValue(curVal - step); break;
                    }
                    self._slide(event, index, newVal); return ret;
                }).keyup(function (event) { var index = $(this).data("index.ui-slider-handle"); if (self._keySliding) { self._keySliding = false; self._stop(event, index); self._change(event, index); $(this).removeClass("ui-state-active"); } }); this._refreshValue(); this._animateOff = false;
            }, destroy: function () { this.handles.remove(); this.range.remove(); this.element.removeClass("ui-slider" + " ui-slider-horizontal" + " ui-slider-vertical" + " ui-slider-disabled" + " ui-widget" + " ui-widget-content" + " ui-corner-all").removeData("slider").unbind(".slider"); this._mouseDestroy(); return this; }, _mouseCapture: function (event) {
                var o = this.options, position, normValue, distance, closestHandle, self, index, allowed, offset, mouseOverHandle; if (o.disabled) { return false; }
                this.elementSize = { width: this.element.outerWidth(), height: this.element.outerHeight() }; this.elementOffset = this.element.offset(); position = { x: event.pageX, y: event.pageY }; normValue = this._normValueFromMouse(position); distance = this._valueMax() - this._valueMin() + 1; self = this; this.handles.each(function (i) { var thisDistance = Math.abs(normValue - self.values(i)); if (distance > thisDistance) { distance = thisDistance; closestHandle = $(this); index = i; } }); if (o.range === true && this.values(1) === o.min) { index += 1; closestHandle = $(this.handles[index]); }
                allowed = this._start(event, index); if (allowed === false) { return false; }
                this._mouseSliding = true; self._handleIndex = index; closestHandle.addClass("ui-state-active").focus(); offset = closestHandle.offset(); mouseOverHandle = !$(event.target).parents().andSelf().is(".ui-slider-handle"); this._clickOffset = mouseOverHandle ? { left: 0, top: 0 } : {
                    left: event.pageX - offset.left - (closestHandle.width() / 2), top: event.pageY - offset.top -
                    (closestHandle.height() / 2) -
                    (parseInt(closestHandle.css("borderTopWidth"), 10) || 0) -
                    (parseInt(closestHandle.css("borderBottomWidth"), 10) || 0) +
                    (parseInt(closestHandle.css("marginTop"), 10) || 0)
                }; if (!this.handles.hasClass("ui-state-hover")) { this._slide(event, index, normValue); }
                this._animateOff = true; return true;
            }, _mouseStart: function (event) { return true; }, _mouseDrag: function (event) { var position = { x: event.pageX, y: event.pageY }, normValue = this._normValueFromMouse(position); this._slide(event, this._handleIndex, normValue); return false; }, _mouseStop: function (event) { this.handles.removeClass("ui-state-active"); this._mouseSliding = false; this._stop(event, this._handleIndex); this._change(event, this._handleIndex); this._handleIndex = null; this._clickOffset = null; this._animateOff = false; return false; }, _detectOrientation: function () { this.orientation = (this.options.orientation === "vertical") ? "vertical" : "horizontal"; }, _normValueFromMouse: function (position) {
                var pixelTotal, pixelMouse, percentMouse, valueTotal, valueMouse; if (this.orientation === "horizontal") { pixelTotal = this.elementSize.width; pixelMouse = position.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0); } else { pixelTotal = this.elementSize.height; pixelMouse = position.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0); }
                percentMouse = (pixelMouse / pixelTotal); if (percentMouse > 1) { percentMouse = 1; }
                if (percentMouse < 0) { percentMouse = 0; }
                if (this.orientation === "vertical") { percentMouse = 1 - percentMouse; }
                valueTotal = this._valueMax() - this._valueMin(); valueMouse = this._valueMin() + percentMouse * valueTotal; return this._trimAlignValue(valueMouse);
            }, _start: function (event, index) {
                var uiHash = { handle: this.handles[index], value: this.value() }; if (this.options.values && this.options.values.length) { uiHash.value = this.values(index); uiHash.values = this.values(); }
                return this._trigger("start", event, uiHash);
            }, _slide: function (event, index, newVal) {
                var otherVal, newValues, allowed; if (this.options.values && this.options.values.length) {
                    otherVal = this.values(index ? 0 : 1); if ((this.options.values.length === 2 && this.options.range === true) && ((index === 0 && newVal > otherVal) || (index === 1 && newVal < otherVal))) { newVal = otherVal; }
                    if (newVal !== this.values(index)) { newValues = this.values(); newValues[index] = newVal; allowed = this._trigger("slide", event, { handle: this.handles[index], value: newVal, values: newValues }); otherVal = this.values(index ? 0 : 1); if (allowed !== false) { this.values(index, newVal, true); } }
                } else { if (newVal !== this.value()) { allowed = this._trigger("slide", event, { handle: this.handles[index], value: newVal }); if (allowed !== false) { this.value(newVal); } } }
            }, _stop: function (event, index) {
                var uiHash = { handle: this.handles[index], value: this.value() }; if (this.options.values && this.options.values.length) { uiHash.value = this.values(index); uiHash.values = this.values(); }
                this._trigger("stop", event, uiHash);
            }, _change: function (event, index) {
                if (!this._keySliding && !this._mouseSliding) {
                    var uiHash = { handle: this.handles[index], value: this.value() }; if (this.options.values && this.options.values.length) { uiHash.value = this.values(index); uiHash.values = this.values(); }
                    this._trigger("change", event, uiHash);
                }
            }, value: function (newValue) {
                if (arguments.length) { this.options.value = this._trimAlignValue(newValue); this._refreshValue(); this._change(null, 0); return; }
                return this._value();
            }, values: function (index, newValue) {
                var vals, newValues, i; if (arguments.length > 1) { this.options.values[index] = this._trimAlignValue(newValue); this._refreshValue(); this._change(null, index); return; }
                if (arguments.length) {
                    if ($.isArray(arguments[0])) {
                        vals = this.options.values; newValues = arguments[0]; for (i = 0; i < vals.length; i += 1) { vals[i] = this._trimAlignValue(newValues[i]); this._change(null, i); }
                        this._refreshValue();
                    } else { if (this.options.values && this.options.values.length) { return this._values(index); } else { return this.value(); } }
                } else { return this._values(); }
            }, _setOption: function (key, value) {
                var i, valsLength = 0; if ($.isArray(this.options.values)) { valsLength = this.options.values.length; }
                $.Widget.prototype._setOption.apply(this, arguments); switch (key) {
                    case "disabled": if (value) { this.handles.filter(".ui-state-focus").blur(); this.handles.removeClass("ui-state-hover"); this.handles.propAttr("disabled", true); this.element.addClass("ui-disabled"); } else { this.handles.propAttr("disabled", false); this.element.removeClass("ui-disabled"); }
                        break; case "orientation": this._detectOrientation(); this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation); this._refreshValue(); break; case "value": this._animateOff = true; this._refreshValue(); this._change(null, 0); this._animateOff = false; break; case "values": this._animateOff = true; this._refreshValue(); for (i = 0; i < valsLength; i += 1) { this._change(null, i); }
                            this._animateOff = false; break;
                }
            }, _value: function () { var val = this.options.value; val = this._trimAlignValue(val); return val; }, _values: function (index) {
                var val, vals, i; if (arguments.length) { val = this.options.values[index]; val = this._trimAlignValue(val); return val; } else {
                    vals = this.options.values.slice(); for (i = 0; i < vals.length; i += 1) { vals[i] = this._trimAlignValue(vals[i]); }
                    return vals;
                }
            }, _trimAlignValue: function (val) {
                if (val <= this._valueMin()) { return this._valueMin(); }
                if (val >= this._valueMax()) { return this._valueMax(); }
                var step = (this.options.step > 0) ? this.options.step : 1, valModStep = (val - this._valueMin()) % step, alignValue = val - valModStep; if (Math.abs(valModStep) * 2 >= step) { alignValue += (valModStep > 0) ? step : (-step); }
                return parseFloat(alignValue.toFixed(5));
            }, _valueMin: function () { return this.options.min; }, _valueMax: function () { return this.options.max; }, _refreshValue: function () {
                var oRange = this.options.range, o = this.options, self = this, animate = (!this._animateOff) ? o.animate : false, valPercent, _set = {}, lastValPercent, value, valueMin, valueMax; if (this.options.values && this.options.values.length) {
                    this.handles.each(function (i, j) {
                        valPercent = (self.values(i) - self._valueMin()) / (self._valueMax() - self._valueMin()) * 100; _set[self.orientation === "horizontal" ? "left" : "bottom"] = valPercent + "%"; $(this).stop(1, 1)[animate ? "animate" : "css"](_set, o.animate); if (self.options.range === true) {
                            if (self.orientation === "horizontal") {
                                if (i === 0) { self.range.stop(1, 1)[animate ? "animate" : "css"]({ left: valPercent + "%" }, o.animate); }
                                if (i === 1) { self.range[animate ? "animate" : "css"]({ width: (valPercent - lastValPercent) + "%" }, { queue: false, duration: o.animate }); }
                            } else {
                                if (i === 0) { self.range.stop(1, 1)[animate ? "animate" : "css"]({ bottom: (valPercent) + "%" }, o.animate); }
                                if (i === 1) { self.range[animate ? "animate" : "css"]({ height: (valPercent - lastValPercent) + "%" }, { queue: false, duration: o.animate }); }
                            }
                        }
                        lastValPercent = valPercent;
                    });
                } else {
                    value = this.value(); valueMin = this._valueMin(); valueMax = this._valueMax(); valPercent = (valueMax !== valueMin) ? (value - valueMin) / (valueMax - valueMin) * 100 : 0; _set[self.orientation === "horizontal" ? "left" : "bottom"] = valPercent + "%"; this.handle.stop(1, 1)[animate ? "animate" : "css"](_set, o.animate); if (oRange === "min" && this.orientation === "horizontal") { this.range.stop(1, 1)[animate ? "animate" : "css"]({ width: valPercent + "%" }, o.animate); }
                    if (oRange === "max" && this.orientation === "horizontal") { this.range[animate ? "animate" : "css"]({ width: (100 - valPercent) + "%" }, { queue: false, duration: o.animate }); }
                    if (oRange === "min" && this.orientation === "vertical") { this.range.stop(1, 1)[animate ? "animate" : "css"]({ height: valPercent + "%" }, o.animate); }
                    if (oRange === "max" && this.orientation === "vertical") { this.range[animate ? "animate" : "css"]({ height: (100 - valPercent) + "%" }, { queue: false, duration: o.animate }); }
                }
            }
        }); $.extend($.ui.slider, { version: "1.8.16" });
    }(jQuery)); (function ($, undefined) {
        var tabId = 0, listId = 0; function getNextTabId() { return ++tabId; }
        function getNextListId() { return ++listId; }
        $.widget("ui.tabs", {
            options: { add: null, ajaxOptions: null, cache: false, cookie: null, collapsible: false, disable: null, disabled: [], enable: null, event: "click", fx: null, idPrefix: "ui-tabs-", load: null, panelTemplate: "<div></div>", remove: null, select: null, show: null, spinner: "<em>Loading&#8230;</em>", tabTemplate: "<li><a href='#{href}'><span>#{label}</span></a></li>" }, _create: function () { this._tabify(true); }, _setOption: function (key, value) {
                if (key == "selected") {
                    if (this.options.collapsible && value == this.options.selected) { return; }
                    this.select(value);
                } else { this.options[key] = value; this._tabify(); }
            }, _tabId: function (a) { return a.title && a.title.replace(/\s/g, "_").replace(/[^\w\u00c0-\uFFFF-]/g, "") || this.options.idPrefix + getNextTabId(); }, _sanitizeSelector: function (hash) { return hash.replace(/:/g, "\\:"); }, _cookie: function () { var cookie = this.cookie || (this.cookie = this.options.cookie.name || "ui-tabs-" + getNextListId()); return $.cookie.apply(null, [cookie].concat($.makeArray(arguments))); }, _ui: function (tab, panel) { return { tab: tab, panel: panel, index: this.anchors.index(tab) }; }, _cleanup: function () { this.lis.filter(".ui-state-processing").removeClass("ui-state-processing").find("span:data(label.tabs)").each(function () { var el = $(this); el.html(el.data("label.tabs")).removeData("label.tabs"); }); }, _tabify: function (init) {
                var self = this, o = this.options, fragmentId = /^#.+/; this.list = this.element.find("ol,ul").eq(0); this.lis = $(" > li:has(a[href])", this.list); this.anchors = this.lis.map(function () { return $("a", this)[0]; }); this.panels = $([]); this.anchors.each(function (i, a) {
                    var href = $(a).attr("href"); var hrefBase = href.split("#")[0], baseEl; if (hrefBase && (hrefBase === location.toString().split("#")[0] || (baseEl = $("base")[0]) && hrefBase === baseEl.href)) { href = a.hash; a.href = href; }
                    if (fragmentId.test(href)) { self.panels = self.panels.add(self.element.find(self._sanitizeSelector(href))); } else if (href && href !== "#") {
                        $.data(a, "href.tabs", href); $.data(a, "load.tabs", href.replace(/#.*$/, "")); var id = self._tabId(a); a.href = "#" + id; var $panel = self.element.find("#" + id); if (!$panel.length) { $panel = $(o.panelTemplate).attr("id", id).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").insertAfter(self.panels[i - 1] || self.list); $panel.data("destroy.tabs", true); }
                        self.panels = self.panels.add($panel);
                    } else { o.disabled.push(i); }
                }); if (init) {
                    this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all"); this.list.addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"); this.lis.addClass("ui-state-default ui-corner-top"); this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom"); if (o.selected === undefined) {
                        if (location.hash) { this.anchors.each(function (i, a) { if (a.hash == location.hash) { o.selected = i; return false; } }); }
                        if (typeof o.selected !== "number" && o.cookie) { o.selected = parseInt(self._cookie(), 10); }
                        if (typeof o.selected !== "number" && this.lis.filter(".ui-tabs-selected").length) { o.selected = this.lis.index(this.lis.filter(".ui-tabs-selected")); }
                        o.selected = o.selected || (this.lis.length ? 0 : -1);
                    } else if (o.selected === null) { o.selected = -1; }
                    o.selected = ((o.selected >= 0 && this.anchors[o.selected]) || o.selected < 0) ? o.selected : 0; o.disabled = $.unique(o.disabled.concat($.map(this.lis.filter(".ui-state-disabled"), function (n, i) { return self.lis.index(n); }))).sort(); if ($.inArray(o.selected, o.disabled) != -1) { o.disabled.splice($.inArray(o.selected, o.disabled), 1); }
                    this.panels.addClass("ui-tabs-hide"); this.lis.removeClass("ui-tabs-selected ui-state-active"); if (o.selected >= 0 && this.anchors.length) { self.element.find(self._sanitizeSelector(self.anchors[o.selected].hash)).removeClass("ui-tabs-hide"); this.lis.eq(o.selected).addClass("ui-tabs-selected ui-state-active"); self.element.queue("tabs", function () { self._trigger("show", null, self._ui(self.anchors[o.selected], self.element.find(self._sanitizeSelector(self.anchors[o.selected].hash))[0])); }); this.load(o.selected); }
                    $(window).bind("unload", function () { self.lis.add(self.anchors).unbind(".tabs"); self.lis = self.anchors = self.panels = null; });
                } else { o.selected = this.lis.index(this.lis.filter(".ui-tabs-selected")); }
                this.element[o.collapsible ? "addClass" : "removeClass"]("ui-tabs-collapsible"); if (o.cookie) { this._cookie(o.selected, o.cookie); }
                for (var i = 0, li; (li = this.lis[i]) ; i++) { $(li)[$.inArray(i, o.disabled) != -1 && !$(li).hasClass("ui-tabs-selected") ? "addClass" : "removeClass"]("ui-state-disabled"); }
                if (o.cache === false) { this.anchors.removeData("cache.tabs"); }
                this.lis.add(this.anchors).unbind(".tabs"); if (o.event !== "mouseover") { var addState = function (state, el) { if (el.is(":not(.ui-state-disabled)")) { el.addClass("ui-state-" + state); } }; var removeState = function (state, el) { el.removeClass("ui-state-" + state); }; this.lis.bind("mouseover.tabs", function () { addState("hover", $(this)); }); this.lis.bind("mouseout.tabs", function () { removeState("hover", $(this)); }); this.anchors.bind("focus.tabs", function () { addState("focus", $(this).closest("li")); }); this.anchors.bind("blur.tabs", function () { removeState("focus", $(this).closest("li")); }); }
                var hideFx, showFx; if (o.fx) { if ($.isArray(o.fx)) { hideFx = o.fx[0]; showFx = o.fx[1]; } else { hideFx = showFx = o.fx; } }
                function resetStyle($el, fx) { $el.css("display", ""); if (!$.support.opacity && fx.opacity) { $el[0].style.removeAttribute("filter"); } }
                var showTab = showFx ? function (clicked, $show) {
                    $(clicked).closest("li").addClass("ui-tabs-selected ui-state-active"); $show.hide().removeClass("ui-tabs-hide")
                    .animate(showFx, showFx.duration || "normal", function () { resetStyle($show, showFx); self._trigger("show", null, self._ui(clicked, $show[0])); });
                } : function (clicked, $show) { $(clicked).closest("li").addClass("ui-tabs-selected ui-state-active"); $show.removeClass("ui-tabs-hide"); self._trigger("show", null, self._ui(clicked, $show[0])); }; var hideTab = hideFx ? function (clicked, $hide) { $hide.animate(hideFx, hideFx.duration || "normal", function () { self.lis.removeClass("ui-tabs-selected ui-state-active"); $hide.addClass("ui-tabs-hide"); resetStyle($hide, hideFx); self.element.dequeue("tabs"); }); } : function (clicked, $hide, $show) { self.lis.removeClass("ui-tabs-selected ui-state-active"); $hide.addClass("ui-tabs-hide"); self.element.dequeue("tabs"); }; this.anchors.bind(o.event + ".tabs", function () {
                    var el = this, $li = $(el).closest("li"), $hide = self.panels.filter(":not(.ui-tabs-hide)"), $show = self.element.find(self._sanitizeSelector(el.hash)); if (($li.hasClass("ui-tabs-selected") && !o.collapsible) || $li.hasClass("ui-state-disabled") || $li.hasClass("ui-state-processing") || self.panels.filter(":animated").length || self._trigger("select", null, self._ui(this, $show[0])) === false) { this.blur(); return false; }
                    o.selected = self.anchors.index(this); self.abort(); if (o.collapsible) {
                        if ($li.hasClass("ui-tabs-selected")) {
                            o.selected = -1; if (o.cookie) { self._cookie(o.selected, o.cookie); }
                            self.element.queue("tabs", function () { hideTab(el, $hide); }).dequeue("tabs"); this.blur(); return false;
                        } else if (!$hide.length) {
                            if (o.cookie) { self._cookie(o.selected, o.cookie); }
                            self.element.queue("tabs", function () { showTab(el, $show); }); self.load(self.anchors.index(this)); this.blur(); return false;
                        }
                    }
                    if (o.cookie) { self._cookie(o.selected, o.cookie); }
                    if ($show.length) {
                        if ($hide.length) { self.element.queue("tabs", function () { hideTab(el, $hide); }); }
                        self.element.queue("tabs", function () { showTab(el, $show); }); self.load(self.anchors.index(this));
                    } else { throw "jQuery UI Tabs: Mismatching fragment identifier."; }
                    if ($.browser.msie) { this.blur(); }
                }); this.anchors.bind("click.tabs", function () { return false; });
            }, _getIndex: function (index) {
                if (typeof index == "string") { index = this.anchors.index(this.anchors.filter("[href$=" + index + "]")); }
                return index;
            }, destroy: function () {
                var o = this.options; this.abort(); this.element.unbind(".tabs").removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible").removeData("tabs"); this.list.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"); this.anchors.each(function () {
                    var href = $.data(this, "href.tabs"); if (href) { this.href = href; }
                    var $this = $(this).unbind(".tabs"); $.each(["href", "load", "cache"], function (i, prefix) { $this.removeData(prefix + ".tabs"); });
                }); this.lis.unbind(".tabs").add(this.panels).each(function () { if ($.data(this, "destroy.tabs")) { $(this).remove(); } else { $(this).removeClass(["ui-state-default", "ui-corner-top", "ui-tabs-selected", "ui-state-active", "ui-state-hover", "ui-state-focus", "ui-state-disabled", "ui-tabs-panel", "ui-widget-content", "ui-corner-bottom", "ui-tabs-hide"].join(" ")); } }); if (o.cookie) { this._cookie(null, o.cookie); }
                return this;
            }, add: function (url, label, index) {
                if (index === undefined) { index = this.anchors.length; }
                var self = this, o = this.options, $li = $(o.tabTemplate.replace(/#\{href\}/g, url).replace(/#\{label\}/g, label)), id = !url.indexOf("#") ? url.replace("#", "") : this._tabId($("a", $li)[0]); $li.addClass("ui-state-default ui-corner-top").data("destroy.tabs", true); var $panel = self.element.find("#" + id); if (!$panel.length) { $panel = $(o.panelTemplate).attr("id", id).data("destroy.tabs", true); }
                $panel.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide"); if (index >= this.lis.length) { $li.appendTo(this.list); $panel.appendTo(this.list[0].parentNode); } else { $li.insertBefore(this.lis[index]); $panel.insertBefore(this.panels[index]); }
                o.disabled = $.map(o.disabled, function (n, i) { return n >= index ? ++n : n; }); this._tabify(); if (this.anchors.length == 1) { o.selected = 0; $li.addClass("ui-tabs-selected ui-state-active"); $panel.removeClass("ui-tabs-hide"); this.element.queue("tabs", function () { self._trigger("show", null, self._ui(self.anchors[0], self.panels[0])); }); this.load(0); }
                this._trigger("add", null, this._ui(this.anchors[index], this.panels[index])); return this;
            }, remove: function (index) {
                index = this._getIndex(index); var o = this.options, $li = this.lis.eq(index).remove(), $panel = this.panels.eq(index).remove(); if ($li.hasClass("ui-tabs-selected") && this.anchors.length > 1) { this.select(index + (index + 1 < this.anchors.length ? 1 : -1)); }
                o.disabled = $.map($.grep(o.disabled, function (n, i) { return n != index; }), function (n, i) { return n >= index ? --n : n; }); this._tabify(); this._trigger("remove", null, this._ui($li.find("a")[0], $panel[0])); return this;
            }, enable: function (index) {
                index = this._getIndex(index); var o = this.options; if ($.inArray(index, o.disabled) == -1) { return; }
                this.lis.eq(index).removeClass("ui-state-disabled"); o.disabled = $.grep(o.disabled, function (n, i) { return n != index; }); this._trigger("enable", null, this._ui(this.anchors[index], this.panels[index])); return this;
            }, disable: function (index) {
                index = this._getIndex(index); var self = this, o = this.options; if (index != o.selected) { this.lis.eq(index).addClass("ui-state-disabled"); o.disabled.push(index); o.disabled.sort(); this._trigger("disable", null, this._ui(this.anchors[index], this.panels[index])); }
                return this;
            }, select: function (index) {
                index = this._getIndex(index); if (index == -1) { if (this.options.collapsible && this.options.selected != -1) { index = this.options.selected; } else { return this; } }
                this.anchors.eq(index).trigger(this.options.event + ".tabs"); return this;
            }, load: function (index) {
                index = this._getIndex(index); var self = this, o = this.options, a = this.anchors.eq(index)[0], url = $.data(a, "load.tabs"); this.abort(); if (!url || this.element.queue("tabs").length !== 0 && $.data(a, "cache.tabs")) { this.element.dequeue("tabs"); return; }
                this.lis.eq(index).addClass("ui-state-processing"); if (o.spinner) { var span = $("span", a); span.data("label.tabs", span.html()).html(o.spinner); }
                this.xhr = $.ajax($.extend({}, o.ajaxOptions, {
                    url: url, success: function (r, s) {
                        self.element.find(self._sanitizeSelector(a.hash)).html(r); self._cleanup(); if (o.cache) { $.data(a, "cache.tabs", true); }
                        self._trigger("load", null, self._ui(self.anchors[index], self.panels[index])); try { o.ajaxOptions.success(r, s); }
                        catch (e) { }
                    }, error: function (xhr, s, e) {
                        self._cleanup(); self._trigger("load", null, self._ui(self.anchors[index], self.panels[index])); try { o.ajaxOptions.error(xhr, s, index, a); }
                        catch (e) { }
                    }
                })); self.element.dequeue("tabs"); return this;
            }, abort: function () {
                this.element.queue([]); this.panels.stop(false, true); this.element.queue("tabs", this.element.queue("tabs").splice(-2, 2)); if (this.xhr) { this.xhr.abort(); delete this.xhr; }
                this._cleanup(); return this;
            }, url: function (index, url) { this.anchors.eq(index).removeData("cache.tabs").data("load.tabs", url); return this; }, length: function () { return this.anchors.length; }
        }); $.extend($.ui.tabs, { version: "1.8.16" }); $.extend($.ui.tabs.prototype, {
            rotation: null, rotate: function (ms, continuing) {
                var self = this, o = this.options; var rotate = self._rotate || (self._rotate = function (e) { clearTimeout(self.rotation); self.rotation = setTimeout(function () { var t = o.selected; self.select(++t < self.anchors.length ? t : 0); }, ms); if (e) { e.stopPropagation(); } }); var stop = self._unrotate || (self._unrotate = !continuing ? function (e) { if (e.clientX) { self.rotate(null); } } : function (e) { t = o.selected; rotate(); }); if (ms) { this.element.bind("tabsshow", rotate); this.anchors.bind(o.event + ".tabs", stop); rotate(); } else { clearTimeout(self.rotation); this.element.unbind("tabsshow", rotate); this.anchors.unbind(o.event + ".tabs", stop); delete this._rotate; delete this._unrotate; }
                return this;
            }
        });
    })(jQuery); (function ($, undefined) {
        $.extend($.ui, { datepicker: { version: "1.8.16" } }); var PROP_NAME = 'datepicker'; var dpuuid = new Date().getTime(); var instActive; function Datepicker() { this.debug = false; this._curInst = null; this._keyEvent = false; this._disabledInputs = []; this._datepickerShowing = false; this._inDialog = false; this._mainDivId = 'ui-datepicker-div'; this._inlineClass = 'ui-datepicker-inline'; this._appendClass = 'ui-datepicker-append'; this._triggerClass = 'ui-datepicker-trigger'; this._dialogClass = 'ui-datepicker-dialog'; this._disableClass = 'ui-datepicker-disabled'; this._unselectableClass = 'ui-datepicker-unselectable'; this._currentClass = 'ui-datepicker-current-day'; this._dayOverClass = 'ui-datepicker-days-cell-over'; this.regional = []; this.regional[''] = { closeText: 'Done', prevText: 'Prev', nextText: 'Next', currentText: 'Today', monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'], monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'], dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'], dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'], weekHeader: 'Wk', dateFormat: 'mm/dd/yy', firstDay: 0, isRTL: false, showMonthAfterYear: false, yearSuffix: '' }; this._defaults = { showOn: 'focus', showAnim: 'fadeIn', showOptions: {}, defaultDate: null, appendText: '', buttonText: '...', buttonImage: '', buttonImageOnly: false, hideIfNoPrevNext: false, navigationAsDateFormat: false, gotoCurrent: false, changeMonth: false, changeYear: false, yearRange: 'c-10:c+10', showOtherMonths: false, selectOtherMonths: false, showWeek: false, calculateWeek: this.iso8601Week, shortYearCutoff: '+10', minDate: null, maxDate: null, duration: 'fast', beforeShowDay: null, beforeShow: null, onSelect: null, onChangeMonthYear: null, onClose: null, numberOfMonths: 1, showCurrentAtPos: 0, stepMonths: 1, stepBigMonths: 12, altField: '', altFormat: '', constrainInput: true, showButtonPanel: false, autoSize: false, disabled: false }; $.extend(this._defaults, this.regional['']); this.dpDiv = bindHover($('<div id="' + this._mainDivId + '" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>')); }
        $.extend(Datepicker.prototype, {
            markerClassName: 'hasDatepicker', maxRows: 4, log: function () {
                if (this.debug)
                    console.log.apply('', arguments);
            }, _widgetDatepicker: function () { return this.dpDiv; }, setDefaults: function (settings) { extendRemove(this._defaults, settings || {}); return this; }, _attachDatepicker: function (target, settings) {
                var inlineSettings = null; for (var attrName in this._defaults) { var attrValue = target.getAttribute('date:' + attrName); if (attrValue) { inlineSettings = inlineSettings || {}; try { inlineSettings[attrName] = eval(attrValue); } catch (err) { inlineSettings[attrName] = attrValue; } } }
                var nodeName = target.nodeName.toLowerCase(); var inline = (nodeName == 'div' || nodeName == 'span'); if (!target.id) { this.uuid += 1; target.id = 'dp' + this.uuid; }
                var inst = this._newInst($(target), inline); inst.settings = $.extend({}, settings || {}, inlineSettings || {}); if (nodeName == 'input') { this._connectDatepicker(target, inst); } else if (inline) { this._inlineDatepicker(target, inst); }
            }, _newInst: function (target, inline) { var id = target[0].id.replace(/([^A-Za-z0-9_-])/g, '\\\\$1'); return { id: id, input: target, selectedDay: 0, selectedMonth: 0, selectedYear: 0, drawMonth: 0, drawYear: 0, inline: inline, dpDiv: (!inline ? this.dpDiv : bindHover($('<div class="' + this._inlineClass + ' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>'))) }; }, _connectDatepicker: function (target, inst) {
                var input = $(target); inst.append = $([]); inst.trigger = $([]); if (input.hasClass(this.markerClassName))
                    return; this._attachments(input, inst); input.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp).bind("setData.datepicker", function (event, key, value) { inst.settings[key] = value; }).bind("getData.datepicker", function (event, key) { return this._get(inst, key); }); this._autoSize(inst); $.data(target, PROP_NAME, inst); if (inst.settings.disabled) { this._disableDatepicker(target); }
            }, _attachments: function (input, inst) {
                var appendText = this._get(inst, 'appendText'); var isRTL = this._get(inst, 'isRTL'); if (inst.append)
                    inst.append.remove(); if (appendText) { inst.append = $('<span class="' + this._appendClass + '">' + appendText + '</span>'); input[isRTL ? 'before' : 'after'](inst.append); }
                input.unbind('focus', this._showDatepicker); if (inst.trigger)
                    inst.trigger.remove(); var showOn = this._get(inst, 'showOn'); if (showOn == 'focus' || showOn == 'both')
                        input.focus(this._showDatepicker); if (showOn == 'button' || showOn == 'both') {
                            var buttonText = this._get(inst, 'buttonText'); var buttonImage = this._get(inst, 'buttonImage'); inst.trigger = $(this._get(inst, 'buttonImageOnly') ? $('<img/>').addClass(this._triggerClass).attr({ src: buttonImage, alt: buttonText, title: buttonText }) : $('<button type="button"></button>').addClass(this._triggerClass).html(buttonImage == '' ? buttonText : $('<img/>').attr({ src: buttonImage, alt: buttonText, title: buttonText }))); input[isRTL ? 'before' : 'after'](inst.trigger); inst.trigger.click(function () {
                                if ($.datepicker._datepickerShowing && $.datepicker._lastInput == input[0])
                                    $.datepicker._hideDatepicker(); else
                                    $.datepicker._showDatepicker(input[0]); return false;
                            });
                        }
            }, _autoSize: function (inst) {
                if (this._get(inst, 'autoSize') && !inst.inline) {
                    var date = new Date(2009, 12 - 1, 20); var dateFormat = this._get(inst, 'dateFormat'); if (dateFormat.match(/[DM]/)) {
                        var findMax = function (names) {
                            var max = 0; var maxI = 0; for (var i = 0; i < names.length; i++) { if (names[i].length > max) { max = names[i].length; maxI = i; } }
                            return maxI;
                        }; date.setMonth(findMax(this._get(inst, (dateFormat.match(/MM/) ? 'monthNames' : 'monthNamesShort')))); date.setDate(findMax(this._get(inst, (dateFormat.match(/DD/) ? 'dayNames' : 'dayNamesShort'))) + 20 - date.getDay());
                    }
                    inst.input.attr('size', this._formatDate(inst, date).length);
                }
            }, _inlineDatepicker: function (target, inst) {
                var divSpan = $(target); if (divSpan.hasClass(this.markerClassName))
                    return; divSpan.addClass(this.markerClassName).append(inst.dpDiv).bind("setData.datepicker", function (event, key, value) { inst.settings[key] = value; }).bind("getData.datepicker", function (event, key) { return this._get(inst, key); }); $.data(target, PROP_NAME, inst); this._setDate(inst, this._getDefaultDate(inst), true); this._updateDatepicker(inst); this._updateAlternate(inst); if (inst.settings.disabled) { this._disableDatepicker(target); }
                inst.dpDiv.css("display", "block");
            }, _dialogDatepicker: function (input, date, onSelect, settings, pos) {
                var inst = this._dialogInst; if (!inst) { this.uuid += 1; var id = 'dp' + this.uuid; this._dialogInput = $('<input type="text" id="' + id + '" style="position: absolute; top: -100px; width: 0px; z-index: -10;"/>'); this._dialogInput.keydown(this._doKeyDown); $('body').append(this._dialogInput); inst = this._dialogInst = this._newInst(this._dialogInput, false); inst.settings = {}; $.data(this._dialogInput[0], PROP_NAME, inst); }
                extendRemove(inst.settings, settings || {}); date = (date && date.constructor == Date ? this._formatDate(inst, date) : date); this._dialogInput.val(date); this._pos = (pos ? (pos.length ? pos : [pos.pageX, pos.pageY]) : null); if (!this._pos) { var browserWidth = document.documentElement.clientWidth; var browserHeight = document.documentElement.clientHeight; var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft; var scrollY = document.documentElement.scrollTop || document.body.scrollTop; this._pos = [(browserWidth / 2) - 100 + scrollX, (browserHeight / 2) - 150 + scrollY]; }
                this._dialogInput.css('left', (this._pos[0] + 20) + 'px').css('top', this._pos[1] + 'px'); inst.settings.onSelect = onSelect; this._inDialog = true; this.dpDiv.addClass(this._dialogClass); this._showDatepicker(this._dialogInput[0]); if ($.blockUI)
                    $.blockUI(this.dpDiv); $.data(this._dialogInput[0], PROP_NAME, inst); return this;
            }, _destroyDatepicker: function (target) {
                var $target = $(target); var inst = $.data(target, PROP_NAME); if (!$target.hasClass(this.markerClassName)) { return; }
                var nodeName = target.nodeName.toLowerCase(); $.removeData(target, PROP_NAME); if (nodeName == 'input') { inst.append.remove(); inst.trigger.remove(); $target.removeClass(this.markerClassName).unbind('focus', this._showDatepicker).unbind('keydown', this._doKeyDown).unbind('keypress', this._doKeyPress).unbind('keyup', this._doKeyUp); } else if (nodeName == 'div' || nodeName == 'span')
                    $target.removeClass(this.markerClassName).empty();
            }, _enableDatepicker: function (target) {
                var $target = $(target); var inst = $.data(target, PROP_NAME); if (!$target.hasClass(this.markerClassName)) { return; }
                var nodeName = target.nodeName.toLowerCase(); if (nodeName == 'input') { target.disabled = false; inst.trigger.filter('button').each(function () { this.disabled = false; }).end().filter('img').css({ opacity: '1.0', cursor: '' }); }
                else if (nodeName == 'div' || nodeName == 'span') { var inline = $target.children('.' + this._inlineClass); inline.children().removeClass('ui-state-disabled'); inline.find("select.ui-datepicker-month, select.ui-datepicker-year").removeAttr("disabled"); }
                this._disabledInputs = $.map(this._disabledInputs, function (value) { return (value == target ? null : value); });
            }, _disableDatepicker: function (target) {
                var $target = $(target); var inst = $.data(target, PROP_NAME); if (!$target.hasClass(this.markerClassName)) { return; }
                var nodeName = target.nodeName.toLowerCase(); if (nodeName == 'input') { target.disabled = true; inst.trigger.filter('button').each(function () { this.disabled = true; }).end().filter('img').css({ opacity: '0.5', cursor: 'default' }); }
                else if (nodeName == 'div' || nodeName == 'span') { var inline = $target.children('.' + this._inlineClass); inline.children().addClass('ui-state-disabled'); inline.find("select.ui-datepicker-month, select.ui-datepicker-year").attr("disabled", "disabled"); }
                this._disabledInputs = $.map(this._disabledInputs, function (value) { return (value == target ? null : value); }); this._disabledInputs[this._disabledInputs.length] = target;
            }, _isDisabledDatepicker: function (target) {
                if (!target) { return false; }
                for (var i = 0; i < this._disabledInputs.length; i++) {
                    if (this._disabledInputs[i] == target)
                        return true;
                }
                return false;
            }, _getInst: function (target) {
                try { return $.data(target, PROP_NAME); }
                catch (err) { throw 'Missing instance data for this datepicker'; }
            }, _optionDatepicker: function (target, name, value) {
                var inst = this._getInst(target); if (arguments.length == 2 && typeof name == 'string') { return (name == 'defaults' ? $.extend({}, $.datepicker._defaults) : (inst ? (name == 'all' ? $.extend({}, inst.settings) : this._get(inst, name)) : null)); }
                var settings = name || {}; if (typeof name == 'string') { settings = {}; settings[name] = value; }
                if (inst) {
                    if (this._curInst == inst) { this._hideDatepicker(); }
                    var date = this._getDateDatepicker(target, true); var minDate = this._getMinMaxDate(inst, 'min'); var maxDate = this._getMinMaxDate(inst, 'max'); extendRemove(inst.settings, settings); if (minDate !== null && settings['dateFormat'] !== undefined && settings['minDate'] === undefined)
                        inst.settings.minDate = this._formatDate(inst, minDate); if (maxDate !== null && settings['dateFormat'] !== undefined && settings['maxDate'] === undefined)
                            inst.settings.maxDate = this._formatDate(inst, maxDate); this._attachments($(target), inst); this._autoSize(inst); this._setDate(inst, date); this._updateAlternate(inst); this._updateDatepicker(inst);
                }
            }, _changeDatepicker: function (target, name, value) { this._optionDatepicker(target, name, value); }, _refreshDatepicker: function (target) { var inst = this._getInst(target); if (inst) { this._updateDatepicker(inst); } }, _setDateDatepicker: function (target, date) { var inst = this._getInst(target); if (inst) { this._setDate(inst, date); this._updateDatepicker(inst); this._updateAlternate(inst); } }, _getDateDatepicker: function (target, noDefault) {
                var inst = this._getInst(target); if (inst && !inst.inline)
                    this._setDateFromField(inst, noDefault); return (inst ? this._getDate(inst) : null);
            }, _doKeyDown: function (event) {
                var inst = $.datepicker._getInst(event.target); var handled = true; var isRTL = inst.dpDiv.is('.ui-datepicker-rtl'); inst._keyEvent = true; if ($.datepicker._datepickerShowing)
                    switch (event.keyCode) {
                        case 9: $.datepicker._hideDatepicker(); handled = false; break; case 13: var sel = $('td.' + $.datepicker._dayOverClass + ':not(.' +
                        $.datepicker._currentClass + ')', inst.dpDiv); if (sel[0])
                            $.datepicker._selectDay(event.target, inst.selectedMonth, inst.selectedYear, sel[0]); var onSelect = $.datepicker._get(inst, 'onSelect'); if (onSelect) { var dateStr = $.datepicker._formatDate(inst); onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]); }
                            else
                                $.datepicker._hideDatepicker(); return false; break; case 27: $.datepicker._hideDatepicker(); break; case 33: $.datepicker._adjustDate(event.target, (event.ctrlKey ? -$.datepicker._get(inst, 'stepBigMonths') : -$.datepicker._get(inst, 'stepMonths')), 'M'); break; case 34: $.datepicker._adjustDate(event.target, (event.ctrlKey ? +$.datepicker._get(inst, 'stepBigMonths') : +$.datepicker._get(inst, 'stepMonths')), 'M'); break; case 35: if (event.ctrlKey || event.metaKey) $.datepicker._clearDate(event.target); handled = event.ctrlKey || event.metaKey; break; case 36: if (event.ctrlKey || event.metaKey) $.datepicker._gotoToday(event.target); handled = event.ctrlKey || event.metaKey; break; case 37: if (event.ctrlKey || event.metaKey) $.datepicker._adjustDate(event.target, (isRTL ? +1 : -1), 'D'); handled = event.ctrlKey || event.metaKey; if (event.originalEvent.altKey) $.datepicker._adjustDate(event.target, (event.ctrlKey ? -$.datepicker._get(inst, 'stepBigMonths') : -$.datepicker._get(inst, 'stepMonths')), 'M'); break; case 38: if (event.ctrlKey || event.metaKey) $.datepicker._adjustDate(event.target, -7, 'D'); handled = event.ctrlKey || event.metaKey; break; case 39: if (event.ctrlKey || event.metaKey) $.datepicker._adjustDate(event.target, (isRTL ? -1 : +1), 'D'); handled = event.ctrlKey || event.metaKey; if (event.originalEvent.altKey) $.datepicker._adjustDate(event.target, (event.ctrlKey ? +$.datepicker._get(inst, 'stepBigMonths') : +$.datepicker._get(inst, 'stepMonths')), 'M'); break; case 40: if (event.ctrlKey || event.metaKey) $.datepicker._adjustDate(event.target, +7, 'D'); handled = event.ctrlKey || event.metaKey; break; default: handled = false;
                    }
                else if (event.keyCode == 36 && event.ctrlKey)
                    $.datepicker._showDatepicker(this); else { handled = false; }
                if (handled) { event.preventDefault(); event.stopPropagation(); }
            }, _doKeyPress: function (event) { var inst = $.datepicker._getInst(event.target); if ($.datepicker._get(inst, 'constrainInput')) { var chars = $.datepicker._possibleChars($.datepicker._get(inst, 'dateFormat')); var chr = String.fromCharCode(event.charCode == undefined ? event.keyCode : event.charCode); return event.ctrlKey || event.metaKey || (chr < ' ' || !chars || chars.indexOf(chr) > -1); } }, _doKeyUp: function (event) {
                var inst = $.datepicker._getInst(event.target); if (inst.input.val() != inst.lastVal) {
                    try { var date = $.datepicker.parseDate($.datepicker._get(inst, 'dateFormat'), (inst.input ? inst.input.val() : null), $.datepicker._getFormatConfig(inst)); if (date) { $.datepicker._setDateFromField(inst); $.datepicker._updateAlternate(inst); $.datepicker._updateDatepicker(inst); } }
                    catch (event) { $.datepicker.log(event); }
                }
                return true;
            }, _showDatepicker: function (input) {
                input = input.target || input; if (input.nodeName.toLowerCase() != 'input')
                    input = $('input', input.parentNode)[0]; if ($.datepicker._isDisabledDatepicker(input) || $.datepicker._lastInput == input)
                        return; var inst = $.datepicker._getInst(input); if ($.datepicker._curInst && $.datepicker._curInst != inst) {
                            if ($.datepicker._datepickerShowing) { $.datepicker._triggerOnClose($.datepicker._curInst); }
                            $.datepicker._curInst.dpDiv.stop(true, true);
                        }
                var beforeShow = $.datepicker._get(inst, 'beforeShow'); var beforeShowSettings = beforeShow ? beforeShow.apply(input, [input, inst]) : {}; if (beforeShowSettings === false) { return; }
                extendRemove(inst.settings, beforeShowSettings); inst.lastVal = null; $.datepicker._lastInput = input; $.datepicker._setDateFromField(inst); if ($.datepicker._inDialog)
                    input.value = ''; if (!$.datepicker._pos) { $.datepicker._pos = $.datepicker._findPos(input); $.datepicker._pos[1] += input.offsetHeight; }
                var isFixed = false; $(input).parents().each(function () { isFixed |= $(this).css('position') == 'fixed'; return !isFixed; }); if (isFixed && $.browser.opera) { $.datepicker._pos[0] -= document.documentElement.scrollLeft; $.datepicker._pos[1] -= document.documentElement.scrollTop; }
                var offset = { left: $.datepicker._pos[0], top: $.datepicker._pos[1] }; $.datepicker._pos = null; inst.dpDiv.empty(); inst.dpDiv.css({ position: 'absolute', display: 'block', top: '-1000px' }); $.datepicker._updateDatepicker(inst); offset = $.datepicker._checkOffset(inst, offset, isFixed); inst.dpDiv.css({ position: ($.datepicker._inDialog && $.blockUI ? 'static' : (isFixed ? 'fixed' : 'absolute')), display: 'none', left: offset.left + 'px', top: offset.top + 'px' }); if (!inst.inline) {
                    var showAnim = $.datepicker._get(inst, 'showAnim'); var duration = $.datepicker._get(inst, 'duration'); var postProcess = function () { var cover = inst.dpDiv.find('iframe.ui-datepicker-cover'); if (!!cover.length) { var borders = $.datepicker._getBorders(inst.dpDiv); cover.css({ left: -borders[0], top: -borders[1], width: inst.dpDiv.outerWidth(), height: inst.dpDiv.outerHeight() }); } }; inst.dpDiv.zIndex($(input).zIndex() + 1); $.datepicker._datepickerShowing = true; if ($.effects && $.effects[showAnim])
                        inst.dpDiv.show(showAnim, $.datepicker._get(inst, 'showOptions'), duration, postProcess); else
                        inst.dpDiv[showAnim || 'show']((showAnim ? duration : null), postProcess); if (!showAnim || !duration)
                            postProcess(); if (inst.input.is(':visible') && !inst.input.is(':disabled'))
                                inst.input.focus(); $.datepicker._curInst = inst;
                }
            }, _updateDatepicker: function (inst) {
                var self = this; self.maxRows = 4; var borders = $.datepicker._getBorders(inst.dpDiv); instActive = inst; inst.dpDiv.empty().append(this._generateHTML(inst)); var cover = inst.dpDiv.find('iframe.ui-datepicker-cover'); if (!!cover.length) { cover.css({ left: -borders[0], top: -borders[1], width: inst.dpDiv.outerWidth(), height: inst.dpDiv.outerHeight() }) }
                inst.dpDiv.find('.' + this._dayOverClass + ' a').mouseover(); var numMonths = this._getNumberOfMonths(inst); var cols = numMonths[1]; var width = 17; inst.dpDiv.removeClass('ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4').width(''); if (cols > 1)
                    inst.dpDiv.addClass('ui-datepicker-multi-' + cols).css('width', (width * cols) + 'em'); inst.dpDiv[(numMonths[0] != 1 || numMonths[1] != 1 ? 'add' : 'remove') + 'Class']('ui-datepicker-multi'); inst.dpDiv[(this._get(inst, 'isRTL') ? 'add' : 'remove') + 'Class']('ui-datepicker-rtl'); if (inst == $.datepicker._curInst && $.datepicker._datepickerShowing && inst.input && inst.input.is(':visible') && !inst.input.is(':disabled') && inst.input[0] != document.activeElement)
                        inst.input.focus(); if (inst.yearshtml) {
                            var origyearshtml = inst.yearshtml; setTimeout(function () {
                                if (origyearshtml === inst.yearshtml && inst.yearshtml) { inst.dpDiv.find('select.ui-datepicker-year:first').replaceWith(inst.yearshtml); }
                                origyearshtml = inst.yearshtml = null;
                            }, 0);
                        }
            }, _getBorders: function (elem) { var convert = function (value) { return { thin: 1, medium: 2, thick: 3 }[value] || value; }; return [parseFloat(convert(elem.css('border-left-width'))), parseFloat(convert(elem.css('border-top-width')))]; }, _checkOffset: function (inst, offset, isFixed) { var dpWidth = inst.dpDiv.outerWidth(); var dpHeight = inst.dpDiv.outerHeight(); var inputWidth = inst.input ? inst.input.outerWidth() : 0; var inputHeight = inst.input ? inst.input.outerHeight() : 0; var viewWidth = document.documentElement.clientWidth + $(document).scrollLeft(); var viewHeight = document.documentElement.clientHeight + $(document).scrollTop(); offset.left -= (this._get(inst, 'isRTL') ? (dpWidth - inputWidth) : 0); offset.left -= (isFixed && offset.left == inst.input.offset().left) ? $(document).scrollLeft() : 0; offset.top -= (isFixed && offset.top == (inst.input.offset().top + inputHeight)) ? $(document).scrollTop() : 0; offset.left -= Math.min(offset.left, (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ? Math.abs(offset.left + dpWidth - viewWidth) : 0); offset.top -= Math.min(offset.top, (offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ? Math.abs(dpHeight + inputHeight) : 0); return offset; }, _findPos: function (obj) {
                var inst = this._getInst(obj); var isRTL = this._get(inst, 'isRTL'); while (obj && (obj.type == 'hidden' || obj.nodeType != 1 || $.expr.filters.hidden(obj))) { obj = obj[isRTL ? 'previousSibling' : 'nextSibling']; }
                var position = $(obj).offset(); return [position.left, position.top];
            }, _triggerOnClose: function (inst) {
                var onClose = this._get(inst, 'onClose'); if (onClose)
                    onClose.apply((inst.input ? inst.input[0] : null), [(inst.input ? inst.input.val() : ''), inst]);
            }, _hideDatepicker: function (input) {
                var inst = this._curInst; if (!inst || (input && inst != $.data(input, PROP_NAME)))
                    return; if (this._datepickerShowing) {
                        var showAnim = this._get(inst, 'showAnim'); var duration = this._get(inst, 'duration'); var postProcess = function () { $.datepicker._tidyDialog(inst); this._curInst = null; }; if ($.effects && $.effects[showAnim])
                            inst.dpDiv.hide(showAnim, $.datepicker._get(inst, 'showOptions'), duration, postProcess); else
                            inst.dpDiv[(showAnim == 'slideDown' ? 'slideUp' : (showAnim == 'fadeIn' ? 'fadeOut' : 'hide'))]((showAnim ? duration : null), postProcess); if (!showAnim)
                                postProcess(); $.datepicker._triggerOnClose(inst); this._datepickerShowing = false; this._lastInput = null; if (this._inDialog) { this._dialogInput.css({ position: 'absolute', left: '0', top: '-100px' }); if ($.blockUI) { $.unblockUI(); $('body').append(this.dpDiv); } }
                        this._inDialog = false;
                    }
            }, _tidyDialog: function (inst) { inst.dpDiv.removeClass(this._dialogClass).unbind('.ui-datepicker-calendar'); }, _checkExternalClick: function (event) {
                if (!$.datepicker._curInst)
                    return; var $target = $(event.target); if ($target[0].id != $.datepicker._mainDivId && $target.parents('#' + $.datepicker._mainDivId).length == 0 && !$target.hasClass($.datepicker.markerClassName) && !$target.hasClass($.datepicker._triggerClass) && $.datepicker._datepickerShowing && !($.datepicker._inDialog && $.blockUI))
                        $.datepicker._hideDatepicker();
            }, _adjustDate: function (id, offset, period) {
                var target = $(id); var inst = this._getInst(target[0]); if (this._isDisabledDatepicker(target[0])) { return; }
                this._adjustInstDate(inst, offset +
                (period == 'M' ? this._get(inst, 'showCurrentAtPos') : 0), period); this._updateDatepicker(inst);
            }, _gotoToday: function (id) {
                var target = $(id); var inst = this._getInst(target[0]); if (this._get(inst, 'gotoCurrent') && inst.currentDay) { inst.selectedDay = inst.currentDay; inst.drawMonth = inst.selectedMonth = inst.currentMonth; inst.drawYear = inst.selectedYear = inst.currentYear; }
                else { var date = new Date(); inst.selectedDay = date.getDate(); inst.drawMonth = inst.selectedMonth = date.getMonth(); inst.drawYear = inst.selectedYear = date.getFullYear(); }
                this._notifyChange(inst); this._adjustDate(target);
            }, _selectMonthYear: function (id, select, period) { var target = $(id); var inst = this._getInst(target[0]); inst['selected' + (period == 'M' ? 'Month' : 'Year')] = inst['draw' + (period == 'M' ? 'Month' : 'Year')] = parseInt(select.options[select.selectedIndex].value, 10); this._notifyChange(inst); this._adjustDate(target); }, _selectDay: function (id, month, year, td) {
                var target = $(id); if ($(td).hasClass(this._unselectableClass) || this._isDisabledDatepicker(target[0])) { return; }
                var inst = this._getInst(target[0]); inst.selectedDay = inst.currentDay = $('a', td).html(); inst.selectedMonth = inst.currentMonth = month; inst.selectedYear = inst.currentYear = year; this._selectDate(id, this._formatDate(inst, inst.currentDay, inst.currentMonth, inst.currentYear));
            }, _clearDate: function (id) { var target = $(id); var inst = this._getInst(target[0]); this._selectDate(target, ''); }, _selectDate: function (id, dateStr) {
                var target = $(id); var inst = this._getInst(target[0]); dateStr = (dateStr != null ? dateStr : this._formatDate(inst)); if (inst.input)
                    inst.input.val(dateStr); this._updateAlternate(inst); var onSelect = this._get(inst, 'onSelect'); if (onSelect)
                        onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]); else if (inst.input)
                            inst.input.trigger('change'); if (inst.inline)
                                this._updateDatepicker(inst); else {
                                this._hideDatepicker(); this._lastInput = inst.input[0]; if (typeof (inst.input[0]) != 'object')
                                    inst.input.focus(); this._lastInput = null;
                            }
            }, _updateAlternate: function (inst) { var altField = this._get(inst, 'altField'); if (altField) { var altFormat = this._get(inst, 'altFormat') || this._get(inst, 'dateFormat'); var date = this._getDate(inst); var dateStr = this.formatDate(altFormat, date, this._getFormatConfig(inst)); $(altField).each(function () { $(this).val(dateStr); }); } }, noWeekends: function (date) { var day = date.getDay(); return [(day > 0 && day < 6), '']; }, iso8601Week: function (date) { var checkDate = new Date(date.getTime()); checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7)); var time = checkDate.getTime(); checkDate.setMonth(0); checkDate.setDate(1); return Math.floor(Math.round((time - checkDate) / 86400000) / 7) + 1; }, parseDate: function (format, value, settings) {
                if (format == null || value == null)
                    throw 'Invalid arguments'; value = (typeof value == 'object' ? value.toString() : value + ''); if (value == '')
                        return null; var shortYearCutoff = (settings ? settings.shortYearCutoff : null) || this._defaults.shortYearCutoff; shortYearCutoff = (typeof shortYearCutoff != 'string' ? shortYearCutoff : new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10)); var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort; var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames; var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort; var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames; var year = -1; var month = -1; var day = -1; var doy = -1; var literal = false; var lookAhead = function (match) {
                            var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match); if (matches)
                                iFormat++; return matches;
                        }; var getNumber = function (match) {
                            var isDoubled = lookAhead(match); var size = (match == '@' ? 14 : (match == '!' ? 20 : (match == 'y' && isDoubled ? 4 : (match == 'o' ? 3 : 2)))); var digits = new RegExp('^\\d{1,' + size + '}'); var num = value.substring(iValue).match(digits); if (!num)
                                throw 'Missing number at position ' + iValue; iValue += num[0].length; return parseInt(num[0], 10);
                        }; var getName = function (match, shortNames, longNames) {
                            var names = $.map(lookAhead(match) ? longNames : shortNames, function (v, k) { return [[k, v]]; }).sort(function (a, b) { return -(a[1].length - b[1].length); }); var index = -1; $.each(names, function (i, pair) { var name = pair[1]; if (value.substr(iValue, name.length).toLowerCase() == name.toLowerCase()) { index = pair[0]; iValue += name.length; return false; } }); if (index != -1)
                                return index + 1; else
                                throw 'Unknown name at position ' + iValue;
                        }; var checkLiteral = function () {
                            if (value.charAt(iValue) != format.charAt(iFormat))
                                throw 'Unexpected literal at position ' + iValue; iValue++;
                        }; var iValue = 0; for (var iFormat = 0; iFormat < format.length; iFormat++) {
                            if (literal)
                                if (format.charAt(iFormat) == "'" && !lookAhead("'"))
                                    literal = false; else
                                    checkLiteral(); else
                                switch (format.charAt(iFormat)) {
                                    case 'd': day = getNumber('d'); break; case 'D': getName('D', dayNamesShort, dayNames); break; case 'o': doy = getNumber('o'); break; case 'm': month = getNumber('m'); break; case 'M': month = getName('M', monthNamesShort, monthNames); break; case 'y': year = getNumber('y'); break; case '@': var date = new Date(getNumber('@')); year = date.getFullYear(); month = date.getMonth() + 1; day = date.getDate(); break; case '!': var date = new Date((getNumber('!') - this._ticksTo1970) / 10000); year = date.getFullYear(); month = date.getMonth() + 1; day = date.getDate(); break; case "'": if (lookAhead("'"))
                                        checkLiteral(); else
                                        literal = true; break; default: checkLiteral();
                                }
                        }
                if (iValue < value.length) { throw "Extra/unparsed characters found in date: " + value.substring(iValue); }
                if (year == -1)
                    year = new Date().getFullYear(); else if (year < 100)
                        year += new Date().getFullYear() - new Date().getFullYear() % 100 +
                        (year <= shortYearCutoff ? 0 : -100); if (doy > -1) {
                            month = 1; day = doy; do {
                                var dim = this._getDaysInMonth(year, month - 1); if (day <= dim)
                                    break; month++; day -= dim;
                            } while (true);
                        }
                var date = this._daylightSavingAdjust(new Date(year, month - 1, day)); if (date.getFullYear() != year || date.getMonth() + 1 != month || date.getDate() != day)
                    throw 'Invalid date'; return date;
            }, ATOM: 'yy-mm-dd', COOKIE: 'D, dd M yy', ISO_8601: 'yy-mm-dd', RFC_822: 'D, d M y', RFC_850: 'DD, dd-M-y', RFC_1036: 'D, d M y', RFC_1123: 'D, d M yy', RFC_2822: 'D, d M yy', RSS: 'D, d M y', TICKS: '!', TIMESTAMP: '@', W3C: 'yy-mm-dd', _ticksTo1970: (((1970 - 1) * 365 + Math.floor(1970 / 4) - Math.floor(1970 / 100) +
            Math.floor(1970 / 400)) * 24 * 60 * 60 * 10000000), formatDate: function (format, date, settings) {
                if (!date)
                    return ''; var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort; var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames; var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort; var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames; var lookAhead = function (match) {
                        var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match); if (matches)
                            iFormat++; return matches;
                    }; var formatNumber = function (match, value, len) {
                        var num = '' + value; if (lookAhead(match))
                            while (num.length < len)
                                num = '0' + num; return num;
                    }; var formatName = function (match, value, shortNames, longNames) { return (lookAhead(match) ? longNames[value] : shortNames[value]); }; var output = ''; var literal = false; if (date)
                        for (var iFormat = 0; iFormat < format.length; iFormat++) {
                            if (literal)
                                if (format.charAt(iFormat) == "'" && !lookAhead("'"))
                                    literal = false; else
                                    output += format.charAt(iFormat); else
                                switch (format.charAt(iFormat)) {
                                    case 'd': output += formatNumber('d', date.getDate(), 2); break; case 'D': output += formatName('D', date.getDay(), dayNamesShort, dayNames); break; case 'o': output += formatNumber('o', Math.round((new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - new Date(date.getFullYear(), 0, 0).getTime()) / 86400000), 3); break; case 'm': output += formatNumber('m', date.getMonth() + 1, 2); break; case 'M': output += formatName('M', date.getMonth(), monthNamesShort, monthNames); break; case 'y': output += (lookAhead('y') ? date.getFullYear() : (date.getYear() % 100 < 10 ? '0' : '') + date.getYear() % 100); break; case '@': output += date.getTime(); break; case '!': output += date.getTime() * 10000 + this._ticksTo1970; break; case "'": if (lookAhead("'"))
                                        output += "'"; else
                                        literal = true; break; default: output += format.charAt(iFormat);
                                }
                        }
                return output;
            }, _possibleChars: function (format) {
                var chars = ''; var literal = false; var lookAhead = function (match) {
                    var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match); if (matches)
                        iFormat++; return matches;
                }; for (var iFormat = 0; iFormat < format.length; iFormat++)
                    if (literal)
                        if (format.charAt(iFormat) == "'" && !lookAhead("'"))
                            literal = false; else
                            chars += format.charAt(iFormat); else
                        switch (format.charAt(iFormat)) {
                            case 'd': case 'm': case 'y': case '@': chars += '0123456789'; break; case 'D': case 'M': return null; case "'": if (lookAhead("'"))
                                chars += "'"; else
                                literal = true; break; default: chars += format.charAt(iFormat);
                        }
                return chars;
            }, _get: function (inst, name) { return inst.settings[name] !== undefined ? inst.settings[name] : this._defaults[name]; }, _setDateFromField: function (inst, noDefault) {
                if (inst.input.val() == inst.lastVal) { return; }
                var dateFormat = this._get(inst, 'dateFormat'); var dates = inst.lastVal = inst.input ? inst.input.val() : null; var date, defaultDate; date = defaultDate = this._getDefaultDate(inst); var settings = this._getFormatConfig(inst); try { date = this.parseDate(dateFormat, dates, settings) || defaultDate; } catch (event) { this.log(event); dates = (noDefault ? '' : dates); }
                inst.selectedDay = date.getDate(); inst.drawMonth = inst.selectedMonth = date.getMonth(); inst.drawYear = inst.selectedYear = date.getFullYear(); inst.currentDay = (dates ? date.getDate() : 0); inst.currentMonth = (dates ? date.getMonth() : 0); inst.currentYear = (dates ? date.getFullYear() : 0); this._adjustInstDate(inst);
            }, _getDefaultDate: function (inst) { return this._restrictMinMax(inst, this._determineDate(inst, this._get(inst, 'defaultDate'), new Date())); }, _determineDate: function (inst, date, defaultDate) {
                var offsetNumeric = function (offset) { var date = new Date(); date.setDate(date.getDate() + offset); return date; }; var offsetString = function (offset) {
                    try { return $.datepicker.parseDate($.datepicker._get(inst, 'dateFormat'), offset, $.datepicker._getFormatConfig(inst)); }
                    catch (e) { }
                    var date = (offset.toLowerCase().match(/^c/) ? $.datepicker._getDate(inst) : null) || new Date(); var year = date.getFullYear(); var month = date.getMonth(); var day = date.getDate(); var pattern = /([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g; var matches = pattern.exec(offset); while (matches) {
                        switch (matches[2] || 'd') { case 'd': case 'D': day += parseInt(matches[1], 10); break; case 'w': case 'W': day += parseInt(matches[1], 10) * 7; break; case 'm': case 'M': month += parseInt(matches[1], 10); day = Math.min(day, $.datepicker._getDaysInMonth(year, month)); break; case 'y': case 'Y': year += parseInt(matches[1], 10); day = Math.min(day, $.datepicker._getDaysInMonth(year, month)); break; }
                        matches = pattern.exec(offset);
                    }
                    return new Date(year, month, day);
                }; var newDate = (date == null || date === '' ? defaultDate : (typeof date == 'string' ? offsetString(date) : (typeof date == 'number' ? (isNaN(date) ? defaultDate : offsetNumeric(date)) : new Date(date.getTime())))); newDate = (newDate && newDate.toString() == 'Invalid Date' ? defaultDate : newDate); if (newDate) { newDate.setHours(0); newDate.setMinutes(0); newDate.setSeconds(0); newDate.setMilliseconds(0); }
                return this._daylightSavingAdjust(newDate);
            }, _daylightSavingAdjust: function (date) { if (!date) return null; date.setHours(date.getHours() > 12 ? date.getHours() + 2 : 0); return date; }, _setDate: function (inst, date, noChange) {
                var clear = !date; var origMonth = inst.selectedMonth; var origYear = inst.selectedYear; var newDate = this._restrictMinMax(inst, this._determineDate(inst, date, new Date())); inst.selectedDay = inst.currentDay = newDate.getDate(); inst.drawMonth = inst.selectedMonth = inst.currentMonth = newDate.getMonth(); inst.drawYear = inst.selectedYear = inst.currentYear = newDate.getFullYear(); if ((origMonth != inst.selectedMonth || origYear != inst.selectedYear) && !noChange)
                    this._notifyChange(inst); this._adjustInstDate(inst); if (inst.input) { inst.input.val(clear ? '' : this._formatDate(inst)); }
            }, _getDate: function (inst) { var startDate = (!inst.currentYear || (inst.input && inst.input.val() == '') ? null : this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay))); return startDate; }, _generateHTML: function (inst) {
                var today = new Date(); today = this._daylightSavingAdjust(new Date(today.getFullYear(), today.getMonth(), today.getDate())); var isRTL = this._get(inst, 'isRTL'); var showButtonPanel = this._get(inst, 'showButtonPanel'); var hideIfNoPrevNext = this._get(inst, 'hideIfNoPrevNext'); var navigationAsDateFormat = this._get(inst, 'navigationAsDateFormat'); var numMonths = this._getNumberOfMonths(inst); var showCurrentAtPos = this._get(inst, 'showCurrentAtPos'); var stepMonths = this._get(inst, 'stepMonths'); var isMultiMonth = (numMonths[0] != 1 || numMonths[1] != 1); var currentDate = this._daylightSavingAdjust((!inst.currentDay ? new Date(9999, 9, 9) : new Date(inst.currentYear, inst.currentMonth, inst.currentDay))); var minDate = this._getMinMaxDate(inst, 'min'); var maxDate = this._getMinMaxDate(inst, 'max'); var drawMonth = inst.drawMonth - showCurrentAtPos; var drawYear = inst.drawYear; if (drawMonth < 0) { drawMonth += 12; drawYear--; }
                if (maxDate) { var maxDraw = this._daylightSavingAdjust(new Date(maxDate.getFullYear(), maxDate.getMonth() - (numMonths[0] * numMonths[1]) + 1, maxDate.getDate())); maxDraw = (minDate && maxDraw < minDate ? minDate : maxDraw); while (this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1)) > maxDraw) { drawMonth--; if (drawMonth < 0) { drawMonth = 11; drawYear--; } } }
                inst.drawMonth = drawMonth; inst.drawYear = drawYear; var prevText = this._get(inst, 'prevText'); prevText = (!navigationAsDateFormat ? prevText : this.formatDate(prevText, this._daylightSavingAdjust(new Date(drawYear, drawMonth - stepMonths, 1)), this._getFormatConfig(inst))); var prev = (this._canAdjustMonth(inst, -1, drawYear, drawMonth) ? '<a class="ui-datepicker-prev ui-corner-all" onclick="DP_jQuery_' + dpuuid + '.datepicker._adjustDate(\'#' + inst.id + '\', -' + stepMonths + ', \'M\');"' + ' title="' + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>' : (hideIfNoPrevNext ? '' : '<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="' + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>')); var nextText = this._get(inst, 'nextText'); nextText = (!navigationAsDateFormat ? nextText : this.formatDate(nextText, this._daylightSavingAdjust(new Date(drawYear, drawMonth + stepMonths, 1)), this._getFormatConfig(inst))); var next = (this._canAdjustMonth(inst, +1, drawYear, drawMonth) ? '<a class="ui-datepicker-next ui-corner-all" onclick="DP_jQuery_' + dpuuid + '.datepicker._adjustDate(\'#' + inst.id + '\', +' + stepMonths + ', \'M\');"' + ' title="' + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>' : (hideIfNoPrevNext ? '' : '<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="' + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>')); var currentText = this._get(inst, 'currentText'); var gotoDate = (this._get(inst, 'gotoCurrent') && inst.currentDay ? currentDate : today); currentText = (!navigationAsDateFormat ? currentText : this.formatDate(currentText, gotoDate, this._getFormatConfig(inst))); var controls = (!inst.inline ? '<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" onclick="DP_jQuery_' + dpuuid + '.datepicker._hideDatepicker();">' + this._get(inst, 'closeText') + '</button>' : ''); var buttonPanel = (showButtonPanel) ? '<div class="ui-datepicker-buttonpane ui-widget-content">' + (isRTL ? controls : '') +
                (this._isInRange(inst, gotoDate) ? '<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" onclick="DP_jQuery_' + dpuuid + '.datepicker._gotoToday(\'#' + inst.id + '\');"' + '>' + currentText + '</button>' : '') + (isRTL ? '' : controls) + '</div>' : ''; var firstDay = parseInt(this._get(inst, 'firstDay'), 10); firstDay = (isNaN(firstDay) ? 0 : firstDay); var showWeek = this._get(inst, 'showWeek'); var dayNames = this._get(inst, 'dayNames'); var dayNamesShort = this._get(inst, 'dayNamesShort'); var dayNamesMin = this._get(inst, 'dayNamesMin'); var monthNames = this._get(inst, 'monthNames'); var monthNamesShort = this._get(inst, 'monthNamesShort'); var beforeShowDay = this._get(inst, 'beforeShowDay'); var showOtherMonths = this._get(inst, 'showOtherMonths'); var selectOtherMonths = this._get(inst, 'selectOtherMonths'); var calculateWeek = this._get(inst, 'calculateWeek') || this.iso8601Week; var defaultDate = this._getDefaultDate(inst); var html = ''; for (var row = 0; row < numMonths[0]; row++) {
                    var group = ''; this.maxRows = 4; for (var col = 0; col < numMonths[1]; col++) {
                        var selectedDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, inst.selectedDay)); var cornerClass = ' ui-corner-all'; var calender = ''; if (isMultiMonth) {
                            calender += '<div class="ui-datepicker-group'; if (numMonths[1] > 1)
                                switch (col) { case 0: calender += ' ui-datepicker-group-first'; cornerClass = ' ui-corner-' + (isRTL ? 'right' : 'left'); break; case numMonths[1] - 1: calender += ' ui-datepicker-group-last'; cornerClass = ' ui-corner-' + (isRTL ? 'left' : 'right'); break; default: calender += ' ui-datepicker-group-middle'; cornerClass = ''; break; }
                            calender += '">';
                        }
                        calender += '<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix' + cornerClass + '">' +
                        (/all|left/.test(cornerClass) && row == 0 ? (isRTL ? next : prev) : '') +
                        (/all|right/.test(cornerClass) && row == 0 ? (isRTL ? prev : next) : '') +
                        this._generateMonthYearHeader(inst, drawMonth, drawYear, minDate, maxDate, row > 0 || col > 0, monthNames, monthNamesShort) + '</div><table class="ui-datepicker-calendar"><thead>' + '<tr>'; var thead = (showWeek ? '<th class="ui-datepicker-week-col">' + this._get(inst, 'weekHeader') + '</th>' : ''); for (var dow = 0; dow < 7; dow++) { var day = (dow + firstDay) % 7; thead += '<th' + ((dow + firstDay + 6) % 7 >= 5 ? ' class="ui-datepicker-week-end"' : '') + '>' + '<span title="' + dayNames[day] + '">' + dayNamesMin[day] + '</span></th>'; }
                        calender += thead + '</tr></thead><tbody>'; var daysInMonth = this._getDaysInMonth(drawYear, drawMonth); if (drawYear == inst.selectedYear && drawMonth == inst.selectedMonth)
                            inst.selectedDay = Math.min(inst.selectedDay, daysInMonth); var leadDays = (this._getFirstDayOfMonth(drawYear, drawMonth) - firstDay + 7) % 7; var curRows = Math.ceil((leadDays + daysInMonth) / 7); var numRows = (isMultiMonth ? this.maxRows > curRows ? this.maxRows : curRows : curRows); this.maxRows = numRows; var printDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1 - leadDays)); for (var dRow = 0; dRow < numRows; dRow++) {
                                calender += '<tr>'; var tbody = (!showWeek ? '' : '<td class="ui-datepicker-week-col">' +
                                this._get(inst, 'calculateWeek')(printDate) + '</td>'); for (var dow = 0; dow < 7; dow++) {
                                    var daySettings = (beforeShowDay ? beforeShowDay.apply((inst.input ? inst.input[0] : null), [printDate]) : [true, '']); var otherMonth = (printDate.getMonth() != drawMonth); var unselectable = (otherMonth && !selectOtherMonths) || !daySettings[0] || (minDate && printDate < minDate) || (maxDate && printDate > maxDate); tbody += '<td class="' +
                                    ((dow + firstDay + 6) % 7 >= 5 ? ' ui-datepicker-week-end' : '') + (otherMonth ? ' ui-datepicker-other-month' : '') + ((printDate.getTime() == selectedDate.getTime() && drawMonth == inst.selectedMonth && inst._keyEvent) || (defaultDate.getTime() == printDate.getTime() && defaultDate.getTime() == selectedDate.getTime()) ? ' ' + this._dayOverClass : '') + (unselectable ? ' ' + this._unselectableClass + ' ui-state-disabled' : '') + (otherMonth && !showOtherMonths ? '' : ' ' + daySettings[1] + (printDate.getTime() == currentDate.getTime() ? ' ' + this._currentClass : '') + (printDate.getTime() == today.getTime() ? ' ui-datepicker-today' : '')) + '"' + ((!otherMonth || showOtherMonths) && daySettings[2] ? ' title="' + daySettings[2] + '"' : '') + (unselectable ? '' : ' onclick="DP_jQuery_' + dpuuid + '.datepicker._selectDay(\'#' +
                                    inst.id + '\',' + printDate.getMonth() + ',' + printDate.getFullYear() + ', this);return false;"') + '>' + (otherMonth && !showOtherMonths ? '&#xa0;' : (unselectable ? '<span class="ui-state-default">' + printDate.getDate() + '</span>' : '<a class="ui-state-default' +
                                    (printDate.getTime() == today.getTime() ? ' ui-state-highlight' : '') +
                                    (printDate.getTime() == currentDate.getTime() ? ' ui-state-active' : '') + (otherMonth ? ' ui-priority-secondary' : '') + '" href="#">' + printDate.getDate() + '</a>')) + '</td>'; printDate.setDate(printDate.getDate() + 1); printDate = this._daylightSavingAdjust(printDate);
                                }
                                calender += tbody + '</tr>';
                            }
                        drawMonth++; if (drawMonth > 11) { drawMonth = 0; drawYear++; }
                        calender += '</tbody></table>' + (isMultiMonth ? '</div>' +
                        ((numMonths[0] > 0 && col == numMonths[1] - 1) ? '<div class="ui-datepicker-row-break"></div>' : '') : ''); group += calender;
                    }
                    html += group;
                }
                html += buttonPanel + ($.browser.msie && parseInt($.browser.version, 10) < 7 && !inst.inline ? '<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>' : ''); inst._keyEvent = false; return html;
            }, _generateMonthYearHeader: function (inst, drawMonth, drawYear, minDate, maxDate, secondary, monthNames, monthNamesShort) {
                var changeMonth = this._get(inst, 'changeMonth'); var changeYear = this._get(inst, 'changeYear'); var showMonthAfterYear = this._get(inst, 'showMonthAfterYear'); var html = '<div class="ui-datepicker-title">'; var monthHtml = ''; if (secondary || !changeMonth)
                    monthHtml += '<span class="ui-datepicker-month">' + monthNames[drawMonth] + '</span>'; else {
                    var inMinYear = (minDate && minDate.getFullYear() == drawYear); var inMaxYear = (maxDate && maxDate.getFullYear() == drawYear); monthHtml += '<select class="ui-datepicker-month" ' + 'onchange="DP_jQuery_' + dpuuid + '.datepicker._selectMonthYear(\'#' + inst.id + '\', this, \'M\');" ' + '>'; for (var month = 0; month < 12; month++) {
                        if ((!inMinYear || month >= minDate.getMonth()) && (!inMaxYear || month <= maxDate.getMonth()))
                            monthHtml += '<option value="' + month + '"' +
                            (month == drawMonth ? ' selected="selected"' : '') + '>' + monthNamesShort[month] + '</option>';
                    }
                    monthHtml += '</select>';
                }
                if (!showMonthAfterYear)
                    html += monthHtml + (secondary || !(changeMonth && changeYear) ? '&#xa0;' : ''); if (!inst.yearshtml) {
                        inst.yearshtml = ''; if (secondary || !changeYear)
                            html += '<span class="ui-datepicker-year">' + drawYear + '</span>'; else {
                            var years = this._get(inst, 'yearRange').split(':'); var thisYear = new Date().getFullYear(); var determineYear = function (value) { var year = (value.match(/c[+-].*/) ? drawYear + parseInt(value.substring(1), 10) : (value.match(/[+-].*/) ? thisYear + parseInt(value, 10) : parseInt(value, 10))); return (isNaN(year) ? thisYear : year); }; var year = determineYear(years[0]); var endYear = Math.max(year, determineYear(years[1] || '')); year = (minDate ? Math.max(year, minDate.getFullYear()) : year); endYear = (maxDate ? Math.min(endYear, maxDate.getFullYear()) : endYear); inst.yearshtml += '<select class="ui-datepicker-year" ' + 'onchange="DP_jQuery_' + dpuuid + '.datepicker._selectMonthYear(\'#' + inst.id + '\', this, \'Y\');" ' + '>'; for (; year <= endYear; year++) {
                                inst.yearshtml += '<option value="' + year + '"' +
                                (year == drawYear ? ' selected="selected"' : '') + '>' + year + '</option>';
                            }
                            inst.yearshtml += '</select>'; html += inst.yearshtml; inst.yearshtml = null;
                        }
                    }
                html += this._get(inst, 'yearSuffix'); if (showMonthAfterYear)
                    html += (secondary || !(changeMonth && changeYear) ? '&#xa0;' : '') + monthHtml; html += '</div>'; return html;
            }, _adjustInstDate: function (inst, offset, period) {
                var year = inst.drawYear + (period == 'Y' ? offset : 0); var month = inst.drawMonth + (period == 'M' ? offset : 0); var day = Math.min(inst.selectedDay, this._getDaysInMonth(year, month)) +
                (period == 'D' ? offset : 0); var date = this._restrictMinMax(inst, this._daylightSavingAdjust(new Date(year, month, day))); inst.selectedDay = date.getDate(); inst.drawMonth = inst.selectedMonth = date.getMonth(); inst.drawYear = inst.selectedYear = date.getFullYear(); if (period == 'M' || period == 'Y')
                    this._notifyChange(inst);
            }, _restrictMinMax: function (inst, date) { var minDate = this._getMinMaxDate(inst, 'min'); var maxDate = this._getMinMaxDate(inst, 'max'); var newDate = (minDate && date < minDate ? minDate : date); newDate = (maxDate && newDate > maxDate ? maxDate : newDate); return newDate; }, _notifyChange: function (inst) {
                var onChange = this._get(inst, 'onChangeMonthYear'); if (onChange)
                    onChange.apply((inst.input ? inst.input[0] : null), [inst.selectedYear, inst.selectedMonth + 1, inst]);
            }, _getNumberOfMonths: function (inst) { var numMonths = this._get(inst, 'numberOfMonths'); return (numMonths == null ? [1, 1] : (typeof numMonths == 'number' ? [1, numMonths] : numMonths)); }, _getMinMaxDate: function (inst, minMax) { return this._determineDate(inst, this._get(inst, minMax + 'Date'), null); }, _getDaysInMonth: function (year, month) { return 32 - this._daylightSavingAdjust(new Date(year, month, 32)).getDate(); }, _getFirstDayOfMonth: function (year, month) { return new Date(year, month, 1).getDay(); }, _canAdjustMonth: function (inst, offset, curYear, curMonth) {
                var numMonths = this._getNumberOfMonths(inst); var date = this._daylightSavingAdjust(new Date(curYear, curMonth + (offset < 0 ? offset : numMonths[0] * numMonths[1]), 1)); if (offset < 0)
                    date.setDate(this._getDaysInMonth(date.getFullYear(), date.getMonth())); return this._isInRange(inst, date);
            }, _isInRange: function (inst, date) { var minDate = this._getMinMaxDate(inst, 'min'); var maxDate = this._getMinMaxDate(inst, 'max'); return ((!minDate || date.getTime() >= minDate.getTime()) && (!maxDate || date.getTime() <= maxDate.getTime())); }, _getFormatConfig: function (inst) { var shortYearCutoff = this._get(inst, 'shortYearCutoff'); shortYearCutoff = (typeof shortYearCutoff != 'string' ? shortYearCutoff : new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10)); return { shortYearCutoff: shortYearCutoff, dayNamesShort: this._get(inst, 'dayNamesShort'), dayNames: this._get(inst, 'dayNames'), monthNamesShort: this._get(inst, 'monthNamesShort'), monthNames: this._get(inst, 'monthNames') }; }, _formatDate: function (inst, day, month, year) {
                if (!day) { inst.currentDay = inst.selectedDay; inst.currentMonth = inst.selectedMonth; inst.currentYear = inst.selectedYear; }
                var date = (day ? (typeof day == 'object' ? day : this._daylightSavingAdjust(new Date(year, month, day))) : this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay))); return this.formatDate(this._get(inst, 'dateFormat'), date, this._getFormatConfig(inst));
            }
        }); function bindHover(dpDiv) {
            var selector = 'button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a'; return dpDiv.bind('mouseout', function (event) {
                var elem = $(event.target).closest(selector); if (!elem.length) { return; }
                elem.removeClass("ui-state-hover ui-datepicker-prev-hover ui-datepicker-next-hover");
            }).bind('mouseover', function (event) {
                var elem = $(event.target).closest(selector); if ($.datepicker._isDisabledDatepicker(instActive.inline ? dpDiv.parent()[0] : instActive.input[0]) || !elem.length) { return; }
                elem.parents('.ui-datepicker-calendar').find('a').removeClass('ui-state-hover'); elem.addClass('ui-state-hover'); if (elem.hasClass('ui-datepicker-prev')) elem.addClass('ui-datepicker-prev-hover'); if (elem.hasClass('ui-datepicker-next')) elem.addClass('ui-datepicker-next-hover');
            });
        }
        function extendRemove(target, props) {
            $.extend(target, props); for (var name in props)
                if (props[name] == null || props[name] == undefined)
                    target[name] = props[name]; return target;
        }; function isArray(a) { return (a && (($.browser.safari && typeof a == 'object' && a.length) || (a.constructor && a.constructor.toString().match(/\Array\(\)/)))); }; $.fn.datepicker = function (options) {
            if (!this.length) { return this; }
            if (!$.datepicker.initialized) { $(document).mousedown($.datepicker._checkExternalClick).find('body').append($.datepicker.dpDiv); $.datepicker.initialized = true; }
            var otherArgs = Array.prototype.slice.call(arguments, 1); if (typeof options == 'string' && (options == 'isDisabled' || options == 'getDate' || options == 'widget'))
                return $.datepicker['_' + options + 'Datepicker'].apply($.datepicker, [this[0]].concat(otherArgs)); if (options == 'option' && arguments.length == 2 && typeof arguments[1] == 'string')
                    return $.datepicker['_' + options + 'Datepicker'].apply($.datepicker, [this[0]].concat(otherArgs)); return this.each(function () { typeof options == 'string' ? $.datepicker['_' + options + 'Datepicker'].apply($.datepicker, [this].concat(otherArgs)) : $.datepicker._attachDatepicker(this, options); });
        }; $.datepicker = new Datepicker(); $.datepicker.initialized = false; $.datepicker.uuid = new Date().getTime(); $.datepicker.version = "1.8.16"; window['DP_jQuery_' + dpuuid] = $;
    })(jQuery); (function ($, undefined) {
        $.widget("ui.progressbar", {
            options: { value: 0, max: 100 }, min: 0, _create: function () { this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({ role: "progressbar", "aria-valuemin": this.min, "aria-valuemax": this.options.max, "aria-valuenow": this._value() }); this.valueDiv = $("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element); this.oldValue = this._value(); this._refreshValue(); }, destroy: function () { this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"); this.valueDiv.remove(); $.Widget.prototype.destroy.apply(this, arguments); }, value: function (newValue) {
                if (newValue === undefined) { return this._value(); }
                this._setOption("value", newValue); return this;
            }, _setOption: function (key, value) {
                if (key === "value") { this.options.value = value; this._refreshValue(); if (this._value() === this.options.max) { this._trigger("complete"); } }
                $.Widget.prototype._setOption.apply(this, arguments);
            }, _value: function () {
                var val = this.options.value; if (typeof val !== "number") { val = 0; }
                return Math.min(this.options.max, Math.max(this.min, val));
            }, _percentage: function () { return 100 * this._value() / this.options.max; }, _refreshValue: function () {
                var value = this.value(); var percentage = this._percentage(); if (this.oldValue !== value) { this.oldValue = value; this._trigger("change"); }
                this.valueDiv.toggle(value > this.min).toggleClass("ui-corner-right", value === this.options.max).width(percentage.toFixed(0) + "%"); this.element.attr("aria-valuenow", value);
            }
        }); $.extend($.ui.progressbar, { version: "1.8.16" });
    })(jQuery);; jQuery.effects || (function ($, undefined) {
        $.effects = {}; $.each(['backgroundColor', 'borderBottomColor', 'borderLeftColor', 'borderRightColor', 'borderTopColor', 'borderColor', 'color', 'outlineColor'], function (i, attr) {
            $.fx.step[attr] = function (fx) {
                if (!fx.colorInit) { fx.start = getColor(fx.elem, attr); fx.end = getRGB(fx.end); fx.colorInit = true; }
                fx.elem.style[attr] = 'rgb(' +
                Math.max(Math.min(parseInt((fx.pos * (fx.end[0] - fx.start[0])) + fx.start[0], 10), 255), 0) + ',' +
                Math.max(Math.min(parseInt((fx.pos * (fx.end[1] - fx.start[1])) + fx.start[1], 10), 255), 0) + ',' +
                Math.max(Math.min(parseInt((fx.pos * (fx.end[2] - fx.start[2])) + fx.start[2], 10), 255), 0) + ')';
            };
        }); function getRGB(color) {
            var result; if (color && color.constructor == Array && color.length == 3)
                return color; if (result = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color))
                    return [parseInt(result[1], 10), parseInt(result[2], 10), parseInt(result[3], 10)]; if (result = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(color))
                        return [parseFloat(result[1]) * 2.55, parseFloat(result[2]) * 2.55, parseFloat(result[3]) * 2.55]; if (result = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(color))
                            return [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)]; if (result = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(color))
                                return [parseInt(result[1] + result[1], 16), parseInt(result[2] + result[2], 16), parseInt(result[3] + result[3], 16)]; if (result = /rgba\(0, 0, 0, 0\)/.exec(color))
                                    return colors['transparent']; return colors[$.trim(color).toLowerCase()];
        }
        function getColor(elem, attr) {
            var color; do {
                color = $.curCSS(elem, attr); if (color != '' && color != 'transparent' || $.nodeName(elem, "body"))
                    break; attr = "backgroundColor";
            } while (elem = elem.parentNode); return getRGB(color);
        }; var colors = { aqua: [0, 255, 255], azure: [240, 255, 255], beige: [245, 245, 220], black: [0, 0, 0], blue: [0, 0, 255], brown: [165, 42, 42], cyan: [0, 255, 255], darkblue: [0, 0, 139], darkcyan: [0, 139, 139], darkgrey: [169, 169, 169], darkgreen: [0, 100, 0], darkkhaki: [189, 183, 107], darkmagenta: [139, 0, 139], darkolivegreen: [85, 107, 47], darkorange: [255, 140, 0], darkorchid: [153, 50, 204], darkred: [139, 0, 0], darksalmon: [233, 150, 122], darkviolet: [148, 0, 211], fuchsia: [255, 0, 255], gold: [255, 215, 0], green: [0, 128, 0], indigo: [75, 0, 130], khaki: [240, 230, 140], lightblue: [173, 216, 230], lightcyan: [224, 255, 255], lightgreen: [144, 238, 144], lightgrey: [211, 211, 211], lightpink: [255, 182, 193], lightyellow: [255, 255, 224], lime: [0, 255, 0], magenta: [255, 0, 255], maroon: [128, 0, 0], navy: [0, 0, 128], olive: [128, 128, 0], orange: [255, 165, 0], pink: [255, 192, 203], purple: [128, 0, 128], violet: [128, 0, 128], red: [255, 0, 0], silver: [192, 192, 192], white: [255, 255, 255], yellow: [255, 255, 0], transparent: [255, 255, 255] }; var classAnimationActions = ['add', 'remove', 'toggle'], shorthandStyles = { border: 1, borderBottom: 1, borderColor: 1, borderLeft: 1, borderRight: 1, borderTop: 1, borderWidth: 1, margin: 1, padding: 1 }; function getElementStyles() {
            var style = document.defaultView ? document.defaultView.getComputedStyle(this, null) : this.currentStyle, newStyle = {}, key, camelCase; if (style && style.length && style[0] && style[style[0]]) { var len = style.length; while (len--) { key = style[len]; if (typeof style[key] == 'string') { camelCase = key.replace(/\-(\w)/g, function (all, letter) { return letter.toUpperCase(); }); newStyle[camelCase] = style[key]; } } } else { for (key in style) { if (typeof style[key] === 'string') { newStyle[key] = style[key]; } } }
            return newStyle;
        }
        function filterStyles(styles) {
            var name, value; for (name in styles) { value = styles[name]; if (value == null || $.isFunction(value) || name in shorthandStyles || (/scrollbar/).test(name) || (!(/color/i).test(name) && isNaN(parseFloat(value)))) { delete styles[name]; } }
            return styles;
        }
        function styleDifference(oldStyle, newStyle) {
            var diff = { _: 0 }, name; for (name in newStyle) { if (oldStyle[name] != newStyle[name]) { diff[name] = newStyle[name]; } }
            return diff;
        }
        $.effects.animateClass = function (value, duration, easing, callback) {
            if ($.isFunction(easing)) { callback = easing; easing = null; }
            return this.queue(function () {
                var that = $(this), originalStyleAttr = that.attr('style') || ' ', originalStyle = filterStyles(getElementStyles.call(this)), newStyle, className = that.attr('class'); $.each(classAnimationActions, function (i, action) { if (value[action]) { that[action + 'Class'](value[action]); } }); newStyle = filterStyles(getElementStyles.call(this)); that.attr('class', className); that.animate(styleDifference(originalStyle, newStyle), {
                    queue: false, duration: duration, easing: easing, complete: function () {
                        $.each(classAnimationActions, function (i, action) { if (value[action]) { that[action + 'Class'](value[action]); } }); if (typeof that.attr('style') == 'object') { that.attr('style').cssText = ''; that.attr('style').cssText = originalStyleAttr; } else { that.attr('style', originalStyleAttr); }
                        if (callback) { callback.apply(this, arguments); }
                        $.dequeue(this);
                    }
                });
            });
        }; $.fn.extend({ _addClass: $.fn.addClass, addClass: function (classNames, speed, easing, callback) { return speed ? $.effects.animateClass.apply(this, [{ add: classNames }, speed, easing, callback]) : this._addClass(classNames); }, _removeClass: $.fn.removeClass, removeClass: function (classNames, speed, easing, callback) { return speed ? $.effects.animateClass.apply(this, [{ remove: classNames }, speed, easing, callback]) : this._removeClass(classNames); }, _toggleClass: $.fn.toggleClass, toggleClass: function (classNames, force, speed, easing, callback) { if (typeof force == "boolean" || force === undefined) { if (!speed) { return this._toggleClass(classNames, force); } else { return $.effects.animateClass.apply(this, [(force ? { add: classNames } : { remove: classNames }), speed, easing, callback]); } } else { return $.effects.animateClass.apply(this, [{ toggle: classNames }, force, speed, easing]); } }, switchClass: function (remove, add, speed, easing, callback) { return $.effects.animateClass.apply(this, [{ add: add, remove: remove }, speed, easing, callback]); } }); $.extend($.effects, {
            version: "1.8.16", save: function (element, set) { for (var i = 0; i < set.length; i++) { if (set[i] !== null) element.data("ec.storage." + set[i], element[0].style[set[i]]); } }, restore: function (element, set) { for (var i = 0; i < set.length; i++) { if (set[i] !== null) element.css(set[i], element.data("ec.storage." + set[i])); } }, setMode: function (el, mode) { if (mode == 'toggle') mode = el.is(':hidden') ? 'show' : 'hide'; return mode; }, getBaseline: function (origin, original) { var y, x; switch (origin[0]) { case 'top': y = 0; break; case 'middle': y = 0.5; break; case 'bottom': y = 1; break; default: y = origin[0] / original.height; }; switch (origin[1]) { case 'left': x = 0; break; case 'center': x = 0.5; break; case 'right': x = 1; break; default: x = origin[1] / original.width; }; return { x: x, y: y }; }, createWrapper: function (element) {
                if (element.parent().is('.ui-effects-wrapper')) { return element.parent(); }
                var props = { width: element.outerWidth(true), height: element.outerHeight(true), 'float': element.css('float') }, wrapper = $('<div></div>').addClass('ui-effects-wrapper').css({ fontSize: '100%', background: 'transparent', border: 'none', margin: 0, padding: 0 }), active = document.activeElement; element.wrap(wrapper); if (element[0] === active || $.contains(element[0], active)) { $(active).focus(); }
                wrapper = element.parent(); if (element.css('position') == 'static') { wrapper.css({ position: 'relative' }); element.css({ position: 'relative' }); } else { $.extend(props, { position: element.css('position'), zIndex: element.css('z-index') }); $.each(['top', 'left', 'bottom', 'right'], function (i, pos) { props[pos] = element.css(pos); if (isNaN(parseInt(props[pos], 10))) { props[pos] = 'auto'; } }); element.css({ position: 'relative', top: 0, left: 0, right: 'auto', bottom: 'auto' }); }
                return wrapper.css(props).show();
            }, removeWrapper: function (element) {
                var parent, active = document.activeElement; if (element.parent().is('.ui-effects-wrapper')) {
                    parent = element.parent().replaceWith(element); if (element[0] === active || $.contains(element[0], active)) { $(active).focus(); }
                    return parent;
                }
                return element;
            }, setTransition: function (element, list, factor, value) { value = value || {}; $.each(list, function (i, x) { unit = element.cssUnit(x); if (unit[0] > 0) value[x] = unit[0] * factor + unit[1]; }); return value; }
        }); function _normalizeArguments(effect, options, speed, callback) {
            if (typeof effect == 'object') { callback = options; speed = null; options = effect; effect = options.effect; }
            if ($.isFunction(options)) { callback = options; speed = null; options = {}; }
            if (typeof options == 'number' || $.fx.speeds[options]) { callback = speed; speed = options; options = {}; }
            if ($.isFunction(speed)) { callback = speed; speed = null; }
            options = options || {}; speed = speed || options.duration; speed = $.fx.off ? 0 : typeof speed == 'number' ? speed : speed in $.fx.speeds ? $.fx.speeds[speed] : $.fx.speeds._default; callback = callback || options.complete; return [effect, options, speed, callback];
        }
        function standardSpeed(speed) {
            if (!speed || typeof speed === "number" || $.fx.speeds[speed]) { return true; }
            if (typeof speed === "string" && !$.effects[speed]) { return true; }
            return false;
        }
        $.fn.extend({
            effect: function (effect, options, speed, callback) {
                var args = _normalizeArguments.apply(this, arguments), args2 = { options: args[1], duration: args[2], callback: args[3] }, mode = args2.options.mode, effectMethod = $.effects[effect]; if ($.fx.off || !effectMethod) { if (mode) { return this[mode](args2.duration, args2.callback); } else { return this.each(function () { if (args2.callback) { args2.callback.call(this); } }); } }
                return effectMethod.call(this, args2);
            }, _show: $.fn.show, show: function (speed) { if (standardSpeed(speed)) { return this._show.apply(this, arguments); } else { var args = _normalizeArguments.apply(this, arguments); args[1].mode = 'show'; return this.effect.apply(this, args); } }, _hide: $.fn.hide, hide: function (speed) { if (standardSpeed(speed)) { return this._hide.apply(this, arguments); } else { var args = _normalizeArguments.apply(this, arguments); args[1].mode = 'hide'; return this.effect.apply(this, args); } }, __toggle: $.fn.toggle, toggle: function (speed) { if (standardSpeed(speed) || typeof speed === "boolean" || $.isFunction(speed)) { return this.__toggle.apply(this, arguments); } else { var args = _normalizeArguments.apply(this, arguments); args[1].mode = 'toggle'; return this.effect.apply(this, args); } }, cssUnit: function (key) {
                var style = this.css(key), val = []; $.each(['em', 'px', '%', 'pt'], function (i, unit) {
                    if (style.indexOf(unit) > 0)
                        val = [parseFloat(style), unit];
                }); return val;
            }
        }); $.easing.jswing = $.easing.swing; $.extend($.easing, {
            def: 'easeOutQuad', swing: function (x, t, b, c, d) { return $.easing[$.easing.def](x, t, b, c, d); }, easeInQuad: function (x, t, b, c, d) { return c * (t /= d) * t + b; }, easeOutQuad: function (x, t, b, c, d) { return -c * (t /= d) * (t - 2) + b; }, easeInOutQuad: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t + b; return -c / 2 * ((--t) * (t - 2) - 1) + b; }, easeInCubic: function (x, t, b, c, d) { return c * (t /= d) * t * t + b; }, easeOutCubic: function (x, t, b, c, d) { return c * ((t = t / d - 1) * t * t + 1) + b; }, easeInOutCubic: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t + b; return c / 2 * ((t -= 2) * t * t + 2) + b; }, easeInQuart: function (x, t, b, c, d) { return c * (t /= d) * t * t * t + b; }, easeOutQuart: function (x, t, b, c, d) { return -c * ((t = t / d - 1) * t * t * t - 1) + b; }, easeInOutQuart: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b; return -c / 2 * ((t -= 2) * t * t * t - 2) + b; }, easeInQuint: function (x, t, b, c, d) { return c * (t /= d) * t * t * t * t + b; }, easeOutQuint: function (x, t, b, c, d) { return c * ((t = t / d - 1) * t * t * t * t + 1) + b; }, easeInOutQuint: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b; return c / 2 * ((t -= 2) * t * t * t * t + 2) + b; }, easeInSine: function (x, t, b, c, d) { return -c * Math.cos(t / d * (Math.PI / 2)) + c + b; }, easeOutSine: function (x, t, b, c, d) { return c * Math.sin(t / d * (Math.PI / 2)) + b; }, easeInOutSine: function (x, t, b, c, d) { return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b; }, easeInExpo: function (x, t, b, c, d) { return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b; }, easeOutExpo: function (x, t, b, c, d) { return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b; }, easeInOutExpo: function (x, t, b, c, d) { if (t == 0) return b; if (t == d) return b + c; if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b; return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b; }, easeInCirc: function (x, t, b, c, d) { return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b; }, easeOutCirc: function (x, t, b, c, d) { return c * Math.sqrt(1 - (t = t / d - 1) * t) + b; }, easeInOutCirc: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b; return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b; }, easeInElastic: function (x, t, b, c, d) {
                var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3; if (a < Math.abs(c)) { a = c; var s = p / 4; }
                else var s = p / (2 * Math.PI) * Math.asin(c / a); return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
            }, easeOutElastic: function (x, t, b, c, d) {
                var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3; if (a < Math.abs(c)) { a = c; var s = p / 4; }
                else var s = p / (2 * Math.PI) * Math.asin(c / a); return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
            }, easeInOutElastic: function (x, t, b, c, d) {
                var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d / 2) == 2) return b + c; if (!p) p = d * (.3 * 1.5); if (a < Math.abs(c)) { a = c; var s = p / 4; }
                else var s = p / (2 * Math.PI) * Math.asin(c / a); if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b; return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
            }, easeInBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; return c * (t /= d) * t * ((s + 1) * t - s) + b; }, easeOutBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b; }, easeInOutBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b; return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b; }, easeInBounce: function (x, t, b, c, d) { return c - $.easing.easeOutBounce(x, d - t, 0, c, d) + b; }, easeOutBounce: function (x, t, b, c, d) { if ((t /= d) < (1 / 2.75)) { return c * (7.5625 * t * t) + b; } else if (t < (2 / 2.75)) { return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b; } else if (t < (2.5 / 2.75)) { return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b; } else { return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b; } }, easeInOutBounce: function (x, t, b, c, d) { if (t < d / 2) return $.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b; return $.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b; }
        });
    })(jQuery); (function ($, undefined) { $.effects.blind = function (o) { return this.queue(function () { var el = $(this), props = ['position', 'top', 'bottom', 'left', 'right']; var mode = $.effects.setMode(el, o.options.mode || 'hide'); var direction = o.options.direction || 'vertical'; $.effects.save(el, props); el.show(); var wrapper = $.effects.createWrapper(el).css({ overflow: 'hidden' }); var ref = (direction == 'vertical') ? 'height' : 'width'; var distance = (direction == 'vertical') ? wrapper.height() : wrapper.width(); if (mode == 'show') wrapper.css(ref, 0); var animation = {}; animation[ref] = mode == 'show' ? distance : 0; wrapper.animate(animation, o.duration, o.options.easing, function () { if (mode == 'hide') el.hide(); $.effects.restore(el, props); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(el[0], arguments); el.dequeue(); }); }); }; })(jQuery); (function ($, undefined) { $.effects.bounce = function (o) { return this.queue(function () { var el = $(this), props = ['position', 'top', 'bottom', 'left', 'right']; var mode = $.effects.setMode(el, o.options.mode || 'effect'); var direction = o.options.direction || 'up'; var distance = o.options.distance || 20; var times = o.options.times || 5; var speed = o.duration || 250; if (/show|hide/.test(mode)) props.push('opacity'); $.effects.save(el, props); el.show(); $.effects.createWrapper(el); var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left'; var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg'; var distance = o.options.distance || (ref == 'top' ? el.outerHeight({ margin: true }) / 3 : el.outerWidth({ margin: true }) / 3); if (mode == 'show') el.css('opacity', 0).css(ref, motion == 'pos' ? -distance : distance); if (mode == 'hide') distance = distance / (times * 2); if (mode != 'hide') times--; if (mode == 'show') { var animation = { opacity: 1 }; animation[ref] = (motion == 'pos' ? '+=' : '-=') + distance; el.animate(animation, speed / 2, o.options.easing); distance = distance / 2; times--; }; for (var i = 0; i < times; i++) { var animation1 = {}, animation2 = {}; animation1[ref] = (motion == 'pos' ? '-=' : '+=') + distance; animation2[ref] = (motion == 'pos' ? '+=' : '-=') + distance; el.animate(animation1, speed / 2, o.options.easing).animate(animation2, speed / 2, o.options.easing); distance = (mode == 'hide') ? distance * 2 : distance / 2; }; if (mode == 'hide') { var animation = { opacity: 0 }; animation[ref] = (motion == 'pos' ? '-=' : '+=') + distance; el.animate(animation, speed / 2, o.options.easing, function () { el.hide(); $.effects.restore(el, props); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(this, arguments); }); } else { var animation1 = {}, animation2 = {}; animation1[ref] = (motion == 'pos' ? '-=' : '+=') + distance; animation2[ref] = (motion == 'pos' ? '+=' : '-=') + distance; el.animate(animation1, speed / 2, o.options.easing).animate(animation2, speed / 2, o.options.easing, function () { $.effects.restore(el, props); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(this, arguments); }); }; el.queue('fx', function () { el.dequeue(); }); el.dequeue(); }); }; })(jQuery); (function ($, undefined) {
        $.effects.clip = function (o) {
            return this.queue(function () {
                var el = $(this), props = ['position', 'top', 'bottom', 'left', 'right', 'height', 'width']; var mode = $.effects.setMode(el, o.options.mode || 'hide'); var direction = o.options.direction || 'vertical'; $.effects.save(el, props); el.show(); var wrapper = $.effects.createWrapper(el).css({ overflow: 'hidden' }); var animate = el[0].tagName == 'IMG' ? wrapper : el; var ref = { size: (direction == 'vertical') ? 'height' : 'width', position: (direction == 'vertical') ? 'top' : 'left' }; var distance = (direction == 'vertical') ? animate.height() : animate.width(); if (mode == 'show') { animate.css(ref.size, 0); animate.css(ref.position, distance / 2); }
                var animation = {}; animation[ref.size] = mode == 'show' ? distance : 0; animation[ref.position] = mode == 'show' ? 0 : distance / 2; animate.animate(animation, { queue: false, duration: o.duration, easing: o.options.easing, complete: function () { if (mode == 'hide') el.hide(); $.effects.restore(el, props); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(el[0], arguments); el.dequeue(); } });
            });
        };
    })(jQuery); (function ($, undefined) { $.effects.drop = function (o) { return this.queue(function () { var el = $(this), props = ['position', 'top', 'bottom', 'left', 'right', 'opacity']; var mode = $.effects.setMode(el, o.options.mode || 'hide'); var direction = o.options.direction || 'left'; $.effects.save(el, props); el.show(); $.effects.createWrapper(el); var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left'; var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg'; var distance = o.options.distance || (ref == 'top' ? el.outerHeight({ margin: true }) / 2 : el.outerWidth({ margin: true }) / 2); if (mode == 'show') el.css('opacity', 0).css(ref, motion == 'pos' ? -distance : distance); var animation = { opacity: mode == 'show' ? 1 : 0 }; animation[ref] = (mode == 'show' ? (motion == 'pos' ? '+=' : '-=') : (motion == 'pos' ? '-=' : '+=')) + distance; el.animate(animation, { queue: false, duration: o.duration, easing: o.options.easing, complete: function () { if (mode == 'hide') el.hide(); $.effects.restore(el, props); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(this, arguments); el.dequeue(); } }); }); }; })(jQuery); (function ($, undefined) {
        $.effects.explode = function (o) {
            return this.queue(function () {
                var rows = o.options.pieces ? Math.round(Math.sqrt(o.options.pieces)) : 3; var cells = o.options.pieces ? Math.round(Math.sqrt(o.options.pieces)) : 3; o.options.mode = o.options.mode == 'toggle' ? ($(this).is(':visible') ? 'hide' : 'show') : o.options.mode; var el = $(this).show().css('visibility', 'hidden'); var offset = el.offset(); offset.top -= parseInt(el.css("marginTop"), 10) || 0; offset.left -= parseInt(el.css("marginLeft"), 10) || 0; var width = el.outerWidth(true); var height = el.outerHeight(true); for (var i = 0; i < rows; i++) { for (var j = 0; j < cells; j++) { el.clone().appendTo('body').wrap('<div></div>').css({ position: 'absolute', visibility: 'visible', left: -j * (width / cells), top: -i * (height / rows) }).parent().addClass('ui-effects-explode').css({ position: 'absolute', overflow: 'hidden', width: width / cells, height: height / rows, left: offset.left + j * (width / cells) + (o.options.mode == 'show' ? (j - Math.floor(cells / 2)) * (width / cells) : 0), top: offset.top + i * (height / rows) + (o.options.mode == 'show' ? (i - Math.floor(rows / 2)) * (height / rows) : 0), opacity: o.options.mode == 'show' ? 0 : 1 }).animate({ left: offset.left + j * (width / cells) + (o.options.mode == 'show' ? 0 : (j - Math.floor(cells / 2)) * (width / cells)), top: offset.top + i * (height / rows) + (o.options.mode == 'show' ? 0 : (i - Math.floor(rows / 2)) * (height / rows)), opacity: o.options.mode == 'show' ? 1 : 0 }, o.duration || 500); } }
                setTimeout(function () { o.options.mode == 'show' ? el.css({ visibility: 'visible' }) : el.css({ visibility: 'visible' }).hide(); if (o.callback) o.callback.apply(el[0]); el.dequeue(); $('div.ui-effects-explode').remove(); }, o.duration || 500);
            });
        };
    })(jQuery); (function ($, undefined) { $.effects.fade = function (o) { return this.queue(function () { var elem = $(this), mode = $.effects.setMode(elem, o.options.mode || 'hide'); elem.animate({ opacity: mode }, { queue: false, duration: o.duration, easing: o.options.easing, complete: function () { (o.callback && o.callback.apply(this, arguments)); elem.dequeue(); } }); }); }; })(jQuery); (function ($, undefined) { $.effects.fold = function (o) { return this.queue(function () { var el = $(this), props = ['position', 'top', 'bottom', 'left', 'right']; var mode = $.effects.setMode(el, o.options.mode || 'hide'); var size = o.options.size || 15; var horizFirst = !(!o.options.horizFirst); var duration = o.duration ? o.duration / 2 : $.fx.speeds._default / 2; $.effects.save(el, props); el.show(); var wrapper = $.effects.createWrapper(el).css({ overflow: 'hidden' }); var widthFirst = ((mode == 'show') != horizFirst); var ref = widthFirst ? ['width', 'height'] : ['height', 'width']; var distance = widthFirst ? [wrapper.width(), wrapper.height()] : [wrapper.height(), wrapper.width()]; var percent = /([0-9]+)%/.exec(size); if (percent) size = parseInt(percent[1], 10) / 100 * distance[mode == 'hide' ? 0 : 1]; if (mode == 'show') wrapper.css(horizFirst ? { height: 0, width: size } : { height: size, width: 0 }); var animation1 = {}, animation2 = {}; animation1[ref[0]] = mode == 'show' ? distance[0] : size; animation2[ref[1]] = mode == 'show' ? distance[1] : 0; wrapper.animate(animation1, duration, o.options.easing).animate(animation2, duration, o.options.easing, function () { if (mode == 'hide') el.hide(); $.effects.restore(el, props); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(el[0], arguments); el.dequeue(); }); }); }; })(jQuery); (function ($, undefined) {
        $.effects.highlight = function (o) {
            return this.queue(function () {
                var elem = $(this), props = ['backgroundImage', 'backgroundColor', 'opacity'], mode = $.effects.setMode(elem, o.options.mode || 'show'), animation = { backgroundColor: elem.css('backgroundColor') }; if (mode == 'hide') { animation.opacity = 0; }
                $.effects.save(elem, props); elem.show().css({ backgroundImage: 'none', backgroundColor: o.options.color || '#ffff99' }).animate(animation, { queue: false, duration: o.duration, easing: o.options.easing, complete: function () { (mode == 'hide' && elem.hide()); $.effects.restore(elem, props); (mode == 'show' && !$.support.opacity && this.style.removeAttribute('filter')); (o.callback && o.callback.apply(this, arguments)); elem.dequeue(); } });
            });
        };
    })(jQuery); (function ($, undefined) {
        $.effects.pulsate = function (o) {
            return this.queue(function () {
                var elem = $(this), mode = $.effects.setMode(elem, o.options.mode || 'show'); times = ((o.options.times || 5) * 2) - 1; duration = o.duration ? o.duration / 2 : $.fx.speeds._default / 2, isVisible = elem.is(':visible'), animateTo = 0; if (!isVisible) { elem.css('opacity', 0).show(); animateTo = 1; }
                if ((mode == 'hide' && isVisible) || (mode == 'show' && !isVisible)) { times--; }
                for (var i = 0; i < times; i++) { elem.animate({ opacity: animateTo }, duration, o.options.easing); animateTo = (animateTo + 1) % 2; }
                elem.animate({ opacity: animateTo }, duration, o.options.easing, function () {
                    if (animateTo == 0) { elem.hide(); }
                    (o.callback && o.callback.apply(this, arguments));
                }); elem.queue('fx', function () { elem.dequeue(); }).dequeue();
            });
        };
    })(jQuery); (function ($, undefined) {
        $.effects.puff = function (o) { return this.queue(function () { var elem = $(this), mode = $.effects.setMode(elem, o.options.mode || 'hide'), percent = parseInt(o.options.percent, 10) || 150, factor = percent / 100, original = { height: elem.height(), width: elem.width() }; $.extend(o.options, { fade: true, mode: mode, percent: mode == 'hide' ? percent : 100, from: mode == 'hide' ? original : { height: original.height * factor, width: original.width * factor } }); elem.effect('scale', o.options, o.duration, o.callback); elem.dequeue(); }); }; $.effects.scale = function (o) {
            return this.queue(function () {
                var el = $(this); var options = $.extend(true, {}, o.options); var mode = $.effects.setMode(el, o.options.mode || 'effect'); var percent = parseInt(o.options.percent, 10) || (parseInt(o.options.percent, 10) == 0 ? 0 : (mode == 'hide' ? 0 : 100)); var direction = o.options.direction || 'both'; var origin = o.options.origin; if (mode != 'effect') { options.origin = origin || ['middle', 'center']; options.restore = true; }
                var original = { height: el.height(), width: el.width() }; el.from = o.options.from || (mode == 'show' ? { height: 0, width: 0 } : original); var factor = { y: direction != 'horizontal' ? (percent / 100) : 1, x: direction != 'vertical' ? (percent / 100) : 1 }; el.to = { height: original.height * factor.y, width: original.width * factor.x }; if (o.options.fade) { if (mode == 'show') { el.from.opacity = 0; el.to.opacity = 1; }; if (mode == 'hide') { el.from.opacity = 1; el.to.opacity = 0; }; }; options.from = el.from; options.to = el.to; options.mode = mode; el.effect('size', options, o.duration, o.callback); el.dequeue();
            });
        }; $.effects.size = function (o) {
            return this.queue(function () {
                var el = $(this), props = ['position', 'top', 'bottom', 'left', 'right', 'width', 'height', 'overflow', 'opacity']; var props1 = ['position', 'top', 'bottom', 'left', 'right', 'overflow', 'opacity']; var props2 = ['width', 'height', 'overflow']; var cProps = ['fontSize']; var vProps = ['borderTopWidth', 'borderBottomWidth', 'paddingTop', 'paddingBottom']; var hProps = ['borderLeftWidth', 'borderRightWidth', 'paddingLeft', 'paddingRight']; var mode = $.effects.setMode(el, o.options.mode || 'effect'); var restore = o.options.restore || false; var scale = o.options.scale || 'both'; var origin = o.options.origin; var original = { height: el.height(), width: el.width() }; el.from = o.options.from || original; el.to = o.options.to || original; if (origin) { var baseline = $.effects.getBaseline(origin, original); el.from.top = (original.height - el.from.height) * baseline.y; el.from.left = (original.width - el.from.width) * baseline.x; el.to.top = (original.height - el.to.height) * baseline.y; el.to.left = (original.width - el.to.width) * baseline.x; }; var factor = { from: { y: el.from.height / original.height, x: el.from.width / original.width }, to: { y: el.to.height / original.height, x: el.to.width / original.width } }; if (scale == 'box' || scale == 'both') { if (factor.from.y != factor.to.y) { props = props.concat(vProps); el.from = $.effects.setTransition(el, vProps, factor.from.y, el.from); el.to = $.effects.setTransition(el, vProps, factor.to.y, el.to); }; if (factor.from.x != factor.to.x) { props = props.concat(hProps); el.from = $.effects.setTransition(el, hProps, factor.from.x, el.from); el.to = $.effects.setTransition(el, hProps, factor.to.x, el.to); }; }; if (scale == 'content' || scale == 'both') { if (factor.from.y != factor.to.y) { props = props.concat(cProps); el.from = $.effects.setTransition(el, cProps, factor.from.y, el.from); el.to = $.effects.setTransition(el, cProps, factor.to.y, el.to); }; }; $.effects.save(el, restore ? props : props1); el.show(); $.effects.createWrapper(el); el.css('overflow', 'hidden').css(el.from); if (scale == 'content' || scale == 'both') { vProps = vProps.concat(['marginTop', 'marginBottom']).concat(cProps); hProps = hProps.concat(['marginLeft', 'marginRight']); props2 = props.concat(vProps).concat(hProps); el.find("*[width]").each(function () { child = $(this); if (restore) $.effects.save(child, props2); var c_original = { height: child.height(), width: child.width() }; child.from = { height: c_original.height * factor.from.y, width: c_original.width * factor.from.x }; child.to = { height: c_original.height * factor.to.y, width: c_original.width * factor.to.x }; if (factor.from.y != factor.to.y) { child.from = $.effects.setTransition(child, vProps, factor.from.y, child.from); child.to = $.effects.setTransition(child, vProps, factor.to.y, child.to); }; if (factor.from.x != factor.to.x) { child.from = $.effects.setTransition(child, hProps, factor.from.x, child.from); child.to = $.effects.setTransition(child, hProps, factor.to.x, child.to); }; child.css(child.from); child.animate(child.to, o.duration, o.options.easing, function () { if (restore) $.effects.restore(child, props2); }); }); }; el.animate(el.to, {
                    queue: false, duration: o.duration, easing: o.options.easing, complete: function () {
                        if (el.to.opacity === 0) { el.css('opacity', el.from.opacity); }
                        if (mode == 'hide') el.hide(); $.effects.restore(el, restore ? props : props1); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(this, arguments); el.dequeue();
                    }
                });
            });
        };
    })(jQuery); (function ($, undefined) { $.effects.shake = function (o) { return this.queue(function () { var el = $(this), props = ['position', 'top', 'bottom', 'left', 'right']; var mode = $.effects.setMode(el, o.options.mode || 'effect'); var direction = o.options.direction || 'left'; var distance = o.options.distance || 20; var times = o.options.times || 3; var speed = o.duration || o.options.duration || 140; $.effects.save(el, props); el.show(); $.effects.createWrapper(el); var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left'; var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg'; var animation = {}, animation1 = {}, animation2 = {}; animation[ref] = (motion == 'pos' ? '-=' : '+=') + distance; animation1[ref] = (motion == 'pos' ? '+=' : '-=') + distance * 2; animation2[ref] = (motion == 'pos' ? '-=' : '+=') + distance * 2; el.animate(animation, speed, o.options.easing); for (var i = 1; i < times; i++) { el.animate(animation1, speed, o.options.easing).animate(animation2, speed, o.options.easing); }; el.animate(animation1, speed, o.options.easing).animate(animation, speed / 2, o.options.easing, function () { $.effects.restore(el, props); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(this, arguments); }); el.queue('fx', function () { el.dequeue(); }); el.dequeue(); }); }; })(jQuery); (function ($, undefined) { $.effects.slide = function (o) { return this.queue(function () { var el = $(this), props = ['position', 'top', 'bottom', 'left', 'right']; var mode = $.effects.setMode(el, o.options.mode || 'show'); var direction = o.options.direction || 'left'; $.effects.save(el, props); el.show(); $.effects.createWrapper(el).css({ overflow: 'hidden' }); var ref = (direction == 'up' || direction == 'down') ? 'top' : 'left'; var motion = (direction == 'up' || direction == 'left') ? 'pos' : 'neg'; var distance = o.options.distance || (ref == 'top' ? el.outerHeight({ margin: true }) : el.outerWidth({ margin: true })); if (mode == 'show') el.css(ref, motion == 'pos' ? (isNaN(distance) ? "-" + distance : -distance) : distance); var animation = {}; animation[ref] = (mode == 'show' ? (motion == 'pos' ? '+=' : '-=') : (motion == 'pos' ? '-=' : '+=')) + distance; el.animate(animation, { queue: false, duration: o.duration, easing: o.options.easing, complete: function () { if (mode == 'hide') el.hide(); $.effects.restore(el, props); $.effects.removeWrapper(el); if (o.callback) o.callback.apply(this, arguments); el.dequeue(); } }); }); }; })(jQuery); (function ($, undefined) { $.effects.transfer = function (o) { return this.queue(function () { var elem = $(this), target = $(o.options.to), endPosition = target.offset(), animation = { top: endPosition.top, left: endPosition.left, height: target.innerHeight(), width: target.innerWidth() }, startPosition = elem.offset(), transfer = $('<div class="ui-effects-transfer"></div>').appendTo(document.body).addClass(o.options.className).css({ top: startPosition.top, left: startPosition.left, height: elem.innerHeight(), width: elem.innerWidth(), position: 'absolute' }).animate(animation, o.duration, o.options.easing, function () { transfer.remove(); (o.callback && o.callback.apply(elem[0], arguments)); elem.dequeue(); }); }); }; })(jQuery); (function ($) {
        var uid = 'ar' + Number(new Date), defaults = autoResize.defaults = { onResize: function () { }, onBeforeResize: function () { return 123 }, onAfterResize: function () { return 555 }, animate: { duration: 200, complete: function () { } }, extraSpace: 50, minHeight: 'original', maxHeight: 500, minWidth: 'original', maxWidth: 500 }; autoResize.cloneCSSProperties = ['lineHeight', 'textDecoration', 'letterSpacing', 'fontSize', 'fontFamily', 'fontStyle', 'fontWeight', 'textTransform', 'textAlign', 'direction', 'wordSpacing', 'fontSizeAdjust', 'paddingTop', 'paddingLeft', 'paddingBottom', 'paddingRight', 'width']; autoResize.cloneCSSValues = { position: 'absolute', top: -9999, left: -9999, opacity: 0, overflow: 'hidden' }; autoResize.resizableFilterSelector = ['textarea:not(textarea.' + uid + ')', 'input:not(input[type])', 'input[type=text]', 'input[type=password]', 'input[type=email]', 'input[type=url]'].join(','); autoResize.AutoResizer = AutoResizer; $.fn.autoResize = autoResize; function autoResize(config) { this.filter(autoResize.resizableFilterSelector).each(function () { new AutoResizer($(this), config); }); return this; }
        function AutoResizer(el, config) {
            if (el.data('AutoResizer')) { el.data('AutoResizer').destroy(); }
            config = this.config = $.extend(true, {}, autoResize.defaults, config); this.el = el; this.nodeName = el[0].nodeName.toLowerCase(); this.originalHeight = el.height(); this.previousScrollTop = null; this.value = el.val(); if (config.maxWidth === 'original') config.maxWidth = el.width(); if (config.minWidth === 'original') config.minWidth = el.width(); if (config.maxHeight === 'original') config.maxHeight = el.height(); if (config.minHeight === 'original') config.minHeight = el.height(); if (this.nodeName === 'textarea') { el.css({ resize: 'none', overflowY: 'hidden' }); }
            el.data('AutoResizer', this); config.animate.complete = (function (f) { return function () { config.onAfterResize.call(el); return f.apply(this, arguments); }; }(config.animate.complete)); this.bind();
        }
        AutoResizer.prototype = {
            bind: function () {
                var check = $.proxy(function () { this.check(); return true; }, this); this.unbind(); this.el.bind('keyup.autoResize', check)
                .bind('change.autoResize', check).bind('paste.autoResize', function () { setTimeout(function () { check(); }, 0); }); if (!this.el.is(':hidden')) { this.check(null, true); }
            }, unbind: function () { this.el.unbind('.autoResize'); }, createClone: function () { var el = this.el, clone = this.nodeName === 'textarea' ? el.clone() : $('<span/>'); this.clone = clone; $.each(autoResize.cloneCSSProperties, function (i, p) { clone[0].style[p] = el.css(p); }); clone.removeAttr('name').removeAttr('id').addClass(uid).attr('tabIndex', -1).css(autoResize.cloneCSSValues); if (this.nodeName === 'textarea') { clone.height('auto'); } else { clone.width('auto').css({ whiteSpace: 'nowrap' }); } }, check: function (e, immediate) {
                if (!this.clone) { this.createClone(); this.injectClone(); }
                var config = this.config, clone = this.clone, el = this.el, value = el.val(); if (value === this.prevValue) { return true; }
                this.prevValue = value; if (this.nodeName === 'input') {
                    clone.text(value); var cloneWidth = clone.width(), newWidth = (cloneWidth + config.extraSpace) >= config.minWidth ? cloneWidth + config.extraSpace : config.minWidth, currentWidth = el.width(); newWidth = Math.min(newWidth, config.maxWidth); if ((newWidth < currentWidth && newWidth >= config.minWidth) || (newWidth >= config.minWidth && newWidth <= config.maxWidth)) { config.onBeforeResize.call(el); config.onResize.call(el); el.scrollLeft(0); if (config.animate && !immediate) { el.stop(1, 1).animate({ width: newWidth }, config.animate); } else { el.width(newWidth); config.onAfterResize.call(el); } }
                    return;
                }
                clone.width(el.width()).height(0).val(value).scrollTop(10000); var scrollTop = clone[0].scrollTop; if (this.previousScrollTop === scrollTop) { return; }
                this.previousScrollTop = scrollTop; if (scrollTop + config.extraSpace >= config.maxHeight) { el.css('overflowY', ''); scrollTop = config.maxHeight; immediate = true; } else if (scrollTop <= config.minHeight) { scrollTop = config.minHeight; } else { el.css('overflowY', 'hidden'); scrollTop += config.extraSpace; }
                config.onBeforeResize.call(el); config.onResize.call(el); if (config.animate && !immediate) { el.stop(1, 1).animate({ height: scrollTop }, config.animate); } else { el.height(scrollTop); config.onAfterResize.call(el); }
            }, destroy: function () { this.unbind(); this.el.removeData('AutoResizer'); this.clone.remove(); delete this.el; delete this.clone; }, injectClone: function () { (autoResize.cloneContainer || (autoResize.cloneContainer = $('<arclones/>').appendTo('body'))).append(this.clone); }
        };
    })(jQuery); (function ($, undefined) {
        var
        hidden = 'hidden', copy = '<textarea style="position:absolute; top:-9999px; left:-9999px; right:auto; bottom:auto; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden">', copyStyle = ['fontFamily', 'fontSize', 'fontWeight', 'fontStyle', 'letterSpacing', 'textTransform', 'wordSpacing'], oninput = 'oninput', onpropertychange = 'onpropertychange', test = $(copy)[0]; test.setAttribute(oninput, "return"); if ($.isFunction(test[oninput]) || onpropertychange in test) {
            $.fn.autosize = function (className) {
                return this.each(function () {
                    var
                    ta = this, $ta = $(ta).css({ overflow: hidden, overflowY: hidden, wordWrap: 'break-word' }), mirror = $(copy).addClass(className || 'autosizejs')[0], minHeight = $ta.height(), maxHeight = parseInt($ta.css('maxHeight'), 10), active, i = copyStyle.length, boxOffset = $ta.css('box-sizing') === 'border-box' ? $ta.outerHeight() - $ta.height() : 0
                    maxHeight = maxHeight && maxHeight > 0 ? maxHeight : 9e4; function adjust() {
                        var height, overflow; if (!active) {
                            active = true; mirror.value = ta.value; mirror.style.overflowY = ta.style.overflowY; mirror.style.width = $ta.css('width'); mirror.scrollTop = 0; mirror.scrollTop = 9e4; height = mirror.scrollTop; overflow = hidden; if (height > maxHeight) { height = maxHeight; overflow = 'scroll'; } else if (height < minHeight) { height = minHeight; }
                            ta.style.overflowY = overflow; ta.style.height = ta.style.minHeight = ta.style.maxHeight = height + boxOffset + 'px'; setTimeout(function () { active = false; }, 1);
                        }
                    }
                    while (i--) { mirror.style[copyStyle[i]] = $ta.css(copyStyle[i]); }
                    $('body').append(mirror); if (onpropertychange in ta) { if (oninput in ta) { ta[oninput] = ta.onkeyup = adjust; } else { ta[onpropertychange] = adjust; } } else { ta[oninput] = adjust; }
                    $(window).resize(adjust); $ta.bind('autosize', adjust); adjust();
                });
            };
        } else { $.fn.autosize = function () { return this; }; }
    }(jQuery)); jQuery.fn.autoGrow = function () {
        return this.each(function () {
            var colsDefault = this.cols; var rowsDefault = this.rows; var grow = function () { growByRef(this); }
            var growByRef = function (obj) {
                var linesCount = 0; var lines = obj.value.split('\n'); for (var i = lines.length - 1; i >= 0; --i)
                { linesCount += Math.floor((lines[i].length / colsDefault) + 1); }
                if (linesCount >= rowsDefault)
                    obj.rows = linesCount + 1; else
                    obj.rows = rowsDefault;
            }
            var characterWidth = function (obj) { var characterWidth = 0; var temp1 = 0; var temp2 = 0; var tempCols = obj.cols; obj.cols = 1; temp1 = obj.offsetWidth; obj.cols = 2; temp2 = obj.offsetWidth; characterWidth = temp2 - temp1; obj.cols = tempCols; return characterWidth; }
            this.style.width = "auto"; this.style.height = "auto"; this.style.overflow = "hidden"; this.style.width = ((characterWidth(this) * this.cols) + 6) + "px"; this.onkeyup = grow; this.onfocus = grow; this.onblur = grow; growByRef(this);
        });
    }; (function (g) {
        var d = {}; d.ytplayers = {}; d.inits = []; d.iframeScriptInited = false; d.inited = false; g.tubeplayer = {}; g.tubeplayer.defaults = { afterReady: function () { }, stateChange: function (a) { var b = this.onPlayer; return function (c) { if (typeof c == "object") c = c.data; switch (c) { case -1: return b.unstarted[a](); case 0: return b.ended[a](); case 1: return b.playing[a](); case 2: return b.paused[a](); case 3: return b.buffering[a](); case 5: return b.cued[a](); default: return null } } }, onError: function (a) { var b = this.onErr; return function (c) { if (typeof c == "object") c = c.data; switch (c) { case 2: return b.invalidParameter[a](); case 100: return b.notFound[a](); case 101: case 150: return b.notEmbeddable[a](); default: return null } } }, qualityChange: function (a) { var b = this; return function (c) { if (typeof c == "object") c = c.data; return b.onQualityChange[a](c) } }, onQualityChange: {}, onPlayer: { unstarted: {}, ended: {}, playing: {}, paused: {}, buffering: {}, cued: {} }, onErr: { notFound: {}, notEmbeddable: {}, invalidParameter: {} } }; var j = { width: 425, height: 355, allowFullScreen: "true", initialVideo: "DkoeNLuMbcI", start: 0, preferredQuality: "default", showControls: true, showRelated: false, autoPlay: false, autoHide: true, theme: "dark", color: "red", showinfo: false, modestbranding: true, wmode: "transparent", swfobjectURL: "http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js", loadSWFObject: true, allowScriptAccess: "always", playerID: "tubeplayer-player-container", iframed: true, onPlay: function () { }, onPause: function () { }, onStop: function () { }, onSeek: function () { }, onMute: function () { }, onUnMute: function () { }, onPlayerUnstarted: function () { }, onPlayerEnded: function () { }, onPlayerPlaying: function () { }, onPlayerPaused: function () { }, onPlayerBuffering: function () { }, onPlayerCued: function () { }, onQualityChange: function () { }, onErrorNotFound: function () { }, onErrorNotEmbeddable: function () { }, onErrorInvalidParameter: function () { } }; g.fn.tubeplayer = function (a, b) { var c = g(this), f = typeof a; if (arguments.length == 0 || f == "object") return c.each(function () { d.init(g(this), a) }); else if (f == "string") return c.triggerHandler(a + ".tubeplayer", b || null) }; var h = function (a) { return function (b, c) { var f = d.getPkg(b); if (f.ytplayer) { b = a(b, c, f); if (typeof b == "undefined") b = f.$player; return b } return f.$player } }; g.tubeplayer.getPlayers = function () { return d.ytplayers }; d.init = function (a, b) { if (a.hasClass("jquery-youtube-tubeplayer")) return a; b = g.extend({}, j, b); b.playerID = b.playerID + (new Date).valueOf() + "_" + Math.random(); a.addClass("jquery-youtube-tubeplayer").data("opts.tubeplayer", b); for (e in i) a.bind(e + ".tubeplayer", a, i[e]); d.initDefaults(g.tubeplayer.defaults, b); jQuery("<div></div>").attr("id", b.playerID).appendTo(a); d.initPlayer(a, b); return a }; d.getPkg = function (a) { a = a.data; var b = a.data("opts.tubeplayer"); return { $player: a, opts: b, ytplayer: d.ytplayers[b.playerID] } }; d.iframeReady = function (a) { d.inits.push(function () { new YT.Player(a.playerID, { videoId: a.initialVideo, width: a.width, height: a.height, playerVars: { autoplay: a.autoPlay ? 1 : 0, autohide: a.autoHide ? 1 : 0, controls: a.showControls ? 1 : 0, rel: a.showRelated ? 1 : 0, fs: a.allowFullScreen ? 1 : 0, wmode: a.wmode, showinfo: a.showinfo ? 1 : 0, modestbranding: a.modestbranding ? 1 : 0, start: a.start, theme: a.theme, color: a.color }, events: { onReady: function (b) { d.ytplayers[a.playerID] = b.target; b = g(b.target).parents(".jquery-youtube-tubeplayer"); g.tubeplayer.defaults.afterReady(b) }, onPlaybackQualityChange: g.tubeplayer.defaults.qualityChange(a.playerID), onStateChange: g.tubeplayer.defaults.stateChange(a.playerID), onError: g.tubeplayer.defaults.onError(a.playerID) } }) }); if (d.inits.length >= 1 && !d.inited) return function () { for (var b = 0; b < d.inits.length; b++) d.inits[b](); d.inited = true }; d.inited && d.inits.pop()(); return onYouTubePlayerAPIReady }; d.supportsHTML5 = function () { return !!document.createElement("video").canPlayType }; d.initDefaults = function (a, b) { var c = b.playerID, f = a.onPlayer; f.unstarted[c] = b.onPlayerUnstarted; f.ended[c] = b.onPlayerEnded; f.playing[c] = b.onPlayerPlaying; f.paused[c] = b.onPlayerPaused; f.buffering[c] = b.onPlayerBuffering; f.cued[c] = b.onPlayerCued; a.onQualityChange[c] = b.onQualityChange; a = a.onErr; a.notFound[c] = b.onErrorNotFound; a.notEmbeddable[c] = b.onErrorNotEmbeddable; a.invalidParameter[c] = b.onErrorInvalidParameter }; d.initPlayer = function (a, b) { b.iframed && d.supportsHTML5() ? d.initIframePlayer(a, b) : d.initFlashPlayer(a, b) }; d.initIframePlayer = function (a, b) { if (!d.iframeScriptInited) { a = document.createElement("script"); a.src = "http://www.youtube.com/player_api"; var c = document.getElementsByTagName("script")[0]; c.parentNode.insertBefore(a, c); d.iframeScriptInited = true } onYouTubePlayerAPIReady = d.iframeReady(b) }; d.initFlashPlayer = function (a, b) { b.loadSWFObject ? g.getScript(b.swfobjectURL, d.initFlashPlayerFN(b)) : d.initFlashPlayerFN(b)() }; d.initFlashPlayerFN = function (a) {
            return function () {
                var b = ["//www.youtube.com/v/"]; b.push(a.initialVideo); b.push("?fs=" + (a.allowFullScreen ? 1 : 0)); b.push("&enablejsapi=1&version=3"); b.push("&playerapiid=" + a.playerID); b.push("&rel= " + (a.showRelated ? 1 : 0)); b.push("&autoplay=" + (a.autoPlay ? 1 : 0)); b.push("&autohide=" + (a.autoHide ? 1 : 0)); b.push("&controls=" + (a.showControls ? 1 : 0)); b.push("&showinfo=" + (a.showinfo ? 1 : 0)); b.push("&modestbranding=" + (a.modestbranding ? 1 : 0)); b.push("&start=" + a.start); b.push("&theme=" + a.theme); b.push("&color=" + a.color); swfobject.embedSWF(b.join(""), a.playerID, a.width, a.height, "8", null, null, { allowScriptAccess: a.allowScriptAccess, wmode: a.wmode, allowFullScreen: a.allowFullScreen }, { id: a.playerID }); onYouTubePlayerReady = function (c) {
                    var f = document.getElementById(c); d.ytplayers[c] = f; f.addEventListener("onStateChange", "$.tubeplayer.defaults.stateChange('" + c + "')"); f.addEventListener("onError", "$.tubeplayer.defaults.onError('" + c + "')"); f.addEventListener("onPlaybackQualityChange", "$.tubeplayer.defaults.qualityChange('" +
                    c + "')"); c = g(f).parents(".jquery-youtube-tubeplayer"); g.tubeplayer.defaults.afterReady(c)
                }
            }
        }; d.getVideoIDFromURL = function (a) { var b = a.indexOf("?"); a = a.substring(b, a.length); b = a.indexOf("v="); if (b > -1) { var c = a.indexOf("&", b); if (c == -1) c = a.length; return videoParam = a.substring(b + 2, c) } return "" }; var i = { cue: h(function (a, b, c) { c.ytplayer.cueVideoById(b, c.opts.preferredQuality) }), play: h(function (a, b, c) { if (typeof b == "object") c.ytplayer.loadVideoById(b.id, b.time, c.opts.preferredQuality); else b ? c.ytplayer.loadVideoById(b, 0, c.opts.preferredQuality) : c.ytplayer.playVideo(); c.opts.onPlay(b) }), pause: h(function (a, b, c) { c.ytplayer.pauseVideo(); c.opts.onPause() }), stop: h(function (a, b, c) { c.ytplayer.stopVideo(); c.opts.onStop() }), seek: h(function (a, b, c) { c.ytplayer.seekTo(b, true); c.opts.onSeek(b) }), mute: h(function (a, b, c) { c.$player.attr("data-prev-mute-volume", c.ytplayer.getVolume()); c.ytplayer.mute(); c.opts.onMute() }), unmute: h(function (a, b, c) { c.ytplayer.unMute(); c.ytplayer.setVolume(c.$player.attr("data-prev-mute-volume") || 50); c.opts.onUnMute() }), isMuted: h(function (a, b, c) { return c.ytplayer.isMuted() }), volume: h(function (a, b, c) { if (b) { c.ytplayer.setVolume(b); c.$player.attr("data-prev-mute-volume", c.ytplayer.getVolume()) } else return c.ytplayer.getVolume() || 0 }), quality: h(function (a, b, c) { if (b) c.ytplayer.setPlaybackQuality(b); else return c.ytplayer.getPlaybackQuality() }), data: h(function (a, b, c) { a = {}; c = c.ytplayer; a.bytesLoaded = c.getVideoBytesLoaded(); a.bytesTotal = c.getVideoBytesTotal(); a.startBytes = c.getVideoStartBytes(); a.state = c.getPlayerState(); a.currentTime = c.getCurrentTime(); a.availableQualityLevels = c.getAvailableQualityLevels(); a.duration = c.getDuration(); a.videoURL = c.getVideoUrl(); a.getVideoEmbedCode = c.getVideoEmbedCode(); a.videoID = d.getVideoIDFromURL(a.videoURL); return a }), videoId: h(function (a, b, c) { return d.getVideoIDFromURL(c.ytplayer.getVideoUrl()) }), size: h(function (a, b, c) { if (b.width && b.height) { c.ytplayer.setSize(b.width, b.height); g(c.ytplayer).css(b) } }), destroy: h(function (a, b, c) { c.$player.removeClass("jquery-youtube-tubeplayer").data("opts.tubeplayer", null).unbind(".tubeplayer").html(""); delete d.ytplayers[c.opts.playerID]; g(c.ytplayer).remove(); return null }), player: h(function (a, b, c) { return c.ytplayer }) }
    })(jQuery); (function ($) {
        $.facebox = function (data, klass) {
            $.facebox.loading(data.settings || [])
            if (data.ajax) fillFaceboxFromAjax(data.ajax, klass)
            else if (data.image) fillFaceboxFromImage(data.image, klass)
            else if (data.div) fillFaceboxFromHref(data.div, klass)
            else if ($.isFunction(data)) data.call($)
            else $.facebox.reveal(data, klass)
        }
        $.extend($.facebox, {
            settings: {
                opacity: 0.2, overlay: true, loadingImage: '/facebox/loading.gif', closeImage: '/facebox/closelabel.png', imageTypes: ['png', 'jpg', 'jpeg', 'gif'], faceboxHtml: '\
    <div id="facebox" style="display:none;"> \
      <div class="popup"> \
        <div class="content"> \
        </div> \
        <a href="#" class="close"></a> \
      </div> \
    </div>'}, loading: function () {
        init()
        if ($('#facebox .loading').length == 1) return true
        showOverlay()
        $('#facebox .content').empty().append('<div class="loading"><img src="' + $.facebox.settings.loadingImage + '"/></div>')
        $('#facebox').show()
        position()
        $(document).bind('keydown.facebox', function (e) {
            if (e.keyCode == 27) $.facebox.close()
            return true
        })
        $(document).trigger('loading.facebox')
    }, reveal: function (data, klass) {
        $(document).trigger('beforeReveal.facebox')
        if (klass) $('#facebox .content').addClass(klass)
        $('#facebox .content').empty().append(data)
        $('#facebox .popup').children().fadeIn('normal')
        $('#facebox').css('left', $(window).width() / 2 - ($('#facebox .popup').outerWidth() / 2))
        $(document).trigger('reveal.facebox').trigger('afterReveal.facebox')
    }, close: function () {
        $(document).trigger('close.facebox')
        return false
    }
        })
        $.fn.facebox = function (settings) {
            if ($(this).length == 0) return
            init(settings)
            function clickHandler() {
                $.facebox.loading(true)
                var klass = this.rel.match(/facebox\[?\.(\w+)\]?/)
                if (klass) klass = klass[1]
                fillFaceboxFromHref(this.href, klass)
                return false
            }
            return this.bind('click.facebox', clickHandler)
        }
        function init(settings) {
            if ($.facebox.settings.inited) return true
            else $.facebox.settings.inited = true
            $(document).trigger('init.facebox')
            makeCompatible()
            var imageTypes = $.facebox.settings.imageTypes.join('|')
            $.facebox.settings.imageTypesRegexp = new RegExp('\\.(' + imageTypes + ')(\\?.*)?$', 'i')
            if (settings) $.extend($.facebox.settings, settings)
            $('body').append($.facebox.settings.faceboxHtml)
            var preload = [new Image(), new Image()]
            preload[0].src = $.facebox.settings.closeImage
            preload[1].src = $.facebox.settings.loadingImage
            $('#facebox').find('.b:first, .bl').each(function () {
                preload.push(new Image())
                preload.slice(-1).src = $(this).css('background-image').replace(/url\((.+)\)/, '$1')
            })
            $('#facebox .close').click($.facebox.close).append('<img src="'
            + $.facebox.settings.closeImage
            + '" class="close_image" title="close">')
        }
        function getPageScroll() {
            var xScroll, yScroll; if (self.pageYOffset) { yScroll = self.pageYOffset; xScroll = self.pageXOffset; } else if (document.documentElement && document.documentElement.scrollTop) { yScroll = document.documentElement.scrollTop; xScroll = document.documentElement.scrollLeft; } else if (document.body) { yScroll = document.body.scrollTop; xScroll = document.body.scrollLeft; }
            return new Array(xScroll, yScroll)
        }
        function getPageHeight() {
            var windowHeight
            if (self.innerHeight) { windowHeight = self.innerHeight; } else if (document.documentElement && document.documentElement.clientHeight) { windowHeight = document.documentElement.clientHeight; } else if (document.body) { windowHeight = document.body.clientHeight; }
            return windowHeight
        }
        function makeCompatible() {
            var $s = $.facebox.settings
            $s.loadingImage = $s.loading_image || $s.loadingImage
            $s.closeImage = $s.close_image || $s.closeImage
            $s.imageTypes = $s.image_types || $s.imageTypes
            $s.faceboxHtml = $s.facebox_html || $s.faceboxHtml
        }
        function fillFaceboxFromHref(href, klass) {
            if (href.match(/#/)) {
                var url = window.location.href.split('#')[0]
                var target = href.replace(url, '')
                if (target == '#') return
                $.facebox.reveal($(target).html(), klass)
            } else if (href.match($.facebox.settings.imageTypesRegexp)) {
                fillFaceboxFromImage(href, klass)
            } else { fillFaceboxFromAjax(href, klass) }
        }
        function fillFaceboxFromImage(href, klass) {
            var image = new Image()
            image.onload = function () { $.facebox.reveal('<div class="image"><img src="' + image.src + '" /></div>', klass) }
            image.src = href
        }
        function fillFaceboxFromAjax(href, klass) { $.facebox.jqxhr = $.get(href, function (data) { $.facebox.reveal(data, klass) }) }
        function skipOverlay() { return $.facebox.settings.overlay == false || $.facebox.settings.opacity === null }
        function showOverlay() {
            if (skipOverlay()) return
            if ($('#facebox_overlay').length == 0)
                $("body").append('<div id="facebox_overlay" class="facebox_hide"></div>')
            $('#facebox_overlay').hide().addClass("facebox_overlayBG").css('opacity', $.facebox.settings.opacity).click(function () { $(document).trigger('close.facebox') }).fadeIn(200)
            return false
        }
        function hideOverlay() {
            if (skipOverlay()) return
            $('#facebox_overlay').fadeOut(200, function () {
                $("#facebox_overlay").removeClass("facebox_overlayBG")
                $("#facebox_overlay").addClass("facebox_hide")
                $("#facebox_overlay").remove()
            })
            return false
        }
        function position() { $('#facebox').css({ top: getPageScroll()[1] + (getPageHeight() / 10), left: $(window).width() / 2 - ($('#facebox .popup').outerWidth() / 2) }) }
        $(window).resize(position)
        $(document).bind('close.facebox', function () {
            if ($.facebox.jqxhr) {
                $.facebox.jqxhr.abort()
                $.facebox.jqxhr = null
            }
            $(document).unbind('keydown.facebox')
            $('#facebox').fadeOut(function () {
                $('#facebox .content').removeClass().addClass('content')
                $('#facebox .loading').remove()
                $(document).trigger('afterClose.facebox')
            })
            hideOverlay()
        })
    })(jQuery); (function (factory) {
        if (typeof define === 'function' && define.amd) { define(['jquery'], factory); }
        else { factory(jQuery); }
    }
    (function ($) {
        "use strict"; var TRUE = true, FALSE = false, NULL = null, undefined, QTIP, PLUGINS, MOUSE, usedIDs = {}, uitooltip = 'ui-tooltip', widget = 'ui-widget', disabled = 'ui-state-disabled', selector = 'div.qtip.' + uitooltip, defaultClass = uitooltip + '-default', focusClass = uitooltip + '-focus', hoverClass = uitooltip + '-hover', fluidClass = uitooltip + '-fluid', hideOffset = '-31000px', replaceSuffix = '_replacedByqTip', oldtitle = 'oldtitle', trackingBound; function log() {
            log.history = log.history || []; log.history.push(arguments); if ('object' === typeof console) {
                var c = console[console.warn ? 'warn' : 'log'], args = Array.prototype.slice.call(arguments), a; if (typeof arguments[0] === 'string') { args[0] = 'qTip2: ' + args[0]; }
                a = c.apply ? c.apply(console, args) : c(args);
            }
        }
        function sanitizeOptions(opts) {
            var content; if (!opts || 'object' !== typeof opts) { return FALSE; }
            if (opts.metadata === NULL || 'object' !== typeof opts.metadata) { opts.metadata = { type: opts.metadata }; }
            if ('content' in opts) {
                if (opts.content === NULL || 'object' !== typeof opts.content || opts.content.jquery) { opts.content = { text: opts.content }; }
                content = opts.content.text || FALSE; if (!$.isFunction(content) && ((!content && !content.attr) || content.length < 1 || ('object' === typeof content && !content.jquery))) { opts.content.text = FALSE; }
                if ('title' in opts.content) {
                    if (opts.content.title === NULL || 'object' !== typeof opts.content.title) { opts.content.title = { text: opts.content.title }; }
                    content = opts.content.title.text || FALSE; if (!$.isFunction(content) && ((!content && !content.attr) || content.length < 1 || ('object' === typeof content && !content.jquery))) { opts.content.title.text = FALSE; }
                }
            }
            if ('position' in opts) { if (opts.position === NULL || 'object' !== typeof opts.position) { opts.position = { my: opts.position, at: opts.position }; } }
            if ('show' in opts) {
                if (opts.show === NULL || 'object' !== typeof opts.show) {
                    if (opts.show.jquery) { opts.show = { target: opts.show }; }
                    else { opts.show = { event: opts.show }; }
                }
            }
            if ('hide' in opts) {
                if (opts.hide === NULL || 'object' !== typeof opts.hide) {
                    if (opts.hide.jquery) { opts.hide = { target: opts.hide }; }
                    else { opts.hide = { event: opts.hide }; }
                }
            }
            if ('style' in opts) { if (opts.style === NULL || 'object' !== typeof opts.style) { opts.style = { classes: opts.style }; } }
            $.each(PLUGINS, function () { if (this.sanitize) { this.sanitize(opts); } }); return opts;
        }
        function QTip(target, options, id, attr) {
            var self = this, docBody = document.body, tooltipID = uitooltip + '-' + id, isPositioning = 0, isDrawing = 0, tooltip = $(), namespace = '.qtip-' + id, elements, cache; self.id = id; self.rendered = FALSE; self.destroyed = FALSE; self.elements = elements = { target: target }; self.timers = { img: {} }; self.options = options; self.checks = {}; self.plugins = {}; self.cache = cache = { event: {}, target: $(), disabled: FALSE, attr: attr, onTarget: FALSE }; function convertNotation(notation) {
                var i = 0, obj, option = options, levels = notation.split('.'); while (option = option[levels[i++]]) { if (i < levels.length) { obj = option; } }
                return [obj || options, levels.pop()];
            }
            function setWidget() {
                var on = options.style.widget; tooltip.toggleClass(widget, on).toggleClass(defaultClass, options.style.def && !on); elements.content.toggleClass(widget + '-content', on); if (elements.titlebar) { elements.titlebar.toggleClass(widget + '-header', on); }
                if (elements.button) { elements.button.toggleClass(uitooltip + '-icon', !on); }
            }
            function removeTitle(reposition)
            { if (elements.title) { elements.titlebar.remove(); elements.titlebar = elements.title = elements.button = NULL; if (reposition !== FALSE) { self.reposition(); } } }
            function createButton() {
                var button = options.content.title.button, isString = typeof button === 'string', close = isString ? button : 'Close tooltip'; if (elements.button) { elements.button.remove(); }
                if (button.jquery) { elements.button = button; }
                else { elements.button = $('<a />', { 'class': 'ui-state-default ui-tooltip-close ' + (options.style.widget ? '' : uitooltip + '-icon'), 'title': close, 'aria-label': close }).prepend($('<span />', { 'class': 'ui-icon ui-icon-close', 'html': '&times;' })); }
                elements.button.appendTo(elements.titlebar).attr('role', 'button').click(function (event) {
                    if (!tooltip.hasClass(disabled)) { self.hide(event); }
                    return FALSE;
                }); self.redraw();
            }
            function createTitle() {
                var id = tooltipID + '-title'; if (elements.titlebar) { removeTitle(); }
                elements.titlebar = $('<div />', { 'class': uitooltip + '-titlebar ' + (options.style.widget ? 'ui-widget-header' : '') }).append(elements.title = $('<div />', { 'id': id, 'class': uitooltip + '-title', 'aria-atomic': TRUE })).insertBefore(elements.content)
                .delegate('.ui-tooltip-close', 'mousedown keydown mouseup keyup mouseout', function (event) { $(this).toggleClass('ui-state-active ui-state-focus', event.type.substr(-4) === 'down'); }).delegate('.ui-tooltip-close', 'mouseover mouseout', function (event) { $(this).toggleClass('ui-state-hover', event.type === 'mouseover'); }); if (options.content.title.button) { createButton(); }
                else if (self.rendered) { self.redraw(); }
            }
            function updateButton(button) {
                var elem = elements.button, title = elements.title; if (!self.rendered) { return FALSE; }
                if (!button) { elem.remove(); }
                else {
                    if (!title) { createTitle(); }
                    createButton();
                }
            }
            function updateTitle(content, reposition) {
                var elem = elements.title; if (!self.rendered || !content) { return FALSE; }
                if ($.isFunction(content)) { content = content.call(target, cache.event, self); }
                if (content === FALSE || (!content && content !== '')) { return removeTitle(FALSE); }
                else if (content.jquery && content.length > 0) { elem.empty().append(content.css({ display: 'block' })); }
                else { elem.html(content); }
                self.redraw(); if (reposition !== FALSE && self.rendered && tooltip[0].offsetWidth > 0) { self.reposition(cache.event); }
            }
            function updateContent(content, reposition) {
                var elem = elements.content; if (!self.rendered || !content) { return FALSE; }
                if ($.isFunction(content)) { content = content.call(target, cache.event, self) || ''; }
                if (content.jquery && content.length > 0) { elem.empty().append(content.css({ display: 'block' })); }
                else { elem.html(content); }
                function detectImages(next) {
                    var images, srcs = {}; function imageLoad(image) {
                        if (image) { delete srcs[image.src]; clearTimeout(self.timers.img[image.src]); $(image).unbind(namespace); }
                        if ($.isEmptyObject(srcs)) {
                            self.redraw(); if (reposition !== FALSE) { self.reposition(cache.event); }
                            next();
                        }
                    }
                    if ((images = elem.find('img[src]:not([height]):not([width])')).length === 0) { return imageLoad(); }
                    images.each(function (i, elem) {
                        if (srcs[elem.src] !== undefined) { return; }
                        var iterations = 0, maxIterations = 3; (function timer() {
                            if (elem.height || elem.width || (iterations > maxIterations)) { return imageLoad(elem); }
                            iterations += 1; self.timers.img[elem.src] = setTimeout(timer, 700);
                        }()); $(elem).bind('error' + namespace + ' load' + namespace, function () { imageLoad(this); }); srcs[elem.src] = elem;
                    });
                }
                if (self.rendered < 0) { tooltip.queue('fx', detectImages); }
                else { isDrawing = 0; detectImages($.noop); }
                return self;
            }
            function assignEvents() {
                var posOptions = options.position, targets = { show: options.show.target, hide: options.hide.target, viewport: $(posOptions.viewport), document: $(document), body: $(document.body), window: $(window) }, events = { show: $.trim('' + options.show.event).split(' '), hide: $.trim('' + options.hide.event).split(' ') }, IE6 = $.browser.msie && parseInt($.browser.version, 10) === 6; function showMethod(event) {
                    if (tooltip.hasClass(disabled)) { return FALSE; }
                    clearTimeout(self.timers.show); clearTimeout(self.timers.hide); var callback = function () { self.toggle(TRUE, event); }; if (options.show.delay > 0) { self.timers.show = setTimeout(callback, options.show.delay); }
                    else { callback(); }
                }
                function hideMethod(event) {
                    if (tooltip.hasClass(disabled) || isPositioning || isDrawing) { return FALSE; }
                    var relatedTarget = $(event.relatedTarget || event.target), ontoTooltip = relatedTarget.closest(selector)[0] === tooltip[0], ontoTarget = relatedTarget[0] === targets.show[0]; clearTimeout(self.timers.show); clearTimeout(self.timers.hide); if ((posOptions.target === 'mouse' && ontoTooltip) || (options.hide.fixed && ((/mouse(out|leave|move)/).test(event.type) && (ontoTooltip || ontoTarget)))) { try { event.preventDefault(); event.stopImmediatePropagation(); } catch (e) { } return; }
                    if (options.hide.delay > 0) { self.timers.hide = setTimeout(function () { self.hide(event); }, options.hide.delay); }
                    else { self.hide(event); }
                }
                function inactiveMethod(event) {
                    if (tooltip.hasClass(disabled)) { return FALSE; }
                    clearTimeout(self.timers.inactive); self.timers.inactive = setTimeout(function () { self.hide(event); }, options.hide.inactive);
                }
                function repositionMethod(event) { if (self.rendered && tooltip[0].offsetWidth > 0) { self.reposition(event); } }
                tooltip.bind('mouseenter' + namespace + ' mouseleave' + namespace, function (event) {
                    var state = event.type === 'mouseenter'; if (state) { self.focus(event); }
                    tooltip.toggleClass(hoverClass, state);
                }); if (options.hide.fixed) { targets.hide = targets.hide.add(tooltip); tooltip.bind('mouseover' + namespace, function () { if (!tooltip.hasClass(disabled)) { clearTimeout(self.timers.hide); } }); }
                if (/mouse(out|leave)/i.test(options.hide.event)) { if (options.hide.leave === 'window') { targets.window.bind('mouseout' + namespace + ' blur' + namespace, function (event) { if (/select|option/.test(event.target) && !event.relatedTarget) { self.hide(event); } }); } }
                else if (/mouse(over|enter)/i.test(options.show.event)) { targets.hide.bind('mouseleave' + namespace, function (event) { clearTimeout(self.timers.show); }); }
                if (('' + options.hide.event).indexOf('unfocus') > -1) { posOptions.container.closest('html').bind('mousedown' + namespace, function (event) { var elem = $(event.target), enabled = self.rendered && !tooltip.hasClass(disabled) && tooltip[0].offsetWidth > 0, isAncestor = elem.parents(selector).filter(tooltip[0]).length > 0; if (elem[0] !== target[0] && elem[0] !== tooltip[0] && !isAncestor && !target.has(elem[0]).length && !elem.attr('disabled')) { self.hide(event); } }); }
                if ('number' === typeof options.hide.inactive) { targets.show.bind('qtip-' + id + '-inactive', inactiveMethod); $.each(QTIP.inactiveEvents, function (index, type) { targets.hide.add(elements.tooltip).bind(type + namespace + '-inactive', inactiveMethod); }); }
                $.each(events.hide, function (index, type) {
                    var showIndex = $.inArray(type, events.show), targetHide = $(targets.hide); if ((showIndex > -1 && targetHide.add(targets.show).length === targetHide.length) || type === 'unfocus') {
                        targets.show.bind(type + namespace, function (event) {
                            if (tooltip[0].offsetWidth > 0) { hideMethod(event); }
                            else { showMethod(event); }
                        }); delete events.show[showIndex];
                    }
                    else { targets.hide.bind(type + namespace, hideMethod); }
                }); $.each(events.show, function (index, type) { targets.show.bind(type + namespace, showMethod); }); if ('number' === typeof options.hide.distance) { targets.show.add(tooltip).bind('mousemove' + namespace, function (event) { var origin = cache.origin || {}, limit = options.hide.distance, abs = Math.abs; if (abs(event.pageX - origin.pageX) >= limit || abs(event.pageY - origin.pageY) >= limit) { self.hide(event); } }); }
                if (posOptions.target === 'mouse') {
                    targets.show.bind('mousemove' + namespace, function (event) { MOUSE = { pageX: event.pageX, pageY: event.pageY, type: 'mousemove' }; }); if (posOptions.adjust.mouse) {
                        if (options.hide.event) { tooltip.bind('mouseleave' + namespace, function (event) { if ((event.relatedTarget || event.target) !== targets.show[0]) { self.hide(event); } }); elements.target.bind('mouseenter' + namespace + ' mouseleave' + namespace, function (event) { cache.onTarget = event.type === 'mouseenter'; }); }
                        targets.document.bind('mousemove' + namespace, function (event) { if (self.rendered && cache.onTarget && !tooltip.hasClass(disabled) && tooltip[0].offsetWidth > 0) { self.reposition(event || MOUSE); } });
                    }
                }
                if (posOptions.adjust.resize || targets.viewport.length) { ($.event.special.resize ? targets.viewport : targets.window).bind('resize' + namespace, repositionMethod); }
                if (targets.viewport.length || (IE6 && tooltip.css('position') === 'fixed')) { targets.viewport.bind('scroll' + namespace, repositionMethod); }
            }
            function unassignEvents() {
                var targets = [options.show.target[0], options.hide.target[0], self.rendered && elements.tooltip[0], options.position.container[0], options.position.viewport[0], window, document]; if (self.rendered) { $([]).pushStack($.grep(targets, function (i) { return typeof i === 'object'; })).unbind(namespace); }
                else { options.show.target.unbind(namespace + '-create'); }
            }
            self.checks.builtin = {
                '^id$': function (obj, o, v) { var id = v === TRUE ? QTIP.nextid : v, tooltipID = uitooltip + '-' + id; if (id !== FALSE && id.length > 0 && !$('#' + tooltipID).length) { tooltip[0].id = tooltipID; elements.content[0].id = tooltipID + '-content'; elements.title[0].id = tooltipID + '-title'; } }, '^content.text$': function (obj, o, v) { updateContent(v); }, '^content.title.text$': function (obj, o, v) {
                    if (!v) { return removeTitle(); }
                    if (!elements.title && v) { createTitle(); }
                    updateTitle(v);
                }, '^content.title.button$': function (obj, o, v) { updateButton(v); }, '^position.(my|at)$': function (obj, o, v) { if ('string' === typeof v) { obj[o] = new PLUGINS.Corner(v); } }, '^position.container$': function (obj, o, v) { if (self.rendered) { tooltip.appendTo(v); } }, '^show.ready$': function () {
                    if (!self.rendered) { self.render(1); }
                    else { self.toggle(TRUE); }
                }, '^style.classes$': function (obj, o, v) { tooltip.attr('class', uitooltip + ' qtip ui-helper-reset ' + v); }, '^style.widget|content.title': setWidget, '^events.(render|show|move|hide|focus|blur)$': function (obj, o, v) { tooltip[($.isFunction(v) ? '' : 'un') + 'bind']('tooltip' + o, v); }, '^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)': function () { var posOptions = options.position; tooltip.attr('tracking', posOptions.target === 'mouse' && posOptions.adjust.mouse); unassignEvents(); assignEvents(); }
            }; $.extend(self, {
                render: function (show) {
                    if (self.rendered) { return self; }
                    var text = options.content.text, title = options.content.title.text, posOptions = options.position, callback = $.Event('tooltiprender'); $.attr(target[0], 'aria-describedby', tooltipID); tooltip = elements.tooltip = $('<div/>', { 'id': tooltipID, 'class': uitooltip + ' qtip ui-helper-reset ' + defaultClass + ' ' + options.style.classes + ' ' + uitooltip + '-pos-' + options.position.my.abbrev(), 'width': options.style.width || '', 'height': options.style.height || '', 'tracking': posOptions.target === 'mouse' && posOptions.adjust.mouse, 'role': 'alert', 'aria-live': 'polite', 'aria-atomic': FALSE, 'aria-describedby': tooltipID + '-content', 'aria-hidden': TRUE }).toggleClass(disabled, cache.disabled).data('qtip', self).appendTo(options.position.container).append(elements.content = $('<div />', { 'class': uitooltip + '-content', 'id': tooltipID + '-content', 'aria-atomic': TRUE })); self.rendered = -1; isDrawing = 1; isPositioning = 1; if (title) { createTitle(); if (!$.isFunction(title)) { updateTitle(title, FALSE); } }
                    if (!$.isFunction(text)) { updateContent(text, FALSE); }
                    self.rendered = TRUE; setWidget(); $.each(options.events, function (name, callback) { if ($.isFunction(callback)) { tooltip.bind(name === 'toggle' ? 'tooltipshow tooltiphide' : 'tooltip' + name, callback); } }); $.each(PLUGINS, function () { if (this.initialize === 'render') { this(self); } }); assignEvents(); tooltip.queue('fx', function (next) {
                        callback.originalEvent = cache.event; tooltip.trigger(callback, [self]); isDrawing = 0; isPositioning = 0; self.redraw(); if (options.show.ready || show) { self.toggle(TRUE, cache.event, FALSE); }
                        next();
                    }); return self;
                }, get: function (notation) {
                    var result, o; switch (notation.toLowerCase())
                    { case 'dimensions': result = { height: tooltip.outerHeight(), width: tooltip.outerWidth() }; break; case 'offset': result = PLUGINS.offset(tooltip, options.position.container); break; default: o = convertNotation(notation.toLowerCase()); result = o[0][o[1]]; result = result.precedance ? result.string() : result; break; }
                    return result;
                }, set: function (option, value) {
                    var rmove = /^position\.(my|at|adjust|target|container)|style|content|show\.ready/i, rdraw = /^content\.(title|attr)|style/i, reposition = FALSE, redraw = FALSE, checks = self.checks, name; function callback(notation, args) { var category, rule, match; for (category in checks) { for (rule in checks[category]) { if (match = (new RegExp(rule, 'i')).exec(notation)) { args.push(match); checks[category][rule].apply(self, args); } } } }
                    if ('string' === typeof option) { name = option; option = {}; option[name] = value; }
                    else { option = $.extend(TRUE, {}, option); }
                    $.each(option, function (notation, value) { var obj = convertNotation(notation.toLowerCase()), previous; previous = obj[0][obj[1]]; obj[0][obj[1]] = 'object' === typeof value && value.nodeType ? $(value) : value; option[notation] = [obj[0], obj[1], value, previous]; reposition = rmove.test(notation) || reposition; redraw = rdraw.test(notation) || redraw; }); sanitizeOptions(options); isPositioning = isDrawing = 1; $.each(option, callback); isPositioning = isDrawing = 0; if (self.rendered && tooltip[0].offsetWidth > 0) {
                        if (reposition) { self.reposition(options.position.target === 'mouse' ? NULL : cache.event); }
                        if (redraw) { self.redraw(); }
                    }
                    return self;
                }, toggle: function (state, event) {
                    if (!self.rendered) { return state ? self.render(1) : self; }
                    var type = state ? 'show' : 'hide', opts = options[type], otherOpts = options[!state ? 'show' : 'hide'], posOptions = options.position, contentOptions = options.content, visible = tooltip[0].offsetWidth > 0, animate = state || opts.target.length === 1, sameTarget = !event || opts.target.length < 2 || cache.target[0] === event.target, delay, callback; if ((typeof state).search('boolean|number')) { state = !visible; }
                    if (!tooltip.is(':animated') && visible === state && sameTarget) { return self; }
                    if (event) {
                        if ((/over|enter/).test(event.type) && (/out|leave/).test(cache.event.type) && options.show.target.add(event.target).length === options.show.target.length && tooltip.has(event.relatedTarget).length) { return self; }
                        cache.event = $.extend({}, event);
                    }
                    callback = $.Event('tooltip' + type); callback.originalEvent = event ? cache.event : NULL; tooltip.trigger(callback, [self, 90]); if (callback.isDefaultPrevented()) { return self; }
                    $.attr(tooltip[0], 'aria-hidden', !!!state); if (state) {
                        cache.origin = $.extend({}, MOUSE); self.focus(event); if ($.isFunction(contentOptions.text)) { updateContent(contentOptions.text, FALSE); }
                        if ($.isFunction(contentOptions.title.text)) { updateTitle(contentOptions.title.text, FALSE); }
                        if (!trackingBound && posOptions.target === 'mouse' && posOptions.adjust.mouse) { $(document).bind('mousemove.qtip', function (event) { MOUSE = { pageX: event.pageX, pageY: event.pageY, type: 'mousemove' }; }); trackingBound = TRUE; }
                        self.reposition(event, arguments[2]); if ((callback.solo = !!opts.solo)) { $(selector, opts.solo).not(tooltip).qtip('hide', callback); }
                    }
                    else {
                        clearTimeout(self.timers.show); delete cache.origin; if (trackingBound && !$(selector + '[tracking="true"]:visible', opts.solo).not(tooltip).length) { $(document).unbind('mousemove.qtip'); trackingBound = FALSE; }
                        self.blur(event);
                    }
                    function after() {
                        if (state) {
                            if ($.browser.msie) { tooltip[0].style.removeAttribute('filter'); }
                            tooltip.css('overflow', ''); if ('string' === typeof opts.autofocus) { $(opts.autofocus, tooltip).focus(); }
                            opts.target.trigger('qtip-' + id + '-inactive');
                        }
                        else { tooltip.css({ display: '', visibility: '', opacity: '', left: '', top: '' }); }
                        callback = $.Event('tooltip' + (state ? 'visible' : 'hidden')); callback.originalEvent = event ? cache.event : NULL; tooltip.trigger(callback, [self]);
                    }
                    if (opts.effect === FALSE || animate === FALSE) { tooltip[type](); after.call(tooltip); }
                    else if ($.isFunction(opts.effect)) { tooltip.stop(1, 1); opts.effect.call(tooltip, self); tooltip.queue('fx', function (n) { after(); n(); }); }
                    else { tooltip.fadeTo(90, state ? 1 : 0, after); }
                    if (state) { opts.target.trigger('qtip-' + id + '-inactive'); }
                    return self;
                }, show: function (event) { return self.toggle(TRUE, event); }, hide: function (event) { return self.toggle(FALSE, event); }, focus: function (event) {
                    if (!self.rendered) { return self; }
                    var qtips = $(selector), curIndex = parseInt(tooltip[0].style.zIndex, 10), newIndex = QTIP.zindex + qtips.length, cachedEvent = $.extend({}, event), focusedElem, callback; if (!tooltip.hasClass(focusClass)) {
                        callback = $.Event('tooltipfocus'); callback.originalEvent = cachedEvent; tooltip.trigger(callback, [self, newIndex]); if (!callback.isDefaultPrevented()) {
                            if (curIndex !== newIndex) { qtips.each(function () { if (this.style.zIndex > curIndex) { this.style.zIndex = this.style.zIndex - 1; } }); qtips.filter('.' + focusClass).qtip('blur', cachedEvent); }
                            tooltip.addClass(focusClass)[0].style.zIndex = newIndex;
                        }
                    }
                    return self;
                }, blur: function (event) { var cachedEvent = $.extend({}, event), callback; tooltip.removeClass(focusClass); callback = $.Event('tooltipblur'); callback.originalEvent = cachedEvent; tooltip.trigger(callback, [self]); return self; }, reposition: function (event, effect) {
                    if (!self.rendered || isPositioning) { return self; }
                    isPositioning = 1; var target = options.position.target, posOptions = options.position, my = posOptions.my, at = posOptions.at, adjust = posOptions.adjust, method = adjust.method.split(' '), elemWidth = tooltip.outerWidth(), elemHeight = tooltip.outerHeight(), targetWidth = 0, targetHeight = 0, callback = $.Event('tooltipmove'), fixed = tooltip.css('position') === 'fixed', viewport = posOptions.viewport, position = { left: 0, top: 0 }, container = posOptions.container, flipoffset = FALSE, tip = self.plugins.tip, visible = tooltip[0].offsetWidth > 0, readjust = {
                        horizontal: method[0], vertical: (method[1] = method[1] || method[0]), enabled: viewport.jquery && target[0] !== window && target[0] !== docBody && adjust.method !== 'none', left: function (posLeft) {
                            var isShift = readjust.horizontal === 'shift', adjustx = adjust.x * (readjust.horizontal.substr(-6) === 'invert' ? 2 : 0), viewportScroll = -container.offset.left + viewport.offset.left + viewport.scrollLeft, myWidth = my.x === 'left' ? elemWidth : my.x === 'right' ? -elemWidth : -elemWidth / 2, atWidth = at.x === 'left' ? targetWidth : at.x === 'right' ? -targetWidth : -targetWidth / 2, tipWidth = tip && tip.size ? tip.size.width || 0 : 0, tipAdjust = tip && tip.corner && tip.corner.precedance === 'x' && !isShift ? tipWidth : 0, overflowLeft = viewportScroll - posLeft + tipAdjust, overflowRight = posLeft + elemWidth - viewport.width - viewportScroll + tipAdjust, offset = myWidth - (my.precedance === 'x' || my.x === my.y ? atWidth : 0) - (at.x === 'center' ? targetWidth / 2 : 0), isCenter = my.x === 'center'; if (isShift) { tipAdjust = tip && tip.corner && tip.corner.precedance === 'y' ? tipWidth : 0; offset = (my.x === 'left' ? 1 : -1) * myWidth - tipAdjust; position.left += overflowLeft > 0 ? overflowLeft : overflowRight > 0 ? -overflowRight : 0; position.left = Math.max(-container.offset.left + viewport.offset.left + (tipAdjust && tip.corner.x === 'center' ? tip.offset : 0), posLeft - offset, Math.min(Math.max(-container.offset.left + viewport.offset.left + viewport.width, posLeft + offset), position.left)); }
                            else {
                                if (overflowLeft > 0 && (my.x !== 'left' || overflowRight > 0)) { position.left -= offset + adjustx; }
                                else if (overflowRight > 0 && (my.x !== 'right' || overflowLeft > 0)) { position.left -= (isCenter ? -offset : offset) + adjustx; }
                                if (position.left < viewportScroll && -position.left > overflowRight) { position.left = posLeft; }
                            }
                            return position.left - posLeft;
                        }, top: function (posTop) {
                            var isShift = readjust.vertical === 'shift', adjusty = adjust.y * (readjust.vertical.substr(-6) === 'invert' ? 2 : 0), viewportScroll = -container.offset.top + viewport.offset.top + viewport.scrollTop, myHeight = my.y === 'top' ? elemHeight : my.y === 'bottom' ? -elemHeight : -elemHeight / 2, atHeight = at.y === 'top' ? targetHeight : at.y === 'bottom' ? -targetHeight : -targetHeight / 2, tipHeight = tip && tip.size ? tip.size.height || 0 : 0, tipAdjust = tip && tip.corner && tip.corner.precedance === 'y' && !isShift ? tipHeight : 0, overflowTop = viewportScroll - posTop + tipAdjust, overflowBottom = posTop + elemHeight - viewport.height - viewportScroll + tipAdjust, offset = myHeight - (my.precedance === 'y' || my.x === my.y ? atHeight : 0) - (at.y === 'center' ? targetHeight / 2 : 0), isCenter = my.y === 'center'; if (isShift) { tipAdjust = tip && tip.corner && tip.corner.precedance === 'x' ? tipHeight : 0; offset = (my.y === 'top' ? 1 : -1) * myHeight - tipAdjust; position.top += overflowTop > 0 ? overflowTop : overflowBottom > 0 ? -overflowBottom : 0; position.top = Math.max(-container.offset.top + viewport.offset.top + (tipAdjust && tip.corner.x === 'center' ? tip.offset : 0), posTop - offset, Math.min(Math.max(-container.offset.top + viewport.offset.top + viewport.height, posTop + offset), position.top)); }
                            else {
                                if (overflowTop > 0 && (my.y !== 'top' || overflowBottom > 0)) { position.top -= offset + adjusty; }
                                else if (overflowBottom > 0 && (my.y !== 'bottom' || overflowTop > 0)) { position.top -= (isCenter ? -offset : offset) + adjusty; }
                                if (position.top < 0 && -position.top > overflowBottom) { position.top = posTop; }
                            }
                            return position.top - posTop;
                        }
                    }, win; if ($.isArray(target) && target.length === 2) { at = { x: 'left', y: 'top' }; position = { left: target[0], top: target[1] }; }
                    else if (target === 'mouse' && ((event && event.pageX) || cache.event.pageX)) { at = { x: 'left', y: 'top' }; event = (event && (event.type === 'resize' || event.type === 'scroll') ? cache.event : event && event.pageX && event.type === 'mousemove' ? event : MOUSE && MOUSE.pageX && (adjust.mouse || !event || !event.pageX) ? { pageX: MOUSE.pageX, pageY: MOUSE.pageY } : !adjust.mouse && cache.origin && cache.origin.pageX && options.show.distance ? cache.origin : event) || event || cache.event || MOUSE || {}; position = { top: event.pageY, left: event.pageX }; }
                    else {
                        if (target === 'event') {
                            if (event && event.target && event.type !== 'scroll' && event.type !== 'resize') { target = cache.target = $(event.target); }
                            else { target = cache.target; }
                        }
                        else { target = cache.target = $(target.jquery ? target : elements.target); }
                        target = $(target).eq(0); if (target.length === 0) { return self; }
                        else if (target[0] === document || target[0] === window) { targetWidth = PLUGINS.iOS ? window.innerWidth : target.width(); targetHeight = PLUGINS.iOS ? window.innerHeight : target.height(); if (target[0] === window) { position = { top: (viewport || target).scrollTop(), left: (viewport || target).scrollLeft() }; } }
                        else if (target.is('area') && PLUGINS.imagemap) { position = PLUGINS.imagemap(target, at, readjust.enabled ? method : FALSE); }
                        else if (target[0].namespaceURI === 'http://www.w3.org/2000/svg' && PLUGINS.svg) { position = PLUGINS.svg(target, at); }
                        else { targetWidth = target.outerWidth(); targetHeight = target.outerHeight(); position = PLUGINS.offset(target, container); }
                        if (position.offset) { targetWidth = position.width; targetHeight = position.height; flipoffset = position.flipoffset; position = position.offset; }
                        if ((PLUGINS.iOS < 4.1 && PLUGINS.iOS > 3.1) || PLUGINS.iOS == 4.3 || (!PLUGINS.iOS && fixed)) { win = $(window); position.left -= win.scrollLeft(); position.top -= win.scrollTop(); }
                        position.left += at.x === 'right' ? targetWidth : at.x === 'center' ? targetWidth / 2 : 0; position.top += at.y === 'bottom' ? targetHeight : at.y === 'center' ? targetHeight / 2 : 0;
                    }
                    position.left += adjust.x + (my.x === 'right' ? -elemWidth : my.x === 'center' ? -elemWidth / 2 : 0); position.top += adjust.y + (my.y === 'bottom' ? -elemHeight : my.y === 'center' ? -elemHeight / 2 : 0); if (readjust.enabled) {
                        viewport = { elem: viewport, height: viewport[(viewport[0] === window ? 'h' : 'outerH') + 'eight'](), width: viewport[(viewport[0] === window ? 'w' : 'outerW') + 'idth'](), scrollLeft: fixed ? 0 : viewport.scrollLeft(), scrollTop: fixed ? 0 : viewport.scrollTop(), offset: viewport.offset() || { left: 0, top: 0 } }; container = { elem: container, scrollLeft: container.scrollLeft(), scrollTop: container.scrollTop(), offset: container.offset() || { left: 0, top: 0 } }; position.adjusted = { left: readjust.horizontal !== 'none' ? readjust.left(position.left) : 0, top: readjust.vertical !== 'none' ? readjust.top(position.top) : 0 }; if (position.adjusted.left + position.adjusted.top) { tooltip.attr('class', tooltip[0].className.replace(/ui-tooltip-pos-\w+/i, uitooltip + '-pos-' + my.abbrev())); }
                        if (flipoffset && position.adjusted.left) { position.left += flipoffset.left; }
                        if (flipoffset && position.adjusted.top) { position.top += flipoffset.top; }
                    }
                    else { position.adjusted = { left: 0, top: 0 }; }
                    callback.originalEvent = $.extend({}, event); tooltip.trigger(callback, [self, position, viewport.elem || viewport]); if (callback.isDefaultPrevented()) { return self; }
                    delete position.adjusted; if (effect === FALSE || !visible || isNaN(position.left) || isNaN(position.top) || target === 'mouse' || !$.isFunction(posOptions.effect)) { tooltip.css(position); }
                    else if ($.isFunction(posOptions.effect)) {
                        posOptions.effect.call(tooltip, self, $.extend({}, position)); tooltip.queue(function (next) {
                            $(this).css({ opacity: '', height: '' }); if ($.browser.msie) { this.style.removeAttribute('filter'); }
                            next();
                        });
                    }
                    isPositioning = 0; return self;
                }, redraw: function () {
                    if (self.rendered < 1 || isDrawing) { return self; }
                    var container = options.position.container, perc, width, max, min; isDrawing = 1; if (options.style.height) { tooltip.css('height', options.style.height); }
                    if (options.style.width) { tooltip.css('width', options.style.width); }
                    else { tooltip.css('width', '').addClass(fluidClass); width = tooltip.width() + 1; max = tooltip.css('max-width') || ''; min = tooltip.css('min-width') || ''; perc = (max + min).indexOf('%') > -1 ? container.width() / 100 : 0; max = ((max.indexOf('%') > -1 ? perc : 1) * parseInt(max, 10)) || width; min = ((min.indexOf('%') > -1 ? perc : 1) * parseInt(min, 10)) || 0; width = max + min ? Math.min(Math.max(width, min), max) : width; tooltip.css('width', Math.round(width)).removeClass(fluidClass); }
                    isDrawing = 0; return self;
                }, disable: function (state) {
                    if ('boolean' !== typeof state) { state = !(tooltip.hasClass(disabled) || cache.disabled); }
                    if (self.rendered) { tooltip.toggleClass(disabled, state); $.attr(tooltip[0], 'aria-disabled', state); }
                    else { cache.disabled = !!state; }
                    return self;
                }, enable: function () { return self.disable(FALSE); }, destroy: function () {
                    var t = target[0], title = $.attr(t, oldtitle), elemAPI = target.data('qtip'); self.destroyed = TRUE; if (self.rendered) { tooltip.stop(1, 0).remove(); $.each(self.plugins, function () { if (this.destroy) { this.destroy(); } }); }
                    clearTimeout(self.timers.show); clearTimeout(self.timers.hide); unassignEvents(); if (!elemAPI || self === elemAPI) {
                        $.removeData(t, 'qtip'); if (options.suppress && title) { $.attr(t, 'title', title); target.removeAttr(oldtitle); }
                        target.removeAttr('aria-describedby');
                    }
                    target.unbind('.qtip-' + id); delete usedIDs[self.id]; return target;
                }
            });
        }
        function init(id, opts) {
            var obj, posOptions, attr, config, title, elem = $(this), docBody = $(document.body), newTarget = this === document ? docBody : elem, metadata = (elem.metadata) ? elem.metadata(opts.metadata) : NULL, metadata5 = opts.metadata.type === 'html5' && metadata ? metadata[opts.metadata.name] : NULL, html5 = elem.data(opts.metadata.name || 'qtipopts'); try { html5 = typeof html5 === 'string' ? (new Function("return " + html5))() : html5; }
            catch (e) { log('Unable to parse HTML5 attribute data: ' + html5); }
            config = $.extend(TRUE, {}, QTIP.defaults, opts, typeof (html5) === 'object' ? sanitizeOptions(html5) : NULL, sanitizeOptions(metadata5 || metadata)); posOptions = config.position; config.id = id; if ('boolean' === typeof config.content.text) {
                attr = elem.attr(config.content.attr); if (config.content.attr !== FALSE && attr) { config.content.text = attr; }
                else { log('Unable to locate content for tooltip! Aborting render of tooltip on element: ', elem); return FALSE; }
            }
            if (!posOptions.container.length) { posOptions.container = docBody; }
            if (posOptions.target === FALSE) { posOptions.target = newTarget; }
            if (config.show.target === FALSE) { config.show.target = newTarget; }
            if (config.show.solo === TRUE) { config.show.solo = posOptions.container.closest('body'); }
            if (config.hide.target === FALSE) { config.hide.target = newTarget; }
            if (config.position.viewport === TRUE) { config.position.viewport = posOptions.container; }
            posOptions.container = posOptions.container.eq(0); posOptions.at = new PLUGINS.Corner(posOptions.at); posOptions.my = new PLUGINS.Corner(posOptions.my); if ($.data(this, 'qtip')) {
                if (config.overwrite) { elem.qtip('destroy'); }
                else if (config.overwrite === FALSE) { return FALSE; }
            }
            if (config.suppress && (title = $.attr(this, 'title'))) { $(this).removeAttr('title').attr(oldtitle, title); }
            obj = new QTip(elem, config, id, !!attr); $.data(this, 'qtip', obj); elem.bind('remove.qtip-' + id + ' removeqtip.qtip-' + id, function () { obj.destroy(); }); return obj;
        }
        QTIP = $.fn.qtip = function (options, notation, newValue) {
            var command = ('' + options).toLowerCase(), returned = NULL, args = $.makeArray(arguments).slice(1), event = args[args.length - 1], opts = this[0] ? $.data(this[0], 'qtip') : NULL; if ((!arguments.length && opts) || command === 'api') { return opts; }
            else if ('string' === typeof options) {
                this.each(function () {
                    var api = $.data(this, 'qtip'); if (!api) { return TRUE; }
                    if (event && event.timeStamp) { api.cache.event = event; }
                    if ((command === 'option' || command === 'options') && notation) {
                        if ($.isPlainObject(notation) || newValue !== undefined) { api.set(notation, newValue); }
                        else { returned = api.get(notation); return FALSE; }
                    }
                    else if (api[command]) { api[command].apply(api[command], args); }
                }); return returned !== NULL ? returned : this;
            }
            else if ('object' === typeof options || !arguments.length)
            { opts = sanitizeOptions($.extend(TRUE, {}, options)); return QTIP.bind.call(this, opts, event); }
        }; QTIP.bind = function (opts, event) {
            return this.each(function (i) {
                var options, targets, events, namespace, api, id; id = $.isArray(opts.id) ? opts.id[i] : opts.id; id = !id || id === FALSE || id.length < 1 || usedIDs[id] ? QTIP.nextid++ : (usedIDs[id] = id); namespace = '.qtip-' + id + '-create'; api = init.call(this, id, opts); if (api === FALSE) { return TRUE; }
                options = api.options; $.each(PLUGINS, function () { if (this.initialize === 'initialize') { this(api); } }); targets = { show: options.show.target, hide: options.hide.target }; events = { show: $.trim('' + options.show.event).replace(/ /g, namespace + ' ') + namespace, hide: $.trim('' + options.hide.event).replace(/ /g, namespace + ' ') + namespace }; if (/mouse(over|enter)/i.test(events.show) && !/mouse(out|leave)/i.test(events.hide)) { events.hide += ' mouseleave' + namespace; }
                targets.show.bind('mousemove' + namespace, function (event) { MOUSE = { pageX: event.pageX, pageY: event.pageY, type: 'mousemove' }; api.cache.onTarget = TRUE; }); function hoverIntent(event) {
                    function render() { api.render(typeof event === 'object' || options.show.ready); targets.show.add(targets.hide).unbind(namespace); }
                    if (api.cache.disabled) { return FALSE; }
                    api.cache.event = $.extend({}, event); api.cache.target = event ? $(event.target) : [undefined]; if (options.show.delay > 0) { clearTimeout(api.timers.show); api.timers.show = setTimeout(render, options.show.delay); if (events.show !== events.hide) { targets.hide.bind(events.hide, function () { clearTimeout(api.timers.show); }); } }
                    else { render(); }
                }
                targets.show.bind(events.show, hoverIntent); if (options.show.ready || options.prerender) { hoverIntent(event); }
            });
        }; PLUGINS = QTIP.plugins = {
            Corner: function (corner) { corner = ('' + corner).replace(/([A-Z])/, ' $1').replace(/middle/gi, 'center').toLowerCase(); this.x = (corner.match(/left|right/i) || corner.match(/center/) || ['inherit'])[0].toLowerCase(); this.y = (corner.match(/top|bottom|center/i) || ['inherit'])[0].toLowerCase(); var f = corner.charAt(0); this.precedance = (f === 't' || f === 'b' ? 'y' : 'x'); this.string = function () { return this.precedance === 'y' ? this.y + this.x : this.x + this.y; }; this.abbrev = function () { var x = this.x.substr(0, 1), y = this.y.substr(0, 1); return x === y ? x : (x === 'c' || (x !== 'c' && y !== 'c')) ? y + x : x + y; }; this.clone = function () { return { x: this.x, y: this.y, precedance: this.precedance, string: this.string, abbrev: this.abbrev, clone: this.clone }; }; }, offset: function (elem, container) {
                var pos = elem.offset(), docBody = elem.closest('body')[0], parent = container, scrolled, coffset, overflow; function scroll(e, i) { pos.left += i * e.scrollLeft(); pos.top += i * e.scrollTop(); }
                if (parent) {
                    do { if (parent.css('position') !== 'static') { coffset = parent.position(); pos.left -= coffset.left + (parseInt(parent.css('borderLeftWidth'), 10) || 0) + (parseInt(parent.css('marginLeft'), 10) || 0); pos.top -= coffset.top + (parseInt(parent.css('borderTopWidth'), 10) || 0) + (parseInt(parent.css('marginTop'), 10) || 0); if (!scrolled && (overflow = parent.css('overflow')) !== 'hidden' && overflow !== 'visible') { scrolled = parent; } } }
                    while ((parent = $(parent[0].offsetParent)).length); if (scrolled && scrolled[0] !== docBody) { scroll(scrolled, 1); }
                }
                return pos;
            }, iOS: parseFloat(('' + (/CPU.*OS ([0-9_]{1,3})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ''])[1]).replace('undefined', '3_2').replace('_', '.')) || FALSE, fn: {
                attr: function (attr, val) {
                    if (this.length) {
                        var self = this[0], title = 'title', api = $.data(self, 'qtip'); if (attr === title && api && 'object' === typeof api && api.options.suppress) {
                            if (arguments.length < 2) { return $.attr(self, oldtitle); }
                            else {
                                if (api && api.options.content.attr === title && api.cache.attr) { api.set('content.text', val); }
                                return this.attr(oldtitle, val);
                            }
                        }
                    }
                    return $.fn['attr' + replaceSuffix].apply(this, arguments);
                }, clone: function (keepData) {
                    var titles = $([]), title = 'title', elems = $.fn['clone' + replaceSuffix].apply(this, arguments); if (!keepData) { elems.filter('[' + oldtitle + ']').attr('title', function () { return $.attr(this, oldtitle); }).removeAttr(oldtitle); }
                    return elems;
                }
            }
        }; $.each(PLUGINS.fn, function (name, func) {
            if (!func || $.fn[name + replaceSuffix]) { return TRUE; }
            var old = $.fn[name + replaceSuffix] = $.fn[name]; $.fn[name] = function () { return func.apply(this, arguments) || old.apply(this, arguments); };
        }); if (!$.ui) {
            $['cleanData' + replaceSuffix] = $.cleanData; $.cleanData = function (elems) {
                for (var i = 0, elem; (elem = elems[i]) !== undefined; i++) {
                    try { $(elem).triggerHandler('removeqtip'); }
                    catch (e) { }
                }
                $['cleanData' + replaceSuffix](elems);
            };
        }
        QTIP.version = 'nightly'; QTIP.nextid = 0; QTIP.inactiveEvents = 'click dblclick mousedown mouseup mousemove mouseleave mouseenter'.split(' '); QTIP.zindex = 15000; QTIP.defaults = { prerender: FALSE, id: FALSE, overwrite: TRUE, suppress: TRUE, content: { text: TRUE, attr: 'title', title: { text: FALSE, button: FALSE } }, position: { my: 'top left', at: 'bottom right', target: FALSE, container: FALSE, viewport: FALSE, adjust: { x: 0, y: 0, mouse: TRUE, resize: TRUE, method: 'flip flip' }, effect: function (api, pos, viewport) { $(this).animate(pos, { duration: 200, queue: FALSE }); } }, show: { target: FALSE, event: 'mouseenter', effect: TRUE, delay: 90, solo: FALSE, ready: FALSE, autofocus: FALSE }, hide: { target: FALSE, event: 'mouseleave', effect: TRUE, delay: 0, fixed: FALSE, inactive: FALSE, leave: 'window', distance: FALSE }, style: { classes: '', widget: FALSE, width: FALSE, height: FALSE, def: TRUE }, events: { render: NULL, move: NULL, show: NULL, hide: NULL, toggle: NULL, visible: NULL, hidden: NULL, focus: NULL, blur: NULL } }; function calculateTip(corner, width, height)
        { var width2 = Math.ceil(width / 2), height2 = Math.ceil(height / 2), tips = { bottomright: [[0, 0], [width, height], [width, 0]], bottomleft: [[0, 0], [width, 0], [0, height]], topright: [[0, height], [width, 0], [width, height]], topleft: [[0, 0], [0, height], [width, height]], topcenter: [[0, height], [width2, 0], [width, height]], bottomcenter: [[0, 0], [width, 0], [width2, height]], rightcenter: [[0, 0], [width, height2], [0, height]], leftcenter: [[width, 0], [width, height], [0, height2]] }; tips.lefttop = tips.bottomright; tips.righttop = tips.bottomleft; tips.leftbottom = tips.topright; tips.rightbottom = tips.topleft; return tips[corner.string()]; }
        function Tip(qTip, command) {
            var self = this, opts = qTip.options.style.tip, elems = qTip.elements, tooltip = elems.tooltip, cache = { top: 0, left: 0 }, size = { width: opts.width, height: opts.height }, color = {}, border = opts.border || 0, namespace = '.qtip-tip', hasCanvas = !!($('<canvas />')[0] || {}).getContext; self.corner = NULL; self.mimic = NULL; self.border = border; self.offset = opts.offset; self.size = size; qTip.checks.tip = {
                '^position.my|style.tip.(corner|mimic|border)$': function () {
                    if (!self.init()) { self.destroy(); }
                    qTip.reposition();
                }, '^style.tip.(height|width)$': function () { size = { width: opts.width, height: opts.height }; self.create(); self.update(); qTip.reposition(); }, '^content.title.text|style.(classes|widget)$': function () { if (elems.tip && elems.tip.length) { self.update(); } }
            }; function swapDimensions() { var temp = size.width; size.width = size.height; size.height = temp; }
            function resetDimensions() { size.width = opts.width; size.height = opts.height; }
            function reposition(event, api, pos, viewport) {
                if (!elems.tip) { return; }
                var newCorner = self.corner.clone(), adjust = pos.adjusted, method = qTip.options.position.adjust.method.split(' '), horizontal = method[0], vertical = method[1] || method[0], shift = { left: FALSE, top: FALSE, x: 0, y: 0 }, offset, css = {}, props; if (self.corner.fixed !== TRUE) {
                    if (horizontal === 'shift' && newCorner.precedance === 'x' && adjust.left && newCorner.y !== 'center') { newCorner.precedance = newCorner.precedance === 'x' ? 'y' : 'x'; }
                    else if (horizontal !== 'shift' && adjust.left) { newCorner.x = newCorner.x === 'center' ? (adjust.left > 0 ? 'left' : 'right') : (newCorner.x === 'left' ? 'right' : 'left'); }
                    if (vertical === 'shift' && newCorner.precedance === 'y' && adjust.top && newCorner.x !== 'center') { newCorner.precedance = newCorner.precedance === 'y' ? 'x' : 'y'; }
                    else if (vertical !== 'shift' && adjust.top) { newCorner.y = newCorner.y === 'center' ? (adjust.top > 0 ? 'top' : 'bottom') : (newCorner.y === 'top' ? 'bottom' : 'top'); }
                    if (newCorner.string() !== cache.corner.string() && (cache.top !== adjust.top || cache.left !== adjust.left)) { self.update(newCorner, FALSE); }
                }
                offset = self.position(newCorner, adjust); if (offset.right !== undefined) { offset.left = -offset.right; }
                if (offset.bottom !== undefined) { offset.top = -offset.bottom; }
                offset.user = Math.max(0, opts.offset); if (shift.left = (horizontal === 'shift' && !!adjust.left)) {
                    if (newCorner.x === 'center') { css['margin-left'] = shift.x = offset['margin-left'] - adjust.left; }
                    else {
                        props = offset.right !== undefined ? [adjust.left, -offset.left] : [-adjust.left, offset.left]; if ((shift.x = Math.max(props[0], props[1])) > props[0]) { pos.left -= adjust.left; shift.left = FALSE; }
                        css[offset.right !== undefined ? 'right' : 'left'] = shift.x;
                    }
                }
                if (shift.top = (vertical === 'shift' && !!adjust.top)) {
                    if (newCorner.y === 'center') { css['margin-top'] = shift.y = offset['margin-top'] - adjust.top; }
                    else {
                        props = offset.bottom !== undefined ? [adjust.top, -offset.top] : [-adjust.top, offset.top]; if ((shift.y = Math.max(props[0], props[1])) > props[0]) { pos.top -= adjust.top; shift.top = FALSE; }
                        css[offset.bottom !== undefined ? 'bottom' : 'top'] = shift.y;
                    }
                }
                elems.tip.css(css).toggle(!((shift.x && shift.y) || (newCorner.x === 'center' && shift.y) || (newCorner.y === 'center' && shift.x))); pos.left -= offset.left.charAt ? offset.user : horizontal !== 'shift' || shift.top || !shift.left && !shift.top ? offset.left : 0; pos.top -= offset.top.charAt ? offset.user : vertical !== 'shift' || shift.left || !shift.left && !shift.top ? offset.top : 0; cache.left = adjust.left; cache.top = adjust.top; cache.corner = newCorner.clone();
            }
            function borderWidth(corner, side, backup) { side = !side ? corner[corner.precedance] : side; var isFluid = tooltip.hasClass(fluidClass), isTitleTop = elems.titlebar && corner.y === 'top', elem = isTitleTop ? elems.titlebar : elems.content, css = 'border-' + side + '-width', val; tooltip.addClass(fluidClass); val = parseInt(elem.css(css), 10); val = (backup ? val || parseInt(tooltip.css(css), 10) : val) || 0; tooltip.toggleClass(fluidClass, isFluid); return val; }
            function borderRadius(corner) { var isTitleTop = elems.titlebar && corner.y === 'top', elem = isTitleTop ? elems.titlebar : elems.content, moz = $.browser.mozilla, prefix = moz ? '-moz-' : $.browser.webkit ? '-webkit-' : '', side = corner.y + (moz ? '' : '-') + corner.x, css = prefix + (moz ? 'border-radius-' + side : 'border-' + side + '-radius'); return parseInt(elem.css(css), 10) || parseInt(tooltip.css(css), 10) || 0; }
            function calculateSize(corner) { var y = corner.precedance === 'y', width = size[y ? 'width' : 'height'], height = size[y ? 'height' : 'width'], isCenter = corner.string().indexOf('center') > -1, base = width * (isCenter ? 0.5 : 1), pow = Math.pow, round = Math.round, bigHyp, ratio, result, smallHyp = Math.sqrt(pow(base, 2) + pow(height, 2)), hyp = [(border / base) * smallHyp, (border / height) * smallHyp]; hyp[2] = Math.sqrt(pow(hyp[0], 2) - pow(border, 2)); hyp[3] = Math.sqrt(pow(hyp[1], 2) - pow(border, 2)); bigHyp = smallHyp + hyp[2] + hyp[3] + (isCenter ? 0 : hyp[0]); ratio = bigHyp / smallHyp; result = [round(ratio * height), round(ratio * width)]; return { height: result[y ? 0 : 1], width: result[y ? 1 : 0] }; }
            $.extend(self, {
                init: function () {
                    var enabled = self.detectCorner() && (hasCanvas || $.browser.msie); if (enabled) { self.create(); self.update(); tooltip.unbind(namespace).bind('tooltipmove' + namespace, reposition); }
                    return enabled;
                }, detectCorner: function () {
                    var corner = opts.corner, posOptions = qTip.options.position, at = posOptions.at, my = posOptions.my.string ? posOptions.my.string() : posOptions.my; if (corner === FALSE || (my === FALSE && at === FALSE)) { return FALSE; }
                    else {
                        if (corner === TRUE) { self.corner = new PLUGINS.Corner(my); }
                        else if (!corner.string) { self.corner = new PLUGINS.Corner(corner); self.corner.fixed = TRUE; }
                    }
                    cache.corner = new PLUGINS.Corner(self.corner.string()); return self.corner.string() !== 'centercenter';
                }, detectColours: function (actual) {
                    var i, fill, border, tip = elems.tip.css('cssText', ''), corner = actual || self.corner, precedance = corner[corner.precedance], borderSide = 'border-' + precedance + '-color', borderSideCamel = 'border' + precedance.charAt(0) + precedance.substr(1) + 'Color', invalid = /rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i, backgroundColor = 'background-color', transparent = 'transparent', important = ' !important', bodyBorder = $(document.body).css('color'), contentColour = qTip.elements.content.css('color'), useTitle = elems.titlebar && (corner.y === 'top' || (corner.y === 'center' && tip.position().top + (size.height / 2) + opts.offset < elems.titlebar.outerHeight(1))), colorElem = useTitle ? elems.titlebar : elems.content; tooltip.addClass(fluidClass); color.fill = fill = tip.css(backgroundColor); color.border = border = tip[0].style[borderSideCamel] || tip.css(borderSide) || tooltip.css(borderSide); if (!fill || invalid.test(fill)) { color.fill = colorElem.css(backgroundColor) || transparent; if (invalid.test(color.fill)) { color.fill = tooltip.css(backgroundColor) || fill; } }
                    if (!border || invalid.test(border) || border === bodyBorder) { color.border = colorElem.css(borderSide) || transparent; if (invalid.test(color.border)) { color.border = border; } }
                    $('*', tip).add(tip).css('cssText', backgroundColor + ':' + transparent + important + ';border:0' + important + ';'); tooltip.removeClass(fluidClass);
                }, create: function () {
                    var width = size.width, height = size.height, vml; if (elems.tip) { elems.tip.remove(); }
                    elems.tip = $('<div />', { 'class': 'ui-tooltip-tip' }).css({ width: width, height: height }).prependTo(tooltip); if (hasCanvas) { $('<canvas />').appendTo(elems.tip)[0].getContext('2d').save(); }
                    else { vml = '<vml:shape coordorigin="0,0" style="display:inline-block; position:absolute; behavior:url(#default#VML);"></vml:shape>'; elems.tip.html(vml + vml); $('*', elems.tip).bind('click mousedown', function (event) { event.stopPropagation(); }); }
                }, update: function (corner, position) {
                    var tip = elems.tip, inner = tip.children(), width = size.width, height = size.height, regular = 'px solid ', transparent = 'px dashed transparent', mimic = opts.mimic, round = Math.round, precedance, context, coords, translate, newSize; if (!corner) { corner = cache.corner || self.corner; }
                    if (mimic === FALSE) { mimic = corner; }
                    else {
                        mimic = new PLUGINS.Corner(mimic); mimic.precedance = corner.precedance; if (mimic.x === 'inherit') { mimic.x = corner.x; }
                        else if (mimic.y === 'inherit') { mimic.y = corner.y; }
                        else if (mimic.x === mimic.y) { mimic[corner.precedance] = corner[corner.precedance]; }
                    }
                    precedance = mimic.precedance; if (corner.precedance === 'x') { swapDimensions(); }
                    else { resetDimensions(); }
                    elems.tip.css({ width: (width = size.width), height: (height = size.height) }); self.detectColours(corner); if (color.border !== 'transparent' && color.border !== '#123456') {
                        border = borderWidth(corner, NULL, TRUE); if (opts.border === 0 && border > 0) { color.fill = color.border; }
                        self.border = border = opts.border !== TRUE ? opts.border : border;
                    }
                    else { self.border = border = 0; }
                    coords = calculateTip(mimic, width, height); self.size = newSize = calculateSize(corner); tip.css(newSize); if (corner.precedance === 'y') { translate = [round(mimic.x === 'left' ? border : mimic.x === 'right' ? newSize.width - width - border : (newSize.width - width) / 2), round(mimic.y === 'top' ? newSize.height - height : 0)]; }
                    else { translate = [round(mimic.x === 'left' ? newSize.width - width : 0), round(mimic.y === 'top' ? border : mimic.y === 'bottom' ? newSize.height - height - border : (newSize.height - height) / 2)]; }
                    if (hasCanvas) {
                        inner.attr(newSize); context = inner[0].getContext('2d'); context.restore(); context.save(); context.clearRect(0, 0, 3000, 3000); context.translate(translate[0], translate[1]); context.beginPath(); context.moveTo(coords[0][0], coords[0][1]); context.lineTo(coords[1][0], coords[1][1]); context.lineTo(coords[2][0], coords[2][1]); context.closePath(); context.fillStyle = color.fill; context.strokeStyle = color.border; context.lineWidth = border * 2; context.lineJoin = 'miter'; context.miterLimit = 100; if (border) { context.stroke(); }
                        context.fill();
                    }
                    else { coords = 'm' + coords[0][0] + ',' + coords[0][1] + ' l' + coords[1][0] + ',' + coords[1][1] + ' ' + coords[2][0] + ',' + coords[2][1] + ' xe'; translate[2] = border && /^(r|b)/i.test(corner.string()) ? parseFloat($.browser.version, 10) === 8 ? 2 : 1 : 0; inner.css({ antialias: '' + (mimic.string().indexOf('center') > -1), left: translate[0] - (translate[2] * Number(precedance === 'x')), top: translate[1] - (translate[2] * Number(precedance === 'y')), width: width + border, height: height + border }).each(function (i) { var $this = $(this); $this[$this.prop ? 'prop' : 'attr']({ coordsize: (width + border) + ' ' + (height + border), path: coords, fillcolor: color.fill, filled: !!i, stroked: !!!i }).css({ display: border || i ? 'block' : 'none' }); if (!i && $this.html() === '') { $this.html('<vml:stroke weight="' + (border * 2) + 'px" color="' + color.border + '" miterlimit="1000" joinstyle="miter" ' + ' style="behavior:url(#default#VML); display:inline-block;" />'); } }); }
                    if (position !== FALSE) { self.position(corner); }
                }, position: function (corner) {
                    var tip = elems.tip, position = {}, userOffset = Math.max(0, opts.offset), precedance, dimensions, corners; if (opts.corner === FALSE || !tip) { return FALSE; }
                    corner = corner || self.corner; precedance = corner.precedance; dimensions = calculateSize(corner); corners = [corner.x, corner.y]; if (precedance === 'x') { corners.reverse(); }
                    $.each(corners, function (i, side) {
                        var b, br; if (side === 'center') { b = precedance === 'y' ? 'left' : 'top'; position[b] = '50%'; position['margin-' + b] = -Math.round(dimensions[precedance === 'y' ? 'width' : 'height'] / 2) + userOffset; }
                        else { b = borderWidth(corner, side, TRUE); br = borderRadius(corner); position[side] = i ? border ? borderWidth(corner, side) : 0 : userOffset + (br > b ? br : 0); }
                    }); position[corner[precedance]] -= dimensions[precedance === 'x' ? 'width' : 'height']; tip.css({ top: '', bottom: '', left: '', right: '', margin: '' }).css(position); return position;
                }, destroy: function () {
                    if (elems.tip) { elems.tip.remove(); }
                    elems.tip = false; tooltip.unbind(namespace);
                }
            }); self.init();
        }
        PLUGINS.tip = function (api)
        { var self = api.plugins.tip; return 'object' === typeof self ? self : (api.plugins.tip = new Tip(api)); }; PLUGINS.tip.initialize = 'render'; PLUGINS.tip.sanitize = function (options) {
            var style = options.style, opts; if (style && 'tip' in style) {
                opts = options.style.tip; if (typeof opts !== 'object') { options.style.tip = { corner: opts }; }
                if (!(/string|boolean/i).test(typeof opts.corner)) { opts.corner = TRUE; }
                if (typeof opts.width !== 'number') { delete opts.width; }
                if (typeof opts.height !== 'number') { delete opts.height; }
                if (typeof opts.border !== 'number' && opts.border !== TRUE) { delete opts.border; }
                if (typeof opts.offset !== 'number') { delete opts.offset; }
            }
        }; $.extend(TRUE, QTIP.defaults, { style: { tip: { corner: TRUE, mimic: FALSE, width: 6, height: 6, border: TRUE, offset: 0 } } });
    })); var hasTouch = 'ontouchstart' in document.documentElement, startEvent = hasTouch ? 'touchstart' : 'mousedown', moveEvent = hasTouch ? 'touchmove' : 'mousemove', endEvent = hasTouch ? 'touchend' : 'mouseup'; jQuery.tableDnD = {
        currentTable: null, dragObject: null, mouseOffset: null, oldY: 0, build: function (options) {
            this.each(function () { this.tableDnDConfig = jQuery.extend({ onDragStyle: null, onDropStyle: null, onDragClass: "tDnD_whileDrag", onDrop: null, onDragStart: null, scrollAmount: 5, serializeRegexp: /[^\-]*$/, serializeParamName: null, dragHandle: null }, options || {}); jQuery.tableDnD.makeDraggable(this); }); return this;
        }, makeDraggable: function (table) { var config = table.tableDnDConfig; if (config.dragHandle) { var cells = jQuery(table.tableDnDConfig.dragHandle, table); cells.each(function () { jQuery(this).bind(startEvent, function (ev) { jQuery.tableDnD.initialiseDrag(jQuery(this).parents('tr')[0], table, this, ev, config); return false; }); }) } else { var rows = jQuery("tr", table); rows.each(function () { var row = jQuery(this); if (!row.hasClass("nodrag")) { row.bind(startEvent, function (ev) { if (ev.target.tagName == "TD") { jQuery.tableDnD.initialiseDrag(this, table, this, ev, config); return false; } }).css("cursor", "move"); } }); } }, initialiseDrag: function (dragObject, table, target, evnt, config) { jQuery.tableDnD.dragObject = dragObject; jQuery.tableDnD.currentTable = table; jQuery.tableDnD.mouseOffset = jQuery.tableDnD.getMouseOffset(target, evnt); jQuery.tableDnD.originalOrder = jQuery.tableDnD.serialize(); jQuery(document).bind(moveEvent, jQuery.tableDnD.mousemove).bind(endEvent, jQuery.tableDnD.mouseup); if (config.onDragStart) { config.onDragStart(table, target); } }, updateTables: function () { this.each(function () { if (this.tableDnDConfig) { jQuery.tableDnD.makeDraggable(this); } }) }, mouseCoords: function (ev) {
            if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
            return { x: ev.clientX + document.body.scrollLeft - document.body.clientLeft, y: ev.clientY + document.body.scrollTop - document.body.clientTop };
        }, getMouseOffset: function (target, ev) { ev = ev || window.event; var docPos = this.getPosition(target); var mousePos = this.mouseCoords(ev); return { x: mousePos.x - docPos.x, y: mousePos.y - docPos.y }; }, getPosition: function (e) {
            var left = 0; var top = 0; if (e.offsetHeight == 0) { e = e.firstChild; }
            while (e.offsetParent) { left += e.offsetLeft; top += e.offsetTop; e = e.offsetParent; }
            left += e.offsetLeft; top += e.offsetTop; return { x: left, y: top };
        }, mousemove: function (ev) {
            if (jQuery.tableDnD.dragObject == null) { return; }
            if (ev.type == 'touchmove') { event.preventDefault(); }
            var dragObj = jQuery(jQuery.tableDnD.dragObject); var config = jQuery.tableDnD.currentTable.tableDnDConfig; var mousePos = jQuery.tableDnD.mouseCoords(ev); var y = mousePos.y - jQuery.tableDnD.mouseOffset.y; var yOffset = window.pageYOffset; if (document.all) {
                if (typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') { yOffset = document.documentElement.scrollTop; }
                else if (typeof document.body != 'undefined') { yOffset = document.body.scrollTop; }
            }
            if (mousePos.y - yOffset < config.scrollAmount) { window.scrollBy(0, -config.scrollAmount); } else { var windowHeight = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight; if (windowHeight - (mousePos.y - yOffset) < config.scrollAmount) { window.scrollBy(0, config.scrollAmount); } }
            if (y != jQuery.tableDnD.oldY) {
                var movingDown = y > jQuery.tableDnD.oldY; jQuery.tableDnD.oldY = y; if (config.onDragClass) { dragObj.addClass(config.onDragClass); } else { dragObj.css(config.onDragStyle); }
                var currentRow = jQuery.tableDnD.findDropTargetRow(dragObj, y); if (currentRow) { if (movingDown && jQuery.tableDnD.dragObject != currentRow) { jQuery.tableDnD.dragObject.parentNode.insertBefore(jQuery.tableDnD.dragObject, currentRow.nextSibling); } else if (!movingDown && jQuery.tableDnD.dragObject != currentRow) { jQuery.tableDnD.dragObject.parentNode.insertBefore(jQuery.tableDnD.dragObject, currentRow); } }
            }
            return false;
        }, findDropTargetRow: function (draggedRow, y) {
            var rows = jQuery.tableDnD.currentTable.rows; for (var i = 0; i < rows.length; i++) {
                var row = rows[i]; var rowY = this.getPosition(row).y; var rowHeight = parseInt(row.offsetHeight) / 2; if (row.offsetHeight == 0) { rowY = this.getPosition(row.firstChild).y; rowHeight = parseInt(row.firstChild.offsetHeight) / 2; }
                if ((y > rowY - rowHeight) && (y < (rowY + rowHeight))) {
                    if (row == draggedRow) { return null; }
                    var config = jQuery.tableDnD.currentTable.tableDnDConfig; if (config.onAllowDrop) { if (config.onAllowDrop(draggedRow, row)) { return row; } else { return null; } } else { var nodrop = jQuery(row).hasClass("nodrop"); if (!nodrop) { return row; } else { return null; } }
                    return row;
                }
            }
            return null;
        }, mouseup: function (e) {
            if (jQuery.tableDnD.currentTable && jQuery.tableDnD.dragObject) {
                jQuery(document).unbind(moveEvent, jQuery.tableDnD.mousemove).unbind(endEvent, jQuery.tableDnD.mouseup); var droppedRow = jQuery.tableDnD.dragObject; var config = jQuery.tableDnD.currentTable.tableDnDConfig; if (config.onDragClass) { jQuery(droppedRow).removeClass(config.onDragClass); } else { jQuery(droppedRow).css(config.onDropStyle); }
                jQuery.tableDnD.dragObject = null; var newOrder = jQuery.tableDnD.serialize(); if (config.onDrop && (jQuery.tableDnD.originalOrder != newOrder)) { config.onDrop(jQuery.tableDnD.currentTable, droppedRow); }
                jQuery.tableDnD.currentTable = null;
            }
        }, serialize: function () { if (jQuery.tableDnD.currentTable) { return jQuery.tableDnD.serializeTable(jQuery.tableDnD.currentTable); } else { return "Error: No Table id set, you need to set an id on your table and every row"; } }, serializeTable: function (table) {
            var result = ""; var tableId = table.id; var rows = table.rows; for (var i = 0; i < rows.length; i++) {
                if (result.length > 0) result += "&"; var rowId = rows[i].id; if (rowId && rowId && table.tableDnDConfig && table.tableDnDConfig.serializeRegexp) { rowId = rowId.match(table.tableDnDConfig.serializeRegexp)[0]; }
                result += tableId + '[]=' + rowId;
            }
            return result;
        }, serializeTables: function () { var result = ""; this.each(function () { result += jQuery.tableDnD.serializeTable(this); }); return result; }
    }; jQuery.fn.extend({ tableDnD: jQuery.tableDnD.build, tableDnDUpdate: jQuery.tableDnD.updateTables, tableDnDSerialize: jQuery.tableDnD.serializeTables }); jQuery.easing['jswing'] = jQuery.easing['swing']; jQuery.extend(jQuery.easing, {
        def: 'easeOutQuad', swing: function (x, t, b, c, d) { return jQuery.easing[jQuery.easing.def](x, t, b, c, d); }, easeInQuad: function (x, t, b, c, d) { return c * (t /= d) * t + b; }, easeOutQuad: function (x, t, b, c, d) { return -c * (t /= d) * (t - 2) + b; }, easeInOutQuad: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t + b; return -c / 2 * ((--t) * (t - 2) - 1) + b; }, easeInCubic: function (x, t, b, c, d) { return c * (t /= d) * t * t + b; }, easeOutCubic: function (x, t, b, c, d) { return c * ((t = t / d - 1) * t * t + 1) + b; }, easeInOutCubic: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t + b; return c / 2 * ((t -= 2) * t * t + 2) + b; }, easeInQuart: function (x, t, b, c, d) { return c * (t /= d) * t * t * t + b; }, easeOutQuart: function (x, t, b, c, d) { return -c * ((t = t / d - 1) * t * t * t - 1) + b; }, easeInOutQuart: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b; return -c / 2 * ((t -= 2) * t * t * t - 2) + b; }, easeInQuint: function (x, t, b, c, d) { return c * (t /= d) * t * t * t * t + b; }, easeOutQuint: function (x, t, b, c, d) { return c * ((t = t / d - 1) * t * t * t * t + 1) + b; }, easeInOutQuint: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b; return c / 2 * ((t -= 2) * t * t * t * t + 2) + b; }, easeInSine: function (x, t, b, c, d) { return -c * Math.cos(t / d * (Math.PI / 2)) + c + b; }, easeOutSine: function (x, t, b, c, d) { return c * Math.sin(t / d * (Math.PI / 2)) + b; }, easeInOutSine: function (x, t, b, c, d) { return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b; }, easeInExpo: function (x, t, b, c, d) { return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b; }, easeOutExpo: function (x, t, b, c, d) { return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b; }, easeInOutExpo: function (x, t, b, c, d) { if (t == 0) return b; if (t == d) return b + c; if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b; return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b; }, easeInCirc: function (x, t, b, c, d) { return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b; }, easeOutCirc: function (x, t, b, c, d) { return c * Math.sqrt(1 - (t = t / d - 1) * t) + b; }, easeInOutCirc: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b; return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b; }, easeInElastic: function (x, t, b, c, d) {
            var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3; if (a < Math.abs(c)) { a = c; var s = p / 4; }
            else var s = p / (2 * Math.PI) * Math.asin(c / a); return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        }, easeOutElastic: function (x, t, b, c, d) {
            var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3; if (a < Math.abs(c)) { a = c; var s = p / 4; }
            else var s = p / (2 * Math.PI) * Math.asin(c / a); return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
        }, easeInOutElastic: function (x, t, b, c, d) {
            var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d / 2) == 2) return b + c; if (!p) p = d * (.3 * 1.5); if (a < Math.abs(c)) { a = c; var s = p / 4; }
            else var s = p / (2 * Math.PI) * Math.asin(c / a); if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b; return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
        }, easeInBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; return c * (t /= d) * t * ((s + 1) * t - s) + b; }, easeOutBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b; }, easeInOutBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b; return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b; }, easeInBounce: function (x, t, b, c, d) { return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b; }, easeOutBounce: function (x, t, b, c, d) { if ((t /= d) < (1 / 2.75)) { return c * (7.5625 * t * t) + b; } else if (t < (2 / 2.75)) { return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b; } else if (t < (2.5 / 2.75)) { return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b; } else { return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b; } }, easeInOutBounce: function (x, t, b, c, d) { if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b; return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b; }
    }); (function () {
        var root = this; var previousUnderscore = root._; var breaker = {}; var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype; var slice = ArrayProto.slice, unshift = ArrayProto.unshift, toString = ObjProto.toString, hasOwnProperty = ObjProto.hasOwnProperty; var
        nativeForEach = ArrayProto.forEach, nativeMap = ArrayProto.map, nativeReduce = ArrayProto.reduce, nativeReduceRight = ArrayProto.reduceRight, nativeFilter = ArrayProto.filter, nativeEvery = ArrayProto.every, nativeSome = ArrayProto.some, nativeIndexOf = ArrayProto.indexOf, nativeLastIndexOf = ArrayProto.lastIndexOf, nativeIsArray = Array.isArray, nativeKeys = Object.keys, nativeBind = FuncProto.bind; var _ = function (obj) { return new wrapper(obj); }; if (typeof exports !== 'undefined') {
            if (typeof module !== 'undefined' && module.exports) { exports = module.exports = _; }
            exports._ = _;
        } else { root['_'] = _; }
        _.VERSION = '1.3.1'; var each = _.each = _.forEach = function (obj, iterator, context) { if (obj == null) return; if (nativeForEach && obj.forEach === nativeForEach) { obj.forEach(iterator, context); } else if (obj.length === +obj.length) { for (var i = 0, l = obj.length; i < l; i++) { if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) return; } } else { for (var key in obj) { if (_.has(obj, key)) { if (iterator.call(context, obj[key], key, obj) === breaker) return; } } } }; _.map = _.collect = function (obj, iterator, context) { var results = []; if (obj == null) return results; if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context); each(obj, function (value, index, list) { results[results.length] = iterator.call(context, value, index, list); }); if (obj.length === +obj.length) results.length = obj.length; return results; }; _.reduce = _.foldl = _.inject = function (obj, iterator, memo, context) {
            var initial = arguments.length > 2; if (obj == null) obj = []; if (nativeReduce && obj.reduce === nativeReduce) { if (context) iterator = _.bind(iterator, context); return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator); }
            each(obj, function (value, index, list) { if (!initial) { memo = value; initial = true; } else { memo = iterator.call(context, memo, value, index, list); } }); if (!initial) throw new TypeError('Reduce of empty array with no initial value'); return memo;
        }; _.reduceRight = _.foldr = function (obj, iterator, memo, context) {
            var initial = arguments.length > 2; if (obj == null) obj = []; if (nativeReduceRight && obj.reduceRight === nativeReduceRight) { if (context) iterator = _.bind(iterator, context); return initial ? obj.reduceRight(iterator, memo) : obj.reduceRight(iterator); }
            var reversed = _.toArray(obj).reverse(); if (context && !initial) iterator = _.bind(iterator, context); return initial ? _.reduce(reversed, iterator, memo, context) : _.reduce(reversed, iterator);
        }; _.find = _.detect = function (obj, iterator, context) { var result; any(obj, function (value, index, list) { if (iterator.call(context, value, index, list)) { result = value; return true; } }); return result; }; _.filter = _.select = function (obj, iterator, context) { var results = []; if (obj == null) return results; if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context); each(obj, function (value, index, list) { if (iterator.call(context, value, index, list)) results[results.length] = value; }); return results; }; _.reject = function (obj, iterator, context) { var results = []; if (obj == null) return results; each(obj, function (value, index, list) { if (!iterator.call(context, value, index, list)) results[results.length] = value; }); return results; }; _.every = _.all = function (obj, iterator, context) { var result = true; if (obj == null) return result; if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context); each(obj, function (value, index, list) { if (!(result = result && iterator.call(context, value, index, list))) return breaker; }); return result; }; var any = _.some = _.any = function (obj, iterator, context) { iterator || (iterator = _.identity); var result = false; if (obj == null) return result; if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context); each(obj, function (value, index, list) { if (result || (result = iterator.call(context, value, index, list))) return breaker; }); return !!result; }; _.include = _.contains = function (obj, target) { var found = false; if (obj == null) return found; if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1; found = any(obj, function (value) { return value === target; }); return found; }; _.invoke = function (obj, method) { var args = slice.call(arguments, 2); return _.map(obj, function (value) { return (_.isFunction(method) ? method || value : value[method]).apply(value, args); }); }; _.pluck = function (obj, key) { return _.map(obj, function (value) { return value[key]; }); }; _.max = function (obj, iterator, context) { if (!iterator && _.isArray(obj)) return Math.max.apply(Math, obj); if (!iterator && _.isEmpty(obj)) return -Infinity; var result = { computed: -Infinity }; each(obj, function (value, index, list) { var computed = iterator ? iterator.call(context, value, index, list) : value; computed >= result.computed && (result = { value: value, computed: computed }); }); return result.value; }; _.min = function (obj, iterator, context) { if (!iterator && _.isArray(obj)) return Math.min.apply(Math, obj); if (!iterator && _.isEmpty(obj)) return Infinity; var result = { computed: Infinity }; each(obj, function (value, index, list) { var computed = iterator ? iterator.call(context, value, index, list) : value; computed < result.computed && (result = { value: value, computed: computed }); }); return result.value; }; _.shuffle = function (obj) { var shuffled = [], rand; each(obj, function (value, index, list) { if (index == 0) { shuffled[0] = value; } else { rand = Math.floor(Math.random() * (index + 1)); shuffled[index] = shuffled[rand]; shuffled[rand] = value; } }); return shuffled; }; _.sortBy = function (obj, iterator, context) { return _.pluck(_.map(obj, function (value, index, list) { return { value: value, criteria: iterator.call(context, value, index, list) }; }).sort(function (left, right) { var a = left.criteria, b = right.criteria; return a < b ? -1 : a > b ? 1 : 0; }), 'value'); }; _.groupBy = function (obj, val) { var result = {}; var iterator = _.isFunction(val) ? val : function (obj) { return obj[val]; }; each(obj, function (value, index) { var key = iterator(value, index); (result[key] || (result[key] = [])).push(value); }); return result; }; _.sortedIndex = function (array, obj, iterator) {
            iterator || (iterator = _.identity); var low = 0, high = array.length; while (low < high) { var mid = (low + high) >> 1; iterator(array[mid]) < iterator(obj) ? low = mid + 1 : high = mid; }
            return low;
        }; _.toArray = function (iterable) { if (!iterable) return []; if (iterable.toArray) return iterable.toArray(); if (_.isArray(iterable)) return slice.call(iterable); if (_.isArguments(iterable)) return slice.call(iterable); return _.values(iterable); }; _.size = function (obj) { return _.toArray(obj).length; }; _.first = _.head = function (array, n, guard) { return (n != null) && !guard ? slice.call(array, 0, n) : array[0]; }; _.initial = function (array, n, guard) { return slice.call(array, 0, array.length - ((n == null) || guard ? 1 : n)); }; _.last = function (array, n, guard) { if ((n != null) && !guard) { return slice.call(array, Math.max(array.length - n, 0)); } else { return array[array.length - 1]; } }; _.rest = _.tail = function (array, index, guard) { return slice.call(array, (index == null) || guard ? 1 : index); }; _.compact = function (array) { return _.filter(array, function (value) { return !!value; }); }; _.flatten = function (array, shallow) { return _.reduce(array, function (memo, value) { if (_.isArray(value)) return memo.concat(shallow ? value : _.flatten(value)); memo[memo.length] = value; return memo; }, []); }; _.without = function (array) { return _.difference(array, slice.call(arguments, 1)); }; _.uniq = _.unique = function (array, isSorted, iterator) {
            var initial = iterator ? _.map(array, iterator) : array; var result = []; _.reduce(initial, function (memo, el, i) {
                if (0 == i || (isSorted === true ? _.last(memo) != el : !_.include(memo, el))) { memo[memo.length] = el; result[result.length] = array[i]; }
                return memo;
            }, []); return result;
        }; _.union = function () { return _.uniq(_.flatten(arguments, true)); }; _.intersection = _.intersect = function (array) { var rest = slice.call(arguments, 1); return _.filter(_.uniq(array), function (item) { return _.every(rest, function (other) { return _.indexOf(other, item) >= 0; }); }); }; _.difference = function (array) { var rest = _.flatten(slice.call(arguments, 1)); return _.filter(array, function (value) { return !_.include(rest, value); }); }; _.zip = function () { var args = slice.call(arguments); var length = _.max(_.pluck(args, 'length')); var results = new Array(length); for (var i = 0; i < length; i++) results[i] = _.pluck(args, "" + i); return results; }; _.indexOf = function (array, item, isSorted) {
            if (array == null) return -1; var i, l; if (isSorted) { i = _.sortedIndex(array, item); return array[i] === item ? i : -1; }
            if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item); for (i = 0, l = array.length; i < l; i++) if (i in array && array[i] === item) return i; return -1;
        }; _.lastIndexOf = function (array, item) { if (array == null) return -1; if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) return array.lastIndexOf(item); var i = array.length; while (i--) if (i in array && array[i] === item) return i; return -1; }; _.range = function (start, stop, step) {
            if (arguments.length <= 1) { stop = start || 0; start = 0; }
            step = arguments[2] || 1; var len = Math.max(Math.ceil((stop - start) / step), 0); var idx = 0; var range = new Array(len); while (idx < len) { range[idx++] = start; start += step; }
            return range;
        }; var ctor = function () { }; _.bind = function bind(func, context) { var bound, args; if (func.bind === nativeBind && nativeBind) return nativeBind.apply(func, slice.call(arguments, 1)); if (!_.isFunction(func)) throw new TypeError; args = slice.call(arguments, 2); return bound = function () { if (!(this instanceof bound)) return func.apply(context, args.concat(slice.call(arguments))); ctor.prototype = func.prototype; var self = new ctor; var result = func.apply(self, args.concat(slice.call(arguments))); if (Object(result) === result) return result; return self; }; }; _.bindAll = function (obj) { var funcs = slice.call(arguments, 1); if (funcs.length == 0) funcs = _.functions(obj); each(funcs, function (f) { obj[f] = _.bind(obj[f], obj); }); return obj; }; _.memoize = function (func, hasher) { var memo = {}; hasher || (hasher = _.identity); return function () { var key = hasher.apply(this, arguments); return _.has(memo, key) ? memo[key] : (memo[key] = func.apply(this, arguments)); }; }; _.delay = function (func, wait) { var args = slice.call(arguments, 2); return setTimeout(function () { return func.apply(func, args); }, wait); }; _.defer = function (func) { return _.delay.apply(_, [func, 1].concat(slice.call(arguments, 1))); }; _.throttle = function (func, wait) {
            var context, args, timeout, throttling, more; var whenDone = _.debounce(function () { more = throttling = false; }, wait); return function () {
                context = this; args = arguments; var later = function () { timeout = null; if (more) func.apply(context, args); whenDone(); }; if (!timeout) timeout = setTimeout(later, wait); if (throttling) { more = true; } else { func.apply(context, args); }
                whenDone(); throttling = true;
            };
        }; _.debounce = function (func, wait) { var timeout; return function () { var context = this, args = arguments; var later = function () { timeout = null; func.apply(context, args); }; clearTimeout(timeout); timeout = setTimeout(later, wait); }; }; _.once = function (func) { var ran = false, memo; return function () { if (ran) return memo; ran = true; return memo = func.apply(this, arguments); }; }; _.wrap = function (func, wrapper) { return function () { var args = [func].concat(slice.call(arguments, 0)); return wrapper.apply(this, args); }; }; _.compose = function () {
            var funcs = arguments; return function () {
                var args = arguments; for (var i = funcs.length - 1; i >= 0; i--) { args = [funcs[i].apply(this, args)]; }
                return args[0];
            };
        }; _.after = function (times, func) { if (times <= 0) return func(); return function () { if (--times < 1) { return func.apply(this, arguments); } }; }; _.keys = nativeKeys || function (obj) { if (obj !== Object(obj)) throw new TypeError('Invalid object'); var keys = []; for (var key in obj) if (_.has(obj, key)) keys[keys.length] = key; return keys; }; _.values = function (obj) { return _.map(obj, _.identity); }; _.functions = _.methods = function (obj) {
            var names = []; for (var key in obj) { if (_.isFunction(obj[key])) names.push(key); }
            return names.sort();
        }; _.extend = function (obj) { each(slice.call(arguments, 1), function (source) { for (var prop in source) { obj[prop] = source[prop]; } }); return obj; }; _.defaults = function (obj) { each(slice.call(arguments, 1), function (source) { for (var prop in source) { if (obj[prop] == null) obj[prop] = source[prop]; } }); return obj; }; _.clone = function (obj) { if (!_.isObject(obj)) return obj; return _.isArray(obj) ? obj.slice() : _.extend({}, obj); }; _.tap = function (obj, interceptor) { interceptor(obj); return obj; }; function eq(a, b, stack) {
            if (a === b) return a !== 0 || 1 / a == 1 / b; if (a == null || b == null) return a === b; if (a._chain) a = a._wrapped; if (b._chain) b = b._wrapped; if (a.isEqual && _.isFunction(a.isEqual)) return a.isEqual(b); if (b.isEqual && _.isFunction(b.isEqual)) return b.isEqual(a); var className = toString.call(a); if (className != toString.call(b)) return false; switch (className) { case '[object String]': return a == String(b); case '[object Number]': return a != +a ? b != +b : (a == 0 ? 1 / a == 1 / b : a == +b); case '[object Date]': case '[object Boolean]': return +a == +b; case '[object RegExp]': return a.source == b.source && a.global == b.global && a.multiline == b.multiline && a.ignoreCase == b.ignoreCase; }
            if (typeof a != 'object' || typeof b != 'object') return false; var length = stack.length; while (length--) { if (stack[length] == a) return true; }
            stack.push(a); var size = 0, result = true; if (className == '[object Array]') { size = a.length; result = size == b.length; if (result) { while (size--) { if (!(result = size in a == size in b && eq(a[size], b[size], stack))) break; } } } else {
                if ('constructor' in a != 'constructor' in b || a.constructor != b.constructor) return false; for (var key in a) { if (_.has(a, key)) { size++; if (!(result = _.has(b, key) && eq(a[key], b[key], stack))) break; } }
                if (result) {
                    for (key in b) { if (_.has(b, key) && !(size--)) break; }
                    result = !size;
                }
            }
            stack.pop(); return result;
        }
        _.isEqual = function (a, b) { return eq(a, b, []); }; _.isEmpty = function (obj) { if (_.isArray(obj) || _.isString(obj)) return obj.length === 0; for (var key in obj) if (_.has(obj, key)) return false; return true; }; _.isElement = function (obj) { return !!(obj && obj.nodeType == 1); }; _.isArray = nativeIsArray || function (obj) { return toString.call(obj) == '[object Array]'; }; _.isObject = function (obj) { return obj === Object(obj); }; _.isArguments = function (obj) { return toString.call(obj) == '[object Arguments]'; }; if (!_.isArguments(arguments)) { _.isArguments = function (obj) { return !!(obj && _.has(obj, 'callee')); }; }
        _.isFunction = function (obj) { return toString.call(obj) == '[object Function]'; }; _.isString = function (obj) { return toString.call(obj) == '[object String]'; }; _.isNumber = function (obj) { return toString.call(obj) == '[object Number]'; }; _.isNaN = function (obj) { return obj !== obj; }; _.isBoolean = function (obj) { return obj === true || obj === false || toString.call(obj) == '[object Boolean]'; }; _.isDate = function (obj) { return toString.call(obj) == '[object Date]'; }; _.isRegExp = function (obj) { return toString.call(obj) == '[object RegExp]'; }; _.isNull = function (obj) { return obj === null; }; _.isUndefined = function (obj) { return obj === void 0; }; _.has = function (obj, key) { return hasOwnProperty.call(obj, key); }; _.noConflict = function () { root._ = previousUnderscore; return this; }; _.identity = function (value) { return value; }; _.times = function (n, iterator, context) { for (var i = 0; i < n; i++) iterator.call(context, i); }; _.escape = function (string) { return ('' + string).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/\//g, '&#x2F;'); }; _.mixin = function (obj) { each(_.functions(obj), function (name) { addToWrapper(name, _[name] = obj[name]); }); }; var idCounter = 0; _.uniqueId = function (prefix) { var id = idCounter++; return prefix ? prefix + id : id; }; _.templateSettings = { evaluate: /<%([\s\S]+?)%>/g, interpolate: /<%=([\s\S]+?)%>/g, escape: /<%-([\s\S]+?)%>/g }; var noMatch = /.^/; var unescape = function (code) {
            return code.replace(/\\\\/g, '\\').replace(/\\'/g, "'");
        };

        // JavaScript micro-templating, similar to John Resig's implementation.
        // Underscore templating handles arbitrary delimiters, preserves whitespace,
        // and correctly escapes quotes within interpolated code.
        _.template = function (str, data) {
            var c = _.templateSettings;
            var tmpl = 'var __p=[],print=function(){__p.push.apply(__p,arguments);};' +
              'with(obj||{}){__p.push(\'' +
              str.replace(/\\/g, '\\\\')
                 .replace(/'/g, "\\'")
                 .replace(c.escape || noMatch, function (match, code) {
                     return "',_.escape(" + unescape(code) + "),'";
                 })
                 .replace(c.interpolate || noMatch, function (match, code) {
                     return "'," + unescape(code) + ",'";
                 })
                 .replace(c.evaluate || noMatch, function (match, code) {
                     return "');" + unescape(code).replace(/[\r\n\t]/g, ' ') + ";__p.push('";
                 })
                 .replace(/\r/g, '\\r')
                 .replace(/\n/g, '\\n')
                 .replace(/\t/g, '\\t')
                 + "');}return __p.join('');";
            var func = new Function('obj', '_', tmpl);
            if (data) return func(data, _);
            return function (data) {
                return func.call(this, data, _);
            };
        };

        // Add a "chain" function, which will delegate to the wrapper.
        _.chain = function (obj) {
            return _(obj).chain();
        };

        // The OOP Wrapper
        // ---------------

        // If Underscore is called as a function, it returns a wrapped object that
        // can be used OO-style. This wrapper holds altered versions of all the
        // underscore functions. Wrapped objects may be chained.
        var wrapper = function (obj) { this._wrapped = obj; };

        // Expose `wrapper.prototype` as `_.prototype`
        _.prototype = wrapper.prototype;

        // Helper function to continue chaining intermediate results.
        var result = function (obj, chain) {
            return chain ? _(obj).chain() : obj;
        };

        // A method to easily add functions to the OOP wrapper.
        var addToWrapper = function (name, func) {
            wrapper.prototype[name] = function () {
                var args = slice.call(arguments);
                unshift.call(args, this._wrapped);
                return result(func.apply(_, args), this._chain);
            };
        };

        // Add all of the Underscore functions to the wrapper object.
        _.mixin(_);

        // Add all mutator Array functions to the wrapper.
        each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function (name) {
            var method = ArrayProto[name];
            wrapper.prototype[name] = function () {
                var wrapped = this._wrapped;
                method.apply(wrapped, arguments);
                var length = wrapped.length;
                if ((name == 'shift' || name == 'splice') && length === 0) delete wrapped[0];
                return result(wrapped, this._chain);
            };
        });

        // Add all accessor Array functions to the wrapper.
        each(['concat', 'join', 'slice'], function (name) {
            var method = ArrayProto[name];
            wrapper.prototype[name] = function () {
                return result(method.apply(this._wrapped, arguments), this._chain);
            };
        });

        // Start chaining a wrapped Underscore object.
        wrapper.prototype.chain = function () {
            this._chain = true;
            return this;
        };

        // Extracts the result from a wrapped and chained object.
        wrapper.prototype.value = function () {
            return this._wrapped;
        };

    }).call(this);

    // Underscore.string
    // (c) 2010 Esa-Matti Suuronen <esa-matti aet suuronen dot org>
    // Underscore.strings is freely distributable under the terms of the MIT license.
    // Documentation: https://github.com/edtsech/underscore.string
    // Some code is borrowed from MooTools and Alexandru Marasteanu.

    // Version 1.1.4

    (function () {
        // ------------------------- Baseline setup ---------------------------------

        // Establish the root object, "window" in the browser, or "global" on the server.
        var root = this;

        var nativeTrim = String.prototype.trim;

        var parseNumber = function (source) { return source * 1 || 0; };

        function str_repeat(i, m) {
            for (var o = []; m > 0; o[--m] = i);
            return o.join('');
        }

        function defaultToWhiteSpace(characters) {
            if (characters) {
                return _s.escapeRegExp(characters);
            }
            return '\\s';
        }

        var _s = {

            isBlank: function (str) {
                return !!str.match(/^\s*$/);
            },

            capitalize: function (str) {
                return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
            },

            chop: function (str, step) {
                step = step || str.length;
                var arr = [];
                for (var i = 0; i < str.length;) {
                    arr.push(str.slice(i, i + step));
                    i = i + step;
                }
                return arr;
            },

            clean: function (str) {
                return _s.strip(str.replace(/\s+/g, ' '));
            },

            count: function (str, substr) {
                var count = 0, index;
                for (var i = 0; i < str.length;) {
                    index = str.indexOf(substr, i);
                    index >= 0 && count++;
                    i = i + (index >= 0 ? index : 0) + substr.length;
                }
                return count;
            },

            chars: function (str) {
                return str.split('');
            },

            escapeHTML: function (str) {
                return String(str || '').replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
                                      .replace(/"/g, '&quot;').replace(/'/g, "&apos;");
            }, unescapeHTML: function (str) { return String(str || '').replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&apos;/g, "'"); }, escapeRegExp: function (str) { return String(str || '').replace(/([-.*+?^${}()|[\]\/\\])/g, '\\$1'); }, insert: function (str, i, substr) { var arr = str.split(''); arr.splice(i, 0, substr); return arr.join(''); }, includes: function (str, needle) { return str.indexOf(needle) !== -1; }, join: function (sep) {
                sep = String(sep); var str = ""; for (var i = 1; i < arguments.length; i += 1) { str += String(arguments[i]); if (i !== arguments.length - 1) { str += sep; } }
                return str;
            }, lines: function (str) { return str.split("\n"); }, splice: function (str, i, howmany, substr) { var arr = str.split(''); arr.splice(i, howmany, substr); return arr.join(''); }, startsWith: function (str, starts) { return str.length >= starts.length && str.substring(0, starts.length) === starts; }, endsWith: function (str, ends) { return str.length >= ends.length && str.substring(str.length - ends.length) === ends; }, succ: function (str) { var arr = str.split(''); arr.splice(str.length - 1, 1, String.fromCharCode(str.charCodeAt(str.length - 1) + 1)); return arr.join(''); }, titleize: function (str) {
                var arr = str.split(' '), word; for (var i = 0; i < arr.length; i++) { word = arr[i].split(''); if (typeof word[0] !== 'undefined') word[0] = word[0].toUpperCase(); i + 1 === arr.length ? arr[i] = word.join('') : arr[i] = word.join('') + ' '; }
                return arr.join('');
            }, camelize: function (str) { return _s.trim(str).replace(/(\-|_|\s)+(.)?/g, function (match, separator, chr) { return chr ? chr.toUpperCase() : ''; }); }, underscored: function (str) { return _s.trim(str).replace(/([a-z\d])([A-Z]+)/g, '$1_$2').replace(/\-|\s+/g, '_').toLowerCase(); }, dasherize: function (str) { return _s.trim(str).replace(/([a-z\d])([A-Z]+)/g, '$1-$2').replace(/^([A-Z]+)/, '-$1').replace(/\_|\s+/g, '-').toLowerCase(); }, trim: function (str, characters) {
                if (!characters && nativeTrim) { return nativeTrim.call(str); }
                characters = defaultToWhiteSpace(characters); return str.replace(new RegExp('\^[' + characters + ']+|[' + characters + ']+$', 'g'), '');
            }, ltrim: function (str, characters) { characters = defaultToWhiteSpace(characters); return str.replace(new RegExp('\^[' + characters + ']+', 'g'), ''); }, rtrim: function (str, characters) { characters = defaultToWhiteSpace(characters); return str.replace(new RegExp('[' + characters + ']+$', 'g'), ''); }, truncate: function (str, length, truncateStr) { truncateStr = truncateStr || '...'; return str.slice(0, length) + truncateStr; }, words: function (str, delimiter) { delimiter = delimiter || " "; return str.split(delimiter); }, pad: function (str, length, padStr, type) {
                var padding = ''; var padlen = 0; if (!padStr) { padStr = ' '; }
                else if (padStr.length > 1) { padStr = padStr[0]; }
                switch (type) { case "right": padlen = (length - str.length); padding = str_repeat(padStr, padlen); str = str + padding; break; case "both": padlen = (length - str.length); padding = { 'left': str_repeat(padStr, Math.ceil(padlen / 2)), 'right': str_repeat(padStr, Math.floor(padlen / 2)) }; str = padding.left + str + padding.right; break; default: padlen = (length - str.length); padding = str_repeat(padStr, padlen);; str = padding + str; }
                return str;
            }, lpad: function (str, length, padStr) { return _s.pad(str, length, padStr); }, rpad: function (str, length, padStr) { return _s.pad(str, length, padStr, 'right'); }, lrpad: function (str, length, padStr) { return _s.pad(str, length, padStr, 'both'); }, sprintf: function () {
                var i = 0, a, f = arguments[i++], o = [], m, p, c, x, s = ''; while (f) {
                    if (m = /^[^\x25]+/.exec(f)) { o.push(m[0]); }
                    else if (m = /^\x25{2}/.exec(f)) { o.push('%'); }
                    else if (m = /^\x25(?:(\d+)\$)?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(f)) {
                        if (((a = arguments[m[1] || i++]) == null) || (a == undefined)) { throw ('Too few arguments.'); }
                        if (/[^s]/.test(m[7]) && (typeof (a) != 'number')) { throw ('Expecting number but found ' + typeof (a)); }
                        switch (m[7]) { case 'b': a = a.toString(2); break; case 'c': a = String.fromCharCode(a); break; case 'd': a = parseInt(a); break; case 'e': a = m[6] ? a.toExponential(m[6]) : a.toExponential(); break; case 'f': a = m[6] ? parseFloat(a).toFixed(m[6]) : parseFloat(a); break; case 'o': a = a.toString(8); break; case 's': a = ((a = String(a)) && m[6] ? a.substring(0, m[6]) : a); break; case 'u': a = Math.abs(a); break; case 'x': a = a.toString(16); break; case 'X': a = a.toString(16).toUpperCase(); break; }
                        a = (/[def]/.test(m[7]) && m[2] && a >= 0 ? '+' + a : a); c = m[3] ? m[3] == '0' ? '0' : m[3].charAt(1) : ' '; x = m[5] - String(a).length - s.length; p = m[5] ? str_repeat(c, x) : ''; o.push(s + (m[4] ? a + p : p + a));
                    }
                    else { throw ('Huh ?!'); }
                    f = f.substring(m[0].length);
                }
                return o.join('');
            }, toNumber: function (str, decimals) { return parseNumber(parseNumber(str).toFixed(parseNumber(decimals))); }, strRight: function (sourceStr, sep) { var pos = (!sep) ? -1 : sourceStr.indexOf(sep); return (pos != -1) ? sourceStr.slice(pos + sep.length, sourceStr.length) : sourceStr; }, strRightBack: function (sourceStr, sep) { var pos = (!sep) ? -1 : sourceStr.lastIndexOf(sep); return (pos != -1) ? sourceStr.slice(pos + sep.length, sourceStr.length) : sourceStr; }, strLeft: function (sourceStr, sep) { var pos = (!sep) ? -1 : sourceStr.indexOf(sep); return (pos != -1) ? sourceStr.slice(0, pos) : sourceStr; }, strLeftBack: function (sourceStr, sep) { var pos = sourceStr.lastIndexOf(sep); return (pos != -1) ? sourceStr.slice(0, pos) : sourceStr; }
        }; _s.strip = _s.trim; _s.lstrip = _s.ltrim; _s.rstrip = _s.rtrim; _s.center = _s.lrpad; _s.ljust = _s.lpad; _s.rjust = _s.rpad; if (typeof window === 'undefined' && typeof module !== 'undefined') { module.exports = _s; } else if (typeof root._ !== 'undefined') { root._.mixin(_s); } else { root._ = _s; }
    }()); (function () {
        var root = this; var previousBackbone = root.Backbone; var slice = Array.prototype.slice; var splice = Array.prototype.splice; var Backbone; if (typeof exports !== 'undefined') { Backbone = exports; } else { Backbone = root.Backbone = {}; }
        Backbone.VERSION = '0.9.2'; var _ = root._; if (!_ && (typeof require !== 'undefined')) _ = require('underscore'); var $ = root.jQuery || root.Zepto || root.ender; Backbone.setDomLibrary = function (lib) { $ = lib; }; Backbone.noConflict = function () { root.Backbone = previousBackbone; return this; }; Backbone.emulateHTTP = false; Backbone.emulateJSON = false; var eventSplitter = /\s+/; var Events = Backbone.Events = {
            on: function (events, callback, context) {
                var calls, event, node, tail, list; if (!callback) return this; events = events.split(eventSplitter); calls = this._callbacks || (this._callbacks = {}); while (event = events.shift()) { list = calls[event]; node = list ? list.tail : {}; node.next = tail = {}; node.context = context; node.callback = callback; calls[event] = { tail: tail, next: list ? list.next : node }; }
                return this;
            }, off: function (events, callback, context) {
                var event, calls, node, tail, cb, ctx; if (!(calls = this._callbacks)) return; if (!(events || callback || context)) { delete this._callbacks; return this; }
                events = events ? events.split(eventSplitter) : _.keys(calls); while (event = events.shift()) { node = calls[event]; delete calls[event]; if (!node || !(callback || context)) continue; tail = node.tail; while ((node = node.next) !== tail) { cb = node.callback; ctx = node.context; if ((callback && cb !== callback) || (context && ctx !== context)) { this.on(event, cb, ctx); } } }
                return this;
            }, trigger: function (events) {
                var event, node, calls, tail, args, all, rest; if (!(calls = this._callbacks)) return this; all = calls.all; events = events.split(eventSplitter); rest = slice.call(arguments, 1); while (event = events.shift()) {
                    if (node = calls[event]) { tail = node.tail; while ((node = node.next) !== tail) { node.callback.apply(node.context || this, rest); } }
                    if (node = all) { tail = node.tail; args = [event].concat(rest); while ((node = node.next) !== tail) { node.callback.apply(node.context || this, args); } }
                }
                return this;
            }
        }; Events.bind = Events.on; Events.unbind = Events.off;
        var Model = Backbone.Model = function (attributes, options) {
            var defaults; attributes || (attributes = {}); if (options && options.parse) attributes = this.parse(attributes); if (defaults = getValue(this, 'defaults')) { attributes = _.extend({}, defaults, attributes); }
            if (options && options.collection) this.collection = options.collection; this.attributes = {}; this._escapedAttributes = {}; this.cid = _.uniqueId('c'); this.changed = {}; this._silent = {}; this._pending = {}; this.set(attributes, { silent: true }); this.changed = {}; this._silent = {}; this._pending = {}; this._previousAttributes = _.clone(this.attributes); this.initialize.apply(this, arguments);
        }; _.extend(Model.prototype, Events, {
            changed: null, _silent: null, _pending: null, idAttribute: 'id', initialize: function () { }, toJSON: function (options) { return _.clone(this.attributes); }, get: function (attr) { return this.attributes[attr]; }, escape: function (attr) { var html; if (html = this._escapedAttributes[attr]) return html; var val = this.get(attr); return this._escapedAttributes[attr] = _.escape(val == null ? '' : '' + val); }, has: function (attr) { return this.get(attr) != null; }, set: function (key, value, options) {
                var attrs, attr, val; if (_.isObject(key) || key == null) { attrs = key; options = value; } else { attrs = {}; attrs[key] = value; }
                options || (options = {}); if (!attrs) return this; if (attrs instanceof Model) attrs = attrs.attributes; if (options.unset) for (attr in attrs) attrs[attr] = void 0; if (!this._validate(attrs, options)) return false; if (this.idAttribute in attrs) this.id = attrs[this.idAttribute]; var changes = options.changes = {}; var now = this.attributes; var escaped = this._escapedAttributes; var prev = this._previousAttributes || {}; for (attr in attrs) {
                    val = attrs[attr]; if (!_.isEqual(now[attr], val) || (options.unset && _.has(now, attr))) { delete escaped[attr]; (options.silent ? this._silent : changes)[attr] = true; }
                    options.unset ? delete now[attr] : now[attr] = val; if (!_.isEqual(prev[attr], val) || (_.has(now, attr) != _.has(prev, attr))) { this.changed[attr] = val; if (!options.silent) this._pending[attr] = true; } else { delete this.changed[attr]; delete this._pending[attr]; }
                }
                if (!options.silent) this.change(options); return this;
            }, unset: function (attr, options) { (options || (options = {})).unset = true; return this.set(attr, null, options); }, clear: function (options) { (options || (options = {})).unset = true; return this.set(_.clone(this.attributes), options); }, fetch: function (options) { options = options ? _.clone(options) : {}; var model = this; var success = options.success; options.success = function (resp, status, xhr) { if (!model.set(model.parse(resp, xhr), options)) return false; if (success) success(model, resp); }; options.error = Backbone.wrapError(options.error, model, options); return (this.sync || Backbone.sync).call(this, 'read', this, options); }, save: function (key, value, options) {
                var attrs, current; if (_.isObject(key) || key == null) { attrs = key; options = value; } else { attrs = {}; attrs[key] = value; }
                options = options ? _.clone(options) : {}; if (options.wait) { if (!this._validate(attrs, options)) return false; current = _.clone(this.attributes); }
                var silentOptions = _.extend({}, options, { silent: true }); if (attrs && !this.set(attrs, options.wait ? silentOptions : options)) { return false; }
                var model = this; var success = options.success; options.success = function (resp, status, xhr) {
                    var serverAttrs = model.parse(resp, xhr); if (options.wait) { delete options.wait; serverAttrs = _.extend(attrs || {}, serverAttrs); }
                    if (!model.set(serverAttrs, options)) return false; if (success) { success(model, resp); } else { model.trigger('sync', model, resp, options); }
                }; options.error = Backbone.wrapError(options.error, model, options); var method = this.isNew() ? 'create' : 'update'; var xhr = (this.sync || Backbone.sync).call(this, method, this, options); if (options.wait) this.set(current, silentOptions); return xhr;
            }, destroy: function (options) {
                options = options ? _.clone(options) : {}; var model = this; var success = options.success; var triggerDestroy = function () { model.trigger('destroy', model, model.collection, options); }; if (this.isNew()) { triggerDestroy(); return false; }
                options.success = function (resp) { if (options.wait) triggerDestroy(); if (success) { success(model, resp); } else { model.trigger('sync', model, resp, options); } }; options.error = Backbone.wrapError(options.error, model, options); var xhr = (this.sync || Backbone.sync).call(this, 'delete', this, options); if (!options.wait) triggerDestroy(); return xhr;
            }, url: function () { var base = getValue(this, 'urlRoot') || getValue(this.collection, 'url') || urlError(); if (this.isNew()) return base; return base + (base.charAt(base.length - 1) == '/' ? '' : '/') + encodeURIComponent(this.id); }, parse: function (resp, xhr) { return resp; }, clone: function () { return new this.constructor(this.attributes); }, isNew: function () { return this.id == null; }, change: function (options) {
                options || (options = {}); var changing = this._changing; this._changing = true; for (var attr in this._silent) this._pending[attr] = true; var changes = _.extend({}, options.changes, this._silent); this._silent = {}; for (var attr in changes) { this.trigger('change:' + attr, this, this.get(attr), options); }
                if (changing) return this; while (!_.isEmpty(this._pending)) {
                    this._pending = {}; this.trigger('change', this, options); for (var attr in this.changed) { if (this._pending[attr] || this._silent[attr]) continue; delete this.changed[attr]; }
                    this._previousAttributes = _.clone(this.attributes);
                }
                this._changing = false; return this;
            }, hasChanged: function (attr) { if (!arguments.length) return !_.isEmpty(this.changed); return _.has(this.changed, attr); }, changedAttributes: function (diff) {
                if (!diff) return this.hasChanged() ? _.clone(this.changed) : false; var val, changed = false, old = this._previousAttributes; for (var attr in diff) { if (_.isEqual(old[attr], (val = diff[attr]))) continue; (changed || (changed = {}))[attr] = val; }
                return changed;
            }, previous: function (attr) { if (!arguments.length || !this._previousAttributes) return null; return this._previousAttributes[attr]; }, previousAttributes: function () { return _.clone(this._previousAttributes); }, isValid: function () { return !this.validate(this.attributes); }, _validate: function (attrs, options) {
                if (options.silent || !this.validate) return true; attrs = _.extend({}, this.attributes, attrs); var error = this.validate(attrs, options); if (!error) return true; if (options && options.error) { options.error(this, error, options); } else { this.trigger('error', this, error, options); }
                return false;
            }
        }); var Collection = Backbone.Collection = function (models, options) { options || (options = {}); if (options.model) this.model = options.model; if (options.comparator) this.comparator = options.comparator; this._reset(); this.initialize.apply(this, arguments); if (models) this.reset(models, { silent: true, parse: options.parse }); }; _.extend(Collection.prototype, Events, {
            model: Model, initialize: function () { }, toJSON: function (options) { return this.map(function (model) { return model.toJSON(options); }); }, add: function (models, options) {
                var i, index, length, model, cid, id, cids = {}, ids = {}, dups = []; options || (options = {}); models = _.isArray(models) ? models.slice() : [models]; for (i = 0, length = models.length; i < length; i++) {
                    if (!(model = models[i] = this._prepareModel(models[i], options))) { throw new Error("Can't add an invalid model to a collection"); }
                    cid = model.cid; id = model.id; if (cids[cid] || this._byCid[cid] || ((id != null) && (ids[id] || this._byId[id]))) { dups.push(i); continue; }
                    cids[cid] = ids[id] = model;
                }
                i = dups.length; while (i--) { models.splice(dups[i], 1); }
                for (i = 0, length = models.length; i < length; i++) { (model = models[i]).on('all', this._onModelEvent, this); this._byCid[model.cid] = model; if (model.id != null) this._byId[model.id] = model; }
                this.length += length; index = options.at != null ? options.at : this.models.length; splice.apply(this.models, [index, 0].concat(models)); if (this.comparator) this.sort({ silent: true }); if (options.silent) return this; for (i = 0, length = this.models.length; i < length; i++) { if (!cids[(model = this.models[i]).cid]) continue; options.index = i; model.trigger('add', model, this, options); }
                return this;
            }, remove: function (models, options) {
                var i, l, index, model; options || (options = {}); models = _.isArray(models) ? models.slice() : [models]; for (i = 0, l = models.length; i < l; i++) {
                    model = this.getByCid(models[i]) || this.get(models[i]); if (!model) continue; delete this._byId[model.id]; delete this._byCid[model.cid]; index = this.indexOf(model); this.models.splice(index, 1); this.length--; if (!options.silent) { options.index = index; model.trigger('remove', model, this, options); }
                    this._removeReference(model);
                }
                return this;
            }, push: function (model, options) { model = this._prepareModel(model, options); this.add(model, options); return model; }, pop: function (options) { var model = this.at(this.length - 1); this.remove(model, options); return model; }, unshift: function (model, options) { model = this._prepareModel(model, options); this.add(model, _.extend({ at: 0 }, options)); return model; }, shift: function (options) { var model = this.at(0); this.remove(model, options); return model; }, get: function (id) { if (id == null) return void 0; return this._byId[id.id != null ? id.id : id]; }, getByCid: function (cid) { return cid && this._byCid[cid.cid || cid]; }, at: function (index) { return this.models[index]; }, where: function (attrs) {
                if (_.isEmpty(attrs)) return []; return this.filter(function (model) {
                    for (var key in attrs) { if (attrs[key] !== model.get(key)) return false; }
                    return true;
                });
            }, sort: function (options) {
                options || (options = {}); if (!this.comparator) throw new Error('Cannot sort a set without a comparator'); var boundComparator = _.bind(this.comparator, this); if (this.comparator.length == 1) { this.models = this.sortBy(boundComparator); } else { this.models.sort(boundComparator); }
                if (!options.silent) this.trigger('reset', this, options); return this;
            }, pluck: function (attr) { return _.map(this.models, function (model) { return model.get(attr); }); }, reset: function (models, options) {
                models || (models = []); options || (options = {}); for (var i = 0, l = this.models.length; i < l; i++) { this._removeReference(this.models[i]); }
                this._reset(); this.add(models, _.extend({ silent: true }, options)); if (!options.silent) this.trigger('reset', this, options); return this;
            }, fetch: function (options) { options = options ? _.clone(options) : {}; if (options.parse === undefined) options.parse = true; var collection = this; var success = options.success; options.success = function (resp, status, xhr) { collection[options.add ? 'add' : 'reset'](collection.parse(resp, xhr), options); if (success) success(collection, resp); }; options.error = Backbone.wrapError(options.error, collection, options); return (this.sync || Backbone.sync).call(this, 'read', this, options); }, create: function (model, options) { var coll = this; options = options ? _.clone(options) : {}; model = this._prepareModel(model, options); if (!model) return false; if (!options.wait) coll.add(model, options); var success = options.success; options.success = function (nextModel, resp, xhr) { if (options.wait) coll.add(nextModel, options); if (success) { success(nextModel, resp); } else { nextModel.trigger('sync', model, resp, options); } }; model.save(null, options); return model; }, parse: function (resp, xhr) { return resp; }, chain: function () { return _(this.models).chain(); }, _reset: function (options) { this.length = 0; this.models = []; this._byId = {}; this._byCid = {}; }, _prepareModel: function (model, options) {
                options || (options = {}); if (!(model instanceof Model)) { var attrs = model; options.collection = this; model = new this.model(attrs, options); if (!model._validate(model.attributes, options)) model = false; } else if (!model.collection) { model.collection = this; }
                return model;
            }, _removeReference: function (model) {
                if (this == model.collection) { delete model.collection; }
                model.off('all', this._onModelEvent, this);
            }, _onModelEvent: function (event, model, collection, options) {
                if ((event == 'add' || event == 'remove') && collection != this) return; if (event == 'destroy') { this.remove(model, options); }
                if (model && event === 'change:' + model.idAttribute) { delete this._byId[model.previous(model.idAttribute)]; this._byId[model.id] = model; }
                this.trigger.apply(this, arguments);
            }
        }); var methods = ['forEach', 'each', 'map', 'reduce', 'reduceRight', 'find', 'detect', 'filter', 'select', 'reject', 'every', 'all', 'some', 'any', 'include', 'contains', 'invoke', 'max', 'min', 'sortBy', 'sortedIndex', 'toArray', 'size', 'first', 'initial', 'rest', 'last', 'without', 'indexOf', 'shuffle', 'lastIndexOf', 'isEmpty', 'groupBy']; _.each(methods, function (method) { Collection.prototype[method] = function () { return _[method].apply(_, [this.models].concat(_.toArray(arguments))); }; }); var Router = Backbone.Router = function (options) { options || (options = {}); if (options.routes) this.routes = options.routes; this._bindRoutes(); this.initialize.apply(this, arguments); }; var namedParam = /:\w+/g; var splatParam = /\*\w+/g; var escapeRegExp = /[-[\]{}()+?.,\\^$|#\s]/g; _.extend(Router.prototype, Events, {
            initialize: function () { }, route: function (route, name, callback) { Backbone.history || (Backbone.history = new History); if (!_.isRegExp(route)) route = this._routeToRegExp(route); if (!callback) callback = this[name]; Backbone.history.route(route, _.bind(function (fragment) { var args = this._extractParameters(route, fragment); callback && callback.apply(this, args); this.trigger.apply(this, ['route:' + name].concat(args)); Backbone.history.trigger('route', this, name, args); }, this)); return this; }, navigate: function (fragment, options) { Backbone.history.navigate(fragment, options); }, _bindRoutes: function () {
                if (!this.routes) return; var routes = []; for (var route in this.routes) { routes.unshift([route, this.routes[route]]); }
                for (var i = 0, l = routes.length; i < l; i++) { this.route(routes[i][0], routes[i][1], this[routes[i][1]]); }
            }, _routeToRegExp: function (route) { route = route.replace(escapeRegExp, '\\$&').replace(namedParam, '([^\/]+)').replace(splatParam, '(.*?)'); return new RegExp('^' + route + '$'); }, _extractParameters: function (route, fragment) { return route.exec(fragment).slice(1); }
        }); var History = Backbone.History = function () { this.handlers = []; _.bindAll(this, 'checkUrl'); }; var routeStripper = /^[#\/]/; var isExplorer = /msie [\w.]+/; History.started = false; _.extend(History.prototype, Events, {
            interval: 50, getHash: function (windowOverride) { var loc = windowOverride ? windowOverride.location : window.location; var match = loc.href.match(/#(.*)$/); return match ? match[1] : ''; }, getFragment: function (fragment, forcePushState) {
                if (fragment == null) { if (this._hasPushState || forcePushState) { fragment = window.location.pathname; var search = window.location.search; if (search) fragment += search; } else { fragment = this.getHash(); } }
                if (!fragment.indexOf(this.options.root)) fragment = fragment.substr(this.options.root.length); return fragment.replace(routeStripper, '');
            }, start: function (options) {
                if (History.started) throw new Error("Backbone.history has already been started"); History.started = true; this.options = _.extend({}, { root: '/' }, this.options, options); this._wantsHashChange = this.options.hashChange !== false; this._wantsPushState = !!this.options.pushState; this._hasPushState = !!(this.options.pushState && window.history && window.history.pushState); var fragment = this.getFragment(); var docMode = document.documentMode; var oldIE = (isExplorer.exec(navigator.userAgent.toLowerCase()) && (!docMode || docMode <= 7)); if (oldIE) { this.iframe = $('<iframe src="javascript:0" tabindex="-1" />').hide().appendTo('body')[0].contentWindow; this.navigate(fragment); }
                if (this._hasPushState) { $(window).bind('popstate', this.checkUrl); } else if (this._wantsHashChange && ('onhashchange' in window) && !oldIE) { $(window).bind('hashchange', this.checkUrl); } else if (this._wantsHashChange) { this._checkUrlInterval = setInterval(this.checkUrl, this.interval); }
                this.fragment = fragment; var loc = window.location; var atRoot = loc.pathname == this.options.root; if (this._wantsHashChange && this._wantsPushState && !this._hasPushState && !atRoot) { this.fragment = this.getFragment(null, true); window.location.replace(this.options.root + '#' + this.fragment); return true; } else if (this._wantsPushState && this._hasPushState && atRoot && loc.hash) { this.fragment = this.getHash().replace(routeStripper, ''); window.history.replaceState({}, document.title, loc.protocol + '//' + loc.host + this.options.root + this.fragment); }
                if (!this.options.silent) { return this.loadUrl(); }
            }, stop: function () { $(window).unbind('popstate', this.checkUrl).unbind('hashchange', this.checkUrl); clearInterval(this._checkUrlInterval); History.started = false; }, route: function (route, callback) { this.handlers.unshift({ route: route, callback: callback }); }, checkUrl: function (e) { var current = this.getFragment(); if (current == this.fragment && this.iframe) current = this.getFragment(this.getHash(this.iframe)); if (current == this.fragment) return false; if (this.iframe) this.navigate(current); this.loadUrl() || this.loadUrl(this.getHash()); }, loadUrl: function (fragmentOverride) { var fragment = this.fragment = this.getFragment(fragmentOverride); var matched = _.any(this.handlers, function (handler) { if (handler.route.test(fragment)) { handler.callback(fragment); return true; } }); return matched; }, navigate: function (fragment, options) {
                if (!History.started) return false; if (!options || options === true) options = { trigger: options }; var frag = (fragment || '').replace(routeStripper, ''); if (this.fragment == frag) return; if (this._hasPushState) { if (frag.indexOf(this.options.root) != 0) frag = this.options.root + frag; this.fragment = frag; window.history[options.replace ? 'replaceState' : 'pushState']({}, document.title, frag); } else if (this._wantsHashChange) {
                    this.fragment = frag; this._updateHash(window.location, frag, options.replace); if (this.iframe && (frag != this.getFragment(this.getHash(this.iframe)))) { if (!options.replace) this.iframe.document.open().close(); this._updateHash(this.iframe.location, frag, options.replace); }
                } else { window.location.assign(this.options.root + fragment); }
                if (options.trigger) this.loadUrl(fragment);
            }, _updateHash: function (location, fragment, replace) { if (replace) { location.replace(location.toString().replace(/(javascript:|#).*$/, '') + '#' + fragment); } else { location.hash = fragment; } }
        }); var View = Backbone.View = function (options) { this.cid = _.uniqueId('view'); this._configure(options || {}); this._ensureElement(); this.initialize.apply(this, arguments); this.delegateEvents(); }; var delegateEventSplitter = /^(\S+)\s*(.*)$/; var viewOptions = ['model', 'collection', 'el', 'id', 'attributes', 'className', 'tagName']; _.extend(View.prototype, Events, {
            tagName: 'div', $: function (selector) { return this.$el.find(selector); }, initialize: function () { }, render: function () { return this; }, remove: function () { this.$el.remove(); return this; }, make: function (tagName, attributes, content) { var el = document.createElement(tagName); if (attributes) $(el).attr(attributes); if (content != null) $(el).html(content); return el; }, setElement: function (element, delegate) { if (this.$el) this.undelegateEvents(); this.$el = (element instanceof $) ? element : $(element); this.el = this.$el[0]; if (delegate !== false) this.delegateEvents(); return this; },
            delegateEvents: function (events) { if (!(events || (events = getValue(this, 'events')))) return; this.undelegateEvents(); for (var key in events) { var method = events[key]; if (!_.isFunction(method)) method = this[events[key]]; if (!method) throw new Error('Method "' + events[key] + '" does not exist'); var match = key.match(delegateEventSplitter); var eventName = match[1], selector = match[2]; method = _.bind(method, this); eventName += '.delegateEvents' + this.cid; if (selector === '') { this.$el.bind(eventName, method); } else { this.$el.delegate(selector, eventName, method); } } }, undelegateEvents: function () { this.$el.unbind('.delegateEvents' + this.cid); }, _configure: function (options) {
                if (this.options) options = _.extend({}, this.options, options); for (var i = 0, l = viewOptions.length; i < l; i++) { var attr = viewOptions[i]; if (options[attr]) this[attr] = options[attr]; }
                this.options = options;
            }, _ensureElement: function () { if (!this.el) { var attrs = getValue(this, 'attributes') || {}; if (this.id) attrs.id = this.id; if (this.className) attrs['class'] = this.className; this.setElement(this.make(this.tagName, attrs), false); } else { this.setElement(this.el, false); } }
        }); var extend = function (protoProps, classProps) { var child = inherits(this, protoProps, classProps); child.extend = this.extend; return child; }; Model.extend = Collection.extend = Router.extend = View.extend = extend; var methodMap = { 'create': 'POST', 'update': 'PUT', 'delete': 'DELETE', 'read': 'GET' }; Backbone.sync = function (method, model, options) {
            var type = methodMap[method]; options || (options = {}); var params = { type: type, dataType: 'json' }; if (!options.url) { params.url = getValue(model, 'url') || urlError(); }
            if (!options.data && model && (method == 'create' || method == 'update')) { params.contentType = 'application/json'; params.data = JSON.stringify(model.toJSON()); }
            if (Backbone.emulateJSON) { params.contentType = 'application/x-www-form-urlencoded'; params.data = params.data ? { model: params.data } : {}; }
            if (Backbone.emulateHTTP) { if (type === 'PUT' || type === 'DELETE') { if (Backbone.emulateJSON) params.data._method = type; params.type = 'POST'; params.beforeSend = function (xhr) { xhr.setRequestHeader('X-HTTP-Method-Override', type); }; } }
            if (params.type !== 'GET' && !Backbone.emulateJSON) { params.processData = false; }
            return $.ajax(_.extend(params, options));
        }; Backbone.wrapError = function (onError, originalModel, options) { return function (model, resp) { resp = model === originalModel ? resp : model; if (onError) { onError(originalModel, resp, options); } else { originalModel.trigger('error', originalModel, resp, options); } }; }; var ctor = function () { }; var inherits = function (parent, protoProps, staticProps) {
            var child; if (protoProps && protoProps.hasOwnProperty('constructor')) { child = protoProps.constructor; } else { child = function () { parent.apply(this, arguments); }; }
            _.extend(child, parent); ctor.prototype = parent.prototype; child.prototype = new ctor(); if (protoProps) _.extend(child.prototype, protoProps); if (staticProps) _.extend(child, staticProps); child.prototype.constructor = child; child.__super__ = parent.prototype; return child;
        }; var getValue = function (object, prop) { if (!(object && object[prop])) return null; return _.isFunction(object[prop]) ? object[prop]() : object[prop]; }; var urlError = function () { throw new Error('A "url" property or function must be specified'); };
    }).call(this); var swfobject = function () { var D = "undefined", r = "object", S = "Shockwave Flash", W = "ShockwaveFlash.ShockwaveFlash", q = "application/x-shockwave-flash", R = "SWFObjectExprInst", x = "onreadystatechange", O = window, j = document, t = navigator, T = false, U = [h], o = [], N = [], I = [], l, Q, E, B, J = false, a = false, n, G, m = true, M = function () { var aa = typeof j.getElementById != D && typeof j.getElementsByTagName != D && typeof j.createElement != D, ah = t.userAgent.toLowerCase(), Y = t.platform.toLowerCase(), ae = Y ? /win/.test(Y) : /win/.test(ah), ac = Y ? /mac/.test(Y) : /mac/.test(ah), af = /webkit/.test(ah) ? parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false, X = !+"\v1", ag = [0, 0, 0], ab = null; if (typeof t.plugins != D && typeof t.plugins[S] == r) { ab = t.plugins[S].description; if (ab && !(typeof t.mimeTypes != D && t.mimeTypes[q] && !t.mimeTypes[q].enabledPlugin)) { T = true; X = false; ab = ab.replace(/^.*\s+(\S+\s+\S+$)/, "$1"); ag[0] = parseInt(ab.replace(/^(.*)\..*$/, "$1"), 10); ag[1] = parseInt(ab.replace(/^.*\.(.*)\s.*$/, "$1"), 10); ag[2] = /[a-zA-Z]/.test(ab) ? parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0 } } else { if (typeof O.ActiveXObject != D) { try { var ad = new ActiveXObject(W); if (ad) { ab = ad.GetVariable("$version"); if (ab) { X = true; ab = ab.split(" ")[1].split(","); ag = [parseInt(ab[0], 10), parseInt(ab[1], 10), parseInt(ab[2], 10)] } } } catch (Z) { } } } return { w3: aa, pv: ag, wk: af, ie: X, win: ae, mac: ac } }(), k = function () { if (!M.w3) { return } if ((typeof j.readyState != D && j.readyState == "complete") || (typeof j.readyState == D && (j.getElementsByTagName("body")[0] || j.body))) { f() } if (!J) { if (typeof j.addEventListener != D) { j.addEventListener("DOMContentLoaded", f, false) } if (M.ie && M.win) { j.attachEvent(x, function () { if (j.readyState == "complete") { j.detachEvent(x, arguments.callee); f() } }); if (O == top) { (function () { if (J) { return } try { j.documentElement.doScroll("left") } catch (X) { setTimeout(arguments.callee, 0); return } f() })() } } if (M.wk) { (function () { if (J) { return } if (!/loaded|complete/.test(j.readyState)) { setTimeout(arguments.callee, 0); return } f() })() } s(f) } }(); function f() { if (J) { return } try { var Z = j.getElementsByTagName("body")[0].appendChild(C("span")); Z.parentNode.removeChild(Z) } catch (aa) { return } J = true; var X = U.length; for (var Y = 0; Y < X; Y++) { U[Y]() } } function K(X) { if (J) { X() } else { U[U.length] = X } } function s(Y) { if (typeof O.addEventListener != D) { O.addEventListener("load", Y, false) } else { if (typeof j.addEventListener != D) { j.addEventListener("load", Y, false) } else { if (typeof O.attachEvent != D) { i(O, "onload", Y) } else { if (typeof O.onload == "function") { var X = O.onload; O.onload = function () { X(); Y() } } else { O.onload = Y } } } } } function h() { if (T) { V() } else { H() } } function V() { var X = j.getElementsByTagName("body")[0]; var aa = C(r); aa.setAttribute("type", q); var Z = X.appendChild(aa); if (Z) { var Y = 0; (function () { if (typeof Z.GetVariable != D) { var ab = Z.GetVariable("$version"); if (ab) { ab = ab.split(" ")[1].split(","); M.pv = [parseInt(ab[0], 10), parseInt(ab[1], 10), parseInt(ab[2], 10)] } } else { if (Y < 10) { Y++; setTimeout(arguments.callee, 10); return } } X.removeChild(aa); Z = null; H() })() } else { H() } } function H() { var ag = o.length; if (ag > 0) { for (var af = 0; af < ag; af++) { var Y = o[af].id; var ab = o[af].callbackFn; var aa = { success: false, id: Y }; if (M.pv[0] > 0) { var ae = c(Y); if (ae) { if (F(o[af].swfVersion) && !(M.wk && M.wk < 312)) { w(Y, true); if (ab) { aa.success = true; aa.ref = z(Y); ab(aa) } } else { if (o[af].expressInstall && A()) { var ai = {}; ai.data = o[af].expressInstall; ai.width = ae.getAttribute("width") || "0"; ai.height = ae.getAttribute("height") || "0"; if (ae.getAttribute("class")) { ai.styleclass = ae.getAttribute("class") } if (ae.getAttribute("align")) { ai.align = ae.getAttribute("align") } var ah = {}; var X = ae.getElementsByTagName("param"); var ac = X.length; for (var ad = 0; ad < ac; ad++) { if (X[ad].getAttribute("name").toLowerCase() != "movie") { ah[X[ad].getAttribute("name")] = X[ad].getAttribute("value") } } P(ai, ah, Y, ab) } else { p(ae); if (ab) { ab(aa) } } } } } else { w(Y, true); if (ab) { var Z = z(Y); if (Z && typeof Z.SetVariable != D) { aa.success = true; aa.ref = Z } ab(aa) } } } } } function z(aa) { var X = null; var Y = c(aa); if (Y && Y.nodeName == "OBJECT") { if (typeof Y.SetVariable != D) { X = Y } else { var Z = Y.getElementsByTagName(r)[0]; if (Z) { X = Z } } } return X } function A() { return !a && F("6.0.65") && (M.win || M.mac) && !(M.wk && M.wk < 312) } function P(aa, ab, X, Z) { a = true; E = Z || null; B = { success: false, id: X }; var ae = c(X); if (ae) { if (ae.nodeName == "OBJECT") { l = g(ae); Q = null } else { l = ae; Q = X } aa.id = R; if (typeof aa.width == D || (!/%$/.test(aa.width) && parseInt(aa.width, 10) < 310)) { aa.width = "310" } if (typeof aa.height == D || (!/%$/.test(aa.height) && parseInt(aa.height, 10) < 137)) { aa.height = "137" } j.title = j.title.slice(0, 47) + " - Flash Player Installation"; var ad = M.ie && M.win ? "ActiveX" : "PlugIn", ac = "MMredirectURL=" + O.location.toString().replace(/&/g, "%26") + "&MMplayerType=" + ad + "&MMdoctitle=" + j.title; if (typeof ab.flashvars != D) { ab.flashvars += "&" + ac } else { ab.flashvars = ac } if (M.ie && M.win && ae.readyState != 4) { var Y = C("div"); X += "SWFObjectNew"; Y.setAttribute("id", X); ae.parentNode.insertBefore(Y, ae); ae.style.display = "none"; (function () { if (ae.readyState == 4) { ae.parentNode.removeChild(ae) } else { setTimeout(arguments.callee, 10) } })() } u(aa, ab, X) } } function p(Y) { if (M.ie && M.win && Y.readyState != 4) { var X = C("div"); Y.parentNode.insertBefore(X, Y); X.parentNode.replaceChild(g(Y), X); Y.style.display = "none"; (function () { if (Y.readyState == 4) { Y.parentNode.removeChild(Y) } else { setTimeout(arguments.callee, 10) } })() } else { Y.parentNode.replaceChild(g(Y), Y) } } function g(ab) { var aa = C("div"); if (M.win && M.ie) { aa.innerHTML = ab.innerHTML } else { var Y = ab.getElementsByTagName(r)[0]; if (Y) { var ad = Y.childNodes; if (ad) { var X = ad.length; for (var Z = 0; Z < X; Z++) { if (!(ad[Z].nodeType == 1 && ad[Z].nodeName == "PARAM") && !(ad[Z].nodeType == 8)) { aa.appendChild(ad[Z].cloneNode(true)) } } } } } return aa } function u(ai, ag, Y) { var X, aa = c(Y); if (M.wk && M.wk < 312) { return X } if (aa) { if (typeof ai.id == D) { ai.id = Y } if (M.ie && M.win) { var ah = ""; for (var ae in ai) { if (ai[ae] != Object.prototype[ae]) { if (ae.toLowerCase() == "data") { ag.movie = ai[ae] } else { if (ae.toLowerCase() == "styleclass") { ah += ' class="' + ai[ae] + '"' } else { if (ae.toLowerCase() != "classid") { ah += " " + ae + '="' + ai[ae] + '"' } } } } } var af = ""; for (var ad in ag) { if (ag[ad] != Object.prototype[ad]) { af += '<param name="' + ad + '" value="' + ag[ad] + '" />' } } aa.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + ah + ">" + af + "</object>"; N[N.length] = ai.id; X = c(ai.id) } else { var Z = C(r); Z.setAttribute("type", q); for (var ac in ai) { if (ai[ac] != Object.prototype[ac]) { if (ac.toLowerCase() == "styleclass") { Z.setAttribute("class", ai[ac]) } else { if (ac.toLowerCase() != "classid") { Z.setAttribute(ac, ai[ac]) } } } } for (var ab in ag) { if (ag[ab] != Object.prototype[ab] && ab.toLowerCase() != "movie") { e(Z, ab, ag[ab]) } } aa.parentNode.replaceChild(Z, aa); X = Z } } return X } function e(Z, X, Y) { var aa = C("param"); aa.setAttribute("name", X); aa.setAttribute("value", Y); Z.appendChild(aa) } function y(Y) { var X = c(Y); if (X && X.nodeName == "OBJECT") { if (M.ie && M.win) { X.style.display = "none"; (function () { if (X.readyState == 4) { b(Y) } else { setTimeout(arguments.callee, 10) } })() } else { X.parentNode.removeChild(X) } } } function b(Z) { var Y = c(Z); if (Y) { for (var X in Y) { if (typeof Y[X] == "function") { Y[X] = null } } Y.parentNode.removeChild(Y) } } function c(Z) { var X = null; try { X = j.getElementById(Z) } catch (Y) { } return X } function C(X) { return j.createElement(X) } function i(Z, X, Y) { Z.attachEvent(X, Y); I[I.length] = [Z, X, Y] } function F(Z) { var Y = M.pv, X = Z.split("."); X[0] = parseInt(X[0], 10); X[1] = parseInt(X[1], 10) || 0; X[2] = parseInt(X[2], 10) || 0; return (Y[0] > X[0] || (Y[0] == X[0] && Y[1] > X[1]) || (Y[0] == X[0] && Y[1] == X[1] && Y[2] >= X[2])) ? true : false } function v(ac, Y, ad, ab) { if (M.ie && M.mac) { return } var aa = j.getElementsByTagName("head")[0]; if (!aa) { return } var X = (ad && typeof ad == "string") ? ad : "screen"; if (ab) { n = null; G = null } if (!n || G != X) { var Z = C("style"); Z.setAttribute("type", "text/css"); Z.setAttribute("media", X); n = aa.appendChild(Z); if (M.ie && M.win && typeof j.styleSheets != D && j.styleSheets.length > 0) { n = j.styleSheets[j.styleSheets.length - 1] } G = X } if (M.ie && M.win) { if (n && typeof n.addRule == r) { n.addRule(ac, Y) } } else { if (n && typeof j.createTextNode != D) { n.appendChild(j.createTextNode(ac + " {" + Y + "}")) } } } function w(Z, X) { if (!m) { return } var Y = X ? "visible" : "hidden"; if (J && c(Z)) { c(Z).style.visibility = Y } else { v("#" + Z, "visibility:" + Y) } } function L(Y) { var Z = /[\\\"<>\.;]/; var X = Z.exec(Y) != null; return X && typeof encodeURIComponent != D ? encodeURIComponent(Y) : Y } var d = function () { if (M.ie && M.win) { window.attachEvent("onunload", function () { var ac = I.length; for (var ab = 0; ab < ac; ab++) { I[ab][0].detachEvent(I[ab][1], I[ab][2]) } var Z = N.length; for (var aa = 0; aa < Z; aa++) { y(N[aa]) } for (var Y in M) { M[Y] = null } M = null; for (var X in swfobject) { swfobject[X] = null } swfobject = null }) } }(); return { registerObject: function (ab, X, aa, Z) { if (M.w3 && ab && X) { var Y = {}; Y.id = ab; Y.swfVersion = X; Y.expressInstall = aa; Y.callbackFn = Z; o[o.length] = Y; w(ab, false) } else { if (Z) { Z({ success: false, id: ab }) } } }, getObjectById: function (X) { if (M.w3) { return z(X) } }, embedSWF: function (ab, ah, ae, ag, Y, aa, Z, ad, af, ac) { var X = { success: false, id: ah }; if (M.w3 && !(M.wk && M.wk < 312) && ab && ah && ae && ag && Y) { w(ah, false); K(function () { ae += ""; ag += ""; var aj = {}; if (af && typeof af === r) { for (var al in af) { aj[al] = af[al] } } aj.data = ab; aj.width = ae; aj.height = ag; var am = {}; if (ad && typeof ad === r) { for (var ak in ad) { am[ak] = ad[ak] } } if (Z && typeof Z === r) { for (var ai in Z) { if (typeof am.flashvars != D) { am.flashvars += "&" + ai + "=" + Z[ai] } else { am.flashvars = ai + "=" + Z[ai] } } } if (F(Y)) { var an = u(aj, am, ah); if (aj.id == ah) { w(ah, true) } X.success = true; X.ref = an } else { if (aa && A()) { aj.data = aa; P(aj, am, ah, ac); return } else { w(ah, true) } } if (ac) { ac(X) } }) } else { if (ac) { ac(X) } } }, switchOffAutoHideShow: function () { m = false }, ua: M, getFlashPlayerVersion: function () { return { major: M.pv[0], minor: M.pv[1], release: M.pv[2] } }, hasFlashPlayerVersion: F, createSWF: function (Z, Y, X) { if (M.w3) { return u(Z, Y, X) } else { return undefined } }, showExpressInstall: function (Z, aa, X, Y) { if (M.w3 && A()) { P(Z, aa, X, Y) } }, removeSWF: function (X) { if (M.w3) { y(X) } }, createCSS: function (aa, Z, Y, X) { if (M.w3) { v(aa, Z, Y, X) } }, addDomLoadEvent: K, addLoadEvent: s, getQueryParamValue: function (aa) { var Z = j.location.search || j.location.hash; if (Z) { if (/\?/.test(Z)) { Z = Z.split("?")[1] } if (aa == null) { return L(Z) } var Y = Z.split("&"); for (var X = 0; X < Y.length; X++) { if (Y[X].substring(0, Y[X].indexOf("=")) == aa) { return L(Y[X].substring((Y[X].indexOf("=") + 1))) } } } return "" }, expressInstallCallback: function () { if (a) { var X = c(R); if (X && l) { X.parentNode.replaceChild(l, X); if (Q) { w(Q, true); if (M.ie && M.win) { l.style.display = "block" } } if (E) { E(B) } } a = false } } } }(); if (typeof JSON !== 'object') { JSON = {}; }
    (function () {
        'use strict'; function f(n) { return n < 10 ? '0' + n : n; }
        if (typeof Date.prototype.toJSON !== 'function') {
            Date.prototype.toJSON = function (key) {
                return isFinite(this.valueOf()) ? this.getUTCFullYear() + '-' +
                f(this.getUTCMonth() + 1) + '-' +
                f(this.getUTCDate()) + 'T' +
                f(this.getUTCHours()) + ':' +
                f(this.getUTCMinutes()) + ':' +
                f(this.getUTCSeconds()) + 'Z' : null;
            }; String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function (key) { return this.valueOf(); };
        }
        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, gap, indent, meta = { '\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"': '\\"', '\\': '\\\\' }, rep; function quote(string) { escapable.lastIndex = 0; return escapable.test(string) ? '"' + string.replace(escapable, function (a) { var c = meta[a]; return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4); }) + '"' : '"' + string + '"'; }
        function str(key, holder) {
            var i, k, v, length, mind = gap, partial, value = holder[key]; if (value && typeof value === 'object' && typeof value.toJSON === 'function') { value = value.toJSON(key); }
            if (typeof rep === 'function') { value = rep.call(holder, key, value); }
            switch (typeof value) {
                case 'string': return quote(value); case 'number': return isFinite(value) ? String(value) : 'null'; case 'boolean': case 'null': return String(value); case 'object': if (!value) { return 'null'; }
                    gap += indent; partial = []; if (Object.prototype.toString.apply(value) === '[object Array]') {
                        length = value.length; for (i = 0; i < length; i += 1) { partial[i] = str(i, value) || 'null'; }
                        v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']'; gap = mind; return v;
                    }
                    if (rep && typeof rep === 'object') { length = rep.length; for (i = 0; i < length; i += 1) { if (typeof rep[i] === 'string') { k = rep[i]; v = str(k, value); if (v) { partial.push(quote(k) + (gap ? ': ' : ':') + v); } } } } else { for (k in value) { if (Object.prototype.hasOwnProperty.call(value, k)) { v = str(k, value); if (v) { partial.push(quote(k) + (gap ? ': ' : ':') + v); } } } }
                    v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}'; gap = mind; return v;
            }
        }
        if (typeof JSON.stringify !== 'function') {
            JSON.stringify = function (value, replacer, space) {
                var i; gap = ''; indent = ''; if (typeof space === 'number') {
                    for (i = 0; i < space; i += 1) { indent += ' '; }
                } else if (typeof space === 'string') { indent = space; }
                rep = replacer; if (replacer && typeof replacer !== 'function' && (typeof replacer !== 'object' || typeof replacer.length !== 'number')) { throw new Error('JSON.stringify'); }
                return str('', { '': value });
            };
        }
        if (typeof JSON.parse !== 'function') {
            JSON.parse = function (text, reviver) {
                var j; function walk(holder, key) {
                    var k, v, value = holder[key]; if (value && typeof value === 'object') { for (k in value) { if (Object.prototype.hasOwnProperty.call(value, k)) { v = walk(value, k); if (v !== undefined) { value[k] = v; } else { delete value[k]; } } } }
                    return reviver.call(holder, key, value);
                }
                text = String(text); cx.lastIndex = 0; if (cx.test(text)) {
                    text = text.replace(cx, function (a) {
                        return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                    });
                }
                if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) { j = eval('(' + text + ')'); return typeof reviver === 'function' ? walk({ '': j }, '') : j; }
                throw new SyntaxError('JSON.parse');
            };
        }
    }());; (function (factory) { if (typeof define === 'function' && define.amd) { define(factory); } else { window.purl = factory(); } })(function () {
        var tag2attr = { a: 'href', img: 'src', form: 'action', base: 'href', script: 'src', iframe: 'src', link: 'href', embed: 'src', object: 'data' }, key = ['source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'fragment'], aliases = { 'anchor': 'fragment' }, parser = { strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/, loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ }, isint = /^[0-9]+$/; function parseUri(url, strictMode) {
            var str = decodeURI(url), res = parser[strictMode || false ? 'strict' : 'loose'].exec(str), uri = { attr: {}, param: {}, seg: {} }, i = 14; while (i--) { uri.attr[key[i]] = res[i] || ''; }
            uri.param['query'] = parseString(uri.attr['query']); uri.param['fragment'] = parseString(uri.attr['fragment']); uri.seg['path'] = uri.attr.path.replace(/^\/+|\/+$/g, '').split('/'); uri.seg['fragment'] = uri.attr.fragment.replace(/^\/+|\/+$/g, '').split('/'); uri.attr['base'] = uri.attr.host ? (uri.attr.protocol ? uri.attr.protocol + '://' + uri.attr.host : uri.attr.host) + (uri.attr.port ? ':' + uri.attr.port : '') : ''; return uri;
        }
        function getAttrName(elm) { var tn = elm.tagName; if (typeof tn !== 'undefined') return tag2attr[tn.toLowerCase()]; return tn; }
        function promote(parent, key) { if (parent[key].length === 0) return parent[key] = {}; var t = {}; for (var i in parent[key]) t[i] = parent[key][i]; parent[key] = t; return t; }
        function parse(parts, parent, key, val) { var part = parts.shift(); if (!part) { if (isArray(parent[key])) { parent[key].push(val); } else if ('object' == typeof parent[key]) { parent[key] = val; } else if ('undefined' == typeof parent[key]) { parent[key] = val; } else { parent[key] = [parent[key], val]; } } else { var obj = parent[key] = parent[key] || []; if (']' == part) { if (isArray(obj)) { if ('' !== val) obj.push(val); } else if ('object' == typeof obj) { obj[keys(obj).length] = val; } else { obj = parent[key] = [parent[key], val]; } } else if (~part.indexOf(']')) { part = part.substr(0, part.length - 1); if (!isint.test(part) && isArray(obj)) obj = promote(parent, key); parse(parts, obj, part, val); } else { if (!isint.test(part) && isArray(obj)) obj = promote(parent, key); parse(parts, obj, part, val); } } }
        function merge(parent, key, val) {
            if (~key.indexOf(']')) { var parts = key.split('['); parse(parts, parent, 'base', val); } else {
                if (!isint.test(key) && isArray(parent.base)) { var t = {}; for (var k in parent.base) t[k] = parent.base[k]; parent.base = t; }
                if (key !== '') { set(parent.base, key, val); }
            }
            return parent;
        }
        function parseString(str) {
            return reduce(String(str).split(/&|;/), function (ret, pair) {
                try { pair = decodeURIComponent(pair.replace(/\+/g, ' ')); } catch (e) { }
                var eql = pair.indexOf('='), brace = lastBraceInKey(pair), key = pair.substr(0, brace || eql), val = pair.substr(brace || eql, pair.length); val = val.substr(val.indexOf('=') + 1, val.length); if (key === '') { key = pair; val = ''; }
                return merge(ret, key, val);
            }, { base: {} }).base;
        }
        function set(obj, key, val) { var v = obj[key]; if (typeof v === 'undefined') { obj[key] = val; } else if (isArray(v)) { v.push(val); } else { obj[key] = [v, val]; } }
        function lastBraceInKey(str) { var len = str.length, brace, c; for (var i = 0; i < len; ++i) { c = str[i]; if (']' == c) brace = false; if ('[' == c) brace = true; if ('=' == c && !brace) return i; } }
        function reduce(obj, accumulator) {
            var i = 0, l = obj.length >> 0, curr = arguments[2]; while (i < l) { if (i in obj) curr = accumulator.call(undefined, curr, obj[i], i, obj); ++i; }
            return curr;
        }
        function isArray(vArg) { return Object.prototype.toString.call(vArg) === "[object Array]"; }
        function keys(obj) {
            var key_array = []; for (var prop in obj) { if (obj.hasOwnProperty(prop)) key_array.push(prop); }
            return key_array;
        }
        function purl(url, strictMode) {
            if (arguments.length === 1 && url === true) { strictMode = true; url = undefined; }
            strictMode = strictMode || false; url = url || window.location.toString(); return { data: parseUri(url, strictMode), attr: function (attr) { attr = aliases[attr] || attr; return typeof attr !== 'undefined' ? this.data.attr[attr] : this.data.attr; }, param: function (param) { return typeof param !== 'undefined' ? this.data.param.query[param] : this.data.param.query; }, fparam: function (param) { return typeof param !== 'undefined' ? this.data.param.fragment[param] : this.data.param.fragment; }, segment: function (seg) { if (typeof seg === 'undefined') { return this.data.seg.path; } else { seg = seg < 0 ? this.data.seg.path.length + seg : seg - 1; return this.data.seg.path[seg]; } }, fsegment: function (seg) { if (typeof seg === 'undefined') { return this.data.seg.fragment; } else { seg = seg < 0 ? this.data.seg.fragment.length + seg : seg - 1; return this.data.seg.fragment[seg]; } } };
        }
        purl.jQuery = function ($) {
            if ($ != null) {
                $.fn.url = function (strictMode) {
                    var url = ''; if (this.length) { url = $(this).attr(getAttrName(this[0])) || ''; }
                    return purl(url, strictMode);
                }; $.url = purl;
            }
        }; purl.jQuery(window.jQuery); return purl;
    }); (function () {
        $.extend($.datepicker, { _checkOffset: function (inst, offset, isFixed) { return offset; } }); $.facebox.settings.opacity = 0.5; $.facebox.settings.closeImage = "/assets/images/blank.png"; $.facebox.settings.loadingImage = "/assets/images/blank.png"; $.fn.delayedFocus = function (delay) {
            if (delay == null) { delay = 10; }
            return this.each(function () { var _this; _this = this; return setTimeout(function () { return _this.focus(); }, delay); });
        }; window.onbeforeunload = function (e) {
            e = e || window.event; if (!window.confirmBeforeUnload) { return; }
            if (e) { e.returnValue = 'You will lose all changes.'; }
            return 'You will lose all changes.';
        }; window.confirmBeforeUnload = true; Object.size = function (obj) {
            var key, size; size = 0; key = void 0; for (key in obj) { if (obj.hasOwnProperty(key)) { size++; } }
            return size;
        }; $.decodedParams = function (object) {
            var decodedObject; decodedObject = {}; if (typeof object !== 'undefined' && _.size(object) > 0) { $.each(object, function (index, item) { return decodedObject[index] = $("<div/>").html(item).text(); }); }
            return decodedObject;
        }; String.prototype.startsWith = function (str) { return this.indexOf(str) === 0; }; String.prototype.nl2br = function (is_xhtml) { var breakTag, str; str = this; breakTag = (is_xhtml || typeof is_xhtml === "undefined" ? "<br />" : "<br>"); return (str + "").replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, "$1" + breakTag + "$2"); }; Number.prototype.format = function (c, d, t) { var i, j, n, s; n = this; c = (isNaN(c = Math.abs(c)) ? 0 : c); d = (d === undefined ? "." : d); t = (t === undefined ? "," : t); s = (n < 0 ? "-" : ""); i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + ""; j = ((j = i.length) > 3 ? j % 3 : 0); return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : ""); };
    }).call(this); (function () {
        window.InvoiceCreator = { Models: {}, Collections: {}, Views: {}, Event: {}, Invoice: {}, Utils: {}, init: function () { this.events = _.clone(Backbone.Events); this.invoice = new InvoiceCreator.Models.Invoice; (new InvoiceCreator.Views.Navigation).render(); (new InvoiceCreator.Views.Contact).render(); (new InvoiceCreator.Views.Company).render(); (new InvoiceCreator.Views.Client).render(); (new InvoiceCreator.Views.Work).render(); (new InvoiceCreator.Views.TotalDate).render(); (new InvoiceCreator.Views.TotalNav).render(); (new InvoiceCreator.Views.Terms).render(); (new InvoiceCreator.Views.Notes).render(); (new InvoiceCreator.Views.Logo).render(); (new InvoiceCreator.Views.Overlay).render(); (new InvoiceCreator.Views.Warlock).render(); (new InvoiceCreator.Views.Funfact).render(); $('[name=company_name]').delayedFocus(); $("[data-highlight]").live('mouseenter', function (event) { var highlightStep; highlightStep = $(event.target).closest('[data-highlight]').data('step'); return InvoiceCreator.events.trigger("addHighlight-" + highlightStep, $(event.target).closest('[data-highlight]')); }); $("[data-highlight]").live('mouseleave', function (event) { var highlightStep; highlightStep = $(event.target).closest('[data-highlight]').data('step'); return InvoiceCreator.events.trigger("removeHighlight-" + highlightStep, $(event.target).closest('[data-highlight]')); }); $("[data-highlight]").live('click', function (event) { var highlightStep; highlightStep = $(event.target).closest('[data-highlight]').data('step'); return InvoiceCreator.events.trigger("clickActivate-" + highlightStep, $(event.target).closest('[data-highlight]')); }); this.resizeOverlayIE(); $(window).resize(this.resizeOverlayIE); return $(document).on('focusin click', '.section input, .section textarea', function (event) { var section, stepNumber; section = $(event.target).closest('.section'); stepNumber = section.data('step'); $('.section').removeClass('focused'); section.addClass('focused'); $('.step', '#navigation').removeClass('focused'); $(".step.step_" + (section.data('step'))).addClass('focused'); if (stepNumber) { return InvoiceCreator.events.trigger("focusSection-" + stepNumber); } }); }, resizeOverlayIE: function () { var invoiceHeight, invoiceWidth; if ($('body.ie7').length > 0) { invoiceHeight = $('#invoice').innerHeight(); invoiceWidth = $('#invoice').innerWidth(); return $('#bg_overlay').css('height', invoiceHeight + 'px').css('width', invoiceWidth + 'px'); } } }; String.prototype.is_email = function () { return this.match(/^[\w\d._%+-]+@[\w\d.-]+\.[\w]{2,4}$/); }; $(document).ready(function () {
            $(document).on('focusin focusout change keypress keyup paste', '.field input, .field textarea, tr.line input, tr.line textarea', function (event) {
                var field, label; field = $(event.target); if (label = field.prev('label')) {
                    switch (event.type) { case 'focusin': label.addClass('focused'); break; case 'focusout': label.removeClass('focused'); }
                    if (field.val()) { return label.addClass('populated'); } else { return label.removeClass('populated'); }
                }
            }); $(document).on('keyup', 'input[data-char-limit]', function (event) { var current_val, field, limit; field = $(event.currentTarget); limit = field.data('char-limit'); current_val = field.val(); if (current_val.length > limit) { return field.val(current_val.substring(0, limit)); } }); $(document).on('focusout', '.section input, .section textarea', function (event) { return $('.section').removeClass('focused'); }); $(document).on('beforeReveal.facebox', function (event) { return $('textarea,input').attr('tabIndex', -1); }); $(document).on('close.facebox', function (event) { $('.video-player').tubeplayer('stop'); $('textarea,input').removeAttr('tabIndex'); return $('label[for],input').qtip('hide'); }); return InvoiceCreator.init();
        });
    }).call(this); (function () {
        InvoiceCreator.Utils.Animator = (function () {
            function Animator(el) { this.el = el; this.total_frames = this.el.data('total-steps'); this.current_step = this.el.data('step'); this.speed = this.el.data('speed'); this.repeat = 1; this.total_repeat = this.el.data('repeat') * (this.total_frames * 2); }
            Animator.prototype.nextFrame = function (el, speed) { var step, total_steps; step = this.el.data("step"); total_steps = this.el.data("total-steps"); return setTimeout((function () { return nextFrame(el, speed); }), speed); }; Animator.prototype.previousFrame = function (el, speed) { var step, total_steps; step = this.el.data("step"); total_steps = this.el.data("total-steps"); return setTimeout((function () { return previousFrame(el, speed); }), speed); }; Animator.prototype.advanceFrame = function (frameNumber, speed) {
                var nextFrame; if (speed == null) { speed = 30; }
                this.el.addClass("step" + frameNumber); nextFrame = frameNumber + 1; this.el.data('step', nextFrame); return this.play(speed);
            }; Animator.prototype.rewindFrame = function (frameNumber, speed) {
                var nextFrame; if (speed == null) { speed = 30; }
                this.el.removeClass("step" + frameNumber); nextFrame = frameNumber - 1; this.el.data('step', nextFrame); return this.rewind(speed);
            }; Animator.prototype.play = function (speed) {
                var _this = this; if (speed == null) { speed = 30; }
                return this.timeout = setTimeout(function () { var currentStep; currentStep = _this.el.data('step'); if (currentStep <= _this.total_frames) { return _this.advanceFrame(currentStep, speed); } }, speed);
            }; Animator.prototype.rewind = function (speed) {
                var _this = this; if (speed == null) { speed = 30; }
                return this.timeout = setTimeout(function () { var currentStep; currentStep = _this.el.data('step'); if (currentStep > 1) { return _this.rewindFrame(currentStep, speed); } }, speed);
            }; Animator.prototype.loopFrames = function () {
                if (this.direction === 'forward') { this.currentStep = this.currentStep + 1; this.el.addClass("step" + this.currentStep); } else if (this.direction === 'back') { this.el.removeClass("step" + this.currentStep); this.currentStep = this.currentStep - 1; } else { this.currentStep = this.currentStep + 1; }
                this.repeat++; this.el.data('step', this.currentStep); if (this.total_repeat) { if (this.repeat < this.total_repeat) { return this.loop(); } } else { return this.loop(); }
            }; Animator.prototype.loop = function (direction) {
                var _this = this; if (direction == null) { direction = 'forward'; }
                this.el.addClass('step1'); return this.timeout = setTimeout(function () { _this.currentStep = _this.el.data('step'); _this.setDirection(); return _this.loopFrames(); }, this.speed);
            }; Animator.prototype.setDirection = function () {
                if (this.direction === 'back' && this.currentStep === 1) { this.direction = 'forward'; }
                if (this.direction === 'forward' && this.currentStep === this.total_frames) { this.direction = 'back'; }
                if (!this.direction) { return this.direction = 'forward'; }
            }; Animator.prototype.reset = function () {
                var frameNumber, _results; this.el.data('step', 1); frameNumber = 1; _results = []; while (frameNumber <= this.total_frames) { this.el.removeClass("step" + frameNumber); _results.push(frameNumber++); }
                return _results;
            }; return Animator;
        })();
    }).call(this); (function () { InvoiceCreator.COUNTRIES = ["United States", "Canada", "United Kingdom", "New Zealand", "South Africa", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Anguilla", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling Islands)", "Colombia", "Comoros", "Cook Islands", "Costa Rica", "Cote D'Ivoire (Ivory Coast)", "Croatia (Hrvatska)", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Egypt", "El Salvador", "Ecuador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Federated States of Micronesia", "Fiji", "Finland", "France", "France, Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and McDonald Islands", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea (North)", "Korea (South)", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Caledonia", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Rwanda", "Reunion", "Romania", "Russian Federation", "Saint-Martin", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Saint Martin", "Slovak Republic", "Slovenia", "Solomon Islands", "Spain", "Sri Lanka", "St. Helena", "Saint Kitts and Nevis", "Saint Lucia", "St. Pierre and Miquelon", "St Vincent and the Grenadines", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "US Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela", "Viet Nam", "Virgin Islands (British)", "Virgin Islands (US)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Zambia"]; }).call(this); (function () { InvoiceCreator.CURRENCIES = { USD: { name: "US dollar", symbol: "$" }, CAD: { name: "Canadian dollar", symbol: "$" }, EUR: { name: "Euro", symbol: "€" }, GBP: { name: "Pound sterling", symbol: "£" }, AUD: { name: "Australian dollar", symbol: "$" }, AED: { name: "United Arab Emirates dirham", symbol: "إ.د" }, AFN: { name: "Afghani", symbol: "؋" }, ALL: { name: "Lek", symbol: "Lek" }, AMD: { name: "Armenian dram", symbol: "դր." }, ANG: { name: "Netherlands Antillian guilder", symbol: "ƒ" }, AOA: { name: "Kwanza", symbol: "Kz" }, ARS: { name: "Argentine peso", symbol: "$" }, AWG: { name: "Aruban guilder", symbol: "ƒ" }, AZN: { name: "Azerbaijanian manat", symbol: "ман" }, BAM: { name: "Convertible marks", symbol: "KM" }, BBD: { name: "Barbados dollar", symbol: "$" }, BDT: { name: "Bangladeshi taka", symbol: "৳" }, BGN: { name: "Bulgarian lev", symbol: "лв" }, BHD: { name: "Bahraini dinar", symbol: ".د.ب" }, BIF: { name: "Burundian franc", symbol: "Fr" }, BMD: { name: "Bermudian dollar", symbol: "$" }, BND: { name: "Brunei dollar", symbol: "$" }, BOB: { name: "Boliviano", symbol: "Bs." }, BRL: { name: "Brazilian real", symbol: "R$" }, BSD: { name: "Bahamian dollar", symbol: "$" }, BTN: { name: "Ngultrum", symbol: "Nu." }, BWP: { name: "Pula", symbol: "P" }, BZD: { name: "Belize dollar", symbol: "BZ$" }, CDF: { name: "Franc Congolais", symbol: "Fr" }, CHF: { name: "Swiss franc", symbol: "CHF" }, CLP: { name: "Chilean peso", symbol: "$" }, CNY: { name: "Renminbi", symbol: "元" }, COP: { name: "Colombian peso", symbol: "$" }, CRC: { name: "Costa Rican colon", symbol: "₡" }, CUP: { name: "Cuban peso", symbol: "₱" }, CVE: { name: "Cape Verde escudo", symbol: "Esc" }, CZK: { name: "Czech koruna", symbol: "Kč" }, DJF: { name: "Djibouti franc" }, DKK: { name: "Danish krone", symbol: "kr" }, DOP: { name: "Dominican peso", symbol: "RD$" }, DZD: { name: "Algerian dinar" }, EEK: { name: "Kroon", symbol: "kr" }, EGP: { name: "Egyptian pound", symbol: "£" }, ERN: { name: "Nakfa", symbol: "Nfk" }, ETB: { name: "Ethiopian birr", symbol: "Br" }, FJD: { name: "Fiji dollar", symbol: "$" }, FKP: { name: "Falkland Islands pound", symbol: "£" }, GEL: { name: "Lari", symbol: "ლ" }, GHS: { name: "Cedi", symbol: "₵" }, GIP: { name: "Gibraltar pound", symbol: "£" }, GMD: { name: "Dalasi", symbol: "D" }, GNF: { name: "Guinea franc", symbol: "Fr" }, GTQ: { name: "Quetzal", symbol: "Q" }, GYD: { name: "Guyana dollar", symbol: "$" }, HKD: { name: "Hong Kong dollar", symbol: "$" }, HNL: { name: "Lempira", symbol: "L" }, HRK: { name: "Croatian kuna", symbol: "kn" }, HTG: { name: "Haiti gourde", symbol: "G" }, HUF: { name: "Forint", symbol: "Ft" }, IDR: { name: "Rupiah", symbol: "Rp" }, ILS: { name: "New Israeli shekel", symbol: "₪" }, INR: { name: "Indian rupee", symbol: "₹" }, IQD: { name: "Iraqi dinar", symbol: "ع.د" }, IRR: { name: "Iranian rial", symbol: "﷼" }, ISK: { name: "Iceland krona", symbol: "kr" }, JMD: { name: "Jamaican dollar", symbol: "J$" }, JOD: { name: "Jordanian dinar", symbol: "د.ا" }, JPY: { name: "Japanese yen", symbol: "¥" }, KES: { name: "Kenyan shilling", symbol: "Sh" }, KGS: { name: "Som", symbol: "лв" }, KHR: { name: "Riel", symbol: "៛" }, KMF: { name: "Comoro franc", symbol: "Fr" }, KPW: { name: "North Korean won", symbol: "₩" }, KRW: { name: "South Korean won", symbol: "₩" }, KWD: { name: "Kuwaiti dinar", symbol: "د.ك" }, KYD: { name: "Cayman Islands dollar", symbol: "$" }, KZT: { name: "Tenge", symbol: "лв" }, LAK: { name: "Kip", symbol: "₭" }, LBP: { name: "Lebanese pound", symbol: "£" }, LKR: { name: "Sri Lanka rupee", symbol: "₨" }, LRD: { name: "Liberian dollar", symbol: "$" }, LSL: { name: "Loti", symbol: "L" }, LTL: { name: "Lithuanian litas", symbol: "Lt" }, LVL: { name: "Latvian lats", symbol: "Ls" }, LYD: { name: "Libyan dinar", symbol: "ل.د" }, MAD: { name: "Moroccan dirham", symbol: "د.م." }, MDL: { name: "Moldovan leu", symbol: "L" }, MGA: { name: "Malagasy ariary", symbol: "Ar" }, MKD: { name: "Denar", symbol: "ден" }, MNT: { name: "Tugrik", symbol: "₮" }, MOP: { name: "Pataca", symbol: "P" }, MRO: { name: "Ouguiya", symbol: "UM" }, MUR: { name: "Mauritius rupee", symbol: "₨" }, MVR: { name: "Rufiyaa", symbol: ".ރ" }, MWK: { name: "Kwacha", symbol: "MK" }, MXN: { name: "Mexican peso", symbol: "$" }, MYR: { name: "Malaysian ringgit", symbol: "RM" }, MZN: { name: "Metical", symbol: "MT" }, NAD: { name: "Namibian dollar", symbol: "$" }, NGN: { name: "Naira", symbol: "₦" }, NIO: { name: "Cordoba oro", symbol: "C$" }, NOK: { name: "Norwegian krone", symbol: "kr" }, NPR: { name: "Nepalese rupee", symbol: "₨" }, NZD: { name: "New Zealand dollar", symbol: "$" }, OMR: { name: "Rial Omani", symbol: "﷼" }, PAB: { name: "Balboa", symbol: "B/." }, PEN: { name: "Nuevo sol", symbol: "S/." }, PGK: { name: "Kina", symbol: "K" }, PHP: { name: "Philippine peso", symbol: "₱" }, PKR: { name: "Pakistan rupee", symbol: "₨" }, PLN: { name: "Zloty", symbol: "zł" }, PYG: { name: "Guarani", symbol: "Gs" }, QAR: { name: "Qatari rial", symbol: "﷼" }, RON: { name: "Romanian new leu", symbol: "lei" }, RSD: { name: "Serbian dinar", symbol: "Дин." }, RUB: { name: "Russian ruble", symbol: "руб" }, RWF: { name: "Rwanda franc", symbol: "Fr" }, SAR: { name: "Saudi riyal", symbol: "﷼" }, SBD: { name: "Solomon Islands dollar" }, SCR: { name: "Seychelles rupee", symbol: "₨" }, SDG: { name: "Sudanese pound", symbol: "£" }, SEK: { name: "Swedish krona", symbol: "kr" }, SGD: { name: "Singapore dollar", symbol: "$" }, SHP: { name: "Saint Helena pound", symbol: "£" }, SKK: { name: "Slovak koruna", symbol: "" }, SLL: { name: "Leone", symbol: "Le" }, SRD: { name: "Surinam dollar", symbol: "$" }, STD: { name: "Dobra", symbol: "Db" }, SYP: { name: "Syrian pound", symbol: "£" }, SZL: { name: "Lilangeni", symbol: "L" }, THB: { name: "Baht", symbol: "฿" }, TJS: { name: "Somoni", symbol: "ЅМ" }, TMT: { name: "Manat", symbol: "m" }, TND: { name: "Tunisian dinar", symbol: "د.ت" }, TOP: { name: "Pa'anga", symbol: "T$" }, TRY: { name: "Turkish Lira", symbol: "" }, TTD: { name: "Trinidad and Tobago dollar", symbol: "TT$" }, TWD: { name: "New Taiwan dollar", symbol: "NT$" }, TZS: { name: "Tanzanian shilling" }, UAH: { name: "Hryvnia", symbol: "₴" }, UGX: { name: "Uganda shilling", symbol: "Sh" }, UYU: { name: "Peso Uruguayo", symbol: "$U" }, UZS: { name: "Uzbekistan som", symbol: "лв" }, VEF: { name: "Venezuelan bolívar fuerte", symbol: "Bs" }, VND: { name: "Vietnamese đồng", symbol: "₫" }, VUV: { name: "Vatu", symbol: "Vt" }, WST: { name: "Samoan tala", system: "T" }, XAF: { name: "CFA franc BEAC", symbol: "Fr" }, XCD: { name: "East Caribbean dollar", symbol: "$" }, XOF: { name: "CFA Franc BCEAO", symbol: "Fr" }, XPF: { name: "CFP franc", symbol: "Fr" }, YER: { name: "Yemeni rial", symbol: "﷼" }, ZAR: { name: "South African rand", symbol: "R" }, ZMK: { name: "Kwacha", symbol: "ZK" } }; }).call(this); (function () { InvoiceCreator.STATES = { "United States": ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District Of Columbia", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming", "Armed Forces Africa", "Armed Forces Americas", "Armed Forces Canada", "Armed Forces Europe", "Armed Forces Middle East", "Armed Forces Pacific", "Puerto Rico"], "Canada": ["Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland And Labrador", "Northwest Territories", "Nova Scotia", "Nunavut", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan", "Yukon"], "Australia": ["Australian Capital Territory", "New South Wales", "Northern Territory", "Queensland", "South Australia", "Tasmania", "Victoria", "Western Australia"] }; }).call(this); (function () { InvoiceCreator.TAXES = { 'Canada': { 'All': { 'GST': { rate: 5 } }, 'Quebec': { 'QST': { rate: 9.5, compound: true } }, 'Prince Edward Island': { 'PST': { rate: 10, compound: true } }, 'British Columbia': { 'HST': { rate: 12 } }, 'Ontario': { 'HST': { rate: 13 } }, 'Nova Scotia': { 'HST': { rate: 13 } }, 'New Brunswick': { 'HST': { rate: 13 } }, 'Newfoundland And Labrador': { 'HST': { rate: 13 } }, 'Manitoba': { 'PST': { rate: 7 } }, 'Saskatchewan': { 'PST': { rate: 5 } } }, 'United Kingdom': { 'All': { 'VAT': { rate: 20 } } }, 'New Zealand': { 'All': { 'GST': { rate: 15 } } }, 'Sweden': { 'All': { 'Moms': { rate: 25 } } }, 'Belgium': { 'All': { 'BTW': { rate: 21 } } }, 'France': { 'All': { 'TVA': { rate: 19.6 } } }, 'Spain': { 'All': { 'IVA': { rate: 16 } } }, 'Italy': { 'All': { 'IVA': { rate: 20 } } }, 'Netherlands': { 'All': { 'BTW': { rate: 19 } } }, 'Germany': { 'All': { 'USt': { rate: 19 }, 'MwSt': { rate: 19 } } }, 'South Africa': { 'All': { 'VAT': { rate: 14 } } }, 'Ireland': { 'All': { 'VAT': { rate: 21 } } }, 'Australia': { 'All': { 'GST': { rate: 10 } } } }; }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Models.Company = (function (_super) {
            __extends(Company, _super); function Company() { return Company.__super__.constructor.apply(this, arguments); }
            Company.prototype.defaults = { 'name': '', 'country': '', 'street': '', 'city': '', 'state': '', 'zipcode': '', 'vat_number': '' }; Company.prototype.initialize = function (attributes) { var decoded_params; decoded_params = $.decodedParams(window.company_params); return this.set(_.extend({}, decoded_params, attributes)); }; Company.prototype.isValid = function () {
                this.errors = {}; if (_.isEmpty(this.get('name'))) { this.errors['name'] = "This can't be empty"; }
                if (_.isEmpty(this.get('country'))) { this.errors['country'] = "This can't be empty"; }
                if (!_.isEmpty(this.get('country'))) { if (!_.include(InvoiceCreator.COUNTRIES, this.get('country'))) { this.errors['country'] = 'Country is invalid'; } }
                if (InvoiceCreator.STATES[this.get('country')] && !_.include(InvoiceCreator.STATES[this.get('country')], this.get('state'))) { this.errors['state'] = 'Oops! This state is invalid'; if (this.get('country') === 'Canada') { this.errors['state'] = 'Oops! This province is invalid'; } }
                return Object.size(this.errors) === 0;
            }; return Company;
        })(Backbone.Model);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Models.Client = (function (_super) {
            __extends(Client, _super); function Client() { return Client.__super__.constructor.apply(this, arguments); }
            Client.prototype.defaults = { 'name': '', 'country': '', 'street': '', 'city': '', 'state': '', 'zipcode': '' }; Client.prototype.isValid = function () {
                this.errors = {}; if (!_.isEmpty(this.get('country'))) { if (!_.include(InvoiceCreator.COUNTRIES, this.get('country'))) { this.errors['country'] = 'Country is invalid'; } }
                if (InvoiceCreator.STATES[this.get('country')] && !_.include(InvoiceCreator.STATES[this.get('country')], this.get('state'))) { this.errors['state'] = 'Oops! This state is invalid'; if (this.get('country') === 'Canada') { this.errors['state'] = 'Oops! This Province is invalid'; } }
                return Object.size(this.errors) === 0;
            }; Client.prototype.validateZip = function () {
                var regex; if (this.get('country') === 'United States') { regex = /^\d{5}(-\d{4})?$/; } else if (this.get('country') === 'Canada') { regex = /^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$/; } else { regex = /.*/; }
                return this.get('zipcode').match(regex) !== null;
            }; return Client;
        })(Backbone.Model);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Models.Invoice = (function (_super) {
            __extends(Invoice, _super); function Invoice() { return Invoice.__super__.constructor.apply(this, arguments); }
            Invoice.prototype.url = '/send'; Invoice.prototype.paramRoot = 'invoice'; Invoice.prototype.defaults = { 'number': '', 'date': '', 'notes': '', 'terms': '', 'currency': '', 'to': '', 'from': '', 'body_header': '', 'body_footer': '', 'vat_number': '', 'remember_me': true }; Invoice.prototype.initialize = function (attributes) { var decoded_params, _this = this; decoded_params = $.decodedParams(window.invoice_params); this.set(_.extend({}, decoded_params, attributes)); this.company = new InvoiceCreator.Models.Company; this.client = new InvoiceCreator.Models.Client; this.lines = new InvoiceCreator.Collections.Lines; this.taxes = new InvoiceCreator.Collections.Taxes; this.cached_total = false; _.each(InvoiceCreator.TAXES, function (taxes, country) { return _.each(taxes, function (tax, state) { return _.each(tax, function (props, name) { return _this.taxes.push(new InvoiceCreator.Models.Tax({ country: country, state: state, name: name, rate: props.rate, compound: props.compound ? true : false })); }); }); }); return this.taxes.add(tax_params); }; Invoice.prototype.subtotal = function () { return _.reduce(this.lines.map(function (line) { return line.subtotal(); }), function (sum, subtotal) { return sum + subtotal; }, 0); }; Invoice.prototype.totalTaxes = function () { var out; out = {}; this.lines.each(function (line) { return _.each(line.taxed(), function (amount, taxCid) { out[taxCid] || (out[taxCid] = 0); return out[taxCid] += Math.round(amount * 100) / 100; }); }); return out; }; Invoice.prototype.total = function () { var tax_total; tax_total = _.reduce(this.totalTaxes(), (function (sum, amount) { return sum + amount; }), 0); return this.subtotal() + tax_total; }; Invoice.prototype.availableTaxes = function () {
                var result, vat_tax; result = new InvoiceCreator.Collections.Taxes(_.union(InvoiceCreator.invoice.taxes.where({ country: this.company.get('country'), state: this.company.get('state') }), InvoiceCreator.invoice.taxes.where({ country: this.company.get('country'), state: 'All' }), InvoiceCreator.invoice.taxes.where({ custom: true }))); result.removeDuplicates(); vat_tax = _.find(result.models, function (item) { return item.get('name') === 'VAT'; }); if (!_.isUndefined(vat_tax)) { if (this.company.get('vat_number')) { vat_tax.set('number', this.company.get('vat_number')); } }
                return result;
            }; Invoice.prototype.isReady = function () {
                if (this.total() < 0) { return false; }
                if (InvoiceCreator.invoice.company.isValid() && InvoiceCreator.invoice.client.isValid()) { return true; } else { return false; }
            }; Invoice.prototype.toJSON = function () { this.set('company', this.company.toJSON()); this.set('client', this.client.toJSON()); this.set('lines', this.lines.toJSON()); this.set('taxes', this.availableTaxes().toJSON()); return _.clone(this.attributes); }; return Invoice;
        })(Backbone.Model);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Models.Line = (function (_super) {
            __extends(Line, _super); function Line() { return Line.__super__.constructor.apply(this, arguments); }
            Line.prototype.defaults = { 'quantity': 1 }; Line.prototype.initialize = function (initial) { if (initial.type !== 'item') { return this.set('quantity', null); } }; Line.prototype.subtotal = function () { return parseFloat(this.get('cost') * this.get('quantity')) || 0; }; Line.prototype.taxed = function () { var out, tax_1, tax_2, taxedSubtotal, taxes, _this = this; tax_1 = this.get('tax_1'); tax_2 = this.get('tax_2'); taxes = tax_1 && tax_1.get('compound') ? [tax_2, tax_1] : [tax_1, tax_2]; out = {}; taxedSubtotal = 0; _.each(_.compact(taxes), function (tax) { var obj; obj = {}; if (tax.get('compound')) { obj[tax.cid] = parseFloat(tax.get('rate')) / 100 * (taxedSubtotal || _this.subtotal()); return _.extend(out, obj); } else { taxedSubtotal = (1 + parseFloat(tax.get('rate')) / 100) * _this.subtotal(); obj[tax.cid] = parseFloat(tax.get('rate')) / 100 * _this.subtotal(); return _.extend(out, obj); } }); return out; }; return Line;
        })(Backbone.Model); InvoiceCreator.Collections.Lines = (function (_super) {
            __extends(Lines, _super); function Lines() { return Lines.__super__.constructor.apply(this, arguments); }
            Lines.prototype.model = InvoiceCreator.Models.Line; Lines.prototype.initialize = function () { this.itemCount = 0; this.timeCount = 0; this.on('add', this.addLine); return this.on('destroy', this.destroyLine); }; Lines.prototype.addLine = function (model) {
                if (model.get('type') === 'item') { this.itemCount++; }
                if (model.get('type') === 'time') { return this.timeCount++; }
            }; Lines.prototype.destroyLine = function (model) {
                if (model.get('type') === 'item') { this.itemCount--; }
                if (model.get('type') === 'time') { return this.timeCount--; }
            }; return Lines;
        })(Backbone.Collection);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Models.Tax = (function (_super) {
            __extends(Tax, _super); function Tax() { return Tax.__super__.constructor.apply(this, arguments); }
            return Tax;
        })(Backbone.Model); InvoiceCreator.Collections.Taxes = (function (_super) {
            __extends(Taxes, _super); function Taxes() { return Taxes.__super__.constructor.apply(this, arguments); }
            Taxes.prototype.model = InvoiceCreator.Models.Tax; Taxes.prototype.removeDuplicates = function () { var index1, _this = this; index1 = 0; return this.each(function (tax1) { var index2; index1++; index2 = 0; return _this.each(function (tax2) { index2++; if (index1 !== index2 && tax1.get('name') === tax2.get('name')) { return _this.remove(tax2); } }); }); }; return Taxes;
        })(Backbone.Collection);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Company = (function (_super) {
            __extends(Company, _super); function Company() { this.updateVatNumber = __bind(this.updateVatNumber, this); this.removeHighlight = __bind(this.removeHighlight, this); this.addHighlight = __bind(this.addHighlight, this); return Company.__super__.constructor.apply(this, arguments); }
            Company.prototype.el = '#company'; Company.prototype.template = JST['company']; Company.prototype.events = { 'click': 'selectFirstInput', 'click .editable.overlay': 'edit', 'click a.done': 'next', 'focus input': 'removeError', 'change input': 'saveAttribute', 'change input[name=country]': 'changePlaceholders' }; Company.prototype.changePlaceholders = function (event) {
                var country, postal_placeholder, show_vat_field, state_placeholder; country = $(event.target).val(); show_vat_field = false; $('#company_state').val(''); this.model.set('state', ''); this.$('label#state_placeholder').removeClass('populated'); switch (country) { case 'Canada': postal_placeholder = 'Postal Code'; state_placeholder = 'Province'; break; case 'United States': postal_placeholder = 'Zip Code'; state_placeholder = 'State'; break; case 'United Kingdom': postal_placeholder = 'Postcode'; state_placeholder = 'County/Region'; show_vat_field = true; this.$el.addClass('show_vat'); break; default: postal_placeholder = 'Zip/Postal Code'; state_placeholder = 'State/Province'; }
                this.$('label#postal_placeholder').html(postal_placeholder); this.$('label#state_placeholder').html(state_placeholder); if (show_vat_field) { this.$el.addClass('show_vat'); return this.$('.field.vat_number').show(); } else { this.$el.removeClass('show_vat'); this.$('.field.vat_number').hide(); this.$('#vat_number').val(''); this.$('label#vat_number_placeholder').removeClass('populated'); return this.model.set('vat_number', ''); }
            }; Company.prototype.initialize = function () { InvoiceCreator.events.on('startStep1', this.enableSection, this); InvoiceCreator.events.on('skipStep1', this.skipStep, this); InvoiceCreator.events.on('disableStep1', this.disableSection, this); InvoiceCreator.events.on('startStep4', this.allowFullAccess, this); InvoiceCreator.events.on('showValidationErrors', this.validateForm, this); InvoiceCreator.events.on('addHighlight-1', this.addHighlight); InvoiceCreator.events.on('removeHighlight-1', this.removeHighlight); InvoiceCreator.events.on('vat_number_changed', this.updateVatNumber, this); this.model = InvoiceCreator.invoice.company; this.warlock = JST['warlock_company']; return this.success = JST['success_message_company']; }; Company.prototype.render = function () {
                var _this = this; this.$el.html(this.template(this.model.attributes)); this.$('input[name=country]').autocomplete({ minLength: 0, close: function (event, ui) { return $('input[name=country]').change(); }, change: function (event, item) { if (!_.include(InvoiceCreator.COUNTRIES, $(event.currentTarget).val())) { return _this.$(event.currentTarget).qtip({ content: { text: 'This country is invalid' }, position: { my: 'right center', at: 'left center' }, show: { ready: true }, style: { classes: 'ui-tooltip-red ui-tooltip-rounded' } }); } }, source: function (req, response) { var matcher, re; re = $.ui.autocomplete.escapeRegex(req.term); matcher = new RegExp("^" + re, "i"); return response($.grep(InvoiceCreator.COUNTRIES, function (item) { return matcher.test(item); })); } }); this.$('input[name=country]').focus(function () { $(this).autocomplete('search', ''); return false; }); if (this.$('input[name=country]').val() !== '') { this.$('input[name=country]').change(); }
                this.$('input').attr('disabled', 'disabled'); this.$el.addClass('disabled'); return this;
            }; Company.prototype.enableSection = function () {
                if (this.$el.hasClass('editable')) { return this.triggerStepCompletion(); } else {
                    this.$el.removeClass('disabled').addClass('enabled').removeClass('focused'); this.$('input').removeAttr('disabled'); if (!this.warlockShown) { this.warlockShown = true; InvoiceCreator.events.trigger('warlock', 'show', this.el, this.warlock()); }
                    this.$('input[name=name]').delayedFocus(); this.$('.flash-wrapper').hide(); return this.$('.action, .submit-wrapper').show();
                }
            }; Company.prototype.disableSection = function () { InvoiceCreator.events.trigger('warlock', 'hide'); this.$el.removeClass('enabled').removeClass('highlight'); this.$('input').attr('disabled', 'disabled'); this.$('.action').hide(); $('input').removeClass('error'); return $('input').qtip('destroy'); }; Company.prototype.allowFullAccess = function () { this.$('input').removeAttr('disabled'); this.$el.removeClass('disabled').removeClass('focused').removeClass('editable'); this.$el.addClass('fullAccess'); return this.fullAccess = true; }; Company.prototype.edit = function () { this.$el.removeClass('editable'); InvoiceCreator.events.trigger('editStep', this.$el.data('step')); return false; }; Company.prototype.validateForm = function () { if (!this.model.isValid()) { return this.validate(); } }; Company.prototype.validate = function (field) {
                var error, name, position, _ref; if (field == null) { field = ''; }
                $('input').removeClass('error'); if (field === '') { $('input').qtip('destroy'); }
                _ref = this.model.errors; for (name in _ref) {
                    error = _ref[name]; if (!(field === '' || field === name)) { return; }
                    if (name === 'state' || name === 'zipcode') { position = { my: 'left center', at: 'right center' }; } else { position = { my: 'right center', at: 'left center' }; }
                    this.$("[name=" + name + "]").qtip({ content: { text: error }, position: position, show: { ready: true }, hide: { event: 'focus' }, style: { classes: 'ui-tooltip-red ui-tooltip-rounded' } }); this.$("[name=" + name + "]").addClass('error');
                }
            }; Company.prototype.next = function (event) {
                if (this.model.isValid()) { this.completeStep(); } else { this.validate(); }
                return false;
            }; Company.prototype.completeStep = function () { var thumbsUp, _this = this; thumbsUp = new InvoiceCreator.Utils.Animator(this.$('.thumbs-up')); thumbsUp.play(70); this.$('.submit-wrapper').fadeOut(50, function () { return _this.$('.flash-wrapper').hide().html(_this.success).fadeIn(50); }); return setTimeout((function () { thumbsUp.rewind(40); return setTimeout((function () { return _this.markAsComplete(); }), 1200); }), 1000); }; Company.prototype.markAsComplete = function () { this.$('input').removeClass('error'); $('input').qtip('destroy'); this.disableSection(); this.$el.removeClass('disabled').addClass('editable'); return this.triggerStepCompletion(); }; Company.prototype.skipStep = function () { if (this.model.isValid()) { return this.markAsComplete(); } else { return this.enableSection(); } }; Company.prototype.saveAttribute = function (event) {
                var errorText, fieldName, fieldTag, fieldValue, stateTag, _this = this; $(event.target).qtip('destroy'); fieldTag = $(event.target); fieldName = fieldTag.attr('name'); fieldValue = $.trim(fieldTag.val()); this.model.set(fieldName, fieldValue); if (fieldName === 'country') {
                    stateTag = this.$('input[name=state]'); if (!this.model.isValid()) { this.validate(fieldName); }
                    if (fieldValue === 'United States' || fieldValue === 'Canada' || fieldValue === 'Australia') {
                        errorText = "Oops! This state is invalid"; if (fieldValue === 'Canada') { errorText = "Oops! This province is invalid"; }
                        if (_.indexOf(InvoiceCreator.STATES[fieldValue], stateTag.val()) === -1) { this.model.set('state', null); stateTag.val(''); stateTag.change(); }
                        stateTag.autocomplete({ minLength: 0, close: function (event, ui) { return $('input[name=state]').change(); }, change: function (event, item) { if (!_.include(InvoiceCreator.STATES[fieldValue], $(event.currentTarget).val())) { return _this.$(event.currentTarget).qtip({ content: { text: errorText }, position: { my: 'left center', at: 'right center' }, show: { ready: true }, style: { classes: 'ui-tooltip-red ui-tooltip-rounded' } }); } }, source: function (req, response) { var matcher, re; re = $.ui.autocomplete.escapeRegex(req.term); matcher = new RegExp("^" + re, "i"); return response($.grep(InvoiceCreator.STATES[fieldValue], function (item) { return matcher.test(item); })); } }); return this.$('input[name=state]').focus(function () { $(this).autocomplete('search', ''); return false; });
                    } else { return stateTag.autocomplete('destroy'); }
                } else if (this.fullAccess) { if (!this.model.isValid()) { return this.validate(fieldName); } }
            }; Company.prototype.triggerStepCompletion = function () { return InvoiceCreator.events.trigger('markStepComplete', this.$el.data('step')); }; Company.prototype.removeError = function (event) { return $(event.target).removeClass('error').qtip('destroy'); }; Company.prototype.selectFirstInput = function (event) { if (this.$el.hasClass('fullAccess') && event.target.nodeName !== 'INPUT') { return this.$('input').first().focus(); } }; Company.prototype.addHighlight = function (sourceElement) { if (this.$el.hasClass('editable') || this.$el.hasClass('fullAccess')) { return this.$el.addClass('highlight'); } }; Company.prototype.removeHighlight = function (sourceElement) { return this.$el.removeClass('highlight'); }; Company.prototype.updateVatNumber = function () { this.$('input#vat_number').val(this.model.get('vat_number')); if (this.model.get('vat_number')) { return this.$('label#vat_number_placeholder').addClass('populated'); } }; return Company;
        })(Backbone.View);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Client = (function (_super) {
            __extends(Client, _super); function Client() { this.removeHighlight = __bind(this.removeHighlight, this); this.addHighlight = __bind(this.addHighlight, this); return Client.__super__.constructor.apply(this, arguments); }
            Client.prototype.el = '#client'; Client.prototype.template = JST['client']; Client.prototype.initialize = function () { InvoiceCreator.events.on('startStep2', this.enableSection, this); InvoiceCreator.events.on('disableStep2', this.disableSection, this); InvoiceCreator.events.on('startStep4', this.allowFullAccess, this); InvoiceCreator.events.on('showValidationErrors', this.validateForm, this); InvoiceCreator.events.on('addHighlight-2', this.addHighlight); InvoiceCreator.events.on('removeHighlight-2', this.removeHighlight); this.model = InvoiceCreator.invoice.client; this.warlock = JST['warlock_client']; return this.success = JST['success_message_client']; }; Client.prototype.addHighlight = function (sourceElement) { if (this.$el.hasClass('editable') || this.$el.hasClass('fullAccess')) { return this.$el.addClass('highlight'); } }; Client.prototype.removeHighlight = function (sourceElement) { return this.$el.removeClass('highlight'); }; return Client;
        })(InvoiceCreator.Views.Company);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Contact = (function (_super) {
            __extends(Contact, _super); function Contact() { return Contact.__super__.constructor.apply(this, arguments); }
            Contact.prototype.el = '#contact'; Contact.prototype.template = JST['contact']; Contact.prototype.events = { 'click a.toggle-contact': 'toggleContact' }; Contact.prototype.initialize = function () { return $(document).click(this.closeContact); }; Contact.prototype.render = function () {
                var country, phone; country = company_params.country; if (country === 'United States' || country === 'Canada') { phone = 'Call toll free: 1.866.303.6061'; } else if (country === 'United Kingdom') { phone = 'Freephone: 0808-101-3408'; } else if (country === 'Ireland') { phone = 'Freephone: 1-800-949-046'; } else { phone = 'Wordwide: 416-481-6946'; }
                $(this.el).html(this.template({ phone: phone })); return this;
            }; Contact.prototype.toggleContact = function (event) { event.preventDefault(); $('#contact-warlock').fadeToggle(100).css({ top: (this.$el.offset().top + this.$el.height() + 60) + 'px', left: (this.$el.offset().left + 30) + 'px' }); return event.stopPropagation(); }; Contact.prototype.closeContact = function (event) { if (!$('#contact-warlock').has($(event.target)).length) { return $('#contact-warlock').fadeOut(100); } }; return Contact;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.CurrencyDropdown = (function (_super) {
            __extends(CurrencyDropdown, _super); function CurrencyDropdown() { return CurrencyDropdown.__super__.constructor.apply(this, arguments); }
            CurrencyDropdown.prototype.id = 'currency_dropdown'; CurrencyDropdown.prototype.template = JST['currency_dropdown']; CurrencyDropdown.prototype.events = { 'click .option': 'selectCurrency' }; CurrencyDropdown.prototype.initialize = function (element) { this.element = element; return $(document).on('click', this.collapseDropdown); }; CurrencyDropdown.prototype.render = function () { this.$el.html(this.template); InvoiceCreator.events.trigger('warlock', 'show', this.element, this.$el); return $('#warlock').addClass('dropdown'); }; CurrencyDropdown.prototype.collapseDropdown = function () { InvoiceCreator.events.trigger('warlock', 'hide', function () { return $('#warlock').removeClass('dropdown'); }); return $(document).off('click', this.collapseDropdown); }; CurrencyDropdown.prototype.selectCurrency = function (event) { var code; code = $(event.target).data('code'); InvoiceCreator.invoice.set('currency', code); return InvoiceCreator.events.trigger('updateCurrency'); }; return CurrencyDropdown;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Funfact = (function (_super) {
            __extends(Funfact, _super); function Funfact() { return Funfact.__super__.constructor.apply(this, arguments); }
            Funfact.prototype.el = '#funfact'; Funfact.prototype.initialize = function () { var facts, random; facts = ['football', 'hairs', 'ostriches', 'pizzas', 'tower', 'wolf']; random = Math.floor(Math.random() * facts.length); return this.template = JST['funfact_' + facts[random]]; }; Funfact.prototype.render = function () { this.$el.html(this.template); return this; }; return Funfact;
        })(Backbone.View);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Logo = (function (_super) {
            __extends(Logo, _super); function Logo() { this.onLoadFile = __bind(this.onLoadFile, this); return Logo.__super__.constructor.apply(this, arguments); }
            Logo.prototype.el = '#logo'; Logo.prototype.template = typeof FileReader !== "undefined" && FileReader !== null ? JST['logo'] : JST['logo_ie']; Logo.prototype.events = { 'change input': 'handleFileSelect', 'drop input': 'handleFileDrop', 'click .disabled.overlay': 'flashLocked', 'mouseover': 'removeTip' }; Logo.prototype.initialize = function () { var _this = this; InvoiceCreator.events.on('startStep4', this.enableSection, this); return window.ieLogoCallback = function (results) { return setTimeout(function () { return _this.onLoadFile({ target: { result: results } }); }, 100); }; }; Logo.prototype.render = function () {
                $(this.el).html(this.template()); $(this.el).addClass('disabled'); if (InvoiceCreator.invoice.get('logo')) { this.$('.image').html($('<img>').attr('src', InvoiceCreator.invoice.get('logo'))); this.$('.image').show(); this.$el.removeClass('border'); }
                if (swfobject.hasFlashPlayerVersion('9')) { this.$('.hide-if-flash-installed').hide(); }
                return this;
            }; Logo.prototype.handleFileSelect = function (event) { if (event.target.files.length) { return this.attachFile(event.target.files[0]); } }; Logo.prototype.handleFileDrop = function (event) { var dataTransfer; dataTransfer = event.originalEvent.dataTransfer; event.stopPropagation(); event.preventDefault(); if (dataTransfer.files.length) { return this.attachFile(dataTransfer.files[0]); } }; Logo.prototype.attachFile = function (file) {
                var reader; if (!file.type.match(/image.*/)) { return; }
                reader = new FileReader(); reader.onload = this.onLoadFile; return reader.readAsDataURL(file);
            }; Logo.prototype.onLoadFile = function (file) { this.$el.removeClass('border'); InvoiceCreator.invoice.set('logo', file.target.result); this.$('.image').html($('<img>').attr('src', InvoiceCreator.invoice.get('logo'))); this.$('.image').show(); return InvoiceCreator.events.trigger('markStepComplete', this.$el.data('step')); }; Logo.prototype.enableSection = function () { var _this = this; this.enabled = true; return setTimeout(function () { _this.$el.removeClass('disabled').addClass('enabled').addClass('fullAccess'); _this.$el.css('z-index', 80); return _this.$el.css('background-color', '#DFEEFD').delay(800).animate({ 'background-color': 'white' }, { duration: 700 }); }, 400); }; Logo.prototype.flashLocked = function () { return this.$('.locked.overlay').show().delay(2000).fadeOut(); }; Logo.prototype.removeTip = function () { if (this.enabled) { return (new InvoiceCreator.Views.SuperChargeTips).animateOut($('.add-logo'), '46px'); } }; return Logo;
        })(Backbone.View);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Navigation = (function (_super) {
            __extends(Navigation, _super); function Navigation() { this.formStart = __bind(this.formStart, this); return Navigation.__super__.constructor.apply(this, arguments); }
            Navigation.prototype.el = '#navigation'; Navigation.prototype.template = JST['navigation']; Navigation.prototype.events = { 'click a.button': 'sendInvoice', 'click [data-focus]': 'setFocus', 'click a.step_4': 'openFileUpload', 'mouseenter a.step': 'addHighlight', 'mouseleave a.step': 'removeHighlight' }; Navigation.prototype.initialize = function () {
                var html, popupView, _this = this; InvoiceCreator.events.on('editStep', this.editStep, this); InvoiceCreator.events.on('markStepComplete', this.markStepComplete, this); InvoiceCreator.events.on('markStepIncomplete', this.markStepIncomplete, this); InvoiceCreator.events.on('startStep4', this.allowSendInvoice, this); InvoiceCreator.events.on('sendInvoice', this.sendInvoice, this); $(document).scroll(function () { var navHeight, offset, padding, scroll, top, wrapperHeight; wrapperHeight = $('.invoice_wrapper').height(); navHeight = $('.nav').height(); scroll = $('body').scrollTop() || $(document).scrollTop(); offset = $('.invoice_wrapper').position().top; padding = 10; top = Math.min(wrapperHeight - navHeight, Math.max(0, scroll - offset) + padding); return $('.nav').css({ 'position': 'absolute', 'top': top, 'bottom': 'auto', 'right': padding }); }); popupView = false; if (app_state === 'success') {
                    if (lastAction === 'draft') { popupView = new InvoiceCreator.Views.PopupSuccessDraft; } else { popupView = new InvoiceCreator.Views.PopupSuccess; }
                    $('#facebox_overlay').unbind('click');
                } else if (app_state === 'returning') { setTimeout(function () { return InvoiceCreator.events.trigger('skipStep1'); }, 100); } else if ($.url().param('lb') === '0') { setTimeout(function () { return InvoiceCreator.events.trigger('skipStep1'); }, 100); } else { popupView = new InvoiceCreator.Views.PopupFirstTime; }
                if (popupView) { $(document).on('close.facebox', this.formStart); html = popupView.render().el; return $.facebox(popupView.render().el); }
            }; Navigation.prototype.render = function () { $(this.el).html(this.template); return this; }; Navigation.prototype.formStart = function () { $(document).off('close.facebox', this.formStart); InvoiceCreator.events.trigger('startStep1'); return this.$("[data-step=1]").addClass('enabled'); }; Navigation.prototype.editStep = function (step) { this.lastStep = this.currentStep; this.currentStep = step; this.$("[data-step=" + this.currentStep + "]").addClass('edit'); InvoiceCreator.events.trigger("disableStep" + this.lastStep); return InvoiceCreator.events.trigger("startStep" + this.currentStep); }; Navigation.prototype.markStepComplete = function (step, context) {
                if (context == null) { context = false; }
                this.$("[data-step=" + step + "]").addClass('complete').removeClass('focused'); if (step === 3) { this.$('.subtext small').html('Compose your email on the next step'); $('#work .hide-when-inactive').hide(); }
                if (this.lastStep) { this.currentStep = this.lastStep; delete this.lastStep; } else { this.currentStep = step + 1; }
                this.$("[data-step=" + this.currentStep + "]").addClass('enabled').removeClass('edit'); return InvoiceCreator.events.trigger("startStep" + this.currentStep, context);
            }; Navigation.prototype.markStepIncomplete = function (step) { return this.$("[data-step=" + step + "]").removeClass('complete'); }; Navigation.prototype.allowSendInvoice = function () { if (InvoiceCreator.invoice.isReady()) { this.$('a.button').addClass('enabled'); this.$('.hide-after-step-3').hide(); return this.$('.hidden').slideDown('slow'); } else { return this.$('a.button').removeClass('enabled'); } }; Navigation.prototype.sendInvoice = function (event) {
                var sendView; if (event) { event.preventDefault(); }
                if (!this.$('.step_3').hasClass('complete')) { return; }
                if (InvoiceCreator.invoice.isReady()) { sendView = new InvoiceCreator.Views.PopupSend; $.facebox(sendView.render().el); $('#facebox_overlay').unbind('click'); } else { InvoiceCreator.events.trigger("showValidationErrors"); }
                return false;
            }; Navigation.prototype.addHighlight = function (event) { var highlightStep; highlightStep = $(event.currentTarget).attr('data-step'); return $(".section[data-step='" + highlightStep + "']").addClass('highlight'); }; Navigation.prototype.removeHighlight = function (event) { var highlightStep; highlightStep = $(event.currentTarget).attr('data-step'); return $(".section[data-step='" + highlightStep + "']").removeClass('highlight'); }; Navigation.prototype.setFocus = function (event) { var focusElement; focusElement = $($(event.currentTarget).data('focus')); if (focusElement.length > 0) { return focusElement[0].focus(); } }; Navigation.prototype.openFileUpload = function () { return $('input[type=file]').click(); }; return Navigation;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Notes = (function (_super) {
            __extends(Notes, _super); function Notes() { return Notes.__super__.constructor.apply(this, arguments); }
            Notes.prototype.el = '#notes'; Notes.prototype.template = JST['notes']; Notes.prototype.events = { 'click': 'selectFirstInput', 'change textarea': 'save', 'click .disabled.overlay': 'flashLocked', 'hover': 'toggleHighlight' }; Notes.prototype.initialize = function () { return InvoiceCreator.events.on('startStep4', this.enableSection, this); }; Notes.prototype.render = function () { $(this.el).html(this.template); this.$('textarea').autoResize({ minHeight: 72, maxHeight: 800, extraSpace: 0 }); $(this.el).addClass('disabled').find('textarea').attr('disabled', 'disabled'); return this; }; Notes.prototype.save = function (event) { InvoiceCreator.invoice.set('notes', $(event.target).val()); if ($(event.target).val()) { return InvoiceCreator.events.trigger('markStepComplete', this.$el.data('step')); } else { return InvoiceCreator.events.trigger('markStepIncomplete', this.$el.data('step')); } }; Notes.prototype.enableSection = function (context) {
                var _this = this; if (context == null) { context = false; }
                this.enabled = true; return setTimeout(function () { _this.$el.removeClass('disabled').addClass('enabled').addClass('fullAccess'); _this.$el.find('textarea').removeAttr('disabled'); if (context !== 'superChargeSend') { return _this.$el.css('background-color', '#DFEEFD').delay(800).animate({ 'background-color': 'white' }, { duration: 700 }); } }, 400);
            }; Notes.prototype.flashLocked = function () { return this.$('.locked.overlay').show().delay(2000).fadeOut(); }; Notes.prototype.selectFirstInput = function (event) { if (this.$el.hasClass('fullAccess') && event.target.nodeName !== 'INPUT') { return this.$('textarea').first().focus(); } }; Notes.prototype.toggleHighlight = function (event) {
                if (this.$('.add-notes') && this.$('.add-notes').is(':visible')) { (new InvoiceCreator.Views.SuperChargeTips).animateOut($('.add-notes'), '-60px'); }
                return $(this.el).toggleClass('highlight');
            }; return Notes;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Overlay = (function (_super) {
            __extends(Overlay, _super); function Overlay() { return Overlay.__super__.constructor.apply(this, arguments); }
            Overlay.prototype.el = '#bg_overlay'; Overlay.prototype.initialize = function () { InvoiceCreator.events.on('editStep', this.disableLight, this); InvoiceCreator.events.on('startStep3', this.enableLight, this); InvoiceCreator.events.on('startStep4', this.widenLight, this); InvoiceCreator.events.on('onAfterResize', this.updateHeight, this); InvoiceCreator.events.on('updateTotals', this.updateHeight, this); InvoiceCreator.invoice.lines.on('add', this.updateHeight, this); InvoiceCreator.invoice.lines.on('remove', this.updateHeight, this); InvoiceCreator.events.on('addHighlight-3', this.enableWorkHighlight, this); InvoiceCreator.events.on('removeHighlight-3', this.disableWorkHighlight, this); InvoiceCreator.events.on('resizeWorkHighlight', this.updateHeight, this); return this.$el.html(''); }; Overlay.prototype.render = function () { $(this.el).append("<div class='light_up'></div>"); $(this.el).append("<div class='work-highlight'></div>"); return this; }; Overlay.prototype.disableLight = function () { this.litUp = false; return this.$('.light_up').css({ height: 0 }); }; Overlay.prototype.enableLight = function () { var height, top; if (InvoiceCreator.invoice.client.isValid() && !this.litUp) { this.litUp = true; this.$('.light_up').show(); top = $('#work').offset().top - $('#invoice').offset().top - 10; height = $('#work').height() + 20; this.$('.light_up').css({ top: top + 'px' }); return this.$('.light_up').animate({ height: height + 'px' }); } }; Overlay.prototype.widenLight = function () { if (InvoiceCreator.invoice.isReady()) { this.$('.light_up').animate({ top: '0px', height: '100%' }); return this.widened = true; } }; Overlay.prototype.updateHeight = function () { var _this = this; return setTimeout(function () { var height; if (!_this.widened && _this.litUp) { height = $('#work').height() + 20; return _this.$('.light_up').animate({ height: height + 'px' }); } }, 100); }; Overlay.prototype.enableWorkHighlight = function (eventTrigger) { var height, top; if ($('#work').hasClass('enabled') && !$('#terms').hasClass('disabled')) { top = $('#work').offset().top - $('#invoice').offset().top - 10; height = $('#work').height() + 20; this.$('.work-highlight').css({ top: top + 'px', height: height + 'px' }); return this.$('.work-highlight').show(); } }; Overlay.prototype.resizeWorkHighlight = function () { var _this = this; return setTimeout(function () { var height; if (_this.litUp) { height = $('#work').height() + 20; return _this.$('.work-highlight').animate({ height: height + 'px' }); } }, 120); }; Overlay.prototype.disableWorkHighlight = function () { return this.$('.work-highlight').css('height', 0); }; return Overlay;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Terms = (function (_super) {
            __extends(Terms, _super); function Terms() { return Terms.__super__.constructor.apply(this, arguments); }
            Terms.prototype.el = '#terms'; Terms.prototype.template = JST['terms']; Terms.prototype.events = { 'click': 'selectFirstInput', 'hover': 'toggleHighlight', 'change textarea': 'save', 'click .disabled.overlay': 'flashLocked' }; Terms.prototype.initialize = function () { return InvoiceCreator.events.on('startStep4', this.enableSection, this); }; Terms.prototype.render = function () { $(this.el).html(this.template); this.$('textarea').autoResize({ minHeight: 72, maxHeight: 800, extraSpace: 0 }); $(this.el).addClass('disabled').find('textarea').attr('disabled', 'disabled'); return this; }; Terms.prototype.save = function (event) { InvoiceCreator.invoice.set('terms', $(event.target).val()); if ($(event.target).val()) { return InvoiceCreator.events.trigger('markStepComplete', this.$el.data('step')); } else { return InvoiceCreator.events.trigger('markStepIncomplete', this.$el.data('step')); } }; Terms.prototype.enableSection = function (context) {
                var _this = this; if (context == null) { context = false; }
                return setTimeout(function () { _this.$el.removeClass('disabled').addClass('enabled').addClass('fullAccess'); _this.$el.find('textarea').removeAttr('disabled'); if (context !== 'superChargeSend') { return _this.$el.css('background-color', '#DFEEFD').delay(800).animate({ 'background-color': 'white' }, { duration: 700 }); } }, 400);
            }; Terms.prototype.flashLocked = function () { return this.$('.locked.overlay').show().delay(2000).fadeOut(); }; Terms.prototype.selectFirstInput = function (event) { if (this.$el.hasClass('fullAccess') && event.target.nodeName !== 'INPUT') { return this.$('textarea').first().focus(); } }; Terms.prototype.toggleHighlight = function (event) {
                if (this.$('.add-terms') && this.$('.add-terms').is(':visible')) { (new InvoiceCreator.Views.SuperChargeTips).animateOut($('.add-terms'), '-60px'); }
                return $(this.el).toggleClass('highlight');
            }; return Terms;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.TotalDate = (function (_super) {
            __extends(TotalDate, _super); function TotalDate() { return TotalDate.__super__.constructor.apply(this, arguments); }
            TotalDate.prototype.el = '#total_date'; TotalDate.prototype.template = JST['total_date']; TotalDate.prototype.events = { 'change input[name=number]': 'setNumber', 'change input[name=date]': 'setDate', 'hover': 'toggleHighlight' }; TotalDate.prototype.initialize = function () { this.fullAccess = false; InvoiceCreator.events.on('updateTotals', this.updateTotals, this); InvoiceCreator.events.on('updateCurrency', this.setCurrency, this); InvoiceCreator.events.on('markStepTwo', this.enableSection, this); InvoiceCreator.events.on('startStep1', this.enableSection, this); return InvoiceCreator.events.on('startStep4', this.allowFullAccess, this); }; TotalDate.prototype.render = function () { $(this.el).html(this.template).addClass('disabled'); this.setNumber(); this.setDate(); this.setCurrency(); this.$('input[name=date]').datepicker({ dateFormat: 'MM dd, yy', dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'] }); return this; }; TotalDate.prototype.updateTotals = function () { return this.$('span.amount').html("" + (InvoiceCreator.invoice.total().format(2))); }; TotalDate.prototype.setNumber = function (event) {
                var number, set_number; number = InvoiceCreator.invoice.get('number'); if (event && (set_number = this.$('input[name=number]').val().substring(0, 10))) { number = set_number; }
                InvoiceCreator.invoice.set('number', number); return this.$('input[name=number]').val(number);
            }; TotalDate.prototype.setDate = function (event) {
                var date; if (event) { date = new Date(this.$('input[name=date]').val()); if (!date.valueOf()) { date = new Date; this.$('input[name=date]').val($.datepicker.formatDate('MM dd, yy', date)); } } else { date = new Date; this.$('input[name=date]').val($.datepicker.formatDate('MM dd, yy', date)); }
                return InvoiceCreator.invoice.set('date', $.datepicker.formatDate('yy-mm-dd', date));
            }; TotalDate.prototype.setCurrency = function () { return this.$('span.currency_symbol').html(InvoiceCreator.CURRENCIES[InvoiceCreator.invoice.get('currency')].symbol); }; TotalDate.prototype.enableSection = function () { if (InvoiceCreator.invoice.client.isValid()) { $(this.el).removeClass('disabled'); return this.$('.form input').removeAttr('disabled', ''); } }; TotalDate.prototype.allowFullAccess = function () { this.$('input').removeAttr('disabled'); this.$el.removeClass('disabled').removeClass('focused').removeClass('editable'); this.$el.addClass('fullAccess'); return this.fullAccess = true; }; TotalDate.prototype.toggleHighlight = function (event) { return $(this.el).toggleClass('highlight'); }; return TotalDate;
        })(Backbone.View);
    }).call(this); (function () {
        InvoiceCreator.Views.SuperChargeTips = (function () {
            function SuperChargeTips() { }
            SuperChargeTips.prototype.showTips = function () { var _this = this; setTimeout(function () { return _this.logoTip(); }, 400); setTimeout(function () { return _this.termsTip(); }, 900); return setTimeout(function () { return _this.notesTip(); }, 1200); }; SuperChargeTips.prototype.logoTip = function () { return this.animate($('.add-logo'), '120px', '86px'); }; SuperChargeTips.prototype.termsTip = function () { return this.animate($('.add-terms'), '-85px', '-38px'); }; SuperChargeTips.prototype.notesTip = function () { return this.animate($('.add-notes'), '-70px', '-35px'); }; SuperChargeTips.prototype.animate = function (el, fromPx, toPx, speed, easing) {
                if (speed == null) { speed = 'slow'; }
                if (easing == null) { easing = 'easeInOutBack'; }
                el.css('display', 'block'); el.animate({ opacity: 0, top: fromPx }, 0); return el.animate({ opacity: 1, top: toPx }, speed, easing);
            }; SuperChargeTips.prototype.animateOut = function (el, toPx, speed, easing) {
                if (speed == null) { speed = 'slow'; }
                if (easing == null) { easing = 'easeInOutBack'; }
                return el.animate({ opacity: 0, top: toPx }, speed, easing);
            }; return SuperChargeTips;
        })();
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Work = (function (_super) {
            __extends(Work, _super); function Work() { this.focusSection = __bind(this.focusSection, this); this.dragStop = __bind(this.dragStop, this); this.dragStart = __bind(this.dragStart, this); this.removeLine = __bind(this.removeLine, this); this.addLine = __bind(this.addLine, this); return Work.__super__.constructor.apply(this, arguments); }
            Work.prototype.el = '#work'; Work.prototype.template = JST['work']; Work.prototype.events = { 'click a.button': 'next', 'click li.add_line': 'addLine', 'click li.remove_line': 'removeLine', 'click #add_line_button': 'toggleDropdown', 'mouseover': 'fireHighlightEvent', 'mouseleave': 'fireDeHighlightEvent' }; Work.prototype.initialize = function () { this.model = InvoiceCreator.invoice; $(document).click(this.collapseDropdown); InvoiceCreator.events.on('startStep3', this.enableSection, this); InvoiceCreator.events.on('disableStep3', this.disableSection, this); InvoiceCreator.events.on('clickActivate-3', this.focusSection, this); InvoiceCreator.events.on('showValidationErrors', this.next, this); InvoiceCreator.events.on('displayWorkValidation', this.displayValidation, this); return this.warlock = JST['warlock_lines']; }; Work.prototype.render = function () { this.$el.html(this.template); this.addLine(null, 'first'); $(this.el).addClass('disabled').find('input, textarea').attr('disabled', 'disabled'); (new InvoiceCreator.Views.Work.Totals).render(); return this; }; Work.prototype.fireHighlightEvent = function (e) { return InvoiceCreator.events.trigger('addHighlight-3', $(e.currentTarget)); }; Work.prototype.fireDeHighlightEvent = function (e) { return InvoiceCreator.events.trigger('removeHighlight-3', $(e.currentTarget)); }; Work.prototype.displayValidation = function () {
                $('tr.total').qtip('destroy'); if (this.model.total() < 0) { this.$('tr.total').qtip({ content: { text: 'The invoice total cannot be less than zero.' }, position: { my: 'right center', at: 'left center' }, show: { ready: true }, style: { classes: 'ui-tooltip-red ui-tooltip-rounded' } }); }
                return false;
            }; Work.prototype.next = function () {
                this.displayValidation(); if (this.model.isReady()) { InvoiceCreator.events.trigger('warlock', 'hide'); this.triggerStepCompletion(); }
                return false;
            }; Work.prototype.addVisibleClass = function (e) { return e.currentTarget.addClass('visible'); }; Work.prototype.removeVisibleClass = function (e) { return e.currentTarget.removeClass('visible'); }; Work.prototype.triggerStepCompletion = function () { var popup; popup = new InvoiceCreator.Views.SuperCharge; $.facebox(popup.render().el); return $('#facebox_overlay').unbind('click'); }; Work.prototype.addLine = function (event, is_first) {
                var line, line_dom, line_type, model, view; line_type = 'item'; line_dom = null; if (event) { event.preventDefault(); if ((line_dom = $(event.target).parents('tr')).length) { line_type = InvoiceCreator.invoice.lines.getByCid(line_dom.data('cid')).get('type'); } else { line_type = $(event.target).data('type'); } }
                model = new InvoiceCreator.Models.Line({ type: line_type }); InvoiceCreator.invoice.lines.add(model); if (InvoiceCreator.invoice.lines["" + line_type + "Count"] === 1) { this.$('table.items').append(JST["work_" + line_type]()); this.$("table.items tbody." + line_type).tableDnD({ onDragClass: 'dragged', dragHandle: '.move_line', onDrop: this.assignPositions, onDragStart: this.dragStart }); }
                view = new InvoiceCreator.Views.Work.Line({ model: model }); line = view.render().el; $('textarea', line).autoResize({ maxHeight: 800, extraSpace: 0, onResize: function () { return InvoiceCreator.events.trigger('onAfterResize'); }, animate: { duration: 0 } }); if (is_first) { $('input[name=item]', line).attr('id', 'item_label'); $('textarea[name=description]', line).attr('id', 'item_desc'); $('td.item .field', line).prepend($('<label>').addClass('placeholder').attr('for', 'item_label').html('e.g. Fertilizer')); $('td.desc .field', line).prepend($('<label>').addClass('placeholder').attr('for', 'item_desc').html('e.g. Super strength lawn care')); }
                if ($(line_dom).length) { line_dom.after(line); } else { this.$("tbody." + line_type).append(line); this.$("thead." + line_type).show(); }
                this.$("tbody." + line_type).tableDnDUpdate(); this.assignPositions(this.$("tbody." + line_type)); return InvoiceCreator.events.trigger('resizeWorkHighlight');
            }; Work.prototype.removeLine = function (event) {
                var line, line_dom, line_type, tbody, thead; event.preventDefault(); line_dom = $(event.target).parents('tr').first(); if (line = InvoiceCreator.invoice.lines.getByCid(line_dom.data('cid'))) { line_type = line.get('type'); line.destroy(); InvoiceCreator.events.trigger('updateTotals'); thead = this.$("thead." + line_type); tbody = this.$("tbody." + line_type); if (InvoiceCreator.invoice.lines.where({ type: line_type }).length === 0) { thead.fadeOut('fast', function () { thead.remove(); tbody.remove(); return line_dom.remove(); }); } else { line_dom.fadeOut(50, function () { return line_dom.remove(); }); } }
                return InvoiceCreator.events.trigger('resizeWorkHighlight');
            }; Work.prototype.toggleDropdown = function (event) { if (this.$el.hasClass('enabled')) { $('#add_line_button').toggleClass('active'); return event.stopPropagation(); } }; Work.prototype.collapseDropdown = function () { return $('#add_line_button').removeClass('active'); }; Work.prototype.dragStart = function (table, listItem) { $(document).bind('mouseup', this.dragStop); return this.$('table.items').addClass('transient'); }; Work.prototype.dragStop = function (table, listItem) { $(document).unbind('mouseup', this.dragStop); return this.$('table.items').removeClass('transient'); }; Work.prototype.assignPositions = function (table, row) { $('td', row).removeClass('active'); return $('tr', table).each(function (index, row) { var line; line = InvoiceCreator.invoice.lines.getByCid($(row).data('cid')); return line.set('position', index); }); }; Work.prototype.enableSection = function () {
                if (!this.warlockShown) { this.warlockShown = true; InvoiceCreator.events.trigger('warlock', 'show', this.$el, this.warlock()); }
                this.$el.removeClass('disabled').addClass('enabled'); this.$('input, textarea').removeAttr('disabled').first().focus(); return this.$('.submit').show();
            }; Work.prototype.focusSection = function (eventTrigger) { var firstLine; if (!eventTrigger.hasClass('section')) { firstLine = this.$el.find('.line:first'); if (firstLine.length > 0) { return firstLine.find('input:first').focus(); } } }; Work.prototype.disableSection = function () { InvoiceCreator.events.trigger('warlock', 'hide'); this.$('input, textarea').attr('disabled', 'disabled'); this.$el.removeClass('enabled').addClass('disabled'); return this.$('.submit').hide(); }; return Work;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.TotalNav = (function (_super) {
            __extends(TotalNav, _super); function TotalNav() { return TotalNav.__super__.constructor.apply(this, arguments); }
            TotalNav.prototype.el = '#total_nav'; TotalNav.prototype.template = JST['total_nav']; TotalNav.prototype.events = { 'click span.currency_code': 'openDropdown' }; TotalNav.prototype.initialize = function () { InvoiceCreator.events.on('updateTotals', this.updateTotals, this); return InvoiceCreator.events.on('updateCurrency', this.setCurrency, this); }; TotalNav.prototype.render = function () { $(this.el).html(this.template); this.setCurrency(); return this; }; TotalNav.prototype.updateTotals = function () { var total; this.$('.total').removeClass('x-reduced').removeClass('reduced'); total = InvoiceCreator.invoice.total(); this.$('span.amount').html("" + (total.format(2))); if (total >= 1000000) { return this.$('.total').addClass('x-reduced'); } else if (total >= 10000) { return this.$('.total').addClass('reduced'); } }; TotalNav.prototype.setCurrency = function () { this.$('span.currency_code').html(InvoiceCreator.invoice.escape('currency')); return this.$('span.currency_symbol').html(InvoiceCreator.CURRENCIES[InvoiceCreator.invoice.escape('currency')].symbol); }; TotalNav.prototype.openDropdown = function (event) { event.stopPropagation(); return (new InvoiceCreator.Views.CurrencyDropdown('.balance')).render(); }; return TotalNav;
        })(Backbone.View);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Warlock = (function (_super) {
            __extends(Warlock, _super); function Warlock() { this._show = __bind(this._show, this); return Warlock.__super__.constructor.apply(this, arguments); }
            Warlock.prototype.el = '#warlock'; Warlock.prototype.template = JST['warlock']; Warlock.prototype.events = { 'click a.close': 'hide' }; Warlock.prototype.initialize = function () { return InvoiceCreator.events.on('warlock', this.action, this); }; Warlock.prototype.render = function () { this.$el.html(this.template); return this; }; Warlock.prototype.hide = function (callback) {
                if (typeof callback === 'function') { this.$el.fadeOut(400, callback); } else { this.$el.fadeOut(); }
                return false;
            }; Warlock.prototype.show = function (element, content) { var _this = this; if (this.showing) { return this.$el.fadeOut(400, function () { return _this._show(element, content); }); } else { return this._show(element, content); } }; Warlock.prototype._show = function (element, content) { var $element; $element = $(element); this.showing = true; this.$('.content').html(content); this.$el.css({ top: ($element.offset().top - this.$el.height() - 20) + 'px', left: ($element.offset().left) + 'px' }); return this.$el.fadeIn(); }; Warlock.prototype.action = function (action) {
                if (action == null) { action = 'show'; }
                if (this[action]) { return this[action].apply(this, $.makeArray(arguments).slice(1)); }
            }; return Warlock;
        })(Backbone.View);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.PopupDraft = (function (_super) {
            __extends(PopupDraft, _super); function PopupDraft() { this.showEmail = __bind(this.showEmail, this); this.closePopup = __bind(this.closePopup, this); return PopupDraft.__super__.constructor.apply(this, arguments); }
            PopupDraft.prototype.template = JST['popup_draft']; PopupDraft.prototype.events = { 'change input[type=text], textarea': 'saveAttribute', 'blur input[type=text]': 'validateField', 'click .placeholder': 'focusInput', 'click a.button': 'saveDraft', 'click a.email': 'sendEmail', 'change input[type=checkbox]': 'saveCheckbox' }; PopupDraft.prototype.validations = { 'notEmpty': ['first_name', 'last_name'], 'validEmail': ['from'] }; PopupDraft.prototype.validationRows = [['first_name', 'last_name'], ['from']]; PopupDraft.prototype.wasValidated = false; PopupDraft.prototype.errors = { 'row1': {}, 'row2': {} }; PopupDraft.prototype.renderedErrors = {}; PopupDraft.prototype.initialize = function () { this.invoice = InvoiceCreator.invoice; this.invoice.set('draft', 1); return $(document).on('close.facebox', this.closePopup); }; PopupDraft.prototype.render = function () { $(this.el).html(this.template({ from: this.invoice.get('from'), first_name: this.invoice.get('first_name'), last_name: this.invoice.get('last_name'), remember_me: this.invoice.get('remember_me') })); this.$('#draft_from').delayedFocus(); return this; }; PopupDraft.prototype.closePopup = function () { $(document).off('close.facebox', this.closePopup); return $('label[for],input').qtip('destroy'); }; PopupDraft.prototype.saveAttribute = function (event) { var field, value; field = $(event.target); value = $.trim(field.val()); return this.invoice.set(field.attr('name'), value); }; PopupDraft.prototype.focusInput = function (event) { return this.$(event.target).nextAll('input').focus(); }; PopupDraft.prototype.validate = function (event) { var hasErrors, _this = this; this.wasValidated = true; $(this.validations['notEmpty']).each(function (index, fieldName) { var field, validationRow; validationRow = _this.validationRow(fieldName); field = _this.$("input[name='" + fieldName + "']"); if ($.trim(field.val()) === '') { return _this.errors[validationRow][fieldName] = field; } else { return delete _this.errors[validationRow][fieldName]; } }); $(this.validations['validEmail']).each(function (index, fieldName) { var field, validationRow; validationRow = _this.validationRow(fieldName); field = _this.$("input[name='" + fieldName + "']"); if (_this.isEmail(field.val())) { return delete _this.errors[validationRow][fieldName]; } else { return _this.errors[validationRow][fieldName] = field; } }); hasErrors = false; $.each(this.errors, function (index, validationRow) { if (_.size(validationRow) > 0) { hasErrors = true; return _this.displayValidationErrors(); } }); if (hasErrors) { return false; } else { return true; } }; PopupDraft.prototype.validateField = function (event) {
                var field, fieldName, fieldRow; field = $(event.currentTarget); fieldName = field.attr('name'); fieldRow = this.validationRow(fieldName); if ($.inArray(fieldName, this.validations['validEmail']) !== -1 && field.val() !== '') { if (this.isEmail(field.val())) { delete this.errors[fieldRow][fieldName]; } else { this.errors[fieldRow][fieldName] = field; } }
                if (this.wasValidated) { if ($.inArray(fieldName, this.validations['notEmpty']) !== -1) { if ($.trim(field.val()) === '') { this.errors[fieldRow][fieldName] = field; } else { delete this.errors[fieldRow][fieldName]; } } }
                return this.displayValidationErrors();
            }; PopupDraft.prototype.validationRow = function (fieldName) { var row; row = false; $.each(this.validationRows, function (index, item) { if ($.inArray(fieldName, item) !== -1) { return row = "row" + (index + 1); } }); return row; }; PopupDraft.prototype.displayValidationErrors = function () {
                var _this = this; $.each(this.errors, function (index, item) {
                    var error_field, error_text; if (index === 'row1') { error_field = 'name'; }
                    if (index === 'row2') { error_field = 'email'; }
                    error_text = []; $.each(_this.errors[index], function (field, errorField) { return error_text.push(errorField.data('error-message')); }); if (error_text.length === 0) { _this.$("label[for=draft_" + error_field + "]").qtip('destroy'); } else { _this.$("label[for=draft_" + error_field + "]").qtip({ content: { text: error_text.join('<br />') }, position: { my: 'right center', at: 'left center' }, style: { classes: 'ui-tooltip-red ui-tooltip-rounded' }, show: { ready: true, effect: function () { return $(this).show(0); } }, hide: { event: 'focus', effect: function () { return $(this).hide(0); } } }); }
                }); return this.renderedErrors = $.extend(true, this.errors);
            }; PopupDraft.prototype.isEmail = function (email) {
                var valid; valid = /^([\w.%-+]+)@(?:(?:[-a-z0-9]+\.)+[a-z]{2,})$/.test(email); if (email === '') { valid = false; }
                return valid;
            }; PopupDraft.prototype.saveDraft = function (event) {
                var _this = this; event.preventDefault(); if (this.validate()) { this.$('.loading_overlay').show(); this.invoice.save(null, { success: function (model, data) { dataLayer.push({ 'public_identifier': model.get('public_identifier'), 'country': model.get('company').country, 'event': 'trial-complete' }); return setTimeout(function () { window.onbeforeunload = null; return window.location = data.redirect_url; }, 1000); }, error: function () { _this.$('.loading_overlay').hide(); return _this.$('.errors').html('Sorry, there was a problem sending your draft invoice.').show(); } }); }
                return false;
            }; PopupDraft.prototype.saveCheckbox = function (event) { var field; field = $(event.target); return this.invoice.set(field.attr('name'), field.is(':checked')); }; PopupDraft.prototype.sendEmail = function (event) { event.preventDefault(); $(document).on('afterClose.facebox', this.showEmail); $(document).trigger('close.facebox'); return false; }; PopupDraft.prototype.showEmail = function () { var sendView; $(document).off('afterClose.facebox', this.showDraft); sendView = new InvoiceCreator.Views.PopupSend; $.facebox(sendView.render().el); return $('#facebox_overlay').unbind('click'); }; return PopupDraft;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.PopupFirstTime = (function (_super) {
            __extends(PopupFirstTime, _super); function PopupFirstTime() { return PopupFirstTime.__super__.constructor.apply(this, arguments); }
            PopupFirstTime.prototype.template = JST['popup_first_time']; PopupFirstTime.prototype.events = { 'click a.button': 'close' }; PopupFirstTime.prototype.render = function () { this.$el.html(this.template); return this; }; PopupFirstTime.prototype.close = function (event) { $(document).trigger('close.facebox'); $('#company_name').delayedFocus(); return false; }; return PopupFirstTime;
        })(Backbone.View);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.PopupSend = (function (_super) {
            __extends(PopupSend, _super); function PopupSend() { this.showDraft = __bind(this.showDraft, this); this.closePopup = __bind(this.closePopup, this); return PopupSend.__super__.constructor.apply(this, arguments); }
            PopupSend.prototype.template = JST['popup_send']; PopupSend.prototype.events = { 'change input[type=text], textarea': 'saveAttribute', 'change input[type=checkbox]': 'saveCheckbox', 'blur input[type=text]': 'validateField', 'click .placeholder': 'focusInput', 'click a.button': 'sendInvoice', 'click a.draft': 'sendDraft', 'focus textarea': 'focusBody', 'blur textarea': 'blurBody', 'keydown [name=body_header]': 'headerMoveCursor', 'keydown [name=body_footer]': 'footerMoveCursor' }; PopupSend.prototype.validations = { 'notEmpty': ['first_name', 'last_name', 'subject'], 'validEmail': ['from', 'to'] }; PopupSend.prototype.validationRows = [['from', 'first_name', 'last_name'], ['to'], ['subject']]; PopupSend.prototype.wasValidated = false; PopupSend.prototype.errors = { 'row1': {}, 'row2': {}, 'row3': {} }; PopupSend.prototype.renderedErrors = {}; PopupSend.prototype.initialize = function () {
                $('#send-question-mark').live({ mouseenter: function () { return $(this).qtip({ position: { my: 'bottom center', at: 'top center' }, show: { ready: true }, style: { classes: 'ui-tooltip-freshbooks-yellow', width: '150px' } }); } }); this.invoice = InvoiceCreator.invoice; this.invoice.unset('draft'); this.invoice.set('subject', "You have an invoice from " + (InvoiceCreator.invoice.company.get('name'))); if (!this.invoice.get('body_footer')) { this.invoice.set('body_footer', "Best regards,\n" + (InvoiceCreator.invoice.company.get('name'))); }
                if (this.invoice.cached_total === false || this.invoice.cached_total !== this.invoice.total()) { this.invoice.cached_total = this.invoice.total(); this.invoice.set('body_header', "To view your invoice from " + (InvoiceCreator.invoice.company.get('name')) + " for " + InvoiceCreator.CURRENCIES[InvoiceCreator.invoice.get('currency')].symbol + (InvoiceCreator.invoice.total().format(2)) + ", or to download a PDF copy for your records, click the link below:"); }
                this.from = this.invoice.get('from'); this.first_name = this.invoice.get('first_name'); this.last_name = this.invoice.get('last_name'); this.to = this.invoice.get('to'); this.subject = this.invoice.escape('subject'); this.body_header = this.invoice.get('body_header'); this.body_footer = this.invoice.escape('body_footer'); this.remember_me = this.invoice.get('remember_me'); return $(document).on('close.facebox', this.closePopup);
            }; PopupSend.prototype.render = function () { $(this.el).html(this.template({ from: this.from, first_name: this.first_name, last_name: this.last_name, to: this.to, subject: this.subject, body_header: this.body_header, body_footer: this.body_footer, remember_me: this.remember_me })); setTimeout(function () { return this.$('textarea').autosize(); }, 500); this.$('#send_from').delayedFocus(); return this; }; PopupSend.prototype.closePopup = function () { $(document).off('close.facebox', this.closePopup); return $('label[for],input').qtip('destroy'); }; PopupSend.prototype.saveAttribute = function (event) { var field, value; field = $(event.target); value = $.trim(field.val()); return this.invoice.set(field.attr('name'), value); }; PopupSend.prototype.saveCheckbox = function (event) { var field; field = $(event.target); return this.invoice.set(field.attr('name'), field.is(':checked')); }; PopupSend.prototype.focusInput = function (event) { return this.$(event.target).nextAll('input').focus(); }; PopupSend.prototype.validate = function (event) { var hasErrors, _this = this; this.wasValidated = true; $(this.validations['notEmpty']).each(function (index, fieldName) { var field, validationRow; validationRow = _this.validationRow(fieldName); field = _this.$("input[name='" + fieldName + "']"); if ($.trim(field.val()) === '') { return _this.errors[validationRow][fieldName] = field; } else { return delete _this.errors[validationRow][fieldName]; } }); $(this.validations['validEmail']).each(function (index, fieldName) { var field, validationRow; validationRow = _this.validationRow(fieldName); field = _this.$("input[name='" + fieldName + "']"); if (_this.isEmail(field.val())) { return delete _this.errors[validationRow][fieldName]; } else { return _this.errors[validationRow][fieldName] = field; } }); hasErrors = false; $.each(this.errors, function (index, validationRow) { if (_.size(validationRow) > 0) { hasErrors = true; return _this.displayValidationErrors(); } }); if (hasErrors) { return false; } else { return true; } }; PopupSend.prototype.validateField = function (event) {
                var field, fieldName, fieldRow; field = $(event.currentTarget); fieldName = field.attr('name'); fieldRow = this.validationRow(fieldName); if ($.inArray(fieldName, this.validations['validEmail']) !== -1 && field.val() !== '') { if (this.isEmail(field.val())) { delete this.errors[fieldRow][fieldName]; } else { this.errors[fieldRow][fieldName] = field; } }
                if (this.wasValidated) { if ($.inArray(fieldName, this.validations['notEmpty']) !== -1) { if ($.trim(field.val()) === '') { this.errors[fieldRow][fieldName] = field; } else { delete this.errors[fieldRow][fieldName]; } } }
                return this.displayValidationErrors();
            }; PopupSend.prototype.isEmail = function (email) {
                var valid; valid = /^([\w.%\-+]+)@(?:(?:[\-a-z0-9]+\.)+[a-z]{2,})$/.test(email); if (email === '') { valid = false; }
                return valid;
            }; PopupSend.prototype.validationRow = function (fieldName) { var row; row = false; $.each(this.validationRows, function (index, item) { if ($.inArray(fieldName, item) !== -1) { return row = "row" + (index + 1); } }); return row; }; PopupSend.prototype.displayValidationErrors = function () {
                var _this = this; $.each(this.errors, function (index, item) {
                    var error_field, error_text; if (index === 'row1') { error_field = 'from'; }
                    if (index === 'row2') { error_field = 'to'; }
                    if (index === 'row3') { error_field = 'subject'; }
                    error_text = []; $.each(_this.errors[index], function (field, errorField) { return error_text.push(errorField.data('error-message')); }); if (error_text.length === 0) { _this.$("label[for=send_" + error_field + "]").qtip('destroy'); } else { _this.$("label[for=send_" + error_field + "]").qtip({ content: { text: error_text.join('<br />') }, position: { my: 'right center', at: 'left center' }, style: { classes: 'ui-tooltip-red ui-tooltip-rounded' }, show: { ready: true, effect: function () { return $(this).show(0); } }, hide: { event: 'focus', effect: function () { return $(this).hide(0); } } }); }
                }); return this.renderedErrors = $.extend(true, this.errors);
            }; PopupSend.prototype.sendInvoice = function (event) {
                var _this = this; event.preventDefault(); if (this.validate()) { this.$('.loading_overlay').fadeIn(300); this.invoice.save(null, { success: function (model, data) { dataLayer.push({ 'public_identifier': model.get('public_identifier'), 'country': model.get('company').country, 'event': 'trial-complete' }); return setTimeout(function () { window.onbeforeunload = null; return window.location = data.redirect_url; }, 1000); }, error: function () { _this.$('.loading_overlay').hide(); return _this.$('.errors').html('Sorry, there was a problem sending your invoice.').show(); } }); }
                return false;
            }; PopupSend.prototype.sendDraft = function (event) { event.preventDefault(); $('#send_to, #send_from').val(''); this.invoice.set('to', ''); this.invoice.set('from', ''); $(document).on('afterClose.facebox', this.showDraft); $(document).trigger('close.facebox'); return false; }; PopupSend.prototype.showDraft = function () { var draftView; $(document).off('afterClose.facebox', this.showDraft); draftView = new InvoiceCreator.Views.PopupDraft; $.facebox(draftView.render().el); return $('#facebox_overlay').unbind('click'); }; PopupSend.prototype.focusBody = function (event) { return this.$('.body_content').addClass('focus'); }; PopupSend.prototype.blurBody = function (event) { return this.$('.body_content').removeClass('focus'); }; PopupSend.prototype.headerMoveCursor = function (event) { if ((event.which === 39 || event.which === 40) && event.target.selectionStart === event.target.value.length) { this.$('textarea[name=body_footer]').focus(); event.preventDefault(); return this.$('.body_content').addClass('focus'); } }; PopupSend.prototype.footerMoveCursor = function (event) { if ((event.which === 37 || event.which === 38) && event.target.selectionStart === 0) { this.$('textarea[name=body_header]').focus(); event.preventDefault(); return this.$('.body_content').addClass('focus'); } }; return PopupSend;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.PopupSuccess = (function (_super) {
            __extends(PopupSuccess, _super); function PopupSuccess() { return PopupSuccess.__super__.constructor.apply(this, arguments); }
            PopupSuccess.prototype.template = JST['popup_success']; PopupSuccess.prototype.events = { 'click a.back': 'close', 'click a.redirect': 'clearConfirm', 'click .video-preview': 'playVideo' }; PopupSuccess.prototype.initialize = function () { return window.confirmBeforeUnload = false; }; PopupSuccess.prototype.render = function () { this.$el.html(this.template); return this; }; PopupSuccess.prototype.close = function () { $(document).trigger('close.facebox'); window.confirmBeforeUnload = true; $('.video-player').tubeplayer('stop'); return false; }; PopupSuccess.prototype.clearConfirm = function () { $('.video-player').tubeplayer('stop'); window.onbeforeunload = null; return true; }; PopupSuccess.prototype.playVideo = function () { return $('.success_popup .info').fadeOut(250, function () { return $('.success_popup .video-player').fadeIn(250, function () { var video_id; video_id = $('.video-player').data('video-id'); return $('.video-player').tubeplayer({ initialVideo: video_id, width: $('.video-player').width(), height: $('.video-player').height(), protocol: 'https', allowFullScreen: true, autoPlay: true }); }); }); }; return PopupSuccess;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.PopupSuccessDraft = (function (_super) {
            __extends(PopupSuccessDraft, _super); function PopupSuccessDraft() { return PopupSuccessDraft.__super__.constructor.apply(this, arguments); }
            PopupSuccessDraft.prototype.template = JST['popup_success_draft']; PopupSuccessDraft.prototype.events = { 'click a.back': 'close', 'click a.redirect': 'clearConfirm', 'click .video-preview': 'playVideo' }; PopupSuccessDraft.prototype.initialize = function () { return window.confirmBeforeUnload = false; }; PopupSuccessDraft.prototype.render = function () { this.$el.html(this.template); return this; }; PopupSuccessDraft.prototype.close = function () { $(document).trigger('close.facebox'); $('.video-player').tubeplayer('stop'); window.confirmBeforeUnload = true; return false; }; PopupSuccessDraft.prototype.clearConfirm = function () { this.$('.video-player').tubeplayer('stop'); window.onbeforeunload = null; return true; }; PopupSuccessDraft.prototype.playVideo = function () { return $('.success_popup .info').fadeOut(250, function () { return $('.success_popup .video-player').fadeIn(250, function () { var video_id; video_id = $('.video-player').data('video-id'); return $('.video-player').tubeplayer({ initialVideo: video_id, width: $('.video-player').width(), height: $('.video-player').height(), allowFullScreen: true, protocol: 'https', autoPlay: true }); }); }); }; return PopupSuccessDraft;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.SuperCharge = (function (_super) {
            __extends(SuperCharge, _super); function SuperCharge() { return SuperCharge.__super__.constructor.apply(this, arguments); }
            SuperCharge.prototype.template = JST['popup_supercharge']; SuperCharge.prototype.events = { 'click a[data-action="continue"]': 'close', 'click a[data-action="save-and-send"]': 'sendInvoice' }; SuperCharge.prototype.render = function () { var _this = this; $('#facebox a.close').on('click', function (e) { if (!$('#terms').hasClass('fullAccess')) { InvoiceCreator.events.trigger('markStepComplete', 3); (new InvoiceCreator.Views.SuperChargeTips).showTips(); return false; } else { return _this.$('#work .submit').show(); } }); this.$el.html(this.template); return this; }; SuperCharge.prototype.close = function () { $(document).trigger('close.facebox'); InvoiceCreator.events.trigger('markStepComplete', 3); (new InvoiceCreator.Views.SuperChargeTips).showTips(); window.confirmBeforeUnload = true; return false; }; SuperCharge.prototype.sendInvoice = function () { InvoiceCreator.events.trigger('markStepComplete', 3, 'superChargeSend'); InvoiceCreator.events.trigger('sendInvoice'); return false; }; return SuperCharge;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.PopupTaxes = (function (_super) {
            __extends(PopupTaxes, _super); function PopupTaxes() { return PopupTaxes.__super__.constructor.apply(this, arguments); }
            PopupTaxes.prototype.template = JST['popup_taxes']; PopupTaxes.prototype.events = { 'click a.add': 'addTaxLine', 'click a.remove': 'removeTaxLine', 'click a.save': 'saveTaxes', 'click a.cancel': 'closePopup', 'mouseleave .body tr': 'hideRemoveLink', 'mouseover .body tr': 'showRemoveLink' }; PopupTaxes.prototype.initialize = function (options) { return this.taxView = options.taxView; }; PopupTaxes.prototype.render = function () { $(this.el).html(this.template({ taxes: InvoiceCreator.invoice.availableTaxes() })); this.$('tbody.custom').html(JST['popup_taxes_line']); return this; }; PopupTaxes.prototype.addTaxLine = function (event) { event.preventDefault(); return this.$('tbody.taxes').append(JST['popup_taxes_line']({ tax: null })); }; PopupTaxes.prototype.removeTaxLine = function (event) { var row; event.preventDefault(); $('label[for],input').qtip('destroy'); row = $($(event.target).closest('tr')); row.data('destroy', true); return row.hide('fast'); }; PopupTaxes.prototype.saveTaxes = function (event) {
                var newTax; event.preventDefault(); if (!this.validate()) { return; }
                newTax = null; _.each(this.$('tbody.taxes tr'), function (row) {
                    var name, number, rate, tax, taxCid; taxCid = $(row).data('cid'); name = $('input[name=name]', row).val(); rate = parseFloat($('input[name=rate]', row).val()); number = $('input[name=number]', row).val(); if (name === 'VAT') { InvoiceCreator.invoice.company.set('vat_number', number); InvoiceCreator.events.trigger("vat_number_changed"); }
                    if ($(row).data('destroy')) { if (tax = InvoiceCreator.invoice.taxes.getByCid(taxCid)) { InvoiceCreator.invoice.taxes.remove(tax); return tax.destroy(); } } else if (name && rate) { if (taxCid) { tax = InvoiceCreator.invoice.taxes.getByCid(taxCid); return tax.set({ name: name, rate: rate, number: number }); } else { newTax = new InvoiceCreator.Models.Tax({ name: name, rate: rate, number: number, custom: true }); return InvoiceCreator.invoice.taxes.push(newTax); } }
                }); if (newTax) { this.taxView.selectTax(null, newTax); }
                InvoiceCreator.events.trigger('updateTotals'); return this.closePopup();
            }; PopupTaxes.prototype.hideRemoveLink = function (e) { return $(e.currentTarget).find('a.remove').css('display', 'none'); }; PopupTaxes.prototype.showRemoveLink = function (e) { return $(e.currentTarget).find('a.remove').css('display', 'block'); }; PopupTaxes.prototype.validate = function () {
                var has_errors; this.$('input').removeClass('error'); $('label[for],input').qtip('destroy'); has_errors = false; _.each(this.$('tbody.taxes tr'), function (row) {
                    var error, errors, key, name, position, rate, taxCid, _results; taxCid = $(row).data('cid'); name = $('input[name=name]', row).val(); rate = $('input[name=rate]', row).val(); errors = {}; if (!$(row).data('destroy') && (name || rate)) {
                        if (!name) { errors['input[name=name]'] = 'This field can’t be empty'; }
                        if (!(parseFloat(rate) && parseFloat(rate) < 100 && parseFloat(rate) > 0)) { errors['input[name=rate]'] = 'Oops! This tax rate is invalid'; }
                    }
                    if (Object.size(errors) > 0) {
                        has_errors = true; _results = []; for (key in errors) {
                            error = errors[key]; position = { my: 'bottom center', at: 'top center' }; if (key.indexOf('name=name') !== -1) { position = { my: 'right center', at: 'left center' }; }
                            if (key.indexOf('name=rate') !== -1) { position = { my: 'left center', at: 'right center' }; }
                            $(key, row).qtip({ content: { text: error }, position: position, show: { ready: true }, style: { classes: 'ui-tooltip-red ui-tooltip-rounded' } }); _results.push($(key, row).closest('input').addClass('error'));
                        }
                        return _results;
                    }
                }); return !has_errors;
            }; PopupTaxes.prototype.closePopup = function (event) {
                if (event) { event.preventDefault(); }
                return $(document).trigger('close.facebox');
            }; return PopupTaxes;
        })(Backbone.View);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Work.Line = (function (_super) {
            __extends(Line, _super); function Line() { this.saveAttribute = __bind(this.saveAttribute, this); this.enforceFormat = __bind(this.enforceFormat, this); return Line.__super__.constructor.apply(this, arguments); }
            Line.prototype.tagName = 'tr'; Line.prototype.className = 'line'; Line.prototype.template = JST['work_line']; Line.prototype.events = { 'focus input, textarea': 'addHighlight', 'blur input, textarea': 'removeHighlight', 'click td': 'focusInput', 'keypress input[name=cost], input[name=quantity]': 'enforceNumbers', 'paste input[name=cost], input[name=quantity]': 'enforceFormat', 'change input, textarea': 'saveAttribute', 'blur input[name=cost], input[name=quantity]': 'triggerValidation' }; Line.prototype.initialize = function () { this.taxesSelectViews = {}; return this.autoSave(); }; Line.prototype.onClose = function () { return window.clearTimeout(this.autoSaveTimer); }; Line.prototype.autoSave = function () { var _this = this; this.$('input[name=cost],input[name=quantity]').trigger('change'); return this.autoSaveTimer = window.setTimeout(function () { return _this.autoSave(); }, 1000); }; Line.prototype.render = function () { var _this = this; this.$el.attr('data-cid', this.model.cid); this.$el.attr('id', "line_" + this.model.cid); this.$el.html(this.template(this.model.attributes)); _.each(['tax_1', 'tax_2'], function (type) { return (new InvoiceCreator.Views.Work.Tax({ line: _this.model, line_view: _this, type: type })).render(); }); InvoiceCreator.events.trigger('resizeWorkHighlight'); $('#totals tr.total').qtip('reposition'); return this; }; Line.prototype.triggerValidation = function (event) { return InvoiceCreator.events.trigger('displayWorkValidation'); }; Line.prototype.enforceNumbers = function (event) { var validModifiers, validNumber; validModifiers = function () { var allowedKeys; allowedKeys = [45, 8, 39, 9, 0]; return _.include(allowedKeys, event.which); }; validNumber = function () { var number; number = $(event.target).val() + String.fromCharCode(event.which); return number.match(/^\-?([0-9]+)?(\.([0-9]+)?)?$/) !== null; }; return validModifiers() || validNumber(); }; Line.prototype.enforceFormat = function (e) { var _this = this; return setTimeout(function () { if (!$(e.currentTarget).val().match(/^(-?[0-9]+\.[0-9]{1,2}(, )?)+$/g)) { return $(e.currentTarget).val(''); } }, 100); }; Line.prototype.saveAttribute = function (event) {
                var cid, field, line, value; field = $(event.target); cid = field.closest('tr').data('cid'); line = InvoiceCreator.invoice.lines.getByCid(cid); value = $.trim(field.val()); if (line) { line.set(field.attr('name'), value); }
                return this.updateTotal();
            }; Line.prototype.updateTotal = function () { var total; total = this.model.subtotal().format(2); if (this.lastTotal !== total) { this.$('input[name=total]').val(total); this.lastTotal = total; return InvoiceCreator.events.trigger('updateTotals'); } }; Line.prototype.addHighlight = function (event) { return $(event.target).closest('td').addClass('active'); }; Line.prototype.removeHighlight = function (event) { return $(event.target).closest('td').removeClass('active'); }; Line.prototype.focusInput = function (event) { return $(event.target).find('input, textarea').focus(); }; return Line;
        })(Backbone.View);
    }).call(this); (function () {
        var __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; }, __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Work.Tax = (function (_super) {
            __extends(Tax, _super); function Tax() { this.navigateTaxesSelect = __bind(this.navigateTaxesSelect, this); return Tax.__super__.constructor.apply(this, arguments); }
            Tax.prototype.template = JST['work_tax']; Tax.prototype.events = { 'focus input': 'renderDropdown', 'blur input': 'removeDropdown', 'keydown input': 'navigateTaxesSelect', 'click li.enabled': 'selectTax', 'mouseover li.enabled': 'hoverEffect' }; Tax.prototype.initialize = function (options) { this.tax = null; this.line = options.line; this.line_view = options.line_view; this.type = options.type; return this.open = false; }; Tax.prototype.render = function () { $("td." + this.type, this.line_view.el).html($(this.el).html(this.template())); return this; }; Tax.prototype.renderDropdown = function () {
                if (this.open) { return; }
                this.$('.dropdown').html(JST['work_tax_dropdown']({ taxes: InvoiceCreator.invoice.availableTaxes(), line: this.line, type: this.type })); this.$('.dropdown').show().addClass('open'); return this.open = true;
            }; Tax.prototype.removeDropdown = function () {
                if (!this.open) { return; }
                $('.dropdown').fadeOut().removeClass('open'); this.open = false;
            }; Tax.prototype.hoverEffect = function (event) { this.$('li').removeClass('active'); return $(event.target).addClass('active'); }; Tax.prototype.selectTax = function (event, tax) {
                var old_tax, taxCid, taxesView; if (tax) { this.tax = tax; }
                if (event) { event.preventDefault(); }
                if (old_tax = this.line.get(this.type)) { old_tax.off('change:name', this.refreshTax, this); old_tax.off('destroy', this.clearTax, this); }
                if (event && (taxCid = $(event.target).data('cid'))) { this.tax = InvoiceCreator.invoice.taxes.getByCid(taxCid); } else { InvoiceCreator.events.trigger('warlock', 'hide'); taxesView = new InvoiceCreator.Views.PopupTaxes({ taxView: this }); $.facebox(taxesView.render().el); $('#facebox_overlay').unbind('click'); }
                if (this.tax) { this.tax.on('change:name', this.refreshTax, this); this.tax.on('destroy', this.clearTax, this); }
                this.refreshTax(); return InvoiceCreator.events.trigger('updateTotals');
            }; Tax.prototype.refreshTax = function () { this.line.set(this.type, this.tax); return $("td." + this.type + " input", this.line_view.el).val(this.tax ? this.tax.get('name') : ''); }; Tax.prototype.clearTax = function () { this.line.set(this.type, null); return $("td." + this.type + " input", this.line_view.el).val(''); }; Tax.prototype.navigateTaxesSelect = function (event) {
                var next, prev, selected; selected = this.$('li.active'); switch (event.which) {
                    case 38: if ((prev = $(selected.prevAll('.enabled'))).length) { prev.first().addClass('active'); return selected.removeClass('active'); }
                        break; case 40: if ((next = $(selected.nextAll('.enabled'))).length) { next.first().addClass('active'); return selected.removeClass('active'); }
                            break; case 13: selected.click(); if (this.open) { return this.removeDropdown(); } else { return this.renderDropdown(); }
                }
            }; return Tax;
        })(Backbone.View);
    }).call(this); (function () {
        var __hasProp = {}.hasOwnProperty, __extends = function (child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; }; InvoiceCreator.Views.Work.Totals = (function (_super) {
            __extends(Totals, _super); function Totals() { return Totals.__super__.constructor.apply(this, arguments); }
            Totals.prototype.el = '#totals'; Totals.prototype.template = JST['work_totals']; Totals.prototype.initialize = function () { InvoiceCreator.events.on('updateTotals', this.updateTotals, this); InvoiceCreator.events.on('updateCurrency', this.setCurrency, this); InvoiceCreator.events.on('startStep1', this.disableSection, this); InvoiceCreator.events.on('startStep2', this.disableSection, this); return InvoiceCreator.events.on('startStep3', this.enableSection, this); }; Totals.prototype.render = function () { $(this.el).addClass('disabled').html(this.template); this.setCurrency(); return this; }; Totals.prototype.updateTotals = function () { this.$('tr.subtotal span.amount').html("" + (InvoiceCreator.invoice.subtotal().format(2))); this.$('tr.total span.amount').html("" + (InvoiceCreator.invoice.total().format(2))); this.$('tbody.taxes').html(JST['work_totals_taxes']({ taxes: InvoiceCreator.invoice.totalTaxes() })); this.$('tr.total').qtip('reposition'); if (InvoiceCreator.invoice.total() > 0) { return this.$('tr.total').qtip('destroy'); } }; Totals.prototype.setCurrency = function () { this.$('span.currency_code').html(InvoiceCreator.invoice.escape('currency')); return this.$('span.currency_symbol').html(InvoiceCreator.CURRENCIES[InvoiceCreator.invoice.escape('currency')].symbol); }; Totals.prototype.disableSection = function () { return this.$el.removeClass('enabled').addClass('disabled'); }; Totals.prototype.enableSection = function () { if (InvoiceCreator.invoice.client.isValid()) { return $(this.el).removeClass('disabled'); } }; return Totals;
        })(Backbone.View);
    }).call(this);
}).call(this);