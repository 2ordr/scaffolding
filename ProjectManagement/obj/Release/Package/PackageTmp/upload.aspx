﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="upload.aspx.cs" Inherits="ProjectManagement.upload" %>

<!DOCTYPE html>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Files.Count == 1)
        {
            myFile.PostedFile.SaveAs(Server.MapPath(".") +
                "\\upload\\myUploadFile.txt");
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


    <script type="text/javascript" src="Upload_Script/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="Upload_Script/swfobject.js"></script>
    <script type="text/javascript" src="Upload_Script/jquery.uploadify.v2.1.4.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>
   
    <title>Upload</title>
    <style>
        h1 {
            font-family: Helvetica;
            font-weight: 100;
        }

        body {
            color: #333;
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        function SubmitForm() {
            // Simply, submit the form
            document.form1.submit();
        }

    </script>

  
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <input type="file" runat="server" id="myFile" name="myFile" style="visibility: hidden;" />
               
            <input type="button" runat="server" id="btnSubmit" name="btnSubmit" onclick="javascript: SubmitForm();" style="visibility: hidden;" />
               
            <br />
            <asp:Label ID="lblMsg" runat="server" ForeColor="red" Font-Size="Medium" Font-Bold="true"></asp:Label>
               

        </div>
    </form>
</body>
</html>
