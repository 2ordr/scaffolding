﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Wizardss.aspx.cs" Inherits="ProjectManagement.Wizardss" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>

    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-4/styles.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style>
        .simple {
            color: lightblue;
        }

            .simple:hover {
                color: blue;
            }

        #wizHeader li .prevStep {
            background-color: #669966;
        }

            #wizHeader li .prevStep:after {
                border-left-color: #669966 !important;
            }

        #wizHeader li .currentStep {
            background-color: #C36615;
        }

            #wizHeader li .currentStep:after {
                border-left-color: #C36615 !important;
            }

        #wizHeader li .nextStep {
            background-color: #fbfbfb;
        }

            #wizHeader li .nextStep:after {
                border-left-color: #fbfbfb;
            }

        #wizHeader {
            list-style: none;
            overflow: hidden;
            font: 18px Helvetica, Arial, Sans-Serif;
            margin: 7px;
            padding: 5px;
        }

            #wizHeader li {
                float: left;
            }

                #wizHeader li a {
                    color: #fbfbfb;
                    text-decoration: none;
                    padding: 10px 0 10px 55px;
                    background: brown; /* fallback color */
                    background: hsla(34,85%,35%,1);
                    position: relative;
                    display: block;
                    float: left;
                }

                    #wizHeader li a:after {
                        content: " ";
                        display: block;
                        width: 0;
                        height: 0;
                        border-top: 50px solid transparent; /* Go big on the size, and let overflow hide */
                        border-bottom: 50px solid transparent;
                        border-left: 30px solid hsla(34,85%,35%,1);
                        position: absolute;
                        top: 50%;
                        margin-top: -50px;
                        left: 100%;
                        z-index: 2;
                    }

                    #wizHeader li a:before {
                        content: " ";
                        display: block;
                        width: 0;
                        height: 0;
                        border-top: 50px solid transparent;
                        border-bottom: 50px solid transparent;
                        border-left: 30px solid #fbfbfb;
                        position: absolute;
                        top: 50%;
                        margin-top: -50px;
                        margin-left: -1px;
                        left: 105%;
                        z-index: 1;
                    }

                #wizHeader li:first-child a {
                    padding-left: 10px;
                }

                #wizHeader li:last-child {
                    padding-right: 50px;
                }

                #wizHeader li a:hover {
                    background: #FE9400;
                }

                    #wizHeader li a:hover:after {
                        border-left-color: #FE9400 !important;
                    }

        .content {
            padding-top: 75px;
            text-align: center;
            background-color: #F9F9F9;
            font-size: 48px;
        }

        .Calendar .ajax__calendar_container {
            border: 1px solid #E0E0E0;
            background-color: #FAFAFA;
            width: 200px;
        }

        .Calendar .ajax__calendar_header {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 12px;
            text-align: center;
            color: #9F9F9F;
            font-weight: normal;
            text-shadow: 0px 0px 2px #D3D3D3;
            height: 20px;
        }

        .Calendar .ajax__calendar_title,
        .Calendar .ajax__calendar_next,
        .Calendar .ajax__calendar_prev {
            color: #004080;
        }

        .Calendar .ajax__calendar_body {
            width: 175px;
            height: 150px;
            position: relative;
        }

        .Calendar .ajax__calendar_dayname {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            color: #FA9900;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            background-color: #EDEDED;
            border: solid 1px #D3D3D3;
            text-transform: uppercase;
            margin: 1px;
        }

        .Calendar .ajax__calendar_day {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            border: solid 1px #E0E0E0;
            text-transform: uppercase;
            margin: 1px;
            width: 17px !important;
            color: #9F9F9F;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_day,
        .Calendar .ajax__calendar_hover .ajax__calendar_month,
        .Calendar .ajax__calendar_hover .ajax__calendar_year,
        .Calendar .ajax__calendar_active {
            color: red;
            font-weight: bold;
            background-color: #ffffff;
        }

        .Calendar .ajax__calendar_year {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_month {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_today {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            text-transform: uppercase;
            margin: 1px;
            color: #6B6B6B;
        }

        .Calendar .ajax__calendar_other {
            background-color: #E0E0E0;
            margin: 1px;
            width: 17px;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_today,
        .Calendar .ajax__calendar_hover .ajax__calendar_title {
        }

        .Calendar .ajax__calendar_footer {
            width: 175px;
            border: none;
            height: 20px;
            vertical-align: middle;
            color: #6B6B6B;
        }

        .w {
            /*Style="z-index: 103; position: absolute; top: 280px;left: 850;"*/
            position: absolute;
            top: 80px;
            left: 1590px;
            z-index: 103;
        }

        .z {
            /*Style="z-index: 103; position: absolute; top: 280px;left: 850;"*/
            position: absolute;
            top: 80px;
            left: 1560px;
            z-index: 103;
        }

        .y {
            position: absolute;
            top: 80px;
            left: 1640px;
            z-index: 103;
        }

        .x {
            position: absolute;
            top: 80px;
            left: 1610px;
            z-index: 103;
        }

        .bg {
            text-align: right;
        }

        .under {
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: 1;
        }

        .over {
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: 2;
        }

        .textboxalign {
            text-align: center;
        }
    </style>
    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>

    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>
    <script type="text/javascript">
        function PrintGridDataAll() {

            var prtGrid = document.getElementById("print111");
            var prtwin = window.open('', 'PrintDivData', 'left=100,top=10,width=730,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
            prtwin.document.writeln();
            prtwin.document.write(prtGrid.outerHTML);
            prtwin.document.close();
            prtwin.focus();
            prtwin.print();
            prtwin.close();
        }
    </script>
    <script type="text/javascript">

        function DisableBackButton() { //for disabling background browser button
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>


</head>

<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server" method="post" autocomplete="off" enctype="multipart/form-data">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" ScriptMode="Debug"></asp:ToolkitScriptManager>

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                if (args.get_error() != undefined) {
                    args.set_errorHandled(true);
                }
            }
        </script>

        <div style="height: 1000px; width: 1920px;">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 1000px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 1000px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtSearchBox" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150" OnTextChanged="txtSearchBox_TextChanged"></asp:TextBox>

                                <asp:FilteredTextBoxExtender runat="server" ID="FEx2" Enabled="true" TargetControlID="txtSearchBox" FilterType="Numbers" FilterMode="ValidChars"></asp:FilteredTextBoxExtender>
                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 1000px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>


                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">

                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%-- <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>--%>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div1')">Rapporter</span></span>

                                                    <div id="Div1" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div3')">Løn Modul</span></a>
                                                    <div id="Div3" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>

                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>--%>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div style="height: 300px; border-color: White;">
                                    <table style="height: 300px; width: 100%">
                                        <tr style="height: 100px;">
                                            <td></td>
                                        </tr>
                                        <tr style="height: 50px;">
                                            <td>
                                                <div>
                                                    &nbsp   
                                                    <asp:Label ID="Label2" runat="server" Text="Timer" ForeColor="White"></asp:Label>
                                                </div>
                                                <div>

                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                                                        <ContentTemplate>
                                                            &nbsp
                                                            <asp:Label ID="lbldisplayTime" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="white">00:00:00</asp:Label>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                                        </Triggers>

                                                    </asp:UpdatePanel>

                                                    <asp:Button ID="BtnStarttime" runat="server" Text="Start" CssClass="button" Width="40" Height="30" BackColor="#009900" OnClick="BtnStarttime_Click" />
                                                    <asp:Button ID="BtnStopttime" runat="server" Text="stop" CssClass="button" Width="40" Height="30" BackColor="#009900" OnClick="BtnStopttime_Click" />

                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtHrs" runat="server" Height="30" Width="70"></asp:TextBox>
                                                    <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick"></asp:Timer>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 100px;">
                                            <td>
                                                <ul>
                                                    <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                        <asp:Label ID="Label1" runat="server" Text="" ForeColor="White"></asp:Label>
                                                    </span></a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <div class="allSides" style="width: 1725px; float: left; height: 70px;">

                            <table>
                                <tr>
                                    <td style="width: 2%;"></td>
                                    <td style="width: 92%;">
                                        <asp:Label ID="Label3" runat="server" Font-Bold="true" Font-Size="x-Large" Text="TILBUD"></asp:Label>
                                        <asp:Label ID="Label5" runat="server" Text="Label" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label>
                                        <asp:Label ID="Label6" runat="server" Text="Label" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label>
                                    </td>
                                    <td style="width: 3%;"></td>
                                    <td style="width: 3%;"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="" style="width: 1725px; float: left; height: 820px;">

                            <asp:Wizard ID="Wizard1" runat="server" Width="100%" ActiveStepIndex="0" DisplaySideBar="false" OnNextButtonClick="Wizard1_NextButtonClick" OnFinishButtonClick="Wizard1_FinishButtonClick" StartNextButtonText="Nyt tilbud" FinishCompleteButtonText="Godkend" FinishPreviousButtonText="Forrige">

                                <NavigationButtonStyle CssClass="button" BackColor="Green" />
                                <StartNextButtonStyle CssClass="buttonn  x" />
                                <StepNavigationTemplate>
                                    <asp:Button CssClass="buttonn w" ID="StepPreviousButton" runat="server" CommandName="MovePrevious" Text="Vis" BackColor="Green" />
                                    <asp:Button CssClass="buttonn y" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Naste" BackColor="Green" OnClick="StepNextButton_Click" ValidationGroup="NextButtonCLick" />
                                </StepNavigationTemplate>
                                <FinishPreviousButtonStyle CssClass="buttonn z" />
                                <FinishCompleteButtonStyle CssClass="buttonn y" />

                                <%--  for Preview=Forrige , finish=Godkend--%>

                                <WizardSteps>
                                    <asp:WizardStep ID="w1" runat="server" Title="Oversigt" StepType="Start">
                                        <div id="HeaderDiv11" runat="server" class="allSides" style="width: 1725px;" visible="true">
                                            <table border="0" style="font-family: Calibri; width: 100%;">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 3%; font-size: large;">Antal</td>
                                                    <td style="width: 16.7%; font-size: large;">Navn</td>
                                                    <td style="width: 10%; font-size: large;">Dato</td>
                                                    <td style="width: 21.8%; font-size: large;">Kundenavn</td>
                                                    <td style="width: 13%; font-size: large;">Godkendt af Admin</td>
                                                    <td style="width: 13%; font-size: large;">Bekræftelsesdato</td>
                                                    <td style="width: 5%; font-size: large;">Total</td>
                                                    <td style="width: 4%; font-size: large;">Vis</td>
                                                    <td style="width: 5.5%; font-size: large;">Rediger</td>
                                                    <td style="width: 4%; font-size: large;">Slet</td>
                                                    <td style="width: 4%; font-size: large;">Print</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="divquote" runat="server" style="overflow: auto; height: 780px; width: 1725px">
                                            <table style="width: 100%; font-family: Calibri;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gridviewquatationView" runat="server" AutoGenerateColumns="False" ShowHeader="false" DataKeyNames="QUOTID" HeaderStyle-BackColor="#F2F2F2" Width="1725px" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" RowStyle-BorderColor="White" RowStyle-BorderWidth="5px" BorderWidth="0px" OnRowDataBound="Bound" OnSelectedIndexChanged="GridView3_SelectedIndexChanged">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="20px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="QUOTID" runat="server" Text='<%# Bind("QUOTID") %>' Visible="true"></asp:Label>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Visible="false" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle BorderColor="White" BorderWidth="5px" Width="20px"></HeaderStyle>
                                                                    <ItemStyle Width="2.7%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="QuoteName" HeaderText=" Tilbudnavn">
                                                                    <ItemStyle BorderColor="White" BorderWidth="5px" Width="16.7%"></ItemStyle>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="DateTime" HeaderText=" Date">
                                                                    <ItemStyle BorderColor="White" BorderWidth="5px" Width="10%"></ItemStyle>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="CustName" HeaderText="CustomerName">
                                                                    <ItemStyle BorderColor="White" BorderWidth="5px" Width="22%"></ItemStyle>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="AdminApproval" HeaderText="Admin_Approval">
                                                                    <ItemStyle BorderColor="White" BorderWidth="5px" Width="13%"></ItemStyle>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="RespondTime" HeaderText="Respond_Time">
                                                                    <ItemStyle BorderColor="White" BorderWidth="5px" Width="13%"></ItemStyle>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="TotalCost" HeaderText="TotalCost">
                                                                    <ItemStyle BorderColor="White" BorderWidth="5px" Width="8"></ItemStyle>
                                                                </asp:BoundField>

                                                                <asp:TemplateField HeaderText="View" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LikButEdit" runat="server" Font-Underline="false" OnClick="LikButEdit_Click"><span class="menu-item-icon icon-eye"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="4%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Edit" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LinkButtonTemplate" runat="server" Font-Underline="false" OnClick="LinkButtonTemplate_Click"><span class="menu-item-icon icon-edit-write"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5.5%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Delete" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <span onclick="return confirm('Are you sure to Delete the record?')">
                                                                            <asp:LinkButton CssClass="simple" ID="Lnkdeleterow" runat="server" Font-Underline="false" OnClick="Lnkdeleterow_Click"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="4%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Print" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LinkButton1" runat="server" CommandName="Print" CausesValidation="false" Font-Underline="false" OnClick="LinkButton1_Click"><img src="image/PrintLogo.jpg" height="20" width="20" class="simple"/></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="4%" />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:WizardStep>
                                    <asp:WizardStep ID="w2" runat="server" Title="Trin1: Valg kunde" StepType="Step">
                                        <table style="width: 1700px; height: 820px;">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 40%; vertical-align: top;">
                                                        <fieldset runat="server" id="fieldBasicInfo" style="height: 800px; width: 100%">

                                                            <legend style="color: black; font-weight: bold;">Stamdata</legend>
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td style="width: 250px" class="bg">Dato                                           
                                                                    </td>
                                                                    <td style="width: 5px"></td>
                                                                    <td style="">
                                                                         <asp:Label ID="lblDateTime" runat="server" Text="" Visible="false"></asp:Label>

                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDate" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtCompareDate" runat="server" Style="display: none"></asp:TextBox>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>

                                                                <tr>
                                                                    <td class="bg">Kundenavn                                           
                                                                    </td>
                                                                    <td style="width: 5px"></td>
                                                                    <td style="">
                                                                        <asp:Panel runat="server" DefaultButton="SearchButton">

                                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName" runat="server" Width="300px" Height="31px" Visible="false"></asp:TextBox>
                                                                            <asp:Label ID="lblCustId" runat="server" Text="" Visible="false"></asp:Label>
                                                                            <asp:Button ID="SearchButton" runat="server" CssClass="buttonn" OnClick="SearchButton_Click" Style="display: none;"></asp:Button>

                                                                            <asp:DropDownList ID="DropDownUserList" runat="server" CssClass="search_categories" Font-Size="Small" Width="300px" Height="40px" OnSelectedIndexChanged="DropDownUserList_SelectedIndexChanged" AutoPostBack="true">
                                                                            </asp:DropDownList>

                                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtName" ValidationGroup="NextButtonCLick"></asp:RequiredFieldValidator>--%>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg">Bekræftelse</td>
                                                                    <td style="width: 20px"></td>
                                                                    <td>
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtResTime" runat="server" Width="300px" Height="31px" Visible="true"></asp:TextBox>
                                                                        <%--  <cc1:DatePicker ID="datepicker1" runat="server" Width="300px" DateFormat="dd-MM-yyyy" Height="36px" Visible="false"></cc1:DatePicker>--%>
                                                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtResTime" CssClass="Calendar" Format="dd-MM-yyyy" Enabled="true"></asp:CalendarExtender>

                                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom, Numbers" ValidChars="-/" TargetControlID="txtResTime"></asp:FilteredTextBoxExtender>

                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtResTime" ErrorMessage="*" ValidationGroup="NextButtonCLick" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Enter Valid Date" Font-Size="Small" Type="Date" Display="Dynamic" Operator="GreaterThanEqual" ControlToValidate="txtResTime" ControlToCompare="txtCompareDate" ValidationGroup="NextButtonCLick"></asp:CompareValidator>

                                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Enter Valid Date" Font-Size="Small" Type="Date" Operator="DataTypeCheck" ControlToValidate="txtResTime" ValidationGroup="NextButtonCLick"></asp:CompareValidator>

                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>

                                                                <tr>
                                                                    <td class="bg">Customer Ack</td>
                                                                    <td style="width: 5px"></td>
                                                                    <td>
                                                                        <asp:DropDownList CssClass="search_categories" ID="ddlCustAck" Font-Size="Small" runat="server" Width="300px" Height="36px">
                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                            <asp:ListItem>No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg">Godkendt af Admin</td>
                                                                    <td style="width: 5px"></td>
                                                                    <td>

                                                                        <asp:DropDownList CssClass="search_categories" ID="ddlAdminApp" Font-Size="Small" runat="server" Width="300px" Height="36px">
                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                            <asp:ListItem>No</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg">Status</td>
                                                                    <td style="width: 5px"></td>
                                                                    <td>

                                                                        <asp:DropDownList CssClass="search_categories" ID="ddquotestatus" Font-Size="Small" runat="server" Width="300px" Height="36px">
                                                                            <%--   <asp:ListItem>Draft</asp:ListItem>
                                                                    <asp:ListItem>Send</asp:ListItem>--%>
                                                                            <asp:ListItem>Accepted</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <%-- <tr>
                                                            <td style="height: 20px;" colspan="3"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right;">Quote Possibility</td>
                                                            <td style="width: 5px"></td>
                                                            <td>
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuoteposs" runat="server" Width="300px" Height="31px" TextMode="Number" Visible="false"></asp:TextBox>
                                                            </td>
                                                        </tr>--%>

                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg">Total Cost::</td>
                                                                    <td style="width: 5px"></td>
                                                                    <td>
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtResCost" runat="server" Width="300px" Height="31px" Visible="true" Enabled="false"></asp:TextBox>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtResCost" ErrorMessage="*" ValidationGroup="NextButtonCLick"></asp:RequiredFieldValidator>--%>

                                                                        <asp:Label ID="LBLResCost" runat="server" Text="" Visible="false" Font-Underline="true"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                            </table>

                                                            <%--23_June--Prathamesh
                                                            Design added for uploading files and view them.--%>

                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">

                                                                <ContentTemplate>

                                                                    <div style="height: 40px; margin-left: 230px;">
                                                                        <div class="divider"></div>
                                                                        <asp:Button CssClass="buttonn" ID="Button6" runat="server" Text="Back" BackColor="#009933" Visible="false" />

                                                                        <asp:Label ID="Quot_ID" runat="server" Visible="false"></asp:Label>

                                                                        <asp:FileUpload ID="FileUpload2" runat="server" />
                                                                        <asp:Button CssClass="buttonn" ID="btnUpload" OnClick="btnUpload_Click" runat="server" Text="Vedhæft" />

                                                                        <div class="divider"></div>
                                                                    </div>
                                                                    <div style="height: 30px"></div>

                                                                    <div id="FileUploadDiv11" runat="server" style="height: 400px;" >
                                                                        <div class="allSides" style="width: 100%;">
                                                                            <table border="0" style="font-family: Calibri;">
                                                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                                                    <td style="width: 5px; font-weight: 700"></td>
                                                                                    <td style="width: 320px; font-weight: 700">Filnavn</td>
                                                                                    <td style="width: 200px; font-weight: 700">Aben</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div runat="server" style="height: 300px; overflow: auto;">
                                                                            <table style="font-family: Calibri;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:GridView ID="gvFileUpload" runat="server" ShowHeader="false" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px">
                                                                                            <Columns>

                                                                                                <asp:BoundField DataField="Quot_ID" HeaderText="Quot_ID" Visible="false" />
                                                                                                <asp:BoundField DataField="FileName" HeaderText="FileName" ItemStyle-Width="350px" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                                                <%--     <asp:BoundField DataField="FilePath" HeaderText="FilePath" ItemStyle-Width="100" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />--%>
                                                                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px" Visible="false">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblFilePath" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Width="600px" BorderWidth="5" BorderColor="White" />
                                                                                                </asp:TemplateField>

                                                                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton CssClass="simple" ID="lbtnViewImage" runat="server" OnClick="lbtnViewImage_Click" Font-Underline="false"><span class="action-icon icon-eye"></asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Width="500px" BorderWidth="5" BorderColor="White" />
                                                                                                </asp:TemplateField>

                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>

                                                                    </div>

                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnUpload" />

                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </fieldset>

                                                    </td>

                                                    <td style="width: 45%; vertical-align: top;">
                                                        <div style="height: 820px">
                                                            <%--<fieldset runat="server" id="fieldsetcustomer" style="height: 800px; width: 100%">

                                                        <legend style="color: black; font-weight: bold; font-size: large">Basic Info</legend>--%>

                                                            <table style="width: 100%; font-family: Calibri; margin-top: 1px;">
                                                                <tbody>

                                                                    <tr>
                                                                        <td>
                                                                            <%--<div id="div1" runat="server" visible="false">
                                                                        <table style="margin-left: 4px;">
                                                                            <tr style="background-color: #F2F2F2; border-width: 5px;">
                                                                                <td style="border-color: white">Customer Name</td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>--%>
                                                                            <div id="divCustlist" runat="server" style="overflow: auto;" visible="true">

                                                                                <table style="width: 100%; font-family: Calibri; margin-top: 2px;" runat="server" visible="false">
                                                                                    <tbody>

                                                                                        <tr>
                                                                                            <td style="width: 100%;">
                                                                                                <asp:GridView ID="GridViewCustDetails" runat="server" ShowHeader="false" DataKeyNames="CustId" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                                    <%--OnRowDataBound="GridViewCustDetails_RowDataBound" OnSelectedIndexChanged="GridViewCustDetails_SelectedIndexChanged"--%>
                                                                                                    <Columns>

                                                                                                        <asp:BoundField DataField="CustName" HeaderText="Customer Name" HeaderStyle-Width="300px" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" Width="300px"></HeaderStyle>
                                                                                                            <ItemStyle BorderColor="White" BorderWidth="5px"></ItemStyle>
                                                                                                        </asp:BoundField>
                                                                                                        <asp:TemplateField>
                                                                                                            <ItemTemplate>
                                                                                                                <asp:Label ID="lblemaill" runat="server" Text='<%# Bind("EmailId") %>' Visible="false"></asp:Label>
                                                                                                                <asp:Label ID="lblAddressLine1" runat="server" Text='<%# Bind("AddressLine1") %>' Visible="false"></asp:Label>
                                                                                                                <asp:Label ID="lblCity" runat="server" Text='<%# Bind("City") %>' Visible="false"></asp:Label>
                                                                                                            </ItemTemplate>
                                                                                                            <ItemStyle BorderColor="White" BorderWidth="5px"></ItemStyle>
                                                                                                        </asp:TemplateField>

                                                                                                        <asp:CommandField SelectText="Select" ShowSelectButton="true" ItemStyle-CssClass="buttonn" Visible="true" ButtonType="Button" ItemStyle-Width="70" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5" />
                                                                                                    </Columns>

                                                                                                    <HeaderStyle BackColor="#F2F2F2" BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                                </asp:GridView>
                                                                                            </td>

                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="height: 10px;"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <%--<tr runat="server">
                                                                                <td runat="server" style="width: 100%;">
                                                                                    <asp:GridView runat="server" AutoGenerateColumns="False" DataKeyNames="CustId" ShowHeader="False" Width="100%" ID="GridViewCustDetails" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" OnRowDataBound="GridViewCustDetails_RowDataBound" OnSelectedIndexChanged="GridViewCustDetails_SelectedIndexChanged">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="CustName" HeaderText="Customer Name">
                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px" Width="300px"></HeaderStyle>

                                                                                                <ItemStyle BorderColor="White" BorderWidth="5px"></ItemStyle>
                                                                                            </asp:BoundField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblemaill" runat="server" Text='<%# Bind("EmailId") %>' Visible="false"></asp:Label>
                                                                                                    <asp:Label ID="lblAddressLine1" runat="server" Text='<%# Bind("AddressLine1") %>' Visible="false"></asp:Label>
                                                                                                    <asp:Label ID="lblCity" runat="server" Text='<%# Bind("City") %>' Visible="false"></asp:Label>

                                                                                                </ItemTemplate>

                                                                                                <ItemStyle BorderColor="White" BorderWidth="5px"></ItemStyle>
                                                                                            </asp:TemplateField>
                                                                                            <asp:CommandField ShowSelectButton="True" ButtonType="Button">
                                                                                                <ItemStyle BorderColor="White" BorderWidth="5px" CssClass="buttonn" Width="70px"></ItemStyle>
                                                                                            </asp:CommandField>
                                                                                        </Columns>

                                                                                        <HeaderStyle BackColor="#F2F2F2" BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                    </asp:GridView>

                                                                                </td>
                                                                            </tr>--%>

                                                                                    <tr runat="server">
                                                                                        <td runat="server" style="height: 10px;"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <div id="divBasicInfo2" runat="server" style="height: 765px" visible="false">

                                                                                <fieldset>

                                                                                    <legend style="color: black; font-weight: bold; font-size: large">Kunder Stamdata:</legend>
                                                                                    <div style="height: 765px; width: 100%">

                                                                                        <table style="width: 100%;">

                                                                                            <tr>
                                                                                                <td class="bg" style="width: 25%">Navn</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName1" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txtName1" ValidationGroup="customer" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtName1" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer"></asp:RegularExpressionValidator>--%>

                                                                                                </td>

                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">e-mail</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmail" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                    <%--<asp:RequiredFieldValidator ID="RfwEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmail" ValidationGroup="customer" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please Enter Valid Email ID" ControlToValidate="txtEmail" CssClass="requiredFieldValidateStyle" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="customer">
                                                                                            </asp:RegularExpressionValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">Adresse</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtAddress" runat="server" Width="300px" Height="50px" Enabled="false"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">By</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtcity" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtcity" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                                                    --%></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">Land</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtcountry" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtcountry" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>--%>

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">Postnr</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtzip" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                    <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtzip" ValidationExpression="\d+" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>--%>

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">Mobil:</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMobile" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtMobile" ValidationExpression="\d+" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">Tif</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtphone" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                    <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtphone" ValidationExpression="\d+" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>--%>

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">Fax</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtFax" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">Website</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtWebsite" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bg">Område</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtProvince" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>

                                                                                              <%-- 30-June Prathamesh
                                                                                            Added new Textbx for adding discount--%>
                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td class="bg">Discount</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDiscount" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                    <asp:Label ID="lblDiscount" runat="server" Visible="false"></asp:Label>
                                                                                                    <asp:Label ID="lblDiscount1" runat="server" Visible="false"></asp:Label>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="height: 20px;" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr id="VatNumber" runat="server">
                                                                                                <td id="td1" runat="server" class="bg">CVR nr:</td>
                                                                                                <td style="width: 20px"></td>
                                                                                                <td colspan="2">
                                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtVatNo" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </fieldset>

                                                                                <asp:GridView CssClass="gridv" ID="GridViewBasicInfo" AutoGenerateColumns="false" Visible="false" runat="server">
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>

                                                            <%--  </fieldset>--%>
                                                        </div>

                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </asp:WizardStep>
                                    <asp:WizardStep ID="w3" runat="server" Title="Trin2:Varenr" StepType="Step">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table style="width: 1700px; height: 830px;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 40%; vertical-align: top;">
                                                                <fieldset id="ItemInfoFieldSet" runat="server" style="width: 100%; height: 830px" visible="true">
                                                                    <legend style="font-weight: 700;"></legend><%--Item Info--%>
                                                                    <div id="headerdiv" runat="server" style="width: 100%;" visible="true">
                                                                        <fieldset style="height: 200px">
                                                                            <legend></legend>
                                                                            <table border="0" id="tblheader" style="width: 100%; border-width: 5px; border-color: white">
                                                                                <tr>
                                                                                    <td style="height: 100px;" colspan="3">
                                                                                        <asp:DropDownList CssClass="search_categories" ID="DropDownListSelect" runat="server" Font-Size="Small" Height="37px" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="DropDownListSelect_SelectedIndexChanged">
                                                                                            <asp:ListItem>-Vælg--</asp:ListItem>
                                                                                            <asp:ListItem>Stock Item(Product)</asp:ListItem>
                                                                                            <asp:ListItem>Stock Item(Service)</asp:ListItem>
                                                                                            <asp:ListItem>Predifined Text</asp:ListItem>
                                                                                            <asp:ListItem>Salutation Text1</asp:ListItem>
                                                                                            <asp:ListItem>Salutation Text2</asp:ListItem>
                                                                                            <asp:ListItem>Salutation Text3</asp:ListItem>
                                                                                            <asp:ListItem>Template</asp:ListItem>

                                                                                            <%-- <asp:ListItem>Empty Line</asp:ListItem>--%>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="TaskIdtblTND" runat="server" style="background-color: #F2F2F2;" visible="false">
                                                                                    <td runat="server" visible="false"></td>
                                                                                    <td style="width: 35%; text-align: left; border-width: 5px; border-color: white">Item Name</td>
                                                                                    <td style="width: 65%; text-align: left; border-width: 5px; border-color: white">Quantity</td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td runat="server" visible="false"></td>
                                                                                    <td>
                                                                                        <asp:DropDownList CssClass="search_categories" ID="ddlItemName1" runat="server" Font-Size="Small" Height="37px" AutoPostBack="true" DataTextField="ItemName" DataValueField="ItemCode" OnSelectedIndexChanged="ddlItemName1_SelectedIndexChanged" Width="250px" Visible="false">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblPrice1" runat="server" Text="" Visible="false"></asp:Label>
                                                                                    </td>

                                                                                    <td>
                                                                                        <asp:Panel runat="server" DefaultButton="btnQuantity">
                                                                                            <asp:TextBox CssClass="textboxalign" ID="txtQuantity1" runat="server" BackColor="#F2F2F2" Visible="false" Height="37" Width="70"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender runat="server" ID="FEx1" Enabled="true" TargetControlID="txtQuantity1" FilterMode="ValidChars" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                                                                                            <asp:Button ID="btnQuantity" runat="server" Style="display: none" OnClick="btnQuantity_Click" ValidationGroup="customer" />
                                                                                        </asp:Panel>
                                                                                        <asp:Label ID="lblQuantity1" runat="server" Text="" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblTot1" runat="server" Text="" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblLineEmptyTotal" runat="server" Text="" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblSubTotal" runat="server" Text="" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="LblItemFees" runat="server" Text="" Visible="false"></asp:Label>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr id="TrIdtblTND" runat="server" style="background-color: #F2F2F2;" visible="false">
                                                                                    <td>Enter Code</td>
                                                                                    <td style="text-align: center;">Term & Condition</td>
                                                                                    <td></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td runat="server" style="width: 11%">
                                                                                        <asp:Panel runat="server" DefaultButton="BtnTermsAnCondition">
                                                                                            <asp:TextBox CssClass="textboxalign" ID="TxtTermsAnCondition" runat="server" Height="37" Width="70" Visible="false" BackColor="#F2F2F2"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender runat="server" Enabled="true" FilterMode="ValidChars" FilterType="Numbers" TargetControlID="TxtTermsAnCondition"></asp:FilteredTextBoxExtender>
                                                                                            <asp:Button CssClass="buttonn" ID="BtnTermsAnCondition" runat="server" Style="display: none" OnClick="BtnTermsAnCondition_Click1" />
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                    <td style="width: 35%">
                                                                                        <asp:DropDownList CssClass="search_categories" ID="ddlistCondition" AutoPostBack="true" runat="server" Font-Size="Small" Height="37px" Width="250px" Visible="false" OnSelectedIndexChanged="ddlistCondition_SelectedIndexChanged2">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td style="width: 54%">
                                                                                        <asp:Button CssClass="buttonn" ID="btnAddTD" runat="server" Text="Add" Height="30px" OnClick="btnAddTD_Click" Visible="false" />
                                                                                        <asp:Label ID="lblID1" runat="server" Text="" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblTermsAndCondition1" runat="server" Text="" Visible="false"></asp:Label>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr id="TrTemplate" runat="server" style="background-color: #F2F2F2" visible="false">
                                                                                    <td>Code Nr:</td>
                                                                                    <td style="text-align: left;">Select Template</td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td runat="server" style="width: 11%">
                                                                                        <asp:Panel runat="server" DefaultButton="BtnTemplate">
                                                                                            <asp:TextBox CssClass="textboxalign" ID="txtTemplate" runat="server" Height="37" Width="70" Visible="false" BackColor="#F2F2F2"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender runat="server" Enabled="true" FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txtTemplate"></asp:FilteredTextBoxExtender>
                                                                                            <asp:Button CssClass="buttonn" ID="BtnTemplate" runat="server" Text="Button" Style="display: none" OnClick="BtnTemplate_Click" />
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:DropDownList CssClass="search_categories" ID="ddlistTemplate" runat="server" AutoPostBack="true" Font-Size="Small" Height="37px" Width="150px" Visible="false"></asp:DropDownList>
                                                                                        <asp:Button CssClass="buttonn" ID="BtnADDTemplate" runat="server" Text="ADD" Height="30px" Visible="false" OnClick="BtnADDTemplate_Click" />
                                                                                    </td>
                                                                                    <td style="width: 74%">

                                                                                        <asp:Label ID="lblTemplateQuoteID" runat="server" Text="" Visible="false"></asp:Label>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr id="TrServiceItem" runat="server" style="background-color: #F2F2F2;" visible="false">
                                                                                    <td style="width: 100%; text-align: left; border-width: 5px; border-color: white" colspan="3">Item Name</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td runat="server" visible="false"></td>
                                                                                    <td>
                                                                                        <asp:DropDownList CssClass="search_categories" ID="DropDownListServiceItem" runat="server" AutoPostBack="true" EnableViewState="true" Font-Size="Small" Height="37px" Width="250px" Visible="false"></asp:DropDownList>
                                                                                        <asp:Label ID="lblServiceItemId" runat="server" Text="" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblServiceItemPrice" runat="server" Text="" Visible="false"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button CssClass="buttonn" ID="BtnAddItemService" runat="server" Text="+ADD" Visible="false" OnClick="BtnAddItemService_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="TrCText" runat="server" style="background-color: #F2F2F2;" visible="false">
                                                                                    <td style="width: 100%; text-align: left; border-width: 5px; border-color: white" colspan="3">Text</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox CssClass="textboxalign" ID="txtCText1" runat="server" Height="40" Width="300" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                                                                        <asp:TextBox CssClass="textboxalign" ID="txtCText2" runat="server" Height="40" Width="600" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                                                                        <asp:TextBox CssClass="textboxalign" ID="txtCText3" runat="server" Height="40" Width="600" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button CssClass="buttonn" ID="btnADDC" runat="server" Text="Add" Height="30px" OnClick="btnADDC_Click" Visible="false" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </div>

                                                                    <div id="headerdiv1" runat="server" style="width: 100%;" visible="true">
                                                                        <fieldset style="height: 620px">
                                                                            <legend></legend>
                                                                            <div id="DivConditionHead" class="allSides" runat="server" style="width: 100%" visible="false">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td style="width: 10%;">Add</td>
                                                                                        <td style="width: 1%;"></td>
                                                                                        <td style="width: 89%;">Predefined Text</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>

                                                                            <div runat="server" style="width: 100%; height: 580px; overflow: auto">
                                                                                <asp:GridView ID="GridViewPreText" runat="server" ShowHeader="false" ShowFooter="false" AutoGenerateColumns="false" Width="100%" Style="font-size: medium" OnRowDataBound="GridViewPreText_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Add:" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblQuatationId1" runat="server" Text='<%# Bind("TEMPID") %>' Visible="false"></asp:Label>
                                                                                                <asp:Label ID="LBLID1" runat="server" Text='<%#Container.DataItemIndex + 1%>' Visible="false"></asp:Label>
                                                                                                <asp:CheckBox ID="ChkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkSelect_CheckedChanged" Visible="false"></asp:CheckBox>
                                                                                                <asp:Button CssClass="button" ID="AddTerms" runat="server" Text="+" ForeColor="#FF3300" OnClick="AddTerms_Click" Enabled="false" Visible="false" />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" Width="2%"></HeaderStyle>
                                                                                            <ItemStyle CssClass="Itemalign" Width="2%" />
                                                                                        </asp:TemplateField>

                                                                                        <%-- <asp:TemplateField HeaderText="Terms Id" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblTermsId" runat="server" Text='<%# Bind("TermsID") %>' Visible="true"></asp:Label>
                                                                                             </ItemTemplate>
                                                                                                 <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                                  <ItemStyle CssClass="Itemalign" />
                                                                                                </asp:TemplateField>

                                                                                             <asp:TemplateField HeaderText="Predefined Text" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                             <ItemTemplate>
                                                                                                <asp:TextBox runat="server" CssClass="twitterStyleTextbox" ID="txtCondition" Text='<%# Bind("TERMSANDCONDITION") %>' Visible="true" Width="98%" Height="36" BorderColor="White"></asp:TextBox>
                                                                                              </ItemTemplate>
                                                                                              <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                                  <HeaderStyle BorderColor="White" BorderWidth="5px" Width="98%"></HeaderStyle>
                                                                                               <ItemStyle CssClass="Itemalign" Width="98%" />
                                                                                                 </asp:TemplateField>--%>

                                                                                        <asp:TemplateField HeaderText="Item Code" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblItemCode1" runat="server" Text='<%# Bind("ItemCode") %>' Visible="false"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                            <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                            <ItemTemplate>
                                                                                                <asp:Panel runat="server" DefaultButton="BtnEmptyTextboxChange">
                                                                                                    <asp:TextBox ID="txtEmptyBox1" Enabled="true" CssClass="twitterStyleTextbox" Text='<%# Bind("ItemName") %>' runat="server" Visible="false" Height="30" Width="550" BorderColor="White"></asp:TextBox><%--OnTextChanged="txtEmptyBox1_TextChanged"--%>
                                                                                                    <asp:Button ID="BtnEmptyTextboxChange" runat="server" Style="display: none" OnClick="BtnEmptyTextboxChange_Click" />
                                                                                                </asp:Panel>
                                                                                                <asp:Label ID="lblItemName1" runat="server" Text='<%# Bind("ItemName") %>' Visible="false"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                            <ItemStyle CssClass="Itemalign" Width="30%" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText="Quantity" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                            <ItemTemplate>
                                                                                                <asp:Panel runat="server" DefaultButton="BtnQuantityChange">
                                                                                                    <asp:Label ID="lblItemQua1" runat="server" Text='<%# Bind("Quantity") %>' Visible="false"></asp:Label>
                                                                                                    <asp:TextBox ID="txtItemQuantity" runat="server" CssClass="twitterStyleTextbox" ReadOnly="false" Text='<%# Bind("Quantity") %>' Visible="true" Height="36" Width="50"></asp:TextBox>
                                                                                                    <asp:FilteredTextBoxExtender runat="server" ID="FiltertxtItemQuantity" Enabled="true" TargetControlID="txtItemQuantity" FilterMode="ValidChars" ValidChars=".," FilterType="Numbers,Custom"></asp:FilteredTextBoxExtender>
                                                                                                    <asp:Button ID="BtnQuantityChange" runat="server" Style="display: none" OnClick="BtnQuantityChange_Click" />
                                                                                                </asp:Panel>
                                                                                            </ItemTemplate>
                                                                                            <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                            <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText="Price" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblItemPrice1" runat="server" Text='<%# Bind("Price") %>' Visible="false"></asp:Label>
                                                                                                <asp:Panel runat="server" DefaultButton="BtnForTextChange">
                                                                                                    <asp:TextBox ID="txtItemprice1" CssClass="twitterStyleTextbox" runat="server" ReadOnly="false" Text='<%# Bind("Price") %>' Visible="true" Height="36" Width="50"></asp:TextBox><%--OnTextChanged="txtItemprice1_TextChanged" AutoPostBack="true"--%>
                                                                                                    <asp:FilteredTextBoxExtender runat="server" ID="FiltertxtItemprice1" Enabled="true" TargetControlID="txtItemprice1" FilterMode="ValidChars" ValidChars=".," FilterType="Numbers,Custom"></asp:FilteredTextBoxExtender>
                                                                                                    <asp:Button ID="BtnForTextChange" runat="server" Style="display: none" OnClick="BtnForTextChange_Click" />
                                                                                                </asp:Panel>
                                                                                            </ItemTemplate>
                                                                                            <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                            <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText="GrossTotal" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblItemgtotal1" runat="server" Text='<%# Bind("TotalPrice") %>' Visible="true"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                            <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText=" " HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblItemFees1" runat="server" Text='<%# Bind("ItemFees") %>' Visible="true"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                            <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText=" " HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblTotalEarn1" runat="server" Text='<%# Bind("TotalEarn") %>' Visible="true"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                            <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                        </asp:TemplateField>

                                                                                    </Columns>

                                                                                </asp:GridView>
                                                                            </div>

                                                                            <div id="TotalEarnDiv" runat="server" style="width: 100%; height: 35px;">
                                                                                <table style="width: 100%; height: 35px;">
                                                                                    <tr>
                                                                                        <td style="width: 20%; height: 35px; background-color: lightgray; text-align: center">
                                                                                            <asp:Label ID="LblTotalEarnQuote" runat="server" Text="Total Earn:-"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 80%; height: 35px; background-color: lightgray; text-align: left;">
                                                                                            <asp:Label ID="LblTotalEarning" runat="server" Text=""></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </fieldset>
                                                                    </div>

                                                                    <div id="divstk" runat="server" style="overflow: auto; width: 100%;" visible="false">
                                                                        <%-----NotUsed-------%>

                                                                        <table style="width: 100%;">
                                                                            <tr>
                                                                                <td style="width: 100%;">
                                                                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" ShowHeader="false" OnRowDataBound="GridView2_RowDataBound1">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="lblItem" runat="server" Checked="true" Visible="false" />
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle Width="20px" />
                                                                                                <ItemStyle Width="20px" CssClass="Itemalign" />
                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderText="Item Name" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:DropDownList CssClass="search_categories" ID="ddlItemName1" runat="server" Height="37px" AutoPostBack="true" DataTextField="ItemName" DataValueField="ItemCode">
                                                                                                    </asp:DropDownList>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                                                                <ItemStyle Width="200px" CssClass="Itemalign" />
                                                                                            </asp:TemplateField>


                                                                                            <asp:TemplateField HeaderText="@Price" HeaderStyle-Width="100px" HeaderStyle-VerticalAlign="Middle" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="200px" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                                                                <ItemStyle Width="200px" CssClass="Itemalign" />
                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderText="Quantity" HeaderStyle-Width="100px" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="150px" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblQuantity" runat="server" Text=""></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                                                                <ItemStyle Width="200px" CssClass="Itemalign" />
                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderText="Requested Quantity" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox ID="txtQuantity" runat="server" BackColor="#F2F2F2" AutoPostBack="true"></asp:TextBox>
                                                                                                </ItemTemplate>

                                                                                                <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                                                                <ItemStyle Width="200px" CssClass="Itemalign" />

                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Cost" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblTot" runat="server" Text="" Visible="true"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <%-- <FooterTemplate>
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txttot" runat="server" Width="200px" Height="31px"></asp:TextBox>
                                                                    </FooterTemplate>--%>
                                                                                                <HeaderStyle Width="250px" CssClass="Itemalign" />
                                                                                                <ItemStyle Width="200px" CssClass="Itemalign" />

                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" ItemStyle-Width="150px">

                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="false"></asp:Label>
                                                                                                    <%--  <asp:TextBox CssClass="twitterStyleTextbox" ID="txttot" runat="server" Width="200px" Height="31px"></asp:TextBox>--%>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle Width="250px" />
                                                                                                <ItemStyle Width="200px" />
                                                                                            </asp:TemplateField>
                                                                                        </Columns>

                                                                                        <HeaderStyle BackColor="#F2F2F2"></HeaderStyle>
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                    </div>

                                                                </fieldset>
                                                            </td>

                                                            <td style="width: 45%; vertical-align: top;">
                                                                <div style="height: 830px">
                                                                    <fieldset runat="server" id="fieldsetDetails" style="height: 830px; width: 100%;" visible="true">

                                                                        <div id="divtemplate" runat="server" style="height: 830px; width: 100%;">
                                                                            <table style="width: 100%; font-family: Calibri; margin-top: 1px;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <div style="width: 100%; height: 20px; background-color: silver; text-align: center">
                                                                                            <asp:Label ID="Label4" runat="server" CssClass="clienthead" Text="TILBUD"></asp:Label>
                                                                                        </div>
                                                                                        <div style="width: 100%; height: 800px; background-color: #FAFAFA;">
                                                                                            <div style="height: 200px; width: 100%">

                                                                                                <div id="FileUploadDiv" runat="server" style="height: 100px; width: 300px; position: relative; margin-left: 600px; overflow: hidden; background-color: lightblue">
                                                                                                    <%-- <asp:FileUpload ID="FileUpload01" ClientIDMode="Static" onchange="this.form.submit()" runat="server" Style="width: 400px; position: absolute; height: 100px; opacity: 0; filter: alpha(opacity=0)" CssClass="over" />--%>
                                                                                                    <asp:Image ID="imguser" runat="server" Height="100px" Width="300px" CssClass="under" />
                                                                                                </div>

                                                                                                <div id="DivQuotation" runat="server" style="height: 100px;">
                                                                                                    <table style="width: 100%">
                                                                                                        <tr>
                                                                                                            <td style="height: 100px; float: left;" width="80%">
                                                                                                                <div style="width: 100%; height: 100px; margin-left: 20px;">
                                                                                                                    <div>
                                                                                                                        <asp:Label ID="LblCustName" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                                                    </div>
                                                                                                                    <asp:Label ID="LblCustAddress" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                                                    <div>
                                                                                                                        <asp:Label ID="LblCity" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                                                    </div>
                                                                                                                    <asp:Label ID="LblEmail" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                                                    <div>
                                                                                                                        <asp:Label ID="lbllDatetime" runat="server" Text="" Style="font-size: medium" Visible="false"></asp:Label>
                                                                                                                        <asp:Label ID="Label7" runat="server" Text="Subject::" Font-Bold="true" Visible="false"></asp:Label><%--Quatation Requested As Dated on--%>
                                                                                                                        <asp:Label ID="datetime1" runat="server" Text="" Font-Underline="true" Visible="false"></asp:Label>
                                                                                                                        <asp:Label ID="lblDeletedId" runat="server" Text="" Font-Underline="true" Visible="false"></asp:Label>
                                                                                                                        <asp:Label ID="LBLQUoteIdForImage" runat="server" Text="" Font-Underline="true" Visible="false"></asp:Label>

                                                                                                                        <asp:Label ID="LblSubid" runat="server" Text="" Font-Underline="false" Visible="false"></asp:Label>
                                                                                                                        <asp:Label ID="LblSubQuantity" runat="server" Text="" Font-Underline="false" Visible="false"></asp:Label>
                                                                                                                        <asp:Label ID="LblSubPrice" runat="server" Text="" Font-Underline="false" Visible="false"></asp:Label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td style="height: 100px; width: 12%;" width="20%">
                                                                                                                <div runat="server" style="height: 100px; width: 100%;">
                                                                                                                    <div style="margin-left: 70px">
                                                                                                                        <asp:Label ID="LabeTilbond" runat="server" Text="TILBUD" Style="font-size: xx-large; font-weight: 900;"></asp:Label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>

                                                                                            </div>
                                                                                            <div id="DivQuotationpdf" runat="server">
                                                                                                <div style="margin-right: 20px; margin-left: 20px">
                                                                                                    <asp:Label ID="Label10" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                                                </div>
                                                                                                <div style="margin-right: 20px; margin-left: 20px">
                                                                                                    <asp:Label ID="Label11" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                                                </div>
                                                                                                <div style="margin-right: 20px; margin-left: 20px">
                                                                                                    <asp:Label ID="Label13" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div id="HeaderTemplatee" class="allSides" runat="server" visible="false">
                                                                                                <table id="HeaderTable" runat="server" style="height: 20px;">
                                                                                                    <tr>
                                                                                                        <td id="FirstId" runat="server" style="width: 02%;"></td>
                                                                                                        <td style="width: 41%;"></td>
                                                                                                        <td style="width: 6%;">Quantity</td>
                                                                                                        <td style="width: 5%;">Price</td>
                                                                                                        <td style="width: 6%;">GrossTotal</td>
                                                                                                        <td id="DleteTd" runat="server" style="width: 6%;">Delete</td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <%-- <div runat="server" style="height: 30px; font-size: large; margin-left: 0px; margin-bottom: 0px;">
                                                                                Respected Sir/Madam
                                                                            </div>--%>

                                                                                            <%--<div runat="server" style="height: 20px; left: 0px; font-size: medium;">
                                                                                <p style="margin-left: 0px;">
                                                                                </p>
                                                                            </div>--%>
                                                                                            <%--<div runat="server" style="height: 20px; width: 100%">
                                                                                <p style="font-size: medium; margin-left: 0px;">
                                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox2" runat="server" Wrap="true" Rows="3" Text="The Requested Quote for following items as referenced on above date is here provided for your reference . Kindly acknowledge the same & Provide neccesary feedback."
                                                                                        ReadOnly="false" TextMode="MultiLine" BorderStyle="None" BorderWidth="0" Width="100%" BackColor="#F9F8F6" Visible="false"></asp:TextBox>
                                                                                </p>
                                                                                <p style="font-size: medium; margin-left: 50px;">
                                                                                    The Requested Quote for following items as referenced on above date is here provided for your reference .<br />
                                                                                    Kindly acknowledge the same & Provide neccesary feedback.
                                                                                </p>
                                                                            </div>--%>
                                                                                            <%--<div style="height: 20px"></div>--%>
                                                                                            <div id="abc" runat="server" style="width: 100%; height: 500px;">

                                                                                                <div style="height: 375px; overflow: auto">
                                                                                                    <asp:GridView ID="GrdViewPrint" runat="server" ShowHeader="false" ShowFooter="true" AutoGenerateColumns="false" Width="100%" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White" Style="font-size: medium" OnRowDataBound="GrdViewPrint_RowDataBound" OnRowCommand="GrdViewPrint_RowCommand">
                                                                                                        <Columns>

                                                                                                            <asp:TemplateField HeaderText="Sr/No:" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblQuatationId" runat="server" Text='<%# Bind("TEMPID") %>' Visible="false"></asp:Label>
                                                                                                                    <asp:Label ID="lblID" runat="server" Text='<%#Container.DataItemIndex + 1%>' Visible="true"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px" ForeColor="White"></HeaderStyle>
                                                                                                                <ItemStyle CssClass="Itemalign" ForeColor="White" Width="2%" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Item Code" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="false"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                                                <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:TextBox ID="txtEmptyBox" Enabled="false" CssClass="twitterStyleTextbox" runat="server" Text='<%# Bind("ItemName") %>' Visible="false" Height="30" Width="550" BorderColor="White"></asp:TextBox>
                                                                                                                    <asp:Label ID="lblItemName" runat="server" Text='<%# Bind("ItemName") %>' Visible="true"></asp:Label>
                                                                                                                    <%-- <asp:TextBox ID="TextBox2" runat="server" Wrap="true" Rows="3"  Text='<%# Bind("ItemName") %>' ReadOnly="true" TextMode="MultiLine" BorderStyle="None" BorderWidth="0"></asp:TextBox>--%>
                                                                                                                </ItemTemplate>
                                                                                                                <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                                                <ItemStyle CssClass="Itemalign" Width="36%" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Quantity" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblItemQua" runat="server" Text='<%# Bind("Quantity") %>' Visible="true"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                                                <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Price" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                <ItemTemplate>
                                                                                                                    <%--<asp:Panel runat="server" DefaultButton="BtnTxtItemPrice">--%>
                                                                                                                    <asp:Label ID="LabelItemFees" runat="server" Text='<%# Bind("ItemFees") %>' Visible="false"></asp:Label>
                                                                                                                    <asp:Label ID="lblItemPrice" runat="server" Text='<%# Bind("Price") %>' Visible="true"></asp:Label>
                                                                                                                    <asp:TextBox ID="txtItemprice" CssClass="twitterStyleTextbox" runat="server" ReadOnly="false" Text='<%# Bind("Price") %>' Visible="false" Height="36" Width="50" OnTextChanged="txtItemprice_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                                    <%--OnTextChanged="txtItemprice_TextChanged"--%>
                                                                                                                    <%-- <asp:Button ID="BtnTxtItemPrice" runat="server" Style="display: none" OnClick="BtnTxtItemPrice_Click" />
                                                                                                                  </asp:Panel>--%>
                                                                                                                </ItemTemplate>
                                                                                                                <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                                                <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="GrossTotal" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblEarning" runat="server" Text='<%# Bind("TotalEarn") %>' Visible="false"></asp:Label>
                                                                                                                    <asp:Label ID="lblItemgtotal" runat="server" Text='<%# Bind("TotalPrice") %>' Visible="true"></asp:Label>
                                                                                                                    <asp:Label ID="lblServiceType" runat="server" Text='<%# Bind("ServiceType") %>' Visible="false"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                                                <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Delete" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:LinkButton CssClass="simple" ID="Lnkdeleterow" runat="server" Text="" Font-Underline="false" CommandArgument='<%#Eval("TEMPID") %>' CommandName="DeleteRow"><span class="action-icon icon-trashcan"></asp:LinkButton>
                                                                                                                    <%--<asp:LinkButton CssClass="simple" ID="LinkEditRow" runat="server" Text="" Font-Underline="false"  OnClick="LinkEditRow_Click" CommandArgument='<%#Eval("TEMPID") %>' CommandName="EditRow"><span class="action-icon icon-science-laboratory"></asp:LinkButton>--%>
                                                                                                                </ItemTemplate>
                                                                                                                <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                                                                <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                                <div style="height: 30px; width: 100%;">
                                                                                                    <table style="width: 100%; height: 17px">
                                                                                                        <tr>
                                                                                                            <td style="width: 70%"></td>
                                                                                                            <td style="width: 30%">
                                                                                                                <div>
                                                                                                                    <table style="width: 100%; height: 17px;">
                                                                                                                        <tr>
                                                                                                                            <td style="width: 40%;">
                                                                                                                                <asp:Label ID="lblTotalCost" runat="server" Text="Total Cost:" Style="font-size: medium; border-color: white; border-width: 2px; margin-left: 25px" BorderColor="White" BorderWidth="5px"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 60%; float: left">
                                                                                                                                <asp:Label ID="lblTotalCost1" runat="server" Style="font-size: medium" BorderColor="White"></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                                <div style="height: 30px; width: 100%;">
                                                                                                    <table style="width: 100%; height: 17px">
                                                                                                        <tr>
                                                                                                            <td style="width: 62%"></td>
                                                                                                            <td style="width: 38%">
                                                                                                                <div>
                                                                                                                    <table style="width: 100%; height: 17px">
                                                                                                                        <tr>
                                                                                                                            <td style="width: 80%">
                                                                                                                                <asp:Label ID="Label8" runat="server" Text="Total Cost is inclusive of all discount:" Style="font-size: medium; border-color: white; border-width: 2px" BorderColor="White" BorderWidth="5px"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 20%">
                                                                                                                                <asp:Label ID="Label9" runat="server" Style="font-size: medium" BorderColor="White"></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                                <div runat="server" visible="false">
                                                                                                    <table style="width: 100%;">
                                                                                                        <tr>
                                                                                                            <td style="font-weight: 700; width: 180px; text-align: center; background-color: #F2F2F2; border-width: 10px; border-color: white; font-size: medium;">$Select Condition</td>
                                                                                                            <td style="font-weight: 700; text-align: center; width: 1330px; background-color: #F2F2F2; border-width: 10px; border-color: white; font-size: medium;">Complete Terms & Condition</td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                                <div id="divShow" runat="server" style="overflow: auto; height: 200px;" visible="false">

                                                                                                    <table style="width: 100%;">

                                                                                                        <tr>
                                                                                                            <td style="width: 100%;">
                                                                                                                <asp:GridView ID="GridVConDition" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" ShowHeader="false" DataKeyNames="StantekstID" OnSelectedIndexChanged="GridVConDition_SelectedIndexChanged" OnRowDataBound="GridVConDition_RowDataBound" Visible="false">
                                                                                                                    <Columns>
                                                                                                                        <asp:TemplateField HeaderText="Select Condition" HeaderStyle-Font-Size="Medium" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:DropDownList ID="DropDownList2" runat="server" Width="200px" AutoPostBack="true"></asp:DropDownList><%--OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"--%>
                                                                                                                                <asp:Label ID="lblID" runat="server" Text="" Visible="false"></asp:Label>
                                                                                                                                <asp:Label ID="lblTermsAndCondition" runat="server" Text="" Visible="false"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle Width="220px" />
                                                                                                                            <ItemStyle Width="200px" />
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderText="Complete Terms & Condition" HeaderStyle-Font-Size="Medium" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblCondition" runat="server" Style="font-size: medium"></asp:Label>
                                                                                                                            </ItemTemplate>

                                                                                                                        </asp:TemplateField>

                                                                                                                    </Columns>
                                                                                                                </asp:GridView>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>

                                                                                                    <asp:GridView ID="GridView1" runat="server" DataKeyNames="StantekstID" AutoGenerateColumns="false" Width="100%">
                                                                                                        <Columns>
                                                                                                            <asp:BoundField DataField="TrID" HeaderText="Sr/No:">
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px" Width="10px"></HeaderStyle>
                                                                                                                <ItemStyle BorderColor="White" BorderWidth="5px"></ItemStyle>
                                                                                                            </asp:BoundField>
                                                                                                            <asp:BoundField DataField="TERMSANDCONDITION" HeaderText="Terms & Condition">
                                                                                                                <HeaderStyle BorderColor="White" BorderWidth="5px" Width="300px"></HeaderStyle>
                                                                                                                <ItemStyle BorderColor="White" BorderWidth="5px"></ItemStyle>
                                                                                                            </asp:BoundField>
                                                                                                        </Columns>

                                                                                                    </asp:GridView>

                                                                                                </div>
                                                                                                <div style="height: 40px;">
                                                                                                    <p style="margin-left: 10px; font-size: medium">
                                                                                                        Kindly Reply, As waiting to here from you.<br />
                                                                                                        Regards,<br />
                                                                                                        <%-- <asp:Image runat="server" ImageUrl="~/image/companylogo.jpg" Height="100px" /><br />--%>
                                                                                                        <%--  <a href="http://www.certigoa.com/" target="_blank">WWW.Certigoa.Com</a>--%>
                                                                                                    </p>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <%--<div style="width: 100%; height: 20px;">
                                                                                            <table style="width: 100%; background-color: lightgray;">
                                                                                                <tr>
                                                                                                    <td style="width: 85%"></td>
                                                                                                    <td style="width: 5%">
                                                                                                         <asp:Button CssClass="buttonn" ID="btnSave" runat="server" Text="Save" Height="25px" OnClick="btnSave_Click" Visible="true" />
                                                                                                       
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>

                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:WizardStep>
                                    <asp:WizardStep ID="W5" runat="server" Title="Trin3:Forbehold og Leveringsbetingelse" StepType="Step">
                                        <table style="width: 1700px; height: 830px;">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 40%; vertical-align: top;">
                                                        <div id="Div2" runat="server" style="width: 100%;" visible="true">
                                                            <fieldset style="height: 830px">
                                                                <legend></legend>
                                                                <table border="0" id="tblheader3" style="width: 100%; border-width: 5px; border-color: white">
                                                                    <tr id="Tr1" runat="server" style="background-color: #F2F2F2;" visible="true">
                                                                        <td>Kodenr</td>
                                                                        <td style="text-align: center;">Term & Condition</td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td runat="server" style="width: 11%">
                                                                            <asp:Panel runat="server" DefaultButton="BtnTermsAnCondition1">
                                                                                <asp:TextBox CssClass="textboxalign" ID="TxtTermsAnCondition1" runat="server" Height="37" Width="70" Visible="true" BackColor="#F2F2F2"></asp:TextBox>
                                                                                <asp:FilteredTextBoxExtender runat="server" Enabled="true" FilterMode="ValidChars" FilterType="Numbers" TargetControlID="TxtTermsAnCondition1"></asp:FilteredTextBoxExtender>
                                                                                <asp:Button CssClass="buttonn" ID="BtnTermsAnCondition1" runat="server" Style="display: none" OnClick="BtnTermsAnCondition1_Click" />
                                                                            </asp:Panel>
                                                                        </td>
                                                                        <td style="width: 35%">
                                                                            <asp:DropDownList CssClass="search_categories" ID="ddlistCondition1" AutoPostBack="true" runat="server" Font-Size="Small" Height="37px" Width="250px" Visible="true">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 54%">
                                                                            <asp:Button CssClass="buttonn" ID="btnAddTD1" runat="server" Text="Add" Height="30px" OnClick="btnAddTD1_Click" Visible="true" />
                                                                            <asp:Label ID="lblID11" runat="server" Text="" Visible="false"></asp:Label>
                                                                            <asp:Label ID="lblTermsAndCondition11" runat="server" Text="" Visible="false"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </div>
                                                    </td>
                                                    <td style="width: 45%; vertical-align: top;">
                                                        <div style="height: 830px">
                                                            <fieldset runat="server" id="fieldsetDetails1" style="height: 830px; width: 100%" visible="true">

                                                                <div id="divtemplate1" runat="server">
                                                                    <table style="width: 100%; font-family: Calibri; margin-top: 1px;">
                                                                        <tr>
                                                                            <td>
                                                                                <div style="width: 100%; height: 20px; background-color: silver; text-align: center">
                                                                                    <asp:Label ID="Label41" runat="server" CssClass="clienthead" Text="TILBUD"></asp:Label>
                                                                                </div>
                                                                                <div style="width: 100%; height: 747px; background-color: #FAFAFA;">

                                                                                    <div id="DivQuotation1" runat="server" style="height: 300px; width: 100%">

                                                                                        <div id="FileUploadDiv1" runat="server" style="height: 100px; width: 300px; position: relative; margin-left: 600px; overflow: hidden; background-color: lightblue">
                                                                                            <asp:Image ID="imguser2" runat="server" Height="100px" Width="300px" CssClass="under" />
                                                                                        </div>
                                                                                        <div style="height: 100px;">
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td style="height: 100px; width: 88%; float: left;">
                                                                                                        <div style="width: 100%; height: 100px; margin-left: 20px;">

                                                                                                            <div>
                                                                                                                <asp:Label ID="LblCustName2" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <asp:Label ID="LblCustAddress2" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <asp:Label ID="LblCity2" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <asp:Label ID="LblEmail2" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td style="height: 100px; width: 12%; float: right;">
                                                                                                        <div runat="server" style="height: 100px; width: 100%;">
                                                                                                            <asp:Label ID="Label20" runat="server" Text="TILBUD" Style="font-size: xx-large; font-weight: 900;"></asp:Label>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div style="margin-right: 20px; margin-left: 20px">
                                                                                            <asp:Label ID="Label14" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                                        </div>
                                                                                        <div style="margin-right: 20px; margin-left: 20px">
                                                                                            <asp:Label ID="Label15" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                                        </div>
                                                                                        <div style="margin-right: 20px; margin-left: 20px">
                                                                                            <asp:Label ID="Label16" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="HeaderTemplatee1" class="allSides" runat="server" visible="false">
                                                                                        <table id="HeaderTable1" runat="server" style="height: 20px;">
                                                                                            <tr>
                                                                                                <%-- <td style="width: 05%;"></td>--%>
                                                                                                <td style="width: 34%;"></td>
                                                                                                <td style="width: 6%;">Quantity</td>
                                                                                                <td style="width: 5%;">Price</td>
                                                                                                <td style="width: 5%;">GrossTotal</td>
                                                                                                <%--<td id="Td3" runat="server" style="width: 5%;" visible="false">Delete</td>--%>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>

                                                                                    <div id="abc1" runat="server" style="width: 100%; height: 500px;">

                                                                                        <div style="height: 250px; overflow: auto">
                                                                                            <asp:GridView ID="GrdViewPrint2" runat="server" ShowHeader="false" ShowFooter="false" AutoGenerateColumns="false" Width="100%" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White" Style="font-size: medium">
                                                                                                <Columns>
                                                                                                    <asp:BoundField DataField="ItemName" HeaderText="ItemName" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="30%" />
                                                                                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="5%" />
                                                                                                    <asp:BoundField DataField="Price" HeaderText="Price" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="5%" />
                                                                                                    <asp:BoundField DataField="TotalPrice" HeaderText="TotalPrice" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="5%" />
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                        <div style="height: 30px; width: 100%">
                                                                                            <table style="width: 100%; height: 17px">
                                                                                                <tr>
                                                                                                    <td style="width: 70%"></td>
                                                                                                    <td style="width: 30%">
                                                                                                        <div>
                                                                                                            <table style="width: 100%; height: 17px">
                                                                                                                <tr>
                                                                                                                    <td style="width: 58%;">
                                                                                                                        <asp:Label ID="lblTotalCost11" runat="server" Text="Total Cost:" Style="font-size: medium; border-color: white; border-width: 2px; margin-left: 55px;" BorderColor="White" BorderWidth="5px"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 42%; float: left">
                                                                                                                        <asp:Label ID="lblTotalCost22" runat="server" Style="font-size: medium" BorderColor="White"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div style="height: 30px; width: 100%">
                                                                                            <table style="width: 100%; height: 17px">
                                                                                                <tr>
                                                                                                    <td style="width: 62%"></td>
                                                                                                    <td style="width: 38%">
                                                                                                        <div>
                                                                                                            <table style="width: 100%; height: 17px">
                                                                                                                <tr>
                                                                                                                    <td style="width: 80%">
                                                                                                                        <asp:Label ID="Label81" runat="server" Text="Total Cost is inclusive of all discount:" Style="font-size: medium; border-color: white; border-width: 2px" BorderColor="White" BorderWidth="5px"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 20%">
                                                                                                                        <asp:Label ID="Label91" runat="server" Style="font-size: medium" BorderColor="White"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div runat="server" style="width: 100%; height: 130px; overflow: auto;">
                                                                                            <asp:GridView ID="GridViewTC" runat="server" ShowHeader="false" ShowFooter="false" AutoGenerateColumns="false" Width="100%" Style="font-size: medium;">
                                                                                                <Columns>

                                                                                                    <asp:TemplateField HeaderText=" " HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="30%">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTermNcondition" runat="server" Text='<%# Bind("ItemName") %>' Visible="true"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <%--<asp:BoundField DataField="ItemName" HeaderText="ItemName" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="30%" Visible="false" />--%>
                                                                                                </Columns>

                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                        <div runat="server" visible="false">
                                                                                            <table style="width: 100%;">
                                                                                                <tr>
                                                                                                    <td style="font-weight: 700; width: 180px; text-align: center; background-color: #F2F2F2; border-width: 10px; border-color: white; font-size: medium;">$Select Condition</td>
                                                                                                    <td style="font-weight: 700; text-align: center; width: 1330px; background-color: #F2F2F2; border-width: 10px; border-color: white; font-size: medium;">Complete Terms & Condition</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div id="divShow1" runat="server" style="overflow: auto; height: 200px;" visible="false">
                                                                                            <table style="width: 100%;">
                                                                                                <tr>
                                                                                                    <td style="width: 100%;">
                                                                                                        <asp:GridView ID="GridVConDition1" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" ShowHeader="false" DataKeyNames="StantekstID" OnSelectedIndexChanged="GridVConDition_SelectedIndexChanged" OnRowDataBound="GridVConDition_RowDataBound" Visible="false">
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField HeaderText="Select Condition" HeaderStyle-Font-Size="Medium" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:DropDownList ID="DropDownList2" runat="server" Width="200px" AutoPostBack="true"></asp:DropDownList>
                                                                                                                        <asp:Label ID="lblID" runat="server" Text="" Visible="false"></asp:Label>
                                                                                                                        <asp:Label ID="lblTermsAndCondition" runat="server" Text="" Visible="false"></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                    <HeaderStyle Width="220px" />
                                                                                                                    <ItemStyle Width="200px" />
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:TemplateField HeaderText="Complete Terms & Condition" HeaderStyle-Font-Size="Medium" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label ID="lblCondition" runat="server" Style="font-size: medium"></asp:Label>
                                                                                                                    </ItemTemplate>

                                                                                                                </asp:TemplateField>

                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <asp:GridView ID="TDGridView1" runat="server" DataKeyNames="StantekstID" AutoGenerateColumns="false" Width="100%">
                                                                                                <Columns>
                                                                                                    <asp:BoundField DataField="TrID" HeaderText="Sr/No:">
                                                                                                        <HeaderStyle BorderColor="White" BorderWidth="5px" Width="10px"></HeaderStyle>
                                                                                                        <ItemStyle BorderColor="White" BorderWidth="5px"></ItemStyle>
                                                                                                    </asp:BoundField>
                                                                                                    <asp:BoundField DataField="TERMSANDCONDITION" HeaderText="Terms & Condition">
                                                                                                        <HeaderStyle BorderColor="White" BorderWidth="5px" Width="300px"></HeaderStyle>
                                                                                                        <ItemStyle BorderColor="White" BorderWidth="5px"></ItemStyle>
                                                                                                    </asp:BoundField>
                                                                                                </Columns>

                                                                                            </asp:GridView>
                                                                                        </div>

                                                                                        <div style="height: 40px;">
                                                                                            <p style="margin-left: 10px; font-size: medium">
                                                                                                Kindly Reply, As waiting to here from you.<br />
                                                                                                Regards,<br />
                                                                                                <%--  <a href="http://www.certigoa.com/" target="_blank">WWW.Certigoa.Com</a>--%>
                                                                                            </p>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <%--  <div style="width: 100%; height: 20px;">
                                                                                    <table style="width: 100%; background-color: lightgray;">
                                                                                        <tr>
                                                                                            <td style="width: 85%"></td>
                                                                                            <td style="width: 5%">
                                                                                                  <asp:Button CssClass="buttonn" ID="btnExtoPdf1" runat="server" Text="Export" BackColor="green" OnClick="btnExtoPdf_Click1" Visible="true" />
                                                                                                 <asp:Button ID="Button11" runat="server" Text="Button" OnClick="Button1_Click" Visible="false" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </fieldset>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:WizardStep>
                                    <asp:WizardStep ID="w4" runat="server" Title="Trin4: Gennemse" StepType="Finish">

                                        <div id="printquote1" runat="server" style="overflow: auto; height: 850px;">

                                            <div style="height: 80px; text-align: right; margin-right: 15px;">
                                                <asp:Button ID="BtnSaveASTemplate" runat="server" Text="SaveTemplate" CssClass="buttonn" ToolTip="Save As Template" Visible="true" OnClick="BtnSaveASTemplate_Click" />
                                                <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="buttonn" OnClick="BtnPrint_Click1" ToolTip="Print Quotation" Visible="true" />
                                                <asp:Button CssClass="buttonn" ID="btnExtoPdf" runat="server" Text="PDF" BackColor="green" OnClick="btnExtoPdf_Click1" Visible="true" ToolTip="Export As PDF" />
                                            </div>

                                            <div style="width: 30%; height: 100%; float: left;"></div>

                                            <div id="printquote" style="width: 38%; float: left;">
                                                <div id="print111" style="border: solid 3px;">

                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td style="height: 250px; width: 100%; text-align: left;">

                                                                <div style="width: 100%; height: 400px;">
                                                                    <div style="text-align: center;">
                                                                        <asp:Label ID="header" runat="server" Text="TILBUD" Font-Size="X-Large" Font-Bold="true"></asp:Label><br />
                                                                        <asp:Label ID="header1" runat="server" Text="PMG Stilladser A/S" Font-Size="Larger"></asp:Label><br />
                                                                    </div>

                                                                    <%-- <div style="margin-left: 10px;">
                                                                        Date & Time:<asp:Label ID="date2" runat="server" Text=""></asp:Label><br />
                                                                        ===================================================================
                                                                    </div>--%>
                                                                    <div id="Div11" runat="server" style="height: 100px; width: 300px; position: relative; margin-left: 340px; overflow: hidden; background-color: lightblue;">
                                                                        <asp:Image ID="imguser1" runat="server" Height="100px" Width="300px" CssClass="under" />
                                                                    </div>

                                                                    <div style="height: 100px;">
                                                                        <table style="width: 100%">
                                                                            <tr>
                                                                                <td style="height: 100px; width: 78%; float: left;">
                                                                                    <div style="width: 100%; height: 100px; margin-left: 20px;">
                                                                                        <div>
                                                                                            <asp:Label ID="LblCustName1" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:Label ID="LblCustAddress1" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:Label ID="LblCity1" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:Label ID="LblEmail1" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                                                                        </div>

                                                                                    </div>
                                                                                </td>
                                                                                <td style="height: 100px; width: 22%; float: right;">
                                                                                    <div runat="server" style="height: 100px; width: 100%;">
                                                                                        <asp:Label ID="Label21" runat="server" Text="TILBUD" Style="font-size: xx-large; font-weight: 900;"></asp:Label>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>

                                                                        </table>
                                                                    </div>

                                                                    <div style="margin-right: 20px; margin-left: 20px;">
                                                                        <asp:Label ID="Label17" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                    </div>
                                                                    <div style="margin-right: 20px; margin-left: 20px;">
                                                                        <asp:Label ID="Label18" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                    </div>
                                                                    <div style="margin-right: 20px; margin-left: 20px;">
                                                                        <asp:Label ID="Label19" runat="server" CssClass="clienthead" Text="" Visible="false"></asp:Label>
                                                                    </div>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <div id="HeaderTemplate1" class="allSides" style="height: 30px; width: 635px; margin-left: 5px;" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 05%;"></td>
                                                                <td style="width: 34%;"></td>
                                                                <td style="width: 7%;">Quantity</td>
                                                                <td style="width: 5%;">Price</td>
                                                                <td style="width: 5%;">GrossTotal</td>
                                                                <td id="Td2" runat="server" style="width: 5%;" visible="false">Delete</td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div style="width: 635px; margin-left: 05px;">
                                                        <asp:GridView runat="server" ID="GrdViewPrint1" ShowHeader="false" Width="100%" AutoGenerateColumns="false">
                                                            <Columns>
                                                                <%--  <asp:TemplateField HeaderText="Sr/No:" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblQuatationId1" runat="server" Text='<%# Bind("TEMPID") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblID1" runat="server" Text='<%#Container.DataItemIndex + 1%>' Visible="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle BorderColor="White" BorderWidth="5px" ForeColor="White" />
                                                                    <ItemStyle CssClass="Itemalign" ForeColor="White" Width="2%" />
                                                                </asp:TemplateField> 

                                                                <asp:TemplateField HeaderText="Item Code" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemCode1" runat="server" Text='<%# Bind("ItemCode") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                    <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                </asp:TemplateField>

                                                                   <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtEmptyBox1" Enabled="false" CssClass="twitterStyleTextbox" runat="server" Visible="false" Height="30" Width="550" BorderColor="White"></asp:TextBox>
                                                                        <asp:Label ID="lblItemName1" runat="server" Text='<%# Bind("ItemName") %>' Visible="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                    <ItemStyle CssClass="Itemalign" Width="30%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Quantity" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemQua1" runat="server" Text='<%# Bind("Quantity") %>' Visible="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                    <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                    <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Price" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemPrice1" runat="server" Text='<%# Bind("Price") %>' Visible="true"></asp:Label>
                                                                        <asp:TextBox ID="txtItemprice1" CssClass="twitterStyleTextbox" runat="server" ReadOnly="false" Text='<%# Bind("Price") %>' Visible="false" Height="36" Width="50"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <FooterStyle BorderColor="White" BorderWidth="5px"></FooterStyle>
                                                                    <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                    <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="GrossTotal" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemgtotal1" runat="server" Text='<%# Bind("TotalPrice") %>' Visible="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                    <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                </asp:TemplateField>--%>

                                                                <asp:BoundField DataField="ItemName" HeaderText="ItemName" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="30%" />
                                                                <asp:BoundField DataField="Quantity" HeaderText="Quantity" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="5%" />
                                                                <asp:BoundField DataField="Price" HeaderText="Price" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="5%" />
                                                                <asp:BoundField DataField="TotalPrice" HeaderText="TotalPrice" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="5%" />


                                                                <%-- <asp:TemplateField HeaderText="Delete" ItemStyle-BorderColor="White" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="Lnkdeleterow1" runat="server" Text="" Font-Underline="false"><span class="action-icon icon-trashcan"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle BorderColor="White" BorderWidth="5px"></HeaderStyle>
                                                                    <ItemStyle CssClass="Itemalign" Width="5%" />
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                        </asp:GridView>

                                                        <div id="DivTot" runat="server">
                                                            <div style="text-align: right; margin-right: 05px;">
                                                                <asp:Label ID="lblTotalCost2" runat="server" Text="Total Cost:" Style="font-size: medium; font-weight: 700; border-color: white; border-width: 2px" BorderColor="White" BorderWidth="5px"></asp:Label>
                                                                <asp:Label ID="lblTotalCost3" runat="server" Style="font-size: medium" BorderColor="White"></asp:Label>
                                                            </div>
                                                            <div style="text-align: right; margin-right: 10px;">
                                                                <asp:Label ID="Label12" runat="server" Text="Total Cost is inclusive of all discount:" Style="font-size: medium; border-color: white; border-width: 2px;" BorderColor="White" BorderWidth="5px"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div runat="server" style="width: 100%;">
                                                            <asp:GridView ID="GridViewTC1" runat="server" ShowHeader="false" ShowFooter="false" AutoGenerateColumns="false" Width="100%" Style="font-size: medium">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemName" HeaderText="ItemName" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="30%" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:WizardStep>
                                </WizardSteps>
                                <HeaderTemplate>
                                    <ul id="wizHeader">
                                        <asp:Repeater ID="SideBarList" runat="server" OnItemCommand="SideBarList_ItemCommand">
                                            <ItemTemplate>
                                                <%--<li><a class="<%# GetClassForWizardStep(Container.DataItem) %>" title="<%#Eval("Name")%>">
                                            <%# Eval("Name")%></a> </li>--%>
                                                <%-- <li>
                                            <a class="<%# GetClassForWizardStep(Container.DataItem) %>" title="<%#Eval("Name")%>">
                                                <%# Eval("Name")%></a>
                                        </li>--%>

                                                <li>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="<%# GetClassForWizardStep(Container.DataItem) %>"><%# Eval("Name")%></asp:LinkButton>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>

                                </HeaderTemplate>
                            </asp:Wizard>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>

</html>
