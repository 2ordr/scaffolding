﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditSalary.aspx.cs" Inherits="ProjectManagement.EditSalary" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>


    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style>
        headform1 {
            color: #383838;
        }

        .simple {
            color: lightblue;
        }

            .simple:hover {
                color: blue;
            }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .divcss {
            border: dotted;
            border-color: black;
            color: green;
        }

        .Calendar .ajax__calendar_container {
            border: 1px solid #E0E0E0;
            background-color: #FAFAFA;
            width: 200px;
        }

        .Calendar .ajax__calendar_header {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 12px;
            text-align: center;
            color: #9F9F9F;
            font-weight: normal;
            text-shadow: 0px 0px 2px #D3D3D3;
            height: 20px;
        }

        .Calendar .ajax__calendar_title,
        .Calendar .ajax__calendar_next,
        .Calendar .ajax__calendar_prev {
            color: #004080;
        }

        .Calendar .ajax__calendar_body {
            width: 175px;
            height: 150px;
            position: relative;
        }

        .Calendar .ajax__calendar_dayname {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            color: #FA9900;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            background-color: #EDEDED;
            border: solid 1px #D3D3D3;
            text-transform: uppercase;
            margin: 1px;
        }

        .Calendar .ajax__calendar_day {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            border: solid 1px #E0E0E0;
            text-transform: uppercase;
            margin: 1px;
            width: 17px !important;
            color: #9F9F9F;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_day,
        .Calendar .ajax__calendar_hover .ajax__calendar_month,
        .Calendar .ajax__calendar_hover .ajax__calendar_year,
        .Calendar .ajax__calendar_active {
            color: red;
            font-weight: bold;
            background-color: #ffffff;
        }

        .Calendar .ajax__calendar_year {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_month {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_today {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            text-transform: uppercase;
            margin: 1px;
            color: #6B6B6B;
        }

        .Calendar .ajax__calendar_other {
            background-color: #E0E0E0;
            margin: 1px;
            width: 17px;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_today,
        .Calendar .ajax__calendar_hover .ajax__calendar_title {
        }

        .Calendar .ajax__calendar_footer {
            width: 175px;
            border: none;
            height: 20px;
            vertical-align: middle;
            color: #6B6B6B;
        }
    </style>


    <script type="text/javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>

    <script type="text/javascript">
        function CheckSingleCheckbox(ob) {
            var grid = ob.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (ob.checked && inputs[i] != ob && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }
    </script>

    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $("#addAnother").click(function () {
                addAnotherRow();
            });
        });

        function addAnotherRow() {
            var row = $("#tbl tr").last().clone();
            var oldId = Number(row.attr('id').slice(-1));
            var id = 1 + oldId;


            row.attr('id', 'tasktr_' + id);
            row.find('#Chkselectt_' + oldId).attr('id', 'Chkselectt_' + id);
            row.find('#txttask_' + oldId).attr('id', 'txttask_' + id);
            //row.find('#lbl_' + oldId).attr('id', 'lbl_' + id);
            //row.find('#txtDate_' + oldId).attr('id', 'txtDate_' + id);
            //row.find('#CalendarExtender1_' + oldId).attr('id', 'CalendarExtender1_' + id);
            //row.find('#ChkBillable_' + oldId).attr('id', 'ChkBillable_' + id);
            //row.find('#txtPricePerHr_' + oldId).attr('id', 'txtPricePerHr_' + id);
            //row.find('#txtBudget_' + oldId).attr('id', 'txtBudget_' + id);

            $('#tbl').append(row);

        }
    </script>
    <script type="text/javascript" language="javascript">
        function deleteRow(tableID, currentRow) {
            try {
                var table = document.getElementById(tableID);
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    /*var chkbox = row.cells[0].childNodes[0];*/
                    /*if (null != chkbox && true == chkbox.checked)*/

                    if (row == currentRow.parentNode.parentNode) {
                        if (rowCount <= 1) {
                            alert("Cannot delete all the rows.");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            //getValues();
        }
    </script>
    <script type="text/javascript" language="javascript">
        function delRow() {

            var current = window.event.srcElement;
            //here we will delete the line
            while ((current = current.parentElement) && current.tagName != "TR");
            current.parentElement.removeChild(current);

        }
    </script>
    <script type="text/javascript" language="javascript">
        function visible(Label4) {

            if (document.getElementById('Label4').style.visibility == "false") {
                document.getElementById('Label4').style.visibility == "true";
            }
            else {
                document.getElementById('Label4').style.visibility == "false";
            }
        }

    </script>
    <script language="javascript" type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
    </script>
    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';
            else
                e.style.display = 'none';

            return false;
        }
    </script>
</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false" EnablePartialRendering="true"></asp:ToolkitScriptManager>
        <div style="height: 1000px; width: 1920px;">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 1000px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 1000px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox1" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150"></asp:TextBox>

                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 1000px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="View Project.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>


                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">

                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div3')">Rapporter</span></span>
                                                    <div id="Div3" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded x-item-selected active" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div6')">Løn Modul</span></a>
                                                    <div id="Div6" runat="server" style="display: block">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="10"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>

                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>--%>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div style="height: 300px; border-color: White;">
                                    <table style="height: 300px; width: 100%">
                                        <tr style="height: 100px;">
                                            <td></td>
                                        </tr>
                                        <tr style="height: 50px;">
                                            <td>
                                                <div>
                                                    &nbsp
                                                    <asp:Label ID="Label2" runat="server" Text="Timer" ForeColor="White"></asp:Label>
                                                </div>
                                                <div>

                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                                                        <ContentTemplate>
                                                            &nbsp

                                                            <asp:Label ID="lbldisplayTime" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="white">00:00:00</asp:Label>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                                        </Triggers>

                                                    </asp:UpdatePanel>

                                                    <asp:Button ID="BtnStarttime" runat="server" Text="Start" OnClick="BtnStarttime_Click" CssClass="button" Width="40" Height="30" BackColor="#009900" />
                                                    <asp:Button ID="BtnStopttime" runat="server" Text="stop" OnClick="BtnStopttime_Click" CssClass="button" Width="40" Height="30" BackColor="#009900" />

                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtHrs" runat="server" Height="30" Width="70"></asp:TextBox>
                                                    <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick"></asp:Timer>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 100px;">
                                            <td>
                                                <ul>
                                                    <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                        <asp:Label ID="Label1" runat="server" Text="" ForeColor="White"></asp:Label>
                                                    </span></a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <div id="pdfGeneration" runat="server">
                            <div class="allSides" style="width: 100%; height: 80px; text-align: center;">
                                <div style="height: 10px"></div>
                                <asp:Label ID="lblSalaryLogo" runat="server" CssClass="clienthead" Text="Løn Modul"></asp:Label>
                                <div style="height: 10px; text-align: left">
                                    <asp:Label runat="server" ID="LoginName" Font-Bold="true" Text=""></asp:Label><br />
                                    <asp:Label ID="lblSystemDataTime" runat="server" Text="" ForeColor="blue"></asp:Label>
                                </div>
                            </div>
                            <div style="width: 1%; float: left; height: 800px; border: solid 0px black;">
                            </div>
                            <div runat="server" style="width: 99%; height: 870px; font-family: Calibri;">
                                <div style="height: 870px; width: 55%; float: left;">
                                    <div style="height: 20px"></div>
                                    <fieldset>
                                        <legend style="color: black; font-weight: bold; font-size: large">Projekt informationer:</legend>
                                        <div style="height: 200px">
                                            <table style="height: 200px; width: 100%">
                                                <tbody>

                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblProjectList" runat="server" Text="Projekter"></asp:Label>
                                                        </td>
                                                        <td style="width: 20px"></td>
                                                        <td>
                                                            <asp:DropDownList CssClass="search_categories" ID="ddProjectList" AutoPostBack="true" runat="server" Width="300" Height="36" OnSelectedIndexChanged="ddProjectList_SelectedIndexChanged" Enabled="false"></asp:DropDownList>
                                                            <asp:Label ID="lblProjectId" runat="server" Text="" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; width: 25%">
                                                            <asp:Label ID="lblStartTime" runat="server" Text="Entry Dato"></asp:Label>
                                                        </td>
                                                        <td style="width: 20px"></td>
                                                        <td>
                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStartTime" runat="server" Width="300" Height="31" Enabled="false"></asp:TextBox>
                                                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartTime" CssClass="Calendar" Format="dd-MM-yyyy" Enabled="true"></asp:CalendarExtender>
                                                            <asp:RequiredFieldValidator ID="ReqtxtDate" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtStartTime" ValidationGroup="Salary"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; width: 25%">
                                                            <asp:Label ID="Label3" runat="server" Text="Hours:" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 20px"></td>
                                                        <td>
                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtHours" runat="server" Width="300" Height="31" Visible="false"></asp:TextBox>
                                                            <asp:Label ID="lblTotalHR" runat="server" Text="" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblATotalHR" runat="server" Text="" Visible="false"></asp:Label>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; width: 25%">
                                                            <asp:Label ID="lblClientlist" runat="server" Text="Client Id:" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 20px"></td>
                                                        <td>
                                                            <asp:Label ID="txtClient" runat="server" CssClass="twitterStyleTextbox" Width="300" Height="31" Enabled="false" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align: right; width: 25%">Type:
                                                        </td>
                                                        <td style="width: 20px"></td>
                                                        <td>
                                                            <asp:RadioButton ID="RBtnUseQuate" runat="server" AutoPostBack="true" Text="HourBased" OnCheckedChanged="RBtnUseQuate_CheckedChanged" Enabled="false" />
                                                            <asp:RadioButton ID="RBtnUseLight" runat="server" AutoPostBack="true" Text="Lite Scaffold" OnCheckedChanged="RBtnUseLight_CheckedChanged" Enabled="false" />
                                                            <asp:RadioButton ID="RBtnUseHeavy" runat="server" AutoPostBack="true" Text="Heavy Scaffold" OnCheckedChanged="RBtnUseHeavy_CheckedChanged" Enabled="false" />
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <asp:GridView ID="gvPInfodetails" runat="server" Visible="false"></asp:GridView>

                                    <fieldset>
                                        <legend style="color: black; font-weight: bold; font-size: large">Valg bruger</legend>
                                        <div class="divtitle" style="height: 50px;">
                                            <table style="height: 50px; width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="3"></td>

                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; width: 25%">
                                                            <asp:Label ID="Label4" runat="server" Text="Bruger :"></asp:Label>
                                                        </td>
                                                        <td style="width: 20px"></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlUser" runat="server" CssClass="search_categories" AutoPostBack="true" Width="300" Height="36" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" Enabled="false"></asp:DropDownList>
                                                        </td>
                                                        <%--  <td style="text-align: center; width: 15%"></td>
                                                <td style="width: 35%;"></td>--%>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"></td>

                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <div style="height: 250px;" id="divUsers" runat="server" visible="false">
                                        <fieldset style="height: 250px;">
                                            <legend style="color: black; font-weight: bold; font-size: large">Detaljer:</legend>
                                            <div id="Div1" runat="server">
                                                <table style="height: 20px; width: 100%">
                                                    <tr>
                                                        <td style="width: 5%"></td>
                                                        <td style="width: 35%">Brugernavn</td>
                                                        <td style="width: 20%">Hours Worked</td>
                                                        <td style="width: 15%">Hours</td>
                                                        <td style="width: 20%">Lunch Break</td>

                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="Div2" runat="server" style="height: 200px; overflow: auto;">
                                                <asp:GridView ID="gridUsers" ShowHeader="false" AutoGenerateColumns="false" runat="server" Width="100%" OnRowDataBound="gridUsers_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkUser" runat="server" Checked="false" Enabled="false" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="5%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LItemName" runat="server" Text='<%# Bind("FName") %>'></asp:Label>
                                                                <asp:Label ID="lblEmpid" runat="server" Text='<%# Bind("Empid") %>' Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="30%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtHours" runat="server" Width="100" Height="31" Text="0" Enabled="false"></asp:TextBox>

                                                                <asp:Label ID="lblWage" runat="server" Text='<%# Bind("WagePerHr") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblHours" runat="server" Text="0" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtAHours" runat="server" Width="100" Height="31" Text="0"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" TargetControlID="txtAHours" FilterMode="ValidChars" FilterType="Numbers,Custom" ValidChars="0123456789.,"></asp:FilteredTextBoxExtender>

                                                                <asp:Label ID="lblAWage" runat="server" Text="0" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="15%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLunchTime" runat="server" Text='<%# Bind("LunchTime") %>' Visible="false"></asp:Label>
                                                                <asp:DropDownList CssClass="search_categories" ID="DDLLunch" runat="server" Height="35" Width="150px">
                                                                    <asp:ListItem>Yes</asp:ListItem>
                                                                    <asp:ListItem>No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20%" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LType" runat="server" Text='<%# Bind("Type") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="LblStatus" runat="server" Text='<%# Bind("Status") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblHours1" runat="server" Text='<%# Bind("Hours") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblContributionFees" runat="server" Text='<%# Bind("ContributionFees") %>' Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div style="height: 20px;" runat="server"></div>
                                    <div style="height: 250px;" id="divSalary" runat="server" visible="false">
                                        <fieldset style="height: 250px;">
                                            <legend style="color: black; font-weight: bold; font-size: large">Løn Detaljer:</legend>
                                            <div id="Div4" runat="server">
                                                <table style="height: 20px; width: 100%">
                                                    <tr>
                                                        <td style="width: 5%">SR No:</td>
                                                        <td style="width: 30%">Brugernavn</td>
                                                        <td style="width: 15%">Hours Worked</td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblTotal" runat="server" Text="Total"></asp:Label>

                                                        </td>
                                                        <td style="width: 20%">Type</td>
                                                        <td style="width: 15%; font-size: large">Status
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="Div5" runat="server" style="height: 200px; overflow: auto;">
                                                <asp:GridView ID="GridSalary" ShowHeader="false" AutoGenerateColumns="false" runat="server" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblID" runat="server" Text='<%#Container.DataItemIndex + 1%>' Visible="true"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="5%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LItemName" runat="server" Text='<%# Bind("FName") %>'></asp:Label>
                                                                <asp:Label ID="lblEmpid" runat="server" Text='<%# Bind("Empid") %>' Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="30%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <%--  <asp:TextBox CssClass="twitterStyleTextbox" ID="txtHours" runat="server" Width="100" Height="31" Text="0"></asp:TextBox>--%>
                                                                <asp:Label ID="lblHours" runat="server" Text='<%# Bind("Hours") %>'></asp:Label>
                                                                /<asp:Label ID="lblHours1" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="15%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotal" runat="server" Text='<%# Bind("ContributionFees") %>'></asp:Label>
                                                                /<asp:Label ID="lblTotal1" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="15%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="15%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                    </div>

                                </div>
                                <div id="finishDiv" runat="server" style="height: 840px; width: 40%; border: double; border-color: black; float: right;" visible="False">
                                    <div id="TopDiv" runat="server" height="80" width="100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 50%">
                                                    <asp:Label runat="server" Text="MONTAGEAFREGNING" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td style="width: 45%; text-align: right">
                                                    <%--   <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="buttonn" OnClick="btnOK_Click" Visible="false" />--%>
                                                    <%--   <asp:Button ID="BtnPDFGene" runat="server" Text="Generate Report" CssClass="buttonn" OnClick="BtnPDFGene_Click" Visible="false" />--%>
                                                    <asp:Label ID="lblType" runat="server" Text="" Font-Bold="true" Visible="False"></asp:Label>
                                                </td>
                                                <td style="width: 5%;"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="TopMainDiv" runat="server" height="760" width="100%;">
                                        <div>
                                            <table style="font-family: Calibri; width: 100%;">
                                                <tr style="background-color: #F2F2F2; height: 30px">

                                                    <td style="width: 20px; font-size: large;"></td>
                                                    <td style="width: 100px; font-size: large;"></td>
                                                    <td style="width: 100px; font-size: large;">Day:</td>
                                                    <td style="width: 100px; font-size: large;">Order No:</td>
                                                    <td style="width: 100px; font-size: large;"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="allSides" id="divheader" runat="server" style="width: 100%;">
                                            <table style="font-family: Calibri; width: 100%;">
                                                <tr style="background-color: #F2F2F2; height: 30px; border: 1px">
                                                    <td style="width: 1%;"></td>
                                                    <td style="width: 98%;">
                                                        <table style="font-family: Calibri; width: 100%;">
                                                            <tr style="background-color: #F2F2F2; height: 30px; border: 1px">
                                                                <td style="width: 20%; font-size: large;">Quantity</td>
                                                                <td style="width: 40%; font-size: large;">Item Name</td>
                                                                <td style="width: 20%; font-size: large;">Item Fees</td>
                                                                <td style="width: 20%; font-size: large;">Tot. Amount</td>
                                                                <%-- <td style="width: 20%; font-size: large;"></td>--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 1%;"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <%--  <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>--%>
                                        <div id="Divstocklist" runat="server" style="overflow: auto; width: 100%; height: 690px;">
                                            <table style="width: 100%; font-family: Calibri;">
                                                <tr>
                                                    <td style="width: 1%;"></td>
                                                    <td style="width: 98%;">
                                                        <asp:GridView ID="GviewProjectStock" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" OnRowDataBound="GviewProjectStock_RowDataBound">
                                                            <Columns>
                                                                <%--<asp:TemplateField HeaderText="" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="stkChkbox" runat="server" Visible="False" />
                                                               
                                                                <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>' Visible="False"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20" />
                                                            <FooterTemplate></FooterTemplate>
                                                            <FooterStyle Height="100" />
                                                        </asp:TemplateField>--%>

                                                                <asp:TemplateField HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderText="StockQuantity">
                                                                    <ItemTemplate>
                                                                        <%--OnTextChanged="txtQuntity_TextChanged" AutoPostBack="true"--%>
                                                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnQuantityCount">
                                                                            <asp:TextBox ID="txtQuntity" runat="server" Height="21" Width="60px" ReadOnly="false"></asp:TextBox>
                                                                            <asp:Button ID="btnQuantityCount" runat="server" Style="display: none" OnClick="btnQuantityCount_Click" />
                                                                        </asp:Panel>
                                                                        <asp:Label ID="lblQuntity" runat="server" Text="" Visible="False"></asp:Label>
                                                                        <asp:Label ID="LblItemcode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="100" />
                                                                    <FooterTemplate></FooterTemplate>
                                                                    <FooterStyle Height="100" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ItemName" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="ItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                                                                        <asp:DropDownList CssClass="search_categories" ID="DDLItemName" runat="server" Height="35" Width="200px" Visible="False" OnSelectedIndexChanged="DDLItemName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="300" />
                                                                    <FooterTemplate></FooterTemplate>
                                                                    <FooterStyle Height="100" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ItemFees" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="ItemFees" runat="server" Text='<%# Bind("ItemFees") %>'></asp:Label>
                                                                        <asp:TextBox ID="txtItemFees" runat="server" Height="21" Width="60px" Visible="False"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="100" />
                                                                    <FooterTemplate></FooterTemplate>
                                                                    <FooterStyle Height="100" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Total Amount" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblTotAmt" runat="server" Text=""></asp:Label>
                                                                        <asp:Label ID="ALblTotAmt" runat="server" Text="" Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="200px" />
                                                                </asp:TemplateField>
                                                                <%--    <asp:TemplateField HeaderText="Total Amount" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTotAmt1" runat="server" Text=""></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="300px" />
                                                <FooterTemplate></FooterTemplate>
                                                <FooterStyle Height="100" />
                                            </asp:TemplateField>--%>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                    <td style="width: 1%;"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="DivFooter" runat="server" style="height: 40px" class="divcss">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 64%"></td>
                                                    <td style="width: 20%; font-size: large" title="">TOTAL:
                                    <asp:Label runat="server" ID="LblGrantTotal"></asp:Label></td>
                                                    <td style="width: 16%"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <%-- </ContentTemplate>
                                     <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnAddNewTask" EventName="Click" />
                                        <asp:PostBackTrigger ControlID="btnUpload" />
                                    </Triggers>
                                </asp:UpdatePanel>--%>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="shadowdiv" style="width: 100%; float: left; height: 50px; text-align: center; border: solid 0px black;">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Button CssClass="buttonn" ID="btnSave" runat="server" Text="Opdater" Width="101px" Style="background-color: #009900;" OnClick="btnSave_Click" ValidationGroup="Salary" />
                                        </td>
                                        <td>
                                            <asp:Button CssClass="buttonn" ID="btnView" runat="server" Text="Vis" Width="120px" Style="background-color: #009933" OnClick="btnView_Click" Visible="false" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
