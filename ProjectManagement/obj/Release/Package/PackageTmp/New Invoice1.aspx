﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="New Invoice1.aspx.cs" Inherits="ProjectManagement.New_Invoice1" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <link rel="stylesheet" href="css/calendarcss.css" type="text/css" />
    <link rel="stylesheet" href="css/application.css" type="text/css" />

    <style>
        headform1 {
            color: #383838;
        }

        .bg {
            text-align: right;
        }

        .myclass {
            height: 20px;
            position: relative;
            border: 2px solid #cdcdcd;
            border-color: silver;
            font-size: 14px;
        }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .horizontal {
            display: none;
        }

        .divtitle {
            font-size: xx-large;
        }

        .message {
            background-color: lightblue;
            width: 300px;
            padding: 5px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }

        #drop_zone {
            margin: 10px 0;
            width: 40%;
            min-height: 150px;
            text-align: center;
            text-transform: uppercase;
            font-weight: bold;
            border: 8px dashed #898;
            height: 160px;
        }

        #volume {
            width: 250px;
            height: 30px;
            position: absolute;
            cursor: pointer;
            background: #444;
            top: 50%;
            left: 50%;
            margin-left: -125px;
            margin-top: -15px;
            box-shadow: inset 0 0 15px #000;
        }

        .vol-box {
            font-size: 14px;
            display: block;
            text-align: center;
            margin-top: 50px;
        }

        .control {
            position: absolute;
            top: 0;
            left: 0;
            width: 125px;
            height: 30px;
            background: #3498db;
            box-shadow: inset 0 0 2px #000;
        }

        .knob {
            position: absolute;
            top: 0;
            right: -5px;
            height: 30px;
            width: 10px;
            background: #FFF;
            box-shadow: 0 0 1px #000;
        }

        .txtalign {
            text-align: right;
        }

        .AlignCheckBox {
            text-align: center;
        }

        .under {
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: 1;
        }

        .over {
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: 2;
        }
    </style>

    <script type="text/javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>
    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>

    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>
    <script type="text/javascript">
        function visibility() {
            document.getElementById(l1).value = "none";
            document.getElementById(l2).value = "none";
        }
    </script>

    <script type="text/javascript">
        function init() {
            $("img[data-type=editable]").each(function (i, e) {
                var _inputFile = $('<input/>')
                    .attr('type', 'file')
                    .attr('hidden', 'hidden')
                    .attr('onchange', 'readImage()')
                    .attr('data-image-placeholder', e.id);

                $(e.parentElement).append(_inputFile);

                $(e).on("click", _inputFile, triggerClick);
            });
        }

        function triggerClick(e) {
            e.data.click();

        }

        Element.prototype.readImage = function () {
            var _inputFile = this;
            if (_inputFile && _inputFile.files && _inputFile.files[0]) {
                var _fileReader = new FileReader();
                _fileReader.onload = function (e) {
                    var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                    var _img = $("#" + _imagePlaceholder);
                    _img.attr("src", e.target.result);
                };
                _fileReader.readAsDataURL(_inputFile.files[0]);
            }
        };

        // 
        // IIFE - Immediately Invoked Function Expression
        // https://stackoverflow.com/questions/18307078/jquery-best-practises-in-case-of-document-ready
        (

        function (yourcode) {
            "use strict";
            // The global jQuery object is passed as a parameter
            yourcode(window.jQuery, window, document);
        }(

        function ($, window, document) {
            "use strict";
            // The $ is now locally scoped 
            $(function () {
                // The DOM is ready!
                init();
            });

            // The rest of your code goes here!
        }));

    </script>

    <script type="text/javascript">
        function PrintGridDataAll() {

            <%--   var prtGrid = document.getElementById('<%# SearchResultGrid.ClientID %>');tbcontain_tb1_SearchResultGrid--%>
            var prtGrid = document.getElementById("container-1041");
            prtGrid.border = 0;
            var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=800,tollbar=0,scrollbars=1,status=0,resizable=1');
            prtwin.document.writeln("<div style='text-align:center; margin-top:20px;'></div>");
            prtwin.document.write(prtGrid.outerHTML);
            prtwin.document.close();
            prtwin.focus();
            prtwin.print();
            prtwin.close();
        }

        $(document).ready(function () {

            $('.modal').fadeOut();

            $(".searchbtnclick").click(function () {
                $(".modal").fadeIn();
            });
        });

    </script>


</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server" enctype="multipart/form-data">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true" ScriptMode="Debug"></asp:ToolkitScriptManager>

        <div style="height: 1000px; width: 1920px;">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtSearch" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150" BackColor="White"></asp:TextBox>
                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>


                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">

                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%--<li class="menu-item menu-app-item app-item  x-item-selected active" id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                --%>
                                                <%-- <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>
                                                --%>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div1')">Rapporter</span></span>

                                                    <div id="Div1" runat="server" style="display: block">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item  x-item-selected active " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div2')">Løn Modul</span></a>
                                                    <div id="Div2" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>


                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link" href=""><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label></span></a></li>

                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <div class="allSides" style="width: 100%; height: 80px">

                            <table style="width: 100%; font-family: Calibri;">
                                <tbody>
                                    <tr>
                                        <td style="height: 20px;" colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px; width: 2%;"></td>
                                        <td style="width: 88%;">
                                            <asp:Label ID="Label3" runat="server" CssClass="clienthead" Text="Opret faktura"></asp:Label>
                                        </td>

                                        <td style="width: 15%;"><span style="float: right">
                                            <asp:Button runat="server" CssClass="buttonn" ID="btnpdf" BackColor="Green" Text="SaveAsPDF" Visible="false" OnClick="btnpdf_Click" ToolTip="save Invoice as PDf" />
                                            <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="buttonn" OnClick="BtnPrint_Click" ToolTip="Print Invoice" Visible="true" /></span>
                                        </td>
                                        <td style="height: 20px; width: 1%;"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 1%; float: left; height: 800px; border: solid 0px black;">
                        </div>
                        <div style="width: 100%; height: 870px; font-family: Calibri;">

                            <div style="height: 862px; width: 99%; overflow: auto;" runat="server">
                                <div style="height: 800px; width: 50%;" runat="server">

                                    <fieldset id="Divpdfgeneration" runat="server">
                                        <legend style="color: black; font-weight: bold; font-size: 13pt">Stamdata</legend>
                                        <div style="height: 150px">
                                            <div style="height: 150px; float: left; width: 49%">
                                                <div style="height: 10px;"></div>
                                                <div id="divcustinfo" runat="server" style="height: 130px;">
                                                    <div id="div11" runat="server" style="height: 30px; display: block">

                                                        <asp:Label ID="lblCompanyLoGo" runat="server" Text="" Font-Size="12pt" Font-Bold="true" Font-Underline="true" Width="300" BorderColor="Silver"></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblcustname" runat="server" Text=""></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblcustemail" runat="server" Text=""></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblcustaddress" runat="server" Text=""></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblcustcity" runat="server" Text=""></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblcustcountry" runat="server" Text=""></asp:Label>
                                                    </div>

                                                </div>
                                            </div>

                                            <div style="height: 149px; float: right; width: 50%">
                                                <div style="height: 10px;"></div>
                                                <div style="height: 130px;">
                                                    <div style="height: 30px;">
                                                        <asp:Label ID="LblProvider" runat="server" Text="" Font-Size="12pt" Font-Bold="true" Font-Underline="true" Width="300" BorderColor="Silver"></asp:Label>
                                                    </div>
                                                    <div id="FileUploadDiv" runat="server" style="height: 100px; width: 300px; position: relative; overflow: hidden; background-color: lightblue">

                                                        <%-- <asp:FileUpload ID="FileUpload01" ClientIDMode="Static" onchange="this.form.submit()" runat="server" Style="width: 400px; position: absolute; height: 100px; opacity: 0; filter: alpha(opacity=0)" CssClass="over" />--%>
                                                        <asp:Image ID="imguser" runat="server" Height="100px" Width="300px" CssClass="under" />
                                                        <%--<img class="message" id="companyLogo" src="" data-type="editable" height="100" width="300" style="background-color: lightblue" runat="server" />--%>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div style="height: 10px; width: 100%;"></div>

                                    <fieldset id="Divpdfgeneration1" runat="server">
                                        <legend style="color: black; font-weight: bold; font-size: 13pt"></legend>
                                        <div runat="server" style="height: 150px">
                                            <div style="height: 150px; float: left; width: 49%">

                                                <div style="height: 150px;">
                                                    <table style="height: 150px; width: 100%">
                                                        <tbody>
                                                            <tr>
                                                                <td style="height: 40px;">
                                                                    <asp:Label ID="LblInvoiceNo" runat="server" Text="Faktura No:-"></asp:Label></td>
                                                                <td style="height: 40px;">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtInvoiceNo" runat="server" Height="31" Width="250px" Enabled="false"></asp:TextBox>
                                                                    <asp:Label ID="lblInvoiceNo1" runat="server" Text="" Height="31" Enabled="false" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="height: 40px;">
                                                                    <asp:Label ID="lblCustomer" runat="server" Text="Kunder:-"></asp:Label>
                                                                </td>
                                                                <td style="height: 40px;">
                                                                    <asp:DropDownList ID="ddListCustomer" runat="server" Font-Size="Small" Height="36" Width="250" CssClass="search_categories" AutoPostBack="true" OnSelectedIndexChanged="ddListCustomer_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:Label runat="server" ID="LblCustId" Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblCustomerName" runat="server" Text="" Height="31" Enabled="false" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="height: 40px;">
                                                                    <asp:Label ID="lblCustOrder" runat="server" Text="Ordre:-"></asp:Label>
                                                                </td>
                                                                <td style="height: 40px;">

                                                                    <asp:DropDownList ID="ddListCustOrder" runat="server" Font-Size="Small" Height="36" Width="250" CssClass="search_categories" AutoPostBack="true" OnSelectedIndexChanged="ddListCustOrder_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:Label ID="lblCustomerOrder" runat="server" Text="" Height="31" Enabled="false" Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblOrderId" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div runat="server" style="height: 150px; float: right; width: 50%">

                                                <div style="height: 150px;">

                                                    <table style="height: 150px; width: 100%">
                                                        <tbody>
                                                            <tr>
                                                                <td style="height: 40px;">
                                                                    <asp:Label ID="LblInvoiceDate" runat="server" Text="Dato:-"></asp:Label></td>
                                                                <td style="height: 40px;">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtInvoiceDate" runat="server" Height="31" Width="200" Enabled="false"></asp:TextBox>
                                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtInvoiceDate" CssClass="Calendar" Format="dd/MM/yyyy hh:mm:ss" Enabled="true"></asp:CalendarExtender>
                                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx2" Enabled="true" TargetControlID="txtInvoiceDate" FilterMode="ValidChars" ValidChars="0123456789/:- "></asp:FilteredTextBoxExtender>

                                                                    <asp:Label runat="server" ID="LabelTimeDiff" Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblInvoiceDate1" runat="server" Text="" Height="31" Enabled="false" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="height: 40px;">
                                                                    <asp:Label ID="LblDueDate" runat="server" Text="Due Dato:-"></asp:Label></td>
                                                                <td style="height: 40px;">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDueDate" runat="server" Height="31" Width="200" OnTextChanged="txtDueDate_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDueDate" CssClass="Calendar" Format="dd/MM/yyyy hh:mm:ss" Enabled="true"></asp:CalendarExtender>
                                                                    <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender1" Enabled="true" TargetControlID="txtDueDate" FilterMode="ValidChars" ValidChars="0123456789/:- "></asp:FilteredTextBoxExtender>

                                                                    <asp:Label ID="lblDueDate1" runat="server" Text="" Height="31" Enabled="false" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="height: 40px;" colspan="2">
                                                                    <asp:CheckBox ID="ChkPayable" runat="server" OnCheckedChanged="ChkPayable_CheckedChanged" AutoPostBack="true" />
                                                                    <span id="SpanInvoce" runat="server">Forfalden belob:</span>
                                                                    <asp:Label runat="server" ID="LbltxtPercentage" Visible="false"></asp:Label>

                                                                    <asp:TextBox CssClass="twitterStyleTextbox" runat="server" ID="txtPercentage" Text="" Height="31" Visible="false" Width="50" AutoPostBack="true" OnTextChanged="txtPercentage_TextChanged"></asp:TextBox><span id="SpanPercentage" runat="server" visible="false">%</span><asp:FilteredTextBoxExtender runat="server" ID="FEx1" Enabled="true" TargetControlID="txtPercentage" ValidChars="0123456789.," FilterMode="ValidChars"></asp:FilteredTextBoxExtender>


                                                                    <asp:CheckBox ID="ChkServicePayable" runat="server" OnCheckedChanged="ChkServicePayable_CheckedChanged" AutoPostBack="true" Enabled="false" />
                                                                    <span id="SpanService" runat="server">Service payable:</span>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <%--  <div>
                                        <input type='range' />
                                    </div>
                                    <div id="volume">
                                        <div class="control">
                                            <span class="knob"></span>
                                        </div>
                                        <span class="vol-box">50</span>
                                    </div>--%>
                                        </div>
                                    </fieldset>
                                    <div runat="server" style="width: 100%">
                                        <fieldset>
                                            <legend style="color: black; font-weight: bold;">..</legend>
                                            <div id="Divpdfgeneration2" class="divtitle" runat="server" style="height: 50px; width: 100%; text-align: center">
                                                <asp:Label ID="lblName" runat="server" Text="FAKTURA" Font-Bold="true" Font-Size="20pt"></asp:Label>
                                            </div>
                                            <div id="SpaceDiv" runat="server" visible="false" style="height: 50px; width: 100%"></div>
                                            <table id="Table1" runat="server" class="items">

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div id="divOverflow" runat="server" style="width: 100%; font-family: Calibri;">

                                                                <table id="tbltask" runat="server" style="width: 100%;">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:GridView ID="gridViewOrder" runat="server" AutoGenerateColumns="False" ShowHeader="true" Width="100%" RowStyle-Height="30px" OnRowDataBound="gridViewOrder_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="SR.No">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lblSubInNo" runat="server" Text='<%#Container.DataItemIndex + 1%>'></asp:Label>
                                                                                            <asp:Label ID="lblInvoiceNo" runat="server" Text="" Visible="false"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                                                                        <HeaderStyle Width="50px" BorderWidth="5" BorderColor="White" BackColor="LightBlue" ForeColor="Black" />
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Item/Service">
                                                                                        <ItemTemplate>

                                                                                            <asp:DropDownList CssClass="search_categories" ID="DDListItem" runat="server" Height="35" Width="250" Visible="true" Font-Size="Small" OnSelectedIndexChanged="DDListItem_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                                                            <%-- <asp:TextBox CssClass="twitterStyleTextbox" runat="server" ID="txtItemName" Text='<%# Bind("ItemName") %>' Height="31" Visible="true" Width="250"></asp:TextBox>--%>
                                                                                            <asp:Label ID="txtItemName" runat="server" Text='<%# Bind("ItemName") %>' Visible="true" Height="25" Width="200"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                                                        <HeaderStyle Width="200px" BorderWidth="5" BorderColor="White" BackColor="LightBlue" ForeColor="Black" />
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Description">
                                                                                        <ItemTemplate>
                                                                                            <span class="title">
                                                                                                <%-- <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDescription" Text="" runat="server" Height="31" Visible="true" Width="250"></asp:TextBox>--%>
                                                                                                <asp:Label ID="txtDescription" runat="server" Text="" Visible="true" Height="25" Width="200"></asp:Label>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                                                        <HeaderStyle Width="200px" BorderWidth="1" BorderColor="White" BackColor="LightBlue" ForeColor="Black" />
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="@Price/Fees">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="txtPrice" runat="server" Text='<%# Bind("Price") %>' Visible="true" Height="25" Width="60"></asp:Label>
                                                                                            <%--   <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPrice" runat="server" Text='<%# Bind("Price") %>' Height="31" Width="60" OnTextChanged="txtPrice_TextChanged" AutoPostBack="true"></asp:TextBox>--%>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="60px" BorderWidth="5" BorderColor="White" />
                                                                                        <HeaderStyle Width="60px" BorderWidth="5" BorderColor="White" BackColor="LightBlue" ForeColor="Black" />
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Quantity">
                                                                                        <ItemTemplate>
                                                                                            <%-- <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity" runat="server" Text='<%# Bind("Quantity") %>' Height="31" Width="60" Enabled="false"></asp:TextBox>--%>
                                                                                            <asp:Label ID="txtQuantity" runat="server" Text='<%# Bind("Quantity") %>' Visible="true" Height="25" Width="60"></asp:Label>
                                                                                             <asp:Label ID="lblServiceType" runat="server" Text='<%# Bind("ServiceType") %>' Visible="false"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="60px" BorderWidth="5" BorderColor="White" />
                                                                                        <HeaderStyle Width="60px" BorderWidth="5" BorderColor="White" BackColor="LightBlue" ForeColor="Black" />
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="GrossTotal">
                                                                                        <ItemTemplate>
                                                                                            <%-- <asp:TextBox CssClass="twitterStyleTextbox" ID="txtGrossTotal" runat="server" Text='<%# Bind("TotalPrice") %>' Height="31" Enabled="false" Width="70"></asp:TextBox>--%>
                                                                                            <asp:Label ID="txtGrossTotal" runat="server" Text='<%# Bind("TotalPrice") %>' Visible="true" Height="25" Width="70" Enabled="false"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="70px" BorderWidth="5" BorderColor="White" />
                                                                                        <HeaderStyle Width="70px" BorderWidth="5" BorderColor="White" BackColor="LightBlue" ForeColor="Black" />
                                                                                    </asp:TemplateField>

                                                                                    <%--<asp:TemplateField HeaderText="Tax">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTax" runat="server" Text="" Height="31" Width="60"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="60px" BorderWidth="5" BorderColor="White" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Tax1">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTax1" runat="server" Text="" Height="31" Width="60"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="60px" BorderWidth="5" BorderColor="White" />
                                                                        </asp:TemplateField>--%>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <fieldset>
                                                <legend></legend>
                                                <asp:GridView ID="gridViewtest" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="LightBlue" ShowHeader="true" Width="100%" Visible="false" RowStyle-Height="30px">
                                                    <Columns>

                                                        <asp:BoundField DataField="ItemName" HeaderText="Item/Service" Visible="true" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="50%" />

                                                        <%--   <asp:BoundField DataField="" HeaderText="Description" Visible="false" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="12%" />--%>

                                                        <asp:BoundField DataField="Price" HeaderText="@Price/Fees" Visible="true" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="20%" />

                                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" Visible="true" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="18.5%" />

                                                        <asp:BoundField DataField="TotalPrice" HeaderText="Total" Visible="true" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" ItemStyle-Width="11.5%" />

                                                    </Columns>
                                                </asp:GridView>

                                                <div id="Divpdfgeneration3" runat="server">

                                                    <table style="width: 100%;">
                                                        <tr style="height: 20px;">
                                                            <td style="width: 72%;">
                                                                <asp:Label ID="lblTotalPercentage" runat="server" Text="0.00" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblTotalAmount" runat="server" Text="0.00" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 13%; text-align: right">Total Invoice:</td>
                                                            <td style="width: 15%; text-align: center">
                                                                <asp:Label ID="LblTotalPrice" runat="server" Text="0.00"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="background-color: white; width: 100%; height: 5px"></td>
                                                        </tr>

                                                        <tr id="TrPaid" runat="server" visible="false" style="height: 20px;">
                                                            <td style="width: 72%; text-align: right">
                                                                <asp:Label ID="lblPaidPercentage" runat="server" Text="0.00"></asp:Label><span>%</span>
                                                            </td>
                                                            <td style="width: 13%; text-align: right">Paid Invoice:</td>
                                                            <td style="width: 15%; text-align: center">
                                                                <asp:Label ID="lblPaidAmount" runat="server" Text="0.00"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="background-color: white; width: 100%; height: 5px"></td>
                                                        </tr>

                                                        <tr id="TrPercentage" runat="server" visible="false" style="height: 20px;">
                                                            <td style="width: 72%; text-align: right">
                                                                <asp:Label ID="lblPercent" runat="server" Text="0.00"></asp:Label><span>%</span>
                                                            </td>
                                                            <td style="width: 13%; text-align: right">Payable Invoice:</td>
                                                            <td style="width: 15%; text-align: center">
                                                                <asp:Label ID="LblPayableAmt" runat="server" Text="0.00"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="background-color: white; width: 100%; height: 5px"></td>
                                                        </tr>

                                                        <tr id="TrServicePay" runat="server" visible="false" style="height: 20px;">
                                                            <td style="width: 72%; text-align: right"></td>
                                                            <td style="width: 13%; text-align: right">Service Total:</td>
                                                            <td style="width: 15%; text-align: center">
                                                                <asp:Label ID="lblServiceTotal" runat="server" Text="0.00"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </div>
                                            </fieldset>
                                        </fieldset>

                                        <div id="add_line_button" class="add_line_button" style="height: 40px">
                                            <span class=" ">
                                                <asp:LinkButton ID="lnkAddLine" runat="server" OnClick="lnkAddLine_Click">+Tilføj linie</asp:LinkButton>
                                            </span>
                                        </div>
                                        <fieldset>
                                            <legend></legend>
                                            <div id="Divpdfgeneration4" runat="server">

                                                <div class="section enabled fullAccess" data-highlight="[data-step=6]" data-step="6" id="notes" style="background-color: rgb(255, 255, 255);">
                                                    <div class="disabled overlay"></div>
                                                    <div class="add-notes" data-total-steps="5" data-step="1" data-repeat="2" data-speed="80" style="display: block; opacity: 0; top: -60px;"></div>
                                                    <div class="locked overlay">
                                                        <%-- <img src="/assets/images/lock.png" />
                                                <p>Not so fast! Complete step 1-3 to unlock notes </p>--%>
                                                    </div>
                                                    <h3>Notes:-</h3>
                                                    <div class="field">
                                                        <label id="l1" class="placeholder" for="invoice_notes" runat="server"></label>
                                                        <textarea id="Invoice_Notes" style="resize: none; overflow-y: hidden; height: 72px; width: 100%" runat="server"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="height: 10px"></div>
                                            <div id="Divpdfgeneration5" runat="server">
                                                <div class="section enabled fullAccess" data-highlight="[data-step=5]" data-step="5" id="terms" style="background-color: rgb(255, 255, 255);">
                                                    <div class="disabled overlay"></div>
                                                    <div class="add-terms" data-total-steps="5" data-step="1" data-repeat="2" data-speed="80" style="display: block; opacity: 0; top: -60px;"></div>
                                                    <div class="locked overlay" style="display: none;">
                                                        <%-- <img src="/assets/images/lock.png" />
                                                <p>Not so fast! Complete step 1-3 to unlock terms</p>--%>
                                                    </div>
                                                    <h3>Terms & Condition:-</h3>
                                                    <div class="field">
                                                        <label id="l2" class="placeholder" for="invoice_terms" runat="server"></label>
                                                        <textarea id="invoice_terms" style="resize: none; overflow-y: hidden; height: 72px; width: 100%" runat="server"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                    </div>
                                </div>
                            </div>
                            <div class="shadowdiv" id="Shadowdiv" runat="server" style="width: 100%; float: left; height: 50px; text-align: center; border: solid 0px black;">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:Button CssClass="buttonn" ID="BtnSaveInvoice" runat="server" Text="Gem " Width="101px" Style="background-color: #009900" Visible="true" OnClick="BtnSaveInvoice_Click" />
                                            </td>

                                            <td>
                                                <asp:Button CssClass="buttonn" ID="BtnUpdateInvoice" runat="server" Text="Opdater" Width="120px" Style="background-color: #009933" Visible="false" OnClick="BtnUpdateInvoice_Click" />
                                            </td>
                                            <td>
                                                <asp:Button CssClass="buttonn" ID="BtnViewInvoice" runat="server" Text="Vis" Width="120px" Style="background-color: #009933" Visible="false" OnClick="BtnViewInvoice_Click" />
                                            </td>
                                            <td>
                                                <asp:Button CssClass="buttonn" ID="BtnEditInvoice" runat="server" Text="Rediger" Width="120px" Style="background-color: #009933" Visible="false" OnClick="BtnEditInvoice_Click" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnpdf" />
                        <asp:PostBackTrigger ControlID="BtnPrint" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
