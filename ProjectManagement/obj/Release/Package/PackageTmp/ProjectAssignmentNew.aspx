﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectAssignmentNew.aspx.cs" Inherits="ProjectManagement.ProjectAssignmentNew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="DatePickerControl"
    Namespace="DatePickerControl" TagPrefix="cc1" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <%-- <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />--%>

    <link rel="stylesheet" href="fonts/untitle-font-4/styles.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style>
        headform1 {
            color: #383838;
        }

        .bc {
            border-color: brown;
        }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .bg {
            text-align: right;
        }

        .table1 {
            border: 1px solid #ddd;
            border-collapse: separate;
            border-radius: 15px;
            border-spacing: 5px;
        }

        .Calendar .ajax__calendar_container {
            border: 1px solid #E0E0E0;
            background-color: #FAFAFA;
            width: 200px;
        }

        .Calendar .ajax__calendar_header {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 12px;
            text-align: center;
            color: #9F9F9F;
            font-weight: normal;
            text-shadow: 0px 0px 2px #D3D3D3;
            height: 20px;
        }

        .Calendar .ajax__calendar_title,
        .Calendar .ajax__calendar_next,
        .Calendar .ajax__calendar_prev {
            color: #004080;
        }

        .Calendar .ajax__calendar_body {
            width: 175px;
            height: 150px;
            position: relative;
        }

        .Calendar .ajax__calendar_dayname {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            color: #FA9900;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            background-color: #EDEDED;
            border: solid 1px #D3D3D3;
            text-transform: uppercase;
            margin: 1px;
        }

        .Calendar .ajax__calendar_day {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            border: solid 1px #E0E0E0;
            text-transform: uppercase;
            margin: 1px;
            width: 17px !important;
            color: #9F9F9F;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_day,
        .Calendar .ajax__calendar_hover .ajax__calendar_month,
        .Calendar .ajax__calendar_hover .ajax__calendar_year,
        .Calendar .ajax__calendar_active {
            color: red;
            font-weight: bold;
            background-color: #ffffff;
        }

        .Calendar .ajax__calendar_year {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_month {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_today {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            text-transform: uppercase;
            margin: 1px;
            color: #6B6B6B;
        }

        .Calendar .ajax__calendar_other {
            background-color: #E0E0E0;
            margin: 1px;
            width: 17px;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_today,
        .Calendar .ajax__calendar_hover .ajax__calendar_title {
        }

        .Calendar .ajax__calendar_footer {
            width: 175px;
            border: none;
            height: 20px;
            vertical-align: middle;
            color: #6B6B6B;
        }

        .simple {
            color: lightblue;
        }

            .simple:hover {
                color: blue;
            }

        .spanid {
            color: lightblue;
        }

            .spanid :hover {
                color: black;
                cursor: pointer;
            }

        .link-button {
            margin-top: 15px;
            max-width: 90px;
            /*background-color: aquamarine;
            /border-color: black;
            color: #333;*/
            display: inline-block;
            vertical-align: middle;
            text-align: center;
            text-decoration: none;
            align-items: flex-start;
            cursor: default;
            -webkit-appearence: push-button;
            border-style: solid;
            border-width: 1px;
            border-radius: 5px;
            font-size: 1em;
            font-family: inherit;
            border-color: floralwhite;
            padding-left: 5px;
            padding-right: 5px;
            width: 100%;
            min-height: 30px;
        }

            .link-button:hover {
                background-color: skyblue;
                border-color: black;
            }

        .GridViewEditOrder tr.rowHover1:hover {
            background-color: lightblue;
            font-family: Arial;
        }

        #GrdViewOrderDetails tr.rowHover:hover {
            background-color: lightblue;
            font-family: Arial;
        }

        .Position {
            /*Style="z-index: 103; position: absolute; top: 280px;left: 850;"*/
            position: absolute /*absolute*/;
            top: 210px;
            left: 40%;
            z-index: 99;
        }

        span:hover {
            cursor: pointer;
        }

        .parentDisable {
            z-index: 9;
            width: 100%;
            height: 100%;
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            background-color: #000;
            color: #aaa;
            opacity: .4;
            filter: alpha(opacity=50);
        }

        input.underlined {
            border: 0;
            border-bottom: solid 1px #000;
            outline: none; /* prevents textbox highlight in chrome */
        }
    </style>

    <script type="text/javascript">

        function DisableBackButton() {                  //for disabling background browser button
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>

    <script type="text/javascript">   //for selecting only single checkbox for row
        function CheckSingleCheckbox(ob) {
            var grid = ob.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (ob.checked && inputs[i] != ob && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }
    </script>

    <script type="text/javascript">
        function SetColor(id) {
            var links = document.getElementsByTagName("<a>")
            for (var i = 0; i < links.length; i++) {
                links[i].style.color = "Black";
            }
            document.getElementById(id).style.color = "red";
        }
    </script>
    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>

    <script type="text/javascript" language="javascript">
        function delRow() {

            var current = window.event.srcElement;
            //here we will delete the line
            while ((current = current.parentElement) && current.tagName != "TR");
            current.parentElement.removeChild(current);
        }

    </script>

    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>
    <script type="text/javascript">
        function ShowDiv2(id) {
            var e = document.getElementById(id);
            if (e.style.visibility == 'hidden')
                e.style.visibility = 'visible';

            else
                e.style.visibility = 'hidden';
            $("#pop1").css("display", "none");

            return false;
        }
    </script>

    <script type="text/javascript">
        function pop(div) {
            document.getElementById(div).style.display = 'block';
            // $("#PopUpDiv").css("visibility", "visible");
            //$("#PopUpDiv2").css("visibility", "visible");
            $("#PopUpDiv1").css("visibility", "visible");
            return false;
        }
        function hide(div) {
            document.getElementById(div).style.display = 'none';
            //$("#PopUpDiv").css("visibility", "hidden");
            //$("#PopUpDiv2").css("visibility", "hidden");
            $("#PopUpDiv1").css("visibility", "hidden");
            return false;
        }
    </script>
</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server" enctype="multipart/form-data">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Debug"></asp:ToolkitScriptManager>
        <div style="height: 1000px; width: 1920px">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">
                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtSearchBox" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150" OnTextChanged="txtSearchBox_TextChanged"></asp:TextBox>

                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon  app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">

                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%-- <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>
                                                --%>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div1')">Rapporter</span></span>

                                                    <div id="Div1" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div2')">Løn Modul</span></a>
                                                    <div id="Div2" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>

                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                --%>
                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUpload" />
                    </Triggers>
                    <ContentTemplate>
                        <div style="float: left; height: 1000px; width: 300px; border-color: silver; border-width: 5px">
                            <fieldset style="height: 1000px">
                                <legend style="color: black; font-weight: bold; font-size: small"></legend>
                                <div style="height: 10px;">
                                </div>
                                <table style="width: 100%; font-family: Calibri;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 50%;">
                                                <asp:Button CssClass="button" ID="BtnAddProject" runat="server" Text="+Tilføj Projekter" BackColor="#CC6699" Style="color: #FFFFFF; font-weight: 500; font-size: medium; background-color: #FF0000" OnClick="BtnAddProject_Click" />
                                            </td>
                                            <td style="width: 50%;">
                                                <%-- <table class="x-table-layout" id="ext-gen1688" role="presentation" cellspacing="0" cellpadding="0">
                                                    <tbody role="presentation">
                                                        <tr role="presentation">
                                                            <td class="x-table-layout-cell " role="presentation" rowspan="1" colspan="1"><a tabindex="0" class="x-btn x-unselectable x-sbtn-first x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon" id="button-1109" hidefocus="on" unselectable="on" data-qtip="Simple List"><span class="x-btn-wrap" id="button-1109-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1109-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1109-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-split " id="button-1109-btnIconEl" role="presentation" unselectable="on">&nbsp;</span></span></span></a></td>
                                                            <td class="x-table-layout-cell " role="presentation" rowspan="1" colspan="1"><a tabindex="0" class="x-btn x-unselectable x-sbtn-last x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon x-pressed x-btn-pressed x-btn-default-small-pressed" id="button-1110" hidefocus="on" unselectable="on" data-qtip="Advanced List"><span class="x-btn-wrap" id="button-1110-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1110-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1110-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-no-split " id="button-1110-btnIconEl" role="presentation" unselectable="on"></span></span></span></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px;" colspan="3"></td>
                                        </tr>
                                        <%--<tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>--%>
                                    </tbody>
                                </table>

                                <div style="height: 850px;">
                                    <fieldset>
                                        <legend style="color: black; font-weight: bold; font-size: large">Projekternavn</legend>
                                        <div id="divOverflowcustomer" runat="server" style="overflow: auto; height: 842px">

                                            <asp:GridView ID="GridViewCustomer" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" RowStyle-Height="40px" RowStyle-BorderWidth="5px" BorderColor="White" BorderWidth="0px" RowStyle-BorderColor="White" OnRowCommand="GridViewCustomer_RowCommand" OnRowDataBound="Bound">
                                                <Columns>

                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="10px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Visible="false" />
                                                            <asp:Label ID="ProjectId" runat="server" Text='<%# Bind("ProjectId") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="ProjectName" runat="server" Text='<%# Bind("ProjectName") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="CustId" runat="server" Text='<%# Bind("CustId") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10px" BorderWidth="0" />
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="CustId" HeaderText="CustId" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />

                                                    <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LBtnProjectName" runat="server" Font-Underline="false" Text='<%# Bind("ProjectName")%>' OnClick="LBtnProjectName_Click"> </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="250px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <span onclick="return confirm('Are you sure to Delete the Project?')">
                                                                <asp:LinkButton CssClass="simple" ID="Lnkdeleterow" runat="server" Text="" Font-Underline="false" CommandArgument='<%#Eval("ProjectId") %>' CommandName="DeleteRow"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton></span>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10px" />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </fieldset>
                                </div>
                            </fieldset>
                        </div>

                        <div id="divVertical" style="float: left; height: 1000px; width: 1420px" runat="server">
                            <div style="width: 100%; height: 90px;">

                                <div style="height: 10px"></div>
                                <div style="height: 40px">
                                    <div class="divider" style="height: 30px; width: 50px"></div>
                                    <asp:Label ID="LblProjectName" runat="server" CssClass="clienthead"></asp:Label>
                                    <asp:Label ID="LblProjectId" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="LblContId" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblProid" runat="server" Text="" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label>
                                </div>

                                <div style="height: 40px">
                                    <table>
                                        <tr>
                                            <td style="height: 30px; width: 50px"></td>
                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnOverview" runat="server" Font-Underline="false" OnClick="LBtnOverview_Click2" ForeColor="RoyalBlue">Oversigt</asp:LinkButton></td>

                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnBasicInfo" runat="server" Font-Underline="false" OnClick="LBtnBasicInfo_Click" ForeColor="RoyalBlue">Stamdata</asp:LinkButton></td>

                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnUserList" runat="server" Font-Underline="false" OnClick="LBtnUserList_Click" ForeColor="RoyalBlue">Bruger</asp:LinkButton></td>

                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnOrderList" runat="server" Font-Underline="false" OnClick="LBtnOrderList_Click" ForeColor="RoyalBlue">Ordre</asp:LinkButton></td>

                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnNewTask" runat="server" Font-Underline="false" OnClick="LBtnNewTask_Click1" ForeColor="RoyalBlue">Ny opgave</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnAPV" runat="server" Font-Underline="false" OnClick="LBtnAPV_Click" ForeColor="RoyalBlue">APV</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnReport" runat="server" Font-Underline="false" OnClick="LBtnReport_Click" ForeColor="RoyalBlue">Report</asp:LinkButton></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="gridv" id="divOverView" runat="server" style="height: 860px;" visible="true">
                                <fieldset>
                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>

                                    <div style="height: 100px"></div>
                                    <div style="height: 750px; width: 100%">

                                        <asp:Chart ID="ChartOrder" runat="server" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)" Palette="BrightPastel" ImageType="Png"
                                            BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderlineWidth="2"
                                            BackColor="#D3DFF0" BorderlineColor="26,59,105" Height="350px" Width="1350px">

                                            <BorderSkin SkinStyle="Emboss" />

                                            <Legends>
                                                <asp:Legend Name="Legend1" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="false"></asp:Legend>
                                            </Legends>

                                            <Titles>
                                                <asp:Title Alignment="TopCenter" BackColor="180, 165, 191, 228" BackGradientStyle="TopBottom" BackHatchStyle="None"
                                                    Font="Microsoft Sans Serif, 12pt, style=Bold" Name="Revenue" Text="Projekter Ordre" ToolTip="Projekter Ordre" ForeColor="26, 59, 105">
                                                </asp:Title>

                                                <asp:Title Alignment="TopCenter" BackColor="Transparent" Font="Microsoft Sans Serif, 10pt, style=Bold" ToolTip="Chart Type"></asp:Title>
                                            </Titles>
                                            <Series>
                                                <asp:Series Name="Count" ChartArea="ChartArea1" Legend="Legend1" CustomProperties="DrawingStyle=Cylinder" BorderColor="64, 0, 0, 0"
                                                    Color="224, 64, 10" MarkerSize="5">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" BackColor="64, 165, 191, 228" BackSecondaryColor="White" BorderColor="64, 64, 64, 64" ShadowColor="Transparent"
                                                    BackGradientStyle="TopBottom">
                                                    <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="false" Title="Total Cost" ArrowStyle="Triangle" Minimum="1000" Maximum="50000" Interval="5000">
                                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                                    </AxisY>
                                                    <AxisX IsLabelAutoFit="false" LineColor="64, 64, 64, 64" Title="Month" ArrowStyle="Triangle" Interval="1">
                                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" IsEndLabelVisible="false" Angle="0" />
                                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                                    </AxisX>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                        </asp:Chart>

                                    </div>
                                    <%-- <asp:GridView CssClass="gridv" ID="GridView2" runat="server">
                            </asp:GridView>--%>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divBasicInfo" runat="server" style="height: 860px;" visible="false">

                                <%--<div style="height: 40px">
                        </div>
                        <div style="height: 30px"></div>--%>
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <div style="height: 850px; width: 100%">

                                        <table style="width: 100%;">
                                            <tr>
                                                <td colspan="3" style="height: 80px; font-weight: bold; font-size: large">Stamdata:</td>
                                            </tr>
                                            <tbody>
                                                <tr>
                                                    <td style="height: 20px;" colspan="2"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 70%;">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td style="text-align: right;">Navn</td>
                                                                <td style="width: 40px"></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtProjectName" runat="server" Width="300px" Height="31px" Text="New Project"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="ReqtxtProjectName" runat="server" ErrorMessage="*" ControlToValidate="txtProjectName" ValidationGroup="Project"></asp:RequiredFieldValidator>
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td style="height: 20px;" colspan="3"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right;">Kunder</td>
                                                                <td style="width: 40px"></td>
                                                                <td colspan="2">
                                                                    <%--<asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmployeeName" runat="server" Width="300px" Height="31px" AutoPostBack="true" OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>--%>
                                                                    <asp:Label ID="lblCustid" runat="server" Text="" Visible="false"></asp:Label>
                                                                    <asp:DropDownList CssClass="search_categories" ID="ddlCustName" runat="server" Width="300px" Height="42px" Font-Size="10" AutoPostBack="true" DataTextField="CustName" DataValueField="CustId" OnSelectedIndexChanged="ddlCustName_SelectedIndexChanged"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 20px;" colspan="3"></td>
                                                            </tr>
                                                            <%-- <tr>
                                                    <td style="text-align: right;">Employee</td>
                                                    <td style="width: 40px"></td>
                                                    <td colspan="2">
                                                        
                                                        <asp:ListBox CssClass="twitterStyleTextbox" ID="LstBoxEmployee" runat="server" Width="300px" Height="36px" SelectionMode="Multiple" Rows="5"></asp:ListBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>--%>
                                                            <tr>
                                                                <td style="text-align: right;">Description </td>
                                                                <td style="width: 40px"></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDescript" runat="server" Width="300px" Height="50px"></asp:TextBox>
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtCompareDate" runat="server" Style="display: none"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 20px;" colspan="3"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right;">Est.Start Date</td>
                                                                <td style="width: 40px"></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStartdt" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                    <cc1:DatePicker ID="dtp3" runat="server" Width="300px" CssClass="twitterStyleTextbox" DateFormat="dd-MM-yyyy" Visible="false"></cc1:DatePicker>
                                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtStartdt" CssClass="Calendar" Format="dd-MM-yyyy" Enabled="true"></asp:CalendarExtender>

                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom, Numbers" ValidChars="-/" TargetControlID="txtStartdt"></asp:FilteredTextBoxExtender>
                                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Enter Valid Date" Font-Size="Small" Type="Date" Operator="GreaterThanEqual" ControlToValidate="txtStartdt" ControlToCompare="txtCompareDate" ValidationGroup="NextButtonCLick"></asp:CompareValidator>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 20px;" colspan="3"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right;">Est.Completion Date</td>
                                                                <td style="width: 40px"></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtCompletionDate" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                    <cc1:DatePicker ID="dtp4" runat="server" Width="300px" CssClass="twitterStyleTextbox" DateFormat="dd-MM-yyyy" Visible="false"></cc1:DatePicker>
                                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtCompletionDate" CssClass="Calendar" Format="dd-MM-yyyy" Enabled="true"></asp:CalendarExtender>

                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom, Numbers" ValidChars="-/" TargetControlID="txtCompletionDate"></asp:FilteredTextBoxExtender>
                                                                    <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Enter Valid Date" Font-Size="Small" Type="Date" Operator="GreaterThanEqual" ControlToValidate="txtCompletionDate" ControlToCompare="txtStartdt" ValidationGroup="NextButtonCLick"></asp:CompareValidator>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 20px;" colspan="3"></td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                    <%--<td style="width: 40%; vertical-align: top;"></td>--%>
                                                </tr>
                                            </tbody>

                                        </table>

                                    </div>
                                </fieldset>
                                <%-- <div class="shadowdiv" style="width: 100%; height: 50px; text-align: center; border: solid 0px black;">
                            <table>
                                <tbody>
                                    <tr>

                                       
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>--%>
                                <asp:GridView CssClass="gridv" ID="GridViewBasicInfo" AutoGenerateColumns="false" Visible="false" runat="server">
                                </asp:GridView>

                            </div>
                            <div class="gridv" id="divContactInfo" runat="server" style="height: 860px;" visible="false">
                                <fieldset style="height: 860px">
                                    <legend style="color: black; font-weight: bold; font-size: large"></legend>
                                    <div>
                                        <table>
                                            <tr>
                                                <td style="width: 90px;"></td>
                                                <td style="width: 100px; font-weight: 700">Id</td>
                                                <td style="width: 250px; font-weight: 700">Brugernavn</td>
                                                <%-- <td style="width: 200px; font-weight: 700">SkillSets</td>--%>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="divEmptlist" style="height: 270px; width: 100%; overflow: auto;">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridViewEmpname" runat="server" DataKeyNames="EmpId" ShowHeader="false" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select" ItemStyle-Width="10" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" OnCheckedChanged="chkSelect_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                                                                    <asp:Label ID="lbllEmpid" runat="server" Text='<%# Bind("EmpId") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="10" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="EmpId" HeaderText="EmpId" Visible="true" ItemStyle-Width="20px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                            <asp:BoundField DataField="FName" HeaderText="Employee Name" ItemStyle-Width="250" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                            <%--  <asp:BoundField DataField="SkillSets" HeaderText="SkillSets" HeaderStyle-Width="200px" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />--%>

                                                            <asp:CommandField SelectText="Select" ShowSelectButton="true" Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                    <div style="height: 30px; width: 100%"></div>
                                    <table style="width: 100%;">
                                        <tbody>

                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridViewSelEmployee" AutoGenerateColumns="false" Width="100%" runat="server" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px" OnRowDataBound="GridViewSelEmployee_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" Visible="false" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="2px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Id" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpID" runat="server" Text='<%# Bind("EmpId") %>' Visible="true"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="50px" />
                                                                <ItemStyle Width="50px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Brugernavn" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNAME" runat="server" Text='<%# Bind("FName") %>' Visible="true"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="200px" />
                                                                <ItemStyle Width="200px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="WagePerHr" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtWagePerHr" runat="server" Height="31" Width="50" Text='<%# Bind("WagePerHr") %>'></asp:TextBox>
                                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx3" Enabled="true" TargetControlID="txtWagePerHr" FilterMode="ValidChars" ValidChars="0123456789.,"></asp:FilteredTextBoxExtender>
                                                                </ItemTemplate>
                                                                <EditItemTemplate></EditItemTemplate>
                                                                <HeaderStyle Width="30px" />
                                                                <ItemStyle Width="30px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="MainTask" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <%-- <asp:Label ID="LbllTaskId" runat="server" Text='<%# Bind("TaskId") %>' Visible="false"></asp:Label>--%>
                                                                    <%--   <asp:DropDownList CssClass="search_categories" ID="ddlMainTask" runat="server" Height="36px"  OnSelectedIndexChanged="ddlMainTask_SelectedIndexChanged1" AutoPostBack="true"></asp:DropDownList>--%>
                                                                    <asp:DropDownList ID="ddlMainTask" CssClass="search_categories" runat="server" Height="42px" AutoPostBack="true" OnSelectedIndexChanged="ddlMainTask_SelectedIndexChanged"></asp:DropDownList>
                                                                </ItemTemplate>
                                                                <EditItemTemplate></EditItemTemplate>
                                                                <HeaderStyle Width="200px" />
                                                                <ItemStyle Width="200px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SubTask" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <%-- <asp:Label ID="LbllTaskId" runat="server" Text='<%# Bind("TaskId") %>' Visible="false"></asp:Label>--%>
                                                                    <asp:DropDownList CssClass="search_categories" ID="ddlSubTaskList" runat="server" Height="42px" DataTextField="SubTask" DataValueField="STID" Visible="true" Width="200px"></asp:DropDownList>
                                                                </ItemTemplate>
                                                                <EditItemTemplate></EditItemTemplate>
                                                                <HeaderStyle Width="200px" />
                                                                <ItemStyle Width="200px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="EstimatedHrs" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEstiHrs" runat="server" Height="31" Width="200" Text='<%# Bind("EstimatedHours") %>'></asp:TextBox>
                                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx4" Enabled="true" TargetControlID="txtEstiHrs" FilterMode="ValidChars" ValidChars="0123456789.,"></asp:FilteredTextBoxExtender>

                                                                </ItemTemplate>
                                                                <EditItemTemplate></EditItemTemplate>
                                                                <HeaderStyle Width="30px" />
                                                                <ItemStyle Width="30px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px;"></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div style="height: 30px; width: 100%"></div>
                                    <div>
                                        <asp:Button ID="Button1" CssClass="buttonn" runat="server" Text="ADD" BackColor="#009933" Visible="false" />
                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divOrder" runat="server" style="height: 860px;" visible="false">
                                <fieldset style="height: 860px">
                                    <legend></legend>
                                    <div>
                                        <fieldset>
                                            <legend></legend>
                                            <table>
                                                <tr>
                                                    <td style="text-align: right;">Ordre</td>
                                                    <td style="width: 40px"></td>
                                                    <td colspan="2">
                                                        <asp:DropDownList CssClass="search_categories" ID="ddOrderList" runat="server" Width="300px" Height="42px" OnSelectedIndexChanged="ddOrderList_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        <asp:Label ID="LblOrder" runat="server" Text="" Visible="false"></asp:Label>
                                                        <asp:Label ID="LabelID" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>

                                    <fieldset style="height: 790px">
                                        <legend style="color: black;"></legend>
                                        <div runat="server" style="height: 790px; width: 100%;">

                                            <div id="DivOrderList2" runat="server" style="height: 790px; width: 50%; overflow: auto; float: left;">
                                                <fieldset style="height: 785px">
                                                    <legend>Ordre:</legend>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GrdViewOrderDetails" runat="server" ShowFooter="true" HeaderStyle-Height="30" AutoGenerateColumns="false" Width="100%" Font-Size="Medium" HeaderStyle-ForeColor="RoyalBlue" HeaderStyle-BackColor="LightGray" OnRowCommand="GrdViewOrderDetails_RowCommand">
                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="Id">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblOrderID" runat="server" Text='<%# Bind("OrderId") %>' Visible="false"></asp:Label>
                                                                                <asp:LinkButton ID="LnkBtnOrderId" runat="server" Text='<%# Bind("OrderId") %>' Font-Underline="false" OnClick="LnkBtnOrderId_Click"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Dato">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblOrderDate" runat="server" Text='<%# Bind("OrderDate") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Total">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTotalOrder" runat="server" Text='<%# Bind("OrderTotal") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Taxes" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTaxes" runat="server" Text='<%# Bind("Taxes") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Penalty" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPenalty" runat="server" Text='<%# Bind("Penalty") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Incentive" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblIncentive" runat="server" Text='<%# Bind("Incentive") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton CssClass="simple" ID="LinkDeleteOrder" runat="server" Font-Underline="false" CommandArgument='<%#Eval("OrderId") %>' CommandName="DeleteRow"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </div>
                                            <div id="divOrderlist1" runat="server" style="height: 790px; width: 49%; overflow: auto; float: right;">
                                                <fieldset style="height: 785px">
                                                    <legend>Ordre detaljer:</legend>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GridViewEditOrder" runat="server" ShowFooter="true" AutoGenerateColumns="false" Width="100%" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White" Style="font-size: medium" OnRowDataBound="Bound" HeaderStyle-ForeColor="RoyalBlue" HeaderStyle-BackColor="LightGray">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Service/Varenr" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle />
                                                                            <ItemStyle CssClass="Itemalign" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Navn" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemName" runat="server" Text='<%# Bind("ItemName") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle CssClass="Itemalign" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Pris" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemPrice" runat="server" Text='<%# Bind("Price") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle CssClass="Itemalign" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Antal" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemQuantity" runat="server" Text='<%# Bind("Quantity") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle CssClass="Itemalign" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Total" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemtotal" runat="server" Text='<%# Bind("TotalPrice") %>' Visible="true"></asp:Label>

                                                                            </ItemTemplate>
                                                                            <ItemStyle CssClass="Itemalign" />
                                                                            <FooterTemplate>
                                                                                <asp:Label ID="lblTot" runat="server" Text="" Visible="true"></asp:Label>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>

                                                                </asp:GridView>

                                                            </td>

                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </fieldset>
                                </fieldset>
                            </div>
                            <div runat="server" id="NewTask" style="height: 860px; font-family: Calibri;" visible="false">
                                <fieldset style="color: black;">
                                    <legend></legend>
                                    <table style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td style="width: 6%">

                                                    <asp:Button CssClass="buttonn" ID="AddTask" runat="server" Text="+ Tilføj opgave" OnClick="AddTask_Click" />
                                                </td>

                                                <td style="width: 5.5%">Hours
                                                 <asp:DropDownList ID="ddlHours" runat="server" CssClass="search_categories" Height="35">
                                                 </asp:DropDownList>
                                                </td>
                                                <td style="width: 2.7%">
                                                    <asp:DropDownList ID="ddlMinutes" runat="server" CssClass="search_categories" Height="35">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: .4%"></td>
                                                <td style="width: 3.2%">
                                                    <asp:DropDownList ID="ddlSeconds" runat="server" CssClass="search_categories" Height="35">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%"></td>
                                                <td style="width: 38%">

                                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                                    <asp:Button CssClass="buttonn" ID="btnUpload" runat="server" Text="Vedhæft" OnClick="btnUpload_Click" />
                                                </td>

                                                <td style="width: 13%; text-align: right">
                                                    <asp:Button CssClass="buttonn" ID="SaveTask" runat="server" Text="Gem" OnClick="SaveTask_Click" />
                                                    <asp:Button CssClass="buttonn" ID="btnVIewTask" runat="server" Text="Vis" OnClick="btnVIewTask_Click" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table id="tblData" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th style="width: 270px;">Opgavenr:
                                                 <asp:Label ID="lbltaskid" runat="server"></asp:Label></th>
                                                <th style="width: 57px;"></th>
                                                <th>Opgave:
                                                  <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMTask" Text="" runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </fieldset>

                                <div id="divOverflow" runat="server" style="width: 100%; height: 200px; font-family: Calibri; overflow: auto;">
                                    <fieldset style="color: black;">
                                        <legend></legend>
                                        <table id="tbltask" runat="server" style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gridTask" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px" OnRowDataBound="gridTask_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SR.No" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltaskid" runat="server" Text='<%# Bind("TaskId") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblSubtask" runat="server" Text='<%# Bind("Subtask") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SubTask" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <span class="title">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTask" Text='<%# Bind("TaskName") %>' runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>
                                                                        <%-- <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTask1" runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                            <%-- <asp:TemplateField HeaderText="Expected Hours" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtExpectedHrs" runat="server" Height="30" Width="40" Text="00:00"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                </asp:TemplateField>--%>

                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList CssClass="search_categories" ID="DDLSkillSet" runat="server" Height="35" Width="200"></asp:DropDownList>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="200px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <span class="actions">
                                                                        <span class="spanid" title="Task Info"><span class="action-icon icon-eye"></span></span>
                                                                        <span class="spanid" title=" Task Note"><span class="action-icon icon-note"></span></span>
                                                                        <span class="spanid" title="Due Date"><span class="action-icon icon-credit-card "></span></span>
                                                                        <%--   <span class="spanid" title="Delete Task"><a class="pagerLink" runat="server" onclick=" delRow()">
                                                                                    <span class="action-icon icon-trashcan"></span></a></span>--%>
                                                                        <asp:LinkButton ID="lnkDeletetask" CssClass="pagerLink" runat="server" OnClientClick="delRow()" OnClick="lnkDeletetask_Click">
                                                                                    <span class="action-icon icon-trashcan"></span></asp:LinkButton></span>
                                                                    <span class="spanid" title="Add file"><span runat="server" class="action-icon icon-attach spanid"></span>
                                                                    </span></span>
                                                                            </span>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStatus" runat="server" Text="Open" Height="31" Enabled="false" Width="100"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="tbl" runat="server" style="width: 100%;" tabindex="0" visible="false">
                                            <tbody>
                                                <tr id="tasktr_0" runat="server">
                                                    <td style="width: 20px">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="TaskId" runat="server" Height="30" Width="30"></asp:TextBox>
                                                        <asp:CheckBox ID="Chkselectt_0" runat="server" Visible="false" />
                                                        <asp:Label ID="TextId" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                    <td style="width: 800px; height: 40px">
                                                        <span class="title">
                                                            <%--<div style="display: block;" class="editable description dataIndex-description" contenteditable="true" spellcheck="false" data-medium-editor-element="true" role="textbox" aria-multiline="true" data-medium-focused="true"></div>--%>
                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txttask_0" runat="server" Height="31" Visible="true" Width="600"></asp:TextBox>

                                                            <span class="actions">
                                                                <span class="spanid" title="Task Info"><span class="action-icon icon-eye"></span></span>
                                                                <span class="spanid" title=" Task Note"><span class="action-icon icon-note"></span></span>
                                                                <span class="spanid" title="Due Date"><span class="action-icon icon-credit-card "></span></span>
                                                                <span class="spanid" title="Delete Task"><a class="pagerLink" runat="server" onclick=" delRow()"><span class="action-icon icon-trashcan"></span></a></span>
                                                                <span class="spanid" title="Add file"><%--<a runat="server" class="pagerLink" href="javascript:void(0)" onclick="OpenFileUpload();">--%><span runat="server" class="action-icon icon-attach spanid"></span><%--</a>--%>
                                                                    <%-- <iframe class="hide" id="frameUpload" name="frameUpload" src="FileUpload.aspx" width="0" height="0" runat="server" />--%>
                                                                </span></span>
                                                        </span>
                                                    </td>
                                                    <td runat="server" style="height: 20px; width: 250px">
                                                        <div>
                                                            <asp:DropDownList CssClass="search_categories" ID="DDLSkillSet" runat="server" Height="35" Width="200"></asp:DropDownList>
                                                        </div>
                                                    </td>
                                                    <td runat="server" style="height: 20px;">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTimeDate" runat="server" Height="31" Width="150"></asp:TextBox>
                                                        <asp:CalendarExtender ID="CalenderExtender1" CssClass="Calendar" Enabled="true" runat="server" TargetControlID="txtTimeDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                    </td>
                                                    <td runat="server" style="height: 20px; text-align: right">
                                                        <div>
                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStatus" runat="server" Text="Open" Height="31" Enabled="false" Width="100"></asp:TextBox>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </fieldset>
                                </div>

                                <fieldset id="Idfiledetails" runat="server" visible="false">
                                    <legend>Oplysninger:</legend>
                                    <div style="height: 50px; width: 100%">
                                        <table style="height: 50px; width: 100%">
                                            <tr>
                                                <td style="width: 180px; font-weight: 700">Filnavn</td>
                                                <%--   <td style="width: 190px; font-weight: 700">FilePath</td>--%>
                                                <td style="width: 100px; font-weight: 700">Vis</td>

                                            </tr>
                                        </table>
                                    </div>
                                    <div id="GridVOverflow" runat="server" style="width: 100%; height: 200px; font-family: Calibri; overflow: auto;">
                                        <asp:GridView ID="gvFileUpload" runat="server" ShowHeader="false" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px">
                                            <Columns>

                                                <asp:BoundField DataField="TaskId" HeaderText="TaskId" Visible="false" />
                                                <asp:BoundField DataField="FileName" HeaderText="FileName" ItemStyle-Width="90px" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                <%--     <asp:BoundField DataField="FilePath" HeaderText="FilePath" ItemStyle-Width="100" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />--%>
                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFilePath" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnViewImage" runat="server" Font-Underline="false" OnClick="lbtnViewImage_Click"><span class="action-icon icon-eye"></span></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </fieldset>


                                <fieldset id="IdTaskDetails" runat="server" visible="false">
                                    <legend>Opgave detaljer:</legend>
                                    <div id="divTaskView" runat="server" style="width: 100%; height: 300px; font-family: Calibri;">

                                        <div style="height: 50px; width: 100%">
                                            <table style="height: 50px; width: 100%;">
                                                <tr>
                                                    <td style="width: 05%; font-weight: 700">SrNo.</td>
                                                    <td style="width: 40%; font-weight: 700">Sub Task</td>
                                                    <td style="width: 35%; font-weight: 700">SkillSet</td>
                                                    <td style="width: 20%; font-weight: 700">ExpectedHours</td>
                                                    <%--  <td style="width: 200px; font-weight: 700">FileName</td>--%>
                                                </tr>
                                            </table>
                                        </div>

                                        <div id="divTaskView1" runat="server" style="width: 100%; height: 220px; font-family: Calibri; overflow: auto;">
                                            <asp:GridView ID="gvTaskView" runat="server" ShowHeader="false" AutoGenerateColumns="false" Width="100%">
                                                <Columns>

                                                    <asp:TemplateField HeaderText="SRNo." HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTaskId" runat="server" Text='<%# Bind("TaskId") %>' Visible="False"></asp:Label>
                                                            <asp:Label ID="lblSTID" runat="server" Text='<%# Bind("STID") %>' Visible="False"></asp:Label>
                                                            <asp:Label ID="lblID" runat="server" Text='<%#Container.DataItemIndex + 1%>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="05%" />

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Task" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                        <ItemTemplate>
                                                            <%--   <asp:Label ID="lblTask" runat="server" Text='<%# Bind("Task") %>' Visible="true"></asp:Label>--%>
                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Bind("SubTask") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="40%" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="SkillSet" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSkillSet" runat="server" Text='<%# Bind("SkillSet") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="35%" />

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ExpectedHours" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblExpectedHours" runat="server" Text='<%# Bind("ExpectedHours") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20%" />

                                                    </asp:TemplateField>

                                                    <%--<asp:TemplateField HeaderText="FileName" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Bind("FileName") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="200" />
                                            <HeaderStyle Width="200" />
                                        </asp:TemplateField>--%>
                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="" id="divAPV" runat="server" style="height: 880px; width: 100%;" visible="false">
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>

                                    <div style="height: 10px; width: 100%;">
                                        <asp:Button CssClass="buttonn" ID="BtnAddAPV" runat="server" Text="+ADDAPV" OnClick="BtnAddAPV_Click" Visible="false" />
                                    </div>
                                    <div style="height: 870px; width: 100%;">
                                        <div style="height: 20px; width: 100%;"></div>
                                        <div id="DivAPV_Header" style="height: 50px; width: 100%;" runat="server" visible="false">
                                            <table border="0" style="font-family: Calibri; width: 100%;">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 3%; font-weight: 700"></td>
                                                    <td style="width: 7%; font-weight: 700">APVID:</td>
                                                    <td style="width: 10%; font-weight: 700">Sag</td>
                                                    <td style="width: 35%; font-weight: 700">Kunde</td>
                                                    <td style="width: 10%; font-weight: 700">Kontakt</td>
                                                    <td style="width: 10%; font-weight: 700">Telefon</td>
                                                    <td style="width: 15%; font-weight: 700">Indgået Dato:</td>
                                                    <td style="width: 5%; font-weight: 700">Vis</td>
                                                    <td style="width: 5%; font-weight: 700">Slet</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height: 800px; width: 100%; overflow: auto;">
                                            <table style="font-family: Calibri; width: 100%;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridViewAPV" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" RowStyle-BorderWidth="5px" BorderColor="White" BorderWidth="0px" RowStyle-BorderColor="White">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAPIDV" runat="server" Text='<%# Bind("APVID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="3%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="APVID" HeaderText="APVID" ItemStyle-Width="7%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="sag" HeaderText="sag" ItemStyle-Width="10%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Kunde" HeaderText="Kunde" ItemStyle-Width="35%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Kontakt" HeaderText="Kontakt" ItemStyle-Width="10%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Telefon" HeaderText="Telefon" ItemStyle-Width="10%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="ComplationDate" HeaderText="ComplationDate" ItemStyle-Width="15%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LBtViewAPV" runat="server" Font-Underline="false" OnClick="LBtViewAPV_Click"><span class="action-icon icon-eye"></span></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LbtnDeleteAPV" runat="server" Font-Underline="false" OnClick="LbtnDeleteAPV_Click"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div runat="server" id="PopUpDiv1" style="height: 880px; width: 82.5%; overflow: auto; visibility: hidden; z-index: 101; background-color: white; position: absolute; left: 17.5%; top: 95px;">

                                        <div style="height: 50px; width: 100%;">
                                            <span class="menu-item-icon icon-times-circle-o" style="float: right" onclick="ShowDiv2('PopUpDiv1')"></span>
                                            <asp:Button CssClass="buttonn" ID="BtnAPV" runat="server" Text="Export" Style="float: right" Visible="false" />
                                            <asp:Button CssClass="buttonn" ID="BtnSaveAPV" runat="server" Text="Save" Style="float: right" Visible="false" OnClick="BtnSaveAPV_Click" />
                                            <asp:Button CssClass="buttonn" ID="BtnUpdateAPV" runat="server" Text="Update" Style="float: right" Visible="false" OnClick="BtnUpdateAPV_Click" />

                                            <asp:Label ID="lblSimpleAPVID" runat="server" Text="" Visible="false"></asp:Label>
                                        </div>
                                        <div runat="server" style="height: 130px; width: 50%;">
                                            <table runat="server" style="height: 100%; width: 100%;">
                                                <tr style="width: 100%;">
                                                    <td style="width: 50%; float: left;">
                                                        <asp:Image ID="imguser" runat="server" Height="100%" Width="60%" ImageUrl="~/image/pmglogo1.jpg" />
                                                    </td>
                                                    <td style="width: 50%; float: right;">

                                                        <asp:Label ID="lblsagsnr" Text="Sagsnr.:" runat="server" Font-Size="24px"></asp:Label><br />


                                                        <asp:Label Text="Sags-APV" runat="server" Font-Size="55px"></asp:Label>

                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 60px; width: 50%;">
                                            <table runat="server" style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 100%; width: 50%; float: left; border-left: solid; border-top: solid;">

                                                        <asp:Label ID="lblsag" Text="Sag:" runat="server" Font-Size="24px"></asp:Label>
                                                        <asp:TextBox ID="txtsag" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                    </td>
                                                    <td style="height: 100%; width: 50%; float: right; border-left: solid; border-top: solid; border-right: solid;">
                                                        <asp:Label ID="lblPabegy" Text="Påbegyndes/udføres:" runat="server" Font-Size="24px"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 60px; width: 50%;">
                                            <table runat="server" style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 100%; width: 50%; float: left; border-left: solid; border-top: solid;">

                                                        <asp:Label ID="lblKunde" Text="Kunde:" runat="server" Font-Size="24px"></asp:Label>
                                                        <asp:TextBox ID="txtKunde" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>

                                                    </td>
                                                    <td style="height: 100%; width: 25%; float: left; border-left: solid; border-top: solid;">
                                                        <asp:Label ID="lblKondu" Text="Konduktør:" runat="server" Font-Size="24px"></asp:Label>
                                                    </td>
                                                    <td style="height: 100%; width: 25%; float: left; border-left: solid; border-top: solid; border-right: solid;">
                                                        <asp:Label ID="lblVej" Text="Vej & Park:" runat="server" Font-Size="24px"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 60px; width: 50%;">
                                            <table runat="server" style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 100%; width: 50%; float: left; border-left: solid; border-top: solid;">

                                                        <asp:Label ID="lblKontakt" Text="Kontakt:" runat="server" Font-Size="24px"></asp:Label>
                                                        <asp:TextBox ID="txtKontakt" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender13" Enabled="true" TargetControlID="txtKontakt" FilterType="Numbers" InvalidChars=" "></asp:FilteredTextBoxExtender>

                                                    </td>
                                                    <td style="height: 100%; width: 50%; float: right; border-left: solid; border-top: solid; border-right: solid;">
                                                        <asp:Label ID="lblIndgaet" Text="Indgået dato:" runat="server" Font-Size="24px"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 60px; width: 50%;">
                                            <table runat="server" style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 100%; width: 50%; float: left; border-left: solid; border-top: solid;">

                                                        <asp:Label ID="lbltelefon" Text="Telefon:" runat="server" Font-Size="24px"></asp:Label>
                                                        <asp:TextBox ID="txtTelefon" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender14" Enabled="true" TargetControlID="txtTelefon" FilterType="Numbers" InvalidChars=" "></asp:FilteredTextBoxExtender>

                                                    </td>
                                                    <td style="height: 100%; width: 50%; float: right; border-left: solid; border-top: solid; border-right: solid;">
                                                        <asp:Label ID="lblsjak" Text="Sjak:" runat="server" Font-Size="24px"></asp:Label>
                                                        <asp:TextBox ID="txtSjak" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 120px; width: 50%;">
                                            <table runat="server" style="height: 100%; width: 100%;">
                                                <tr>

                                                    <td style="height: 100%; width: 25%; float: left; border-left: solid; border-top: solid;">
                                                        <asp:Label ID="lblstillads" Text="Stillads til:" runat="server" Font-Size="24px"></asp:Label>
                                                        <table runat="server" style="height: 75%; width: 100%;">
                                                            <tr>
                                                                <%--<asp:Label ID="lblstillads" Text="Stillads til:" runat="server" Font-Size="24px"></asp:Label>--%>
                                                                <td style="height: 100%; width: 50%; float: left; border-top: solid;">
                                                                    <asp:CheckBoxList ID="ChkLstStillads1" runat="server">
                                                                        <asp:ListItem> Facade</asp:ListItem>
                                                                        <asp:ListItem>  Vinduer</asp:ListItem>
                                                                        <asp:ListItem>  Tag</asp:ListItem>

                                                                    </asp:CheckBoxList>
                                                                </td>
                                                                <td style="height: 100%; width: 50%; float: right; border-left: solid; border-top: solid;">
                                                                    <asp:CheckBoxList ID="ChkLstStillads2" runat="server">
                                                                        <asp:ListItem> Gade</asp:ListItem>
                                                                        <asp:ListItem>  Gård</asp:ListItem>
                                                                        <asp:ListItem>  Andet</asp:ListItem>

                                                                    </asp:CheckBoxList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>

                                                    <td style="height: 100%; width: 25%; float: left; border-left: solid; border-top: solid;">
                                                        <asp:Label ID="Label4" Text=" " runat="server" Font-Size="20px"></asp:Label><br />
                                                        <asp:Label ID="lblAntalranker" Text="Antal ranker:" runat="server" Font-Size="20px"></asp:Label><br />
                                                        <asp:Label ID="lblLangde" Text="Længde:" runat="server" Font-Size="20px"></asp:Label><br />
                                                        <asp:Label ID="lblHojde" Text="Højde:" runat="server" Font-Size="20px"></asp:Label>

                                                    </td>
                                                    <td style="height: 100%; width: 25%; float: left; border-left: solid; border-top: solid;">
                                                        <asp:Label ID="lblStilladssystem" Text="Stilladssystem:" runat="server" Font-Size="24px"></asp:Label>

                                                        <asp:CheckBoxList ID="ChklstStilladssystem" runat="server">
                                                            <asp:ListItem> (+8)</asp:ListItem>
                                                            <asp:ListItem>  Haki ram</asp:ListItem>
                                                            <asp:ListItem>  Haki mur</asp:ListItem>

                                                        </asp:CheckBoxList>
                                                    </td>
                                                    <td style="height: 100%; width: 25%; float: left; border-left: solid; border-top: solid; border-right: solid;">
                                                        <asp:Label ID="lblLastklasse" Text="Lastklasse:" runat="server" Font-Size="24px"></asp:Label>

                                                        <asp:CheckBoxList ID="ChklstLastklasse" runat="server">
                                                            <asp:ListItem> Kl.3-200Kg/m2</asp:ListItem>
                                                            <asp:ListItem>  Kl.5-450 Kg/m2</asp:ListItem>
                                                            <asp:ListItem>   _____________</asp:ListItem>

                                                        </asp:CheckBoxList>
                                                    </td>

                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 255px; width: 50%;">
                                            <table runat="server" style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 100%; width: 25%; float: left; border-left: solid; border-top: solid;">
                                                        <asp:Label ID="lblFastgorelser" Text="Fastgørelser:" runat="server" Font-Size="24px"></asp:Label>
                                                        <asp:CheckBoxList ID="ChklstFastgørelser" runat="server">
                                                            <asp:ListItem>   Mur</asp:ListItem>
                                                            <asp:ListItem>    Træ</asp:ListItem>
                                                            <asp:ListItem>    Beton</asp:ListItem>
                                                            <asp:ListItem>    Andet</asp:ListItem>
                                                        </asp:CheckBoxList>
                                                        <table style="height: 50%; width: 100%;">
                                                            <tr>
                                                                <td style="border-top: solid">
                                                                    <asp:Label ID="Label3" Text="Strømforsyning:" runat="server" Font-Size="24px"></asp:Label>
                                                                    <asp:CheckBoxList ID="ChklstStrømforsyning" runat="server">
                                                                        <asp:ListItem>   Byggestrøm</asp:ListItem>
                                                                        <asp:ListItem>    Generator</asp:ListItem>
                                                                        <asp:ListItem>    Andet</asp:ListItem>
                                                                    </asp:CheckBoxList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="height: 100%; width: 32%; float: left; border-left: solid; border-top: solid;">
                                                        <asp:Label ID="lblTransportafmateriel" Text="Transport af materiel:" runat="server" Font-Size="24px"></asp:Label>
                                                        <asp:CheckBoxList ID="ChklstTran_materiel" runat="server">
                                                            <asp:ListItem>   Lastbil</asp:ListItem>
                                                            <asp:ListItem>    Lille bil</asp:ListItem>
                                                            <asp:ListItem>    Truck</asp:ListItem>
                                                            <asp:ListItem>   Trækvogn</asp:ListItem>
                                                            <asp:ListItem>    Alurullevogne</asp:ListItem>
                                                            <asp:ListItem>    Andet</asp:ListItem>
                                                        </asp:CheckBoxList>
                                                        <asp:Label ID="lblTransportantalgange" Text="Transport antal gange:" runat="server" Font-Size="20px"></asp:Label>
                                                    </td>
                                                    <td style="height: 100%; width: 43%; float: left; border-left: solid; border-top: solid; border-right: solid;">
                                                        <asp:Label ID="lblPersonlige" Text="Personlige værnemidler:" runat="server" Font-Size="24px"></asp:Label>
                                                        <asp:CheckBoxList ID="Chklst_værnemidler" runat="server">
                                                            <asp:ListItem>   Sele/line</asp:ListItem>
                                                            <asp:ListItem>    Maske</asp:ListItem>
                                                            <asp:ListItem>    Høreværn</asp:ListItem>
                                                            <asp:ListItem>   Briller</asp:ListItem>
                                                            <asp:ListItem>    Andet</asp:ListItem>
                                                        </asp:CheckBoxList>
                                                        <table style="height: 40%; width: 100%;">
                                                            <tr>
                                                                <td style="border-top: solid">
                                                                    <asp:Label ID="lblVelfardsforanstal" Text="Velfærdsforanstaltninger:" runat="server" Font-Size="24px"></asp:Label>
                                                                    <asp:CheckBoxList ID="Chklst_Velfærdsforanstaltninger" runat="server">
                                                                        <asp:ListItem>   Deleskur</asp:ListItem>
                                                                        <asp:ListItem>    Mangler</asp:ListItem>
                                                                        <asp:ListItem>    Adgang til toilet</asp:ListItem>

                                                                    </asp:CheckBoxList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 60px; width: 50%;">
                                            <table style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 60px; width: 100%; border-left: solid; border-top: solid; border-right: solid;"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 60px; width: 50%;">
                                            <table style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 60px; width: 100%; border-left: solid; border-top: solid; border-right: solid;"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 60px; width: 50%;">
                                            <table style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 60px; width: 100%; border-left: solid; border-top: solid; border-right: solid;"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 60px; width: 50%;">
                                            <table style="height: 100%; width: 100%;">
                                                <tr>
                                                    <td style="height: 60px; width: 100%; border: solid;"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divReport" runat="server" style="height: 860px;" visible="true">
                                <fieldset>
                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>

                                    <div style="height: 50px"></div>
                                    <div style="height: 380px; width: 100%">

                                        <asp:Chart ID="Chart1" runat="server" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)" Palette="BrightPastel" ImageType="Png"
                                            BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderlineWidth="2"
                                            BackColor="#D3DFF0" BorderlineColor="26,59,105" Height="350px" Width="1350px">

                                            <BorderSkin SkinStyle="Emboss" />

                                            <Legends>
                                                <asp:Legend Name="Legend1" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="false"></asp:Legend>
                                            </Legends>

                                            <Titles>
                                                <asp:Title Alignment="TopCenter" BackColor="180, 165, 191, 228" BackGradientStyle="TopBottom" BackHatchStyle="None"
                                                    Font="Microsoft Sans Serif, 12pt, style=Bold" Name="Revenue" Text="Projekter Ordre" ToolTip="Projekter Ordre" ForeColor="26, 59, 105">
                                                </asp:Title>

                                                <asp:Title Alignment="TopCenter" BackColor="Transparent" Font="Microsoft Sans Serif, 10pt, style=Bold" ToolTip="Chart Type"></asp:Title>
                                            </Titles>
                                            <Series>
                                                <asp:Series Name="Count" ChartArea="ChartArea1" Legend="Legend1" CustomProperties="DrawingStyle=Cylinder" BorderColor="64, 0, 0, 0"
                                                    Color="224, 64, 10" MarkerSize="5">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" BackColor="64, 165, 191, 228" BackSecondaryColor="White" BorderColor="64, 64, 64, 64" ShadowColor="Transparent"
                                                    BackGradientStyle="TopBottom">
                                                    <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="false" Title="Total Cost" ArrowStyle="Triangle" Minimum="1000" Maximum="50000" Interval="5000">
                                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                                    </AxisY>
                                                    <AxisX IsLabelAutoFit="false" LineColor="64, 64, 64, 64" Title="Month" ArrowStyle="Triangle" Interval="1">
                                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" IsEndLabelVisible="false" Angle="0" />
                                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                                    </AxisX>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                        </asp:Chart>

                                    </div>

                                </fieldset>

                                <fieldset style="height: 430px;">
                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>

                                    <div style="height: 50px"></div>
                                    <div class="BoxSize" style="height: 100px; width: 35%; font-size: xx-large; background-color: lightblue; float: left;">
                                        <div style="margin-top: 25px; margin-left: 10px; font-size: xx-large;">
                                            Order Total Cost:
                                            <asp:Label ID="lblProjectCost" Font-Size="XX-Large" Font-Underline="true" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="BoxSize" style="height: 100px; width: 35%; font-size: xx-large; background-color: lightblue; float: right; margin-right: 150px;">
                                        <div style="margin-top: 25px; margin-left: 10px; font-size: xx-large;">
                                            Salary Total Cost:<asp:Label ID="lblSalaryCost" Font-Size="XX-Large" Font-Underline="true" runat="server"></asp:Label>
                                        </div>
                                    </div>

                                </fieldset>
                            </div>

                            <div id="BottomButtonDiv" runat="server" class="shadowdiv" style="width: 100%; float: left; height: 50px; text-align: center; border: solid 0px black;" visible="false">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:Button CssClass="buttonn" ID="BtnUpdate" runat="server" Text="OPDATER " Width="101px" Style="background-color: #009900" ValidationGroup="User" OnClick="BtnUpdate_Click" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="pop1" class="parentDisable"></div>
                        <asp:GridView ID="gvUpdateProject" AutoGenerateDeleteButton="false" runat="server" Visible="false">
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
