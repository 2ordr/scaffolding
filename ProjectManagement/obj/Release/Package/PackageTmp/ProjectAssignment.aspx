﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectAssignment.aspx.cs" Inherits="ProjectManagement.ProjectAssignment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="DatePickerControl"
    Namespace="DatePickerControl" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>


    <style>
        .main-panel-with-title > .x-panel-header .x-panel-header-text-container-default {
            font-weight: normal;
            color: #383838;
            padding-left: 20px;
            font-size: 182%;
            line-height: inherit;
        }

        headform1 {
            color: #383838;
        }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .Calendar .ajax__calendar_container {
            border: 1px solid #E0E0E0;
            background-color: #FAFAFA;
            width: 200px;
        }

        .Calendar .ajax__calendar_header {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 12px;
            text-align: center;
            color: #9F9F9F;
            font-weight: normal;
            text-shadow: 0px 0px 2px #D3D3D3;
            height: 20px;
        }

        .Calendar .ajax__calendar_title,
        .Calendar .ajax__calendar_next,
        .Calendar .ajax__calendar_prev {
            color: #004080;
        }

        .Calendar .ajax__calendar_body {
            width: 175px;
            height: 150px;
            position: relative;
        }

        .Calendar .ajax__calendar_dayname {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            color: #FA9900;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            background-color: #EDEDED;
            border: solid 1px #D3D3D3;
            text-transform: uppercase;
            margin: 1px;
        }

        .Calendar .ajax__calendar_day {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            border: solid 1px #E0E0E0;
            text-transform: uppercase;
            margin: 1px;
            width: 17px !important;
            color: #9F9F9F;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_day,
        .Calendar .ajax__calendar_hover .ajax__calendar_month,
        .Calendar .ajax__calendar_hover .ajax__calendar_year,
        .Calendar .ajax__calendar_active {
            color: red;
            font-weight: bold;
            background-color: #ffffff;
        }

        .Calendar .ajax__calendar_year {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_month {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_today {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            text-transform: uppercase;
            margin: 1px;
            color: #6B6B6B;
        }

        .Calendar .ajax__calendar_other {
            background-color: #E0E0E0;
            margin: 1px;
            width: 17px;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_today,
        .Calendar .ajax__calendar_hover .ajax__calendar_title {
        }

        .Calendar .ajax__calendar_footer {
            width: 175px;
            border: none;
            height: 20px;
            vertical-align: middle;
            color: #6B6B6B;
        }

        img.PopupImg {
            vertical-align: middle;
            padding: 0px;
            margin: 0px;
            border: none;
        }

        .spanid {
            color: lightblue;
        }

            .spanid :hover {
                color: black;
                cursor: pointer;
            }

        .visoft__tab_xpie7 .ajax__tab_header {
            font-family: verdana,tahoma,helvetica;
            font-size: 11px;
        }

        .visoft__tab_xpie7 .ajax__tab_outer {
            height: 25px;
            border: 1px solid #999999;
            border-bottom: 0;
        }

        .visoft__tab_xpie7 .ajax__tab_inner {
            padding-left: 3px;
        }

        .visoft__tab_xpie7 .ajax__tab_tab {
            padding: 2px 10px;
            margin: 0;
            background-color: silver; /*#3476CC;*/
            color: white;
        }

        .visoft__tab_xpie7 .ajax__tab_body {
            /*font-family: verdana,tahoma,helvetica;*/
            font-size: 10pt;
            /*border: 1px solid #999999;*/
            border: 0.5px solid #999999;
            /*border-top: 0;*/
            padding: 10px;
            /*background-color: #ffffff;*/
        }

        .visoft__tab_xpie7 .ajax__tab_active {
            color: white;
            background-color: green; /*#7AD0ED;*/
        }

        .link-button {
            margin-top: 15px;
            max-width: 90px;
            /*background-color: aquamarine;
            /border-color: black;
            color: #333;*/
            display: inline-block;
            vertical-align: middle;
            text-align: center;
            text-decoration: none;
            align-items: flex-start;
            cursor: default;
            -webkit-appearence: push-button;
            border-style: solid;
            border-width: 1px;
            border-radius: 5px;
            font-size: 1em;
            font-family: inherit;
            border-color: floralwhite;
            padding-left: 5px;
            padding-right: 5px;
            width: 100%;
            min-height: 30px;
        }

            .link-button:hover {
                background-color: skyblue;
                border-color: black;
            }

        .simple {
            color: lightblue;
        }

            .simple:hover {
                color: blue;
            }
    </style>
    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $("#addAnother").click(function () {
                addAnotherRow();
            });
        });

        function addAnotherRow() {
            var row = $("#tbl tr").last().clone();
            var oldId = Number(row.attr('id').slice(-1));
            var id = 1 + oldId;


            row.attr('id', 'tasktr_' + id);
            row.find('#Chkselectt_' + oldId).attr('id', 'Chkselectt_' + id);
            row.find('#txttask_' + oldId).attr('id', 'txttask_' + id);
            //row.find('#lbl_' + oldId).attr('id', 'lbl_' + id);
            //row.find('#txtDate_' + oldId).attr('id', 'txtDate_' + id);
            //row.find('#CalendarExtender1_' + oldId).attr('id', 'CalendarExtender1_' + id);
            //row.find('#ChkBillable_' + oldId).attr('id', 'ChkBillable_' + id);
            //row.find('#txtPricePerHr_' + oldId).attr('id', 'txtPricePerHr_' + id);
            //row.find('#txtBudget_' + oldId).attr('id', 'txtBudget_' + id);
            //row.attr('id', 'tasktr_' + id);
            //row.attr('id', 'Chkselectt_' + id);
            //row.attr('id', 'txttask_' + id);  

            $('#tbl').append(row);
            //$('table #tbl tr#txttask_' + id).remove();          
            row.removeData();

        }
        //$(document).on('click', '#tbl tr#txttask_' + id + '.delete', function () {
        //    $(this).closest('tr#txttask_' + id).remove()
        //});

        function Add() {
            $("#tblData tbody").append("<tr>" +
             "<td><input type='text'/></td>" +
             "<td><input type='text'/></td>" +
             "<td><input type='text'/></td>" +
             "</tr>");
            //$(".btnSave").bind("click", Save);
            //$(".btnDelete").bind("click", Delete);
        };

        function Save() {
            var par = $(this).parent().parent();  //tr
            var tdName = par.children("td:nth-child(1)");
            var tdPhone = par.children("td:nth-child(2)");
            //var tdEmail = par.children("td:nth-child(3)");
            //var tdButtons = par.children("td:nth-child(4)");
            tdName.html(tdName.children("input[type=text]").val());
            tdPhone.html(tdPhone.children("input[type=text]").val());
            //tdEmail.html(tdEmail.children("input[type=text]").val());
            //tdButtons.html("<img src='images/delete.png' class='btnDelete'/><img src='images/pencil.png' class='btnEdit'/>");
            //$(".btnEdit").bind("click", Edit);
            //$(".btnDelete").bind("click", Delete);
        };


    </script>

    <script type="text/javascript" language="javascript">
        function deleteRow(tableID, currentRow) {
            try {
                var table = document.getElementById(tableID);
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    /*var chkbox = row.cells[0].childNodes[0];*/
                    /*if (null != chkbox && true == chkbox.checked)*/

                    if (row == currentRow.parentNode.parentNode) {
                        if (rowCount <= 1) {
                            alert("Cannot delete all the rows.");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            //getValues();
        }
    </script>
    <script type="text/javascript" language="javascript">
        function delRow() {

            var current = window.event.srcElement;
            //here we will delete the line
            while ((current = current.parentElement) && current.tagName != "TR");
            current.parentElement.removeChild(current);

        }
    </script>

    <script type="text/javascript">
        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>

    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>



</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server" enctype="multipart/form-data">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Debug"></asp:ToolkitScriptManager>

        <div style="height: 1000px; width: 1920px;">
            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 1000px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 1000px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox1" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150"></asp:TextBox>

                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 1000px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%-- <li class="menu-item menu-app-item app-item " id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>

                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%-- <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>
                                                --%>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div1')">Rapporter</span></span>

                                                    <div id="Div1" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div2')">Løn Modul</span></a>
                                                    <div id="Div2" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>
                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>


                                <div style="height: 300px; border-color: White;">
                                    <table style="height: 300px; width: 100%">
                                        <tr style="height: 100px;">
                                            <td></td>
                                        </tr>
                                        <tr style="height: 50px;">
                                            <td>
                                                <div>
                                                    &nbsp
                                                    <asp:Label ID="Label2" runat="server" Text="Timer" ForeColor="White"></asp:Label>
                                                </div>
                                                <div>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                                                        <ContentTemplate>
                                                            &nbsp

                                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="white">00:00:00</asp:Label>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                                        </Triggers>

                                                    </asp:UpdatePanel>

                                                    <asp:Button ID="Button2" runat="server" Text="Start" OnClick="Button2_Click" CssClass="button" Width="40" Height="30" BackColor="#009900" />
                                                    <asp:Button ID="Button3" runat="server" Text="stop" OnClick="Button3_Click" CssClass="button" Width="40" Height="30" BackColor="#009900" />

                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtHrs" runat="server" Height="30" Width="70"></asp:TextBox>
                                                    <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick"></asp:Timer>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 100px;">
                                            <td>
                                                <ul>
                                                    <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                        <asp:Label ID="Label1" runat="server" Text="" ForeColor="White"></asp:Label>
                                                    </span></a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 1000px; right: -37px;">

                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUpload" />
                    </Triggers>
                    <ContentTemplate>
                        <div class="allSides" style="width: 100%; height: 100px;">
                            <div class="divider"></div>
                            <asp:Label ID="Label3" runat="server" CssClass="clienthead" Text="Ny Projekter"></asp:Label>
                            <asp:Label ID="Label5" runat="server" Text="Label" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label><br />
                            <asp:Label ID="Label6" runat="server" Text="Label" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label>
                            <asp:Label ID="lblProid" runat="server" Text="" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label>
                            <%-- </div>
                            <div style="width: 1%; float: left; height: 800px; border: solid 0px black;">
                            </div>
                            <div style="height: 50px">--%>
                            <table>
                                <tr>

                                    <td style="height: 50px; width: 50px"></td>

                                    <td style="height: 50px; width: 130px">
                                        <asp:LinkButton CssClass="link-button" ID="LBtnBasicInfo" runat="server" Font-Underline="false" OnClick="LBtnBasicInfo_Click" ForeColor="RoyalBlue">Stamdata</asp:LinkButton>
                                    </td>

                                    <td style="height: 50px; width: 130px">
                                        <asp:LinkButton CssClass="link-button" ID="LBtnUserList" runat="server" Font-Underline="false" OnClick="LBtnUserList_Click" ForeColor="RoyalBlue">Bruger</asp:LinkButton>
                                    </td>

                                    <td style="height: 50px; width: 130px">
                                        <asp:LinkButton CssClass="link-button" ID="LBtnOrderList" runat="server" Font-Underline="false" OnClick="LBtnOrderList_Click" ForeColor="RoyalBlue">Ordre</asp:LinkButton>
                                    </td>

                                    <td style="height: 50px; width: 130px">
                                        <asp:LinkButton CssClass="link-button" ID="LBtnNewTask" runat="server" Font-Underline="false" OnClick="LBtnNewTask_Click" ForeColor="RoyalBlue">Ny opgave</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div style="width: 100%; float: left; left: 195px; height: 850px; font-family: Calibri;">

                            <div id="divBasicInfo" runat="server" style="width: 100%; float: left; left: 195px; height: 850px;">

                                <fieldset style="height: 840px;">

                                    <legend style="color: black; font-weight: bold; font-size: large">Stamdata:</legend>
                                    <table style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td style="height: 20px;" colspan="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 70%;">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td style="text-align: right;">Navn </td>
                                                            <td style="width: 40px"></td>
                                                            <td colspan="2">
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtProjectName" runat="server" Width="300px" Height="31px" Text="New Project"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="ReqtxtProjectName" runat="server" ErrorMessage="*" ControlToValidate="txtProjectName" ValidationGroup="Project"></asp:RequiredFieldValidator>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td style="height: 20px;" colspan="3"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right;">Kunder</td>
                                                            <td style="width: 40px"></td>
                                                            <td colspan="2">
                                                                <%--<asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmployeeName" runat="server" Width="300px" Height="31px" AutoPostBack="true" OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>--%>
                                                                <asp:Label ID="lblCustid" runat="server" Text="" Visible="false"></asp:Label>
                                                                <asp:DropDownList CssClass="search_categories" ID="ddlCustName" runat="server" Width="300px" Height="36px" Font-Size="10" AutoPostBack="true" DataTextField="CustName" DataValueField="CustId" OnSelectedIndexChanged="ddlCustName_SelectedIndexChanged"></asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 20px;" colspan="3"></td>
                                                        </tr>
                                                        <%-- <tr>
                                                    <td style="text-align: right;">Employee</td>
                                                    <td style="width: 40px"></td>
                                                    <td colspan="2">
                                                        
                                                        <asp:ListBox CssClass="twitterStyleTextbox" ID="LstBoxEmployee" runat="server" Width="300px" Height="36px" SelectionMode="Multiple" Rows="5"></asp:ListBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>--%>
                                                        <tr>
                                                            <td style="text-align: right;">Description </td>
                                                            <td style="width: 40px"></td>
                                                            <td colspan="2">
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDescript" runat="server" Width="300px" Height="50px"></asp:TextBox>
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtCompareDate" runat="server" Style="display: none;"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 20px;" colspan="3"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right;">Est.Start Date</td>
                                                            <td style="width: 40px"></td>
                                                            <td colspan="2">
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStartdt" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                <cc1:DatePicker ID="dtp1" runat="server" Width="300px" CssClass="twitterStyleTextbox" DateFormat="dd-MM-yyyy" Visible="false"></cc1:DatePicker>
                                                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtStartdt" CssClass="Calendar" Format="dd-MM-yyyy" Enabled="true"></asp:CalendarExtender>

                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom, Numbers" ValidChars="-/" TargetControlID="txtStartdt"></asp:FilteredTextBoxExtender>
                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Enter Valid Date" Font-Size="Small" Type="Date" Operator="GreaterThanEqual" ControlToValidate="txtStartdt" ControlToCompare="txtCompareDate" ValidationGroup="NextButtonCLick"></asp:CompareValidator>

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 20px;" colspan="3"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right;">Est.Completion Date</td>
                                                            <td style="width: 40px"></td>
                                                            <td colspan="2">
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtCompletionDate" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                <cc1:DatePicker ID="dtp2" runat="server" Width="300px" CssClass="twitterStyleTextbox" DateFormat="dd-MM-yyyy" Visible="false"></cc1:DatePicker>
                                                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtCompletionDate" CssClass="Calendar" Format="dd-MM-yyyy" Enabled="true"></asp:CalendarExtender>

                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom, Numbers" ValidChars="-/" TargetControlID="txtCompletionDate"></asp:FilteredTextBoxExtender>
                                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Enter Valid Date" Font-Size="Small" Type="Date" Operator="GreaterThanEqual" ControlToValidate="txtCompletionDate" ControlToCompare="txtStartdt" ValidationGroup="NextButtonCLick"></asp:CompareValidator>

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 20px;" colspan="3"></td>
                                                        </tr>

                                                    </table>
                                                </td>
                                                <%--<td style="width: 40%; vertical-align: top;"></td>--%>
                                            </tr>
                                        </tbody>

                                    </table>
                                </fieldset>
                            </div>

                            <div id="divUserList" runat="server" style="float: left; height: 850px; width: 100%;">

                                <fieldset id="FieldSetUserList" runat="server">
                                    <legend style="color: black; font-weight: bold; font-size: large">Bruger List:</legend>
                                    <div id="userid1" runat="server">
                                        <table>
                                            <tr>
                                                <td style="width: 110px;"></td>
                                                <td style="width: 90px; font-weight: 700">Id</td>
                                                <td style="width: 250px; font-weight: 700">Brugernavn</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="divEmptlist" runat="server" style="height: 250px; width: 100%; overflow: auto;">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridViewEmpname" runat="server" DataKeyNames="EmpId" ShowHeader="False" AutoGenerateColumns="False" Width="100%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" OnRowDataBound="GridViewEmpname_RowDataBound" OnSelectedIndexChanged="GridViewEmpname_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"></asp:CheckBox>
                                                                    <asp:Label ID="lbllEmpid" runat="server" Text='<%# Bind("EmpId") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                <ItemStyle BorderColor="White" BorderWidth="5px" Width="10px" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="EmpId" HeaderText="EmpId">
                                                                <ItemStyle BorderColor="White" BorderWidth="5px" Width="10px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FName" HeaderText="Employee Name">
                                                                <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                <ItemStyle BorderColor="White" BorderWidth="5px" Width="250px" />
                                                            </asp:BoundField>

                                                            <asp:CommandField ShowSelectButton="True" Visible="False" />
                                                        </Columns>
                                                        <HeaderStyle BackColor="#F2F2F2" BorderColor="White" BorderWidth="5px" />
                                                    </asp:GridView>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </fieldset>
                                <div style="height: 30px; width: 100%"></div>
                                <div>
                                    <asp:Button ID="Button1" CssClass="buttonn" runat="server" Text="ADD" OnClick="BtnAdd_Click" BackColor="#009933" Visible="False" />

                                    <asp:Label ID="lblTaskEmpId" runat="server" Text="" Visible="false"></asp:Label><%--Use for Task Adding In DropdownList--%>
                                </div>
                                <fieldset id="AssignedUserFieldSet" runat="server" style="height: 500px" visible="true">

                                    <legend style="color: black; font-weight: bold; font-size: large">Assigned Bruger</legend>
                                    <div id="divOverflowEmployee" runat="server" style="height: 500px; width: 100%; overflow: auto">
                                        <table style="width: 100%;">
                                            <tbody>

                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridViewSelEmployee" AutoGenerateColumns="false" Width="100%" runat="server" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px" OnRowDataBound="GridViewSelEmployee_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="CheckBox1" runat="server" Visible="false" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="2px" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="UserId" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEmpID" runat="server" Text='<%# Bind("EmpId") %>' Visible="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle Width="50px" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="User Name" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEmpNAME" runat="server" Text='<%# Bind("FName") %>' Visible="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="200px" />
                                                                    <ItemStyle Width="200px" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="WagePerHr" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtWagePerHr" runat="server" Height="31" Width="50" Text='<%# Bind("WagePerHr") %>'></asp:TextBox>
                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FEx1" Enabled="true" TargetControlID="txtWagePerHr" FilterMode="ValidChars" ValidChars="0123456789.,"></asp:FilteredTextBoxExtender>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate></EditItemTemplate>
                                                                    <HeaderStyle Width="30px" />
                                                                    <ItemStyle Width="30px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="MainTask" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <%-- <asp:Label ID="LbllTaskId" runat="server" Text='<%# Bind("TaskId") %>' Visible="false"></asp:Label>--%>
                                                                        <%--   <asp:DropDownList CssClass="search_categories" ID="ddlMainTask" runat="server" Height="36px"  OnSelectedIndexChanged="ddlMainTask_SelectedIndexChanged1" AutoPostBack="true"></asp:DropDownList>--%>
                                                                        <asp:DropDownList ID="ddlMainTask" CssClass="search_categories" runat="server" Height="36px" OnSelectedIndexChanged="ddlMainTask_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate></EditItemTemplate>
                                                                    <HeaderStyle Width="200px" />
                                                                    <ItemStyle Width="200px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubTask" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <%-- <asp:Label ID="LbllTaskId" runat="server" Text='<%# Bind("TaskId") %>' Visible="false"></asp:Label>--%>
                                                                        <asp:DropDownList CssClass="search_categories" ID="ddlSubTaskList" runat="server" Height="36px" DataTextField="SubTask" DataValueField="STID" Visible="false"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate></EditItemTemplate>
                                                                    <HeaderStyle Width="200px" />
                                                                    <ItemStyle Width="200px" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="EstimatedHrs" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEstiHrs" runat="server" Height="31" Width="50" Text='<%# Bind("EstimatedHours") %>'></asp:TextBox>

                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FEx2" Enabled="true" TargetControlID="txtEstiHrs" FilterMode="ValidChars" ValidChars="0123456789.,"></asp:FilteredTextBoxExtender>

                                                                    </ItemTemplate>
                                                                    <EditItemTemplate></EditItemTemplate>
                                                                    <HeaderStyle Width="30px" />
                                                                    <ItemStyle Width="30px" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 30px;"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </fieldset>

                            </div>

                            <div id="divOrderList" runat="server" style="width: 100%; float: left; height: 850px;">

                                <fieldset>
                                    <legend style="color: black; font-weight: bold; font-size: large"></legend>
                                    <div>
                                        <table>
                                            <tr>
                                                <td style="width: 45px;"></td>
                                                <td style="width: 130px; font-weight: 700"></td>
                                                <td style="width: 250px; font-weight: 700"></td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div id="ddorder" runat="server">
                                        <fieldset>
                                            <legend style="color: black; font-weight: bold; font-size: large"></legend>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <asp:DropDownList CssClass="search_categories" ID="ddOrderList" runat="server" Width="300px" Height="36px" Font-Size="Small" AutoPostBack="true" OnSelectedIndexChanged="ddOrderList_SelectedIndexChanged"></asp:DropDownList>
                                                    <asp:Label ID="LblOrder" runat="server" Text="" Visible="false"></asp:Label>
                                                    <asp:Label ID="LabelID" runat="server" Text="" Visible="false"></asp:Label>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>

                                    <fieldset style="height: 790px">
                                        <legend style="color: black; font-weight: bold; font-size: large"></legend>

                                        <div runat="server" style="height: 790px; width: 100%;">

                                            <div id="DivOrderList2" runat="server" style="height: 790px; width: 50%; overflow: auto; float: left;">
                                                <fieldset style="height: 790px">
                                                    <legend>ORDRE:</legend>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GrdViewOrderDetails" runat="server" ShowFooter="true" HeaderStyle-Height="30" AutoGenerateColumns="false" Width="100%" Font-Size="Medium" OnRowCommand="GrdViewOrderDetails_RowCommand">
                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="Id">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblOrderID" runat="server" Text='<%# Bind("OrderId") %>' Visible="false"></asp:Label>
                                                                                <asp:LinkButton ID="LnkBtnOrderId" runat="server" Text='<%# Bind("OrderId") %>' Font-Underline="false" OnClick="LnkBtnOrderId_Click"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Dato">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblOrderDate" runat="server" Text='<%# Bind("OrderDate") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Total">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTotalOrder" runat="server" Text='<%# Bind("OrderTotal") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton CssClass="simple" ID="LinkDeleteOrder" runat="server" Font-Underline="false" CommandArgument='<%#Eval("OrderId") %>' CommandName="DeleteRow"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </div>

                                            <div id="divOrderlist1" runat="server" style="height: 790px; width: 49%; overflow: auto; float: right;">
                                                <fieldset style="height: 790px">
                                                    <legend>ORDRE DETAILS:</legend>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GridViewEditOrder" runat="server" ShowFooter="True" AutoGenerateColumns="False" Width="100%" Style="font-size: medium" OnRowDataBound="Bound">

                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Item Code">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ItemName">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemName" runat="server" Text='<%# Bind("ItemName") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Price">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemPrice" runat="server" Text='<%# Bind("Price") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Quantity">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemQuantity" runat="server" Text='<%# Bind("Quantity") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Amount">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblItemtotal" runat="server" Text='<%# Bind("TotalPrice") %>' Visible="true"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle BorderColor="White" BorderWidth="5px" />
                                                                            <HeaderStyle BorderColor="White" BorderWidth="5px" />
                                                                            <ItemStyle CssClass="Itemalign" BorderColor="White" BorderWidth="5px" />
                                                                            <FooterTemplate>
                                                                                <asp:Label ID="lblTot" runat="server" Text="" Visible="true"></asp:Label>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <FooterStyle BorderColor="White" BorderWidth="5px" />

                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div style="height: 30px; width: 100%"></div>
                                    </fieldset>
                                </fieldset>
                            </div>

                            <div runat="server" id="divTaskList" style="width: 99%; float: left; height: 850px; font-family: Calibri;" visible="false">
                                <fieldset style="color: black;">
                                    <legend></legend>
                                    <table style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td style="width: 6%">
                                                    <%-- <span runat="server" id="addAnother" class="add-another">+ Add NewTask</span>--%>
                                                    <asp:Button CssClass="buttonn" ID="AddTask" runat="server" Text="+ Tilføj Nyopgave" OnClick="AddTask_Click" />
                                                </td>
                                                <%--<asp:TextBox CssClass="twitterStyleTextbox" ID="txtExpectedHrs" runat="server" Height="30" Width="40" Text="00:00"></asp:TextBox>(Expected Hours) --%>

                                                <td style="width: 4.2%">Hours
                                                  <asp:DropDownList ID="ddlHours" runat="server" CssClass="search_categories" Height="35">
                                                  </asp:DropDownList>
                                                </td>
                                                <td style="width: 2.6%">
                                                    <asp:DropDownList ID="ddlMinutes" runat="server" CssClass="search_categories" Height="35">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 3%">
                                                    <asp:DropDownList ID="ddlSeconds" runat="server" CssClass="search_categories" Height="35">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 40%">

                                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                                    <asp:Button CssClass="buttonn" ID="btnUpload" runat="server" Text="Vedhæft" OnClick="btnUpload_Click" />
                                                </td>

                                                <td style="width: 10%; text-align: right">
                                                    <asp:Button CssClass="buttonn" ID="SaveTask" runat="server" Text="Gem" OnClick="SaveTask_Click" />
                                                    <asp:Button CssClass="buttonn" ID="btnVIewTask" runat="server" Text="Vis" OnClick="btnVIewTask_Click" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table id="tblData" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th style="width: 275px;">Opgavenr:
                                                  <asp:Label ID="lbltaskid" runat="server"></asp:Label></th>
                                                <th style="width: 57px;"></th>
                                                <th>Opgave:
                                                  <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMTask" Text="" runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </fieldset>

                                <div id="divOverflow" runat="server" style="width: 100%; height: 200px; font-family: Calibri; overflow: auto;">
                                    <fieldset style="color: black;">
                                        <legend></legend>
                                        <table id="tbltask" runat="server" style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gridTask" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px" OnRowDataBound="gridTask_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SR.No" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltaskid" runat="server" Text='<%# Bind("TaskId") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblSubtask" runat="server" Text='<%# Bind("Subtask") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SubTask" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <span class="title">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTask" Text='<%# Bind("TaskName") %>' runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>
                                                                        <%-- <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTask1" runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                            <%-- <asp:TemplateField HeaderText="Expected Hours" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtExpectedHrs" runat="server" Height="30" Width="40" Text="00:00"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                </asp:TemplateField>--%>

                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList CssClass="search_categories" ID="DDLSkillSet" runat="server" Height="35" Width="200"></asp:DropDownList>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="200px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <span class="actions">
                                                                        <span class="spanid" title="Task Info"><span class="action-icon icon-eye"></span></span>
                                                                        <span class="spanid" title=" Task Note"><span class="action-icon icon-note"></span></span>
                                                                        <span class="spanid" title="Due Date"><span class="action-icon icon-credit-card "></span></span>
                                                                        <%--   <span class="spanid" title="Delete Task"><a class="pagerLink" runat="server" onclick=" delRow()">
                                                                                    <span class="action-icon icon-trashcan"></span></a></span>--%>
                                                                        <asp:LinkButton ID="lnkDeletetask" CssClass="pagerLink" runat="server" OnClientClick="delRow()" OnClick="lnkDeletetask_Click">
                                                                                    <span class="action-icon icon-trashcan"></span></asp:LinkButton></span>
                                                                    <span class="spanid" title="Add file"><span runat="server" class="action-icon icon-attach spanid"></span>
                                                                    </span></span>
                                                                            </span>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                <ItemTemplate>
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStatus" runat="server" Text="Open" Height="31" Enabled="false" Width="100"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="tbl" runat="server" style="width: 100%;" tabindex="0" visible="false">
                                            <tbody>
                                                <tr id="tasktr_0" runat="server">
                                                    <td style="width: 20px">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="TaskId" runat="server" Height="30" Width="30"></asp:TextBox>
                                                        <asp:CheckBox ID="Chkselectt_0" runat="server" Visible="false" />
                                                        <asp:Label ID="TextId" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                    <td style="width: 800px; height: 40px">
                                                        <span class="title">
                                                            <%--<div style="display: block;" class="editable description dataIndex-description" contenteditable="true" spellcheck="false" data-medium-editor-element="true" role="textbox" aria-multiline="true" data-medium-focused="true"></div>--%>
                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txttask_0" runat="server" Height="31" Visible="true" Width="600"></asp:TextBox>

                                                            <span class="actions">
                                                                <span class="spanid" title="Task Info"><span class="action-icon icon-eye"></span></span>
                                                                <span class="spanid" title=" Task Note"><span class="action-icon icon-note"></span></span>
                                                                <span class="spanid" title="Due Date"><span class="action-icon icon-credit-card "></span></span>
                                                                <span class="spanid" title="Delete Task"><a class="pagerLink" runat="server" onclick=" delRow()"><span class="action-icon icon-trashcan"></span></a></span>
                                                                <span class="spanid" title="Add file"><%--<a runat="server" class="pagerLink" href="javascript:void(0)" onclick="OpenFileUpload();">--%><span runat="server" class="action-icon icon-attach spanid"></span><%--</a>--%>
                                                                    <%-- <iframe class="hide" id="frameUpload" name="frameUpload" src="FileUpload.aspx" width="0" height="0" runat="server" />--%>
                                                                </span></span>
                                                        </span>
                                                    </td>
                                                    <td runat="server" style="height: 20px; width: 250px">
                                                        <div>
                                                            <asp:DropDownList CssClass="search_categories" ID="DDLSkillSet" runat="server" Height="35" Width="200"></asp:DropDownList>
                                                        </div>
                                                    </td>
                                                    <td runat="server" style="height: 20px;">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTimeDate" runat="server" Height="31" Width="150"></asp:TextBox>
                                                        <asp:CalendarExtender ID="CalenderExtender1" CssClass="Calendar" Enabled="true" runat="server" TargetControlID="txtTimeDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                    </td>
                                                    <td runat="server" style="height: 20px; text-align: right">
                                                        <div>
                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStatus" runat="server" Text="Open" Height="31" Enabled="false" Width="100"></asp:TextBox>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </fieldset>
                                </div>

                                <fieldset id="Idfiledetails" runat="server" visible="false">
                                    <legend>Oplysninger:</legend>
                                    <div style="height: 50px; width: 100%">
                                        <table style="height: 50px; width: 100%">
                                            <tr>
                                                <td style="width: 180px; font-weight: 700">FilNavn</td>
                                                <%--   <td style="width: 190px; font-weight: 700">FilePath</td>--%>
                                                <td style="width: 100px; font-weight: 700">View</td>

                                            </tr>
                                        </table>
                                    </div>
                                    <div id="GridVOverflow" runat="server" style="width: 100%; height: 200px; font-family: Calibri; overflow: auto;">

                                        <asp:GridView ID="gvFileUpload" runat="server" ShowHeader="false" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px">
                                            <Columns>

                                                <asp:BoundField DataField="TaskId" HeaderText="TaskId" Visible="false" />
                                                <asp:BoundField DataField="FileName" HeaderText="FileName" ItemStyle-Width="90px" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                <%--     <asp:BoundField DataField="FilePath" HeaderText="FilePath" ItemStyle-Width="100" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />--%>
                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFilePath" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnViewImage" runat="server" Font-Underline="false" OnClick="lbtnViewImage_Click"><span class="action-icon icon-eye"></span></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </fieldset>


                                <fieldset id="IdTaskDetails" runat="server" visible="false">
                                    <legend>Opgave detaljer:</legend>
                                    <div id="divTaskView" runat="server" style="width: 100%; height: 300px; font-family: Calibri;">

                                        <div style="height: 50px; width: 100%">
                                            <table style="height: 50px; width: 100%;">
                                                <tr>
                                                    <td style="width: 05%; font-weight: 700">SrNo.</td>
                                                    <td style="width: 40%; font-weight: 700">Sub Task</td>
                                                    <td style="width: 35%; font-weight: 700">SkillSet</td>
                                                    <td style="width: 20%; font-weight: 700">ExpectedHours</td>
                                                    <%--  <td style="width: 200px; font-weight: 700">FileName</td>--%>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="divTaskView1" runat="server" style="width: 100%; height: 250px; font-family: Calibri; overflow: auto;">

                                            <asp:GridView ID="gvTaskView" runat="server" ShowHeader="false" AutoGenerateColumns="false" Width="100%">

                                                <Columns>

                                                    <asp:TemplateField HeaderText="SRNo." HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTaskId" runat="server" Text='<%# Bind("TaskId") %>' Visible="False"></asp:Label>
                                                            <asp:Label ID="lblSTID" runat="server" Text='<%# Bind("STID") %>' Visible="False"></asp:Label>
                                                            <asp:Label ID="lblID" runat="server" Text='<%#Container.DataItemIndex + 1%>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="05%" />

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Task" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                        <ItemTemplate>
                                                            <%--   <asp:Label ID="lblTask" runat="server" Text='<%# Bind("Task") %>' Visible="true"></asp:Label>--%>
                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Bind("SubTask") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="40%" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="SkillSet" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSkillSet" runat="server" Text='<%# Bind("SkillSet") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="35%" />

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ExpectedHours" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblExpectedHours" runat="server" Text='<%# Bind("ExpectedHours") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20%" />

                                                    </asp:TemplateField>

                                                    <%--<asp:TemplateField HeaderText="FileName" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Bind("FileName") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="200" />
                                            <HeaderStyle Width="200" />
                                        </asp:TemplateField>--%>
                                                </Columns>

                                            </asp:GridView>

                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="shadowdiv" style="width: 100%; float: left; height: 50px; text-align: center; border: solid 0px black;">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:Button CssClass="buttonn" ID="BtnAdd" runat="server" Text="GEM" Width="101px" Style="background-color: #009900" OnClick="BtnAssign_Click" ValidationGroup="Project" />

                                            </td>
                                            <td>
                                                <asp:Button CssClass="buttonn" ID="BtnView" runat="server" Text="View" Width="120px" Style="background-color: #009933" OnClick="BtnView_Click" Visible="false" />

                                            </td>
                                            <td>
                                                <asp:Button CssClass="buttonn" ID="BtnUpdate" runat="server" Text="Update" Width="120px" Style="background-color: #009933" OnClick="BtnUpdate_Click" Visible="false" />
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <asp:GridView ID="gvUpdateProject" AutoGenerateDeleteButton="false" runat="server" Visible="false"></asp:GridView>
                            <%--Not Use  --%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </form>
</body>

</html>
