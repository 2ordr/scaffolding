﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Popup.aspx.cs" Inherits="ProjectManagement.Popup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <style>
        .gridv {
            display: none;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Debug"></asp:ToolkitScriptManager>

        <div style="height: 850px; width: 100%">

            <div id="divContact" class="allSides" runat="server" style="height: 850px; border-color: black;">

                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td style="height: 20px;" colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="width: 60%">
                                <asp:Label ID="LblNewCon" runat="server" CssClass="clienthead" Text="Add New Contact"></asp:Label>
                                <asp:Label ID="LblContId" runat="server" Text=""></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 100%">

                                <table style="width: 100%">
                                    <tr>
                                        <td style="height: 20px" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%; text-align: center;">Name</td>
                                        <td style="width: 25%;">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtName" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 50%;">
                                            <asp:Label ID="LblCustId" runat="server" Text="" Visible="false"></asp:Label>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center;">Email</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmail" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please Enter Valid Email ID" ControlToValidate="txtEmail" CssClass="requiredFieldValidateStyle" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="customer">
                                            </asp:RegularExpressionValidator>
                                            <asp:FilteredTextBoxExtender runat="server" ID="FEx1" Enabled="true" TargetControlID="txtEmail" FilterMode="ValidChars" FilterType="Custom, Numbers, LowercaseLetters" ValidChars=".@"></asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center;">Address</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtAddress" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center;">City</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtcity" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtcity" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center;">Country</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtCountry" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtCountry" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer"></asp:RegularExpressionValidator>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center;">ZipCode</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtZipCode" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtZipCode" ValidationExpression="\d+" ValidationGroup="customer"></asp:RegularExpressionValidator>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center;">Mobile</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMobile" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender runat="server" ID="FEx2" Enabled="true" FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txtMobile"></asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center;">Phone</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtphone" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtphone" ValidationExpression="\d+" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                            <asp:FilteredTextBoxExtender runat="server" ID="FEx3" Enabled="true" TargetControlID="txtphone" FilterMode="ValidChars" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center;">Remark</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtRemark" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: center">Province</td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtProvince" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 0px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="text-align: right">
                                            <asp:Button CssClass="buttonn" ID="BtnSaveContact" runat="server" Text="Save Contact" OnClick="BtnSaveContact_Click" BackColor="#009933" /></td>
                                        <td colspan="2">
                                            <asp:Button CssClass="buttonn" ID="BtnUpdate" runat="server" Text="Update" OnClick="BtnUpdate_Click" BackColor="#009933" ValidationGroup="customer" /></td>
                                        <td>
                                            <asp:GridView CssClass="gridv" ID="GridViewPopup" AutoGenerateColumns="false" Visible="false" runat="server"></asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                    </tbody>
                </table>


            </div>

        </div>
    </form>
</body>
</html>
