﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Event Calender.aspx.cs" Inherits="ProjectManagement.Event_Calender" %>

<%@ Register TagPrefix="ECalendar" Namespace="ExtendedControls" Assembly="EventCalendar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;<asp:GridView ID="gvSelectedDateEvents" runat="server" Width="100%">
            </asp:GridView>
            <ECalendar:EventCalendar ID="Calendar1" runat="server" BackColor="White" BorderColor="Silver"
                BorderWidth="1px" Font-Names="Verdana"
                Font-Size="9pt" ForeColor="Black" Height="500px"
                Width="100%" FirstDayOfWeek="Monday" NextMonthText="Next &gt;" PrevMonthText="&lt; Prev" SelectionMode="DayWeekMonth" ShowGridLines="True" NextPrevFormat="FullMonth"
                ShowDescriptionAsToolTip="True" BorderStyle="Solid" EventDateColumnName="" EventDescriptionColumnName="" EventHeaderColumnName="" OnSelectionChanged="Calendar1_SelectionChanged" OnDayRender="Calendar1_DayRender">

                <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                <TodayDayStyle BackColor="#CCCCCC" />
                <SelectorStyle BorderColor="#404040" BorderStyle="Solid" />
                <DayStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="True" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <NextPrevStyle Font-Size="8pt" ForeColor="#333333" Font-Bold="True" VerticalAlign="Bottom" />
                <DayHeaderStyle BorderWidth="1px" Font-Bold="True" Font-Size="8pt" />
                <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True"
                    Font-Size="12pt" ForeColor="#333399" HorizontalAlign="Center" VerticalAlign="Middle" />
            </ECalendar:EventCalendar>
        </div>

        <div>
            <table>
                <tr>
                    <td>EventStartDate</td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>EventEndDate</td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>EventDescription</td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>EventHeader</td>
                    <td>
                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>EventBackColor</td>
                    <td>
                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>EventForeColor</td>
                    <td>
                        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnaddEvent" runat="server" Text="ADD" OnClick="btnaddEvent_Click"  /></td>
                    <td>
                        <asp:Button ID="btnViewEvent" runat="server" Text="VIEWS" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
