﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalenderEV.aspx.cs" Inherits="ProjectManagement.CalenderEV" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="css/cupertino/jquery-ui-1.10.3.min.css" rel="stylesheet" type="text/css" />
    <link href="fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.qtip-2.2.0.css" rel="stylesheet" type="text/css" />


    <script src="jquery/moment-2.8.1.min.js" type="text/javascript"></script>
    <script src="jquery/jquery-2.1.1.js" type="text/javascript"></script>
    <script src="jquery/jquery-ui-1.11.1.js" type="text/javascript"></script>
    <script src="jquery/jquery.qtip-2.2.0.js" type="text/javascript"></script>
    <script src="fullcalendar/fullcalendar-2.0.3.js" type="text/javascript"></script>
    <script src="script/calendarscript.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="Styles/style.css" />


    <%--<script src="jquery/jquery-ui-1.11.1.min.js" type="text/javascript"></script>
    <script src="jquery/jquery-2.1.1.min.map" type="text/javascript"></script>--%>

    <style type='text/css'>
        body {
            margin-top: 40px;
            text-align: center;
            font-size: 14px;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        }

        #calendar {
            width: 900px;
            margin: 0 auto;
        }
        /* css for timepicker */
        .ui-timepicker-div dl {
            text-align: left;
        }

            .ui-timepicker-div dl dt {
                height: 25px;
            }

            .ui-timepicker-div dl dd {
                margin: -25px 0 10px 65px;
            }

        .style1 {
            width: 100%;
        }

        /* table fields alignment*/
        .alignRight {
            text-align: right;
            padding-right: 10px;
            padding-bottom: 10px;
        }

        .alignLeft {
            text-align: left;
            padding-bottom: 10px;
        }

        .td1 {
            text-align: left;
        }
    </style>


</head>
<body>
    <form id="form1" runat="server">
        <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" ScriptMode="Debug">--%>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Debug" EnablePageMethods="true">
            <Scripts>
                <asp:ScriptReference Path="~/script/calendarscript.js" />
            </Scripts>
        </asp:ToolkitScriptManager>
        <%-- </asp:ScriptManager>--%>
        <div style="width: 100%; height: 100%;">



            <div style="width: 100%">
                <div id="calendar">
                </div>

                <div id="updatedialog" style="font: 70% 'Trebuchet MS', sans-serif; margin: 50px; display: none;" title="Update or Delete Event">

                    <table cellpadding="0" class="style1">
                        <tr>
                            <td class="alignRight">Name:</td>
                            <td class="alignLeft">
                                <input id="eventName" type="text" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="alignRight">Description:</td>
                            <td class="alignLeft">
                                <textarea id="eventDesc" cols="30" rows="3"></textarea></td>
                        </tr>
                        <tr>
                            <td class="alignRight">Start:</td>
                            <td class="alignLeft">
                                <span id="eventStart"></span></td>
                        </tr>
                        <tr>
                            <td class="alignRight">End: </td>
                            <td class="alignLeft">
                                <span id="eventEnd"></span>
                                <input type="hidden" id="eventId" /></td>
                        </tr>
                    </table>
                </div>
                <div id="addDialog" style="font: 70% 'Trebuchet MS', sans-serif; margin: 50px;" title="Add Event">
                    <table cellpadding="0" class="style1">
                        <tr>
                            <td class="alignRight">Name:</td>
                            <td class="alignLeft">
                                <input id="addEventName" type="text" size="20" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="alignRight">Description:</td>
                            <td class="alignLeft">
                                <textarea id="addEventDesc" cols="30" rows="3"></textarea></td>
                        </tr>
                        <tr>
                            <td class="alignRight">Start:</td>
                            <td class="alignLeft">
                                <span id="addEventStartDate"></span></td>
                        </tr>
                        <tr>
                            <td class="alignRight">End:</td>
                            <td class="alignLeft">
                                <span id="addEventEndDate"></span></td>
                        </tr>
                    </table>

                </div>
                <div runat="server" id="jsonDiv" />
                <input type="hidden" id="hdClient" runat="server" />
            </div>

            <div style="width: 100%; height: 220px;">

                <table style="width: 100%; height: 100%; border-color: red;" title="Add Event">
                    <tr>
                        <td style="width: 26%;"></td>
                        <td style="width: 48%;">
                            <fieldset>
                                <legend style="font-size: large; font-weight: 400;">Add Event:</legend>
                                <table>
                                    <tr>
                                        <td class="td1">Event Title:</td>
                                        <td>
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtTitle" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="td1">EventDescription</td>
                                        <td>
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtDescription" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="td1">EventStartDate</td>
                                        <td>
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtSDate" runat="server"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtSDate" Enabled="true" Format="dd-MM-yyyy HH:mm:ss"></asp:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td1">EventEndDate</td>
                                        <td>
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtEDate" runat="server"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TxtEDate" Enabled="true" Format="dd-MM-yyyy HH:mm:ss"></asp:CalendarExtender>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td1">All_Day</td>
                                        <td>
                                            <%-- <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtAllDay" runat="server"></asp:TextBox>--%>
                                            <asp:DropDownList runat="server" ID="DDList" CssClass="search_categories" Width="188">
                                                <asp:ListItem>-Select-</asp:ListItem>
                                                <asp:ListItem>False</asp:ListItem>
                                                <asp:ListItem>True</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button CssClass="buttonn" ID="btnaddEvent" runat="server" Text="ADD" OnClick="btnaddEvent_Click" /></td>
                                        <td class="td1">
                                            <asp:Button CssClass="buttonn" ID="btnViewEvent" runat="server" Text="VIEWS" Visible="false" /></td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td style="width: 26%;"></td>
                    </tr>
                </table>

            </div>

        </div>
    </form>
</body>
</html>
