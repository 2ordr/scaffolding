﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.UI.DataVisualization.Charting;

namespace ProjectManagement
{
    public partial class ProjectAssignmentNew : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            // DatePickerControl.DatePicker stdtp;
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    txtSearchBox.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    BindData();
                    //BindOrderList();
                    PopulateHours();
                    PopulateMinutes();
                    PopulateSeconds();

                    BindSkillset();
                    BindCustomerList();

                    String var = Request.QueryString["ProjectID"];
                    if (var != "")
                    {
                        divOverView.Visible = true;
                        divBasicInfo.Visible = false;
                        divContactInfo.Visible = false;
                        divOrder.Visible = false;
                        NewTask.Visible = false;

                        //BindData();
                        //LBtnOverview.ForeColor = System.Drawing.Color.Red;
                        LblProjectId.Text = var;

                        SqlConnection con2 = new SqlConnection(cs);
                        con2.Open();
                        SqlCommand cmd2 = new SqlCommand();
                        cmd2.Connection = con2;

                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd2.CommandText = "sp_ViewProject";

                        cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.NVarChar).Value = LblProjectId.Text; //Request.QueryString["Projectid"];
                        SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
                        DataSet dt2 = new DataSet();
                        adpt2.Fill(dt2);

                        if (dt2.Tables[0].Rows.Count > 0)
                        {
                            //BottomButtonDiv.Visible = true;

                            gvUpdateProject.DataSource = dt2;
                            gvUpdateProject.DataBind();

                            lblCustid.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                            txtProjectName.Text = dt2.Tables[0].Rows[0]["ProjectName"].ToString();
                            txtDescript.Text = dt2.Tables[0].Rows[0]["Description"].ToString();
                            txtStartdt.Text = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(dt2.Tables[0].Rows[0]["ProjectDate"].ToString()));
                            txtCompareDate.Text = txtStartdt.Text;
                            txtCompletionDate.Text = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(dt2.Tables[0].Rows[0]["CompletionDate"].ToString()));

                            ddlCustName.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                            //ddOrderList.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();
                            //LblOrder.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();
                            LblProjectName.Text = txtProjectName.Text;
                            dtp3.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["ProjectDate"].ToString());
                            dtp4.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["CompletionDate"].ToString());
                            Global.Editflag = true;

                            // for Project name color changing-----------------
                            foreach (GridViewRow rows in GridViewCustomer.Rows)
                            {
                                if (rows.RowType == DataControlRowType.DataRow)
                                {
                                    System.Web.UI.WebControls.LinkButton LBtnProjectName = (rows.Cells[1].FindControl("LBtnProjectName") as System.Web.UI.WebControls.LinkButton);

                                    if (LblProjectName.Text == LBtnProjectName.Text)
                                    {
                                        LBtnProjectName.ForeColor = System.Drawing.Color.Red;
                                    }
                                    else
                                    {
                                        LBtnProjectName.ForeColor = System.Drawing.Color.RoyalBlue;
                                    }
                                }
                            }

                            GridViewSelEmployee.DataSource = dt2;
                            GridViewSelEmployee.DataBind();

                            //int i = 0;
                            int count = dt2.Tables[0].Rows.Count - 1;

                            foreach (GridViewRow rw in GridViewSelEmployee.Rows)
                            {
                                if (rw.RowType == DataControlRowType.DataRow)
                                {
                                    foreach (GridViewRow rows in GridViewEmpname.Rows)
                                    {
                                        if (rows.RowType == DataControlRowType.DataRow)
                                        {
                                            System.Web.UI.WebControls.Label lbllEmpid = (rows.Cells[0].FindControl("lbllEmpid") as System.Web.UI.WebControls.Label);
                                            CheckBox chkselect = (rows.Cells[0].FindControl("chkSelect") as CheckBox);
                                            Label lblEmpID = (rw.Cells[1].FindControl("lblEmpID") as Label);
                                            DropDownList ddlTaskList = (rw.Cells[4].FindControl("ddlTaskList") as DropDownList);

                                            if (lbllEmpid.Text == lblEmpID.Text)
                                            {
                                                chkselect.Checked = true;
                                            }
                                        }

                                    }
                                }
                            }
                            BOrderList();
                            Chart();
                        }
                    }

                    int taskid;
                    int subtskid;

                    SqlConnection con11 = new SqlConnection(cs);
                    SqlCommand cmd11 = new SqlCommand();
                    cmd11.Connection = con11;
                    con11.Open();
                    cmd11.CommandType = System.Data.CommandType.Text;
                    cmd11.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
                    taskid = Convert.ToInt32(cmd11.ExecuteScalar()) + 1;
                    cmd11.Parameters.Clear();
                    cmd11.CommandText = "Delete FROM TEMPTASK";
                    cmd11.ExecuteNonQuery();

                    cmd11.Parameters.Clear();
                    cmd11.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
                    subtskid = Convert.ToInt32(cmd11.ExecuteScalar());
                    cmd11.Parameters.Clear();

                    cmd11.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd11.CommandText = "sp_InsertTempTask";
                    cmd11.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
                    cmd11.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
                    cmd11.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
                    cmd11.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
                    cmd11.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
                    cmd11.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

                    cmd11.ExecuteNonQuery();
                    cmd11.Parameters.Clear();

                    cmd11.CommandType = System.Data.CommandType.Text;
                    cmd11.CommandText = "SELECT * FROM TEMPTASK";
                    SqlDataAdapter dtask = new SqlDataAdapter(cmd11);
                    DataTable dttask = new DataTable();
                    dtask.Fill(dttask);
                    gridTask.DataSource = dttask;
                    gridTask.DataBind();

                    con11.Close();

                    lbltaskid.Text = Convert.ToInt32(taskid).ToString();
                    TaskId.Text = lbltaskid.Text;
                }
            }
        }

        private void PopulateHours()
        {
            for (int i = 0; i <= 24; i++)
            {
                ddlHours.Items.Add(i.ToString("D2"));
            }
        }

        private void PopulateMinutes()
        {
            for (int i = 0; i < 60; i++)
            {
                ddlMinutes.Items.Add(i.ToString("D2"));
            }
        }

        private void PopulateSeconds()
        {
            for (int i = 0; i < 60; i++)
            {
                ddlSeconds.Items.Add(i.ToString("D2"));
            }
        }

        public void BindSkillset()
        {
            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();
            com.Connection = con1;

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "Select Distinct EmpId,SkillSets from EMPLOYEE Where SkillSets!='" + null + "'"; // Change Code//
            SqlDataAdapter adp1 = new SqlDataAdapter(com);
            DataSet dtt1 = new DataSet();
            adp1.Fill(dtt1);
            DDLSkillSet.DataSource = dtt1;
            DDLSkillSet.DataTextField = "SkillSets";
            DDLSkillSet.DataValueField = "EmpId";
            DDLSkillSet.DataBind();
            DDLSkillSet.Items.Insert(0, new ListItem("-Vælg SkillSet-", "0"));
            con1.Close();
        }

        public void BindOrderList()
        {
            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();
            com.Connection = con1;

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select * from ORDERS";
            SqlDataAdapter adp1 = new SqlDataAdapter(com);
            DataSet dtt1 = new DataSet();
            adp1.Fill(dtt1);
            ddOrderList.DataSource = dtt1;
            ddOrderList.DataTextField = "OrderName";
            ddOrderList.DataValueField = "OrderId";
            ddOrderList.DataBind();
            ddOrderList.Items.Insert(0, new ListItem("-Vælg Ordre-", "0"));
            con1.Close();
        }

        public void BindData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from PROJECTS ORDER BY ProjectDate", con);
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                GridViewCustomer.DataSource = dt;
                GridViewCustomer.DataBind();
            }
            else
            {
                GridViewCustomer.DataSource = dt;
                GridViewCustomer.DataBind();
            }

            com.Parameters.Clear();
            com.CommandText = "select * from EMPLOYEE";
            SqlDataAdapter adp = new SqlDataAdapter(com);
            DataTable dtt = new DataTable();
            adp.Fill(dtt);
            if (dtt.Rows.Count > 0)
            {
                GridViewEmpname.DataSource = dtt;
                GridViewEmpname.DataBind();
            }
            else
            {
                GridViewEmpname.DataSource = dtt;
                GridViewEmpname.DataBind();
            }
            con.Close();
        }

        public void BindCustomerList()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select * from CUSTOMER";
            SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            DataSet dt1 = new DataSet();
            adpt1.Fill(dt1);
            if (dt1.Tables[0].Rows.Count > 0)
            {
                ddlCustName.DataSource = dt1;
                ddlCustName.DataTextField = "CustName";
                ddlCustName.DataValueField = "CustId";
                ddlCustName.DataBind();
                ddlCustName.Items.Insert(0, new ListItem("-Vælg-", "0"));
            }
        }

        protected void LBtnUserList_Click(object sender, EventArgs e)
        {
            if (LblProjectId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Project Name');", true);
                return;
            }

            else
            {
                LBtnUserList.BackColor = System.Drawing.Color.SkyBlue;
                LBtnUserList.BorderColor = System.Drawing.Color.Black;
                LBtnReport.BackColor = LBtnNewTask.BackColor = LBtnOrderList.BackColor = LBtnOverview.BackColor = LBtnBasicInfo.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnReport.BorderColor = LBtnNewTask.BorderColor = LBtnOrderList.BorderColor = LBtnOverview.BorderColor = LBtnBasicInfo.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnUserList.ForeColor = System.Drawing.Color.Red;
                LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnReport.ForeColor = System.Drawing.Color.RoyalBlue;

                divContactInfo.Visible = true;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divOrder.Visible = false;
                NewTask.Visible = false;
                BottomButtonDiv.Visible = true;
                divAPV.Visible = false;
                divReport.Visible = false;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            }
        }

        public void BOrderList()
        {
            SqlConnection con12 = new SqlConnection(cs);
            SqlCommand cmd12 = new SqlCommand();
            cmd12.Connection = con12;
            con12.Open();

            cmd12.CommandType = System.Data.CommandType.Text;
            cmd12.CommandText = "SELECT DISTINCT O.OrderId,O.OrderDate,O.Taxes,O.OrderTotal,O.Penalty,O.Incentive,O.Status FROM ORDERS O,PROJECTS,PROJECTORDER WHERE O.OrderId=PROJECTORDER.OrderId AND PROJECTS.ProjectId=PROJECTORDER.ProjectId AND PROJECTS.ProjectId= '" + LblProjectId.Text + "'";
            SqlDataAdapter adpt12 = new SqlDataAdapter(cmd12);
            DataTable dt12 = new DataTable();
            adpt12.Fill(dt12);

            if (dt12.Rows.Count > 0)
            {
                GrdViewOrderDetails.DataSource = dt12;
                GrdViewOrderDetails.DataBind();

                //if we want to delete row than first want to use gridview for each loop to save data in temporder table than delete it from table//
            }
            else
            {
                GrdViewOrderDetails.DataSource = dt12;
                GrdViewOrderDetails.DataBind();
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Order Not Available');", true);
                //return;
            }
            con12.Close();
        }

        protected void LBtnOrderList_Click(object sender, EventArgs e)
        {

            if (LblProjectId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Project Name');", true);
                return;
            }

            else
            {
                LBtnOrderList.BackColor = System.Drawing.Color.SkyBlue;
                LBtnOrderList.BorderColor = System.Drawing.Color.Black;
                LBtnReport.BackColor = LBtnNewTask.BackColor = LBtnOverview.BackColor = LBtnUserList.BackColor = LBtnBasicInfo.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnReport.BorderColor = LBtnNewTask.BorderColor = LBtnOverview.BorderColor = LBtnUserList.BorderColor = LBtnBasicInfo.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrderList.ForeColor = System.Drawing.Color.Red;
                LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnReport.ForeColor = System.Drawing.Color.RoyalBlue;

                divOrder.Visible = true;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                NewTask.Visible = false;
                BottomButtonDiv.Visible = true;
                divReport.Visible = false;

                GridViewEditOrder.DataSource = null;
                GridViewEditOrder.DataBind();
                BindOrderList();
                divAPV.Visible = false;
                //BOrderList();
            }
        }

        protected void LBtnNewTask_Click(object sender, EventArgs e)//Not In Use??//
        {
            //if (LblProjectId.Text == string.Empty)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Customer Name');", true);
            //    return;
            //}

            //else
            //{
            NewTask.Visible = true;
            divOverView.Visible = false;
            divBasicInfo.Visible = false;
            divContactInfo.Visible = false;
            divOrder.Visible = false;
            BottomButtonDiv.Visible = true;

            //BindDataForQuote();
            //CallJavaScript(sender);
            //LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
            LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            //}
        }

        public void CallJavaScript(object sender)
        {
            LinkButton lnk = (LinkButton)sender;
            //Response.Write("link button:" + lnk.Text + " is clicked");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Ming1", "SetColor('" + lnk.ClientID + "')", true);
        }

        protected void LBtnProjectName_Click(object sender, EventArgs e)
        {
            SqlConnection con2 = new SqlConnection(cs);
            SqlCommand cmd2 = new SqlCommand();
            cmd2.Connection = con2;
            con2.Open();

            Global.OrderValues = 0;// when first time order Selected all gridview data and selected order is saved in Temporder table 
            //and if OrderValues==1 than  gridview values is already saved only want to save dropdown order values save in table - stop repeatation

            cmd2.CommandType = System.Data.CommandType.Text;
            cmd2.CommandText = "DELETE  FROM TEMPORDER";
            cmd2.ExecuteNonQuery();
            cmd2.Parameters.Clear();

            Global.DeleteProOrder = 0;//for Deleting Data from ProjectOrder Table

            LBtnOverview.BackColor = System.Drawing.Color.SkyBlue;
            LBtnOverview.BorderColor = System.Drawing.Color.Black;
            LBtnReport.BackColor = LBtnNewTask.BackColor = LBtnOrderList.BackColor = LBtnUserList.BackColor = LBtnBasicInfo.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
            LBtnReport.BorderColor = LBtnNewTask.BorderColor = LBtnOrderList.BorderColor = LBtnUserList.BorderColor = LBtnBasicInfo.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

            LBtnOverview.ForeColor = System.Drawing.Color.Red;
            LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnReport.ForeColor = System.Drawing.Color.RoyalBlue;

            divOverView.Visible = true;
            divBasicInfo.Visible = false;
            divContactInfo.Visible = false;
            divOrder.Visible = false;
            NewTask.Visible = false;
            BottomButtonDiv.Visible = false;
            divReport.Visible = false;

            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            Label ProId = ((Label)grdrow.Cells[0].FindControl("ProjectId"));
            Label ProName = ((Label)grdrow.Cells[0].FindControl("ProjectName"));
            Label lblCustid1 = ((Label)grdrow.Cells[0].FindControl("CustId"));

            //LinkButton LBtnCustomerName = ((LinkButton)grdrow.Cells[2].FindControl("LBtnCustomerName"));
            LblProjectId.Text = ProId.Text;
            LblProjectName.Text = ProName.Text;
            lblCustid.Text = lblCustid1.Text;

            //for Color changing//
            foreach (GridViewRow rows in GridViewCustomer.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.LinkButton LBtnProjectName = (rows.Cells[1].FindControl("LBtnProjectName") as System.Web.UI.WebControls.LinkButton);
                    System.Web.UI.WebControls.Label LBLProjectId = (rows.Cells[1].FindControl("ProjectId") as System.Web.UI.WebControls.Label);

                    if (LblProjectName.Text == LBtnProjectName.Text && LBLProjectId.Text == ProId.Text)
                    {
                        LBtnProjectName.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        LBtnProjectName.ForeColor = System.Drawing.Color.RoyalBlue;
                    }
                }
            }

            if (LblProjectId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Project Name');", true);
                return;
            }
            else
            {
                //BtnUpdate.Visible = true;
                cmd2.Parameters.Clear();
                cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                cmd2.CommandText = "sp_ViewProject";

                cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.NVarChar).Value = LblProjectId.Text; //Request.QueryString["Projectid"];
                SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
                DataSet dt2 = new DataSet();
                adpt2.Fill(dt2);

                if (dt2.Tables[0].Rows.Count > 0)
                {
                    gvUpdateProject.DataSource = dt2;
                    gvUpdateProject.DataBind();

                    lblCustid.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                    txtProjectName.Text = dt2.Tables[0].Rows[0]["ProjectName"].ToString();
                    txtDescript.Text = dt2.Tables[0].Rows[0]["Description"].ToString();
                    //txtStartdt.Text = dt2.Tables[0].Rows[0]["ProjectDate"].ToString();
                    //txtCompletionDate.Text = dt2.Tables[0].Rows[0]["CompletionDate"].ToString();
                    txtStartdt.Text = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(dt2.Tables[0].Rows[0]["ProjectDate"].ToString()));

                    txtCompareDate.Text = txtStartdt.Text;
                    txtCompletionDate.Text = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(dt2.Tables[0].Rows[0]["CompletionDate"].ToString()));

                    ddlCustName.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                    //ddOrderList.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();
                    //LblOrder.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();

                    dtp3.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["ProjectDate"].ToString());
                    dtp4.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["CompletionDate"].ToString());
                    Global.Editflag = true;

                    GridViewSelEmployee.DataSource = dt2;
                    GridViewSelEmployee.DataBind();

                    int count = dt2.Tables[0].Rows.Count - 1;

                    foreach (GridViewRow rw in GridViewEmpname.Rows)
                    {
                        if (rw.RowType == DataControlRowType.DataRow)
                        {
                            Global.chkUser = 0;

                            foreach (GridViewRow rows in GridViewSelEmployee.Rows)
                            {
                                if (rows.RowType == DataControlRowType.DataRow)
                                {
                                    System.Web.UI.WebControls.Label lblEmpID = (rw.Cells[0].FindControl("lbllEmpid") as System.Web.UI.WebControls.Label);
                                    CheckBox chkselect = (rw.Cells[0].FindControl("chkSelect") as CheckBox);

                                    Label lbllEmpid = (rows.Cells[1].FindControl("lblEmpID") as Label);
                                    DropDownList ddlTaskList = (rows.Cells[4].FindControl("ddlTaskList") as DropDownList);

                                    if (lbllEmpid.Text == lblEmpID.Text)
                                    {
                                        chkselect.Checked = true;

                                        Global.chkUser = 1;
                                    }
                                    if (Global.chkUser == 0)
                                    {
                                        chkselect.Checked = false;
                                    }
                                }
                            }
                        }
                    }
                    Chart();
                }
                else
                {
                    cmd2.Parameters.Clear();
                    dt2.Tables[0].Rows.Clear();
                    cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd2.CommandText = "sp_GetProjectDetails";
                    cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.NVarChar).Value = LblProjectId.Text; //Request.QueryString["Projectid"];
                    adpt2.SelectCommand = cmd2;
                    adpt2.Fill(dt2);
                    if (dt2.Tables[0].Rows.Count > 0)
                    {
                        gvUpdateProject.DataSource = dt2;
                        gvUpdateProject.DataBind();

                        GridViewSelEmployee.DataSource = null;
                        GridViewSelEmployee.DataBind();

                        foreach (GridViewRow rows in GridViewEmpname.Rows)
                        {
                            if (rows.RowType == DataControlRowType.DataRow)
                            {
                                System.Web.UI.WebControls.Label lbllEmpid = (rows.Cells[0].FindControl("lbllEmpid") as System.Web.UI.WebControls.Label);
                                CheckBox chkselect = (rows.Cells[0].FindControl("chkSelect") as CheckBox);

                                if (chkselect.Checked == true)
                                {
                                    chkselect.Checked = false;
                                }
                            }
                        }

                        lblCustid.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                        txtProjectName.Text = dt2.Tables[0].Rows[0]["ProjectName"].ToString();
                        txtDescript.Text = dt2.Tables[0].Rows[0]["Description"].ToString();

                        //txtStartdt.Text = dt2.Tables[0].Rows[0]["ProjectDate"].ToString();
                        //txtCompletionDate.Text = dt2.Tables[0].Rows[0]["CompletionDate"].ToString();

                        txtStartdt.Text = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(dt2.Tables[0].Rows[0]["ProjectDate"].ToString()));
                        txtCompareDate.Text = txtStartdt.Text;
                        txtCompletionDate.Text = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(dt2.Tables[0].Rows[0]["CompletionDate"].ToString()));

                        ddlCustName.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();

                        // ddOrderList.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();
                        //LblOrder.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();

                        dtp3.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["ProjectDate"].ToString());
                        dtp4.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["CompletionDate"].ToString());

                        Global.Editflag = false;
                        Chart();
                    }
                }
                BOrderList();
            }
            con2.Close();
        }

        protected void LBtnBasicInfo_Click(object sender, EventArgs e)
        {

            if (LblProjectId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Project Name');", true);
                return;
            }
            else
            {
                LBtnBasicInfo.BackColor = System.Drawing.Color.SkyBlue;
                LBtnBasicInfo.BorderColor = System.Drawing.Color.Black;
                LBtnReport.BackColor = LBtnNewTask.BackColor = LBtnOrderList.BackColor = LBtnUserList.BackColor = LBtnOverview.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnReport.BorderColor = LBtnNewTask.BorderColor = LBtnOrderList.BorderColor = LBtnUserList.BorderColor = LBtnOverview.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.Red;
                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnReport.ForeColor = System.Drawing.Color.RoyalBlue;

                divBasicInfo.Visible = true;
                divOverView.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                NewTask.Visible = false;
                BottomButtonDiv.Visible = true;
                divAPV.Visible = false;
                divReport.Visible = false;
            }
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)//----Not In Use---------------
        {
            DataTable dt1 = new DataTable();

            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("EmpId") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("FName") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("WagePerHr") });
            //dt1.Columns.AddRange(new DataColumn[] { new DataColumn("TaskID") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("EstimatedHours") });
            foreach (GridViewRow gvrows in GridViewEmpname.Rows)
            {

                if (gvrows.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkselect = (gvrows.Cells[0].FindControl("chkSelect") as CheckBox);
                    Label lbllEmpid = (gvrows.Cells[0].FindControl("lbllEmpid") as Label);
                    //int i = 0;
                    if (chkselect.Checked)
                    {
                        SqlConnection con2 = new SqlConnection(cs);
                        con2.Open();
                        SqlCommand cmd2 = new SqlCommand();
                        cmd2.Connection = con2;

                        if (LblProjectId.Text != "")
                        {
                            cmd2.Parameters.Clear();
                            cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd2.CommandText = "sp_GetPAssign";
                            cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = LblProjectId.Text; //Request.QueryString["Projectid"];
                            cmd2.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = lbllEmpid.Text;
                            SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
                            DataTable dt2 = new DataTable();
                            adpt2.Fill(dt2);
                            DataRow dr = dt1.NewRow();

                            if (dt2.Rows.Count > 0)
                            {
                                dr["EmpId"] = dt2.Rows[0]["empid"];
                                dr["FName"] = dt2.Rows[0]["fname"];
                                dr["WagePerHr"] = dt2.Rows[0]["wageperhr"];
                                //dr["Taskid"] = dt2.Rows[0]["TaskId"]; 
                                dr["EstimatedHours"] = dt2.Rows[0]["EstimatedHours"];
                                dt1.Rows.Add(dr);
                                //i = i + 1;
                            }
                            //if (lblEmpID.Text == gvrows.Cells[1].Text)
                            //{

                            //    dr["EmpId"] = gvrows.Cells[1].Text;
                            //    dr["FName"] = gvrows.Cells[2].Text;
                            //    dr["WagePerHr"] = txtWagePerHr.Text;
                            //    //dr["Task"] = ddlTaskList.SelectedItem.Text;
                            //    dr["EstimatedHours"] = txtEstiHrs.Text;
                            //    dt1.Rows.Add(dr);
                            //    break;

                            //}
                            else
                            {
                                //DataRow dr = dt1.NewRow();
                                dr["EmpId"] = gvrows.Cells[1].Text;
                                dr["FName"] = gvrows.Cells[2].Text;
                                dr["WagePerHr"] = 0;
                                //dr["Taskid"] = ""; 
                                dr["EstimatedHours"] = 0;
                                dt1.Rows.Add(dr);
                                //break;
                            }

                            //String EmpId = gvrows.Cells[1].Text;
                            //dt1.Rows.Add(EmpId);

                            //String EmployeeName = gvrows.Cells[2].Text;
                            //dt1.Rows.Add(EmployeeName);

                            GridViewSelEmployee.DataSource = dt1;
                            GridViewSelEmployee.DataBind();
                        }
                        else
                        {
                            DataRow dr = dt1.NewRow();
                            //DataRow dr = dt1.NewRow();
                            dr["EmpId"] = gvrows.Cells[1].Text;
                            dr["FName"] = gvrows.Cells[2].Text;
                            dr["WagePerHr"] = 0;
                            //dr["Taskid"] = ""; 
                            dr["EstimatedHours"] = 0;
                            dt1.Rows.Add(dr);
                            //break;
                        }

                        //String EmpId = gvrows.Cells[1].Text;
                        //dt1.Rows.Add(EmpId);

                        //String EmployeeName = gvrows.Cells[2].Text;
                        //dt1.Rows.Add(EmployeeName);

                        GridViewSelEmployee.DataSource = dt1;
                        GridViewSelEmployee.DataBind();
                    }
                    else
                    {
                        GridViewSelEmployee.DataSource = dt1;
                        GridViewSelEmployee.DataBind();

                        //cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        //cmd2.CommandText = "sp_GetPAssign";
                        //cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProid.Text; //Request.QueryString["Projectid"];
                        //cmd2.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = lbllEmpid.Text;
                        //SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
                        //DataTable dt2 = new DataTable();
                        //adpt2.Fill(dt2);
                    }
                }
            }
        }

        protected void ddOrderList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            if (ddOrderList.SelectedValue != "-Vælg Ordre-")
            {
                Global.DeleteProOrder = 1;// for deleting data from TEMPORDER Table

                if (Global.OrderValues == 0)
                {
                    foreach (GridViewRow rows in GrdViewOrderDetails.Rows)
                    {
                        if (rows.RowType == DataControlRowType.DataRow)
                        {
                            System.Web.UI.WebControls.Label lblOrderID = (rows.Cells[0].FindControl("lblOrderID") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.LinkButton LnkBtnOrderId = (rows.Cells[0].FindControl("LnkBtnOrderId") as System.Web.UI.WebControls.LinkButton);
                            System.Web.UI.WebControls.Label lblOrderDate = (rows.Cells[1].FindControl("lblOrderDate") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblTotalOrder = (rows.Cells[2].FindControl("lblTotalOrder") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblStatus = (rows.Cells[3].FindControl("lblStatus") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblTaxes = (rows.Cells[4].FindControl("lblTaxes") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblPenalty = (rows.Cells[5].FindControl("lblPenalty") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblIncentive = (rows.Cells[6].FindControl("lblIncentive") as System.Web.UI.WebControls.Label);

                            if (lblOrderID.Text != "")
                            {
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.CommandText = "sp_InsertTempOrder";

                                cmd.Parameters.Add("@TEMPORDERID", System.Data.SqlDbType.BigInt).Value = lblOrderID.Text;
                                cmd.Parameters.Add("@ORDERDATE", System.Data.SqlDbType.NVarChar).Value = lblOrderDate.Text;
                                cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = lblTaxes.Text;
                                cmd.Parameters.Add("@ORDERTOTAL", System.Data.SqlDbType.Decimal).Value = lblTotalOrder.Text;
                                cmd.Parameters.Add("@PENALTY", System.Data.SqlDbType.VarChar).Value = lblPenalty.Text;
                                cmd.Parameters.Add("@INCENTIVE", System.Data.SqlDbType.VarChar).Value = lblIncentive.Text;
                                cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = lblStatus.Text;

                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                            }
                        }
                    }
                }

                Global.OrderValues = 1;

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select * From ORDERS Where OrderId= '" + ddOrderList.SelectedValue + "'";
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                adpt.Fill(dt);

                if (dt.Tables[0].Rows.Count > 0)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_InsertTempOrder";

                    cmd.Parameters.Add("@TEMPORDERID", System.Data.SqlDbType.BigInt).Value = dt.Tables[0].Rows[0]["OrderId"].ToString();
                    cmd.Parameters.Add("@ORDERDATE", System.Data.SqlDbType.NVarChar).Value = dt.Tables[0].Rows[0]["OrderDate"].ToString();
                    cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = dt.Tables[0].Rows[0]["Taxes"].ToString();
                    cmd.Parameters.Add("@ORDERTOTAL", System.Data.SqlDbType.Decimal).Value = dt.Tables[0].Rows[0]["OrderTotal"].ToString();
                    cmd.Parameters.Add("@PENALTY", System.Data.SqlDbType.VarChar).Value = dt.Tables[0].Rows[0]["Penalty"].ToString();
                    cmd.Parameters.Add("@INCENTIVE", System.Data.SqlDbType.VarChar).Value = dt.Tables[0].Rows[0]["Incentive"].ToString();
                    cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dt.Tables[0].Rows[0]["Status"].ToString();

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Order Not Available');", true);
                    return;
                }

                cmd.Parameters.Clear();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select distinct * From TEMPORDER";
                SqlDataAdapter adpt1 = new SqlDataAdapter(cmd);
                DataSet dt1 = new DataSet();
                adpt1.Fill(dt1);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    GrdViewOrderDetails.DataSource = dt1;
                    GrdViewOrderDetails.DataBind();
                }
                else
                {
                    GrdViewOrderDetails.DataSource = dt1;
                    GrdViewOrderDetails.DataBind();
                }
                con.Close();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Order');", true);
                return;
            }


        }

        protected void ddlCustName_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCustid.Text = ddlCustName.SelectedValue;
        }

        public void resetTask()
        {

            int taskid;
            int subtskid;
            SqlConnection con11 = new SqlConnection(cs);
            SqlCommand cmd11 = new SqlCommand();
            cmd11.Connection = con11;
            con11.Open();

            cmd11.CommandType = System.Data.CommandType.Text;
            cmd11.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TEMPTASK";
            taskid = Convert.ToInt32(cmd11.ExecuteScalar());
            lbltaskid.Text = taskid.ToString();
            cmd11.Parameters.Clear();

            cmd11.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
            subtskid = Convert.ToInt32(cmd11.ExecuteScalar());

            cmd11.CommandType = System.Data.CommandType.StoredProcedure;
            cmd11.CommandText = "sp_InsertTempTask";
            cmd11.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
            cmd11.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
            cmd11.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
            cmd11.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
            cmd11.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
            cmd11.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

            cmd11.ExecuteNonQuery();
            cmd11.Parameters.Clear();

            cmd11.CommandType = System.Data.CommandType.Text;
            cmd11.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(cmd11);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();

            cmd11.Parameters.Clear();
            con11.Close();

            lbltaskid.Text = Convert.ToInt32(taskid).ToString();
            TaskId.Text = lbltaskid.Text;
        }

        protected void btnAddNewTask_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubtask = (rows.Cells[0].FindControl("lblSubtask") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[1].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLSkillSet = (rows.Cells[3].FindControl("DDLSkillSet") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtStatus = (rows.Cells[4].FindControl("txtStatus") as System.Web.UI.WebControls.TextBox);

                    //if (txtTask.Text == "")
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                    //    return;
                    //}

                    //else
                    //{
                    com.Parameters.Clear();
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    com.Connection = con;
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_UpdateTempTask";
                    com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                    com.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                    com.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = DDLSkillSet.SelectedItem.Text;
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtStatus.Text;
                    com.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = lblSubtask.Text;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = DDLSkillSet.SelectedValue;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    //com.CommandType = System.Data.CommandType.Text;
                    //com.CommandText = "SELECT * FROM TEMPTASK";
                    //SqlDataAdapter dtask = new SqlDataAdapter(com);
                    //DataTable dttask = new DataTable();
                    //dtask.Fill(dttask);
                    //gridTask.DataSource = dttask;
                    //gridTask.DataBind();

                    con.Close();
                    // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details Added SuccessFully');", true);

                    //resetall();
                    //}
                }
            }
            resetTask();
        }

        //protected void btnUpload_Click(object sender, EventArgs e)
        //{
        //    //int taskid;
        //    //SqlConnection con1 = new SqlConnection(cs);
        //    //con1.Open();
        //    //SqlCommand cmd1 = new SqlCommand();C:\Tools\ProjectManagement\ProjectManagement\Order Form.aspx
        //    //cmd1.Connection = con1;

        //    //cmd1.CommandType = System.Data.CommandType.Text;
        //    //cmd1.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
        //    //taskid = Convert.ToInt32(cmd1.ExecuteScalar()) + 1;
        //    //con1.Close();
        //    //lbltaskid.Text = taskid.ToString();
        //    string filename = Path.GetFileName(FileUpload2.PostedFile.FileName);
        //    string filePath = @"~\Uploads\" + filename.Trim();
        //    string newfile = Server.MapPath(filePath);
        //    FileUpload2.SaveAs(newfile);
        //    //Stream str = FileUpload1.PostedFile.InputStream;

        //    //BinaryReader br = new BinaryReader(str);

        //    //Byte[] size = br.ReadBytes((int)str.Length);

        //    using (SqlConnection con = new SqlConnection(cs))
        //    {

        //        using (SqlCommand cmd = new SqlCommand())
        //        {

        //            cmd.CommandText = "insert into TaskFIleData(TaskId,FileName,FilePath) values(@id,@Name,@FilePath)";

        //            cmd.Parameters.AddWithValue("@id", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;

        //            cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarChar).Value = filename;

        //            cmd.Parameters.AddWithValue("@FilePath", System.Data.SqlDbType.VarChar).Value = newfile;

        //            cmd.Connection = con;

        //            con.Open();

        //            cmd.ExecuteNonQuery();

        //            con.Close();



        //        }

        //    } gvFileUpload1();
        //}
        private void gvFileUpload1()
        {

            using (SqlConnection con = new SqlConnection(cs))
            {

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from TaskFIleData where TaskId=" + lbltaskid.Text;

                    cmd.Connection = con;

                    con.Open();

                    gvFileUpload.DataSource = cmd.ExecuteReader();

                    gvFileUpload.DataBind();

                    con.Close();
                }

            }

        }

        protected void SaveTask_Click(object sender, EventArgs e)
        {
            string hours = "00:00:00";

            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubtask = (rows.Cells[0].FindControl("lblSubtask") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[1].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLSkillSet = (rows.Cells[3].FindControl("DDLSkillSet") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtStatus = (rows.Cells[4].FindControl("txtStatus") as System.Web.UI.WebControls.TextBox);

                    if (txtTask.Text == string.Empty || txtMTask.Text == string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                        return;
                    }

                    else
                    {
                        hours = ddlHours.Text + ":" + ddlMinutes.Text + ":" + ddlSeconds.Text;

                        SqlConnection con = new SqlConnection(cs);
                        con.Open();
                        com.Connection = con;
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertTaskDetails";
                        com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                        com.Parameters.Add("@STATUS ", System.Data.SqlDbType.VarChar).Value = txtStatus.Text;
                        com.Parameters.Add("@ENDDATE", System.Data.SqlDbType.VarChar).Value = "";
                        com.Parameters.Add("@TASKS ", System.Data.SqlDbType.VarChar).Value = txtMTask.Text;
                        com.Parameters.Add("@FILEPATH", System.Data.SqlDbType.VarChar).Value = FileUpload1.FileName;
                        com.Parameters.Add("@EXPECTEDHOURS", System.Data.SqlDbType.Time).Value = hours;
                        com.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = DDLSkillSet.SelectedItem.Text;
                        com.Parameters.Add("@SUBTASK", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                        com.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = lblSubtask.Text;

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                        con.Close();
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details Added SuccessFully');", true);
                    }
                }
            }
            resetall();
            ViewTaskList();
            BindTaskForEmployee();
        }

        public void BindTaskForEmployee()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = " select distinct Task,TaskId from TASK";

            SqlDataAdapter adp = new SqlDataAdapter(com);
            DataTable dtb = new DataTable();

            adp.Fill(dtb);

            System.Web.UI.WebControls.DropDownList ddlMainTask = (GridViewSelEmployee.Rows[0].FindControl("ddlMainTask") as System.Web.UI.WebControls.DropDownList);

            if (dtb.Rows.Count > 0)
            {
                ddlMainTask.DataSource = dtb;
                ddlMainTask.DataTextField = "Task";
                ddlMainTask.DataValueField = "TaskId";
                ddlMainTask.DataBind();
            }
            con.Close();
        }

        public void resetall()
        {
            txttask_0.Text = "";
            //txtExpectedHrs.Text = "00:00";
            txtMTask.Text = "";
            int taskid, subtskid;
            SqlConnection con11 = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con11;
            con11.Open();

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
            taskid = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
            lbltaskid.Text = taskid.ToString();

            cmd.Parameters.Clear();
            cmd.CommandText = "Delete FROM TEMPTASK";
            cmd.ExecuteNonQuery();

            cmd.Parameters.Clear();
            cmd.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
            subtskid = Convert.ToInt32(cmd.ExecuteScalar());
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_InsertTempTask";
            cmd.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
            cmd.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
            cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
            cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(cmd);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();

            cmd.Parameters.Clear();

            con11.Close();

            lbltaskid.Text = Convert.ToInt32(taskid).ToString();
            TaskId.Text = lbltaskid.Text;
        }

        protected void GridViewSelEmployee_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();

            SqlCommand comm = new SqlCommand("select distinct Task,TaskId from TASK", con1);
            SqlDataAdapter adpt12 = new SqlDataAdapter(comm);
            DataTable dt12 = new DataTable();

            adpt12.Fill(dt12);
            if (dt12.Rows.Count > 0)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Global.Editflag == false)
                    {
                        //DropDownList ddlDropDownList = (DropDownList)e.Row.FindControl("ddlSubTaskList");
                        DropDownList ddlMainTask = (DropDownList)e.Row.FindControl("ddlMainTask");

                        if (ddlMainTask != null)
                        {
                            ddlMainTask.DataSource = dt12;
                            ddlMainTask.DataTextField = "Task";
                            ddlMainTask.DataValueField = "TaskId";
                            ddlMainTask.DataBind();
                        }
                        ddlMainTask.Items.Insert(0, new ListItem("-Vælg Opgave-", "0"));
                    }
                    else
                    {
                        comm.Parameters.Clear();

                        //if (ddlMainTask != null)
                        //{

                        //    ddlMainTask.DataSource = dt12;
                        //    ddlMainTask.DataTextField = "Task";
                        //    ddlMainTask.DataValueField = "TaskId";
                        //    ddlMainTask.DataBind();
                        //}

                        //foreach (GridViewRow rw in GridViewSelEmployee.Rows)
                        //{
                        //    if (rw.RowType == DataControlRowType.DataRow)
                        //    {
                        Label lblEmpID = (Label)e.Row.FindControl("lblEmpID");
                        //DropDownList ddlTaskList = (rw.Cells[4].FindControl("ddlTaskList") as DropDownList);
                        DropDownList ddlMainTask = (DropDownList)e.Row.FindControl("ddlMainTask");
                        DropDownList ddlSubTaskList = (DropDownList)e.Row.FindControl("ddlSubTaskList");

                        comm.CommandText = "select distinct Task.Task,Task.TaskId,Task.SubTask,Task.STID from TASK,ASSIGNED where ASSIGNED.STID=TASK.STID AND Task.TaskId=ASSIGNED.TaskId AND ASSIGNED.Empid=" + lblEmpID.Text + " and ASSIGNED.ProjectId=" + LblProjectId.Text + "";
                        adpt12 = new SqlDataAdapter(comm);
                        DataTable dttask = new DataTable();

                        adpt12.Fill(dttask);

                        if (dttask.Rows.Count > 0)
                        {
                            //

                            if (ddlMainTask != null)
                            {
                                ddlMainTask.DataSource = dt12;
                                ddlMainTask.DataTextField = "Task";
                                ddlMainTask.DataValueField = "TaskId";
                                ddlMainTask.DataBind();

                                ddlSubTaskList.DataSource = dttask;
                                ddlSubTaskList.DataTextField = "SubTask";
                                ddlSubTaskList.DataValueField = "STID";
                                ddlSubTaskList.DataBind();

                                ddlMainTask.SelectedItem.Text = dttask.Rows[0]["Task"].ToString();
                                ddlMainTask.SelectedValue = dttask.Rows[0]["TaskId"].ToString();

                                ddlSubTaskList.Visible = true;

                                ddlSubTaskList.SelectedItem.Text = dttask.Rows[0]["SubTask"].ToString();
                                ddlSubTaskList.SelectedValue = dttask.Rows[0]["STID"].ToString();
                                dttask.Rows.Clear();
                                comm.Parameters.Clear();
                            }
                        }
                        else
                        {
                            if (ddlMainTask != null)
                            {

                                ddlMainTask.DataSource = dt12;
                                ddlMainTask.DataTextField = "Task";
                                ddlMainTask.DataValueField = "TaskId";
                                ddlMainTask.DataBind();
                            }
                            ddlMainTask.Items.Insert(0, new ListItem("-Vælg Opgave-", "0"));
                        }
                        //    }
                        //}
                    }


                }
            }
            con1.Close();
        }

        protected void LBtnNewTask_Click1(object sender, EventArgs e)
        {
            if (LblProjectId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Project Name');", true);
                return;
            }

            else
            {
                LBtnNewTask.BackColor = System.Drawing.Color.SkyBlue;
                LBtnNewTask.BorderColor = System.Drawing.Color.Black;
                LBtnReport.BackColor = LBtnOverview.BackColor = LBtnOrderList.BackColor = LBtnUserList.BackColor = LBtnBasicInfo.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnReport.BorderColor = LBtnOverview.BorderColor = LBtnOrderList.BorderColor = LBtnUserList.BorderColor = LBtnBasicInfo.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnNewTask.ForeColor = System.Drawing.Color.Red;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnReport.ForeColor = System.Drawing.Color.RoyalBlue;

                NewTask.Visible = true;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                BottomButtonDiv.Visible = false;
                divAPV.Visible = false;
                divReport.Visible = false;

                BindSkillset();
            }
        }

        protected void ddlMainTask_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            DropDownList ddlMainTask = (DropDownList)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)ddlMainTask.Parent.Parent);
            //DropDownList ddlMainTask = (DropDownList)grdrDropDownRow.FindControl("ddlTaskList");

            DropDownList ddlSubTaskList = (DropDownList)grdrDropDownRow.FindControl("ddlSubTaskList");
            com.CommandText = "select  distinct SubTask,STID from TASK where TASKID= " + ddlMainTask.SelectedValue;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                if (ddlSubTaskList != null)
                {

                    ddlSubTaskList.DataSource = dt;
                    ddlSubTaskList.DataTextField = "SubTask";
                    ddlSubTaskList.DataValueField = "STID";
                    ddlSubTaskList.DataBind();
                }
                ddlSubTaskList.Items.Insert(0, new ListItem("-Vælg Opgave-", "0"));
                ddlSubTaskList.Visible = true;
            }
            con.Close();
        }

        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            con.Open();

            int projid = 0;
            int projid1 = 0;
            if (lblCustid.Text == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select proper Customer Name');", true);
                return;
            }
            else
            {
                DateTime inTime = Convert.ToDateTime(txtStartdt.Text);
                DateTime outTime = Convert.ToDateTime(txtCompletionDate.Text);
                if (inTime > outTime)
                {
                    //BtnUpdate.Enabled = true;
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper date Which is Greater Than Start Date');", true);
                    return;
                }
                else
                {
                    //BtnUpdate.Enabled = false;

                    cmd.Parameters.Clear();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT ISNULL((Projectid),0) FROM ASSIGNED where ProjectId=@ASSPROJECTID";
                    cmd.Parameters.Add("@ASSPROJECTID", System.Data.SqlDbType.BigInt).Value = LblProjectId.Text;
                    projid = Convert.ToInt32(cmd.ExecuteScalar());

                    if (projid > 0)
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "DELETE FROM ASSIGNED WHERE ProjectId=@ASSignPROJECTID";
                        cmd.Parameters.Add("@ASSignPROJECTID", System.Data.SqlDbType.BigInt).Value = projid;
                        cmd.ExecuteNonQuery();
                    }
                    foreach (GridViewRow rows in GridViewSelEmployee.Rows)
                    {
                        if (rows.RowType == DataControlRowType.DataRow)
                        {
                            System.Web.UI.WebControls.Label lblEmpID = (rows.Cells[1].FindControl("lblEmpID") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblEmpNAME = (rows.Cells[2].FindControl("lblEmpNAME") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.TextBox txtWagePerHr = (rows.Cells[3].FindControl("txtWagePerHr") as System.Web.UI.WebControls.TextBox);
                            System.Web.UI.WebControls.DropDownList ddlMainTask = (rows.Cells[4].FindControl("ddlMainTask") as System.Web.UI.WebControls.DropDownList);
                            System.Web.UI.WebControls.DropDownList ddlSubTaskList = (rows.Cells[5].FindControl("ddlSubTaskList") as System.Web.UI.WebControls.DropDownList);
                            System.Web.UI.WebControls.TextBox txtEstiHrs = (rows.Cells[6].FindControl("txtEstiHrs") as System.Web.UI.WebControls.TextBox);

                            if (lblEmpID.Text != "")
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.CommandText = "sp_UpdateAssigned";
                                cmd.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = ddlMainTask.SelectedValue;
                                cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpID.Text;
                                cmd.Parameters.Add("@ASSIGN_PROJECTID  ", System.Data.SqlDbType.BigInt).Value = LblProjectId.Text;
                                cmd.Parameters.Add("@ASSIGNEDDATE", System.Data.SqlDbType.DateTime).Value = txtStartdt.Text;

                                if (txtWagePerHr.Text != "")
                                {
                                    cmd.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = txtWagePerHr.Text;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = 0;
                                }
                                if (txtEstiHrs.Text != "")
                                {
                                    cmd.Parameters.Add("@ESTIMATEDHOURS", System.Data.SqlDbType.Decimal).Value = txtEstiHrs.Text;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@ESTIMATEDHOURS", System.Data.SqlDbType.Decimal).Value = 0;
                                }
                                cmd.Parameters.Add("@LABORCHARGES", System.Data.SqlDbType.Decimal).Value = 0;

                                if (ddlSubTaskList.Text != "-Vælg Opgave-" && ddlSubTaskList.Text != "")
                                {
                                    cmd.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = ddlSubTaskList.SelectedValue;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = 0;
                                }
                                cmd.ExecuteNonQuery();
                            }
                        }
                        // BottomButtonDiv.Visible = false;
                    }
                    cmd.Parameters.Clear();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT ISNULL((ProjectId),0) FROM PROJECTORDER  where ProjectId=@PROJECT_ORDERID";
                    cmd.Parameters.Add("@PROJECT_ORDERID", System.Data.SqlDbType.BigInt).Value = LblProjectId.Text;
                    projid1 = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Parameters.Clear();

                    if (projid1 > 0)
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "DELETE FROM PROJECTORDER WHERE ProjectId=@PROJECT_ORID";
                        cmd.Parameters.Add("@PROJECT_ORID", System.Data.SqlDbType.BigInt).Value = LblProjectId.Text;
                        cmd.ExecuteNonQuery();
                    }

                    foreach (GridViewRow rows in GrdViewOrderDetails.Rows)
                    {
                        if (rows.RowType == DataControlRowType.DataRow)
                        {
                            System.Web.UI.WebControls.Label lblOrderID = (rows.Cells[0].FindControl("lblOrderID") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.LinkButton LnkBtnOrderId = (rows.Cells[0].FindControl("LnkBtnOrderId") as System.Web.UI.WebControls.LinkButton);
                            System.Web.UI.WebControls.Label lblOrderDate = (rows.Cells[1].FindControl("lblOrderDate") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblTotalOrder = (rows.Cells[2].FindControl("lblTotalOrder") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblStatus = (rows.Cells[3].FindControl("lblStatus") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblTaxes = (rows.Cells[4].FindControl("lblTaxes") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblPenalty = (rows.Cells[5].FindControl("lblPenalty") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblIncentive = (rows.Cells[6].FindControl("lblIncentive") as System.Web.UI.WebControls.Label);

                            if (lblOrderID.Text != "")
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandType = System.Data.CommandType.Text;
                                cmd.CommandText = "INSERT INTO PROJECTORDER (ProjectId,OrderId) Values (@PRO_JECTID,@ORDERID)";

                                cmd.Parameters.Add("@PRO_JECTID  ", System.Data.SqlDbType.BigInt).Value = LblProjectId.Text;
                                cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = lblOrderID.Text;
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }

                    cmd.Parameters.Clear();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_UpdateProject";

                    cmd.Parameters.Add("@I_PRJECTID", System.Data.SqlDbType.BigInt).Value = LblProjectId.Text;
                    cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustid.Text;
                    //com.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = LblOrder.Text;
                    cmd.Parameters.Add("@PROJECTNAME", System.Data.SqlDbType.NVarChar).Value = txtProjectName.Text;
                    cmd.Parameters.Add("@DESCRIPTION", System.Data.SqlDbType.NVarChar).Value = txtDescript.Text;
                    cmd.Parameters.Add("@PROJECTDATE", System.Data.SqlDbType.DateTime).Value = txtStartdt.Text;
                    cmd.Parameters.Add("@COMPDATE", System.Data.SqlDbType.DateTime).Value = txtCompletionDate.Text;

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "DELETE  FROM TEMPORDER";
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    //Thread.Sleep(2000);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<scripts>alert('!! Data submited !! ');</scripts>");
                    //BtnUpdate.Visible = false;
                }
            }
            con.Close();
            Chart();
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Project Update Successfully !!');", true);
            return;
        }

        protected void GridViewCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                int ProId = Convert.ToInt32(e.CommandArgument);

                SqlConnection con1 = new SqlConnection(cs);
                con1.Open();

                SqlCommand com1 = new SqlCommand("delete FROM PROJECTS where ProjectId = @PROJECTID ; delete FROM ASSIGNED  where ProjectId = @PROJECTID;  delete FROM PROJECTORDER  where ProjectId = @PROJECTID", con1);

                //SqlCommand com1 = new SqlCommand();
                //com1.CommandText = "sp_deleteQuatation";
                //com1.CommandType = System.Data.CommandType.StoredProcedure;
                //com1.Connection = con1; 
                //com1.CommandType = CommandType.Text;
                com1.Parameters.Add("@PROJECTID", SqlDbType.BigInt).Value = ProId;


                //SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
                //DataTable dt1 = new DataTable();
                //adpt1.Fill(dt1);
                //GridView2.DataSource = dt1;

                com1.ExecuteNonQuery();
                con1.Close();
                BindData();
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Project Deleted Successfully!!');", true);
                return;
            }
        }

        protected void AddTask_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubtask = (rows.Cells[0].FindControl("lblSubtask") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[1].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLSkillSet = (rows.Cells[2].FindControl("DDLSkillSet") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtStatus = (rows.Cells[4].FindControl("txtStatus") as System.Web.UI.WebControls.TextBox);

                    //if (txtTask.Text == "")
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                    //    return;
                    //}

                    //else
                    //{
                    com.Parameters.Clear();
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    com.Connection = con;
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_UpdateTempTask";
                    com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                    com.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                    com.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = DDLSkillSet.SelectedItem.Text;
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtStatus.Text;
                    com.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = lblSubtask.Text;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = DDLSkillSet.SelectedValue;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    //com.CommandType = System.Data.CommandType.Text;
                    //com.CommandText = "SELECT * FROM TEMPTASK";
                    //SqlDataAdapter dtask = new SqlDataAdapter(com);
                    //DataTable dttask = new DataTable();
                    //dtask.Fill(dttask);
                    //gridTask.DataSource = dttask;
                    //gridTask.DataBind();

                    con.Close();
                    // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details Added SuccessFully');", true);

                    //resetall();
                    //}
                }
            }
            resetTask();
        }

        protected void btnVIewTask_Click(object sender, EventArgs e)
        {
            ViewTaskList();
        }

        public void ViewTaskList()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand amdd = new SqlCommand();
            con.Open();
            amdd.Connection = con;
            amdd.CommandType = System.Data.CommandType.StoredProcedure;
            amdd.CommandText = "sp_getTaskDetails";
            SqlDataAdapter adp1 = new SqlDataAdapter(amdd);
            DataSet dtt1 = new DataSet();
            adp1.Fill(dtt1);
            if (dtt1.Tables[0].Rows.Count > 0)
            {
                gvTaskView.DataSource = dtt1;
                gvTaskView.DataBind();
                IdTaskDetails.Visible = true;
            }
            else
            {
                gvTaskView.DataSource = dtt1;
                gvTaskView.DataBind();
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
            if (filename == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select FFile To Upload');", true);
                return;
            }
            else
            {
                string filePath = @"~\Uploads\" + filename.Trim();
                string newfile = Server.MapPath(filePath);
                FileUpload1.SaveAs(newfile);
                //Stream str = FileUpload1.PostedFile.InputStream;

                //BinaryReader br = new BinaryReader(str);

                //Byte[] size = br.ReadBytes((int)str.Length);

                using (SqlConnection con = new SqlConnection(cs))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "insert into TaskFIleData(TaskId,FileName,FilePath) values(@id,@Name,@FilePath)";

                        cmd.Parameters.AddWithValue("@id", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;

                        cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarChar).Value = filename;

                        cmd.Parameters.AddWithValue("@FilePath", System.Data.SqlDbType.VarChar).Value = newfile;

                        cmd.Connection = con;

                        con.Open();

                        cmd.ExecuteNonQuery();

                        con.Close();
                    }

                } gvFileUpload1();
                Idfiledetails.Visible = true;

            }
        }

        protected void lnkDeletetask_Click(object sender, EventArgs e)
        {
            LinkButton lnkDeletetask = (LinkButton)sender;
            GridViewRow grdRow = ((GridViewRow)lnkDeletetask.Parent.Parent);
            Label lbltaskid = (Label)grdRow.FindControl("lbltaskid");
            Label lblSubtask = (Label)grdRow.FindControl("lblSubtask");

            com.Parameters.Clear();
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            com.Connection = conn;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "Delete from TempTask where TASKID=" + lbltaskid.Text + "AND SubTask=" + lblSubtask.Text + "";
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(com);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();
            com.Parameters.Clear();

            conn.Close();
        }

        protected void gridTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            com.Connection = conn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList DDLSkillSet = (DropDownList)e.Row.FindControl("DDLSkillSet");
                Label lbltaskid = (Label)e.Row.FindControl("lbltaskid");
                Label lblSubtask = (Label)e.Row.FindControl("lblSubtask");

                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "Select Distinct EmpId,SkillSets from EMPLOYEE Where SkillSets!='" + null + "'"; // Change Code//
                SqlDataAdapter adp1 = new SqlDataAdapter(com);
                DataSet dtt1 = new DataSet();
                adp1.Fill(dtt1);
                DDLSkillSet.DataSource = dtt1;
                DDLSkillSet.DataTextField = "SkillSets";
                DDLSkillSet.DataValueField = "EmpId";
                DDLSkillSet.DataBind();
                //DDLSkillSet.Items.Insert(0, new ListItem("-Select SkillSet-", "0"));
                com.Parameters.Clear();

                dtt1.Tables[0].Rows.Clear();
                com.CommandText = "SELECT SkillSet,Empid,TaskName FROM TEMPTASK WHERE TASKID=" + lbltaskid.Text + "AND SubTask=" + lblSubtask.Text + "";
                adp1.SelectCommand = com;
                adp1.Fill(dtt1);
                if (dtt1.Tables[0].Rows.Count > 0)
                {
                    if (dtt1.Tables[0].Rows[0]["TaskName"] != "")
                    {
                        DDLSkillSet.SelectedItem.Text = dtt1.Tables[0].Rows[0]["Skillset"].ToString();
                        DDLSkillSet.SelectedValue = dtt1.Tables[0].Rows[0]["Empid"].ToString();
                    }
                    else
                    {
                        DDLSkillSet.Items.Insert(0, new ListItem("-Vælg SkillSet-", "0"));
                    }


                }
                com.Parameters.Clear();
                conn.Close();
            }
        }

        protected void lbtnViewImage_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //string FilePath = grdrow.Cells[2].Text;
            Label lblFilePath = (Label)grdrow.Cells[2].FindControl("lblFilePath");
            string FilePath = lblFilePath.Text;

            if (FilePath.Contains(".doc") || FilePath.Contains(".rtf") || FilePath.Contains(".html") || FilePath.Contains(".log") || FilePath.Contains(".jpeg") || FilePath.Contains(".tif") || FilePath.Contains(".tiff") || FilePath.Contains(".gif") || FilePath.Contains(".bmp") || FilePath.Contains(".asf"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            //view pdf files 
            if (FilePath.Contains(".pdf") || FilePath.Contains(".avi") || FilePath.Contains(".zip") || FilePath.Contains(".xls") || FilePath.Contains(".csv") || FilePath.Contains(".wav") || FilePath.Contains(".mp3"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }


            if (FilePath.Contains(".htm") || FilePath.Contains(".mpg") || FilePath.Contains(".mpeg") || FilePath.Contains(".fdf") || FilePath.Contains(".ppt") || FilePath.Contains(".dwg") || FilePath.Contains(".msg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;

            }

            if (FilePath.Contains(".txt") || FilePath.Contains(".xml") || FilePath.Contains(".sdxl") || FilePath.Contains(".xdp") || FilePath.Contains(".jar"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".jpg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".png"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

        }

        protected void LBtnOverview_Click2(object sender, EventArgs e)
        {
            if (LblProjectId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Project Name');", true);
                return;
            }

            else
            {
                LBtnOverview.BackColor = System.Drawing.Color.SkyBlue;
                LBtnOverview.BorderColor = System.Drawing.Color.Black;
                LBtnReport.BackColor = LBtnNewTask.BackColor = LBtnOrderList.BackColor = LBtnUserList.BackColor = LBtnBasicInfo.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnReport.BorderColor = LBtnNewTask.BorderColor = LBtnOrderList.BorderColor = LBtnUserList.BorderColor = LBtnBasicInfo.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOverview.ForeColor = System.Drawing.Color.Red;
                LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnReport.ForeColor = System.Drawing.Color.RoyalBlue;

                divOverView.Visible = true;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                NewTask.Visible = false;
                //CallJavaScript(sender);
                BottomButtonDiv.Visible = false;
                divAPV.Visible = false;
                divReport.Visible = false;
                Chart();
            }
        }

        protected void BtnAddProject_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProjectAssignment.aspx");
        }

        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            String sql = "Select * from PROJECTS where ProjectName Like @ProjectName";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {

                cmd.Parameters.Add("@ProjectName", System.Data.SqlDbType.VarChar, 200).Value = "%" + txtSearchBox.Text + "%";

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                DataSet dt1 = new DataSet();

                adpt.Fill(dt1);

                if (dt1.Tables[0].Rows.Count > 0)
                {

                    GridViewCustomer.DataSource = dt1;

                    GridViewCustomer.DataBind();
                }
            }
            con.Close();
        }

        protected void Chart()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            //Line Chart
            DataSet ds2 = new DataSet();
            string query2 = "Select Distinct DATEPART(MM,PROJECTS.ProjectDate) AS QuoteMonth,ORDERS.OrderTotal from HAS,ORDERS,QUOTATIONDATA ,PROJECTS ,PROJECTORDER  Where ORDERS.OrderId=HAS.OrderId and HAS.ItemCode=QUOTATIONDATA.ItemCode and HAS.TemDate=QUOTATIONDATA.TempDate and HAS.DateTime=ORDERS.OrderDate and PROJECTS.ProjectId=PROJECTORDER.ProjectId and  ORDERS.OrderId=PROJECTORDER.OrderId and QUOTATIONDATA.TEMPType='false' and  PROJECTS.ProjectId ='" + LblProjectId.Text + "'";

            SqlCommand cmd2 = new SqlCommand(query2, con);
            cmd2.ExecuteNonQuery();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);

            da2.Fill(ds2, "PremiumData");

            if (ds2.Tables["PremiumData"].Rows.Count > 0)
            {
                ChartOrder.DataSource = ds2.Tables["PremiumData"];
                // Set series members names for the X and Y values

                ChartOrder.Series["Count"].XValueMember = "QuoteMonth";

                ChartOrder.Series["Count"].YValueMembers = "OrderTotal";

                ChartOrder.Series["Count"].IsValueShownAsLabel = true;

                ChartOrder.Series["Count"].ChartType = SeriesChartType.Bar;

                ChartOrder.Titles[1].Text = "Bar Chart";
                ChartOrder.DataBind();
            }
            con.Close();

        }

        protected void LnkBtnOrderId_Click(object sender, EventArgs e)
        {
            LinkButton ID = (LinkButton)sender;

            if (ID != null)
            {
                LabelID.Text = ID.Text;
            }

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select * from HAS,ORDERS,QUOTATIONDATA where ORDERS.OrderId=HAS.OrderId and HAS.ItemCode=QUOTATIONDATA.ItemCode and HAS.TemDate=QUOTATIONDATA.TempDate and HAS.DateTime=ORDERS.OrderDate and QUOTATIONDATA.TEMPType='false' and ORDERS.OrderId= '" + LabelID.Text + "'";
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                GridViewEditOrder.DataSource = dt;
                GridViewEditOrder.DataBind();
            }
            else
            {
                GridViewEditOrder.DataSource = dt;
                GridViewEditOrder.DataBind();

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Order Not Available');", true);
                return;
            }
            con.Close();

            foreach (GridViewRow rows in GrdViewOrderDetails.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.LinkButton LnkBtnOrderId = (rows.Cells[0].FindControl("LnkBtnOrderId") as System.Web.UI.WebControls.LinkButton);

                    if (ID.Text == LnkBtnOrderId.Text)
                    {
                        rows.BackColor = System.Drawing.Color.LightBlue;
                    }
                    else
                    {
                        rows.BackColor = System.Drawing.Color.Empty;
                    }
                }
            }

            //if (Session["PreviousRowIndex"] != null)
            //{
            //    var previousRowIndex = (int)Session["PreviousRowIndex"];
            //    GridViewRow PreviousRow = GrdViewOrderDetails.Rows[previousRowIndex];
            //    PreviousRow.BackColor = System.Drawing.Color.Empty;
            //}
            //GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
            //row.BackColor = System.Drawing.Color.LightBlue;
            //Session["PreviousRowIndex"] = row.RowIndex;
        }

        protected void GrdViewOrderDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            SqlConnection con1 = new SqlConnection(cs);
            SqlCommand com1 = new SqlCommand();
            con1.Open();
            com1.Connection = con1;

            if (e.CommandName == "DeleteRow")
            {
                int ORDERId = Convert.ToInt32(e.CommandArgument);

                if (Global.DeleteProOrder == 1)
                {
                    com1.CommandType = System.Data.CommandType.Text;
                    com1.CommandText = "delete FROM TEMPORDER where OrderId = @ORDER_ID  ";
                    com1.Parameters.Add("@ORDER_ID", SqlDbType.BigInt).Value = ORDERId;
                    com1.ExecuteNonQuery();

                    com1.Parameters.Clear();
                    com1.CommandType = System.Data.CommandType.Text;
                    com1.CommandText = "delete FROM PROJECTORDER where OrderId = @ORDER_ID AND ProjectId=@P_Id ";
                    com1.Parameters.Add("@ORDER_ID", SqlDbType.BigInt).Value = ORDERId;
                    com1.Parameters.Add("@P_Id", SqlDbType.BigInt).Value = LblProjectId.Text;
                    com1.ExecuteNonQuery();

                    com1.Parameters.Clear();
                    com1.CommandType = System.Data.CommandType.Text;
                    com1.CommandText = "Select distinct * From TEMPORDER";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
                    DataSet dt1 = new DataSet();
                    adpt1.Fill(dt1);
                    if (dt1.Tables[0].Rows.Count > 0)
                    {
                        GrdViewOrderDetails.DataSource = dt1;
                        GrdViewOrderDetails.DataBind();

                        if (ORDERId.ToString() == LabelID.Text)
                        {
                            GridViewEditOrder.DataSource = null;
                            GridViewEditOrder.DataBind();
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        GrdViewOrderDetails.DataSource = dt1;
                        GrdViewOrderDetails.DataBind();

                        GridViewEditOrder.DataSource = null;
                        GridViewEditOrder.DataBind();
                    }
                }
                else
                {
                    com1.Parameters.Clear();
                    com1.CommandType = System.Data.CommandType.Text;
                    com1.CommandText = "delete FROM PROJECTORDER where OrderId = @ORDER_ID AND ProjectId=@P_Id ";
                    com1.Parameters.Add("@ORDER_ID", SqlDbType.BigInt).Value = ORDERId;
                    com1.Parameters.Add("@P_Id", SqlDbType.BigInt).Value = LblProjectId.Text;
                    com1.ExecuteNonQuery();

                    com1.Parameters.Clear();
                    com1.CommandType = System.Data.CommandType.Text;
                    com1.CommandText = "SELECT DISTINCT O.OrderId,O.OrderDate,O.Taxes,O.OrderTotal,O.Penalty,O.Incentive,O.Status FROM ORDERS O,PROJECTS,PROJECTORDER WHERE O.OrderId=PROJECTORDER.OrderId AND PROJECTS.ProjectId=PROJECTORDER.ProjectId AND PROJECTS.ProjectId= '" + LblProjectId.Text + "'";

                    SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
                    DataSet dt1 = new DataSet();
                    adpt1.Fill(dt1);
                    if (dt1.Tables[0].Rows.Count > 0)
                    {
                        GrdViewOrderDetails.DataSource = dt1;
                        GrdViewOrderDetails.DataBind();

                        GridViewEditOrder.DataSource = null;
                        GridViewEditOrder.DataBind();
                    }
                    else
                    {
                        GrdViewOrderDetails.DataSource = dt1;
                        GrdViewOrderDetails.DataBind();

                        GridViewEditOrder.DataSource = null;
                        GridViewEditOrder.DataBind();
                    }
                }

                con1.Close();
                // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('ORDER Deleted Successfully!!');", true);
                return;
            }
        }

        protected void Bound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "style.backgroundColor='LightBlue'");
            e.Row.Attributes.Add("onmouseout", "style.backgroundColor='#fbfbfb'");
        }

        protected void LBtnAPV_Click(object sender, EventArgs e)
        {
            if (LblProjectId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Project Name');", true);
                return;
            }

            else
            {
                LBtnAPV.BackColor = System.Drawing.Color.SkyBlue;
                LBtnAPV.BorderColor = System.Drawing.Color.Black;
                LBtnReport.BackColor = LBtnOverview.BackColor = LBtnOrderList.BackColor = LBtnUserList.BackColor = LBtnBasicInfo.BackColor = LBtnNewTask.BackColor = System.Drawing.Color.Empty;
                LBtnReport.BorderColor = LBtnOverview.BorderColor = LBtnOrderList.BorderColor = LBtnUserList.BorderColor = LBtnBasicInfo.BorderColor = LBtnNewTask.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.Red;
                LBtnReport.ForeColor = System.Drawing.Color.RoyalBlue;

                NewTask.Visible = false;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                BottomButtonDiv.Visible = false;
                divReport.Visible = false;

                divAPV.Visible = true;
                BindAPV();
            }
        }

        protected void BtnAddAPV_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
            BtnSaveAPV.Visible = false;
            BtnUpdateAPV.Visible = false;
            ClearCheckBoxList();
        }

        protected void BtnSaveAPV_Click(object sender, EventArgs e)
        {
            SaveAPV();
            ClearCheckBoxList();
            BindAPV();
        }

        protected void SaveAPV()
        {
            if (string.IsNullOrEmpty(txtsag.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Fill Sag Field');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                SqlCommand com = new SqlCommand();
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_Insert_APV";
                com.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = lblCustid.Text;
                com.Parameters.Add("@SAG", System.Data.SqlDbType.NVarChar).Value = txtsag.Text;
                com.Parameters.Add("@KUNDE", System.Data.SqlDbType.NVarChar).Value = txtKunde.Text;
                com.Parameters.Add("@KONTAKT", System.Data.SqlDbType.Decimal).Value = txtKontakt.Text;
                com.Parameters.Add("@TELEFON", System.Data.SqlDbType.Decimal).Value = txtTelefon.Text;
                com.Parameters.Add("@COMPLATIONDATE", System.Data.SqlDbType.Date).Value = DateTime.Now;

                //for (int i = 0; i < ChkLstStillads1.Items.Count - 1; i++)
                // {
                if (ChkLstStillads1.Items[0].Selected)
                {
                    //string var = string.Empty;
                    //var += ChkLstStillads1.Items[0].Text.ToString();
                    com.Parameters.Add("@FACADE", System.Data.SqlDbType.Bit).Value = 1;
                    //com.Parameters.AddWithValue("@FACADE", var);
                }
                else
                {
                    com.Parameters.Add("@FACADE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChkLstStillads1.Items[1].Selected)
                {
                    com.Parameters.Add("@VINDUER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@VINDUER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChkLstStillads1.Items[2].Selected)
                {
                    com.Parameters.Add("@TAG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TAG", System.Data.SqlDbType.Bit).Value = 0;
                }
                // }

                // for (int i = 0; i < ChkLstStillads2.Items.Count - 1; i++)
                // {
                if (ChkLstStillads2.Items[0].Selected)
                {
                    com.Parameters.Add("@GADE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GADE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChkLstStillads2.Items[1].Selected)
                {
                    com.Parameters.Add("@GÅRD", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GÅRD", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChkLstStillads2.Items[2].Selected)
                {
                    com.Parameters.Add("@STILLADS_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@STILLADS_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < ChklstStilladssystem.Items.Count - 1; i++)
                //{
                if (ChklstStilladssystem.Items[0].Selected)
                {
                    com.Parameters.Add("@EIGHT", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@EIGHT", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstStilladssystem.Items[1].Selected)
                {
                    com.Parameters.Add("@HAKI_RAM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HAKI_RAM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstStilladssystem.Items[2].Selected)
                {
                    com.Parameters.Add("@HAKI_MUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HAKI_MUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < ChklstLastklasse.Items.Count - 1; i++)
                //{
                if (ChklstLastklasse.Items[0].Selected)
                {
                    com.Parameters.Add("@KI_3_200KG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@KI_3_200KG", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstLastklasse.Items[1].Selected)
                {
                    com.Parameters.Add("@KI_5_450KG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@KI_5_450KG", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstLastklasse.Items[2].Selected)
                {
                    string var = string.Empty;
                    var += ChklstLastklasse.Items[2].Text.ToString();
                    com.Parameters.AddWithValue("@LASTKLASSE", var);
                    //com.Parameters.Add("@LASTKLASSE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LASTKLASSE", System.Data.SqlDbType.NVarChar).Value = "";
                }
                //}

                //for (int i = 0; i < ChklstFastgørelser.Items.Count - 1; i++)
                //{
                if (ChklstFastgørelser.Items[0].Selected)
                {
                    com.Parameters.Add("@MUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstFastgørelser.Items[1].Selected)
                {
                    com.Parameters.Add("@TRÆ", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRÆ", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstFastgørelser.Items[2].Selected)
                {
                    com.Parameters.Add("@BETON", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BETON", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstFastgørelser.Items[3].Selected)
                {
                    com.Parameters.Add("@FASTGØRELSER_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@FASTGØRELSER_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < ChklstStrømforsyning.Items.Count - 1; i++)
                //{
                if (ChklstStrømforsyning.Items[0].Selected)
                {
                    com.Parameters.Add("@BYGGESTRØM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BYGGESTRØM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstStrømforsyning.Items[1].Selected)
                {
                    com.Parameters.Add("@GENERATOR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GENERATOR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstStrømforsyning.Items[2].Selected)
                {
                    com.Parameters.Add("@STRØMFORSYNING_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@STRØMFORSYNING_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < ChklstTran_materiel.Items.Count - 1; i++)
                //{
                if (ChklstTran_materiel.Items[0].Selected)
                {
                    com.Parameters.Add("@LASTBIL", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LASTBIL", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[1].Selected)
                {
                    com.Parameters.Add("@LILLE_BIL", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LILLE_BIL", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[2].Selected)
                {
                    com.Parameters.Add("@TRUCK", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRUCK", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[3].Selected)
                {
                    com.Parameters.Add("@TRÆKVOGN", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRÆKVOGN", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[4].Selected)
                {
                    com.Parameters.Add("@ALURULLEVOGNE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@ALURULLEVOGNE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[5].Selected)
                {
                    com.Parameters.Add("@TRANSPORT_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRANSPORT_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < Chklst_værnemidler.Items.Count - 1; i++)
                //{
                if (Chklst_værnemidler.Items[0].Selected)
                {
                    com.Parameters.Add("@SELE_LINE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@SELE_LINE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_værnemidler.Items[1].Selected)
                {
                    com.Parameters.Add("@MASKE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MASKE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_værnemidler.Items[2].Selected)
                {
                    com.Parameters.Add("@HØREVÆRM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HØREVÆRM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_værnemidler.Items[3].Selected)
                {
                    com.Parameters.Add("@BRILLER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BRILLER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_værnemidler.Items[4].Selected)
                {
                    com.Parameters.Add("@VÆRNEMIDLER_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@VÆRNEMIDLER_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < Chklst_Velfærdsforanstaltninger.Items.Count - 1; i++)
                //{
                if (Chklst_Velfærdsforanstaltninger.Items[0].Selected)
                {
                    com.Parameters.Add("@DELESKUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@DELESKUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_Velfærdsforanstaltninger.Items[1].Selected)
                {
                    com.Parameters.Add("@MANGLER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MANGLER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_Velfærdsforanstaltninger.Items[2].Selected)
                {
                    com.Parameters.Add("@ADGANG_TIL_TOILET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@ADGANG_TIL_TOILET", System.Data.SqlDbType.Bit).Value = 0;
                }
                // }

                com.Parameters.Add("@NOTE", System.Data.SqlDbType.NVarChar).Value = "";

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }
        }

        public void ClearCheckBoxList()
        {
            txtsag.Text = "";
            txtKunde.Text = "";
            txtKontakt.Text = "";
            txtTelefon.Text = "";
            txtSjak.Text = "";

            int count = ChkLstStillads1.Items.Count;
            for (int i = 0; i < count; i++)
            {
                if (ChkLstStillads1.Items[i].Selected == true)
                {
                    ChkLstStillads1.Items[i].Selected = false;
                }
            }

            int count1 = ChkLstStillads2.Items.Count;
            for (int i = 0; i < count1; i++)
            {
                if (ChkLstStillads2.Items[i].Selected == true)
                {
                    ChkLstStillads2.Items[i].Selected = false;
                }
            }

            int count2 = ChklstStilladssystem.Items.Count;
            for (int i = 0; i < count2; i++)
            {
                if (ChklstStilladssystem.Items[i].Selected == true)
                {
                    ChklstStilladssystem.Items[i].Selected = false;
                }
            }

            int count3 = ChklstLastklasse.Items.Count;
            for (int i = 0; i < count3; i++)
            {
                if (ChklstLastklasse.Items[i].Selected == true)
                {
                    ChklstLastklasse.Items[i].Selected = false;
                }
            }

            int count4 = ChklstFastgørelser.Items.Count;
            for (int i = 0; i < count4; i++)
            {
                if (ChklstFastgørelser.Items[i].Selected == true)
                {
                    ChklstFastgørelser.Items[i].Selected = false;
                }
            }

            int count5 = ChklstStrømforsyning.Items.Count;
            for (int i = 0; i < count5; i++)
            {
                if (ChklstStrømforsyning.Items[i].Selected == true)
                {
                    ChklstStrømforsyning.Items[i].Selected = false;
                }
            }

            int count6 = ChklstTran_materiel.Items.Count;
            for (int i = 0; i < count6; i++)
            {
                if (ChklstTran_materiel.Items[i].Selected == true)
                {
                    ChklstTran_materiel.Items[i].Selected = false;
                }
            }

            int count7 = Chklst_værnemidler.Items.Count;
            for (int i = 0; i < count7; i++)
            {
                if (Chklst_værnemidler.Items[i].Selected == true)
                {
                    Chklst_værnemidler.Items[i].Selected = false;
                }
            }

            int count8 = Chklst_Velfærdsforanstaltninger.Items.Count;
            for (int i = 0; i < count8; i++)
            {
                if (Chklst_Velfærdsforanstaltninger.Items[i].Selected == true)
                {
                    Chklst_Velfærdsforanstaltninger.Items[i].Selected = false;
                }
            }
        }

        protected void BindAPV()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "Select Distinct APVID, Sag, Kunde, Kontakt, Telefon, ComplationDate from APV Where CID=@I_CUSTID";
            com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustid.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                DivAPV_Header.Visible = true;
                GridViewAPV.DataSource = dt;
                GridViewAPV.DataBind();
            }
            else
            {
                GridViewAPV.DataSource = null;
                GridViewAPV.DataBind();
                DivAPV_Header.Visible = false;
            }
            con.Close();
        }

        protected void BtnUpdateAPV_Click(object sender, EventArgs e)
        {
            UpdateAPV();
            ClearCheckBoxList();
        }

        protected void UpdateAPV()
        {
            if (string.IsNullOrEmpty(txtsag.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Fill Sag Field');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                SqlCommand com = new SqlCommand();
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_Update_APV";
                com.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = lblCustid.Text;
                com.Parameters.Add("@I_APVID", System.Data.SqlDbType.BigInt).Value = lblSimpleAPVID.Text;
                com.Parameters.Add("@SAG", System.Data.SqlDbType.NVarChar).Value = txtsag.Text;
                com.Parameters.Add("@KUNDE", System.Data.SqlDbType.NVarChar).Value = txtKunde.Text;
                com.Parameters.Add("@KONTAKT", System.Data.SqlDbType.Decimal).Value = txtKontakt.Text;
                com.Parameters.Add("@TELEFON", System.Data.SqlDbType.Decimal).Value = txtTelefon.Text;
                com.Parameters.Add("@COMPLATIONDATE", System.Data.SqlDbType.Date).Value = DateTime.Now;

                //for (int i = 0; i < ChkLstStillads1.Items.Count - 1; i++)
                // {
                if (ChkLstStillads1.Items[0].Selected)
                {
                    //string var = string.Empty;
                    //var += ChkLstStillads1.Items[0].Text.ToString();
                    com.Parameters.Add("@FACADE", System.Data.SqlDbType.Bit).Value = 1;
                    //com.Parameters.AddWithValue("@FACADE", var);
                }
                else
                {
                    com.Parameters.Add("@FACADE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChkLstStillads1.Items[1].Selected)
                {
                    com.Parameters.Add("@VINDUER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@VINDUER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChkLstStillads1.Items[2].Selected)
                {
                    com.Parameters.Add("@TAG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TAG", System.Data.SqlDbType.Bit).Value = 0;
                }
                // }

                // for (int i = 0; i < ChkLstStillads2.Items.Count - 1; i++)
                // {
                if (ChkLstStillads2.Items[0].Selected)
                {
                    com.Parameters.Add("@GADE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GADE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChkLstStillads2.Items[1].Selected)
                {
                    com.Parameters.Add("@GÅRD", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GÅRD", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChkLstStillads2.Items[2].Selected)
                {
                    com.Parameters.Add("@STILLADS_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@STILLADS_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < ChklstStilladssystem.Items.Count - 1; i++)
                //{
                if (ChklstStilladssystem.Items[0].Selected)
                {
                    com.Parameters.Add("@EIGHT", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@EIGHT", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstStilladssystem.Items[1].Selected)
                {
                    com.Parameters.Add("@HAKI_RAM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HAKI_RAM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstStilladssystem.Items[2].Selected)
                {
                    com.Parameters.Add("@HAKI_MUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HAKI_MUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < ChklstLastklasse.Items.Count - 1; i++)
                //{
                if (ChklstLastklasse.Items[0].Selected)
                {
                    com.Parameters.Add("@KI_3_200KG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@KI_3_200KG", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstLastklasse.Items[1].Selected)
                {
                    com.Parameters.Add("@KI_5_450KG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@KI_5_450KG", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstLastklasse.Items[2].Selected)
                {
                    string var = string.Empty;
                    var += ChklstLastklasse.Items[2].Text.ToString();
                    com.Parameters.AddWithValue("@LASTKLASSE", var);
                    //com.Parameters.Add("@LASTKLASSE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LASTKLASSE", System.Data.SqlDbType.NVarChar).Value = "";
                }
                //}

                //for (int i = 0; i < ChklstFastgørelser.Items.Count - 1; i++)
                //{
                if (ChklstFastgørelser.Items[0].Selected)
                {
                    com.Parameters.Add("@MUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstFastgørelser.Items[1].Selected)
                {
                    com.Parameters.Add("@TRÆ", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRÆ", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstFastgørelser.Items[2].Selected)
                {
                    com.Parameters.Add("@BETON", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BETON", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstFastgørelser.Items[3].Selected)
                {
                    com.Parameters.Add("@FASTGØRELSER_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@FASTGØRELSER_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < ChklstStrømforsyning.Items.Count - 1; i++)
                //{
                if (ChklstStrømforsyning.Items[0].Selected)
                {
                    com.Parameters.Add("@BYGGESTRØM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BYGGESTRØM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstStrømforsyning.Items[1].Selected)
                {
                    com.Parameters.Add("@GENERATOR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GENERATOR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstStrømforsyning.Items[2].Selected)
                {
                    com.Parameters.Add("@STRØMFORSYNING_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@STRØMFORSYNING_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < ChklstTran_materiel.Items.Count - 1; i++)
                //{
                if (ChklstTran_materiel.Items[0].Selected)
                {
                    com.Parameters.Add("@LASTBIL", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LASTBIL", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[1].Selected)
                {
                    com.Parameters.Add("@LILLE_BIL", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LILLE_BIL", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[2].Selected)
                {
                    com.Parameters.Add("@TRUCK", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRUCK", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[3].Selected)
                {
                    com.Parameters.Add("@TRÆKVOGN", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRÆKVOGN", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[4].Selected)
                {
                    com.Parameters.Add("@ALURULLEVOGNE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@ALURULLEVOGNE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (ChklstTran_materiel.Items[5].Selected)
                {
                    com.Parameters.Add("@TRANSPORT_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRANSPORT_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < Chklst_værnemidler.Items.Count - 1; i++)
                //{
                if (Chklst_værnemidler.Items[0].Selected)
                {
                    com.Parameters.Add("@SELE_LINE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@SELE_LINE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_værnemidler.Items[1].Selected)
                {
                    com.Parameters.Add("@MASKE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MASKE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_værnemidler.Items[2].Selected)
                {
                    com.Parameters.Add("@HØREVÆRM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HØREVÆRM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_værnemidler.Items[3].Selected)
                {
                    com.Parameters.Add("@BRILLER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BRILLER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_værnemidler.Items[4].Selected)
                {
                    com.Parameters.Add("@VÆRNEMIDLER_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@VÆRNEMIDLER_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < Chklst_Velfærdsforanstaltninger.Items.Count - 1; i++)
                //{
                if (Chklst_Velfærdsforanstaltninger.Items[0].Selected)
                {
                    com.Parameters.Add("@DELESKUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@DELESKUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_Velfærdsforanstaltninger.Items[1].Selected)
                {
                    com.Parameters.Add("@MANGLER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MANGLER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (Chklst_Velfærdsforanstaltninger.Items[2].Selected)
                {
                    com.Parameters.Add("@ADGANG_TIL_TOILET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@ADGANG_TIL_TOILET", System.Data.SqlDbType.Bit).Value = 0;
                }
                // }

                com.Parameters.Add("@NOTE", System.Data.SqlDbType.NVarChar).Value = "";

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }

        }

        protected void LBtViewAPV_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string APVID = grdrow.Cells[1].Text;

            lblSimpleAPVID.Text = APVID.ToString(); // FOr Updating  APV Per Customer---

            //BtnSaveActiveAPV.Visible = false;
            //BtnUpdateActiveAPV.Visible = true;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select DIstinct * from APV where APVID = @I_APVID ", con);

            //String var = lbAktiveAPV.Text;
            cmd.Parameters.Add("@I_APVID", System.Data.SqlDbType.BigInt).Value = APVID;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);

            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                txtsag.Text = dt.Tables[0].Rows[0]["Sag"].ToString();
                txtKunde.Text = dt.Tables[0].Rows[0]["Kunde"].ToString();
                txtKontakt.Text = dt.Tables[0].Rows[0]["Kontakt"].ToString();
                txtTelefon.Text = dt.Tables[0].Rows[0]["Telefon"].ToString();
                //txtKunde.Text = dt.Tables[0].Rows[0]["ComplationDate"].ToString();

                string Facade = dt.Tables[0].Rows[0]["Facade"].ToString();

                if (Facade.ToString() != null)
                {
                    if (Facade.ToString() == "True")
                    {
                        ChkLstStillads1.Items[0].Selected = true;
                    }

                }
                string Vinduer = dt.Tables[0].Rows[0]["Vinduer"].ToString();

                if (Vinduer.ToString() != null)
                {
                    if (Vinduer.ToString() == "True")
                    {
                        ChkLstStillads1.Items[1].Selected = true;
                    }
                }
                string Tag = dt.Tables[0].Rows[0]["Tag"].ToString();

                if (Tag.ToString() != null)
                {
                    if (Tag.ToString() == "True")
                    {
                        ChkLstStillads1.Items[2].Selected = true;
                    }
                }

                string Gade = dt.Tables[0].Rows[0]["Gade"].ToString();

                if (Gade.ToString() != null)
                {
                    if (Gade.ToString() == "True")
                    {
                        ChkLstStillads2.Items[0].Selected = true;
                    }
                }
                string Gård = dt.Tables[0].Rows[0]["Gård"].ToString();

                if (Gård.ToString() != null)
                {
                    if (Gård.ToString() == "True")
                    {
                        ChkLstStillads2.Items[1].Selected = true;
                    }
                }
                string Stillads_Andet = dt.Tables[0].Rows[0]["Stillads_Andet"].ToString();

                if (Stillads_Andet.ToString() != null)
                {
                    if (Stillads_Andet.ToString() == "True")
                    {
                        ChkLstStillads2.Items[2].Selected = true;
                    }
                }

                string Eight = dt.Tables[0].Rows[0]["Eight"].ToString();

                if (Eight.ToString() != null)
                {
                    if (Eight.ToString() == "True")
                    {
                        ChklstStilladssystem.Items[0].Selected = true;
                    }
                }
                string Haki_ram = dt.Tables[0].Rows[0]["Haki ram"].ToString();

                if (Haki_ram.ToString() != null)
                {
                    if (Haki_ram.ToString() == "True")
                    {
                        ChklstStilladssystem.Items[1].Selected = true;
                    }
                }
                string Haki_mur = dt.Tables[0].Rows[0]["Haki mur"].ToString();

                if (Haki_mur.ToString() != null)
                {
                    if (Haki_mur.ToString() == "True")
                    {
                        ChklstStilladssystem.Items[2].Selected = true;
                    }

                }

                string Kl_3_200Kg_m2 = dt.Tables[0].Rows[0]["Kl.3-200Kg/m2"].ToString();

                if (Kl_3_200Kg_m2.ToString() != null)
                {
                    if (Kl_3_200Kg_m2.ToString() == "True")
                    {
                        ChklstLastklasse.Items[0].Selected = true;
                    }
                }

                string Kl_5_450Kg_m2 = dt.Tables[0].Rows[0]["Kl.5-450 Kg/m2"].ToString();

                if (Kl_5_450Kg_m2.ToString() != null)
                {
                    if (Kl_5_450Kg_m2.ToString() == "True")
                    {
                        ChklstLastklasse.Items[1].Selected = true;
                    }
                }

                string Lastklasse = dt.Tables[0].Rows[0]["Lastklasse:"].ToString();

                if (Lastklasse.ToString() != null)
                {
                    if (Lastklasse.ToString() == "True")
                    {
                        ChklstLastklasse.Items[2].Selected = true;
                    }
                }


                string Mur = dt.Tables[0].Rows[0]["Mur"].ToString();

                if (Mur.ToString() != null)
                {
                    if (Mur.ToString() == "True")
                    {
                        ChklstFastgørelser.Items[0].Selected = true;
                    }
                }
                string Træ = dt.Tables[0].Rows[0]["Træ"].ToString();

                if (Træ.ToString() != null)
                {
                    if (Træ.ToString() == "True")
                    {
                        ChklstFastgørelser.Items[1].Selected = true;
                    }
                }
                string Beton = dt.Tables[0].Rows[0]["Beton"].ToString();

                if (Beton.ToString() != null)
                {
                    if (Beton.ToString() == "True")
                    {
                        ChklstFastgørelser.Items[2].Selected = true;
                    }
                }
                string Fastgørelser_Andet = dt.Tables[0].Rows[0]["Fastgørelser_Andet"].ToString();

                if (Fastgørelser_Andet.ToString() != null)
                {
                    if (Fastgørelser_Andet.ToString() == "True")
                    {
                        ChklstFastgørelser.Items[3].Selected = true;
                    }
                }


                string Byggestrøm = dt.Tables[0].Rows[0]["Byggestrøm"].ToString();

                if (Byggestrøm.ToString() != null)
                {
                    if (Byggestrøm.ToString() == "True")
                    {
                        ChklstStrømforsyning.Items[0].Selected = true;
                    }
                }
                string Generator = dt.Tables[0].Rows[0]["Generator"].ToString();

                if (Generator.ToString() != null)
                {
                    if (Generator.ToString() == "True")
                    {
                        ChklstStrømforsyning.Items[1].Selected = true;
                    }
                }
                string Strømforsyning_Andet = dt.Tables[0].Rows[0]["Strømforsyning_Andet"].ToString();

                if (Strømforsyning_Andet.ToString() != null)
                {
                    if (Strømforsyning_Andet.ToString() == "True")
                    {
                        ChklstStrømforsyning.Items[2].Selected = true;
                    }
                }


                string Lastbil = dt.Tables[0].Rows[0]["Lastbil"].ToString();

                if (Lastbil.ToString() != null)
                {
                    if (Lastbil.ToString() == "True")
                    {
                        ChklstTran_materiel.Items[0].Selected = true;
                    }
                }
                string Lille_bil = dt.Tables[0].Rows[0]["Lille bil"].ToString();

                if (Lille_bil.ToString() != null)
                {
                    if (Lille_bil.ToString() == "True")
                    {
                        ChklstTran_materiel.Items[1].Selected = true;
                    }
                }
                string Truck = dt.Tables[0].Rows[0]["Truck"].ToString();

                if (Truck.ToString() != null)
                {
                    if (Truck.ToString() == "True")
                    {
                        ChklstTran_materiel.Items[2].Selected = true;
                    }
                }
                string Trækvogn = dt.Tables[0].Rows[0]["Trækvogn"].ToString();

                if (Trækvogn.ToString() != null)
                {
                    if (Trækvogn.ToString() == "True")
                    {
                        ChklstTran_materiel.Items[3].Selected = true;
                    }
                }
                string Alurullevogne = dt.Tables[0].Rows[0]["Alurullevogne"].ToString();

                if (Alurullevogne.ToString() != null)
                {
                    if (Alurullevogne.ToString() == "True")
                    {
                        ChklstTran_materiel.Items[4].Selected = true;
                    }
                }
                string Transport_Andet = dt.Tables[0].Rows[0]["Transport_Andet"].ToString();

                if (Transport_Andet.ToString() != null)
                {
                    if (Transport_Andet.ToString() == "True")
                    {
                        ChklstTran_materiel.Items[5].Selected = true;
                    }
                }

                string Sele_line = dt.Tables[0].Rows[0]["Sele/line"].ToString();

                if (Sele_line.ToString() != null)
                {
                    if (Sele_line.ToString() == "True")
                    {
                        Chklst_værnemidler.Items[0].Selected = true;
                    }
                }
                string Maske = dt.Tables[0].Rows[0]["Maske"].ToString();

                if (Maske.ToString() != null)
                {
                    if (Maske.ToString() == "True")
                    {
                        Chklst_værnemidler.Items[1].Selected = true;
                    }
                }
                string Høreværn = dt.Tables[0].Rows[0]["Høreværn"].ToString();

                if (Høreværn.ToString() != null)
                {
                    if (Høreværn.ToString() == "True")
                    {
                        Chklst_værnemidler.Items[2].Selected = true;
                    }
                }
                string Briller = dt.Tables[0].Rows[0]["Briller"].ToString();

                if (Briller.ToString() != null)
                {
                    if (Briller.ToString() == "True")
                    {
                        Chklst_værnemidler.Items[3].Selected = true;
                    }
                }
                string værnemidler_Andet = dt.Tables[0].Rows[0]["værnemidler_Andet"].ToString();

                if (værnemidler_Andet.ToString() != null)
                {
                    if (værnemidler_Andet.ToString() == "True")
                    {
                        Chklst_værnemidler.Items[4].Selected = true;
                    }
                }
                string Deleskur = dt.Tables[0].Rows[0]["Deleskur"].ToString();

                if (Deleskur.ToString() != null)
                {
                    if (Deleskur.ToString() == "True")
                    {
                        Chklst_Velfærdsforanstaltninger.Items[0].Selected = true;
                    }
                }
                string Mangler = dt.Tables[0].Rows[0]["Mangler"].ToString();

                if (Mangler.ToString() != null)
                {
                    if (Mangler.ToString() == "True")
                    {
                        Chklst_Velfærdsforanstaltninger.Items[1].Selected = true;
                    }
                }
                string Adgang_til_toilet = dt.Tables[0].Rows[0]["Adgang til toilet"].ToString();

                if (Adgang_til_toilet.ToString() != null)
                {
                    if (Adgang_til_toilet.ToString() == "True")
                    {
                        Chklst_Velfærdsforanstaltninger.Items[2].Selected = true;
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
            BtnSaveAPV.Visible = false;
            BtnUpdateAPV.Visible = false;
        }

        protected void LbtnDeleteAPV_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string APVID = grdrow.Cells[1].Text;


            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("Delete  From APV where APVID = @I_APVID AND CID= @CID ", con);

            cmd.Parameters.Add("@I_APVID", System.Data.SqlDbType.BigInt).Value = APVID;
            cmd.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = lblCustid.Text; //LblCustId.Text;

            cmd.ExecuteNonQuery();
            con.Close();
            BindAPV();

            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('APV Deleted SUccessfully!!');", true);
            return;
        }

        protected void LBtnReport_Click(object sender, EventArgs e)
        {

            {
                if (LblProjectId.Text == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Project Name');", true);
                    return;
                }

                else
                {
                    LBtnReport.BackColor = System.Drawing.Color.SkyBlue;
                    LBtnReport.BorderColor = System.Drawing.Color.Black;

                    LBtnOverview.BackColor = LBtnNewTask.BackColor = LBtnOrderList.BackColor = LBtnUserList.BackColor = LBtnBasicInfo.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                    LBtnOverview.BorderColor = LBtnNewTask.BorderColor = LBtnOrderList.BorderColor = LBtnUserList.BorderColor = LBtnBasicInfo.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                    LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                    LBtnReport.ForeColor = System.Drawing.Color.Red;
                    LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;
                    LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
                    LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
                    LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                    LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;

                    divReport.Visible = true;
                    divOverView.Visible = false;
                    divBasicInfo.Visible = false;
                    divContactInfo.Visible = false;
                    divOrder.Visible = false;
                    NewTask.Visible = false;
                    //CallJavaScript(sender);
                    BottomButtonDiv.Visible = false;
                    divAPV.Visible = false;
                    ChartForReport();
                }
            }
        }

        protected void ChartForReport()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            string query2 = "Select Distinct DATEPART(MM,PROJECTS.ProjectDate) AS QuoteMonth,ORDERS.OrderTotal from HAS,ORDERS,QUOTATIONDATA ,PROJECTS ,PROJECTORDER  Where ORDERS.OrderId=HAS.OrderId and HAS.ItemCode=QUOTATIONDATA.ItemCode and HAS.TemDate=QUOTATIONDATA.TempDate and HAS.DateTime=ORDERS.OrderDate and PROJECTS.ProjectId=PROJECTORDER.ProjectId and  ORDERS.OrderId=PROJECTORDER.OrderId and QUOTATIONDATA.TEMPType='false' and  PROJECTS.ProjectId ='" + LblProjectId.Text + "'";
            string query3 = "Select sum(ContributionFees) from SALARYREPORT where ProjectId='" + LblProjectId.Text + "'";
            SqlCommand cmd2 = new SqlCommand(query2, con);
            cmd2.ExecuteNonQuery();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            da2.Fill(dt2);

            SqlCommand cmd3 = new SqlCommand(query3, con);
            cmd3.ExecuteNonQuery();
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            da3.Fill(dt3);

            int sum2 = Convert.ToInt32(dt3.Compute("SUM(Column1)", string.Empty));
            int sum = Convert.ToInt32(dt2.Compute("SUM(OrderTotal)", string.Empty));

            System.Data.DataColumn newColumn = new System.Data.DataColumn("Total", typeof(System.Int32));
            newColumn.DefaultValue = sum + sum2;
            dt2.Columns.Add(newColumn);

            if (dt2.Rows.Count > 0)
            {
                Chart1.DataSource = dt2.Rows;

                Chart1.Series["Count"].XValueMember = "QuoteMonth";

                Chart1.Series["Count"].YValueMembers = "Total";

                Chart1.Series["Count"].IsValueShownAsLabel = true;

                Chart1.Series["Count"].ChartType = SeriesChartType.Bar;

                Chart1.Titles[1].Text = "Bar Chart";

                Chart1.DataBind();
                lblProjectCost.Text = Convert.ToString(sum);
                lblSalaryCost.Text = Convert.ToString(sum2);
            }

            con.Close();
        }
    }
}
