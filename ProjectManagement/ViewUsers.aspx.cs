﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace ProjectManagement
{
    public partial class ViewUsers : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {

            Label1.Text = Session["username"].ToString();

            if (!IsPostBack)
            {
               

                SqlConnection con = new SqlConnection(cs);
                con.Open();


                SqlCommand com = new SqlCommand("select * from USERS ", con);

                SqlDataAdapter adpt = new SqlDataAdapter(com);

                DataTable dt = new DataTable();

                adpt.Fill(dt);

                GridViewUser.DataSource = dt;

                GridViewUser.DataBind();

            }
        }

        protected void BtnAddUser_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateUser.aspx");
        }
    }
}