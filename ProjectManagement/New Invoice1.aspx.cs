﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using AjaxControlToolkit;
using System.Net;
using System.Net.Mail;
using System.Text;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml;

namespace ProjectManagement
{
    public partial class New_Invoice1 : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        int InvoiceNo;
        //int total = 0;
        float total1 = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            imguser.ImageUrl = "~/image/pmglogo1.jpg";

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    Global.ChkFlagg = 0;

                    TxtSearch.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label2.Text = Session["username"].ToString();

                    txtInvoiceNo.Text = Request.QueryString["InvoiceNO"];
                    LblCustId.Text = Request.QueryString["CustId"];
                    lblOrderId.Text = Request.QueryString["OrderId"];

                    SqlConnection SQLCON = new SqlConnection(cs);
                    SQLCON.Open();
                    SqlCommand SQLCMD = new SqlCommand();
                    SQLCMD.Connection = SQLCON;
                    SQLCMD.CommandType = System.Data.CommandType.Text;
                    SQLCMD.Parameters.Clear();

                    SQLCMD.CommandText = "Delete From TempInvoice";
                    SQLCMD.ExecuteNonQuery();
                    SQLCMD.Parameters.Clear();
                    SQLCON.Close();

                    if (LblCustId.Text != "")
                    {
                        BindCustomerList();
                        ddListCustomer.Text = LblCustId.Text;
                        CommanDataUpload();
                        ViewCustomerInfo();
                        BindOrderList();
                        ddListCustomer.Enabled = false;
                        ddListCustOrder.Enabled = true;
                        //ChkServicePayable.Enabled = false;
                    }
                    else
                    {
                        if (txtInvoiceNo.Text == "")
                        {
                            if (lblOrderId.Text != "")
                            {
                                BindOrderListFromOrder();
                                BindCustomerList();
                                ddListCustomer.Text = LblCustId.Text;
                                ddListCustomer.Enabled = false;
                                ddListCustOrder.Enabled = false;
                                CommanDataUpload();
                            }
                            else
                            {
                                BindCustomerList();
                                CommanDataUpload();
                                //ChkServicePayable.Enabled = false;
                            }
                        }
                        else
                        {
                            Global.FLAGG = 0;
                            ChkServicePayable.Enabled = true;
                            BtnSaveInvoice.Visible = false;
                            BtnUpdateInvoice.Visible = true;
                            ddListCustomer.Enabled = false;
                            ddListCustOrder.Enabled = false;
                            DataForUpdate();
                        }
                    }

                }
            }

            //if (IsPostBack && FileUpload01.PostedFile != null)
            //{
            //    if (FileUpload01.PostedFile.FileName.Length > 0)
            //    {
            //        String ext = System.IO.Path.GetExtension(FileUpload01.FileName);

            //        if (ext != ".jpg" && ext != ".png" && ext != ".gif" && ext != ".jpeg" && ext != ".jpe" && ext != ".jfif" && ext != ".gif" && ext != ".bmp" && ext != ".tit" && ext != ".jpeg2000" && ext != ".exif" && ext != ".gif" && ext != ".ppm" && ext != ".pgm" && ext != ".pbm" && ext != ".pnm" && ext != ".bpg" && ext != ".psd" && ext != ".tif" && ext != ".pspimage" && ext != ".yuv" && ext != ".thm" && ext != ".ai" && ext != ".drw" && ext != ".eps" && ext != ".ps" && ext != ".svg")
            //        {
            //            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Only Images Allowed.');", true);
            //            return;
            //        }
            //        else
            //        {
            //            string filename = Path.GetFileName(FileUpload01.PostedFile.FileName);

            //            int length = FileUpload01.PostedFile.ContentLength;

            //            byte[] imgbyte = new byte[length];

            //            HttpPostedFile img = FileUpload01.PostedFile;

            //            img.InputStream.Read(imgbyte, 0, length);

            //            DeleteImageData();

            //            FileUpload01.SaveAs(Server.MapPath("~/image/") + Path.GetFileName(FileUpload01.FileName));

            //            imguser.ImageUrl = "~/image/" + Path.GetFileName(FileUpload01.FileName);

            //            //string filePath = "~/image/" + Path.GetFileName(FileUpload01.FileName);

            //            //string newfile = Server.MapPath(filePath);

            //            using (SqlConnection con = new SqlConnection(cs))
            //            {
            //                using (SqlCommand cmd = new SqlCommand())
            //                {
            //                    //cmd.CommandText = "insert into INVOICELOGO (InvoiceNo,Image) values(@id,@Name)";
            //                    cmd.CommandText = "insert into imagedetails (imageid,imagename,imagedata) values(@ImageId,@Name,@Data)";

            //                    cmd.Parameters.AddWithValue("@ImageId", txtInvoiceNo.Text);
            //                    //cmd.Parameters.AddWithValue("@id", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
            //                    cmd.Parameters.AddWithValue("@Name", filename);
            //                    // cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarBinary).Value = filename;
            //                    //cmd.Parameters.AddWithValue("@Data", imgbyte);
            //                    cmd.Parameters.AddWithValue("@Data", System.Data.SqlDbType.VarBinary).Value = imgbyte;
            //                    //cmd.Parameters.AddWithValue("@ImagePath", System.Data.SqlDbType.NVarChar).Value = newfile;,ImagePath,@ImagePath

            //                    cmd.Connection = con;

            //                    con.Open();

            //                    cmd.ExecuteNonQuery();

            //                    con.Close();
            //                }
            //            }
            //            imguser.ImageUrl = "~/ImageHandler.ashx?roll_no=" + txtInvoiceNo.Text;
            //            //imguser.ImageUrl = "~/ShowImage.ashx?id=" + txtInvoiceNo.Text;
            //        }
            //    }
            //}
        }

        private void DeleteImageData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Delete from imagedetails Where imageid= '" + txtInvoiceNo.Text + "'";
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            con.Close();
        }

        public void CommanDataUpload()
        {
            Global.FLAGG = 0;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT  ISNULL(MAX(InvoiceNo),0) FROM INVOICE";
            InvoiceNo = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
            cmd.Parameters.Clear();

            txtInvoiceNo.Text = InvoiceNo.ToString();

            cmd.CommandText = "Delete FROM TempInvoice";
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

            con.Close();
        }

        public void DataForUpdate()
        {
            //imguser.ImageUrl = "~/ImageHandler.ashx?roll_no=" + txtInvoiceNo.Text;

            TrPaid.Visible = true;
            BindCustomerList();

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select Distinct * From Invoice Where InvoiceNo='" + txtInvoiceNo.Text + "' ";

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                gridViewOrder.DataSource = dt;
                gridViewOrder.DataBind();

                gridViewtest.DataSource = dt;
                gridViewtest.DataBind();

                txtInvoiceDate.Text = dt.Tables[0].Rows[0]["InvoiceStartDate"].ToString();
                txtDueDate.Text = dt.Tables[0].Rows[0]["InvoiceDueDate"].ToString();
                ddListCustomer.Text = dt.Tables[0].Rows[0]["CustomerId"].ToString();

                LblCustId.Text = ddListCustomer.Text;
                BindOrderList();
                ViewCustomerInfo();

                ddListCustOrder.Text = dt.Tables[0].Rows[0]["OrderId"].ToString();
                LblTotalPrice.Text = dt.Tables[0].Rows[0]["TotalInvoiceCost"].ToString();
                txtPercentage.Text = dt.Tables[0].Rows[0]["PercentagePayable"].ToString();
                lblPaidAmount.Text = dt.Tables[0].Rows[0]["AmountPayable"].ToString();
                lblPaidPercentage.Text = dt.Tables[0].Rows[0]["PercentagePayable"].ToString();
                LbltxtPercentage.Text = dt.Tables[0].Rows[0]["PercentagePayable"].ToString();

                lblTotalPercentage.Text = dt.Tables[0].Rows[0]["PercentagePayable"].ToString();
                lblTotalAmount.Text = dt.Tables[0].Rows[0]["AmountPayable"].ToString();
            }
            else
            {
                gridViewOrder.DataSource = dt;
                gridViewOrder.DataBind();

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No data For Updation');", true);
                return;
            }

            con.Close();
        }

        protected void lnkAddLine_Click(object sender, EventArgs e)
        {
            UpdateOrderData();
            resetTask();
            Global.FLAGG = 1;
        }

        public void resetTask()
        {
            //int InvoiceNo;
            int subInNo;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            //cmd.CommandType = System.Data.CommandType.Text;
            //cmd.CommandText = "SELECT  ISNULL(MAX(InvoiceNo),0) FROM INVOCE";
            //InvoiceNo = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
            //cmd.Parameters.Clear();

            //cmd.CommandText = "Delete FROM TempInvoice";
            //cmd.ExecuteNonQuery();
            //cmd.Parameters.Clear();

            cmd.CommandText = "SELECT ISNULL(MAX(SubInNo),0) + 1 FROM TempInvoice where InvoiceNo=" + txtInvoiceNo.Text + "";
            subInNo = Convert.ToInt32(cmd.ExecuteScalar());
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_InsertTempInvoice";
            cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
            cmd.Parameters.Add("@SUBINNO", System.Data.SqlDbType.BigInt).Value = subInNo;
            cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = 0;
            cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = 0;
            cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = 0;
            cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = 0;
            cmd.Parameters.Add("@TAX1", System.Data.SqlDbType.Decimal).Value = 0;
            cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = 0;
            cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 1;

            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT Distinct * FROM TempInvoice";
            SqlDataAdapter dtask = new SqlDataAdapter(cmd);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridViewOrder.DataSource = dttask;
            gridViewOrder.DataBind();

            con.Close();
        }

        public void BindCustomerList()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select * from CUSTOMER";
            SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            DataSet dt1 = new DataSet();
            adpt1.Fill(dt1);
            if (dt1.Tables[0].Rows.Count > 0)
            {
                ddListCustomer.DataSource = dt1;
                ddListCustomer.DataTextField = "CustName";
                ddListCustomer.DataValueField = "CustId";
                ddListCustomer.DataBind();
                ddListCustomer.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Vælg-", "0"));
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert(' Is No Customer Here');", true);
                return;
            }
            con.Close();
        }

        protected void ddListCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddListCustomer.SelectedItem.Text == "-Vælg-")
            {
                LblCustId.Text = "0";
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Customer');", true);
                return;
            }
            else
            {
                LblCustId.Text = ddListCustomer.SelectedValue;
                BindOrderList();
                ViewCustomerInfo();
            }
        }

        public void BindOrderList()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_OrderByName";
            com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.NVarChar).Value = LblCustId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                ddListCustOrder.DataSource = dt;
                ddListCustOrder.DataTextField = "OrderName";
                ddListCustOrder.DataValueField = "OrderId";
                ddListCustOrder.DataBind();
                BtnSaveInvoice.Enabled = true;
                ddListCustOrder.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Vælg-", "0"));
            }
            else
            {
                ddListCustOrder.DataSource = dt;
                //ddListCustOrder.DataTextField = "OrderTotal";
                //ddListCustOrder.DataValueField = "OrderId";
                ddListCustOrder.DataBind();
                BtnSaveInvoice.Enabled = false;
                ddListCustOrder.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Vælg-", "0"));
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Not Created any Order');", true);
                return;
            }
            con.Close();
        }

        public void BindOrderListFromOrder()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_OrderByOrderID";
            com.Parameters.Add("@I_ORDERID", System.Data.SqlDbType.BigInt).Value = lblOrderId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                LblCustId.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                txtInvoiceDate.Text = dt.Tables[0].Rows[0]["OrderDate"].ToString();
                ddListCustOrder.DataSource = dt;
                ddListCustOrder.DataTextField = "OrderName";
                ddListCustOrder.DataValueField = "OrderId";
                ddListCustOrder.DataBind();

                //ddListCustOrder.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Select-", "0"));

                OrderIndexChange();
            }
            else
            {
                ddListCustOrder.DataSource = dt;
                ddListCustOrder.DataBind();
                ddListCustOrder.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Vælg-", "0"));
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Not Created any Order');", true);
                return;
            }
            con.Close();
        }

        protected void ddListCustOrder_SelectedIndexChanged(object sender, EventArgs e)/* Want To Delete Or Make Here Empty TempInvoice Table ...MG*/
        {
            OrderIndexChange();
        }

        public void OrderIndexChange()
        {
            Global.FLAGG = 0;

            txtInvoiceDate.Visible = true;
            txtDueDate.Visible = true;
            txtInvoiceNo.Visible = true;

            if (ddListCustOrder.SelectedValue != "-Vælg-")
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "select distinct * from HAS,ORDERS,QUOTATIONDATA where ORDERS.OrderId=HAS.OrderId and HAS.ItemCode=QUOTATIONDATA.ItemCode and HAS.DateTime=ORDERS.OrderDate and has.DateTime=QUOTATIONDATA.QuaDate and QUOTATIONDATA.TEMPType='false' and ORDERS.OrderId= '" + ddListCustOrder.SelectedValue + "'";

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                adpt.Fill(dt);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    txtInvoiceDate.Text = dt.Tables[0].Rows[0]["OrderDate"].ToString();
                    LblTotalPrice.Text = dt.Tables[0].Rows[0]["OrderTotal"].ToString();
                    gridViewOrder.DataSource = dt;
                    gridViewOrder.DataBind();
                }
                else
                {
                    gridViewOrder.DataSource = dt;
                    gridViewOrder.DataBind();

                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No Data In Order');", true);
                    return;
                }
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Delete From TempInvoice";
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                con.Close();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Order');", true);
                return;
            }
        }

        protected void txtPrice_TextChanged(object sender, EventArgs e)
        {
            if (txtDueDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Due Date');", true);
                return;
            }
            else
            {
                decimal TOTAL = 0;

                DateTime inTime = Convert.ToDateTime(txtInvoiceDate.Text);
                DateTime outTime = Convert.ToDateTime(txtDueDate.Text);
                // TimeSpan i = outTime.Subtract(inTime);
                // this.LabelTimeDiff.Text = i.ToString();

                if (inTime <= outTime)
                {
                    TimeSpan i = outTime.Subtract(inTime);
                    this.LabelTimeDiff.Text = i.ToString();
                }
                else
                {
                    //TimeSpan i = inTime.Subtract(outTime);
                    //this.LabelTimeDiff.Text = i.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Date Which is Greater than Start Date');", true);
                    return;
                }

                decimal dec = Convert.ToDecimal(TimeSpan.Parse((LabelTimeDiff.Text)).TotalDays);

                //LabelTimeDiff.Text = Convert.ToString(dec);
                TextBox CalculateTotal = (TextBox)sender;
                GridViewRow gvr = (GridViewRow)CalculateTotal.NamingContainer;

                //Label lblID = ((Label)gvr.Cells[0].FindControl("lblID"));
                //Label lblQuatationId = ((Label)gvr.Cells[0].FindControl("lblQuatationId"));
                //Label lblItemCode = ((Label)gvr.Cells[1].FindControl("lblItemCode"));
                //TextBox txtEmptyBox = ((TextBox)gvr.Cells[2].FindControl("txtEmptyBox"));
                // Label lblItemName = ((Label)gvr.Cells[2].FindControl("lblItemName"));
                //Label lblItemQua = ((Label)gvr.Cells[3].FindControl("lblItemQua"));
                Label txtItemprice = ((Label)gvr.Cells[3].FindControl("txtPrice"));
                Label txtGrossTotal = ((Label)gvr.Cells[5].FindControl("txtGrossTotal"));

                if (txtGrossTotal.Text != "")
                {
                    TOTAL = Convert.ToDecimal((txtGrossTotal.Text).ToString());
                    //id = Convert.ToInt32(lblID.Text);
                }
                else
                {
                    TOTAL = 0;
                }
                if (TOTAL.ToString() != "")
                {
                    decimal Price = Convert.ToDecimal(txtItemprice.Text);
                    long qty = Convert.ToInt64(dec);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    total = total + pricqty;
                    //lblTot1.Text = total.ToString();
                    txtGrossTotal.Text = total.ToString();
                }
                if (LblTotalPrice.Text != "")
                {
                    LblTotalPrice.Text = Convert.ToString(((Convert.ToDecimal(LblTotalPrice.Text)) - TOTAL) + (Convert.ToDecimal(txtGrossTotal.Text)));

                    //LBLResCost.Text = lblTotalCost1.Text;
                    //TxtResCost.Text = lblTotalCost1.Text;
                    //lblSubTotal.Text = lblTotalCost1.Text;
                }

            }
        }

        protected void gridViewOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            foreach (GridViewRow rows in gridViewOrder.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblInvoiceNo = (rows.Cells[0].FindControl("lblInvoiceNo") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubInNo = (rows.Cells[0].FindControl("lblSubInNo") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.DropDownList DDListItem = (rows.Cells[1].FindControl("DDListItem") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtItemName = (rows.Cells[1].FindControl("txtItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtDescription = (rows.Cells[2].FindControl("txtDescription") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtPrice = (rows.Cells[3].FindControl("txtPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtGrossTotal = (rows.Cells[4].FindControl("txtGrossTotal") as System.Web.UI.WebControls.Label);

                    if (txtItemName.Text == "")
                    {
                        DDListItem.Visible = true;
                        txtItemName.Visible = false;
                        txtPrice.Enabled = true;
                    }
                    else
                    {
                        DDListItem.Visible = false;
                        txtItemName.Visible = true;
                        txtItemName.Enabled = false;
                        txtPrice.Enabled = true;
                    }
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "select Distinct * from STOCKSITEM Where Type= 'Service'";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                    DataSet dt1 = new DataSet();
                    adpt1.Fill(dt1);
                    if (dt1.Tables[0].Rows.Count > 0)
                    {
                        DDListItem.DataSource = dt1;
                        DDListItem.DataTextField = "ItemName";
                        DDListItem.DataValueField = "ItemCode";
                        DDListItem.DataBind();
                        DDListItem.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Vælg-", "0"));
                    }
                    else
                    {

                    }
                }
            }
            //if (Global.Updateflag == 1)
            //{
            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //         total1 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "TotalPrice"));
            //         lblServiceTotal.Text = total1.ToString();
            //    }
            //}
            con.Close();
        }

        public void UpdateOrderData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            foreach (GridViewRow rows in gridViewOrder.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblInvoiceNo = (rows.Cells[0].FindControl("lblInvoiceNo") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubInNo = (rows.Cells[0].FindControl("lblSubInNo") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.DropDownList DDListItem = (rows.Cells[1].FindControl("DDListItem") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtItemName = (rows.Cells[1].FindControl("txtItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtDescription = (rows.Cells[2].FindControl("txtDescription") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtPrice = (rows.Cells[3].FindControl("txtPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblServiceType = (rows.Cells[4].FindControl("lblServiceType") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtGrossTotal = (rows.Cells[4].FindControl("txtGrossTotal") as System.Web.UI.WebControls.Label);

                    // System.Web.UI.WebControls.Label lblSubInNo = (gridViewOrder.Rows[0].FindControl("lblSubInNo") as System.Web.UI.WebControls.Label);
                    if (Global.FLAGG == 0)
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_InsertTempInvoice";
                        cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                        cmd.Parameters.Add("@SUBINNO", System.Data.SqlDbType.BigInt).Value = lblSubInNo.Text;
                        cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                        cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtItemName.Text;
                        cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtPrice.Text;
                        cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = txtQuantity.Text;
                        cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = 0;
                        cmd.Parameters.Add("@TAX1", System.Data.SqlDbType.Decimal).Value = 0;
                        cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = txtGrossTotal.Text;
                        cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = lblServiceType.Text;

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    if (Global.FLAGG == 1)
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_UpdateTempInvoice";

                        cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                        cmd.Parameters.Add("@SUBINNO", System.Data.SqlDbType.BigInt).Value = lblSubInNo.Text;
                        cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                        cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtItemName.Text;
                        cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtPrice.Text;
                        cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = txtQuantity.Text;
                        cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = 0;
                        cmd.Parameters.Add("@TAX1", System.Data.SqlDbType.Decimal).Value = 0;
                        cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = txtGrossTotal.Text;
                        cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = lblServiceType.Text;

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                    }
                }
            }
            con.Close();
        }

        protected void DDListItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            if (txtInvoiceDate.Text == "" || txtDueDate.Text == "")
            {
                DropDownList DDListItem = (DropDownList)sender;
                GridViewRow gridViewOrder = ((GridViewRow)DDListItem.Parent.Parent);
                DDListItem.SelectedValue = "0";

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper date');", true);
                return;
            }
            else
            {
                DropDownList DDListItem = (DropDownList)sender;
                GridViewRow gridViewOrder = ((GridViewRow)DDListItem.Parent.Parent);

                Label txtPrice = (Label)gridViewOrder.FindControl("txtPrice");
                Label txtItemName = (Label)gridViewOrder.FindControl("txtItemName");
                Label txtGrossTotal = (Label)gridViewOrder.FindControl("txtGrossTotal");
                Label lblItemCode = (Label)gridViewOrder.FindControl("lblItemCode");
                Label lblSubInNo = (Label)gridViewOrder.FindControl("lblSubInNo");

                Label lblServiceType = (Label)gridViewOrder.FindControl("lblServiceType");

                com.CommandText = "select Price, ItemFees from Stocksitem where ItemCode= " + DDListItem.SelectedValue;
                SqlDataAdapter adpt = new SqlDataAdapter(com);
                DataTable dt = new DataTable();
                adpt.Fill(dt);

                txtPrice.Text = dt.Rows[0]["ItemFees"].ToString();
                lblItemCode.Text = DDListItem.SelectedValue;

                txtItemName.Text = DDListItem.SelectedItem.Text;
                DDListItem.Visible = false;
                txtItemName.Visible = true;

                decimal TOTAL = 0;
                DateTime inTime = Convert.ToDateTime(txtInvoiceDate.Text);
                DateTime outTime = Convert.ToDateTime(txtDueDate.Text);
                if (inTime <= outTime)
                {
                    TimeSpan i = outTime.Subtract(inTime);
                    this.LabelTimeDiff.Text = i.ToString();
                }
                else
                {
                    //TimeSpan i = inTime.Subtract(outTime);
                    //this.LabelTimeDiff.Text = i.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Duedate Must be Greater than Order Date');", true);
                    return;
                }
                decimal dec = Convert.ToDecimal(TimeSpan.Parse((LabelTimeDiff.Text)).TotalDays);

                if (txtGrossTotal.Text != "")
                {
                    TOTAL = Convert.ToDecimal((txtGrossTotal.Text).ToString());
                }
                else
                {
                    TOTAL = 0;
                }
                if (TOTAL.ToString() != "")
                {
                    decimal Price = Convert.ToDecimal(txtPrice.Text);
                    long qty = Convert.ToInt64(dec);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    total = total + pricqty;
                    txtGrossTotal.Text = total.ToString();

                    if (Global.Updateflag == 1)
                    {
                        lblServiceTotal.Text = Convert.ToString((Convert.ToDecimal(lblServiceTotal.Text)) + (Convert.ToDecimal(txtGrossTotal.Text)));
                    }
                }
                if (BtnSaveInvoice.Visible == true)
                {
                    decimal TOTALPercentage = 0;

                    if (LblTotalPrice.Text != "")
                    {
                        LblTotalPrice.Text = Convert.ToString(((Convert.ToDecimal(LblTotalPrice.Text)) - TOTAL) + (Convert.ToDecimal(txtGrossTotal.Text)));
                    }
                    else
                    {
                        LblTotalPrice.Text = txtGrossTotal.Text;
                    }
                    if (lblPercent.Text != "0.00")
                    {
                        decimal Price = Convert.ToDecimal(LblTotalPrice.Text);
                        decimal Percentage = Convert.ToDecimal(lblPercent.Text);

                        TOTALPercentage = (Price * Percentage) / 100;

                        LblPayableAmt.Text = TOTALPercentage.ToString();
                    }
                }
                dt.Clear();
                com.Parameters.Clear();

                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertTempInvoice";
                com.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                com.Parameters.Add("@SUBINNO", System.Data.SqlDbType.BigInt).Value = lblSubInNo.Text;
                com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtItemName.Text;
                com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtPrice.Text;
                com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = 0;
                com.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = 0;
                com.Parameters.Add("@TAX1", System.Data.SqlDbType.Decimal).Value = 0;
                com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = txtGrossTotal.Text;
                com.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = lblServiceType.Text;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
            }
            con.Close();
        }

        protected void txtDueDate_TextChanged(object sender, EventArgs e)
        {
            if (ChkPayable.Checked == true)
            {
                PercentageCalculate();
            }
            if (ChkServicePayable.Checked == true)
            {
                ServicePaybleData();
            }

        }

        protected void BtnSaveInvoice_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == "" || ddListCustOrder.SelectedValue == "" || txtInvoiceDate.Text == "" || txtDueDate.Text == "" || ddListCustOrder.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Fill All Information');", true);
            }
            else
            {
                gridViewOrder.Visible = false;
                gridViewtest.Visible = true;

                BtnSaveInvoice.Visible = false;
                BtnViewInvoice.Visible = true;
                BtnEditInvoice.Visible = false;
                btnpdf.Visible = true;
                ChkPayable.Enabled = false;
                ChkServicePayable.Enabled = false;

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;

                foreach (GridViewRow rows in gridViewOrder.Rows)
                {
                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.Label lblInvoiceNo = (rows.Cells[0].FindControl("lblInvoiceNo") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblSubInNo = (rows.Cells[0].FindControl("lblSubInNo") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.DropDownList DDListItem = (rows.Cells[1].FindControl("DDListItem") as System.Web.UI.WebControls.DropDownList);
                        System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtItemName = (rows.Cells[1].FindControl("txtItemName") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtDescription = (rows.Cells[2].FindControl("txtDescription") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtPrice = (rows.Cells[3].FindControl("txtPrice") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblServiceType = (rows.Cells[4].FindControl("lblServiceType") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtGrossTotal = (rows.Cells[4].FindControl("txtGrossTotal") as System.Web.UI.WebControls.Label);

                        if (LblTotalPrice.Text != "")
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.CommandText = "sp_InsertINVOICE";
                            cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                            cmd.Parameters.Add("@SUBINNO", System.Data.SqlDbType.BigInt).Value = lblSubInNo.Text;
                            cmd.Parameters.Add("@CUSTOMERID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                            cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = ddListCustOrder.SelectedValue;
                            cmd.Parameters.Add("@INVOICESTARTDATE", System.Data.SqlDbType.DateTime).Value = txtInvoiceDate.Text;
                            if (txtDueDate.Text == "")
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Date');", true);
                                return;
                            }
                            else
                            {
                                cmd.Parameters.Add("@INVOICEDUETDATE", System.Data.SqlDbType.DateTime).Value = txtDueDate.Text;
                            }
                            cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                            cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtItemName.Text;
                            cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtPrice.Text;
                            cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = txtQuantity.Text;
                            cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = 0;
                            cmd.Parameters.Add("@TAX1", System.Data.SqlDbType.Decimal).Value = 0;
                            cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = txtGrossTotal.Text;
                            cmd.Parameters.Add("@TOTALINVOICECOST", System.Data.SqlDbType.Decimal).Value = LblTotalPrice.Text;
                            if (txtPercentage.Text == "")
                            {
                                cmd.Parameters.Add("@PERCENTAGEPAYABLE", System.Data.SqlDbType.Decimal).Value = 0;
                            }
                            else
                            {
                                cmd.Parameters.Add("@PERCENTAGEPAYABLE", System.Data.SqlDbType.Decimal).Value = txtPercentage.Text;
                            }
                            cmd.Parameters.Add("@AMOUNTPAYABLE", System.Data.SqlDbType.Decimal).Value = LblPayableAmt.Text;
                            cmd.Parameters.Add("@SERVICEPAYABLE", System.Data.SqlDbType.Decimal).Value = lblServiceTotal.Text;

                            cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = lblServiceType.Text;

                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                    }
                }
                cmd.Parameters.Clear();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Delete From TempInvoice Where InvoiceNo ='" + txtInvoiceNo.Text + "'";
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_InsertINVOICEDATA";
                cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                cmd.Parameters.Add("@CUSTOMERID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = ddListCustOrder.SelectedValue;
                cmd.Parameters.Add("@INVOICESTARTDATE", System.Data.SqlDbType.DateTime).Value = txtInvoiceDate.Text;
                cmd.Parameters.Add("@INVOICEDUETDATE", System.Data.SqlDbType.DateTime).Value = txtDueDate.Text;
                cmd.Parameters.Add("@TOTALINVOICECOST", System.Data.SqlDbType.Decimal).Value = LblTotalPrice.Text;

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                lblInvoiceNo1.Text = txtInvoiceNo.Text;
                txtInvoiceNo.Visible = false;
                lblInvoiceNo1.Visible = true;

                lblInvoiceDate1.Text = txtInvoiceDate.Text;
                txtInvoiceDate.Visible = false;
                lblInvoiceDate1.Visible = true;

                lblDueDate1.Text = txtDueDate.Text;
                txtDueDate.Visible = false;
                lblDueDate1.Visible = true;

                lblCustomerName.Text = ddListCustomer.SelectedItem.Text;
                lblCustomerOrder.Text = ddListCustOrder.SelectedItem.Text;
                ddListCustOrder.Visible = false;
                ddListCustomer.Visible = false;
                lblCustomerOrder.Visible = true;
                lblCustomerName.Visible = true;

                LbltxtPercentage.Text = txtPercentage.Text;
                txtPercentage.Visible = false;
                LbltxtPercentage.Visible = true;

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT  ISNULL(MAX(InvoiceNo),0) FROM INVOICE";
                InvoiceNo = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
                cmd.Parameters.Clear();
                txtInvoiceNo.Text = InvoiceNo.ToString();

                custinfo();
                con.Close();


                Global.FLAGG = 0;
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Invoice Generated Successfully');", true);
                return;
            }
        }

        protected void BtnViewInvoice_Click(object sender, EventArgs e)
        {
            Response.Redirect("Invoice.aspx");
        }

        protected void BtnUpdateInvoice_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == "" || LblCustId.Text == "0" || ddListCustOrder.SelectedValue == "" || txtInvoiceDate.Text == "" || txtDueDate.Text == "" || ddListCustOrder.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Fill All Information');", true);
            }
            else
            {
                gridViewtest.Visible = true;
                gridViewOrder.Visible = false;

                BtnUpdateInvoice.Visible = false;
                BtnViewInvoice.Visible = true;
                BtnEditInvoice.Visible = false;
                btnpdf.Visible = true;

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;

                //if (Global.Updateflag == 0)
                //{
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Delete From INVOICE Where InvoiceNo ='" + txtInvoiceNo.Text + "'";
                cmd.ExecuteScalar();
                cmd.Parameters.Clear();

                foreach (GridViewRow rows in gridViewOrder.Rows)
                {
                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.Label lblInvoiceNo = (rows.Cells[0].FindControl("lblInvoiceNo") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblSubInNo = (rows.Cells[0].FindControl("lblSubInNo") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.DropDownList DDListItem = (rows.Cells[1].FindControl("DDListItem") as System.Web.UI.WebControls.DropDownList);
                        System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtItemName = (rows.Cells[1].FindControl("txtItemName") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtDescription = (rows.Cells[2].FindControl("txtDescription") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtPrice = (rows.Cells[3].FindControl("txtPrice") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblServiceType = (rows.Cells[4].FindControl("lblServiceType") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label txtGrossTotal = (rows.Cells[4].FindControl("txtGrossTotal") as System.Web.UI.WebControls.Label);

                        if (LblTotalPrice.Text != "")
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.CommandText = "sp_InsertINVOICE";
                            cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                            cmd.Parameters.Add("@SUBINNO", System.Data.SqlDbType.BigInt).Value = lblSubInNo.Text;
                            cmd.Parameters.Add("@CUSTOMERID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                            cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = ddListCustOrder.SelectedValue;
                            cmd.Parameters.Add("@INVOICESTARTDATE", System.Data.SqlDbType.DateTime).Value = txtInvoiceDate.Text;
                            cmd.Parameters.Add("@INVOICEDUETDATE", System.Data.SqlDbType.DateTime).Value = txtDueDate.Text;
                            cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                            cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtItemName.Text;
                            cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtPrice.Text;
                            cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = txtQuantity.Text;
                            cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = 0;
                            cmd.Parameters.Add("@TAX1", System.Data.SqlDbType.Decimal).Value = 0;
                            cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = txtGrossTotal.Text;
                            cmd.Parameters.Add("@TOTALINVOICECOST", System.Data.SqlDbType.Decimal).Value = LblTotalPrice.Text;
                            cmd.Parameters.Add("@PERCENTAGEPAYABLE", System.Data.SqlDbType.Decimal).Value = lblTotalPercentage.Text;
                            cmd.Parameters.Add("@AMOUNTPAYABLE", System.Data.SqlDbType.Decimal).Value = lblTotalAmount.Text;
                            cmd.Parameters.Add("@SERVICEPAYABLE", System.Data.SqlDbType.Decimal).Value = lblServiceTotal.Text;
                            cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = lblServiceType.Text;


                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                        else
                        {

                        }
                    }
                }
                //}
                //else
                //{
                //    foreach (GridViewRow rows in gridViewOrder.Rows)
                //    {
                //        if (rows.RowType == DataControlRowType.DataRow)
                //        {
                //            System.Web.UI.WebControls.Label lblInvoiceNo = (rows.Cells[0].FindControl("lblInvoiceNo") as System.Web.UI.WebControls.Label);
                //            System.Web.UI.WebControls.Label lblSubInNo = (rows.Cells[0].FindControl("lblSubInNo") as System.Web.UI.WebControls.Label);
                //            System.Web.UI.WebControls.DropDownList DDListItem = (rows.Cells[1].FindControl("DDListItem") as System.Web.UI.WebControls.DropDownList);
                //            System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                //            System.Web.UI.WebControls.Label txtItemName = (rows.Cells[1].FindControl("txtItemName") as System.Web.UI.WebControls.Label);
                //            System.Web.UI.WebControls.Label txtDescription = (rows.Cells[2].FindControl("txtDescription") as System.Web.UI.WebControls.Label);
                //            System.Web.UI.WebControls.Label txtPrice = (rows.Cells[3].FindControl("txtPrice") as System.Web.UI.WebControls.Label);
                //            System.Web.UI.WebControls.Label txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.Label);
                //            System.Web.UI.WebControls.Label txtGrossTotal = (rows.Cells[4].FindControl("txtGrossTotal") as System.Web.UI.WebControls.Label);

                //            if (LblTotalPrice.Text != "")
                //            {
                //                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //                cmd.CommandText = "sp_UpdateInvoice";
                //                cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                //                //cmd.Parameters.Add("@SUBINNO", System.Data.SqlDbType.BigInt).Value = lblSubInNo.Text;
                //                cmd.Parameters.Add("@CUSTOMERID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                //                cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = ddListCustOrder.SelectedValue;
                //                cmd.Parameters.Add("@INVOICESTARTDATE", System.Data.SqlDbType.DateTime).Value = txtInvoiceDate.Text;
                //                cmd.Parameters.Add("@INVOICEDUETDATE", System.Data.SqlDbType.DateTime).Value = txtDueDate.Text;
                //                cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                //                cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtItemName.Text;
                //                cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtPrice.Text;
                //                cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = txtQuantity.Text;
                //                cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = 0;
                //                cmd.Parameters.Add("@TAX1", System.Data.SqlDbType.Decimal).Value = 0;
                //                cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = txtGrossTotal.Text;
                //                cmd.Parameters.Add("@TOTALINVOICECOST", System.Data.SqlDbType.Decimal).Value = LblTotalPrice.Text;
                //                cmd.Parameters.Add("@PERCENTAGEPAYABLE", System.Data.SqlDbType.Decimal).Value = lblTotalPercentage.Text;
                //                cmd.Parameters.Add("@AMOUNTPAYABLE", System.Data.SqlDbType.Decimal).Value = lblTotalAmount.Text;
                //                cmd.Parameters.Add("@SERVICEPAYABLE", System.Data.SqlDbType.Decimal).Value = lblServiceTotal.Text;

                //                cmd.ExecuteNonQuery();
                //                cmd.Parameters.Clear();
                //            }
                //            else
                //            {

                //            }
                //        }
                //    }
                //}
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_UpdateInvoiceData";
                cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                cmd.Parameters.Add("@CUSTOMERID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = ddListCustOrder.SelectedValue;
                cmd.Parameters.Add("@INVOICESTARTDATE", System.Data.SqlDbType.DateTime).Value = txtInvoiceDate.Text;
                cmd.Parameters.Add("@INVOICEDUETDATE", System.Data.SqlDbType.DateTime).Value = txtDueDate.Text;
                cmd.Parameters.Add("@TOTALINVOICECOST", System.Data.SqlDbType.Decimal).Value = LblTotalPrice.Text;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                lblInvoiceNo1.Text = txtInvoiceNo.Text;

                cmd.Parameters.Clear();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Delete From TempInvoice Where InvoiceNo ='" + txtInvoiceNo.Text + "'";
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT  ISNULL(MAX(InvoiceNo),0) FROM INVOICE";
                InvoiceNo = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
                cmd.Parameters.Clear();
                txtInvoiceNo.Text = InvoiceNo.ToString();

                custinfo();

                con.Close();

                txtInvoiceNo.Visible = false;
                lblInvoiceNo1.Visible = true;

                lblInvoiceDate1.Text = txtInvoiceDate.Text;
                txtInvoiceDate.Visible = false;
                lblInvoiceDate1.Visible = true;

                lblDueDate1.Text = txtDueDate.Text;
                txtDueDate.Visible = false;
                lblDueDate1.Visible = true;

                lblCustomerName.Text = ddListCustomer.SelectedItem.Text;
                lblCustomerOrder.Text = ddListCustOrder.SelectedItem.Text;
                ddListCustOrder.Visible = false;
                ddListCustomer.Visible = false;
                lblCustomerOrder.Visible = true;
                lblCustomerName.Visible = true;

                LbltxtPercentage.Text = txtPercentage.Text;
                txtPercentage.Visible = false;
                LbltxtPercentage.Visible = true;

                Global.FLAGG = 0;
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Invoice Updated Successfully');", true);
                return;

            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void BtnPrint_Click(object sender, EventArgs e)
        {
            Shadowdiv.Visible = true;
            gridViewOrder.ShowHeader = true;

            //thead1.Visible = false;

            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>PrintGridDataAll();</script>");
        }

        private void DisableAjaxExtenders(Control parent)
        {
            for (int i = 0; i < parent.Controls.Count; i++)
            {
                if (parent.Controls[i].GetType().ToString().IndexOf("Extender") != -1 && parent.Controls[i].ID != null)
                {
                    parent.Controls.RemoveAt(i);
                    parent.Controls[i].Dispose();
                }
                if (parent.Controls[i].Controls.Count > 0)
                {
                    DisableAjaxExtenders(parent.Controls[i]);
                }
            }
        }

        protected void BtnEditInvoice_Click(object sender, EventArgs e)
        {
            gridViewtest.Visible = false;
            gridViewOrder.Visible = true;

            BtnViewInvoice.Visible = false;
            BtnEditInvoice.Visible = false;
            BtnSaveInvoice.Visible = true;
            btnpdf.Visible = false;

            // lblInvoiceNo1.Text = txtInvoiceNo.Text;
            txtInvoiceNo.Visible = true;
            lblInvoiceNo1.Visible = false;

            //lblInvoiceDate1.Text = txtInvoiceDate.Text;
            txtInvoiceDate.Visible = true;
            lblInvoiceDate1.Visible = false;

            //lblDueDate1.Text = txtDueDate.Text;
            txtDueDate.Visible = true;
            lblDueDate1.Visible = false;

            //lblCustomerName.Text = ddListCustomer.SelectedItem.Text;
            //blCustomerOrder.Text = ddListCustOrder.SelectedItem.Text;
            ddListCustOrder.Visible = true;
            ddListCustomer.Visible = true;
            lblCustomerOrder.Visible = false;
            lblCustomerName.Visible = false;

            //LbltxtPercentage.Text = txtPercentage.Text;
            txtPercentage.Visible = true;
            LbltxtPercentage.Visible = false;
        }

        public class ITextEvents : PdfPageEventHelper
        {

            // This is the contentbyte object of the writer
            PdfContentByte cb;

            // we will put the final number of pages in a template
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer
            BaseFont bf = null;

            // This keeps track of the creation time
            DateTime PrintTime = DateTime.Now;


            #region Fields
            private string _header;
            #endregion

            #region Properties
            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }
            #endregion


            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                    //handle exception here
                }
                catch (System.IO.IOException ioe)
                {
                    //handle exception here
                }
            }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnEndPage(writer, document);

                iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

                // Phrase p1Header = new Phrase("INVOICE", baseFontNormal);

                //Create PdfTable object
                PdfPTable pdfTab = new PdfPTable(3);

                ////We will have to create separate cells to include image logo and 2 separate strings
                ////Row 1
                iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/image/pmglogo1.jpg"));
                logo.ScaleAbsolute(180f, 50f);

                PdfPCell pdfCell1 = new PdfPCell();
                PdfPCell pdfCell2 = new PdfPCell();
                PdfPCell pdfCell3 = new PdfPCell(logo);

                String text = "Page " + writer.PageNumber + " of ";
                String address = "PMG Stilladser A/S-";
                String address1 = "Hundigevej 87-2670 Greve CVR-nr:34058008";
                String address2 = "Tif:33155515- Mail:info@pmg.dk";

                ////Add paging to header
                //{
                //    //cb.BeginText();
                //    //cb.SetFontAndSize(bf, 12);
                //    //cb.SetTextMatrix(document.PageSize.GetRight(155), document.PageSize.GetTop(45));
                //    //cb.ShowText(text);
                //    //cb.EndText();
                //    //float len = bf.GetWidthPoint(text, 12);
                //    ////Adds "12" in Page 1 of 12
                //    //cb.AddTemplate(headerTemplate, document.PageSize.GetRight(155) + len, document.PageSize.GetTop(45));
                //}
                //Add paging to footer
                {
                    cb.BeginText();
                    cb.SetFontAndSize(bf, 15);
                    cb.SetTextMatrix(document.PageSize.GetRight(370), document.PageSize.GetBottom(35));
                    cb.ShowText(address);
                    cb.EndText();
                    float len1 = bf.GetWidthPoint(address, 15);
                    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(370) + len1, document.PageSize.GetBottom(35));

                    cb.BeginText();
                    cb.SetFontAndSize(bf, 12);
                    cb.SetTextMatrix(document.PageSize.GetRight(422), document.PageSize.GetBottom(24));
                    cb.ShowText(address1);
                    cb.EndText();
                    float len2 = bf.GetWidthPoint(address1, 12);
                    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(422) + len2, document.PageSize.GetBottom(24));

                    cb.BeginText();
                    cb.SetFontAndSize(bf, 12);
                    cb.SetTextMatrix(document.PageSize.GetRight(390), document.PageSize.GetBottom(12));
                    cb.ShowText(address2);
                    cb.EndText();
                    float len3 = bf.GetWidthPoint(address2, 12);
                    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(390) + len3, document.PageSize.GetBottom(12));

                    cb.BeginText();
                    cb.SetFontAndSize(bf, 12);
                    cb.SetTextMatrix(document.PageSize.GetRight(105), document.PageSize.GetBottom(35));
                    cb.ShowText(text);
                    cb.EndText();
                    float len = bf.GetWidthPoint(text, 12);
                    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(105) + len, document.PageSize.GetBottom(35));
                }
                //Row 2
                //PdfPCell pdfCell4 = new PdfPCell(new Phrase("CERTIGOA ENTERPRISES", baseFontNormal));
                //Row 3

                PdfPCell pdfCell5 = new PdfPCell();
                PdfPCell pdfCell6 = new PdfPCell();
                PdfPCell pdfCell7 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));

                PdfPCell pdfCell8 = new PdfPCell();
                PdfPCell pdfCell9 = new PdfPCell();
                PdfPCell pdfCell10 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

                ////set the alignment of all three cells and set border to 0
                pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell3.HorizontalAlignment = Element.ALIGN_LEFT;
                //pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell7.HorizontalAlignment = Element.ALIGN_LEFT;
                pdfCell8.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell9.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell10.HorizontalAlignment = Element.ALIGN_LEFT;

                pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell3.VerticalAlignment = Element.ALIGN_LEFT;
                //pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
                pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell7.VerticalAlignment = Element.ALIGN_LEFT;
                pdfCell8.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell9.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell10.VerticalAlignment = Element.ALIGN_LEFT;

                //pdfCell4.Colspan = 3;
                //// pdfCell10.Colspan = 3;

                pdfCell1.Border = 0;
                pdfCell2.Border = 0;
                pdfCell3.Border = 0;
                //pdfCell4.Border = 0;
                pdfCell5.Border = 0;
                pdfCell6.Border = 0;
                pdfCell7.Border = 0;
                pdfCell8.Border = 0;
                pdfCell9.Border = 0;
                pdfCell10.Border = 0;

                ////add all three cells into PdfTable
                pdfTab.AddCell(pdfCell1);
                pdfTab.AddCell(pdfCell2);
                pdfTab.AddCell(pdfCell3);
                //pdfTab.AddCell(pdfCell4);
                pdfTab.AddCell(pdfCell5);
                pdfTab.AddCell(pdfCell6);
                pdfTab.AddCell(pdfCell7);
                pdfTab.AddCell(pdfCell8);
                pdfTab.AddCell(pdfCell9);
                pdfTab.AddCell(pdfCell10);

                pdfTab.TotalWidth = document.PageSize.Width - 80f;
                pdfTab.WidthPercentage = 70;


                ////call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                ////first param is start row. -1 indicates there is no end row and all the rows to be included to write
                ////Third and fourth param is x and y position to start writing
                pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                ////Move the pointer and draw line to separate header section from rest of page
                //cb.MoveTo(40, document.PageSize.Height - 100);
                //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                //cb.Stroke();

                //Move the pointer and draw line to separate footer section from rest of page
                cb.MoveTo(40, document.PageSize.GetBottom(50));
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
                cb.Stroke();
            }

            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

                footerTemplate.BeginText();
                footerTemplate.SetFontAndSize(bf, 12);
                footerTemplate.SetTextMatrix(0, 0);
                footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                footerTemplate.EndText();
            }

        }

        protected void txtPercentage_TextChanged(object sender, EventArgs e)
        {
            PercentageCalculate();
        }

        public void PercentageCalculate()
        {
            decimal TOTAL = 0;

            if (txtDueDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper date Which is Greater Than Start Date');", true);
                return;
            }
            else
            {
                if (txtPercentage.Text == "" || (Convert.ToDecimal(txtPercentage.Text) > 100))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Percentage');", true);
                    return;
                }
                else
                {
                    if (LbltxtPercentage.Text == "")
                    {
                        decimal Price = Convert.ToDecimal(LblTotalPrice.Text);
                        decimal Percentage = Convert.ToDecimal(txtPercentage.Text);

                        TOTAL = (Price * Percentage) / 100;

                        LblPayableAmt.Text = TOTAL.ToString();
                    }
                    else
                    {
                        if ((((Convert.ToDecimal(txtPercentage.Text))) + (Convert.ToDecimal(LbltxtPercentage.Text))) <= 100)
                        {

                            decimal Price = Convert.ToDecimal(LblTotalPrice.Text);
                            decimal Percentage = Convert.ToDecimal(txtPercentage.Text);

                            TOTAL = (Price * Percentage) / 100;

                            LblPayableAmt.Text = TOTAL.ToString();// Convert.ToString(((Convert.ToDecimal(LblPayableAmt.Text))) + (Convert.ToDecimal(TOTAL)));

                            //lblTotalPercentage.Text = Convert.ToString(((Convert.ToDecimal(txtPercentage.Text))) + (Convert.ToDecimal(LbltxtPercentage.Text)));

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Percentage. Total of  Percentage is Greater than 100%');", true);
                            return;
                        }
                    }
                }
            }
            LbltxtPercentage.Text = txtPercentage.Text;

            lblPercent.Text = txtPercentage.Text;

            lblTotalPercentage.Text = Convert.ToString(((Convert.ToDecimal(lblPaidPercentage.Text))) + (Convert.ToDecimal(lblPercent.Text)));
            lblTotalAmount.Text = Convert.ToString(((Convert.ToDecimal(lblPaidAmount.Text))) + (Convert.ToDecimal(LblPayableAmt.Text)));
        }

        protected void ChkPayable_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkPayable.Checked == true)
            {
                ChkServicePayable.Checked = false;

                Global.Updateflag = 0;
                txtPercentage.Visible = true;
                SpanPercentage.Visible = true;
                TrPercentage.Visible = true;
                TrServicePay.Visible = true;
                TrPaid.Visible = true;
                txtPercentage.Focus();

                if (BtnUpdateInvoice.Visible == true)
                {
                    CHkClick_Order();

                    if (Global.ChkFlagg == 1)
                    {
                        //Global.ChkFlagg = 0;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(txtDueDate.Text))
                        {

                        }
                        else
                        {
                            txtInvoiceDate.Text = txtDueDate.Text;
                            txtDueDate.Text = "";
                        }

                        Global.ChkFlagg = 1;
                        if (txtDueDate.Text == "")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Enter DueDate');", true);
                            return;
                        }
                    }
                }
                else
                {

                }
            }
            else
            {
                txtPercentage.Visible = false;
                SpanPercentage.Visible = false;
                TrPercentage.Visible = false;
                TrPaid.Visible = false;
            }
        }

        protected void ChkServicePayable_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkServicePayable.Checked == true)
            {
                Global.Updateflag = 1;

                TrServicePay.Visible = true;
                ChkPayable.Checked = false;
                TrPaid.Visible = true;
                txtPercentage.Visible = false;
                SpanPercentage.Visible = false;
                TrPercentage.Visible = true;

                if (BtnUpdateInvoice.Visible == true && Global.ChkFlagg == 0)
                {
                    if (string.IsNullOrEmpty(txtDueDate.Text))
                    {

                    }
                    else
                    {
                        txtInvoiceDate.Text = txtDueDate.Text;
                        txtDueDate.Text = "";
                    }


                    Global.ChkFlagg = 1;

                    if (txtDueDate.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Enter DueDate');", true);
                        return;
                    }
                }
                else
                {

                }
                if (txtDueDate.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper date Which is Greater Than Start Date');", true);
                    return;
                }
                else
                {
                    ServicePaybleData();
                }
            }
            else
            {

            }
        }

        public void ServicePaybleData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            decimal TOTAL = 0;
            DateTime inTime = Convert.ToDateTime(txtInvoiceDate.Text);
            DateTime outTime = Convert.ToDateTime(txtDueDate.Text);
            if (inTime < outTime)
            {
                BtnSaveInvoice.Enabled = true;
                BtnUpdateInvoice.Enabled = true;
                TimeSpan i = outTime.Subtract(inTime);
                this.LabelTimeDiff.Text = i.ToString();
            }
            else
            {
                //TimeSpan i = inTime.Subtract(outTime);
                //this.LabelTimeDiff.Text = i.ToString();
                BtnSaveInvoice.Enabled = false;
                BtnUpdateInvoice.Enabled = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper date Which is Greater Than Start Date');", true);
                return;
            }
            decimal dec = Convert.ToDecimal(TimeSpan.Parse((LabelTimeDiff.Text)).TotalDays);

            foreach (GridViewRow rows in gridViewOrder.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblInvoiceNo = (rows.Cells[0].FindControl("lblInvoiceNo") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubInNo = (rows.Cells[0].FindControl("lblSubInNo") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.DropDownList DDListItem = (rows.Cells[1].FindControl("DDListItem") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtItemName = (rows.Cells[1].FindControl("txtItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtDescription = (rows.Cells[2].FindControl("txtDescription") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtPrice = (rows.Cells[3].FindControl("txtPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblServiceType = (rows.Cells[4].FindControl("lblServiceType") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtGrossTotal = (rows.Cells[4].FindControl("txtGrossTotal") as System.Web.UI.WebControls.Label);

                    //if (txtQuantity.Text == "0.00" || txtQuantity.Text == "0,00")

                    if (lblServiceType.Text == "False")
                    {
                    }
                    else
                    {
                        if (txtGrossTotal.Text != "")
                        {
                            TOTAL = Convert.ToDecimal((txtGrossTotal.Text).ToString());
                        }
                        else
                        {
                            TOTAL = 0;
                        }
                        if (TOTAL.ToString() != "")
                        {
                            decimal Price = Convert.ToDecimal(txtPrice.Text);
                            long qty = Convert.ToInt64(dec);
                            decimal total = 0;
                            decimal pricqty = 0;
                            pricqty = Price * qty;
                            total = total + pricqty;
                            txtGrossTotal.Text = total.ToString();
                        }
                        if (BtnSaveInvoice.Visible == true)
                        {
                            if (LblTotalPrice.Text != "")
                            {
                                LblTotalPrice.Text = Convert.ToString(((Convert.ToDecimal(LblTotalPrice.Text)) - TOTAL) + (Convert.ToDecimal(txtGrossTotal.Text)));
                            }
                            else
                            {
                                LblTotalPrice.Text = txtGrossTotal.Text;
                            }
                        }
                    }
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_UpdateTempInvoice";
                    cmd.Parameters.Add("@INVOICENO", System.Data.SqlDbType.BigInt).Value = txtInvoiceNo.Text;
                    cmd.Parameters.Add("@SUBINNO", System.Data.SqlDbType.BigInt).Value = lblSubInNo.Text;
                    cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                    cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtItemName.Text;
                    cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtPrice.Text;
                    cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = txtQuantity.Text;
                    cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = 0;
                    cmd.Parameters.Add("@TAX1", System.Data.SqlDbType.Decimal).Value = 0;
                    cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = txtGrossTotal.Text;
                    cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = lblServiceType.Text;

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
                if (Global.Updateflag == 1)
                {
                    System.Web.UI.WebControls.Label txtGrossTotal1 = (rows.Cells[4].FindControl("txtGrossTotal") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label txtQuantity1 = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblServiceType1 = (rows.Cells[4].FindControl("lblServiceType") as System.Web.UI.WebControls.Label);
                    //if (txtQuantity1.Text == "0.00" || txtQuantity1.Text == "0,00")
                    if (lblServiceType1.Text == "True")
                    {
                        rows.BackColor = System.Drawing.Color.LightGoldenrodYellow;

                        float rowValue = 0;
                        if (float.TryParse(txtGrossTotal1.Text.Trim(), out rowValue))
                            total1 += rowValue;
                        lblServiceTotal.Text = total1.ToString();
                    }
                }
            }
            con.Close();
        }

        public void CHkClick_Order()//Not Use as Save TIme It Use Only Update Time//
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT Distinct * FROM  INVOICE Where InvoiceNo='" + txtInvoiceNo.Text + "'";
            SqlDataAdapter DAddapter = new SqlDataAdapter(cmd);
            DataSet DTable = new DataSet();
            DAddapter.Fill(DTable);

            gridViewOrder.DataSource = DTable;
            gridViewOrder.DataBind();

            LblTotalPrice.Text = DTable.Tables[0].Rows[0]["TotalInvoiceCost"].ToString();
            txtPercentage.Text = DTable.Tables[0].Rows[0]["PercentagePayable"].ToString();
            lblPaidAmount.Text = DTable.Tables[0].Rows[0]["AmountPayable"].ToString();
            lblPaidPercentage.Text = DTable.Tables[0].Rows[0]["PercentagePayable"].ToString();
            LbltxtPercentage.Text = DTable.Tables[0].Rows[0]["PercentagePayable"].ToString();

            cmd.ExecuteScalar();
            cmd.Parameters.Clear();

            con.Close();
        }

        protected void btnpdf_Click(object sender, EventArgs e)
        {

            if (ChkPayable.Checked == true)
            {
                SpanService.Visible = false;
            }

            if (ChkServicePayable.Checked == true)
            {
                SpanInvoce.Visible = false;

                LbltxtPercentage.Visible = false;
            }

            Table1.Visible = false;
            divOverflow.Visible = true;
            FileUploadDiv.Visible = false;
            SpaceDiv.Visible = true;
            DisableAjaxExtenders(this);
            imguser.Visible = false;

            //string imagepath = Server.MapPath("\\image\\pmglogo1.jpg");

            //SqlConnection con = new SqlConnection(cs);
            //con.Open();
            //SqlCommand com = new SqlCommand("select imagename from imagedetails where imageid ='" + lblInvoiceNo1.Text + "'", con);

            //SqlDataAdapter adpt = new SqlDataAdapter(com);
            //DataTable dt = new DataTable();

            //adpt.Fill(dt);
            //string imagename = dt.Rows[0][0].ToString();

            if (gridViewtest.Rows.Count > 0)
            {
                PdfPTable table = new PdfPTable(gridViewtest.Columns.Count);
                table.WidthPercentage = 100;

                Font NFont = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
                Font NFont1 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14);
                //table.Cellpadding = 5;
                //Set the column widths
                int[] widths = new int[gridViewtest.Columns.Count];
                for (int x = 0; x < gridViewtest.Columns.Count; x++)
                {
                    //Phrase phrs = new Phrase();
                    widths[x] = (int)gridViewtest.Columns[x].ItemStyle.Width.Value;
                    string cellText = Server.HtmlDecode(gridViewtest.HeaderRow.Cells[x].Text);
                    //phrs.Add(new Chunk(cellText));
                    PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont));
                    cell.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    cell.Border = 0;
                    cell.BorderWidthBottom = 1f;
                    cell.BorderWidthTop = 1f;
                    cell.PaddingBottom = 5;

                    //cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    table.AddCell(cell);
                }

                table.SetWidths(widths);
                //Transfer rows from GridView to table

                for (int i = 0; i < gridViewtest.Rows.Count; i++)
                {
                    if (gridViewtest.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        for (int j = 0; j < gridViewtest.Columns.Count; j++)
                        {
                            //Phrase phrs = new Phrase();
                            string cellText = Server.HtmlDecode(gridViewtest.Rows[i].Cells[j].Text);

                            //phrs.Add(new Chunk(cellText));
                            PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont1));
                            cell.BorderColor = BaseColor.WHITE;
                            //Set Color of Alternating row
                            //if (i % 2 != 0)
                            //{
                            //    cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#C2D69B"));
                            //}

                            table.AddCell(cell);
                        }
                    }
                }

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Invoice.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                gridViewOrder.AllowPaging = false;

                StringWriter sw = new StringWriter();
                StringWriter sw1 = new StringWriter();
                StringWriter sw2 = new StringWriter();
                StringWriter sw3 = new StringWriter();

                HtmlTextWriter hw = new HtmlTextWriter(sw);
                HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
                HtmlTextWriter hw2 = new HtmlTextWriter(sw2);
                HtmlTextWriter hw3 = new HtmlTextWriter(sw3);

                //this.RenderControl(hw);

                divcustinfo.RenderControl(hw1);
                Divpdfgeneration1.RenderControl(hw3);

                Divpdfgeneration2.RenderControl(hw2);//for factura

                //SpaceDiv.RenderControl(hw);// for Space
                //divOverflow.RenderControl(hw);

                Divpdfgeneration3.RenderControl(hw);//for Total Calculation
                Divpdfgeneration4.RenderControl(hw);//for note
                Divpdfgeneration5.RenderControl(hw);//for terms n Condition

                StringReader sr = new StringReader(sw.ToString());
                StringReader sr1 = new StringReader(sw1.ToString());
                StringReader sr2 = new StringReader(sw2.ToString());
                StringReader sr3 = new StringReader(sw3.ToString());
                Document pdfDoc = new Document(PageSize.A4, 50f, 50f, 100f, 50f);


                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

                //iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(imagepath);
                // gif.ScaleAbsolute(200f, 40f);
                // gif.SetAbsolutePosition(350f, 760f);

                writer.PageEvent = new ITextEvents();
                pdfDoc.Open();
                //pdfDoc.Add(new Paragraph("Company Logo:"));
                //pdfDoc.Add(gif);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                htmlparser.Parse(sr1);
                htmlparser.Parse(sr2);
                Paragraph para = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0f, 100.0f, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                pdfDoc.Add(para);
                htmlparser.Parse(sr3);
                Phrase psp = new Phrase(Environment.NewLine);
                pdfDoc.Add(psp);
                pdfDoc.Add(table);
                pdfDoc.Add(para);
                //pdfDoc.Add(psp);
                //htmlparser.Parse(sr);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);

                pdfDoc.Close();
                Response.Write(pdfDoc);
                gridViewOrder.AllowPaging = true;
                Response.End();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No data found!');", true);
            }
        }

        public void custinfo()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select Distinct * From Invoice Where InvoiceNo='" + lblInvoiceNo1.Text + "' ";

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                gridViewtest.DataSource = dt;
                gridViewtest.DataBind();
            }
            else
            {
                gridViewtest.DataSource = dt;
                gridViewtest.DataBind();
            }
        }

        public void ViewCustomerInfo()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand("select * from CUSTOMER where CustId =" + LblCustId.Text);
            com.Connection = con;
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                lblcustname.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                lblcustemail.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                lblcustaddress.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                lblcustcity.Text = dt.Tables[0].Rows[0]["City"].ToString();
                lblcustcountry.Text = dt.Tables[0].Rows[0]["Country"].ToString();

            }
            con.Close();
        }

    }
}