﻿var s = document.createElement('style'),
    r = document.querySelector('[type=range]');

document.body.appendChild(s);

/* IE doesn't need the JS part & this won't work in IE anyway ;) */
r.addEventListener('input', function () {
    var val = this.value + '% 100%';

    s.textContent =
      '.js input[type=range]::-webkit-slider-runnable-track{background-size:' + val + '}' +
      '.js input[type=range]::-moz-range-track{background-size:' + val + '}';
    s.textContent += '.js input[type=range] /deep/ #thumb:before{content:"' + this.value + '%"}';
}, false);






var volume = {

    init: function () {
        $('#volume').on('click', volume.change);
        $('#volume .control').on('mousedown', volume.drag);
    },

    change: function (e) {
        e.preventDefault();
        var percent = helper.getFrac(e, $(this)) * 100;
        $('#volume .control').animate({
            width: percent + '%'
        }, 100);
        volume.update(percent);
    },

    update: function (percent) {
        $('.vol-box').text(Math.round(percent));
        //console.log(percent);
    },

    drag: function (e) {
        e.preventDefault();
        $(document).on('mousemove', volume.moveHandler);
        $(document).on('mouseup', volume.stopHandler);
    },

    moveHandler: function (e) {
        var holderOffset = $('#volume').offset().left,
          sliderWidth = $('#volume').width(),
          posX = Math.min(Math.max(0, e.pageX - holderOffset), sliderWidth);

        $('#volume .control').width(posX);
        volume.update(posX / sliderWidth * 100);
    },

    stopHandler: function () {
        $(document).off('mousemove', volume.moveHandler);
        $(document).off('mouseup', volume.stopHandler);
    }

}

var helper = {
    getFrac: function (e, $this) {
        return (e.pageX - $this.offset().left) / $this.width();
    }
}

volume.init();