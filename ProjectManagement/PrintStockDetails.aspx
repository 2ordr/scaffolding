﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintStockDetails.aspx.cs" Inherits="ProjectManagement.PrintStockDetails" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>


    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style type="text/css">
        .divcss {
            border: dotted;
            border-color: black;
            color: green;
        }
    </style>
    <script type="text/javascript" language="javascript">

    function CloseWindow() {
    window.close();
    }
         </script>

</head>
<body>
    <form id="form1" runat="server">
        <div runat="server" style="height: 100%; width: 100%; border: double; border-color: black;">
            <div id="finishDiv" runat="server" style="height: 100%; width: 100%; border: double; border-color: black;">
                <div id="TopDiv" runat="server" height="80" width="100%">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%">
                                <asp:Label runat="server" Text="MONTAGEAFREGNING" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width: 45%; text-align: right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="buttonn" OnClick="btnSave_Click" />
                                <asp:Button ID="BtnPDFGene" runat="server" Text="Generate Report" CssClass="buttonn" OnClick="BtnPDFGene_Click" Visible="false" />
                                <asp:Label ID="lblType" runat="server" Text="" Font-Bold="true" Visible="False"></asp:Label>
                            </td>
                            <td style="width: 5%;"></td>
                        </tr>
                    </table>
                </div>
                <div id="TopMainDiv" runat="server" height="810" width="100%;">
                    <div>
                        <table style="font-family: Calibri; width: 100%;">
                            <tr style="background-color: #F2F2F2; height: 30px">

                                <td style="width: 20px; font-size: large;"></td>
                                <td style="width: 100px; font-size: large;"></td>
                                <td style="width: 100px; font-size: large;">Day:</td>
                                <td style="width: 100px; font-size: large;">Order No:</td>
                                <td style="width: 100px; font-size: large;"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="allSides" id="divheader" runat="server" style="width: 100%;">
                        <table style="font-family: Calibri; width: 100%;">
                            <tr style="background-color: #F2F2F2; height: 30px; border: 1px">
                                <td style="width: 10%;"></td>
                                <td style="width: 80%;">
                                    <table style="font-family: Calibri; width: 100%;">
                                        <tr style="background-color: #F2F2F2; height: 30px; border: 1px">
                                            <td style="width: 3%; font-size: large;"></td>
                                            <td style="width: 21%; font-size: large;">StockQuantity</td>
                                            <td style="width: 22%; font-size: large;">Item Name</td>
                                            <td style="width: 22%; font-size: large;">Item Fees</td>
                                            <td style="width: 32%; font-size: large;">Tot. Amount</td>
                                            <%-- <td style="width: 20%; font-size: large;"></td>--%>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 10%;"></td>
                            </tr>
                        </table>
                    </div>
                    <div id="Divstocklist" runat="server" style="overflow: auto; width: 100%; height: 750px;">
                        <table style="width: 100%; font-family: Calibri;">
                            <tr>
                                <td style="width: 10%;"></td>
                                <td style="width: 80%;">
                                    <asp:GridView ID="GviewProjectStock" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="stkChkbox" runat="server" Visible="False" />
                                                    <asp:Label ID="LblItemcode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="False"></asp:Label>
                                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>' Visible="False"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="20" />
                                                <FooterTemplate></FooterTemplate>
                                                <FooterStyle Height="100" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="StockQuantity" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtQuntity" runat="server" Height="21" OnTextChanged="txtQuntity_TextChanged1" AutoPostBack="true"></asp:TextBox>
                                                    <asp:Label ID="lblQuntity" runat="server" Text="" Visible="False"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="20" />
                                                <FooterTemplate></FooterTemplate>
                                                <FooterStyle Height="100" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ItemName" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                <ItemTemplate>
                                                    <asp:Label ID="ItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                                                    <asp:TextBox ID="txtItemName" runat="server" Height="21" Visible="False"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle Width="200" />
                                                <FooterTemplate></FooterTemplate>
                                                <FooterStyle Height="100" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ItemFees" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                <ItemTemplate>
                                                    <asp:Label ID="ItemFees" runat="server" Text='<%# Bind("ItemFees") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="200" />
                                                <FooterTemplate></FooterTemplate>
                                                <FooterStyle Height="100" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Total Amount" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTotAmt" runat="server" Text=""></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="300px" />
                                            </asp:TemplateField>

                                            <%--    <asp:TemplateField HeaderText="Total Amount" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTotAmt1" runat="server" Text=""></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="300px" />
                                                <FooterTemplate></FooterTemplate>
                                                <FooterStyle Height="100" />
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                                <td style="width: 10%;"></td>
                            </tr>
                        </table>

                    </div>
                    <div id="DivFooter" runat="server" style="height: 50px" class="divcss">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 24%"></td>
                                <td style="width: 18%"></td>
                                <td style="width: 30%; font-size: x-large" title="">TOTAL:
                                    <asp:Label runat="server" ID="LblGrantTotal"></asp:Label></td>
                                <td style="width: 28%"></td>
                            </tr>
                        </table>
                    </div>
                    <div id="DivStockHeavy" runat="server" style="overflow: auto; width: 100%; height: 750px;" visible="false">
                        <table style="width: 100%; font-family: Calibri;">
                            <tr>
                                <td style="width: 100%;">
                                    <asp:GridView ID="GridViewHeavy" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="stkChkbox" runat="server" Visible="true" />
                                                </ItemTemplate>
                                                <ItemStyle Width="20" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="StockQuantity1" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtQuntity" runat="server" Height="21" OnTextChanged="txtQuntity_TextChanged"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle Width="20" />
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="ItemName" HeaderText="ItemCode" ItemStyle-Width="200" />

                                            <asp:BoundField DataField="ItemFees" HeaderText="ItemCode" ItemStyle-Width="200" />

                                            <asp:TemplateField HeaderText="Total Amount" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTotAmt" runat="server" Text=""></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="600px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Total Amount" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTotAmt1" runat="server" Text=""></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="300px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </form>
</body>
</html>
