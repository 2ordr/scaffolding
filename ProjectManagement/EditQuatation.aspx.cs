﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Sql;

namespace ProjectManagement
{
    public partial class EditQuatation : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    txtSearchBox.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    //ButView.Visible = false;
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_getQuatationByDate";

                    cmd.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = Request.QueryString["Quatdate"];
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        GridViewEditQuatation.DataSource = dt;
                        GridViewEditQuatation.DataBind();

                        txtDate.Text = dt.Tables[0].Rows[0]["DateTime"].ToString();
                        txtName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                        txtResTime.Text = dt.Tables[0].Rows[0]["RespondTime"].ToString();
                        TxtResCost.Text = dt.Tables[0].Rows[0]["TotalCost"].ToString();
                        ddlCustAck.Text = dt.Tables[0].Rows[0]["CustAck"].ToString();
                        ddlAdminApp.Text = dt.Tables[0].Rows[0]["AdminApproval"].ToString();
                        ddquotestatus.Text = dt.Tables[0].Rows[0]["Status"].ToString();
                        lblName.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                        txtEarning.Text = dt.Tables[0].Rows[0]["QuoteEarn"].ToString();
                    }
                    con.Close();
                }
            }
        }

        protected void ButUpdate_Click(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            //String orderid = SELECT ISNULL(MAX(OrderId),0) + 1 FROM ORDERS;
            int id = 0;
            cmd.CommandText = "SELECT (ISNULL(MAX(OrderId),0)) as OrderId FROM ORDERS";
            id = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            cmd.Parameters.Clear();
            //con.Close();

            foreach (GridViewRow rows in GridViewEditQuatation.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label ItemName = (rows.Cells[1].FindControl("ItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemPrice = (rows.Cells[2].FindControl("lblItemPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtQuantity = (rows.Cells[3].FindControl("txtQuantity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.TextBox txtAmount = (rows.Cells[4].FindControl("txtAmount") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblTot = (rows.Cells[5].FindControl("lblTot") as System.Web.UI.WebControls.Label);

                    //con.Open();
                    // cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_UpdateHAS";
                    cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblName.Text;
                    cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                    cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                    cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                    cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                    cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedItem.Text; ;
                    cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                    cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                    cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = txtEarning.Text;

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    // con.Close();
                }
            }

            //SqlConnection con1 = new SqlConnection(cs);
            //SqlCommand com = new SqlCommand();
            //con1.Open();
            //com.Connection = con1;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_UpdateRequestQuote";

            cmd.Parameters.Add("@DATETIME", System.Data.SqlDbType.NVarChar).Value = txtDate.Text; ;
            cmd.Parameters.Add("@CUSTACK", System.Data.SqlDbType.VarChar).Value = ddlCustAck.SelectedItem.Text;
            cmd.Parameters.Add("@ADMINAPPR", System.Data.SqlDbType.VarChar).Value = ddlAdminApp.SelectedItem.Text;
            cmd.Parameters.Add("@EXPECTTIME", System.Data.SqlDbType.DateTime).Value = txtResTime.Text;
            cmd.Parameters.Add("@RESPTIME", System.Data.SqlDbType.DateTime).Value = txtResTime.Text;
            cmd.Parameters.Add("@TOTALCOST", System.Data.SqlDbType.Decimal).Value = TxtResCost.Text;

            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            //con1.Close();

            if (ddquotestatus.SelectedValue == "Accepted")
            {
                Response.Redirect("Order Form.aspx?DATETIME=" + txtDate.Text);
            }
            ButUpdate.Visible = false;
            ButView.Visible = true;
            con.Close();
        }

        protected void ddquotestatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridViewEditQuatation_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("ViewQuatation.aspx");
        }

        protected void ButView_Click(object sender, EventArgs e)
        {
            Response.Redirect("Wizardss.aspx");
        }
    }
}