﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace ProjectManagement
{
    public partial class customer_timesheet : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblCustId.Text = Session["labelid"].ToString();

            if (!IsPostBack)
            {


                SqlConnection con = new SqlConnection(cs);
                con.Open();

                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_getQuatationByName";
                com.Parameters.Add("@CUSTID", System.Data.SqlDbType.NVarChar).Value = lblCustId.Text;


                SqlDataAdapter adpt = new SqlDataAdapter(com);

                DataSet dt = new DataSet();

                //dt.Rows.Clear();

                adpt.Fill(dt);

                CusQuoteGrideView.DataSource = dt;

                CusQuoteGrideView.DataBind();

                LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();

            }
        }

        //protected void QuoteChkbox_CheckedChanged(object sender, EventArgs e)
        //{
        //    var checkbox = (CheckBox)sender;
        //    if (checkbox.Checked)
        //    {
        //        Response.Redirect("CompileVariance.aspx");
        //    }

        //}

        protected void BtnQuotes_Click(object sender, EventArgs e)
        {
            Response.Redirect("Quatation request form.aspx");
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {


            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[1].Text;
            //Response.Redirect("QuatationPrint.aspx?Quatdate=" + quatdate);


            string url = ("QuatationPrint.aspx?Quatdate=" + quatdate);

            string s = "window.open('" + url + "');";

            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);


        }

        protected void LikButEdit_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[1].Text;
            Response.Redirect("EditQuatation.aspx?Quatdate=" + quatdate);
        }
    }
}
