﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuatationPrint.aspx.cs" Inherits="ProjectManagement.QuatationPrint" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>


    <link href="Styles/style.css" rel="stylesheet" type="text/css" />

    <style>
        .clienthead {
            font-size: xx-large;
            font-weight: bold;
        }

        .Grid {
            background-color: #fff;
            margin: 5px 0 10px 0;
            border: solid 1px #525252;
            border-collapse: collapse;
            font-family: Calibri;
            color: #474747;
        }

            .Grid td {
                padding: 2px;
                border: solid 1px #c1c1c1;
            }

            .Grid th {
                padding: 4px 2px;
                color: #fff;
                background: #363670 url(Images/grid-header.png) repeat-x top;
                border-left: solid 1px #525252;
                font-size: 0.9em;
            }

            .Grid .alt {
                background: #fcfcfc url(Images/grid-alt.png) repeat-x top;
            }

            .Grid .pgr {
                background: #363670 url(Images/grid-pgr.png) repeat-x top;
            }

                .Grid .pgr table {
                    margin: 3px 0;
                }

                .Grid .pgr td {
                    border-width: 0;
                    padding: 0 6px;
                    border-left: solid 1px #666;
                    font-weight: bold;
                    color: #fff;
                    line-height: 12px;
                }

                .Grid .pgr a {
                    color: Gray;
                    text-decoration: none;
                }

                    .Grid .pgr a:hover {
                        color: #000;
                        text-decoration: none;
                    }

        .Itemalign {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 1000px; width: 100%; font-size: xx-large; background-color: #FAFAFA;">
            <%--<div style="margin: 0px; left: 0px; top: 0px; width: 200px; height: 1000px; float: left"></div>--%>
            <div id="QuationMainDiv" runat="server" style="margin: 0px; left: 195px; top: 0px; width: 100%; height: 100%; float: left; background-color: #FAFAFA;" visible="true">
                <div style="width: 100%; height: 50px; background-color: silver; text-align: center">
                    <asp:Label ID="Label3" runat="server" CssClass="clienthead" Text="Quatation"></asp:Label>
                </div>
                <div style="height: 250px">
                    <table style="width: 100%">
                        <tr>
                            <td style="height: 250px; width: 50%;">
                                <div style="width: 100%; height: 250px">
                                    <div></div>

                                    <div style="margin-left: 50px">
                                        <asp:Label ID="CompanyName" runat="server" Text="" Style="font-size: medium"><%--<img src="image/Logo.jpg" height="200" width="200"/>--%></asp:Label>
                                    </div>
                                    <%-- <div style="margin-left: 50px">
                                        <asp:Label ID="Address" runat="server" Text="" Style="font-size: medium">aaa</asp:Label>
                                    </div>
                                    <div style="margin-left: 50px">
                                        <asp:Label ID="Phone" runat="server" Text="" Style="font-size: medium">aaaaa</asp:Label>
                                    </div>--%>
                                </div>
                            </td>
                            <td style="height: 250px; left: 350px; width: 50%; text-align: right;">
                                <div style="width: 100%; height: 250px">


                                    <div style="height: 50px;"></div>
                                    <div style="margin-right: 50px;">
                                        <asp:Label ID="LblCustName" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                    </div>
                                    <div style="margin-right: 50px;">
                                        <asp:Label ID="LblCustAddress" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                    </div>
                                    <div style="margin-right: 50px;">
                                        <asp:Label ID="LblCity" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                    </div>
                                    <div style="margin-right: 50px;">
                                        <asp:Label ID="LblEmail" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                    </div>
                                    <div style="margin-right: 50px;">
                                        <asp:Label ID="Datetime" runat="server" Text="" Style="font-size: medium"></asp:Label>
                                    </div>


                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 30px; font-size: large; margin-left: 50px; margin-bottom: 0px;">
                    Respected Sir/Madam:
                </div>
                <div style="height: 20px; left: 50px; font-size: medium;">
                    <p style="margin-left: 50px;">
                        <asp:Label ID="Label1" runat="server" Text="Subject::" Font-Bold="true"></asp:Label>Quatation Requested As Dated on
                    <asp:Label ID="datetime1" runat="server" Text=""></asp:Label>
                    </p>
                </div>
                <div style="height: 20px">
                    <p style="font-size: medium; margin-left: 50px;">
                        The Requested Quote for following items as referenced on above date is here in provided for your reference .<br />
                        Kindly acknowledge the same & do the neccesary Advance payment..
                    </p>
                </div>
                <div style="height: 20px"></div>
                <div style="width: 100%; height: 200px">
                    <div style="height: 20px"></div>

                    <asp:GridView ID="GrdViewPrint" runat="server" ShowFooter="true" AutoGenerateColumns="false" Width="100%" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White" Style="font-size: medium">
                        <Columns>
                            <asp:TemplateField HeaderText="Item Code" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                <ItemTemplate>
                                    <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="true"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="Itemalign" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ItemName" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                <ItemTemplate>
                                    <asp:Label ID="lblItemName" runat="server" Text='<%# Bind("ItemName") %>' Visible="true"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="Itemalign" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Quantity" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                <ItemTemplate>
                                    <asp:Label ID="lblItemQua" runat="server" Text='<%# Bind("RespondQuantity") %>' Visible="true"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="Itemalign" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Price" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                <ItemTemplate>
                                    <asp:Label ID="lblItemPrice" runat="server" Text='<%# Bind("Price") %>' Visible="true"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="Itemalign" />
                                <FooterTemplate>
                                    <%--<div>
                                        <asp:Label ID="lblTotalCost" runat="server" Text="Total Cost" />
                                    </div>--%>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GrossTotal" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                <ItemTemplate>
                                   <%-- <asp:Label ID="lblItemgtotal" runat="server" Text='<%# Convert.ToInt32(Eval("RespondQuantity"))*Convert.ToInt32(Eval("Price")) %>' Visible="true"></asp:Label>--%>
                                     <asp:Label ID="lblItemgtotal" runat="server" Text='<%# Bind("TotalPrice") %>' Visible="true"></asp:Label>

                                </ItemTemplate>
                                <ItemStyle CssClass="Itemalign" />
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalCost1" runat="server" Text="" Visible="true"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>
                    <div style="height: 38px; width: 100%">
                        <table style="width: 100%; height: 17px">
                            <tr>
                                <td style="width: 62%"></td>
                                <td style="width: 38%">
                                    <div>
                                        <table style="width: 100%; height: 17px">
                                            <tr>
                                                <td style="width: 40%">
                                                    <asp:Label ID="lblTotalCost" runat="server" Text="Total Cost:" Style="font-size: medium; border-color: white; border-width: 2px" BorderColor="White" BorderWidth="5px"></asp:Label>
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:Label ID="lblTotalCost1" runat="server" Style="font-size: medium" BorderColor="White"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 38px; width: 100%">
                        <table style="width: 100%; height: 17px">
                            <tr>
                                <td style="width: 62%"></td>
                                <td style="width: 38%">
                                    <div>
                                        <table style="width: 100%; height: 17px">
                                            <tr>
                                                <td style="width: 80%">
                                                    <asp:Label ID="Label2" runat="server" Text="Total Cost is inclusive of all discount:" Style="font-size: medium; border-color: white; border-width: 2px" BorderColor="White" BorderWidth="5px"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="Label4" runat="server" Style="font-size: medium" BorderColor="White"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 40px"></div>
                    <div>
                        <table style="width: 100%;">
                            <tr>
                                <td style="font-weight: 700; width: 180px; text-align: center; background-color: #F2F2F2; border-width: 10px; border-color: white; font-size: medium;">$Select Condition</td>
                                <td style="font-weight: 700; text-align: center; width: 1330px; background-color: #F2F2F2; border-width: 10px; border-color: white; font-size: medium;">Complete Terms & Condition</td>
                            </tr>
                        </table>
                    </div>
                    <div id="divShow" runat="server" style="overflow: auto; height: 200px;">
                        <table style="width: 100%;">

                            <tr>
                                <td style="width: 100%;">
                                    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" OnRowDataBound="GridView3_RowDataBound" ShowHeader="false" DataKeyNames="StantekstID" OnSelectedIndexChanged="GridView3_SelectedIndexChanged">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Select Condition" HeaderStyle-Font-Size="Medium" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DropDownList2" runat="server" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"></asp:DropDownList>
                                                    <%-- <asp:SqlDataSource ID="DataSKnoB" runat="server" ConnectionString="<%$ ConnectionStrings:myConnection %>" SelectCommand="SELECT DISTINCT [StantekstID], [KortNavn] FROM [Predifined-Text] "></asp:SqlDataSource>--%>
                                                </ItemTemplate>
                                                <HeaderStyle Width="220px" />
                                                <ItemStyle Width="200px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Complete Terms & Condition" HeaderStyle-Font-Size="Medium" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCondition" runat="server" Style="font-size: medium"></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </td>

                            </tr>
                        </table>
                    </div>
                    <div style="height: 40px;">

                        <p style="margin-left: 50px; font-size: medium">
                            Kindly Reply, As waiting to here from you..<br />
                            Regards:
                        </p>
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="btnSave" Style="background-color: #009900;" runat="server" Text="Save" Height="25px" OnClick="btnSave_Click" />
                                    <asp:Button CssClass="buttonn" ID="ExportTopdf" runat="server" Text="ExPdf" Style="background-color: #009900;" Height="25px" OnClick="ExportTopdf_Click" Visible="true" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
