﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data.OleDb;
using System.Data;

namespace ProjectManagement
{
    public partial class Stocks : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }

                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                    DIvTabPanel.Visible = false;
                    Label1.Text = Session["username"].ToString();
                    lblItemCode.Text = Request.QueryString["ItemCode"];
                    UpdateStocks.Visible = false;
                    ViewStocks.Visible = false;

                    STOCKGROUP();
                    StockItemProperty();
                    DdStockType.Items.Insert(0, new ListItem("-Vælg-"));

                    if (lblItemCode.Text != String.Empty)
                    {
                        UpdateStocks.Visible = true;
                        AddStocks.Visible = false;
                        SqlConnection con = new SqlConnection(cs);
                        con.Open();

                        SqlCommand com = new SqlCommand("select * from STOCKSITEM where ItemCode =  @I_ITEMCODE", con);
                        com.Parameters.Add("@I_ITEMCODE", SqlDbType.BigInt).Value = lblItemCode.Text;
                        SqlDataAdapter adpt = new SqlDataAdapter(com);
                        DataSet dt = new DataSet();

                        adpt.Fill(dt);

                        if (dt.Tables[0].Rows.Count > 0)
                        {
                            gvUpdateStockItem.DataSource = dt;
                            gvUpdateStockItem.DataBind();

                            txtName.Text = dt.Tables[0].Rows[0]["ItemName"].ToString();
                            DdStockType.Text = dt.Tables[0].Rows[0]["Type"].ToString();
                            txtprice.Text = dt.Tables[0].Rows[0]["Price"].ToString();
                            txtQuantity.Text = dt.Tables[0].Rows[0]["Quantity"].ToString();
                            txtUnit.Text = dt.Tables[0].Rows[0]["Units"].ToString();
                            txtstkno.Text = dt.Tables[0].Rows[0]["StockNumber"].ToString();
                            ddItemProperty.Text = dt.Tables[0].Rows[0]["ItemProperty1"].ToString();
                            //DdStockGroup.Text = dt.Tables[0].Rows[0]["Type"].ToString();
                            txtItemFees.Text = dt.Tables[0].Rows[0]["ItemFees"].ToString();
                            RBtnUseQuate.Checked = Convert.ToBoolean(dt.Tables[0].Rows[0]["UseQuate"].ToString());
                            RBtnUseLight.Checked = Convert.ToBoolean(dt.Tables[0].Rows[0]["UseLight"].ToString());
                            RBtnUseHeavy.Checked = Convert.ToBoolean(dt.Tables[0].Rows[0]["UseHeavy"].ToString());
                        }

                        con.Close();

                    }

                }
            }
        }

        public void STOCKGROUP()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;

            String str = "Select * from STOCKGROUP";
            SqlDataAdapter adpt = new SqlDataAdapter(str, con);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                DdStockGroup.DataSource = dt;
                DdStockGroup.DataBind();
                DdStockGroup.DataTextField = "GNAME";
                DdStockGroup.DataValueField = "GID";
                DdStockGroup.DataBind();
                DdStockGroup.Items.Insert(0, new ListItem("-Vælg-"));
            }
            con.Close();
        }
        public void StockItemProperty()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            com.Connection = conn;

            string str1 = "Select * from StockItemProperty";
            SqlDataAdapter adpt1 = new SqlDataAdapter(str1, conn);
            DataTable dt1 = new DataTable();
            adpt1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                ddItemProperty.DataSource = dt1;
                ddItemProperty.DataBind();
                ddItemProperty.DataTextField = "ItemProName";
                ddItemProperty.DataValueField = "ITEMPROID";
                ddItemProperty.DataBind();
                ddItemProperty.Items.Insert(0, new ListItem("-Vælg-"));
            }
            conn.Close();
        }

        public void resetall()
        {
            txtName.Text = "";
            //txtDescript.Text = "";
            txtprice.Text = "";
            txtQuantity.Text = "";
            txtUnit.Text = "";
            txtstkno.Text = "";
            //DdStockType.SelectedValue = "";
            txtItemFees.Text = "";
        }

        protected void AddStocks_Click(object sender, EventArgs e)
        {
            if (txtName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertStockItems";

                com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;

                if (ddItemProperty.SelectedValue != "-Vælg-")
                {
                    com.Parameters.Add("@ITEMPROPERTY1", System.Data.SqlDbType.VarChar).Value = ddItemProperty.SelectedItem.Text;
                }
                else
                {
                    com.Parameters.Add("@ITEMPROPERTY1", System.Data.SqlDbType.VarChar).Value = "";
                }
                com.Parameters.Add("@ITEMPROPERTY2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@ITEMPROPERTY3", System.Data.SqlDbType.VarChar).Value = "";

                if (txtprice.Text != "")
                {
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtprice.Text;
                }
                else
                {
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = 0;
                }

                if (txtQuantity.Text != "")
                {
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                }
                else
                {
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = 0;
                }

                com.Parameters.Add("@FLAG", System.Data.SqlDbType.Bit).Value = 0;

                if (txtUnit.Text != "")
                {
                    com.Parameters.Add("@UNIT", System.Data.SqlDbType.VarChar).Value = txtUnit.Text;
                }
                else
                {
                    com.Parameters.Add("@UNIT", System.Data.SqlDbType.VarChar).Value = 0;
                }

                if (txtstkno.Text != "")
                {
                    com.Parameters.Add("@STOCKNUMBER", System.Data.SqlDbType.NVarChar).Value = txtstkno.Text;
                }
                else
                {
                    com.Parameters.Add("@STOCKNUMBER", System.Data.SqlDbType.NVarChar).Value = "";
                }

                if (DdStockType.SelectedValue != "-Vælg-")
                {
                    com.Parameters.Add("@TYPE", System.Data.SqlDbType.NVarChar).Value = DdStockType.SelectedValue;
                }
                else
                {
                    com.Parameters.Add("@TYPE", System.Data.SqlDbType.NVarChar).Value = "";
                }
                if (txtItemFees.Text != "")
                {
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = txtItemFees.Text;
                }
                else
                {
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = 0;
                }

                com.Parameters.Add("@USEQUATE", System.Data.SqlDbType.Bit).Value = RBtnUseQuate.Checked;
                com.Parameters.Add("@USELIGHT", System.Data.SqlDbType.Bit).Value = RBtnUseLight.Checked;
                com.Parameters.Add("@USEHEAVY", System.Data.SqlDbType.Bit).Value = RBtnUseHeavy.Checked;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!New Stock Created Successfully!!!!!!!!!!!');", true);
                con.Close();
            }
            resetall();
            AddStocks.Visible = false;
            ViewStocks.Visible = true;
        }

        protected void ViewStocks_Click(object sender, EventArgs e)
        {
            Response.Redirect("View Stocks.aspx?");

        }

        protected void AddGroup_Click(object sender, EventArgs e)
        {
            //Response.Redirect("StockInfo.aspx");
            DIvTabPanel.Visible = true;

            //tb1.TabIndex = 1;
            //tb1.Visible = true;
            ContentInfo.ActiveTab = tb1;
            tb2.Enabled = false;
            // tb2.Visible = false;
        }

        //protected void txtAddGroup_TextChanged(object sender, EventArgs e)
        //{
        //    SqlConnection con1 = new SqlConnection(cs);
        //    con1.Open();
        //    com.Connection = con1;
        //    com.CommandType = System.Data.CommandType.StoredProcedure;
        //    com.CommandText = "sp_InsertStockGroup";
        //    com.Parameters.Add("@GNAME", System.Data.SqlDbType.VarChar).Value = txtAddGroup.Text;

        //    com.ExecuteNonQuery();
        //    com.Parameters.Clear();


        //}

        protected void AddProperty_Click(object sender, EventArgs e)
        {
            //Response.Redirect("StockInfo.aspx");
            DIvTabPanel.Visible = true;

            //tb2.TabIndex = 1;
            // tb2.Visible = true;
            tb1.Enabled = false;
            ContentInfo.ActiveTab = tb2;

        }

        protected void UpdateStocks_Click(object sender, EventArgs e)
        {
            if (txtName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_UpdatesStockItemDetails";
                com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;

                com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;

                if (ddItemProperty.SelectedValue != "-Vælg-")
                {
                    com.Parameters.Add("@ITEMPROPERTY1", System.Data.SqlDbType.VarChar).Value = ddItemProperty.SelectedItem.Text;
                }
                else
                {
                    com.Parameters.Add("@ITEMPROPERTY1", System.Data.SqlDbType.VarChar).Value = "";
                }
                com.Parameters.Add("@ITEMPROPERTY2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@ITEMPROPERTY3", System.Data.SqlDbType.VarChar).Value = "";

                if (txtprice.Text != "")
                {
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtprice.Text;
                }
                else
                {
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = 0;
                }

                if (txtQuantity.Text != "")
                {
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                }
                else
                {
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = 0;
                }

                com.Parameters.Add("@FLAG", System.Data.SqlDbType.Bit).Value = 0;

                if (txtUnit.Text != "")
                {
                    com.Parameters.Add("@UNIT", System.Data.SqlDbType.VarChar).Value = txtUnit.Text;
                }
                else
                {
                    com.Parameters.Add("@UNIT", System.Data.SqlDbType.VarChar).Value = 0;
                }

                if (txtstkno.Text != "")
                {
                    com.Parameters.Add("@STOCKNUMBER", System.Data.SqlDbType.NVarChar).Value = txtstkno.Text;
                }
                else
                {
                    com.Parameters.Add("@STOCKNUMBER", System.Data.SqlDbType.NVarChar).Value = "";
                }

                if (DdStockType.SelectedValue != "-Vælg-")
                {
                    com.Parameters.Add("@TYPE", System.Data.SqlDbType.NVarChar).Value = DdStockType.SelectedItem.Text;
                }
                else
                {
                    com.Parameters.Add("@TYPE", System.Data.SqlDbType.NVarChar).Value = "";
                }
                if (txtItemFees.Text != "")
                {
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = txtItemFees.Text;
                }
                else
                {
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = 0;
                }

                com.Parameters.Add("@USEQUATE", System.Data.SqlDbType.Bit).Value = RBtnUseQuate.Checked;
                com.Parameters.Add("@USELIGHT", System.Data.SqlDbType.Bit).Value = RBtnUseLight.Checked;
                com.Parameters.Add("@USEHEAVY", System.Data.SqlDbType.Bit).Value = RBtnUseHeavy.Checked;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
                UpdateStocks.Visible = false;
                ViewStocks.Visible = true;
            }

            //ScriptManager.RegisterStartupScript(this.GetType(), "Scripts", "<scripts>alert('!! Stock Item Updated Successfully !! ');</scripts>");
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Stock Item Updated Successfully !!!!!!!!!!!');", true);
        }

        private bool value = true;
        protected void RBtnUseQuate_CheckedChanged(object sender, EventArgs e)
        {
            if (RBtnUseQuate.Checked == true)
            {
                RBtnUseHeavy.Checked = false;
                RBtnUseLight.Checked = false;
                value = true;
            }
        }

        protected void RBtnUseLight_CheckedChanged(object sender, EventArgs e)
        {
            if (RBtnUseLight.Checked == true)
            {
                RBtnUseHeavy.Checked = false;
                RBtnUseQuate.Checked = false;
                value = true;
            }
        }

        protected void RBtnUseHeavy_CheckedChanged(object sender, EventArgs e)
        {
            if (RBtnUseLight.Checked == true)
            {
                RBtnUseLight.Checked = false;
                RBtnUseQuate.Checked = false;
                value = true;
            }
        }

        protected void BtnADD_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            if (string.IsNullOrWhiteSpace(txtStockGroup.Text))
            {
            }
            else
            {
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertStockGroup";
                com.Parameters.Add("@GNAME", System.Data.SqlDbType.NVarChar).Value = txtStockGroup.Text;
                com.ExecuteNonQuery();

                com.Parameters.Clear();
                STOCKGROUP();
                txtStockGroup.Text = "";
            }
            con.Close();
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            if (string.IsNullOrWhiteSpace(txtItemProperty.Text))
            {
            }
            else
            {
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertstockItemPro";
                com.Parameters.Add("@ItemProName", System.Data.SqlDbType.NVarChar).Value = txtItemProperty.Text;

                com.ExecuteNonQuery();

                com.Parameters.Clear();
                StockItemProperty();
                txtItemProperty.Text = "";
            }
            con.Close();

        }

    }
}

