﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;

namespace ProjectManagement
{
    public partial class Employee_Information : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    Label1.Text = Session["username"].ToString();
                    resetall();
                }
            }
        }
        public void resetall()
        {
            txtempName.Text = "";
            txtlname.Text = "";
            txtMobile.Text = "";
            txtphone.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtProvince.Text = "";
            txtSkillset.Text = "";
            txtZipcode.Text = "";
            txtOccupation.Text = "";
            txtWage.Text = "";
            txtUName.Text = "";
        }
        protected void AddEmployee_Click(object sender, EventArgs e)
        {

            if (txtempName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name can't be blank');", true);
                return;
            }
            else
            {
                TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertEmployeeDetails";

                com.Parameters.Add("@USERNAME", System.Data.SqlDbType.VarChar).Value = txtUName.Text;
                com.Parameters.Add("@PASSWORD", System.Data.SqlDbType.VarChar).Value = txtPassword.Text;
                com.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = txtempName.Text;
                com.Parameters.Add("@LNAME", System.Data.SqlDbType.VarChar).Value = txtlname.Text;
                com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
                com.Parameters.Add("@PHONE2", System.Data.SqlDbType.VarChar).Value = txtphone.Text; ;
                com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
                com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress.Text;
                com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtCity.Text;
                com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince.Text;
                com.Parameters.Add("@SKILLSETS", System.Data.SqlDbType.VarChar).Value = txtSkillset.Text;
                com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.VarChar).Value = txtZipcode.Text;
                com.Parameters.Add("@OCCUPATION", System.Data.SqlDbType.VarChar).Value = txtOccupation.Text;
                com.Parameters.Add("@USERTYPE", System.Data.SqlDbType.VarChar).Value = ddlUserType.SelectedItem.Text;
                com.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = txtWage.Text;
                if (txtLunchTime.Text != "")
                {
                    com.Parameters.Add("@LunchTime", System.Data.SqlDbType.Decimal).Value = txtLunchTime.Text;
                }
                else
                {
                    com.Parameters.Add("@LunchTime", System.Data.SqlDbType.Decimal).Value = 0;
                }
                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!New User Created Successfully!!!!!!!!!!!');", true);
            }
            resetall();
            AddEmployee.Visible = false;
            ViewEmployee.Visible = true;
        }

        protected void ViewEmployee_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddContactVertical.aspx?");
        }


    }

}