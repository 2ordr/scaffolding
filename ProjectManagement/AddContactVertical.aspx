﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddContactVertical.aspx.cs" Inherits="ProjectManagement.AddContactVertical" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <title>>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <%--<link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />--%>
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <link rel="stylesheet" href="fonts/untitle-font-4/styles.css" type="text/css" />

    <style>
        headform1 {
            color: #383838;
        }

        .bc {
            border-color: brown;
        }


        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .bg {
            text-align: right;
        }

        .table1 {
            border: 1px solid #ddd;
            border-collapse: separate;
            border-radius: 15px;
            border-spacing: 5px;
        }

        .simple {
            color: lightblue;
        }

            .simple:hover {
                color: blue;
            }

        .link-button {
            margin-top: 15px;
            max-width: 90px;
            /*background-color: aquamarine;
            /border-color: black;
            color: #333;*/
            display: inline-block;
            vertical-align: middle;
            text-align: center;
            text-decoration: none;
            align-items: flex-start;
            cursor: default;
            -webkit-appearence: push-button;
            border-style: solid;
            border-width: 1px;
            border-radius: 5px;
            font-size: 1em;
            font-family: inherit;
            border-color: floralwhite;
            padding-left: 5px;
            padding-right: 5px;
            width: 100%;
            min-height: 30px;
        }

            .link-button:hover {
                background-color: skyblue;
                border-color: black;
            }

        .Position {
            /*Style="z-index: 103; position: absolute; top: 280px;left: 850;"*/
            position: absolute /*absolute*/;
            top: 300px;
            left: 30%;
            z-index: 999;
        }

        span:hover {
            cursor: pointer;
        }

        .parentDisable {
            z-index: 9;
            width: 100%;
            height: 100%;
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            background-color: #000;
            color: #aaa;
            opacity: .4;
            filter: alpha(opacity=50);
        }
    </style>

    <script type="text/javascript" language="javascript">//disable back button

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>

    <script type="text/javascript">
        function CheckSingleCheckbox(ob) {
            var grid = ob.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (ob.checked && inputs[i] != ob && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }
    </script>

    <script type="text/javascript">
        function Confirmationbox() {
            var result = confirm('Are you sure you want to delete selected User(s)?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function SetColor(id) {
            var links = document.getElementsByTagName("<a>")
            for (var i = 0; i < links.length; i++) {
                links[i].style.color = "Black";
            }
            document.getElementById(id).style.color = "red";
        }
    </script>

    <script type="text/javascript">
        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.visibility == 'hidden')
                e.style.visibility = 'visible';

            else
                e.style.visibility = 'hidden';
            $("#pop1").css("display", "none");

            return false;
        }
    </script>

    <script type="text/javascript">

        function ShowDiv1(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>

    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>

    <script type="text/javascript">
        function pop(div) {
            document.getElementById(div).style.display = 'block';
            $("#PopUpDiv").css("visibility", "visible");
            return false;
        }
        function hide(div) {
            document.getElementById(div).style.display = 'none';
            $("#PopUpDiv").css("visibility", "hidden");
            return false;
        }
    </script>


    <%--<script type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });
        function SearchText() {
            $("#txtSearchBox").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "AddContactVertical.aspx/GetEmployeeName",
                        data: "{'empName':'" + document.getElementById('txtSearchBox').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }
    </script>--%> 
</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Debug"></asp:ToolkitScriptManager>

        <div style="height: 1000px; width: 1920px">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtSearchBox" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150" OnTextChanged="txtSearchBox_TextChanged"></asp:TextBox>

                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon  app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server">
                                                        <ul class="menu-group" id="ext-gen3448">
                                                            <li class="menu-item menu-app-item app-item item-child  x-item-selected active" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%-- <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>
                                                --%>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv1('Div1')">Rapporter</span></span>

                                                    <div id="Div1" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv1('Div2')">Løn Modul</span></a>
                                                    <div id="Div2" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>

                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                --%>
                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btn_SendMail" />
                    </Triggers>
                    <ContentTemplate>
                        <div style="float: left; height: 1000px; width: 300px; border-color: silver; border-width: 5px">
                            <fieldset style="height: 1000px">
                                <legend style="color: black; font-weight: bold; font-size: small"></legend>
                                <div style="height: 10px;">
                                </div>
                                <table style="width: 100%; font-family: Calibri;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 50%;">
                                                <asp:Button CssClass="button" ID="BtnAddEmployee" runat="server" Text="+Tilføj Bruger" BackColor="#CC6699" Style="color: #FFFFFF; font-weight: 500; font-size: medium; background-color: #FF0000" OnClick="BtnAddEmployee_Click" />
                                            </td>
                                            <td style="width: 50%;">
                                                <table class="x-table-layout" id="ext-gen1688" role="presentation" cellspacing="0" cellpadding="0">
                                                    <tbody role="presentation">
                                                        <%-- <tr role="presentation">
                                                            <td class="x-table-layout-cell " role="presentation" rowspan="1" colspan="1"><a tabindex="0" class="x-btn x-unselectable x-sbtn-first x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon" id="button-1109" hidefocus="on" unselectable="on" data-qtip="Simple List"><span class="x-btn-wrap" id="button-1109-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1109-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1109-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-split " id="button-1109-btnIconEl" role="presentation" unselectable="on">&nbsp;</span></span></span></a></td>
                                                            <td class="x-table-layout-cell " role="presentation" rowspan="1" colspan="1"><a tabindex="0" class="x-btn x-unselectable x-sbtn-last x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon x-pressed x-btn-pressed x-btn-default-small-pressed" id="button-1110" hidefocus="on" unselectable="on" data-qtip="Advanced List"><span class="x-btn-wrap" id="button-1110-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1110-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1110-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-no-split " id="button-1110-btnIconEl" role="presentation" unselectable="on"></span></span></span></a></td>
                                                        </tr>--%>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px;" colspan="2"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div style="height: 850px">
                                    <fieldset>
                                        <legend style="color: black; font-weight: bold; font-size: large">Brugernavn</legend>
                                        <div id="divOverflowEmp" runat="server" style="height: 840px">
                                            <asp:GridView ID="GridViewEmployee" runat="server" AutoGenerateColumns="False" ShowHeader="false" DataKeyNames="EmpId" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" RowStyle-Height="40px" RowStyle-BorderWidth="5px" BorderColor="White" BorderWidth="0px" RowStyle-BorderColor="White" OnRowCommand="GridViewEmployee_RowCommand" OnRowDataBound="Bound">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="10px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" onclick="CheckSingleCheckbox(this)" Visible="false" />
                                                            <asp:Label ID="EmpId" runat="server" Text='<%# Bind("EmpId") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="FName" runat="server" Text='<%# Bind("FName")%>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10px" BorderWidth="0" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="EmpId" HeaderText="EmpId" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />
                                                    <%-- <asp:BoundField DataField="FName" HeaderText="Employee Name" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />--%>

                                                    <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LBtnEmployeeName" runat="server" Font-Underline="false" Text='<%# Bind("FName")%>' OnClick="LBtnEmployeeName_Click"> </asp:LinkButton></td>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="250px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <span onclick="return confirm('Are you sure to Delete the Customer?')">
                                                                <asp:LinkButton CssClass="simple" ID="Lnkdeleterow" runat="server" Text="" Font-Underline="false" CommandArgument='<%#Eval("EmpId") %>' CommandName="DeleteRow"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton></span>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </fieldset>
                                </div>
                            </fieldset>
                        </div>
                        <div id="divVertical" style="float: left; height: 1000px; width: 1420px" runat="server">
                            <div style="width: 100%; height: 90px;">

                                <div style="height: 10px"></div>
                                <div style="height: 40px">
                                    <div class="divider" style="height: 30px; width: 50px"></div>
                                    <asp:Label ID="LblEmpName" runat="server" CssClass="clienthead"></asp:Label>
                                    <asp:Label ID="LblEmpId" runat="server" Text="" Visible="false"></asp:Label>
                                </div>

                                <div style="height: 40px">
                                    <table>
                                        <tr>
                                            <td style="height: 30px; width: 50px"></td>
                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnOverview" runat="server" Font-Underline="false" OnClick="LBtnOverview_Click" ForeColor="RoyalBlue">Oversigt</asp:LinkButton></td>
                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnBasicInfo" runat="server" Font-Underline="false" OnClick="LBtnBasicInfo_Click" ForeColor="RoyalBlue">Stamdata</asp:LinkButton></td>

                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnNotes" runat="server" Font-Underline="false" OnClick="LBtnNotes_Click" ForeColor="RoyalBlue">Noter</asp:LinkButton></td>

                                            <td style="height: 30px; width: 130px">
                                                <asp:LinkButton CssClass="link-button" ID="lbtnSendMail" runat="server" Font-Underline="false" OnClick="lbtnSendMail_Click" ForeColor="RoyalBlue">e-mail</asp:LinkButton></td>

                                            <%-- <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnOrders" runat="server" Font-Underline="false" OnClick="LBtnOrders_Click">Orders</asp:LinkButton></td>
                                    <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnQuotes" runat="server" Font-Underline="false" OnClick="LBtnQuotes_Click">Quotes</asp:LinkButton></td>
                                    <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnProjects" runat="server" Font-Underline="false" OnClick="LBtnProjects_Click">Projects</asp:LinkButton></td>--%>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="gridv" id="divOverView" runat="server" style="height: 900px;" visible="true">
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <%--<div style="height: 40px">
                                <div class="divider"></div>
                                <asp:Button CssClass="buttonn" ID="BtnBack" runat="server" Text="Back" BackColor="#009933" />
                                <div class="divider"></div>
                                <asp:Button CssClass="buttonn" ID="BtnContact" runat="server" Text="Click OverView" BackColor="#009933" />
                            </div>--%>
                                    <div style="height: 30px"></div>
                                    <div style="height: 820px; width: 100%">
                                        <%--<asp:Image ID="Image1" runat="server" Height="780" Width="100%" ImageUrl="~/image/dashbord.png" />--%>
                                        <%--<table>
                                    <tr>
                                        <td>
                                            <asp:Chart ID="Chart3" runat="server" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)"
                                                Palette="BrightPastel" ImageType="Png" BorderDashStyle="Solid" BackSecondaryColor="White"
                                                BackGradientStyle="TopBottom" BorderWidth="2" BackColor="#D3DFF0" BorderColor="26, 59, 105"
                                                Height="320px" Width="700px">

                                                <BorderSkin SkinStyle="Emboss" />
                                                <Legends>
                                                    <asp:Legend Name="Legend1" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold"
                                                        IsTextAutoFit="False">
                                                    </asp:Legend>
                                                </Legends>
                                                <Titles>
                                                    <asp:Title Alignment="TopCenter" BackColor="180, 165, 191, 228" BackGradientStyle="TopBottom"
                                                        BackHatchStyle="None" Font="Microsoft Sans Serif, 12pt, style=Bold" Name="Revenue"
                                                        Text="Daily Project Work" ToolTip="Revenue" ForeColor="26, 59, 105">
                                                    </asp:Title>

                                                    <asp:Title Alignment="TopCenter" BackColor="Transparent" Font="Microsoft Sans Serif, 10pt, style=Bold "
                                                        ToolTip="Chart Type">
                                                    </asp:Title>

                                                </Titles>

                                                <Series>
                                                    <asp:Series Name="Count" ChartArea="ChartArea1" Legend="Legend1" CustomProperties="DrawingStyle=Cylinder"
                                                        BorderColor="64, 0, 0, 0" Color="224, 64, 10" MarkerSize="5">
                                                        <EmptyPointStyle AxisLabel="0" />
                                                    </asp:Series>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" BackColor="64, 165, 191, 228" BackSecondaryColor="White"
                                                        BorderColor="64, 64, 64, 64" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                                        <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="False" Title="Total Cost" ArrowStyle="Triangle" Minimum="1000" Maximum="10000" Interval="1000">
                                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                                        </AxisY>
                                                        <AxisX IsLabelAutoFit="False" LineColor="64, 64, 64, 64" Title="Days" ArrowStyle="Triangle"
                                                            Interval="1">
                                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" IsEndLabelVisible="False" Angle="0" />
                                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </td>
                                    </tr>
                                </table>--%>
                                    </div>

                                </fieldset>
                            </div>
                            <div id="DivBasicEmpInfo" style="width: 1420px; float: left; height: 900px;" runat="server" visible="false">

                                <fieldset style="height: 858px">

                                    <legend style="color: black; font-weight: bold; font-size: large"></legend>


                                    <table style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td colspan="3" style="height: 80px; font-weight: bold; font-size: large; padding-left: 3em">Stamdata</td>
                                            </tr>
                                            <tr>
                                                <td class="bg">First navn</td>

                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtempName" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtempName" ValidationGroup="Users" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender1" Enabled="true" TargetControlID="txtempName" FilterType="UppercaseLetters, LowercaseLetters" FilterMode="InvalidChars" InvalidChars=" "></asp:FilteredTextBoxExtender>

                                                </td>

                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Last navn                                            
                                                </td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtlname" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender2" Enabled="true" TargetControlID="txtlname" FilterType="UppercaseLetters, LowercaseLetters" FilterMode="InvalidChars" InvalidChars=" "></asp:FilteredTextBoxExtender>

                                                </td>

                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>

                                            <tr>
                                                <td class="bg">Wage Per Hour</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtWage" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Requiredwageperhr" runat="server" ErrorMessage="*" ControlToValidate="txtWage" ValidationGroup="User" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtWage" ValidationExpression="\d+" ValidationGroup="User"></asp:RegularExpressionValidator>--%>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx1" Enabled="true" TargetControlID="txtWage" ValidChars=",.0123456789" FilterMode="ValidChars"></asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>

                                            <tr>
                                                <td class="bg">e-mail </td>
                                                <td style="width: 40px"></td>

                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmail" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Please Enter Valid E-mail" ControlToValidate="txtEmail" CssClass="requiredFieldValidateStyle" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="User">
                                                    </asp:RegularExpressionValidator>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx2" Enabled="true" FilterMode="ValidChars" FilterType="Custom, Numbers,LowercaseLetters" ValidChars=",.@ _-" TargetControlID="txtEmail"></asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Adresse</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtAddress" runat="server" Width="300px" Height="50px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">By</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtCity" runat="server" Width="300px" Height="31px"></asp:TextBox>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Postnr</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtZipcode" runat="server" Width="300px" Height="31px"></asp:TextBox>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Mobil:</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMobile" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Min 8 & Max 12 digit required." Font-Size="Small" ControlToValidate="txtMobile" ValidationExpression="^[\s\S]{8,12}$" ValidationGroup="User"></asp:RegularExpressionValidator>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx3" TargetControlID="txtMobile" FilterMode="ValidChars" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Tif</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtphone" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Min 8 & Max 12 digit required." Font-Size="Small" ControlToValidate="txtphone" ValidationExpression="^[\s\S]{8,12}$" ValidationGroup="User"></asp:RegularExpressionValidator>

                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx4" TargetControlID="txtphone" FilterType="Numbers" FilterMode="ValidChars"></asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Occupation</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtOccupation" runat="server" Width="300px" Height="31px"></asp:TextBox>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Område</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtProvince" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender6" Enabled="true" FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="txtProvince"></asp:FilteredTextBoxExtender>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">SkillSets</td>
                                                <td style="width: 40px"></td>
                                                <td>
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtSkillset" runat="server" Width="300px" Height="50px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </fieldset>
                                <div class="shadowdiv" style="width: 100%; height: 50px; text-align: center; border: solid 0px black;">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <asp:Button CssClass="buttonn" ID="BtnUpdate" runat="server" Text="Opdater " Width="101px" Style="background-color: #009900" OnClick="BtnUpdate_Click1" ValidationGroup="User" />
                                                </td>
                                                <td>
                                                    <asp:Button CssClass="buttonn" ID="BtndeleteEmployee" runat="server" Text="Slet" Width="101px" Style="background-color: #009933" OnClick="BtndeleteEmployee_Click" OnClientClick="javascript:return Confirmationbox();" Visible="false" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <asp:GridView CssClass="gridv" ID="GridViewBasicInfo" AutoGenerateColumns="false" Visible="false" runat="server">
                                </asp:GridView>
                            </div>
                            <div id="DivNotes" runat="server" style="width: 1420px; float: left; height: 900px;" visible="false">
                                <fieldset style="height: 858px">

                                    <div style="height: 40px; width: 100%;">
                                        <div runat="server" style="height: 40px; width: 52px; display: inline-block;"></div>
                                        <asp:Button ID="BtnUserNote" runat="server" Text="+Tilføj noter" CssClass="buttonn" OnClick="BtnUserNote_Click" /><%--OnClientClick="return pop('pop1')"--%>
                                        <asp:Label ID="LabelNoteId" runat="server" Text="" Visible="false"></asp:Label>
                                    </div>
                                    <div style="height: 30px;"></div>

                                    <div id="PopUpDiv" runat="server" class="Position" style="height: 400px; width: 600px; bottom: 390px; background-color: #fbfbfb; visibility: hidden">
                                        <fieldset style="height: 400px; width: 600px">
                                            <div style="height: 100%; width: 100%">
                                                <div style="height: 50px; width: 100%; background-color: lightblue;">
                                                    <div style="height: 30px; width: 10px; display: inline-block;"></div>
                                                    <asp:Label ID="LblNoteHeading" runat="server" Font-Size="Large"></asp:Label>
                                                    <span class="menu-item-icon icon-times-circle-o" style="float: right;" onclick="ShowDiv('PopUpDiv')"></span>
                                                </div>
                                                <div style="height: 300px; width: 100%;">
                                                    <table style="width: 100%;">
                                                        <tbody>
                                                            <tr style="height: 30px;">
                                                                <td colspan="3"></td>
                                                            </tr>
                                                            <tr style="height: 30px;">
                                                                <td style="width: 20%; text-align: center">
                                                                    <asp:Label ID="LblNoteName" runat="server" Text="Overskrift:"></asp:Label>
                                                                </td>
                                                                <td style="width: 5%;"></td>
                                                                <td style="width: 75%;">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtNoteName" runat="server" Width="90%" Height="36"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="height: 20px;">
                                                                <td colspan="3"></td>
                                                            </tr>
                                                            <tr style="height: 200px;">
                                                                <td style="width: 20%; vertical-align: top; text-align: center">
                                                                    <asp:Label ID="LblNoteDiscription" runat="server" Text="Beskrivelse:"></asp:Label>
                                                                </td>
                                                                <td style="width: 5%;"></td>
                                                                <td style="width: 75%; vertical-align: top;">
                                                                    <textarea id="txtNoteDescr" style="resize: none; overflow-y: hidden; height: 170px; width: 90%; font-size: small;" runat="server"></textarea>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div style="height: 50px; width: 100%;">
                                                    <div style="height: 20px; width: 10px; display: inline-block"></div>
                                                    <asp:Button CssClass="twitterStyleTextbox" ID="BtnSaveNote" runat="server" Text="GEM" Height="30" Width="70" OnClick="BtnSaveNote_Click" />
                                                    <asp:Button CssClass="twitterStyleTextbox" ID="BtnUpdateNote" runat="server" Text="OPDATER" Height="30" Width="70" OnClick="BtnUpdateNote_Click" />
                                                    <asp:Button CssClass="twitterStyleTextbox" ID="BtnCancelNote" runat="server" Text="SLET" Height="30" Width="70" OnClientClick="return hide('pop1')" />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div style="height: 780px;">
                                        <div id="NoteHeader" runat="server" class="allSides" style="" visible="false">
                                            <table style="font-family: Calibri; width: 100%">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 5%; font-size: large;">Nr:</td>
                                                    <td style="width: 25%; font-size: large;">Overskrift</td>
                                                    <td style="width: 60%; font-size: large;">Beskrivelse</td>
                                                    <td id="TdView" runat="server" style="width: 5%; font-size: large;">Vis</td>
                                                    <td id="TdDelete" runat="server" style="width: 5%; font-size: large;">Slet</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 745px; overflow: auto;">

                                            <table style="font-family: Calibri; width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="grdviewNote" runat="server" AutoGenerateColumns="False" ShowHeader="false" Width="100%" RowStyle-Height="30px" OnRowCommand="grdviewNote_RowCommand">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblUsrid" runat="server" Text='<%# Bind("UID") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblIndexId" runat="server" Text='<%#Container.DataItemIndex + 1%>' Visible="true"></asp:Label>
                                                                        <asp:Label ID="lblNoteId" runat="server" Text='<%# Bind("NOTEID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="5px" BorderColor="White" />
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="NoteName" HeaderText="Title" ItemStyle-Width="25%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <%--  <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-Width="60%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />--%>

                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDescription" runat="server" Wrap="true" Rows="1" Text='<%# Bind("Description") %>' ReadOnly="true" Style="overflow: auto;" TextMode="MultiLine" BorderStyle="None" Width="100%" BackColor="#fbfbfb"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="60%" BorderWidth="5px" BorderColor="White" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="lbltnViewNote" runat="server" Font-Underline="false" CommandArgument='<%#Eval("NOTEID") %>' CommandName="ViewNote"><span class="action-icon icon-eye"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="5px" BorderColor="White" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="lbtnDeleteNote" runat="server" Font-Underline="false" CommandArgument='<%#Eval("NOTEID") %>' CommandName="DeleteRow"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="5px" BorderColor="White" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="" id="divSendMail" runat="server" style="height: 900px;" visible="false">
                                <div style="width: 100%; height: 900px;">

                                    <fieldset style="width: 100%; height: 858px;">

                                        <legend style="color: black; font-weight: bold; font-size: large"></legend>
                                        <table style="width: 100%;">

                                            <tbody>
                                                <tr>
                                                    <td colspan="3" style="height: 100px"></td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 20%;"></td>
                                                    <td class="divcol" style="width: 60%;">

                                                        <table class="table1" style="width: 100%;">

                                                            <tr>
                                                                <td style="height: 15px; text-align: center; font-weight: bold; font-size: large">Ny e-mail</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div>
                                                                        <div style="margin: 10px 5px; padding-top: 10px;">
                                                                            <div>
                                                                                Til :
                                                                            </div>
                                                                            <div>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 5%;"></td>
                                                                                        <td style="width: 90%;">
                                                                                            <asp:TextBox ID="txt_To" CssClass="twitterStyleTextbox" runat="server" Width="100%" Height="40"></asp:TextBox></td>
                                                                                        <td style="width: 5%;"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 15px">
                                                                    <div>
                                                                        <div style="margin: 10px 5px; padding-top: 10px;">
                                                                            <div>
                                                                                Emne :
                                                                            </div>
                                                                            <div>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 5%;"></td>
                                                                                        <td style="width: 90%;">
                                                                                            <asp:TextBox ID="txt_Subject" CssClass="twitterStyleTextbox" runat="server" Width="100%" Height="40"></asp:TextBox></td>
                                                                                        <td style="width: 5%;"></td>
                                                                                    </tr>
                                                                                </table>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 15px">
                                                                    <div>
                                                                        <div style="margin: 10px 5px; padding-top: 10px;">
                                                                            <div>
                                                                                Fil :
                                                                            </div>
                                                                            <div>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 5%;"></td>
                                                                                        <td style="width: 90%;">
                                                                                            <asp:FileUpload ID="FileUpload1" runat="server" Height="40" Width="750px" />
                                                                                        </td>
                                                                                        <td style="width: 5%;"></td>
                                                                                    </tr>
                                                                                </table>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 15px">
                                                                    <div>
                                                                        <div style="margin: 10px 5px; padding-top: 10px;">
                                                                            <div>
                                                                                Besked :
                                                                            </div>
                                                                            <div>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 5%;"></td>
                                                                                        <td style="width: 90%;">
                                                                                            <asp:TextBox ID="txt_Message" CssClass="twitterStyleTextbox" runat="server" Width="100%" Height="200px" TextMode="MultiLine"></asp:TextBox></td>
                                                                                        <td style="width: 5%;"></td>
                                                                                    </tr>
                                                                                </table>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 15px;">
                                                                    <div style="text-align: center;">
                                                                        <asp:Button ID="btn_SendMail" CssClass="buttonn" runat="server" OnClick="btn_SendMail_Click" Text="Send" BackColor="#009933" />
                                                                        <asp:Button ID="btn_cancel" CssClass="buttonn" runat="server" Text="Slet" OnClick="btn_cancel_Click" BackColor="Red" />
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                        </table>

                                                    </td>
                                                    <td style="width: 20%;"></td>
                                                </tr>

                                                <tr>
                                                    <td colspan="3" style="height: 50px"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="height: 50px; text-align: center;">
                                                        <asp:Label ID="lblResult" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>

                            <div id="pop1" class="parentDisable"></div>
                            <asp:Label ID="LblUseType" runat="server" Text="" Visible="false"></asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
