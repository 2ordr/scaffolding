﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;
using System.Collections;

namespace ProjectManagement
{
    public partial class StockInfo : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    Label1.Text = Session["username"].ToString();
                }
                ViewState["PreviousPage"] = Request.UrlReferrer;
            }
        }

        protected void BtnADD_Click(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_InsertStockGroup";
            com.Parameters.Add("@GNAME", System.Data.SqlDbType.NVarChar).Value = txtStockGroup.Text;

            com.ExecuteNonQuery();
            com.Parameters.Clear();

            txtStockGroup.Text = "";
            con.Close();
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            
            //Response.Redirect("Stocks.aspx");
            if (ViewState["PreviousPage"] != null)	//Check if the ViewState 
                //contains Previous page URL
                //{
                Response.Redirect(ViewState["PreviousPage"].ToString());//Redirect to 
            //    Previous page by retrieving the PreviousPage Url from ViewState.
            //}


        }

        protected void BtnBack1_Click(object sender, EventArgs e)
        {
            //Response.Redirect("Stocks.aspx");
            if (ViewState["PreviousPage"] != null)	//Check if the ViewState 
            //contains Previous page URL
            {
                Response.Redirect(ViewState["PreviousPage"].ToString());//Redirect to 
                //Previous page by retrieving the PreviousPage Url from ViewState.
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_InsertstockItemPro";
            com.Parameters.Add("@ItemProName", System.Data.SqlDbType.NVarChar).Value = txtItemProperty.Text;

            com.ExecuteNonQuery();
            com.Parameters.Clear();

            txtItemProperty.Text = "";
        }
    }
}