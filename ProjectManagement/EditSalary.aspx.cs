﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Net;
using System.Net.Mail;
using System.Text;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Threading;
using System.Globalization;

namespace ProjectManagement
{
    public partial class EditSalary : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        public Decimal ATotalFees = 0;
        Boolean Flag = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {

                    Label1.Text = Session["username"].ToString();
                    LoginName.Text = Label1.Text;
                    //LblUserId.Text = Session["UserId"].ToString();

                    string projectid = Session["Projectid"].ToString();
                    string type = Session["Type"].ToString();
                    string dtEntrydt = Session["EntryDate"].ToString();
                    string empID = Session["Empid"].ToString();
                    string Status = "Hold";

                    DateTime dt11 = DateTime.ParseExact(dtEntrydt, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    DateTime orddate = DateTime.Now;
                    lblSystemDataTime.Text = Convert.ToString(orddate);
                    lblSystemDataTime.Text = string.Format("{0:dd-MM-yyyy hh:mm:ss}", orddate);
                    txtStartTime.Text = string.Format("{0:dd-MM-yyyy}", dt11);
                    Session["date"] = txtStartTime.Text;

                    BtnStopttime.Visible = false;
                    BtnStarttime.Enabled = false;

                    BindProjectDeetails();
                    BindUserDetails();
                    //BtnStopttime.Visible = false;
                    //BtnStarttime.Enabled = false;
                    //ddProjectList.Items.Insert(0, ("-Select-"));

                    SqlConnection con11 = new SqlConnection(cs);
                    SqlCommand cmd11 = new SqlCommand();
                    cmd11.Connection = con11;
                    con11.Open();
                    cmd11.CommandType = System.Data.CommandType.Text;
                    cmd11.Parameters.Clear();
                    cmd11.CommandText = "Delete FROM TempSalaryTask";
                    cmd11.ExecuteNonQuery();
                    cmd11.Parameters.Clear();

                    string dtEntryDate = txtStartTime.Text;
                    DateTime Entrydt = DateTime.ParseExact(dtEntryDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    cmd11.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd11.CommandText = "sp_getSalaryReportNew";
                    cmd11.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dtEntryDate;
                    cmd11.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = projectid;
                    cmd11.Parameters.Add("@Type", System.Data.SqlDbType.VarChar).Value = type;
                    cmd11.Parameters.Add("@Flag", System.Data.SqlDbType.Bit).Value = Flag;
                    cmd11.Parameters.Add("@I_EmpID", System.Data.SqlDbType.BigInt).Value = empID;
                    cmd11.Parameters.Add("@Status", System.Data.SqlDbType.VarChar).Value = Status;

                    SqlDataAdapter adpt = new SqlDataAdapter(cmd11);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        ddProjectList.SelectedValue = projectid;
                        lblProjectId.Text = projectid;
                        if (type == "Hours")
                        {
                            RBtnUseQuate.Checked = true;
                        }
                        else if (type == "LightWeight")
                        {
                            RBtnUseLight.Checked = true;
                            BindStock();
                        }
                        else if (type == "HeavyWeight")
                        {
                            RBtnUseHeavy.Checked = true;
                            BindHeavyStock();
                        }
                        //txtStartTime.Text = Convert.ToString(dt11);

                        //GviewProjectStock.DataSource = dt;
                        //GviewProjectStock.DataBind();
                        GridSalary.DataSource = dt;
                        GridSalary.DataBind();
                        divSalary.Visible = true;

                        dt.Tables[0].Rows.Clear();
                        cmd11.Parameters.Clear();

                        cmd11.CommandType = System.Data.CommandType.Text;
                        cmd11.CommandText = "Select distinct * from EMPLOYEE,ASSIGNED,PROJECTS where ASSIGNED.ProjectId=PROJECTS.ProjectId and EMPLOYEE.EmpId=ASSIGNED.EmpId and PROJECTS.ProjectId='" + projectid + "' and EMPLOYEE.EmpId='" + empID + "'";
                        adpt = new SqlDataAdapter(cmd11);
                        adpt.Fill(dt);
                        if (dt.Tables[0].Rows.Count > 0)
                        {
                            gridUsers.DataSource = dt;
                            gridUsers.DataBind();
                            divUsers.Visible = true;
                        }

                        //BindStock();

                        dt.Tables[0].Rows.Clear();
                        cmd11.Parameters.Clear();
                        //LblItemCode.Text = dt.Tables[0].Rows[0]["ItemCode"].ToString();
                    }

                    con11.Close();

                }
            }
        }

        public void BindStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from STOCKSITEM where UseLight = 1", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GviewProjectStock.DataSource = dt;

                GviewProjectStock.DataBind();
                finishDiv.Visible = true;
            }

            dt.Rows.Clear();
            con.Close();
        }

        public void BindHeavyStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from STOCKSITEM where UseHeavy = 1", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GviewProjectStock.DataSource = dt;

                GviewProjectStock.DataBind();
                finishDiv.Visible = true;
            }
            dt.Rows.Clear();
            con.Close();
        }

        public void BindProjectDeetails()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from PROJECTS", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                ddProjectList.DataSource = dt;
                ddProjectList.DataTextField = "ProjectName";
                ddProjectList.DataValueField = "ProjectId";
                ddProjectList.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Customer had Not Created any Project');", true);
                return;
            }

            con.Close();
        }

        //03.11.15
        public void BindUserDetails()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            //com.CommandText = "select * from PROJECTS where ProjectId= " + ddlCurrentDropDownList.SelectedValue;
            com.CommandText = "Select distinct * from EMPLOYEE";
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                ddlUser.DataSource = dt;
                ddlUser.DataTextField = "FName";
                ddlUser.DataValueField = "EmpId";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, ("-Vælg-"));
            }
        }

        protected void RBtnUseQuate_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseLight.Checked = false;
            divUsers.Visible = true;
            LblGrantTotal.Text = "";
            btnView.Visible = false;
            finishDiv.Visible = false;
        }

        protected void RBtnUseLight_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseQuate.Checked = false;
            btnView.Visible = false;
            LblGrantTotal.Text = "";
            BindStock();
            finishDiv.Visible = true;
        }

        protected void RBtnUseHeavy_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseLight.Checked = false;
            RBtnUseQuate.Checked = false;
            divUsers.Visible = true;
            LblGrantTotal.Text = "";
            btnView.Visible = false;
            BindHeavyStock();
            finishDiv.Visible = true;
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            if (ddProjectList.SelectedItem.Text != "-Vælg-")
            {
                if (ddlUser.SelectedItem.Text != "-Vælg-")
                {
                    string dtEntry1 = txtStartTime.Text;
                    DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "DELETE FROM ASSIGNED WHERE ProjectId=@PROJECTID AND EMPID=@EMPID";
                    com.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = ddProjectList.SelectedValue;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = ddlUser.SelectedValue;
                    com.ExecuteNonQuery();

                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_UpdateAssigned";
                    com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = 0;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = ddlUser.SelectedValue;
                    com.Parameters.Add("@PROJECTID  ", System.Data.SqlDbType.BigInt).Value = ddProjectList.SelectedValue;
                    com.Parameters.Add("@ASSIGNEDDATE", System.Data.SqlDbType.DateTime).Value = dt11;
                    //com.Parameters.Add("@ASSIGNEDDATE", System.Data.SqlDbType.DateTime).Value = dtp1.CalendarDateString;          
                    com.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = 0;
                    com.Parameters.Add("@ESTIMATEDHOURS", System.Data.SqlDbType.Decimal).Value = 0;
                    com.Parameters.Add("@LABORCHARGES", System.Data.SqlDbType.Decimal).Value = 0;
                    com.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = 0;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "Select distinct * from EMPLOYEE,ASSIGNED,PROJECTS where ASSIGNED.ProjectId=PROJECTS.ProjectId and EMPLOYEE.EmpId=ASSIGNED.EmpId and PROJECTS.ProjectId= " + ddProjectList.SelectedValue;
                    SqlDataAdapter adpt = new SqlDataAdapter(com);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        gridUsers.DataSource = dt;
                        gridUsers.DataBind();
                    }
                    com.Parameters.Clear();
                    dt.Tables[0].Rows.Clear();
                }
            }
            con.Close();
        }

        protected void gridUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            string empID = Session["Empid"].ToString();
            string type = "";
            string Status = "Hold";

            foreach (GridViewRow Srows in gridUsers.Rows)
            {

                if (Srows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblEmpid = (Srows.Cells[1].FindControl("lblEmpid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.CheckBox chkUser = (Srows.Cells[0].FindControl("chkUser") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.TextBox txtHours = (Srows.Cells[2].FindControl("txtHours") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.TextBox txtAHours = (Srows.Cells[3].FindControl("txtAHours") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblHours = (Srows.Cells[2].FindControl("lblHours") as System.Web.UI.WebControls.Label);

                    if (lblEmpid.Text == empID)
                    {
                        string dtEntry1 = txtStartTime.Text;
                        DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        com.CommandType = System.Data.CommandType.Text;
                        com.CommandText = "SELECT Empid,TOTALITEMFEES,Type,Hours,AHours,ContributionFees FROM [SALARYREPORT] WHERE ([EmpId] = @I_EMPID AND [EntryDate]= @DATE AND [ProjectId]= @I_PROJECTID AND [Type]=@TYPE AND [Status]=@Status)";
                        com.Parameters.AddWithValue("@I_EMPID", lblEmpid.Text);
                        com.Parameters.AddWithValue("@DATE", dt11);
                        com.Parameters.AddWithValue("@I_PROJECTID", ddProjectList.SelectedValue);
                        com.Parameters.AddWithValue("@Status", Status);

                        if (RBtnUseQuate.Checked == true)
                        {
                            type = "Hours";
                        }
                        else if (RBtnUseLight.Checked == true)
                        {
                            type = "LightWeight";
                        }
                        else if (RBtnUseHeavy.Checked == true)
                        {
                            type = "HeavyWeight";
                        }
                        com.Parameters.AddWithValue("@TYPE", type);

                        SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                        DataSet dt1 = new DataSet();
                        adpt1.Fill(dt1);
                        if (dt1.Tables[0].Rows.Count > 0)
                        {
                            txtHours.Text = dt1.Tables[0].Rows[0]["Hours"].ToString();
                            txtAHours.Text = dt1.Tables[0].Rows[0]["AHours"].ToString();
                            decimal AHrs = Convert.ToDecimal(txtAHours.Text);

                            chkUser.Checked = true;

                            if (AHrs > 0)
                            {
                                txtAHours.Text = txtAHours.Text;
                            }
                            else
                            {
                                txtAHours.Text = txtHours.Text;
                            }

                        }
                        com.Parameters.Clear();
                        dt1.Tables[0].Rows.Clear();
                    }
                }
            }

        }

        protected void GviewProjectStock_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            string type = "";
            foreach (GridViewRow Srows in GviewProjectStock.Rows)
            {

                if (Srows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.TextBox txtQuntity = (Srows.Cells[0].FindControl("txtQuntity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblQuntity = (Srows.Cells[0].FindControl("lblQuntity") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label LblItemcode = (Srows.Cells[0].FindControl("LblItemcode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label ItemName = (Srows.Cells[1].FindControl("ItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.DropDownList DDLItemName = (Srows.Cells[1].FindControl("DDLItemName") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.Label ItemFees = (Srows.Cells[2].FindControl("ItemFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtItemFees = (Srows.Cells[2].FindControl("txtItemFees") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label LblTotAmt = (Srows.Cells[3].FindControl("LblTotAmt") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label ALblTotAmt = (Srows.Cells[3].FindControl("ALblTotAmt") as System.Web.UI.WebControls.Label);

                    if (RBtnUseLight.Checked == true)
                    {
                        type = "LightWeight";
                    }
                    else if (RBtnUseHeavy.Checked == true)
                    {
                        type = "HeavyWeight";
                    }

                    string dtEntry1 = txtStartTime.Text;
                    DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_getSalaryStockReport";
                    //com.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dtEntry1;
                    com.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                    com.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = ddProjectList.SelectedValue;
                    com.Parameters.Add("@Type", System.Data.SqlDbType.VarChar).Value = type;
                    //com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = ddProjectList.SelectedValue;
                    com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = LblItemcode.Text;

                    SqlDataAdapter adpt = new SqlDataAdapter(com);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        string itemcode = dt.Tables[0].Rows[0]["ItemCode"].ToString();
                        if (LblItemcode.Text == itemcode)
                        {
                            txtQuntity.Text = dt.Tables[0].Rows[0]["Quantity"].ToString();
                            LblTotAmt.Text = dt.Tables[0].Rows[0]["SubTotal"].ToString();
                            ALblTotAmt.Text = LblTotAmt.Text;
                            lblQuntity.Text = txtQuntity.Text;
                        }
                        LblGrantTotal.Text = dt.Tables[0].Rows[0]["TOTAL"].ToString();
                        ATotalFees = Convert.ToDecimal(LblGrantTotal.Text);
                    }
                    com.Parameters.Clear();
                    dt.Tables[0].Rows.Clear();

                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "SELECT M.ItemCode,M.ItemName,M.ItemFees,M.ICODE FROM MULTIVALUESTOCK M,STOCKSITEM S WHERE M.ItemCode=S.ItemCode AND M.ITEMCODE=@ITEMCODE AND M.StockType=@STOCKTYPE";
                    com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = LblItemcode.Text;
                    com.Parameters.Add("@STOCKTYPE", System.Data.SqlDbType.VarChar).Value = type;

                    adpt.SelectCommand = com;
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        DDLItemName.DataSource = dt;
                        DDLItemName.DataTextField = "ItemName";
                        DDLItemName.DataValueField = "ICODE";
                        DDLItemName.DataBind();
                        ItemName.Visible = false;
                        DDLItemName.Visible = true;

                    }
                    com.Parameters.Clear();
                    dt.Tables[0].Rows.Clear();
                }
            }
            con.Close();
        }

        protected void txtQuntity_TextChanged(object sender, EventArgs e)
        {
            TextBox txtQuntity = (TextBox)sender;
            GridViewRow grdrRow = ((GridViewRow)txtQuntity.Parent.Parent);
            Label ItemFees = (Label)grdrRow.FindControl("ItemFees");
            Label LblTotAmt = (Label)grdrRow.FindControl("LblTotAmt");
            Label lblQuntity = (Label)grdrRow.FindControl("lblQuntity");
            Label ALblTotAmt = (Label)grdrRow.FindControl("ALblTotAmt");
            //TextBox txtSubtotal = (TextBox)grdrRow.FindControl("txtSubtotal");
            if (txtQuntity.Text != "")
            {
                if (ItemFees.Text != "")
                {
                    decimal Price = Convert.ToDecimal(ItemFees.Text);
                    long Aqty = Convert.ToInt64(txtQuntity.Text);
                    //long qty = Convert.ToInt64(lblQuntity.Text);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * Aqty;
                    pricqty = pricqty / 100;
                    total = total + pricqty;
                    LblTotAmt.Text = total.ToString();

                    if (LblGrantTotal.Text != "")
                    {
                        //if (qty > Aqty)
                        //{
                        if (ALblTotAmt.Text != "")
                        {
                            LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) - Convert.ToDecimal(ALblTotAmt.Text)).ToString();
                        }
                        LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(LblTotAmt.Text)).ToString();
                        //}
                        //else if (qty < Aqty)
                        //{
                        //    LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(ALblTotAmt.Text)).ToString();
                        //    LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) - Convert.ToDecimal(LblTotAmt.Text)).ToString();
                        //}
                        //LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(LblTotAmt.Text)).ToString();

                        //LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) - Convert.ToDecimal(LblTotAmt.Text)).ToString();
                        //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                    }
                    else
                    {
                        LblGrantTotal.Text = LblTotAmt.Text;
                        //txttot.Text = lblTot.Text;
                    }
                    lblQuntity.Text = txtQuntity.Text;
                    lblQuntity.Visible = true;
                    txtQuntity.Visible = false;
                }
                else
                {
                    txtQuntity.Text = "";
                }

                if (LblGrantTotal.Text != "")
                {
                    Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text);
                    //Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text) / 100;
                }
                else
                {
                    Global.TotIFees = 0;
                }
            }
        }

        protected void DDLItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            string type = "";

            if (RBtnUseLight.Checked == true)
            {
                type = "LightWeight";
            }
            else if (RBtnUseHeavy.Checked == true)
            {
                type = "HeavyWeight";
            }

            DropDownList DDLItemName = (DropDownList)sender;
            GridViewRow grdrRow = ((GridViewRow)DDLItemName.Parent.Parent);
            Label ItemFees = (Label)grdrRow.FindControl("ItemFees");
            Label ItemName = (Label)grdrRow.FindControl("ItemName");
            TextBox txtItemFees = (TextBox)grdrRow.FindControl("txtItemFees");

            ItemName.Text = DDLItemName.SelectedItem.Text;

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT M.ItemCode,M.ItemName,M.ItemFees FROM MULTIVALUESTOCK M,STOCKSITEM S WHERE M.ItemCode=S.ItemCode AND M.ICODE=@ICODE AND M.StockType=@STOCKTYPE";
            com.Parameters.Add("@ICODE", System.Data.SqlDbType.BigInt).Value = DDLItemName.SelectedValue;
            com.Parameters.Add("@STOCKTYPE", System.Data.SqlDbType.VarChar).Value = type;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                //ItemFees.Text = dt.Tables[0].Rows[0]["ItemFees"].ToString();
                ItemFees.Visible = false;
                txtItemFees.Visible = true;
            }
            con.Close();
        }

        protected void ddProjectList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            if (ddProjectList.SelectedItem.Text != "-Vælg-")
            {
                DropDownList ddlCurrentDropDownList = (DropDownList)sender;
                //com.CommandText = "select * from PROJECTS where ProjectId= " + ddlCurrentDropDownList.SelectedValue;
                com.CommandText = "Select distinct * from EMPLOYEE,ASSIGNED,PROJECTS where ASSIGNED.ProjectId=PROJECTS.ProjectId and EMPLOYEE.EmpId=ASSIGNED.EmpId and PROJECTS.ProjectId= " + ddlCurrentDropDownList.SelectedValue;
                SqlDataAdapter adpt = new SqlDataAdapter(com);
                DataSet dt = new DataSet();
                adpt.Fill(dt);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    gvPInfodetails.DataSource = dt;
                    gvPInfodetails.DataBind();

                    lblProjectId.Text = dt.Tables[0].Rows[0]["ProjectId"].ToString();
                    txtClient.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                    //txtStartTime.Text = dt.Tables[0].Rows[0]["ProjectDate"].ToString();
                    //txtCompletionDate.Text = dt.Tables[0].Rows[0]["CompletionDate"].ToString();

                    //BindTaskDetails();                 

                    gridUsers.DataSource = dt;
                    gridUsers.DataBind();
                }
            }
            con.Close();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string empID = Session["Empid"].ToString();
            string type = "";
            Decimal TotalHOURS = 0;
            Decimal ATotalHOURS = 0;
            Decimal ContributionFees = 0;
            Decimal AContributionFees = 0;
            Decimal TotalWage = 0;
            Decimal ATotalWage = 0;
            Decimal Hours = 0;
            Decimal TotalContributionFees = 0;
            Decimal TotalHoursWorked = 0;
            Decimal NewHours = 0;
            string Status = "Approve";
            string UserType = Session["UserType"].ToString();
            Decimal TotalFees = 0;
            Flag = true;

            if (LblGrantTotal.Text != "")
            {
                TotalFees = Convert.ToDecimal(LblGrantTotal.Text);
                //Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text);
            }
            else
            {
                TotalFees = 0;
            }
            if (ddProjectList.SelectedItem.Text == "-Vælg-")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Project Name');", true);
                return;
            }
            else
            {

                float total = 0, Atotal = 0;
                foreach (GridViewRow gridViewRow in gridUsers.Rows)
                {

                    if (gridViewRow.RowType == DataControlRowType.DataRow)
                    {

                        // System.Web.UI.WebControls.TextBox thours = (gridViewRow.Cells[4].FindControl("thours") as System.Web.UI.WebControls.TextBox);
                        DropDownList DDLLunch = (DropDownList)gridViewRow.FindControl("DDLLunch");
                        TextBox txtHours = (TextBox)gridViewRow.FindControl("txtHours");
                        TextBox txtAHours = (TextBox)gridViewRow.FindControl("txtAHours");

                        float rowValue = 0, rowValue1 = 0;
                        if (float.TryParse(txtHours.Text.Trim(), out rowValue))
                            total += rowValue;


                        if (float.TryParse(txtAHours.Text.Trim(), out rowValue1))
                            Atotal += rowValue1;

                        if (DDLLunch.SelectedItem.Text == "Yes")
                        {
                            Atotal = Atotal - 1;
                        }
                    }
                }
                lblTotalHR.Text = total.ToString();
                lblATotalHR.Text = Atotal.ToString();

                foreach (GridViewRow rows in gridUsers.Rows)
                {

                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.CheckBox chkUser = (rows.Cells[0].FindControl("chkUser") as System.Web.UI.WebControls.CheckBox);
                        System.Web.UI.WebControls.Label lblEmpid = (rows.Cells[1].FindControl("lblEmpid") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.TextBox txtHours = (rows.Cells[2].FindControl("txtHours") as System.Web.UI.WebControls.TextBox);
                        System.Web.UI.WebControls.Label lblWage = (rows.Cells[2].FindControl("lblWage") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.TextBox txtAHours = (rows.Cells[3].FindControl("txtAHours") as System.Web.UI.WebControls.TextBox);
                        System.Web.UI.WebControls.Label lblLunchTime = (rows.Cells[4].FindControl("lblLunchTime") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.DropDownList DDLLunch = (rows.Cells[4].FindControl("DDLLunch") as System.Web.UI.WebControls.DropDownList);
                        System.Web.UI.WebControls.Label lblContributionFees = (rows.Cells[5].FindControl("lblContributionFees") as System.Web.UI.WebControls.Label);
                        //System.Web.UI.WebControls.Label LType = (rows.Cells[1].FindControl("LType") as System.Web.UI.WebControls.Label);
                        //System.Web.UI.WebControls.Label lblHours1 = (rows.Cells[1].FindControl("lblHours") as System.Web.UI.WebControls.Label);
                        ////System.Web.UI.WebControls.Label lblTotalFees = (rows.Cells[1].FindControl("lblTotalFees") as System.Web.UI.WebControls.Label);
                        //System.Web.UI.WebControls.Label LblStatus = (rows.Cells[1].FindControl("LblStatus") as System.Web.UI.WebControls.Label);

                        if (chkUser.Checked == true)
                        {
                            if (DDLLunch.SelectedItem.Text == "Yes")
                            {
                                txtAHours.Text = ((Convert.ToDecimal(txtAHours.Text)) - (Convert.ToDecimal(lblLunchTime.Text))).ToString();
                            }
                            if (RBtnUseQuate.Checked == true)
                            {

                                ATotalWage = ((Convert.ToDecimal(txtAHours.Text)) * (Convert.ToDecimal(lblWage.Text)));
                                AContributionFees = ATotalWage;
                                NewHours = Convert.ToDecimal(txtAHours.Text);
                                TotalWage = ((Convert.ToDecimal(txtHours.Text)) * (Convert.ToDecimal(lblWage.Text)));
                                ContributionFees = TotalWage;
                                TotalContributionFees = ContributionFees + AContributionFees;
                                TotalHoursWorked = ((Convert.ToDecimal(txtAHours.Text)) + (Convert.ToDecimal(txtHours.Text)));


                                //ContributionFees = ContributionFees / 100;
                            }
                            else
                            {
                                ATotalHOURS = ((Convert.ToDecimal(txtAHours.Text)) / (Convert.ToDecimal(lblATotalHR.Text))) * 100;

                                AContributionFees = (ATotalHOURS * ATotalFees) / 100;

                                TotalHOURS = ((Convert.ToDecimal(txtAHours.Text)) / (Convert.ToDecimal(lblTotalHR.Text))) * 100;

                                ContributionFees = (TotalHOURS * TotalFees) / 100;
                                NewHours = Convert.ToDecimal(txtAHours.Text);
                                TotalContributionFees = ContributionFees + AContributionFees;
                                TotalHoursWorked = ((Convert.ToDecimal(txtAHours.Text)) + (Convert.ToDecimal(txtHours.Text)));
                                ATotalWage = TotalContributionFees;
                                //ContributionFees = ContributionFees / 100;

                            }

                            string dtEntry1 = txtStartTime.Text;
                            DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            //TotIFees = Convert.ToDecimal(LblItemFees.Text);
                            //Decimal TotalItemFees = Convert.ToDecimal(dt1.Tables[0].Rows[0]["TOTALITEMFEES"].ToString());
                            //Decimal Sal = TotalFees;
                            //TotalItemFees = TotalItemFees + Sal;
                        }

                    }
                }
                foreach (GridViewRow rows in GridSalary.Rows)
                {
                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.Label lblEmpid = (rows.Cells[1].FindControl("lblEmpid") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblHours = (rows.Cells[2].FindControl("lblHours") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblHours1 = (rows.Cells[2].FindControl("lblHours1") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblTotal = (rows.Cells[3].FindControl("lblTotal") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblTotal1 = (rows.Cells[3].FindControl("lblTotal1") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label LType = (rows.Cells[4].FindControl("LType") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label LblStatus = (rows.Cells[5].FindControl("LblStatus") as System.Web.UI.WebControls.Label);


                        if ((LblStatus.Text != Status) && (lblEmpid.Text == empID))
                        {
                            string dtEntry1 = txtStartTime.Text;
                            DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            if (RBtnUseQuate.Checked == true)
                            {
                                type = "Hours";
                            }
                            else if (RBtnUseLight.Checked == true)
                            {
                                type = "LightWeight";
                            }
                            else if (RBtnUseHeavy.Checked == true)
                            {
                                type = "HeavyWeight";
                            }

                            SqlConnection con = new SqlConnection(cs);
                            con.Open();
                            SqlCommand cm1 = new SqlCommand();
                            cm1.Connection = con;
                            cm1.CommandType = System.Data.CommandType.StoredProcedure;
                            cm1.CommandText = "sp_EditSalaryReportAdmin";
                            cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                            cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                            cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = LType.Text;
                            cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = LblStatus.Text;        //DDLStockItem.SelectedValue;
                            cm1.Parameters.Add("@STATUS1", System.Data.SqlDbType.VarChar).Value = Status;
                            cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = TotalHoursWorked;
                            cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = TotalContributionFees;        //DDLStockItem.SelectedValue;
                            //cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = lblTotalFees.Text;
                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = TotalHoursWorked;
                            cm1.Parameters.Add("@CONTRIBUTION", System.Data.SqlDbType.Decimal).Value = TotalContributionFees;
                            cm1.ExecuteNonQuery();
                            cm1.Parameters.Clear();

                            cm1 = new SqlCommand("SELECT SUM(Hours),SUM(ContributionFees),SUM(AHours),SUM(AContributionFees) from SALARYREPORT where EmpId=@I_EMPID  AND ProjectId=@I_PROJECTID and Type=@TYPE and Status=@STATUS1 ", con);
                            cm1.Parameters.AddWithValue("@I_EMPID", lblEmpid.Text);
                            cm1.Parameters.AddWithValue("@TYPE", LType.Text);
                            cm1.Parameters.AddWithValue("@I_PROJECTID", lblProjectId.Text);
                            cm1.Parameters.AddWithValue("@STATUS1", Status);
                            SqlDataAdapter adpt1 = new SqlDataAdapter(cm1);
                            DataSet dt1 = new DataSet();
                            adpt1.Fill(dt1);
                            double totalhours = 0;
                            double contributionfees = 0;
                            double totalAhours = 0;
                            double Acontributionfees = 0;
                            totalhours = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column1"].ToString());
                            contributionfees = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column2"].ToString());
                            totalAhours = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column3"].ToString());
                            Acontributionfees = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column4"].ToString());

                            cm1.ExecuteNonQuery();
                            cm1.Parameters.Clear();
                            cm1 = new SqlCommand("DELETE from SALARYREPORT where EmpId=@I_EMPID  AND ProjectId=@I_PROJECTID and Type=@TYPE and Status=@STATUS1 ", con);
                            cm1.Parameters.AddWithValue("@I_EMPID", lblEmpid.Text);
                            cm1.Parameters.AddWithValue("@TYPE", LType.Text);
                            cm1.Parameters.AddWithValue("@I_PROJECTID", lblProjectId.Text);
                            cm1.Parameters.AddWithValue("@STATUS1", Status);

                            cm1.ExecuteNonQuery();
                            cm1.Parameters.Clear();

                            cm1.CommandType = System.Data.CommandType.StoredProcedure;
                            cm1.CommandText = "sp_UpdateSalaryReportsNew";
                            cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                            cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                            cm1.Parameters.Add("@I_TASKID", System.Data.SqlDbType.BigInt).Value = 0;
                            cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = 0;
                            cm1.Parameters.Add("@TOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = TotalFees;
                            cm1.Parameters.Add("@I_STID", System.Data.SqlDbType.BigInt).Value = 0;
                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = totalhours;
                            cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;
                            cm1.Parameters.Add("@CONTRIBUTION", System.Data.SqlDbType.Decimal).Value = contributionfees;
                            cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                            cm1.Parameters.Add("@STATUS1", System.Data.SqlDbType.VarChar).Value = Status;
                            cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = totalAhours;
                            cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = Acontributionfees;
                            cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = TotalFees;

                            cm1.ExecuteNonQuery();
                            cm1.Parameters.Clear();
                            con.Close();
                        }

                        lblHours1.Text = (Convert.ToString(decimal.Round(NewHours, 2)));
                        lblTotal1.Text = (Convert.ToString(decimal.Round(ATotalWage, 2)));
                    }

                }
                btnView.Visible = true;
                btnSave.Visible = false;
            }

        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports.aspx");
        }

        protected void BtnStarttime_Click(object sender, EventArgs e)
        {

        }

        protected void BtnStopttime_Click(object sender, EventArgs e)
        {

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            lbldisplayTime.Text = DateTime.Now.ToString();
        }

        protected void btnQuantityCount_Click(object sender, EventArgs e)
        {
            Button btnQuantityCount = (Button)sender;

            GridViewRow grdrRow = ((GridViewRow)btnQuantityCount.NamingContainer);

            TextBox txtQuntity = (TextBox)grdrRow.FindControl("txtQuntity");
            Label ItemFees = (Label)grdrRow.FindControl("ItemFees");
            Label LblTotAmt = (Label)grdrRow.FindControl("LblTotAmt");
            Label lblQuntity = (Label)grdrRow.FindControl("lblQuntity");
            Label ALblTotAmt = (Label)grdrRow.FindControl("ALblTotAmt");
            //TextBox txtSubtotal = (TextBox)grdrRow.FindControl("txtSubtotal");
            if (txtQuntity.Text != "")
            {
                if (ItemFees.Text != "")
                {
                    decimal Price = Convert.ToDecimal(ItemFees.Text);
                    long Aqty = Convert.ToInt64(txtQuntity.Text);
                    //long qty = Convert.ToInt64(lblQuntity.Text);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * Aqty;
                    // pricqty = pricqty / 100;
                    total = total + pricqty;
                    LblTotAmt.Text = total.ToString();

                    if (LblGrantTotal.Text != "")
                    {
                        //if (qty > Aqty)
                        //{
                        if (ALblTotAmt.Text != "")
                        {
                            LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) - Convert.ToDecimal(ALblTotAmt.Text)).ToString();
                        }
                        LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(LblTotAmt.Text)).ToString();
                        //}
                        //else if (qty < Aqty)
                        //{
                        //    LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(ALblTotAmt.Text)).ToString();
                        //    LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) - Convert.ToDecimal(LblTotAmt.Text)).ToString();
                        //}
                        //LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(LblTotAmt.Text)).ToString();

                        //LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) - Convert.ToDecimal(LblTotAmt.Text)).ToString();
                        //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                    }
                    else
                    {
                        LblGrantTotal.Text = LblTotAmt.Text;
                        //txttot.Text = lblTot.Text;
                    }
                    lblQuntity.Text = txtQuntity.Text;
                    lblQuntity.Visible = true;
                    txtQuntity.Visible = false;
                }
                else
                {
                    txtQuntity.Text = "";
                }

                if (LblGrantTotal.Text != "")
                {
                    Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text);
                    //Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text) / 100;
                }
                else
                {
                    Global.TotIFees = 0;
                }
            }
        }
    }
}