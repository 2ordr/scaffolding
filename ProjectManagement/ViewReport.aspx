﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewReport.aspx.cs" Inherits="ProjectManagement.ViewReport" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <div style="align-content: center;">
                <br />
                <CR:CrystalReportViewer ID="CRPTViewer" runat="server" AutoDataBind="true"
                    PrintMode="ActiveX" HasDrilldownTabs="False" HasCrystalLogo="False" ToolPanelView="None"
                    HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False" />
            </div>
            <br />
            <a class="CloseLink" href="javascript:window.close();">Close this window</a>

        </div>
    </form>
</body>
</html>
