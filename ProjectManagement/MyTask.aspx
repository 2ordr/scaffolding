﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTask.aspx.cs" Inherits="ProjectManagement.MyTask" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />

    <link rel="stylesheet" href="fonts/untitle-font-4/styles.css" type="text/css" />

    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Upload_Script/jquery-1.4.4.min.js"></script>

    <script type="text/javascript">

        function OpenFileUpload() {

            var myFrame = document.getElementById('frameUpload');
            $(myFrame).focus();
            $(myFrame).contents().find("#FileUpload1").click();
            var value = $(myFrame).contents().find("#FileUpload1").val();
            if (value != '') {
                $(myFrame).contents().find("#btnSubmit").click();
            }

        }

    </script>
    <style>
        .headform1 {
            color: #383838;
        }

        .bg {
            text-align: right;
        }

        .myclass {
            height: 20px;
            position: relative;
            border: 2px solid #cdcdcd;
            border-color: silver;
            font-size: 14px;
        }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .horizontal {
            display: none;
        }

        .datagrid tr:hover {
            background-color: #f2e8da;
        }

        .spanid {
            color: lightblue;
        }

            .spanid :hover {
                color: black;
                cursor: pointer;
            }

        .hide {
            display: none;
        }

        .pagerLink {
            text-decoration: none;
        }

        .Calendar .ajax__calendar_container {
            border: 1px solid #E0E0E0;
            background-color: #FAFAFA;
            width: 200px;
        }

        .Calendar .ajax__calendar_header {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 12px;
            text-align: center;
            color: #9F9F9F;
            font-weight: normal;
            text-shadow: 0px 0px 2px #D3D3D3;
            height: 20px;
        }

        .Calendar .ajax__calendar_title,
        .Calendar .ajax__calendar_next,
        .Calendar .ajax__calendar_prev {
            color: #004080;
        }

        .Calendar .ajax__calendar_body {
            width: 175px;
            height: 150px;
            position: relative;
        }

        .Calendar .ajax__calendar_dayname {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            color: #FA9900;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            background-color: #EDEDED;
            border: solid 1px #D3D3D3;
            text-transform: uppercase;
            margin: 1px;
        }

        .Calendar .ajax__calendar_day {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            border: solid 1px #E0E0E0;
            text-transform: uppercase;
            margin: 1px;
            width: 17px !important;
            color: #9F9F9F;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_day,
        .Calendar .ajax__calendar_hover .ajax__calendar_month,
        .Calendar .ajax__calendar_hover .ajax__calendar_year,
        .Calendar .ajax__calendar_active {
            color: red;
            font-weight: bold;
            background-color: #ffffff;
        }

        .Calendar .ajax__calendar_year {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_month {
            border: solid 1px #E0E0E0;
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            vertical-align: middle;
            margin: 1px;
        }

        .Calendar .ajax__calendar_today {
            font-family: Tahoma, Calibri, sans-serif;
            font-size: 10px;
            text-align: center;
            font-weight: bold;
            text-shadow: 0px 0px 2px #D3D3D3;
            text-align: center !important;
            text-transform: uppercase;
            margin: 1px;
            color: #6B6B6B;
        }

        .Calendar .ajax__calendar_other {
            background-color: #E0E0E0;
            margin: 1px;
            width: 17px;
        }

        .Calendar .ajax__calendar_hover .ajax__calendar_today,
        .Calendar .ajax__calendar_hover .ajax__calendar_title {
        }

        .Calendar .ajax__calendar_footer {
            width: 175px;
            border: none;
            height: 20px;
            vertical-align: middle;
            color: #6B6B6B;
        }

        img.PopupImg {
            vertical-align: middle;
            padding: 0px;
            margin: 0px;
            border: none;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $("#addAnother").click(function () {
                addAnotherRow();
            });
        });

        function addAnotherRow() {
            var row = $("#tbl tr").last().clone();
            var oldId = Number(row.attr('id').slice(-1));
            var id = 1 + oldId;


            row.attr('id', 'tasktr_' + id);
            row.find('#Chkselectt_' + oldId).attr('id', 'Chkselectt_' + id);
            row.find('#txttask_' + oldId).attr('id', 'txttask_' + id);
            //row.find('#lbl_' + oldId).attr('id', 'lbl_' + id);
            //row.find('#txtDate_' + oldId).attr('id', 'txtDate_' + id);
            //row.find('#CalendarExtender1_' + oldId).attr('id', 'CalendarExtender1_' + id);
            //row.find('#ChkBillable_' + oldId).attr('id', 'ChkBillable_' + id);
            //row.find('#txtPricePerHr_' + oldId).attr('id', 'txtPricePerHr_' + id);
            //row.find('#txtBudget_' + oldId).attr('id', 'txtBudget_' + id);

            $('#tbl').append(row);

        }
    </script>
    <script type="text/javascript" language="javascript">
        function deleteRow(tableID, currentRow) {
            try {
                var table = document.getElementById(tableID);
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    /*var chkbox = row.cells[0].childNodes[0];*/
                    /*if (null != chkbox && true == chkbox.checked)*/

                    if (row == currentRow.parentNode.parentNode) {
                        if (rowCount <= 1) {
                            alert("Cannot delete all the rows.");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            //getValues();
        }
    </script>
    <script type="text/javascript" language="javascript">
        function delRow() {

            var current = window.event.srcElement;
            //here we will delete the line
            while ((current = current.parentElement) && current.tagName != "TR");
            current.parentElement.removeChild(current);

        }
    </script>
    <script type="text/javascript" language="javascript">
        function visible(Label4) {

            if (document.getElementById('Label4').style.visibility == "false") {
                document.getElementById('Label4').style.visibility == "true";
            }
            else {
                document.getElementById('Label4').style.visibility == "false";
            }
        }

    </script>

    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';
            else
                e.style.display = 'none';
            return false;
        }
    </script>

    <script type="text/javascript">
        function validTime(source, args) {
            var res = false;
            var orgVAlue = args.Value;
            var hour = orgVAlue.substring(0, 2);
            var min = orgVAlue.substring(3, 5);
            var sec = orgVAlue.substring(6, 8);
            if (orgVAlue.length == 0)
                res = false;
            else if (orgVAlue.length > 8)
                res = false;
            else {
                if ((hour > 23) || (min > 59) || (sec > 59))
                    res = false;
                else
                    res = true;
            }
            args.IsValid = res;
        }
    </script>

    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>

</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="Toolkitscriptmanager1" runat="server"></asp:ToolkitScriptManager>
        <div style="height: 1000px; width: 1920px;">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 1000px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 1000px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox1" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150"></asp:TextBox>
                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 1000px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVerticalaspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li> --%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%-- <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>
                                                --%>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div1')">Rapporter</span></span>

                                                    <div id="Div1" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div2')">Løn Modul</span></a>
                                                    <div id="Div2" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  x-item-selected active" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>

                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <%-- <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;">
                                    </div>
                                </div>
                                <div style="height: 300px; border-color: White;">
                                    <table style="height: 300px; width: 100%">
                                        <tr style="height: 100px;">
                                            <td></td>
                                        </tr>
                                        <tr style="height: 50px;">
                                            <td>
                                                <div>
                                                    &nbsp
                                                    <asp:Label ID="Label2" runat="server" Text="Timer" ForeColor="White"></asp:Label>
                                                </div>
                                                <div>

                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                                                        <ContentTemplate>
                                                            &nbsp

                                                            <asp:Label ID="lbltimedisplay" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="white">00:00:00</asp:Label>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                                        </Triggers>

                                                    </asp:UpdatePanel>

                                                    <asp:Button ID="BtnStart" runat="server" Text="Start" OnClick="BtnStart_Click" CssClass="button" Width="40" Height="30" BackColor="#009900" />
                                                    <asp:Button ID="BtnStop" runat="server" Text="stop" OnClick="BtnStop_Click" CssClass="button" Width="40" Height="30" BackColor="#009900" />

                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtHrs" runat="server" Height="30" Width="70"></asp:TextBox>
                                                    <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick1"></asp:Timer>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 100px;">
                                            <td>
                                                <ul>
                                                    <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                        <asp:Label ID="Label1" runat="server" Text="" ForeColor="White"></asp:Label>
                                                    </span></a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px; bottom: 999px;">

                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUpload" />
                    </Triggers>
                    <ContentTemplate>
                        <div class="allSides" style="width: 100%; height: 70px">
                            <div class="divider"></div>
                            <asp:Label ID="Label3" runat="server" CssClass="clienthead" Font-Bold="true" Font-Size="Large" Text="Opgave"></asp:Label>
                            <%--<asp:Label ID="lbltaskid" runat="server" Text="" Visible="false"></asp:Label>--%>

                            <asp:Label ID="Label5" runat="server" Text="Label" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label><br />
                            <asp:Label ID="Label6" runat="server" Text="Label" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label>
                        </div>
                        <div style="width: 1%; float: left; height: 800px; border: solid 0px black;">
                        </div>

                        <div runat="server" style="width: 99%; float: left; height: 800px; font-family: Calibri;">
                            <fieldset style="color: black;">
                                <legend></legend>
                                <table style="width: 100%">
                                    <tbody>
                                        <tr>
                                            <td style="width: 6%">
                                                <%-- <span runat="server" id="addAnother" class="add-another">+ Add NewTask</span>--%>
                                                <asp:Button CssClass="buttonn" ID="AddTask" runat="server" Text="+ Tilføj opgave" OnClick="AddTask_Click" />
                                            </td>
                                            <%--<asp:TextBox CssClass="twitterStyleTextbox" ID="txtExpectedHrs" runat="server" Height="30" Width="40" Text="00:00"></asp:TextBox>(Expected Hours) --%>

                                            <td style="width: 4.2%">Hours
                                        <asp:DropDownList ID="ddlHours" runat="server" CssClass="search_categories" Height="35">
                                        </asp:DropDownList>
                                            </td>
                                            <td style="width: 2.6%">
                                                <asp:DropDownList ID="ddlMinutes" runat="server" CssClass="search_categories" Height="35">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 3%">
                                                <asp:DropDownList ID="ddlSeconds" runat="server" CssClass="search_categories" Height="35">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 40%">

                                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                                <asp:Button CssClass="buttonn" ID="btnUpload" runat="server" Text="Vedhæft" OnClick="btnUpload_Click" />
                                            </td>

                                            <td style="width: 10%; text-align: right">
                                                <asp:Button CssClass="buttonn" ID="SaveTask" runat="server" Text="Gem" OnClick="SaveTask_Click" />
                                                <asp:Button CssClass="buttonn" ID="btnVIewTask" runat="server" Text="Vis" OnClick="btnVIewTask_Click" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="tblData" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 290px;">Opgavenr:
                                      <asp:Label ID="lbltaskid" runat="server"></asp:Label></th>
                                            <th style="width: 57px;"></th>
                                            <th>Opgave:
                                      <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMTask" Text="" runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </fieldset>

                            <div id="divOverflow" runat="server" style="width: 100%; height: 200px; font-family: Calibri; overflow: auto;">
                                <fieldset style="color: black;">
                                    <legend></legend>
                                    <table id="tbltask" runat="server" style="width: 100%;">
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gridTask" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px" OnRowDataBound="gridTask_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="SR.No" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltaskid" runat="server" Text='<%# Bind("TaskId") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblSubtask" runat="server" Text='<%# Bind("Subtask") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SubTask" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <span class="title">
                                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTask" Text='<%# Bind("TaskName") %>' runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>
                                                                    <%-- <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTask1" runat="server" Height="31" Visible="true" Width="300"></asp:TextBox>--%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                        </asp:TemplateField>
                                                        <%-- <asp:TemplateField HeaderText="Expected Hours" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtExpectedHrs" runat="server" Height="30" Width="40" Text="00:00"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                </asp:TemplateField>--%>

                                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:DropDownList CssClass="search_categories" ID="DDLSkillSet" runat="server" Height="35" Width="200"></asp:DropDownList>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="200px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <span class="actions">
                                                                    <span class="spanid" title="Task Info"><span class="action-icon icon-eye"></span></span>
                                                                    <span class="spanid" title=" Task Note"><span class="action-icon icon-clone"></span></span>
                                                                    <span class="spanid" title="Due Date"><span class="action-icon icon-calendar-1"></span></span>
                                                                    <%--   <span class="spanid" title="Delete Task"><a class="pagerLink" runat="server" onclick=" delRow()">
                                                                                    <span class="action-icon icon-trashcan"></span></a></span>--%>
                                                                    <asp:LinkButton ID="lnkDeletetask" CssClass="pagerLink" runat="server" OnClientClick="delRow()" OnClick="lnkDeletetask_Click">
                                                                                    <span class="action-icon icon-trashcan"></span></asp:LinkButton></span>
                                                                <span class="spanid" title="Add file"><span runat="server" class="action-icon icon-attach spanid"></span>
                                                                </span></span>
                                                                            </span>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                            <ItemTemplate>
                                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStatus" runat="server" Text="Open" Height="31" Enabled="false" Width="100"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                    <table id="tbl" runat="server" style="width: 100%;" tabindex="0" visible="false">
                                        <tbody>
                                            <tr id="tasktr_0" runat="server">
                                                <td style="width: 20px">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="TaskId" runat="server" Height="30" Width="30"></asp:TextBox>
                                                    <asp:CheckBox ID="Chkselectt_0" runat="server" Visible="false" />
                                                    <asp:Label ID="TextId" runat="server" Text="" Visible="false"></asp:Label>
                                                </td>
                                                <td style="width: 800px; height: 40px">
                                                    <span class="title">
                                                        <%--<div style="display: block;" class="editable description dataIndex-description" contenteditable="true" spellcheck="false" data-medium-editor-element="true" role="textbox" aria-multiline="true" data-medium-focused="true"></div>--%>
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txttask_0" runat="server" Height="31" Visible="true" Width="600"></asp:TextBox>

                                                        <span class="actions">
                                                            <span class="spanid" title="Task Info"><span class="action-icon icon-eye"></span></span>
                                                            <span class="spanid" title=" Task Note"><span class="action-icon icon-note"></span></span>
                                                            <span class="spanid" title="Due Date"><span class="action-icon icon-credit-card "></span></span>
                                                            <span class="spanid" title="Delete Task"><a class="pagerLink" runat="server" onclick=" delRow()"><span class="action-icon icon-trashcan"></span></a></span>
                                                            <span class="spanid" title="Add file"><%--<a runat="server" class="pagerLink" href="javascript:void(0)" onclick="OpenFileUpload();">--%><span runat="server" class="action-icon icon-attach spanid"></span><%--</a>--%>
                                                                <%-- <iframe class="hide" id="frameUpload" name="frameUpload" src="FileUpload.aspx" width="0" height="0" runat="server" />--%>
                                                            </span></span>
                                                    </span>
                                                </td>
                                                <td runat="server" style="height: 20px; width: 250px">
                                                    <div>
                                                        <asp:DropDownList CssClass="search_categories" ID="DDLSkillSet" runat="server" Height="35" Width="200"></asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td runat="server" style="height: 20px;">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTimeDate" runat="server" Height="31" Width="150"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalenderExtender1" CssClass="Calendar" Enabled="true" runat="server" TargetControlID="txtTimeDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                </td>
                                                <td runat="server" style="height: 20px; text-align: right">
                                                    <div>
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtStatus" runat="server" Text="Open" Height="31" Enabled="false" Width="100"></asp:TextBox>
                                                    </div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>

                            <fieldset id="Idfiledetails" runat="server" visible="false">
                                <legend>Oplysninger:</legend>
                                <div style="height: 50px; width: 100%">
                                    <table style="height: 50px; width: 100%">
                                        <tr>
                                            <td style="width: 80%; font-weight: 700">Filnavn</td>
                                            <%--   <td style="width: 190px; font-weight: 700">FilePath</td>--%>
                                            <td style="width: 20%; font-weight: 700">Vis</td>

                                        </tr>
                                    </table>
                                </div>
                                <div id="GridVOverflow" runat="server" style="width: 100%; height: 200px; font-family: Calibri; overflow: auto;">
                                    <asp:GridView ID="gvFileUpload" runat="server" ShowHeader="false" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px">
                                        <Columns>

                                            <asp:BoundField DataField="TaskId" HeaderText="TaskId" Visible="false" />
                                            <asp:BoundField DataField="FileName" HeaderText="FileName" ItemStyle-Width="80%" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                            <%--     <asp:BoundField DataField="FilePath" HeaderText="FilePath" ItemStyle-Width="100" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />--%>
                                            <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFilePath" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnViewImage" runat="server" Font-Underline="false" OnClick="lbtnViewImage_Click"><span class="action-icon icon-eye"></span></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="20%" BorderWidth="5" BorderColor="White" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </fieldset>


                            <fieldset id="IdTaskDetails" runat="server" visible="false" style="height: 350px">
                                <legend>Opgave detaljer:</legend>
                                <div id="divTaskView" runat="server" style="width: 100%; height: 300px; font-family: Calibri;">

                                    <div style="height: 50px; width: 100%">
                                        <table style="height: 50px; width: 100%;">
                                            <tr>
                                                <td style="width: 05%; font-weight: 700">SrNo.</td>
                                                <td style="width: 40%; font-weight: 700">Sub Task</td>
                                                <td style="width: 35%; font-weight: 700">SkillSet</td>
                                                <td style="width: 20%; font-weight: 700">ExpectedHours</td>
                                                <%--  <td style="width: 200px; font-weight: 700">FileName</td>--%>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="divTaskView1" runat="server" style="width: 100%; height: 250px; font-family: Calibri; overflow: auto;">
                                        <asp:GridView ID="gvTaskView" runat="server" ShowHeader="false" AutoGenerateColumns="false" Width="100%">
                                            <Columns>

                                                <asp:TemplateField HeaderText="SRNo." HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaskId" runat="server" Text='<%# Bind("TaskId") %>' Visible="False"></asp:Label>
                                                        <asp:Label ID="lblSTID" runat="server" Text='<%# Bind("STID") %>' Visible="False"></asp:Label>
                                                        <asp:Label ID="lblID" runat="server" Text='<%#Container.DataItemIndex + 1%>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="05%" />

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Task" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                    <ItemTemplate>
                                                        <%--   <asp:Label ID="lblTask" runat="server" Text='<%# Bind("Task") %>' Visible="true"></asp:Label>--%>
                                                        <asp:Label ID="lblTask" runat="server" Text='<%# Bind("SubTask") %>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="40%" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="SkillSet" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSkillSet" runat="server" Text='<%# Bind("SkillSet") %>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="35%" />

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ExpectedHours" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExpectedHours" runat="server" Text='<%# Bind("ExpectedHours") %>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20%" />

                                                </asp:TemplateField>

                                                <%--<asp:TemplateField HeaderText="FileName" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Bind("FileName") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="200" />
                                            <HeaderStyle Width="200" />
                                        </asp:TemplateField>--%>
                                            </Columns>

                                        </asp:GridView>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
