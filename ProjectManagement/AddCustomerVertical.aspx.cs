﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.UI.DataVisualization.Charting;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp;
using iTextSharp.text.html;
using iTextSharp.tool.xml;
using Spire.Doc;
using Spire.Doc.Documents;
//using Spire.Doc.Formatting;
//using Spire.Doc.Fields;
using Microsoft.Office.Interop.Word;
//using System.Reflection;
//using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Wordprocessing;

namespace ProjectManagement
{
    public partial class AddCustomerVertical : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        Boolean AktiveFlag = true;
        protected void Page_Load(object sender, EventArgs e)
        {

            //Page.Form.Attributes.Add("enctype", "multipart/form-data");

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    txtSearchBox.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    BindData();
                }
            }
        }

        public void BindData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from CUSTOMER ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                GridViewCustomer.DataSource = dt;
                GridViewCustomer.DataBind();
            }
            con.Close();
        }

        protected void BtnAddCustomer_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddCustomer.aspx");
        }

        protected void GridViewCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection(cs);
            con.Open();

            String sql = "Select * from CUSTOMER where CustName Like @CUSTNAME";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {

                cmd.Parameters.Add("@CUSTNAME", System.Data.SqlDbType.VarChar, 100).Value = "%" + txtSearchBox.Text + "%";

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                DataSet dt1 = new DataSet();

                adpt.Fill(dt1);

                if (dt1.Tables[0].Rows.Count > 0)
                {

                    GridViewCustomer.DataSource = dt1;

                    GridViewCustomer.DataBind();
                }
            }
            con.Close();
        }

        protected void LBtnBasicInfo_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }
            else
            {
                LBtnBasicInfo.BackColor = System.Drawing.Color.SkyBlue;
                LBtnBasicInfo.BorderColor = System.Drawing.Color.Black;
                LBtnSendMail.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnAPV.BackColor = LBtnProjects.BackColor = LBtnInvoice.BackColor = LBtnDocuments.BackColor = LBtnOverview.BackColor = LBtnActiveAPV.BackColor = System.Drawing.Color.Empty;
                LBtnSendMail.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnQuotes.BorderColor = LBtnAPV.BorderColor = LBtnProjects.BorderColor = LBtnInvoice.BorderColor = LBtnDocuments.BorderColor = LBtnOverview.BorderColor = LBtnActiveAPV.BorderColor = System.Drawing.Color.Empty;


                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.Red;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divBasicInfo.Visible = true;
                divOverView.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divQuate.Visible = false;
                divProject.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;

                SqlConnection con = new SqlConnection(cs);
                con.Open();

                SqlCommand com = new SqlCommand("select * from CUSTOMER where CustId =" + LblCustId.Text);

                com.Connection = con;

                SqlDataAdapter adpt = new SqlDataAdapter(com);

                DataSet dt = new DataSet();

                adpt.Fill(dt);

                if (dt.Tables[0].Rows.Count > 0)
                {

                    GridViewBasicInfo.DataSource = dt;

                    GridViewBasicInfo.DataBind();
                    //LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                    txtName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                    txtEmail.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                    txtAddress.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                    txtcity.Text = dt.Tables[0].Rows[0]["City"].ToString();
                    txtcountry.Text = dt.Tables[0].Rows[0]["Country"].ToString();
                    txtzip.Text = dt.Tables[0].Rows[0]["ZipCode"].ToString();
                    txtMobile.Text = dt.Tables[0].Rows[0]["Mobile"].ToString();
                    txtphone.Text = dt.Tables[0].Rows[0]["Phone2"].ToString();
                    txtFax.Text = dt.Tables[0].Rows[0]["Fax"].ToString();
                    txtProvince.Text = dt.Tables[0].Rows[0]["Province"].ToString();
                    txtVatNo.Text = dt.Tables[0].Rows[0]["VATNumber"].ToString();
                    txtWebsite.Text = dt.Tables[0].Rows[0]["Website"].ToString();
                }
                con.Close();
            }
        }

        protected void DdlistVat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (txtName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con1 = new SqlConnection(cs);
                con1.Open();
                com.Connection = con1;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_UpdateCustomerDetails";
                com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                com.Parameters.Add("@CUSTNAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;
                com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
                com.Parameters.Add("@PHONE2", System.Data.SqlDbType.VarChar).Value = txtphone.Text;
                com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
                com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress.Text;
                com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtcity.Text;
                com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince.Text;
                com.Parameters.Add("@REMARK", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.VarChar).Value = txtzip.Text;
                com.Parameters.Add("@VATNUMBER", System.Data.SqlDbType.VarChar).Value = txtVatNo.Text;
                com.Parameters.Add("@FAX", System.Data.SqlDbType.VarChar).Value = txtFax.Text;
                com.Parameters.Add("@COUNTRY", System.Data.SqlDbType.VarChar).Value = txtcountry.Text;
                com.Parameters.Add("@WEBSITE", System.Data.SqlDbType.NVarChar).Value = txtWebsite.Text;
                com.Parameters.Add("@DISCOUNT", System.Data.SqlDbType.Decimal).Value = txtDiscount.Text;

                com.ExecuteNonQuery();

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Update SuccessFully');", true);
                com.Parameters.Clear();
                BindData();
                con1.Close();

                foreach (GridViewRow rows in GridViewCustomer.Rows)
                {
                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.LinkButton LBtnCustomerName = (rows.Cells[1].FindControl("LBtnCustomerName") as System.Web.UI.WebControls.LinkButton);

                        if (LblCustName.Text == LBtnCustomerName.Text)
                        {
                            LBtnCustomerName.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            LBtnCustomerName.ForeColor = System.Drawing.Color.RoyalBlue;

                        }
                    }
                }
            }
        }

        protected void chkSelect_CheckedChanged1(object sender, EventArgs e)
        {

            ArrayList selectedValues = new ArrayList();
            foreach (GridViewRow item in GridViewCustomer.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)(item.Cells[0].FindControl("chkSelect"));
                    if (chk.Checked)
                    {
                        Label lbCustId = ((Label)item.Cells[0].FindControl("CustId"));
                        Label lbCustName = ((Label)item.Cells[1].FindControl("CustName"));

                        LblCustId.Text = lbCustId.Text;
                        LblCustName.Text = lbCustName.Text;
                        //Response.Redirect("customer overview.aspx?CustId=" + CustId.Text);

                    }

                }

            }
        }

        protected void LBtnOverview_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnOverview.BackColor = System.Drawing.Color.SkyBlue;
                LBtnOverview.BorderColor = System.Drawing.Color.Black;
                LBtnSendMail.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnAPV.BackColor = LBtnProjects.BackColor = LBtnInvoice.BackColor = LBtnDocuments.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = System.Drawing.Color.Empty;
                LBtnSendMail.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnQuotes.BorderColor = LBtnAPV.BorderColor = LBtnProjects.BorderColor = LBtnInvoice.BorderColor = LBtnDocuments.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.Red;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divOverView.Visible = true;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divQuate.Visible = false;
                divProject.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;

                Chart();
            }
        }

        protected void LBtnContacts_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnContacts.BackColor = System.Drawing.Color.SkyBlue;
                LBtnContacts.BorderColor = System.Drawing.Color.Black;
                LBtnSendMail.BackColor = LBtnOverview.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnAPV.BackColor = LBtnProjects.BackColor = LBtnInvoice.BackColor = LBtnDocuments.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = System.Drawing.Color.Empty;
                LBtnSendMail.BorderColor = LBtnOverview.BorderColor = LBtnOrders.BorderColor = LBtnQuotes.BorderColor = LBtnAPV.BorderColor = LBtnProjects.BorderColor = LBtnInvoice.BorderColor = LBtnDocuments.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.Red;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divContactInfo.Visible = true;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divOrder.Visible = false;
                divQuate.Visible = false;
                divProject.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;

                BindDataForContacts();
                BtnRefresh.Visible = false;

            }
        }

        public void BindDataForContacts()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_ContactByName";
            com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.NVarChar).Value = LblCustId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                ContactHeader.Visible = true;
                GViewContact.DataSource = dt;
                GViewContact.DataBind();
            }
            else
            {
                GViewContact.DataSource = dt;
                GViewContact.DataBind();
                ContactHeader.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Insen Kontakt oprettet');", true);
                //return;
            }
            con.Close();
        }

        protected void OpenWindow(object sender, EventArgs e)
        {
            Global.flag = 0;
            string url = ("Popup.aspx?CustId=" + LblCustId.Text);

            string s = "window.open('" + url + "', 'popup_window', 'width=1000, height=800, left=200, top=200, resizable=no');";

            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            BtnRefresh.Visible = true;
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            Global.flag = 1;

            ArrayList selectedValues = new ArrayList();
            foreach (GridViewRow item in GViewContact.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)(item.Cells[0].FindControl("chkSelect"));
                    if (chk.Checked)
                    {
                        Label ContId = ((Label)item.Cells[0].FindControl("CONTID"));
                        BtnRefresh.Visible = true;
                        string url = ("Popup.aspx?ContId=" + ContId.Text);

                        string s = "window.open('" + url + "', 'popup_window', 'width=1000, height=800, left=200, top=200, resizable=no');";

                        ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                        //Response.Redirect("Popup.aspx?ContId=" + LblContId.Text);

                    }

                }
                //Session["SELECTEDVALUES"] = selectedValues;
                //Response.Redirect("customer overview.aspx");

            }
        }

        protected void LBtnOrders_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnOrders.BackColor = System.Drawing.Color.SkyBlue;
                LBtnOrders.BorderColor = System.Drawing.Color.Black;
                LBtnSendMail.BackColor = LBtnOverview.BackColor = LBtnContacts.BackColor = LBtnQuotes.BackColor = LBtnAPV.BackColor = LBtnProjects.BackColor = LBtnInvoice.BackColor = LBtnDocuments.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = System.Drawing.Color.Empty;
                LBtnSendMail.BorderColor = LBtnOverview.BorderColor = LBtnContacts.BorderColor = LBtnQuotes.BorderColor = LBtnAPV.BorderColor = LBtnProjects.BorderColor = LBtnInvoice.BorderColor = LBtnDocuments.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.Red;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divOrder.Visible = true;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divQuate.Visible = false;
                divProject.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;

                BindDataForOrder();

            }
        }

        public void BindDataForOrder()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_OrderByName";
            com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.NVarChar).Value = LblCustId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                OrderHeader.Visible = true;
                GridViewOrder.DataSource = dt;
                GridViewOrder.DataBind();
            }
            else
            {
                GridViewOrder.DataSource = dt;
                GridViewOrder.DataBind();
                OrderHeader.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Insen ordre oprettet');", true);
                //return;
            }

            con.Close();
        }

        protected void BtnOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("View Order.aspx");
        }

        protected void GridViewOrder_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void LikButEdit_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[2].Text;
            Response.Redirect("EditOrder.aspx?Quatdate=" + quatdate);
        }

        protected void LBtnQuotes_Click(object sender, EventArgs e)
        {

            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }
            else
            {
                LBtnQuotes.BackColor = System.Drawing.Color.SkyBlue;
                LBtnQuotes.BorderColor = System.Drawing.Color.Black;
                LBtnSendMail.BackColor = LBtnOverview.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnProjects.BackColor = LBtnInvoice.BackColor = LBtnAPV.BackColor = LBtnDocuments.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = System.Drawing.Color.Empty;
                LBtnSendMail.BorderColor = LBtnOverview.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnProjects.BorderColor = LBtnInvoice.BorderColor = LBtnAPV.BorderColor = LBtnDocuments.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.Red;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divQuate.Visible = true;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divProject.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;

                BindDataForQuote();

            }
        }

        public void BindDataForQuote()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getQuatationByName";
            com.Parameters.Add("@CUSTID", System.Data.SqlDbType.NVarChar).Value = LblCustId.Text;
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();

            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                QuoteHeader.Visible = true;
                CusQuoteGrideView.DataSource = dt;
                CusQuoteGrideView.DataBind();
            }
            else
            {
                CusQuoteGrideView.DataSource = dt;
                CusQuoteGrideView.DataBind();
                QuoteHeader.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Insen Tilbud oprettet');", true);
                //return;
            }
            con.Close();
        }

        public void CountQUote()
        {
            SqlConnection con = new SqlConnection(cs);

            con.Open();

            SqlCommand com = new SqlCommand("sp_getQuatationByName");

            com.Connection = con;

            com.CommandType = System.Data.CommandType.StoredProcedure;

            com.Parameters.Add("@CUSTID", System.Data.SqlDbType.NVarChar).Value = LblCustId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                QuoteDivNothing.Visible = false;

                QuoteCount.Text = (dt.Tables[0].Rows.Count).ToString();
            }
            else
            {
                QuoteDivNothing.Visible = true;

                QuoteCount.Text = "";
            }
            con.Close();
        }

        protected void BtnQuotes_Click(object sender, EventArgs e)
        {
            Response.Redirect("Wizardss.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[1].Text;
            //Response.Redirect("QuatationPrint.aspx?Quatdate=" + quatdate);

            string url = ("QuatationPrint.aspx?Quatdate=" + quatdate);

            string s = "window.open('" + url + "');";

            //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
            return;
        }

        protected void LikButEdit1_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate1 = grdrow.Cells[1].Text;
            Session["quatdate"] = quatdate1;
            Response.Redirect("Wizardss.aspx");
        }

        protected void Btnproject_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProjectAssignment.aspx");
        }

        protected void LBtnProjects_Click(object sender, EventArgs e)
        {

            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnProjects.BackColor = System.Drawing.Color.SkyBlue;
                LBtnProjects.BorderColor = System.Drawing.Color.Black;
                LBtnSendMail.BackColor = LBtnOverview.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnInvoice.BackColor = LBtnDocuments.BackColor = LBtnAPV.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = System.Drawing.Color.Empty;
                LBtnSendMail.BorderColor = LBtnOverview.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnQuotes.BorderColor = LBtnInvoice.BorderColor = LBtnDocuments.BorderColor = LBtnAPV.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.Red;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divProject.Visible = true;
                divQuate.Visible = false;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;

                BindDataforProject();
            }
        }

        public void BindDataforProject()
        {

            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com1 = new SqlCommand("select * from PROJECTS where CustId=" + LblCustId.Text);
            com1.Connection = con;
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            System.Data.DataTable dt = new System.Data.DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                ProHeader.Visible = true;
                GridViewProjectDetails.DataSource = dt;
                GridViewProjectDetails.DataBind();
            }
            else
            {
                GridViewProjectDetails.DataSource = dt;
                GridViewProjectDetails.DataBind();
                ProHeader.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('" + LblCustName.Text + " had Not Created any Project');", true);
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Insen Projekter oprettet');", true);
                //return;
            }
            con.Close();
        }

        public void CountProjects()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com1 = new SqlCommand("select * from PROJECTS where CustId=" + LblCustId.Text);
            com1.Connection = con;
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            System.Data.DataTable dt = new System.Data.DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                ProjectDivNothing.Visible = false;

                ProjectCount.Text = (dt.Rows.Count).ToString();
            }
            else
            {
                ProjectDivNothing.Visible = true;
                ProjectCount.Text = "";
            }
            con.Close();
        }

        protected void LBtnSendMail_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnSendMail.BackColor = System.Drawing.Color.SkyBlue;
                LBtnSendMail.BorderColor = System.Drawing.Color.Black;
                LBtnProjects.BackColor = LBtnOverview.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnInvoice.BackColor = LBtnDocuments.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnProjects.BorderColor = LBtnOverview.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnQuotes.BorderColor = LBtnInvoice.BorderColor = LBtnDocuments.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.Red;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divSendMail.Visible = true;
                divProject.Visible = false;
                divQuate.Visible = false;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;
                txt_To.Text = txtEmail.Text;

            }
        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            Clearall();
            //Response.Redirect("AddCustomerVertical.aspx");
        }

        protected void Clearall()
        {
            txt_Message.Text = string.Empty;
            txt_Subject.Text = string.Empty;
            txt_To.Text = string.Empty;

        }

        protected void btn_SendMai_Click(object sender, EventArgs e)
        {
            string to = txt_To.Text;
            string from = "manojgawas49@gmail.com"; //info@certigoa.com
            string subject = txt_Subject.Text;
            string body = txt_Message.Text;

            try
            {
                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(from, to))
                {

                    mm.Subject = subject;
                    mm.Body = body;
                    if (FileUpload1.HasFile)
                    {
                        string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                        mm.Attachments.Add(new Attachment(FileUpload1.PostedFile.InputStream, FileName));
                    }

                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();


                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("manojgawas49@gmail.com", "papa2119");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Mail Send Successfully')", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + ex.Message + "')", true);

            }
            txt_Subject.Text = "";
            txt_Message.Text = "";
        }

        protected void Lnkdeleterow_Click(object sender, EventArgs e)
        {

            //GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //string CustId = grdrow.Cells[0].Text;

            //    SqlConnection con1 = new SqlConnection(cs);
            //    con1.Open();

            //    SqlCommand com1 = new SqlCommand("delete from CUSTOMER where CustId = @I_CUSTID ", con1);                    
            //    com1.Parameters.Add("@I_CUSTID", SqlDbType.BigInt).Value = LblCustId.Text;            

            //    com1.ExecuteNonQuery();
            //    con1.Close();
            //    GridViewCustomer.DataBind();
            //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Quatation Deleted Successfully!!');", true);
            //    return;


        }

        protected void GridViewCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("delete from CUSTOMER where CustId=" + id, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                BindData();
                resetall();
            }
        }

        public void resetall()
        {
            txtName.Text = "";
            txtMobile.Text = "";
            txtphone.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtcity.Text = "";
            txtProvince.Text = "";
            txtzip.Text = "";
            txtVatNo.Text = "";
            txtFax.Text = "";
            txtcountry.Text = "";
            //DdlistVat.SelectedValue = "";
            txtName1.Text = "";
            txtMobile1.Text = "";
            txtphone1.Text = "";
            txtEmail1.Text = "";
            txtAddress1.Text = "";
            txtcity1.Text = "";
            txtProvince1.Text = "";
            txtZipCode.Text = "";
            txtCountry1.Text = "";
            txtRemark.Text = "";
        }

        protected void BtnRefresh_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            //SqlCommand com = new SqlCommand("select * from CONTACTS ", con);

            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_ContactByName";
            com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.NVarChar).Value = LblCustId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                GViewContact.DataSource = dt;

                GViewContact.DataBind();
                BtnRefresh.Visible = false;
                ContactHeader.Visible = true;
            }
        }

        protected void lnkDeleteProject_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string ProId = grdrow.Cells[0].Text;

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();

            SqlCommand com1 = new SqlCommand("delete FROM PROJECTS where ProjectId = @PROJECTID ; delete FROM ASSIGNED  where ProjectId = @PROJECTID;", con1);
            com1.Parameters.Add("@PROJECTID", SqlDbType.BigInt).Value = ProId;

            com1.ExecuteNonQuery();
            con1.Close();
            BindDataforProject();
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Project Deleted Successfully!!');", true);
            return;
        }

        public void CallJavaScript(object sender)
        {
            LinkButton lnk = (LinkButton)sender;
            //Response.Write("link button:" + lnk.Text + " is clicked");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Ming1", "SetColor('" + lnk.ClientID + "')", true);
        }

        protected void LBtnCustomerName_Click(object sender, EventArgs e)
        {
            LBtnOverview.BackColor = System.Drawing.Color.SkyBlue;
            LBtnOverview.BorderColor = System.Drawing.Color.Black;
            LBtnSendMail.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnProjects.BackColor = LBtnInvoice.BackColor = LBtnDocuments.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
            LBtnSendMail.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnQuotes.BorderColor = LBtnProjects.BorderColor = LBtnInvoice.BorderColor = LBtnDocuments.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

            LBtnOverview.ForeColor = System.Drawing.Color.Red;
            LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

            divBasicInfo.Visible = false;
            divOverView.Visible = true;
            divContactInfo.Visible = false;
            divOrder.Visible = false;
            divQuate.Visible = false;
            divProject.Visible = false;
            divSendMail.Visible = false;
            divDocuments.Visible = false;
            DivActiveAPV.Visible = false;
            divAPV.Visible = false;

            //CallJavaScript(sender);
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            Label quatdate = ((Label)grdrow.Cells[0].FindControl("CustId"));
            Label lbEmpName = ((Label)grdrow.Cells[0].FindControl("CustName"));

            LblCustId.Text = quatdate.Text;
            LblCustName.Text = lbEmpName.Text;

            foreach (GridViewRow rows in GridViewCustomer.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.LinkButton LBtnCustomerName = (rows.Cells[1].FindControl("LBtnCustomerName") as System.Web.UI.WebControls.LinkButton);

                    if (lbEmpName.Text == LBtnCustomerName.Text)
                    {
                        LBtnCustomerName.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        LBtnCustomerName.ForeColor = System.Drawing.Color.RoyalBlue;

                    }
                }
            }

            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                CountQUote();

                CountProjects();

                Chart();

                SqlConnection con = new SqlConnection(cs);

                con.Open();

                SqlCommand com = new SqlCommand("select * from CUSTOMER where CustId =" + LblCustId.Text);

                com.Connection = con;

                SqlDataAdapter adpt = new SqlDataAdapter(com);

                DataSet dt = new DataSet();

                adpt.Fill(dt);

                if (dt.Tables[0].Rows.Count > 0)
                {

                    GridViewBasicInfo.DataSource = dt;

                    GridViewBasicInfo.DataBind();
                    //LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                    txtName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                    txtName.Enabled = false;
                    txtEmail.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                    txtAddress.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                    txtcity.Text = dt.Tables[0].Rows[0]["City"].ToString();
                    txtcountry.Text = dt.Tables[0].Rows[0]["Country"].ToString();
                    txtzip.Text = dt.Tables[0].Rows[0]["ZipCode"].ToString();
                    txtMobile.Text = dt.Tables[0].Rows[0]["Mobile"].ToString();
                    txtphone.Text = dt.Tables[0].Rows[0]["Phone2"].ToString();
                    txtFax.Text = dt.Tables[0].Rows[0]["Fax"].ToString();
                    txtProvince.Text = dt.Tables[0].Rows[0]["Province"].ToString();
                    txtVatNo.Text = dt.Tables[0].Rows[0]["VATNumber"].ToString();
                    txtWebsite.Text = dt.Tables[0].Rows[0]["Website"].ToString();
                    txtDiscount.Text = dt.Tables[0].Rows[0]["Discount"].ToString();

                    LBVIEWCUSTNAME.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                    LBVIEWEMAIL.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                    LBVIEWCITY.Text = dt.Tables[0].Rows[0]["City"].ToString();
                    LBVIEWZIPCODE.Text = dt.Tables[0].Rows[0]["ZipCode"].ToString();
                    LBVIEWCOUNTRY.Text = dt.Tables[0].Rows[0]["Country"].ToString();

                }
                con.Close();
            }
        }

        protected void LBtnName_Click1(object sender, EventArgs e)
        {
            LblNoteHeading.Text = "Update Contact";
            BtnUpdate1.Visible = true;
            BtnSaveContact.Visible = false;

            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            Label quatdate = ((Label)grdrow.Cells[0].FindControl("CONTID"));

            LblContId.Text = quatdate.Text;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from CONTACTS where ContId =@I_CONTID ", con);

            String var = quatdate.Text;
            cmd.Parameters.Add("@I_CONTID", System.Data.SqlDbType.VarChar).Value = var;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);

            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                LblContId.Text = dt.Tables[0].Rows[0]["CONTID"].ToString();
                txtName1.Text = dt.Tables[0].Rows[0]["Name"].ToString();
                LblCustId.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                txtEmail1.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                txtAddress1.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                txtcity1.Text = dt.Tables[0].Rows[0]["City"].ToString();
                txtCountry1.Text = dt.Tables[0].Rows[0]["Country"].ToString();
                txtZipCode.Text = dt.Tables[0].Rows[0]["ZipCode"].ToString();
                txtMobile1.Text = dt.Tables[0].Rows[0]["Mobile"].ToString();
                txtphone1.Text = dt.Tables[0].Rows[0]["Phone"].ToString();
                txtProvince1.Text = dt.Tables[0].Rows[0]["Province"].ToString();
                txtRemark.Text = dt.Tables[0].Rows[0]["Remark"].ToString();


            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);

            if (LblContId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            //else
            //{

            //    Global.flag = 1;
            //    BtnRefresh.Visible = true;
            //    string url = ("Popup.aspx?ContId=" + LblContId.Text);

            //    string s = "window.open('" + url + "', 'popup_window', 'width=1000, height=800, left=200, top=200, resizable=no');";

            //    ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            //}
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string filename = Path.GetFileName(FileUpload2.PostedFile.FileName);

            if (filename == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select File To Upload');", true);
                return;
            }
            else
            {

                string filePath = @"~\CustomerData\" + filename.Trim();
                string newfile = Server.MapPath(filePath);
                FileUpload2.SaveAs(newfile);
                //Stream str = FileUpload1.PostedFile.InputStream;

                //BinaryReader br = new BinaryReader(str);

                //Byte[] size = br.ReadBytes((int)str.Length);

                using (SqlConnection con = new SqlConnection(cs))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "insert into CUSTOMERFILEDATA (CustId,FileName,FilePath) values(@id,@Name,@FilePath)";

                        cmd.Parameters.AddWithValue("@id", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;

                        cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarChar).Value = filename;

                        cmd.Parameters.AddWithValue("@FilePath", System.Data.SqlDbType.VarChar).Value = newfile;

                        cmd.Connection = con;

                        con.Open();

                        cmd.ExecuteNonQuery();

                        con.Close();
                    }

                }
                gvFileUpload1();
            }
        }

        private void gvFileUpload1()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from CUSTOMERFILEDATA where CustId=" + LblCustId.Text;

                    cmd.Connection = con;

                    con.Open();

                    gvFileUpload.DataSource = cmd.ExecuteReader();

                    gvFileUpload.DataBind();

                    con.Close();
                }
            }
        }

        protected void LBtnDocuments_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnDocuments.BackColor = System.Drawing.Color.SkyBlue;
                LBtnDocuments.BorderColor = System.Drawing.Color.Black;
                LBtnProjects.BackColor = LBtnOverview.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnInvoice.BackColor = LBtnSendMail.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnProjects.BorderColor = LBtnOverview.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnInvoice.BorderColor = LBtnSendMail.BorderColor = LBtnQuotes.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.Red;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divQuate.Visible = false;
                divProject.Visible = false;
                BtnRefresh.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = true;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;

                gvFileUpload1();
            }
        }

        protected void lbtnViewImage_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //string FilePath = grdrow.Cells[2].Text;
            Label lblFilePath = (Label)grdrow.Cells[2].FindControl("lblFilePath");
            string FilePath = lblFilePath.Text;

            if (FilePath.Contains(".doc"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;


            }
            //view pdf files 
            if (FilePath.Contains(".pdf"))
            {


                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }


            if (FilePath.Contains(".htm"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;

            }

            if (FilePath.Contains(".txt"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;


            }

            if (FilePath.Contains(".doc") || FilePath.Contains(".rtf") || FilePath.Contains(".html") || FilePath.Contains(".log") || FilePath.Contains(".jpeg") || FilePath.Contains(".tif") || FilePath.Contains(".tiff") || FilePath.Contains(".gif") || FilePath.Contains(".bmp") || FilePath.Contains(".asf"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            //view pdf files 
            if (FilePath.Contains(".pdf") || FilePath.Contains(".avi") || FilePath.Contains(".zip") || FilePath.Contains(".xls") || FilePath.Contains(".csv") || FilePath.Contains(".wav") || FilePath.Contains(".mp3"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }


            if (FilePath.Contains(".htm") || FilePath.Contains(".mpg") || FilePath.Contains(".mpeg") || FilePath.Contains(".fdf") || FilePath.Contains(".ppt") || FilePath.Contains(".dwg") || FilePath.Contains(".msg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;

            }

            if (FilePath.Contains(".txt") || FilePath.Contains(".xml") || FilePath.Contains(".sdxl") || FilePath.Contains(".xdp") || FilePath.Contains(".jar"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".jpg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".png"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }
        }

        protected void lnkUpdateProject_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string ProId = grdrow.Cells[0].Text;

            Response.Redirect("ProjectAssignmentNew.aspx?ProjectID=" + ProId);
        }

        protected void LBtnInvoice_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnInvoice.BackColor = System.Drawing.Color.SkyBlue;
                LBtnInvoice.BorderColor = System.Drawing.Color.Black;
                LBtnProjects.BackColor = LBtnOverview.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnDocuments.BackColor = LBtnSendMail.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnProjects.BorderColor = LBtnOverview.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnDocuments.BorderColor = LBtnSendMail.BorderColor = LBtnQuotes.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.Red;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divQuate.Visible = false;
                divProject.Visible = false;
                BtnRefresh.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = true;
                DivActiveAPV.Visible = false;
                divAPV.Visible = false;
                BindInvoiceData();
            }
        }

        public void BindInvoiceData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com1 = new SqlCommand("select distinct * from INVOICEDATA where CustomerId=" + LblCustId.Text);
            com1.Connection = con;
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            System.Data.DataTable dt = new System.Data.DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                InvoiceHeader.Visible = true;
                CustInvoiceGridView.DataSource = dt;
                CustInvoiceGridView.DataBind();
            }
            else
            {
                CustInvoiceGridView.DataSource = dt;
                CustInvoiceGridView.DataBind();
                InvoiceHeader.Visible = false;
                // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('" + LblCustName.Text + " had Not Generated any Invoice');", true);
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Insen faktura oprettet');", true);
                //return;
            }
            con.Close();
        }

        protected void BtnInvoice_Click(object sender, EventArgs e)
        {
            Response.Redirect("New Invoice1.aspx?CustId=" + LblCustId.Text);
        }

        protected void LnkBtnEditInvoice_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string InvoiceNo = grdrow.Cells[1].Text;

            Response.Redirect("New Invoice1.aspx?InvoiceNO=" + InvoiceNo);
        }

        protected void LinkBtnView_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[1].Text;

            //Response.Redirect("EditQuatation.aspx?Quatdate=" + quatdate);

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select FilePath from PDFDATA where QuoteDate ='" + quatdate.ToString() + "'";
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                string FilePath = dt.Tables[0].Rows[0]["FilePath"].ToString();
                con.Close();

                try
                {
                    //view document file 
                    if (FilePath.Contains(".doc"))
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();

                        Session["FILEPATH"] = FilePath;
                        string url = "ViewFile.aspx";
                        string s = "window.open('" + url + "');";

                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                        return;
                    }
                    //view pdf files 
                    if (FilePath.Contains(".pdf"))
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();

                        Session["FILEPATH"] = FilePath;
                        string url = "ViewFile.aspx";
                        string s = "window.open('" + url + "');";

                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                        return;
                    }
                    if (FilePath.Contains(".htm"))
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();

                        Session["FILEPATH"] = FilePath;
                        string url = "ViewFile.aspx";
                        string s = "window.open('" + url + "');";

                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                        return;
                    }
                    if (FilePath.Contains(".txt"))
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();

                        Session["FILEPATH"] = FilePath;
                        string url = "ViewFile.aspx";
                        string s = "window.open('" + url + "');";

                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('File not in Proper format');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Pdf Not Created');", true);
                return;
            }
        }

        protected void Chart()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            //Line Chart
            DataSet ds2 = new DataSet();
            string query2 = "Select Distinct DATEPART(MM,REQUESTEDQUOTE.ExpectedTime) AS QuoteMonth,REQUESTEDQUOTE.TotalCost from REQUESTEDQUOTE, HAS, CUSTOMER Where CUSTOMER.CustId=HAS.CustId and REQUESTEDQUOTE.DateTime=HAS.DateTime and CUSTOMER.CustId='" + LblCustId.Text + "'";


            SqlCommand cmd2 = new SqlCommand(query2, con);

            cmd2.ExecuteNonQuery();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);

            da2.Fill(ds2, "PremiumData");

            if (ds2.Tables["PremiumData"].Rows.Count > 0)
            {
                Chart11.DataSource = ds2.Tables["PremiumData"];
                // Set series members names for the X and Y values
                //string x1 = ds2.Tables[0].Rows[0]["TotalCost"].ToString();
                //int result = x1.IndexOf(',');

                //if (result > 0)
                //{
                //    x1 = ((Convert.ToDecimal(x1.ToString())) / 100).ToString();
                //}

                Chart11.Series["Count"].XValueMember = "QuoteMonth";

                Chart11.Series["Count"].YValueMembers = "TotalCost";

                Chart11.Series["Count"].IsValueShownAsLabel = true;
                Chart11.Series["Count"].ChartType = SeriesChartType.Bar;

                Chart11.Titles[1].Text = "Bar Chart";
                Chart11.DataBind();
            }
            con.Close();

        }

        protected void Bound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "style.backgroundColor='LightBlue'");
            e.Row.Attributes.Add("onmouseout", "style.backgroundColor='#fbfbfb'");
        }

        protected void BtnAddContact_Click(object sender, EventArgs e)
        {
            LblNoteHeading.Text = "tilføje nye kontakter";
            BtnUpdate1.Visible = false;
            BtnSaveContact.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
            resetall();
        }

        protected void BtnUpdate1_Click(object sender, EventArgs e)
        {
            int id1 = 0;

            id1 = Convert.ToInt32(LblCustId.Text);

            if (string.IsNullOrWhiteSpace(txtName1.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_UpdateContactsDetails";

                com.Parameters.Add("@I_CONTID", System.Data.SqlDbType.VarChar).Value = LblContId.Text;
                com.Parameters.Add("@NAME", System.Data.SqlDbType.VarChar).Value = txtName1.Text;
                com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = id1;//LblCustId.Text;
                com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile1.Text;
                com.Parameters.Add("@PHONE", System.Data.SqlDbType.VarChar).Value = txtphone1.Text;
                com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail1.Text;
                com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress1.Text;
                com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtcity1.Text;
                com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince1.Text;
                com.Parameters.Add("@REMARK", System.Data.SqlDbType.VarChar).Value = txtRemark.Text;
                com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.VarChar).Value = txtZipCode.Text;
                com.Parameters.Add("@COUNTRY", System.Data.SqlDbType.VarChar).Value = txtCountry1.Text;
                //com.Parameters.Add("@DELETESTATUS", System.Data.SqlDbType.BigInt).Value =false;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }
            BindDataForContacts();
            resetall();
        }

        protected void BtnSaveContact_Click1(object sender, EventArgs e)
        {
            int id = 0;

            id = Convert.ToInt32(LblCustId.Text);
            if (string.IsNullOrWhiteSpace(txtName1.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertContactsDetails";
                com.Parameters.Add("@NAME", System.Data.SqlDbType.VarChar).Value = txtName1.Text;
                com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = id;
                com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile1.Text;
                com.Parameters.Add("@PHONE", System.Data.SqlDbType.VarChar).Value = txtphone1.Text;
                com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail1.Text;
                com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress1.Text;
                com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtcity1.Text;
                com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince1.Text;
                com.Parameters.Add("@REMARK", System.Data.SqlDbType.VarChar).Value = txtRemark.Text;
                com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.VarChar).Value = txtZipCode.Text;
                com.Parameters.Add("@COUNTRY", System.Data.SqlDbType.VarChar).Value = txtCountry1.Text;

                //com.Parameters.Add("@DELETESTATUS", System.Data.SqlDbType.BigInt).Value =false;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }
            BindDataForContacts();
            resetall();
            //Response.Redirect("customer contacts.aspx");
            //string display = "Contact Saved";

            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + display + "');", true);//this will dispaly the alert box


        }

        protected void LBtnActiveAPV_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnActiveAPV.BackColor = System.Drawing.Color.SkyBlue;
                LBtnActiveAPV.BorderColor = System.Drawing.Color.Black;
                LBtnProjects.BackColor = LBtnOverview.BackColor = LBtnContacts.BackColor = LBtnInvoice.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnDocuments.BackColor = LBtnSendMail.BackColor = LBtnBasicInfo.BackColor = LBtnAPV.BackColor = System.Drawing.Color.Empty;
                LBtnProjects.BorderColor = LBtnOverview.BorderColor = LBtnContacts.BorderColor = LBtnInvoice.BorderColor = LBtnOrders.BorderColor = LBtnDocuments.BorderColor = LBtnSendMail.BorderColor = LBtnQuotes.BorderColor = LBtnBasicInfo.BorderColor = LBtnAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.Red;

                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divQuate.Visible = false;
                divProject.Visible = false;
                BtnRefresh.Visible = false;
                divSendMail.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                divAPV.Visible = false;
                DivActiveAPV.Visible = true;

                BindAktiveAPV();
            }
        }

        protected void BtnExportToDoc_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.Parameters.Clear();

            lblsagsnavn.Text = txtsagsnavn.Text;
            lbldeltagere.Text = txtdeltagere.Text;
            lbldeltagere.Visible = true;
            lblsagsnavn.Visible = true;
            txtdeltagere.Visible = false;
            txtsagsnavn.Visible = false;
            DisableAjaxExtenders(this);

            string image = Server.MapPath("~/image/pmglogo1.jpg");

            string strpath = Request.PhysicalApplicationPath + "DocFile" + "\\" + "AktiveAPV" + lblAktiveAPVID.Text + ".doc";
            // string outputPath = Request.PhysicalApplicationPath + "DocFile" + "\\" + "AktiveAPV" + lblAktiveAPVID.Text + ".pdf";

            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ActiveAPV.doc"));
            //Response.AddHeader("content-disposition", "attachment; filename=" + strpath);
            Response.ContentType = "application/ms-word";

            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Charset = "";
            EnableViewState = false;

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            DivPDF0.RenderControl(hw);
            //Image2.RenderControl(hw);
            DivPDF1.RenderControl(hw);
            DivPDF2.RenderControl(hw);
            DivPDF3.RenderControl(hw);
            DivPDF4.RenderControl(hw);
            DivHoriPDF.RenderControl(hw);

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "delete from DocData where AktiveAPVID=" + lblAktiveAPVID.Text;
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "insert into DocData (Cid,ApvId,AktiveAPVID,FileNamedoc,FileNamepdf,FilePathDoc,FilePathPDF) values(@CID,@APVID,@AKTIVEAPVID,@NAME1,@NAME2,@PATHDOC,@PATHPDF)";
            com.Parameters.AddWithValue("@CID", LblCustId.Text);
            com.Parameters.AddWithValue("@APVID", 0);
            com.Parameters.AddWithValue("@AKTIVEAPVID", lblAktiveAPVID.Text);
            com.Parameters.AddWithValue("@NAME1", System.Data.SqlDbType.NVarChar).Value = "AktiveAPV" + lblAktiveAPVID.Text + ".doc";
            com.Parameters.AddWithValue("@NAME2", System.Data.SqlDbType.NVarChar).Value = "";
            com.Parameters.AddWithValue("@PATHDOC", System.Data.SqlDbType.NVarChar).Value = strpath;
            com.Parameters.AddWithValue("@PATHPDF", System.Data.SqlDbType.NVarChar).Value = "";
            com.ExecuteNonQuery();
            con.Close();

            //System.IO.FileStream wFile;
            //byte[] byteData = null;
            //byteData = Encoding.ASCII.GetBytes(sw.ToString());
            //wFile = new FileStream(strpath, FileMode.Append);
            //wFile.Write(byteData, 0, byteData.Length);
            //wFile.Close();

            StreamWriter sw1 = new StreamWriter(strpath);
            sw1.Write(sw.ToString());
            sw1.Close();
            hw.Flush();

            //OpenAktiveDoc(strpath, outputPath);
            //OpenAktiveAPVPdf();

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
            //BindAktiveAPV();
            //Response.Redirect("AddCustomerVertical.aspx");
            // BindData();
        }

        public void OpenAktiveDoc(string inputFile, string outputPath)//---------Not inuse-------------
        {
            //string FilePath = null;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;

            //com.CommandText = "Select FilePath from DocData where AktiveAPVID=" + lblAktiveAPVID.Text;
            //SqlDataAdapter adpt = new SqlDataAdapter(com);
            //System.Data.DataTable dt = new System.Data.DataTable();
            //adpt.Fill(dt);
            //if (dt.Rows.Count > 0)
            //{
            //    FilePath = dt.Rows[0][0].ToString();
            //    inputFile = FilePath.ToString();

            try
            {
                if (outputPath.Equals("") || !File.Exists(inputFile))
                {
                    throw new Exception("Either file does not exist or invalid output path");

                }

                // app to open the document belower
                Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
                Microsoft.Office.Interop.Word.Document wordDocument = new Microsoft.Office.Interop.Word.Document();

                // input variables
                object objInputFile = inputFile;
                object missParam = Type.Missing;

                wordDocument = wordApp.Documents.Open(ref objInputFile, ref missParam, ref missParam, ref missParam,
                    ref missParam, ref missParam, ref missParam, ref missParam, ref missParam, ref missParam,
                    ref missParam, ref missParam, ref missParam, ref missParam, ref missParam, ref missParam);

                if (wordDocument != null)
                {
                    // make the convertion
                    wordDocument.ExportAsFixedFormat(outputPath, WdExportFormat.wdExportFormatPDF, false,
                        WdExportOptimizeFor.wdExportOptimizeForOnScreen, WdExportRange.wdExportAllDocument,
                        0, 0, WdExportItem.wdExportDocumentContent, true, true,
                        WdExportCreateBookmarks.wdExportCreateWordBookmarks, true, true, false, ref missParam);
                }

                // close document and quit application
                wordDocument.Close();
                wordApp.Quit();

                //MessageBox.Show("File successfully converted");
                //ClearTextBoxes();
            }
            catch (Exception e)
            {
                throw e;
            }

            com.Parameters.Clear();
            //com.CommandType = System.Data.CommandType.Text;
            //com.CommandText = "delete from DocData where AktiveAPVID=" + lblAktiveAPVID.Text;
            //com.ExecuteNonQuery();
            //com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;

            com.CommandText = "Update DocData SET Cid=@CID, ApvId=@APVID, FIleNamedoc=@NAMEdoc, FIleNamepdf=@NAMEpdf, FilePathDoc=@PATH1, FilePathpdf=@PATH2 Where  AktiveAPVID= @AKTIVEAPVID";
            com.Parameters.AddWithValue("@CID", LblCustId.Text);
            com.Parameters.AddWithValue("@APVID", 0);
            com.Parameters.AddWithValue("@AKTIVEAPVID", lblAktiveAPVID.Text);
            com.Parameters.AddWithValue("@NAMEdoc", System.Data.SqlDbType.NVarChar).Value = "AktiveAPV" + lblAktiveAPVID.Text + ".doc";
            com.Parameters.AddWithValue("@NAMEpdf", System.Data.SqlDbType.NVarChar).Value = "AktiveAPV" + lblAktiveAPVID.Text + ".pdf";
            com.Parameters.AddWithValue("@PATH1", System.Data.SqlDbType.NVarChar).Value = inputFile;
            com.Parameters.AddWithValue("@PATH2", System.Data.SqlDbType.NVarChar).Value = outputPath;
            com.ExecuteNonQuery();

            //view pdf files 
            try
            {
                if (outputPath.Contains(".pdf"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = outputPath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('File not in Proper format');", true);
            }

            //}
            //else
            //{

            //}
            con.Close();
        }

        public void OpenAktiveAPVPdf()//-----not in use--------------
        {
            string FilePath = null;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;

            com.CommandText = "Select FilePathPDF from DocData where AktiveAPVID=" + lblAktiveAPVID.Text;
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            System.Data.DataTable dt = new System.Data.DataTable();
            adpt.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                FilePath = dt.Rows[0][0].ToString();
                byte[] bytes = File.ReadAllBytes(FilePath);
                iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font pmglogo = FontFactory.GetFont("Times New Roman", 60, iTextSharp.text.Font.BOLD, BaseColor.RED);
                iTextSharp.text.Font stilladser = FontFactory.GetFont("Times New Roman", 16, iTextSharp.text.Font.BOLD, BaseColor.RED);
                using (MemoryStream stream = new MemoryStream())
                {
                    PdfReader reader = new PdfReader(bytes);
                    using (PdfStamper stamper = new PdfStamper(reader, stream))
                    {
                        int pages = reader.NumberOfPages;
                        for (int i = 1; i <= pages; i++)
                        {
                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(i.ToString(), blackFont), 568f, 15f, 0);

                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("PMG", pmglogo), 540f, 740f, 0);
                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("STILLADSER A/S", stilladser), 537f, 720f, 0);
                        }
                    }
                    bytes = stream.ToArray();
                }
                File.WriteAllBytes(FilePath, bytes);


                //FileStream fs = new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.None);
                //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document();

                //PdfWriter writer = PdfWriter.GetInstance(pdfDoc, fs);

                //writer.PageEvent = new ITextEvent();

                //pdfDoc.Open();
                //// pdfDoc.Add(gif);
                //iTextSharp.text.Paragraph p = new iTextSharp.text.Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                //pdfDoc.Add(p);
                //Phrase ph1 = new Phrase(Environment.NewLine);
                //pdfDoc.Add(ph1);

                //pdfDoc.Add(p);

                //pdfDoc.Add(ph1);

                //pdfDoc.Close();
                //Response.Write(pdfDoc);
            }
            else
            {

            }
            con.Close();
        }

        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Verifies that the control is rendered */
        }

        private void DisableAjaxExtenders(Control parent)
        {
            for (int i = 0; i < parent.Controls.Count; i++)
            {
                if (parent.Controls[i].GetType().ToString().IndexOf("Extender") != -1 && parent.Controls[i].ID != null)
                {
                    parent.Controls.RemoveAt(i);
                    parent.Controls[i].Dispose();
                }
                if (parent.Controls[i].Controls.Count > 0)
                {
                    DisableAjaxExtenders(parent.Controls[i]);
                }
            }
        }

        protected void LBtnAPV_Click(object sender, EventArgs e)
        {
            if (LblCustId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt kunde');", true);
                return;
            }

            else
            {
                LBtnAPV.BackColor = System.Drawing.Color.SkyBlue;
                LBtnAPV.BorderColor = System.Drawing.Color.Black;
                LBtnSendMail.BackColor = LBtnProjects.BackColor = LBtnOverview.BackColor = LBtnContacts.BackColor = LBtnOrders.BackColor = LBtnQuotes.BackColor = LBtnInvoice.BackColor = LBtnDocuments.BackColor = LBtnBasicInfo.BackColor = LBtnActiveAPV.BackColor = System.Drawing.Color.Empty;
                LBtnSendMail.BorderColor = LBtnProjects.BorderColor = LBtnOverview.BorderColor = LBtnContacts.BorderColor = LBtnOrders.BorderColor = LBtnQuotes.BorderColor = LBtnInvoice.BorderColor = LBtnDocuments.BorderColor = LBtnBasicInfo.BorderColor = LBtnActiveAPV.BorderColor = System.Drawing.Color.Empty;

                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnContacts.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnDocuments.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOrders.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnProjects.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnInvoice.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnAPV.ForeColor = System.Drawing.Color.Red;
                LBtnActiveAPV.ForeColor = System.Drawing.Color.RoyalBlue;

                divAPV.Visible = true;
                divSendMail.Visible = false;
                divProject.Visible = false;
                divQuate.Visible = false;
                divOverView.Visible = false;
                divBasicInfo.Visible = false;
                divContactInfo.Visible = false;
                divOrder.Visible = false;
                divDocuments.Visible = false;
                divInvoice.Visible = false;
                DivActiveAPV.Visible = false;

                //APVPdf.Visible = true;
                BindAPV();
            }
        }

        protected void BtnAPV_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.Parameters.Clear();

            lbltxtsag.Text = txtsag.Text;
            lbltxtKunde.Text = txtKunde.Text;
            lbltxtKontakt.Text = txtKontakt.Text;
            lbltxtTelefon.Text = txtTelefon.Text;

            imguser.Visible = false;
            lblsagsnr.Visible = false;
            lblsagsAPV.Visible = false;

            lbltxtsag.Visible = true;
            lbltxtKunde.Visible = true;
            lbltxtKontakt.Visible = true;
            lbltxtTelefon.Visible = true;

            txtsag.Visible = false;
            txtKunde.Visible = false;
            txtKontakt.Visible = false;
            txtTelefon.Visible = false;
            DisableAjaxExtenders(this);

            //string strpath = "Downloads " + "\\" + "APV" + LAPVID.Text + ".doc";
            string strpath = Request.PhysicalApplicationPath + "DocFile" + "\\" + "APV" + LAPVID.Text + ".doc";//+ "PDFDATA" + "/"
            // string strpath = Server.MapPath("~/DocFile");
            // string outputPath = Request.PhysicalApplicationPath + "DocFile" + "\\" + "APV" + LAPVID.Text + ".pdf";

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "APV.doc"));
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-word";

            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            EnableViewState = false;

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            divAPV1.RenderControl(hw);

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "delete from DocData where ApvId=" + LAPVID.Text;
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "insert into DocData (Cid,ApvId,AktiveAPVID,FileNamedoc,FileNamepdf,FilePathDoc,FilePathPDF) values(@CID,@APVID,@AKTIVEAPVID,@NAME1,@NAME2,@PATHDOC,@PATHPDF)";
            com.Parameters.AddWithValue("@CID", LblCustId.Text);
            com.Parameters.AddWithValue("@APVID", LAPVID.Text);
            com.Parameters.AddWithValue("@AKTIVEAPVID", 0);
            com.Parameters.AddWithValue("@NAME1", System.Data.SqlDbType.NVarChar).Value = "APV" + LAPVID.Text + ".doc";
            com.Parameters.AddWithValue("@NAME2", System.Data.SqlDbType.NVarChar).Value = "";
            com.Parameters.AddWithValue("@PATHDOC", System.Data.SqlDbType.NVarChar).Value = strpath;
            com.Parameters.AddWithValue("@PATHPDF", System.Data.SqlDbType.NVarChar).Value = "";
            com.ExecuteNonQuery();
            con.Close();

            StreamWriter sw1 = new StreamWriter(strpath);
            sw1.Write(sw.ToString());
            sw1.Close();
            hw.Flush();

            Response.Write(sw.ToString());
            Response.Flush();
            Response.End();

            //OpenDoc(strpath, outputPath);

            //OpenAPVPdf();
        }

        public void OpenDoc(string inputFile, string outputPath)//------------NotIn Use--------------
        {
            // string FilePath = null;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;

            //com.CommandText = "Select FilePath from DocData where ApvId=" + LAPVID.Text;
            //SqlDataAdapter adpt = new SqlDataAdapter(com);
            //System.Data.DataTable dt = new System.Data.DataTable();
            //adpt.Fill(dt);
            //if (dt.Rows.Count > 0)
            //{
            //    FilePath = dt.Rows[0][0].ToString();
            //    inputFile = FilePath.ToString();

            try
            {
                if (outputPath.Equals("") || !File.Exists(inputFile))
                {
                    throw new Exception("Either file does not exist or invalid output path");
                }

                // app to open the document belower
                Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
                Microsoft.Office.Interop.Word.Document wordDocument = new Microsoft.Office.Interop.Word.Document();

                // input variables
                object objInputFile = inputFile;
                object missParam = Type.Missing;

                wordDocument = wordApp.Documents.Open(ref objInputFile, ref missParam, ref missParam, ref missParam,
                    ref missParam, ref missParam, ref missParam, ref missParam, ref missParam, ref missParam,
                    ref missParam, ref missParam, ref missParam, ref missParam, ref missParam, ref missParam);

                if (wordDocument != null)
                {
                    // make the convertion
                    wordDocument.ExportAsFixedFormat(outputPath, WdExportFormat.wdExportFormatPDF, false,
                        WdExportOptimizeFor.wdExportOptimizeForOnScreen, WdExportRange.wdExportAllDocument,
                        0, 0, WdExportItem.wdExportDocumentContent, true, true,
                        WdExportCreateBookmarks.wdExportCreateWordBookmarks, true, true, false, ref missParam);
                }

                // close document and quit application
                wordDocument.Close();
                wordApp.Quit();

                //MessageBox.Show("File successfully converted");
                //ClearTextBoxes();
            }
            catch (Exception e)
            {
                throw e;
            }

            //com.Parameters.Clear();
            //com.CommandType = System.Data.CommandType.Text;
            //com.CommandText = "delete from DocData where ApvId=" + LAPVID.Text;
            //com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "Update DocData SET Cid=@CID,  AktiveAPVID= @AKTIVEAPVID , FIleNamedoc=@NAMEdoc, FIleNamepdf=@NAMEpdf, FilePathDoc=@PATH1, FilePathpdf=@PATH2 Where  ApvId=@APVID";
            com.Parameters.AddWithValue("@CID", LblCustId.Text);
            com.Parameters.AddWithValue("@APVID", LAPVID.Text);
            com.Parameters.AddWithValue("@AKTIVEAPVID", 0);
            com.Parameters.AddWithValue("@NAMEdoc", System.Data.SqlDbType.NVarChar).Value = "APV" + LAPVID.Text + ".doc";
            com.Parameters.AddWithValue("@NAMEpdf", System.Data.SqlDbType.NVarChar).Value = "APV" + LAPVID.Text + ".pdf";
            com.Parameters.AddWithValue("@PATH1", System.Data.SqlDbType.NVarChar).Value = inputFile;
            com.Parameters.AddWithValue("@PATH2", System.Data.SqlDbType.NVarChar).Value = outputPath;
            com.ExecuteNonQuery();
            //}
            //else
            //{

            //}
            try
            {
                if (outputPath.Contains(".pdf"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = outputPath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('File not in Proper format');", true);
            }
            con.Close();
        }

        public void OpenAPVPdf()//-------------Not In Use----------
        {
            string FilePath = null;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;

            com.CommandText = "Select FilePathPDF from DocData where ApvId=" + LAPVID.Text;
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            System.Data.DataTable dt = new System.Data.DataTable();
            adpt.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                FilePath = dt.Rows[0][0].ToString();
                byte[] bytes = File.ReadAllBytes(FilePath);
                iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font pmgFont = FontFactory.GetFont("Times New Roman", 65, iTextSharp.text.Font.BOLD, BaseColor.RED);
                iTextSharp.text.Font StillFont = FontFactory.GetFont("Times New Roman", 17, iTextSharp.text.Font.BOLD, BaseColor.RED);

                iTextSharp.text.Font SagFont = FontFactory.GetFont("Arial", 13, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font Sag_apvFont = FontFactory.GetFont("Arial", 30, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                using (MemoryStream stream = new MemoryStream())
                {
                    PdfReader reader = new PdfReader(bytes);
                    using (PdfStamper stamper = new PdfStamper(reader, stream))
                    {
                        int pages = reader.NumberOfPages;
                        for (int i = 1; i <= pages; i++)
                        {
                            // iTextSharp.text.Paragraph p = new iTextSharp.text.Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));

                            //string image = Server.MapPath("\\image\\pmglogo1.jpg");
                            //iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(image);
                            //gif.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;

                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(i.ToString(), blackFont), 568f, 15f, 0);

                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("PMG", pmgFont), 220f, 730f, 0);
                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("STILLADSER A/S", StillFont), 215, 710f, 0);

                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Sagsnr:", SagFont), 355f, 760f, 0);
                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Sags-APV", Sag_apvFont), 450f, 720f, 0);
                        }
                    }
                    bytes = stream.ToArray();
                }
                File.WriteAllBytes(FilePath, bytes);


                //FileStream fs = new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.None);
                //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document();

                //PdfWriter writer = PdfWriter.GetInstance(pdfDoc, fs);

                //writer.PageEvent = new ITextEvent();

                //pdfDoc.Open();
                //// pdfDoc.Add(gif);
                //iTextSharp.text.Paragraph p = new iTextSharp.text.Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                //pdfDoc.Add(p);
                //Phrase ph1 = new Phrase(Environment.NewLine);
                //pdfDoc.Add(ph1);

                //pdfDoc.Add(p);

                //pdfDoc.Add(ph1);

                //pdfDoc.Close();
                //Response.Write(pdfDoc);
            }
            else
            {

            }
            con.Close();
        }

        protected void BtnAddActiveAPV_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
            //ClientScript.RegisterStartupScript(this.GetType(), "isActive", "<script>pop('pop1');</script>", true);

            BtnSaveActiveAPV.Visible = true;
            BtnUpdateActiveAPV.Visible = false;

            //txtdeltagere.Visible = true;
            //txtsagsnavn.Visible = true;
            //lblsagsnavn.Visible = false;
            //lbldeltagere.Visible = false;
            ClearCheckBox();
        }

        protected void BtnAddAPV_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
            BtnSaveAPV.Visible = true;
            BtnUpdateAPV.Visible = false;
            BtnPDFAPV.Visible = false;

            //lbltxtsag.Visible = false;
            //lbltxtKunde.Visible = false;
            //lbltxtKontakt.Visible = false;
            //lbltxtTelefon.Visible = false;

            //txtsag.Visible = true;
            //txtKunde.Visible = true;
            //txtKontakt.Visible = true;
            //txtTelefon.Visible = true;

            //TextBoxdivAPV12.Visible = true;
            //TextBoxdivAPV13.Visible = true;
            //TextBoxdivAPV14.Visible = true;
            //TextBoxdivAPV15.Visible = true;

            //LabeldivAPV12.Visible = false;
            //LabeldivAPV13.Visible = false;
            //LabeldivAPV14.Visible = false;
            //LabeldivAPV15.Visible = false;

            ClearCheckBoxList();
        }

        protected void btnActivepdf_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            //iTextSharp.text.Font fontH1 = new iTextSharp.text.Font(Currier, 16, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            PdfPTable table = new PdfPTable(6);

            table.TotalWidth = 800f;
            //table.TotalHeight = 200f;
            //fix the absolute width of the table

            table.LockedWidth = true;

            //relative col widths in proportions - 1/3 and 2/3

            float[] widths = new float[] { 1f, 5f, 8f, 3f, 1.5f, 2f };
            table.SetWidths(widths);
            table.HorizontalAlignment = 0;

            //leave a gap before and after the table

            // table.SpacingBefore = 5f;
            //table.SpacingAfter = 5f;

            PdfPCell cell = new PdfPCell();
            //table.AddCell(new PdfPCell(new Phrase(yourDatabaseValue, fontH1)));
            cell.Colspan = 6;

            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);

            table.AddCell(new PdfPCell(new Phrase("Punkt nr.:", blackFont)));
            table.AddCell(new PdfPCell(new Phrase("Beskriv problemet:", blackFont)));
            table.AddCell(new PdfPCell(new Phrase("Beskriv den forebyggende handling eller problemløsning:", blackFont)));
            table.AddCell(new PdfPCell(new Phrase("Iværksættes af:", blackFont)));
            table.AddCell(new PdfPCell(new Phrase("Prioriteret tidsfrist for løsning:", blackFont)));
            table.AddCell(new PdfPCell(new Phrase("Evaluering. Fungerer løsningen?", blackFont)));

            table.AddCell("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            table.AddCell(" ");
            //cell.FixedHeight = 300f;

            table.AddCell(" ");
            //cell.FixedHeight = 300f;

            table.AddCell(" ");
            //cell.FixedHeight = 300f;

            table.AddCell(" ");
            //cell.FixedHeight = 300f;

            table.AddCell(" ");
            //cell.FixedHeight = 300f;

            LabelPunkt_nr.Text = TextBoxPunkt_nr.Text;
            LabelBeskriv_problemet.Text = TextBoxBeskriv_problemet.Text;
            LabelBeskriv_den.Text = TextBoxBeskriv_den.Text;
            LabelIværksættes.Text = TextBoxIværksættes.Text;
            LabelPrioriteret.Text = TextBoxPrioriteret.Text;
            LabelEvaluering.Text = TextBoxEvaluering.Text;

            TextBoxPunkt_nr.Visible = false;
            TextBoxBeskriv_problemet.Visible = false;
            TextBoxBeskriv_den.Visible = false;
            TextBoxIværksættes.Visible = false;
            TextBoxPrioriteret.Visible = false;
            TextBoxEvaluering.Visible = false;

            Prepare1(DivPDF0);
            Prepare1(DivPDF1);
            Prepare1(DivPDF2);
            Prepare1(DivPDF3);
            Prepare1(DivPDF4);
            Prepare1(DivHoriPDF1);
            Prepare1(DivHoriPDF2);
            Prepare1(DivHoriPDF3);

            lblsagsnavn.Text = txtsagsnavn.Text;
            lbldeltagere.Text = txtdeltagere.Text;

            lbldeltagere.Visible = true;
            lblsagsnavn.Visible = true;

            txtdeltagere.Visible = false;
            txtsagsnavn.Visible = false;

            DisableAjaxExtenders(this);

            string path = Server.MapPath("~/DocFile/");
            string image = Server.MapPath("~/image/pmglogo1.jpg");
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ActiveAPV.pdf"));
            Response.ContentType = "application/pdf";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Charset = "";
            EnableViewState = false;

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);

            StringWriter sw2 = new StringWriter();
            HtmlTextWriter hw2 = new HtmlTextWriter(sw2);

            StringWriter sw3 = new StringWriter();
            HtmlTextWriter hw3 = new HtmlTextWriter(sw3);

            //iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
            //styles.LoadTagStyle("td11", "background-color", "Blue");
            //styles.LoadTagStyle(HtmlTags.TD, HtmlTags.BGCOLOR, "LightBlue");

            DivPDF0.RenderControl(hw);
            DivPDF1.RenderControl(hw);
            DivPDF2.RenderControl(hw);
            DivPDF3.RenderControl(hw);
            DivPDF4.RenderControl(hw);
            DivHoriPDF1.RenderControl(hw1);
            DivHoriPDF2.RenderControl(hw2);
            DivHoriPDF3.RenderControl(hw3);

            StringReader sr = new StringReader(sw.ToString());
            StringReader sr1 = new StringReader(sw1.ToString());
            StringReader sr2 = new StringReader(sw2.ToString());
            StringReader sr3 = new StringReader(sw3.ToString());

            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20f, 20f, 100f, 55f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //htmlparser.SetStyleSheet(styles);
            FileStream file = new FileStream(path + "AktiveAPV" + lblAktiveAPVID.Text + ".pdf", FileMode.Create);

            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            PdfWriter writer1 = PdfWriter.GetInstance(pdfDoc, file);
            writer.PageEvent = new ITextEvent();
            writer1.PageEvent = new ITextEvent();

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "delete from DocData where AktiveAPVID=" + lblAktiveAPVID.Text;
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "insert into DocData (Cid,ApvId,AktiveAPVID,FileNamepdf,FilePathPDF) values(@CID,@APVID,@AKTIVEAPVID,@NAME1,@PATHPDF)";
            com.Parameters.AddWithValue("@CID", LblCustId.Text);
            com.Parameters.AddWithValue("@APVID", 0);
            com.Parameters.AddWithValue("@AKTIVEAPVID", lblAktiveAPVID.Text);
            com.Parameters.AddWithValue("@NAME1", System.Data.SqlDbType.NVarChar).Value = "AktiveAPV" + lblAktiveAPVID.Text + ".pdf";
            com.Parameters.AddWithValue("@PATHPDF", System.Data.SqlDbType.NVarChar).Value = (path + "AktiveAPV" + lblAktiveAPVID.Text + ".pdf");
            com.ExecuteNonQuery();
            con.Close();

            pdfDoc.Open();
            htmlparser.Parse(sr);

            pdfDoc.SetPageSize(new iTextSharp.text.Rectangle(850f, 680f));
            pdfDoc.NewPage();
            htmlparser.Parse(sr1);

            if ((LabelPunkt_nr.Text == "") && (LabelBeskriv_problemet.Text == "") && (LabelBeskriv_den.Text == "") && (LabelIværksættes.Text == "") && (LabelPrioriteret.Text == "") && (LabelEvaluering.Text == ""))
            {
                pdfDoc.Add(table);
            }
            else
            {
                htmlparser.Parse(sr2);
            }
            htmlparser.Parse(sr3);
            writer.CloseStream = false;
            pdfDoc.Close();

            Response.Write(pdfDoc);
            Response.End();
        }

        public void Prepare1(Control control)
        {
            string path;
            path = Server.MapPath("~/image/");
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is System.Web.UI.WebControls.CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as System.Web.UI.WebControls.CheckBox).Checked ? "<img  src='" + path + "checked.png' alt=''width='15' height='15' float='right' />" : "<img border='10' src='" + path + "unchecked.png' alt=''width='15' height='15' float='right' />"));
                    //current.Visible = true;
                }
                else if (current is CheckBoxList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBoxList).SelectedItem.Value = "<img  src='" + path + "checked.png' alt=''width='15' height='15' float='right' />"));
                    //: "<img border='10' src='" + path + "unchecked.png' alt=''width='15' height='15' float='right' />"));
                    current.Visible = true;
                }
            }
        }

        protected void SaveActiveAPV()
        {
            if (string.IsNullOrEmpty(txtsagsnavn.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Fill Sagsnavn Field');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                SqlCommand com = new SqlCommand();
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertAktive_APV";
                com.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                com.Parameters.Add("@SAGSNAVN", System.Data.SqlDbType.NVarChar).Value = txtsagsnavn.Text;
                com.Parameters.Add("@APV_DELTAGERE", System.Data.SqlDbType.NVarChar).Value = txtdeltagere.Text;

                if (CheckBox1.Checked == true)
                {
                    com.Parameters.Add("@ER_ALLE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox2.Checked == true)
                {
                    com.Parameters.Add("@ER_ALLE", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_ALLE", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox3.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q1", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox4.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q1", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q1", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox5.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q2", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox6.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q2", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q2", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox7.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q3", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox8.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q3", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q3", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox9.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q4", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox10.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q4", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q4", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox11.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q5", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox12.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q5", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q5", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox13.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q6", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox14.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q6", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q6", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox15.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q7", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox16.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q7", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q7", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox17.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q8", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox18.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q8", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q8", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox19.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q9", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox20.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q9", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q9", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox21.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q10", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox22.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q10", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q10", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox23.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11A", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox24.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11A", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11A", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox25.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11B", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox26.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11B", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11B", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox27.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11C", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox28.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11C", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11C", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox29.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11D", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox30.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11D", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11D", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox31.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11E", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox32.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11E", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11E", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox33.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11F", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox34.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11F", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11F", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox35.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox36.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox37.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12A", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox38.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12A", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12A", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox39.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12B", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox40.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12B", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12B", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox41.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12C", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox42.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12C", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12C", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox43.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12D", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox44.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12D", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12D", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox45.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q13", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox46.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q13", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q13", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox47.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q14", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox48.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q14", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q14", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }
        }

        protected void BtnSaveActiveAPV_Click(object sender, EventArgs e)
        {
            SaveActiveAPV();
            ClearCheckBox();
            BindAktiveAPV();
        }

        public void ClearCheckBox()
        {
            txtsagsnavn.Text = "";
            txtdeltagere.Text = "";

            TextBoxPunkt_nr.Text = "";
            TextBoxBeskriv_problemet.Text = "";
            TextBoxBeskriv_den.Text = "";
            TextBoxIværksættes.Text = "";
            TextBoxPrioriteret.Text = "";
            TextBoxEvaluering.Text = "";

            CheckBox1.Checked = false;
            CheckBox2.Checked = false;
            CheckBox3.Checked = false;
            CheckBox4.Checked = false;
            CheckBox5.Checked = false;
            CheckBox6.Checked = false;
            CheckBox7.Checked = false;
            CheckBox8.Checked = false;
            CheckBox9.Checked = false;
            CheckBox10.Checked = false;
            CheckBox11.Checked = false;
            CheckBox12.Checked = false;
            CheckBox13.Checked = false;
            CheckBox14.Checked = false;
            CheckBox15.Checked = false;
            CheckBox16.Checked = false;
            CheckBox17.Checked = false;
            CheckBox18.Checked = false;
            CheckBox19.Checked = false;
            CheckBox20.Checked = false;
            CheckBox21.Checked = false;
            CheckBox22.Checked = false;
            CheckBox23.Checked = false;
            CheckBox24.Checked = false;
            CheckBox25.Checked = false;
            CheckBox26.Checked = false;
            CheckBox27.Checked = false;
            CheckBox28.Checked = false;
            CheckBox29.Checked = false;
            CheckBox30.Checked = false;
            CheckBox31.Checked = false;
            CheckBox32.Checked = false;
            CheckBox33.Checked = false;
            CheckBox34.Checked = false;
            CheckBox35.Checked = false;
            CheckBox36.Checked = false;
            CheckBox37.Checked = false;
            CheckBox38.Checked = false;
            CheckBox39.Checked = false;
            CheckBox40.Checked = false;
            CheckBox41.Checked = false;
            CheckBox42.Checked = false;
            CheckBox43.Checked = false;
            CheckBox44.Checked = false;
            CheckBox45.Checked = false;
            CheckBox46.Checked = false;
            CheckBox47.Checked = false;
            CheckBox48.Checked = false;
        }

        protected void BindAktiveAPV()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "Select Distinct AktiveAPVID, sagsnavn from AktiveAPV Where CID=@I_CUSTID";
            com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                AktiveAPV_Header.Visible = true;
                GridActiveAPV.DataSource = dt;
                GridActiveAPV.DataBind();
            }
            else
            {
                GridActiveAPV.DataSource = null;
                GridActiveAPV.DataBind();
                AktiveAPV_Header.Visible = false;
            }
            con.Close();
        }

        protected void LBtnAktiveAPV_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string AktiveID = grdrow.Cells[1].Text;

            lblAktiveAPVID.Text = AktiveID.ToString(); // FOr Updating Active APV Per Customer---

            BtnSaveActiveAPV.Visible = false;
            BtnUpdateActiveAPV.Visible = true;
            btnActivepdf.Visible = true;

            //txtsagsnavn.Visible = true;
            //txtdeltagere.Visible = true;

            //lblsagsnavn.Text = "";
            //lbldeltagere.Text = "";

            //TextBoxPunkt_nr.Visible = true;
            //TextBoxBeskriv_problemet.Visible = true;
            //TextBoxBeskriv_den.Visible = true;
            //TextBoxIværksættes.Visible = true;
            //TextBoxPrioriteret.Visible = true;
            //TextBoxEvaluering.Visible = true;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select DIstinct * from AktiveAPV where AktiveAPVID =@I_AktiveAPVID ", con);

            //String var = lbAktiveAPV.Text;
            cmd.Parameters.Add("@I_AktiveAPVID", System.Data.SqlDbType.BigInt).Value = AktiveID;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);

            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                txtsagsnavn.Text = dt.Tables[0].Rows[0]["sagsnavn"].ToString();
                txtdeltagere.Text = dt.Tables[0].Rows[0]["APV_deltagere"].ToString();
                string ER_alle = dt.Tables[0].Rows[0]["ER_alle"].ToString();

                if (ER_alle.ToString() != null)
                {
                    if (ER_alle.ToString() == "True")
                    {
                        CheckBox1.Checked = true;
                    }
                    if (ER_alle.ToString() == "False")
                    {
                        CheckBox2.Checked = true;
                    }
                }
                string Q1 = dt.Tables[0].Rows[0]["Er_APV_Q1"].ToString();

                if (Q1.ToString() != null)
                {
                    if (Q1.ToString() == "True")
                    {
                        CheckBox3.Checked = true;
                    }
                    if (Q1.ToString() == "False")
                    {
                        CheckBox4.Checked = true;
                    }
                }
                string Q2 = dt.Tables[0].Rows[0]["Er_APV_Q2"].ToString();

                if (Q2.ToString() != null)
                {
                    if (Q2.ToString() == "True")
                    {
                        CheckBox5.Checked = true;
                    }
                    if (Q2.ToString() == "False")
                    {
                        CheckBox6.Checked = true;
                    }
                }

                string Q3 = dt.Tables[0].Rows[0]["Er_APV_Q3"].ToString();

                if (Q3.ToString() != null)
                {
                    if (Q3.ToString() == "True")
                    {
                        CheckBox7.Checked = true;
                    }
                    if (Q3.ToString() == "False")
                    {
                        CheckBox8.Checked = true;
                    }
                }
                string Q4 = dt.Tables[0].Rows[0]["Er_APV_Q4"].ToString();

                if (Q4.ToString() != null)
                {
                    if (Q4.ToString() == "True")
                    {
                        CheckBox9.Checked = true;
                    }
                    if (Q4.ToString() == "False")
                    {
                        CheckBox10.Checked = true;
                    }
                }
                string Q5 = dt.Tables[0].Rows[0]["Er_APV_Q5"].ToString();

                if (Q5.ToString() != null)
                {
                    if (Q5.ToString() == "True")
                    {
                        CheckBox11.Checked = true;
                    }
                    if (Q5.ToString() == "False")
                    {
                        CheckBox12.Checked = true;
                    }
                }
                string Q6 = dt.Tables[0].Rows[0]["Er_APV_Q6"].ToString();

                if (Q6.ToString() != null)
                {
                    if (Q6.ToString() == "True")
                    {
                        CheckBox13.Checked = true;
                    }
                    if (Q6.ToString() == "False")
                    {
                        CheckBox14.Checked = true;
                    }
                }
                string Q7 = dt.Tables[0].Rows[0]["Er_APV_Q7"].ToString();

                if (Q7.ToString() != null)
                {
                    if (Q7.ToString() == "True")
                    {
                        CheckBox15.Checked = true;
                    }
                    if (Q7.ToString() == "False")
                    {
                        CheckBox16.Checked = true;
                    }
                }
                string Q8 = dt.Tables[0].Rows[0]["Er_APV_Q8"].ToString();

                if (Q8.ToString() != null)
                {
                    if (Q8.ToString() == "True")
                    {
                        CheckBox17.Checked = true;
                    }
                    if (Q8.ToString() == "False")
                    {
                        CheckBox18.Checked = true;
                    }
                }
                string Q9 = dt.Tables[0].Rows[0]["Er_APV_Q9"].ToString();

                if (Q9.ToString() != null)
                {
                    if (Q9.ToString() == "True")
                    {
                        CheckBox19.Checked = true;
                    }
                    if (Q9.ToString() == "False")
                    {
                        CheckBox20.Checked = true;
                    }
                }
                string Q10 = dt.Tables[0].Rows[0]["Er_APV_Q10"].ToString();

                if (Q10.ToString() != null)
                {
                    if (Q10.ToString() == "True")
                    {
                        CheckBox21.Checked = true;
                    }
                    if (Q10.ToString() == "False")
                    {
                        CheckBox22.Checked = true;
                    }
                }
                string Q11A = dt.Tables[0].Rows[0]["Er_APV_Q11A"].ToString();

                if (Q11A.ToString() != null)
                {
                    if (Q11A.ToString() == "True")
                    {
                        CheckBox23.Checked = true;
                    }
                    if (Q11A.ToString() == "False")
                    {
                        CheckBox24.Checked = true;
                    }
                }
                string Q11B = dt.Tables[0].Rows[0]["Er_APV_Q11B"].ToString();

                if (Q11B.ToString() != null)
                {
                    if (Q11B.ToString() == "True")
                    {
                        CheckBox25.Checked = true;
                    }
                    if (Q11B.ToString() == "False")
                    {
                        CheckBox26.Checked = true;
                    }
                }
                string Q11C = dt.Tables[0].Rows[0]["Er_APV_Q11C"].ToString();

                if (Q11C.ToString() != null)
                {
                    if (Q11C.ToString() == "True")
                    {
                        CheckBox27.Checked = true;
                    }
                    if (Q11C.ToString() == "False")
                    {
                        CheckBox28.Checked = true;
                    }
                }
                string Q11D = dt.Tables[0].Rows[0]["Er_APV_Q11D"].ToString();

                if (Q11D.ToString() != null)
                {
                    if (Q11D.ToString() == "True")
                    {
                        CheckBox29.Checked = true;
                    }
                    if (Q11D.ToString() == "False")
                    {
                        CheckBox30.Checked = true;
                    }
                }
                string Q11E = dt.Tables[0].Rows[0]["Er_APV_Q11E"].ToString();

                if (Q11E.ToString() != null)
                {
                    if (Q11E.ToString() == "True")
                    {
                        CheckBox31.Checked = true;
                    }
                    if (Q11E.ToString() == "False")
                    {
                        CheckBox32.Checked = true;
                    }
                }
                string Q11F = dt.Tables[0].Rows[0]["Er_APV_Q11F"].ToString();

                if (Q11F.ToString() != null)
                {
                    if (Q11F.ToString() == "True")
                    {
                        CheckBox33.Checked = true;
                    }
                    if (Q11F.ToString() == "False")
                    {
                        CheckBox34.Checked = true;
                    }
                }
                string Q12 = dt.Tables[0].Rows[0]["Er_APV_Q12"].ToString();

                if (Q12.ToString() != null)
                {
                    if (Q12.ToString() == "True")
                    {
                        CheckBox35.Checked = true;
                    }
                    if (Q12.ToString() == "False")
                    {
                        CheckBox36.Checked = true;
                    }
                }
                string Q12A = dt.Tables[0].Rows[0]["Er_APV_Q12A"].ToString();

                if (Q12A.ToString() != null)
                {
                    if (Q12A.ToString() == "True")
                    {
                        CheckBox37.Checked = true;
                    }
                    if (Q12A.ToString() == "False")
                    {
                        CheckBox38.Checked = true;
                    }
                }
                string Q12B = dt.Tables[0].Rows[0]["Er_APV_Q12B"].ToString();

                if (Q12B.ToString() != null)
                {
                    if (Q12B.ToString() == "True")
                    {
                        CheckBox39.Checked = true;
                    }
                    if (Q12B.ToString() == "False")
                    {
                        CheckBox40.Checked = true;
                    }
                }
                string Q12C = dt.Tables[0].Rows[0]["Er_APV_Q12C"].ToString();

                if (Q12C.ToString() != null)
                {
                    if (Q12C.ToString() == "True")
                    {
                        CheckBox41.Checked = true;
                    }
                    if (Q12C.ToString() == "False")
                    {
                        CheckBox42.Checked = true;
                    }
                }
                string Q12D = dt.Tables[0].Rows[0]["Er_APV_Q12D"].ToString();

                if (Q12D.ToString() != null)
                {
                    if (Q12D.ToString() == "True")
                    {
                        CheckBox43.Checked = true;
                    }
                    if (Q12D.ToString() == "False")
                    {
                        CheckBox44.Checked = true;
                    }
                }
                string Q13 = dt.Tables[0].Rows[0]["Er_APV_Q13"].ToString();

                if (Q13.ToString() != null)
                {
                    if (Q13.ToString() == "True")
                    {
                        CheckBox45.Checked = true;
                    }
                    if (Q13.ToString() == "False")
                    {
                        CheckBox46.Checked = true;
                    }
                }
                string Q14 = dt.Tables[0].Rows[0]["Er_APV_Q14"].ToString();

                if (Q14.ToString() != null)
                {
                    if (Q14.ToString() == "True")
                    {
                        CheckBox47.Checked = true;
                    }
                    if (Q14.ToString() == "False")
                    {
                        CheckBox48.Checked = true;
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void BtnUpdateActiveAPV_Click(object sender, EventArgs e)
        {
            UpdateAktiveAPV();
            ClearCheckBox();
        }

        protected void UpdateAktiveAPV()
        {
            if (string.IsNullOrEmpty(txtsagsnavn.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Field Sagsnavn Field');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                SqlCommand com = new SqlCommand();
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_UpdateAktive_APV";
                com.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                com.Parameters.Add("@I_AktiveAPVID", System.Data.SqlDbType.BigInt).Value = lblAktiveAPVID.Text;
                com.Parameters.Add("@SAGSNAVN", System.Data.SqlDbType.NVarChar).Value = txtsagsnavn.Text;
                com.Parameters.Add("@APV_DELTAGERE", System.Data.SqlDbType.NVarChar).Value = txtdeltagere.Text;

                if (CheckBox1.Checked == true)
                {
                    com.Parameters.Add("@ER_ALLE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox2.Checked == true)
                {
                    com.Parameters.Add("@ER_ALLE", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_ALLE", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox3.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q1", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox4.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q1", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q1", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox5.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q2", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox6.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q2", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q2", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox7.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q3", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox8.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q3", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q3", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox9.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q4", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox10.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q4", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q4", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox11.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q5", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox12.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q5", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q5", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox13.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q6", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox14.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q6", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q6", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox15.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q7", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox16.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q7", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q7", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox17.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q8", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox18.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q8", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q8", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox19.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q9", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox20.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q9", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q9", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox21.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q10", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox22.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q10", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q10", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox23.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11A", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox24.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11A", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11A", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox25.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11B", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox26.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11B", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11B", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox27.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11C", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox28.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11C", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11C", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox29.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11D", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox30.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11D", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11D", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }

                if (CheckBox31.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11E", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox32.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11E", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11E", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox33.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11F", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox34.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q11F", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q11F", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox35.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox36.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox37.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12A", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox38.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12A", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12A", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox39.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12B", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox40.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12B", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12B", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }



                if (CheckBox41.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12C", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox42.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12C", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12C", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox43.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12D", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox44.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q12D", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q12D", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox45.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q13", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox46.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q13", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q13", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }


                if (CheckBox47.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q14", System.Data.SqlDbType.Bit).Value = 1;
                }
                else if (CheckBox48.Checked == true)
                {
                    com.Parameters.Add("@ER_APV_Q14", System.Data.SqlDbType.Bit).Value = 0;
                }
                else
                {
                    com.Parameters.Add("@ER_APV_Q14", System.Data.SqlDbType.Bit).Value = DBNull.Value;
                }

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox2.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox1.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox4.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox4_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox3.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox5_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox6.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox6_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox5.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox7_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox8.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox8_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox7.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox9_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox10.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox10_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox9.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox11_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox12.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox12_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox11.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox13_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox13.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox14_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox14.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox15_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox16.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);

        }

        protected void CheckBox16_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox17.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox17_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox18.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox18_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox17.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox19_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox20.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox20_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox19.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox21_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox22.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox22_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox21.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox23_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox24.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox24_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox23.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox25_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox26.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox26_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox25.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox27_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox28.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox28_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox27.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox29_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox30.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox30_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox29.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox31_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox32.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox32_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox31.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox33_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox34.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox34_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox33.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox35_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox36.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox36_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox35.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox37_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox38.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox38_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox37.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox39_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox40.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox40_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox39.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox41_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox42.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox42_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox41.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox43_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox44.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox44_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox43.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox45_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox46.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox46_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox45.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox47_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox48.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void CheckBox48_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox47.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void BtnSaveAPV_Click(object sender, EventArgs e)
        {
            SaveAPV();
            ClearCheckBoxList();
            BindAPV();
        }

        protected void SaveAPV()
        {
            if (string.IsNullOrEmpty(txtsag.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Fill Sag Field');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                SqlCommand com = new SqlCommand();
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_Insert_APV";
                com.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                com.Parameters.Add("@SAG", System.Data.SqlDbType.NVarChar).Value = txtsag.Text;
                com.Parameters.Add("@KUNDE", System.Data.SqlDbType.NVarChar).Value = txtKunde.Text;
                com.Parameters.Add("@KONTAKT", System.Data.SqlDbType.Decimal).Value = txtKontakt.Text;
                com.Parameters.Add("@TELEFON", System.Data.SqlDbType.Decimal).Value = txtTelefon.Text;
                com.Parameters.Add("@COMPLATIONDATE", System.Data.SqlDbType.Date).Value = DateTime.Now;

                //for (int i = 0; i < ChkLstStillads1.Items.Count - 1; i++)
                // {
                if (CheckBox49.Checked)
                {
                    //string var = string.Empty;
                    //var += ChkLstStillads1.Items[0].Text.ToString();
                    com.Parameters.Add("@FACADE", System.Data.SqlDbType.Bit).Value = 1;
                    //com.Parameters.AddWithValue("@FACADE", var);
                }
                else
                {
                    com.Parameters.Add("@FACADE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox50.Checked)
                {
                    com.Parameters.Add("@VINDUER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@VINDUER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox51.Checked)
                {
                    com.Parameters.Add("@TAG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TAG", System.Data.SqlDbType.Bit).Value = 0;
                }
                // }

                // for (int i = 0; i < ChkLstStillads2.Items.Count - 1; i++)
                // {
                if (CheckBox52.Checked)
                {
                    com.Parameters.Add("@GADE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GADE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox53.Checked)
                {
                    com.Parameters.Add("@GÅRD", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GÅRD", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox54.Checked)
                {
                    com.Parameters.Add("@STILLADS_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@STILLADS_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < ChklstStilladssystem.Items.Count - 1; i++)
                //{
                if (CheckBox55.Checked)
                {
                    com.Parameters.Add("@EIGHT", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@EIGHT", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox56.Checked)
                {
                    com.Parameters.Add("@HAKI_RAM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HAKI_RAM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox57.Checked)
                {
                    com.Parameters.Add("@HAKI_MUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HAKI_MUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < ChklstLastklasse.Items.Count - 1; i++)
                //{
                if (CheckBox58.Checked)
                {
                    com.Parameters.Add("@KI_3_200KG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@KI_3_200KG", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox59.Checked)
                {
                    com.Parameters.Add("@KI_5_450KG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@KI_5_450KG", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox60.Checked)
                {
                    string var = string.Empty;
                    //var += ChklstLastklasse.Items[2].Text.ToString();
                    var += CheckBox60.Checked.ToString();
                    com.Parameters.AddWithValue("@LASTKLASSE", var);
                    //com.Parameters.Add("@LASTKLASSE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LASTKLASSE", System.Data.SqlDbType.NVarChar).Value = "";
                }
                //}

                //for (int i = 0; i < ChklstFastgørelser.Items.Count - 1; i++)
                //{
                if (CheckBox61.Checked)
                {
                    com.Parameters.Add("@MUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox62.Checked)
                {
                    com.Parameters.Add("@TRÆ", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRÆ", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox63.Checked)
                {
                    com.Parameters.Add("@BETON", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BETON", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox64.Checked)
                {
                    com.Parameters.Add("@FASTGØRELSER_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@FASTGØRELSER_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < ChklstStrømforsyning.Items.Count - 1; i++)
                //{
                if (CheckBox65.Checked)
                {
                    com.Parameters.Add("@BYGGESTRØM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BYGGESTRØM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox66.Checked)
                {
                    com.Parameters.Add("@GENERATOR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GENERATOR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox67.Checked)
                {
                    com.Parameters.Add("@STRØMFORSYNING_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@STRØMFORSYNING_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < ChklstTran_materiel.Items.Count - 1; i++)
                //{
                if (CheckBox68.Checked)
                {
                    com.Parameters.Add("@LASTBIL", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LASTBIL", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox69.Checked)
                {
                    com.Parameters.Add("@LILLE_BIL", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LILLE_BIL", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox70.Checked)
                {
                    com.Parameters.Add("@TRUCK", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRUCK", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox71.Checked)
                {
                    com.Parameters.Add("@TRÆKVOGN", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRÆKVOGN", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox72.Checked)
                {
                    com.Parameters.Add("@ALURULLEVOGNE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@ALURULLEVOGNE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox73.Checked)
                {
                    com.Parameters.Add("@TRANSPORT_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRANSPORT_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < Chklst_værnemidler.Items.Count - 1; i++)
                //{
                if (CheckBox74.Checked)
                {
                    com.Parameters.Add("@SELE_LINE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@SELE_LINE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox75.Checked)
                {
                    com.Parameters.Add("@MASKE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MASKE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox76.Checked)
                {
                    com.Parameters.Add("@HØREVÆRM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HØREVÆRM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox77.Checked)
                {
                    com.Parameters.Add("@BRILLER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BRILLER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox78.Checked)
                {
                    com.Parameters.Add("@VÆRNEMIDLER_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@VÆRNEMIDLER_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < Chklst_Velfærdsforanstaltninger.Items.Count - 1; i++)
                //{
                if (CheckBox79.Checked)
                {
                    com.Parameters.Add("@DELESKUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@DELESKUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox80.Checked)
                {
                    com.Parameters.Add("@MANGLER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MANGLER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox81.Checked)
                {
                    com.Parameters.Add("@ADGANG_TIL_TOILET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@ADGANG_TIL_TOILET", System.Data.SqlDbType.Bit).Value = 0;
                }
                // }

                com.Parameters.Add("@NOTE", System.Data.SqlDbType.NVarChar).Value = "";

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }
        }

        public void ClearCheckBoxList()
        {
            txtsag.Text = "";
            txtKunde.Text = "";
            txtKontakt.Text = "";
            txtTelefon.Text = "";
            txtSjak.Text = "";

            TextBoxdivAPV12.Text = "";
            TextBoxdivAPV13.Text = "";
            TextBoxdivAPV14.Text = "";
            TextBoxdivAPV15.Text = "";

            if (CheckBox49.Checked == true)
            {
                CheckBox49.Checked = false;
            }
            if (CheckBox50.Checked == true)
            {
                CheckBox50.Checked = false;
            }
            if (CheckBox51.Checked == true)
            {
                CheckBox51.Checked = false;
            }
            if (CheckBox52.Checked == true)
            {
                CheckBox52.Checked = false;
            }
            if (CheckBox53.Checked == true)
            {
                CheckBox53.Checked = false;
            }
            if (CheckBox54.Checked == true)
            {
                CheckBox54.Checked = false;
            }
            if (CheckBox55.Checked == true)
            {
                CheckBox55.Checked = false;
            }
            if (CheckBox56.Checked == true)
            {
                CheckBox56.Checked = false;
            }
            if (CheckBox57.Checked == true)
            {
                CheckBox57.Checked = false;
            }
            if (CheckBox58.Checked == true)
            {
                CheckBox58.Checked = false;
            }
            if (CheckBox59.Checked == true)
            {
                CheckBox59.Checked = false;
            }
            if (CheckBox60.Checked == true)
            {
                CheckBox60.Checked = false;
            }
            if (CheckBox61.Checked == true)
            {
                CheckBox61.Checked = false;
            }
            if (CheckBox62.Checked == true)
            {
                CheckBox62.Checked = false;
            }
            if (CheckBox63.Checked == true)
            {
                CheckBox63.Checked = false;
            }
            if (CheckBox64.Checked == true)
            {
                CheckBox64.Checked = false;
            }
            if (CheckBox65.Checked == true)
            {
                CheckBox65.Checked = false;
            }
            if (CheckBox66.Checked == true)
            {
                CheckBox66.Checked = false;
            }
            if (CheckBox67.Checked == true)
            {
                CheckBox67.Checked = false;
            }

            if (CheckBox68.Checked == true)
            {
                CheckBox68.Checked = false;
            }
            if (CheckBox69.Checked == true)
            {
                CheckBox69.Checked = false;
            }
            if (CheckBox70.Checked == true)
            {
                CheckBox70.Checked = false;
            }
            if (CheckBox71.Checked == true)
            {
                CheckBox71.Checked = false;
            }
            if (CheckBox72.Checked == true)
            {
                CheckBox72.Checked = false;
            }
            if (CheckBox73.Checked == true)
            {
                CheckBox73.Checked = false;
            }

            if (CheckBox74.Checked == true)
            {
                CheckBox74.Checked = false;
            }
            if (CheckBox75.Checked == true)
            {
                CheckBox75.Checked = false;
            }
            if (CheckBox76.Checked == true)
            {
                CheckBox76.Checked = false;
            }
            if (CheckBox77.Checked == true)
            {
                CheckBox77.Checked = false;
            }
            if (CheckBox78.Checked == true)
            {
                CheckBox78.Checked = false;
            }
            if (CheckBox79.Checked == true)
            {
                CheckBox79.Checked = false;
            }
            if (CheckBox80.Checked == true)
            {
                CheckBox80.Checked = false;
            }
            if (CheckBox81.Checked == true)
            {
                CheckBox81.Checked = false;
            }
        }

        protected void BindAPV()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "Select Distinct APVID, Sag, Kunde, Kontakt, Telefon, ComplationDate from APV Where CID=@I_CUSTID";
            com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                DivAPV_Header.Visible = true;
                GridViewAPV.DataSource = dt;
                GridViewAPV.DataBind();
            }
            else
            {
                GridViewAPV.DataSource = null;
                GridViewAPV.DataBind();
                DivAPV_Header.Visible = false;
            }
            con.Close();
        }

        protected void LbtnEditAPV_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string APVID = grdrow.Cells[1].Text;
            LAPVID.Text = APVID;

            BtnSaveAPV.Visible = false;
            BtnUpdateAPV.Visible = true;
            BtnPDFAPV.Visible = true;

            //txtsag.Visible = true;
            //txtKunde.Visible = true;
            //txtKontakt.Visible = true;
            //txtTelefon.Visible = true;

            //lbltxtsag.Text = "";
            //lbltxtKunde.Text = "";
            //lbltxtKontakt.Text = "";
            //lbltxtTelefon.Text = "";

            //TextBoxdivAPV12.Visible = true;
            //TextBoxdivAPV13.Visible = true;
            //TextBoxdivAPV14.Visible = true;
            //TextBoxdivAPV15.Visible = true;

            //LabeldivAPV12.Visible = false;
            //LabeldivAPV13.Visible = false;
            //LabeldivAPV14.Visible = false;
            //LabeldivAPV15.Visible = false;

            lblSimpleAPVID.Text = APVID.ToString(); // FOr Updating  APV Per Customer---

            //BtnSaveActiveAPV.Visible = false;
            //BtnUpdateActiveAPV.Visible = true;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select DIstinct * from APV where APVID = @I_APVID ", con);

            //String var = lbAktiveAPV.Text;
            cmd.Parameters.Add("@I_APVID", System.Data.SqlDbType.BigInt).Value = APVID;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);

            DataSet dt = new DataSet();

            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                txtsag.Text = dt.Tables[0].Rows[0]["Sag"].ToString();
                txtKunde.Text = dt.Tables[0].Rows[0]["Kunde"].ToString();
                txtKontakt.Text = dt.Tables[0].Rows[0]["Kontakt"].ToString();
                txtTelefon.Text = dt.Tables[0].Rows[0]["Telefon"].ToString();
                //txtKunde.Text = dt.Tables[0].Rows[0]["ComplationDate"].ToString();

                string Facade = dt.Tables[0].Rows[0]["Facade"].ToString();

                if (Facade.ToString() != null)
                {
                    if (Facade.ToString() == "True")
                    {
                        CheckBox49.Checked = true;
                    }

                }
                string Vinduer = dt.Tables[0].Rows[0]["Vinduer"].ToString();

                if (Vinduer.ToString() != null)
                {
                    if (Vinduer.ToString() == "True")
                    {
                        CheckBox50.Checked = true;
                    }
                }
                string Tag = dt.Tables[0].Rows[0]["Tag"].ToString();

                if (Tag.ToString() != null)
                {
                    if (Tag.ToString() == "True")
                    {
                        CheckBox51.Checked = true;
                    }
                }

                string Gade = dt.Tables[0].Rows[0]["Gade"].ToString();

                if (Gade.ToString() != null)
                {
                    if (Gade.ToString() == "True")
                    {
                        CheckBox52.Checked = true;
                    }
                }
                string Gård = dt.Tables[0].Rows[0]["Gård"].ToString();

                if (Gård.ToString() != null)
                {
                    if (Gård.ToString() == "True")
                    {
                        CheckBox53.Checked = true;
                    }
                }
                string Stillads_Andet = dt.Tables[0].Rows[0]["Stillads_Andet"].ToString();

                if (Stillads_Andet.ToString() != null)
                {
                    if (Stillads_Andet.ToString() == "True")
                    {
                        CheckBox54.Checked = true;
                    }
                }

                string Eight = dt.Tables[0].Rows[0]["Eight"].ToString();

                if (Eight.ToString() != null)
                {
                    if (Eight.ToString() == "True")
                    {
                        CheckBox55.Checked = true;
                    }
                }
                string Haki_ram = dt.Tables[0].Rows[0]["Haki ram"].ToString();

                if (Haki_ram.ToString() != null)
                {
                    if (Haki_ram.ToString() == "True")
                    {
                        CheckBox56.Checked = true;
                    }
                }
                string Haki_mur = dt.Tables[0].Rows[0]["Haki mur"].ToString();

                if (Haki_mur.ToString() != null)
                {
                    if (Haki_mur.ToString() == "True")
                    {
                        CheckBox57.Checked = true;
                    }

                }

                string Kl_3_200Kg_m2 = dt.Tables[0].Rows[0]["Kl.3-200Kg/m2"].ToString();

                if (Kl_3_200Kg_m2.ToString() != null)
                {
                    if (Kl_3_200Kg_m2.ToString() == "True")
                    {
                        CheckBox58.Checked = true;
                    }
                }

                string Kl_5_450Kg_m2 = dt.Tables[0].Rows[0]["Kl.5-450 Kg/m2"].ToString();

                if (Kl_5_450Kg_m2.ToString() != null)
                {
                    if (Kl_5_450Kg_m2.ToString() == "True")
                    {
                        CheckBox59.Checked = true;
                    }
                }

                string Lastklasse = dt.Tables[0].Rows[0]["Lastklasse:"].ToString();

                if (Lastklasse.ToString() != null)
                {
                    if (Lastklasse.ToString() == "True")
                    {
                        CheckBox60.Checked = true;
                    }
                }


                string Mur = dt.Tables[0].Rows[0]["Mur"].ToString();

                if (Mur.ToString() != null)
                {
                    if (Mur.ToString() == "True")
                    {
                        CheckBox61.Checked = true;
                    }
                }
                string Træ = dt.Tables[0].Rows[0]["Træ"].ToString();

                if (Træ.ToString() != null)
                {
                    if (Træ.ToString() == "True")
                    {
                        CheckBox62.Checked = true;
                    }
                }
                string Beton = dt.Tables[0].Rows[0]["Beton"].ToString();

                if (Beton.ToString() != null)
                {
                    if (Beton.ToString() == "True")
                    {
                        CheckBox63.Checked = true;
                    }
                }
                string Fastgørelser_Andet = dt.Tables[0].Rows[0]["Fastgørelser_Andet"].ToString();

                if (Fastgørelser_Andet.ToString() != null)
                {
                    if (Fastgørelser_Andet.ToString() == "True")
                    {
                        CheckBox64.Checked = true;
                    }
                }


                string Byggestrøm = dt.Tables[0].Rows[0]["Byggestrøm"].ToString();

                if (Byggestrøm.ToString() != null)
                {
                    if (Byggestrøm.ToString() == "True")
                    {
                        CheckBox65.Checked = true;
                    }
                }
                string Generator = dt.Tables[0].Rows[0]["Generator"].ToString();

                if (Generator.ToString() != null)
                {
                    if (Generator.ToString() == "True")
                    {
                        CheckBox66.Checked = true;
                    }
                }
                string Strømforsyning_Andet = dt.Tables[0].Rows[0]["Strømforsyning_Andet"].ToString();

                if (Strømforsyning_Andet.ToString() != null)
                {
                    if (Strømforsyning_Andet.ToString() == "True")
                    {
                        CheckBox67.Checked = true;
                    }
                }


                string Lastbil = dt.Tables[0].Rows[0]["Lastbil"].ToString();

                if (Lastbil.ToString() != null)
                {
                    if (Lastbil.ToString() == "True")
                    {
                        CheckBox68.Checked = true;
                    }
                }
                string Lille_bil = dt.Tables[0].Rows[0]["Lille bil"].ToString();

                if (Lille_bil.ToString() != null)
                {
                    if (Lille_bil.ToString() == "True")
                    {
                        CheckBox69.Checked = true;
                    }
                }
                string Truck = dt.Tables[0].Rows[0]["Truck"].ToString();

                if (Truck.ToString() != null)
                {
                    if (Truck.ToString() == "True")
                    {
                        CheckBox70.Checked = true;
                    }
                }
                string Trækvogn = dt.Tables[0].Rows[0]["Trækvogn"].ToString();

                if (Trækvogn.ToString() != null)
                {
                    if (Trækvogn.ToString() == "True")
                    {
                        CheckBox71.Checked = true;
                    }
                }
                string Alurullevogne = dt.Tables[0].Rows[0]["Alurullevogne"].ToString();

                if (Alurullevogne.ToString() != null)
                {
                    if (Alurullevogne.ToString() == "True")
                    {
                        CheckBox72.Checked = true;
                    }
                }
                string Transport_Andet = dt.Tables[0].Rows[0]["Transport_Andet"].ToString();

                if (Transport_Andet.ToString() != null)
                {
                    if (Transport_Andet.ToString() == "True")
                    {
                        CheckBox73.Checked = true;
                    }
                }

                string Sele_line = dt.Tables[0].Rows[0]["Sele/line"].ToString();

                if (Sele_line.ToString() != null)
                {
                    if (Sele_line.ToString() == "True")
                    {
                        CheckBox74.Checked = true;
                    }
                }
                string Maske = dt.Tables[0].Rows[0]["Maske"].ToString();

                if (Maske.ToString() != null)
                {
                    if (Maske.ToString() == "True")
                    {
                        CheckBox75.Checked = true;
                    }
                }
                string Høreværn = dt.Tables[0].Rows[0]["Høreværn"].ToString();

                if (Høreværn.ToString() != null)
                {
                    if (Høreværn.ToString() == "True")
                    {
                        CheckBox76.Checked = true;
                    }
                }
                string Briller = dt.Tables[0].Rows[0]["Briller"].ToString();

                if (Briller.ToString() != null)
                {
                    if (Briller.ToString() == "True")
                    {
                        CheckBox77.Checked = true;
                    }
                }
                string værnemidler_Andet = dt.Tables[0].Rows[0]["værnemidler_Andet"].ToString();

                if (værnemidler_Andet.ToString() != null)
                {
                    if (værnemidler_Andet.ToString() == "True")
                    {
                        CheckBox78.Checked = true;
                    }
                }
                string Deleskur = dt.Tables[0].Rows[0]["Deleskur"].ToString();

                if (Deleskur.ToString() != null)
                {
                    if (Deleskur.ToString() == "True")
                    {
                        CheckBox79.Checked = true;
                    }
                }
                string Mangler = dt.Tables[0].Rows[0]["Mangler"].ToString();

                if (Mangler.ToString() != null)
                {
                    if (Mangler.ToString() == "True")
                    {
                        CheckBox80.Checked = true;
                    }
                }
                string Adgang_til_toilet = dt.Tables[0].Rows[0]["Adgang til toilet"].ToString();

                if (Adgang_til_toilet.ToString() != null)
                {
                    if (Adgang_til_toilet.ToString() == "True")
                    {
                        CheckBox81.Checked = true;
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);

        }

        protected void BtnUpdateAPV_Click(object sender, EventArgs e)
        {
            UpdateAPV();
            ClearCheckBoxList();
        }

        protected void UpdateAPV()
        {
            if (string.IsNullOrEmpty(txtsag.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Fill Sag Field');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                SqlCommand com = new SqlCommand();
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_Update_APV";
                com.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
                com.Parameters.Add("@I_APVID", System.Data.SqlDbType.BigInt).Value = lblSimpleAPVID.Text;
                com.Parameters.Add("@SAG", System.Data.SqlDbType.NVarChar).Value = txtsag.Text;
                com.Parameters.Add("@KUNDE", System.Data.SqlDbType.NVarChar).Value = txtKunde.Text;
                com.Parameters.Add("@KONTAKT", System.Data.SqlDbType.Decimal).Value = txtKontakt.Text;
                com.Parameters.Add("@TELEFON", System.Data.SqlDbType.Decimal).Value = txtTelefon.Text;
                com.Parameters.Add("@COMPLATIONDATE", System.Data.SqlDbType.Date).Value = DateTime.Now;

                //for (int i = 0; i < ChkLstStillads1.Items.Count - 1; i++)
                // {
                if (CheckBox49.Checked)
                {
                    //string var = string.Empty;
                    //var += ChkLstStillads1.Items[0].Text.ToString();
                    com.Parameters.Add("@FACADE", System.Data.SqlDbType.Bit).Value = 1;
                    //com.Parameters.AddWithValue("@FACADE", var);
                }
                else
                {
                    com.Parameters.Add("@FACADE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox50.Checked)
                {
                    com.Parameters.Add("@VINDUER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@VINDUER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox51.Checked)
                {
                    com.Parameters.Add("@TAG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TAG", System.Data.SqlDbType.Bit).Value = 0;
                }
                // }

                // for (int i = 0; i < ChkLstStillads2.Items.Count - 1; i++)
                // {
                if (CheckBox52.Checked)
                {
                    com.Parameters.Add("@GADE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GADE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox53.Checked)
                {
                    com.Parameters.Add("@GÅRD", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GÅRD", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox54.Checked)
                {
                    com.Parameters.Add("@STILLADS_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@STILLADS_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < ChklstStilladssystem.Items.Count - 1; i++)
                //{
                if (CheckBox55.Checked)
                {
                    com.Parameters.Add("@EIGHT", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@EIGHT", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox56.Checked)
                {
                    com.Parameters.Add("@HAKI_RAM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HAKI_RAM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox57.Checked)
                {
                    com.Parameters.Add("@HAKI_MUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HAKI_MUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < ChklstLastklasse.Items.Count - 1; i++)
                //{
                if (CheckBox58.Checked)
                {
                    com.Parameters.Add("@KI_3_200KG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@KI_3_200KG", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox59.Checked)
                {
                    com.Parameters.Add("@KI_5_450KG", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@KI_5_450KG", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox60.Checked)
                {
                    string var = string.Empty;
                    //var += ChklstLastklasse.Items[2].Text.ToString();
                    var += CheckBox60.Checked.ToString();
                    com.Parameters.AddWithValue("@LASTKLASSE", var);
                    //com.Parameters.Add("@LASTKLASSE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LASTKLASSE", System.Data.SqlDbType.NVarChar).Value = "";
                }
                //}

                //for (int i = 0; i < ChklstFastgørelser.Items.Count - 1; i++)
                //{
                if (CheckBox61.Checked)
                {
                    com.Parameters.Add("@MUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox62.Checked)
                {
                    com.Parameters.Add("@TRÆ", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRÆ", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox63.Checked)
                {
                    com.Parameters.Add("@BETON", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BETON", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox64.Checked)
                {
                    com.Parameters.Add("@FASTGØRELSER_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@FASTGØRELSER_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < ChklstStrømforsyning.Items.Count - 1; i++)
                //{
                if (CheckBox65.Checked)
                {
                    com.Parameters.Add("@BYGGESTRØM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BYGGESTRØM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox66.Checked)
                {
                    com.Parameters.Add("@GENERATOR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@GENERATOR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox67.Checked)
                {
                    com.Parameters.Add("@STRØMFORSYNING_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@STRØMFORSYNING_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < ChklstTran_materiel.Items.Count - 1; i++)
                //{
                if (CheckBox68.Checked)
                {
                    com.Parameters.Add("@LASTBIL", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LASTBIL", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox69.Checked)
                {
                    com.Parameters.Add("@LILLE_BIL", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@LILLE_BIL", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox70.Checked)
                {
                    com.Parameters.Add("@TRUCK", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRUCK", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox71.Checked)
                {
                    com.Parameters.Add("@TRÆKVOGN", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRÆKVOGN", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox72.Checked)
                {
                    com.Parameters.Add("@ALURULLEVOGNE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@ALURULLEVOGNE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox73.Checked)
                {
                    com.Parameters.Add("@TRANSPORT_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@TRANSPORT_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}


                //for (int i = 0; i < Chklst_værnemidler.Items.Count - 1; i++)
                //{
                if (CheckBox74.Checked)
                {
                    com.Parameters.Add("@SELE_LINE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@SELE_LINE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox75.Checked)
                {
                    com.Parameters.Add("@MASKE", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MASKE", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox76.Checked)
                {
                    com.Parameters.Add("@HØREVÆRM", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@HØREVÆRM", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox77.Checked)
                {
                    com.Parameters.Add("@BRILLER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@BRILLER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox78.Checked)
                {
                    com.Parameters.Add("@VÆRNEMIDLER_ANDET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@VÆRNEMIDLER_ANDET", System.Data.SqlDbType.Bit).Value = 0;
                }
                //}

                //for (int i = 0; i < Chklst_Velfærdsforanstaltninger.Items.Count - 1; i++)
                //{
                if (CheckBox79.Checked)
                {
                    com.Parameters.Add("@DELESKUR", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@DELESKUR", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox80.Checked)
                {
                    com.Parameters.Add("@MANGLER", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@MANGLER", System.Data.SqlDbType.Bit).Value = 0;
                }
                if (CheckBox81.Checked)
                {
                    com.Parameters.Add("@ADGANG_TIL_TOILET", System.Data.SqlDbType.Bit).Value = 1;
                }
                else
                {
                    com.Parameters.Add("@ADGANG_TIL_TOILET", System.Data.SqlDbType.Bit).Value = 0;
                }
                // }

                com.Parameters.Add("@NOTE", System.Data.SqlDbType.NVarChar).Value = "";

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con.Close();
            }

        }

        protected void LbtnDeleteAPV_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string APVID = grdrow.Cells[1].Text;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("Delete  From APV where APVID = @I_APVID AND CID= @CID ", con);

            cmd.Parameters.Add("@I_APVID", System.Data.SqlDbType.BigInt).Value = APVID;
            cmd.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;

            cmd.ExecuteNonQuery();
            con.Close();
            BindAPV();

            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('APV Deleted SUccessfully!!');", true);
            return;
        }

        protected void LBtnDeleteAktiveAPV_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string ActiveAPVID = grdrow.Cells[1].Text;


            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("Delete  From AktiveAPV where AktiveAPVID = @I_AktiveAPVID AND CID= @CID ", con);

            cmd.Parameters.Add("@I_AktiveAPVID", System.Data.SqlDbType.BigInt).Value = ActiveAPVID;
            cmd.Parameters.Add("@CID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;

            cmd.ExecuteNonQuery();
            con.Close();
            BindAktiveAPV();

            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('AktiveAPV Deleted SUccessfully!!');", true);
            return;
        }

        protected void BtnPrint_Click(object sender, EventArgs e)
        {
            //lbltxtsag.Text = txtsag.Text;
            //lbltxtKunde.Text = txtKunde.Text;
            //lbltxtKontakt.Text = txtKontakt.Text;
            //lbltxtTelefon.Text = txtTelefon.Text;

            //lbltxtsag.Visible = true;
            //lbltxtKunde.Visible = true;
            //lbltxtKontakt.Visible = true;
            //lbltxtTelefon.Visible = true;

            //txtsag.Visible = false;
            //txtKunde.Visible = false;
            //txtKontakt.Visible = false;
            //txtTelefon.Visible = false;
            //txtSjak.Visible = false;

            //LabeldivAPV12.Text = TextBoxdivAPV12.Text;
            //LabeldivAPV13.Text = TextBoxdivAPV13.Text;
            //LabeldivAPV14.Text = TextBoxdivAPV14.Text;
            //LabeldivAPV15.Text = TextBoxdivAPV15.Text;

            //LabeldivAPV12.Visible = true;
            //LabeldivAPV13.Visible = true;
            //LabeldivAPV14.Visible = true;
            //LabeldivAPV15.Visible = true;

            //TextBoxdivAPV12.Visible = false;
            //TextBoxdivAPV13.Visible = false;
            //TextBoxdivAPV14.Visible = false;
            //TextBoxdivAPV15.Visible = false;

            string script = "<script>PrintGridDataAll();</script>";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), script, false);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void btnPrintAktiveAPV_Click(object sender, EventArgs e)
        {
            //lblsagsnavn.Text = txtsagsnavn.Text;
            //lbldeltagere.Text = txtdeltagere.Text;

            //lbldeltagere.Visible = true;
            //lblsagsnavn.Visible = true;

            //txtdeltagere.Visible = false;
            //txtsagsnavn.Visible = false;

            //LabelPunkt_nr.Text = TextBoxPunkt_nr.Text;
            //LabelBeskriv_problemet.Text = TextBoxBeskriv_problemet.Text;
            //LabelBeskriv_den.Text = TextBoxBeskriv_den.Text;
            //LabelIværksættes.Text = TextBoxIværksættes.Text;
            //LabelPrioriteret.Text = TextBoxPrioriteret.Text;
            //LabelEvaluering.Text = TextBoxEvaluering.Text;

            //TextBoxPunkt_nr.Visible = false;
            //TextBoxBeskriv_problemet.Visible = false;
            //TextBoxBeskriv_den.Visible = false;
            //TextBoxIværksættes.Visible = false;
            //TextBoxPrioriteret.Visible = false;
            //TextBoxEvaluering.Visible = false;


            string script = "<script>PrintGridDataAll1();</script>";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), script, false);

            string script1 = "<script>PrintGridDataAll11();</script>";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), script1, false);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
        }

        protected void BtnPDFAPV_Click(object sender, EventArgs e)
        {
            PdfPTable table = new PdfPTable(1);

            table.TotalWidth = 515f;
            table.LockedWidth = true;

            table.HorizontalAlignment = 0;
            table.SpacingBefore = 0f;

            table.SpacingAfter = 0f;
            PdfPCell cell = new PdfPCell();
            cell.FixedHeight = 25f;

            cell.Colspan = 1;

            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right

            table.AddCell(cell);
            //table.AddCell(" ");

            Prepare2(divAPV11);
            Prepare2(divAPV12);
            Prepare2(divAPV13);
            Prepare2(divAPV14);
            Prepare2(divAPV15);

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.Parameters.Clear();

            lbltxtsag.Text = txtsag.Text;
            lbltxtKunde.Text = txtKunde.Text;
            lbltxtKontakt.Text = txtKontakt.Text;
            lbltxtTelefon.Text = txtTelefon.Text;

            LabeldivAPV12.Text = TextBoxdivAPV12.Text;
            LabeldivAPV13.Text = TextBoxdivAPV13.Text;
            LabeldivAPV14.Text = TextBoxdivAPV14.Text;
            LabeldivAPV15.Text = TextBoxdivAPV15.Text;

            lbltxtsag.Visible = true;
            lbltxtKunde.Visible = true;
            lbltxtKontakt.Visible = true;
            lbltxtTelefon.Visible = true;

            imguser.Visible = false;

            txtsag.Visible = false;
            txtKunde.Visible = false;
            txtKontakt.Visible = false;
            txtTelefon.Visible = false;

            DisableAjaxExtenders(this);

            //string strpath = Request.PhysicalApplicationPath + "APV" + LAPVID.Text + ".pdf";
            string path = Server.MapPath("~/DocFile/");

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "APV.pdf"));
            Response.Charset = "";
            Response.ContentType = "application/pdf";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            EnableViewState = false;

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            StringWriter sw2 = new StringWriter();
            HtmlTextWriter hw2 = new HtmlTextWriter(sw2);
            StringWriter sw3 = new StringWriter();
            HtmlTextWriter hw3 = new HtmlTextWriter(sw3);
            StringWriter sw4 = new StringWriter();
            HtmlTextWriter hw4 = new HtmlTextWriter(sw4);

            //System.Web.UI.HtmlControls.HtmlForm frm = new System.Web.UI.HtmlControls.HtmlForm();
            //divAPV1.Parent.Controls.Add(frm);
            ////div01.RenderControl(hw);
            //frm.Attributes["runat"] = "server";
            //frm.Controls.Add(divAPV1);
            //frm.RenderControl(hw);

            divAPV11.RenderControl(hw);
            divAPV12.RenderControl(hw1);
            divAPV13.RenderControl(hw2);
            divAPV14.RenderControl(hw3);
            divAPV15.RenderControl(hw4);

            StringReader sr = new StringReader(sw.ToString());
            StringReader sr1 = new StringReader(sw1.ToString());
            StringReader sr2 = new StringReader(sw2.ToString());
            StringReader sr3 = new StringReader(sw3.ToString());
            StringReader sr4 = new StringReader(sw4.ToString());

            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 40f, 40f, 10f, 50f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

            FileStream file = new FileStream(path + "APV" + LAPVID.Text + ".pdf", FileMode.Create);
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            PdfWriter writer1 = PdfWriter.GetInstance(pdfDoc, file);

            writer.PageEvent = new ITextEvent1();
            writer1.PageEvent = new ITextEvent1();

            // iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(imagepath + "/" + imagename.ToString());
            // gif.ScaleAbsolute(300f, 80f);
            // writer1.PageEvent = new itextEvent1();
            //gif.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "delete from DocData where ApvId=" + LAPVID.Text;
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "insert into DocData (Cid,ApvId,AktiveAPVID,FileNamepdf,FilePathPDF) values(@CID,@APVID,@AktiveAPVID,@NAME,@PATH)";
            com.Parameters.AddWithValue("@CID", LblCustId.Text);
            com.Parameters.AddWithValue("@APVID", LAPVID.Text);
            com.Parameters.AddWithValue("@AktiveAPVID", 0);
            com.Parameters.AddWithValue("@NAME", System.Data.SqlDbType.NVarChar).Value = "APV" + LAPVID.Text + ".pdf";
            com.Parameters.AddWithValue("@PATH", System.Data.SqlDbType.NVarChar).Value = (path + "APV" + LAPVID.Text + ".pdf");
            com.ExecuteNonQuery();
            con.Close();

            pdfDoc.Open();
            htmlparser.Parse(sr);
            if (LabeldivAPV12.Text == "")
            {
                pdfDoc.Add(table);
            }
            else
            {
                htmlparser.Parse(sr1);
            }
            if (LabeldivAPV13.Text == "")
            {
                pdfDoc.Add(table);
            }
            else
            {
                htmlparser.Parse(sr2);
            }
            if (LabeldivAPV14.Text == "")
            {
                pdfDoc.Add(table);
            }
            else
            {
                htmlparser.Parse(sr3);
            }
            if (LabeldivAPV15.Text == "")
            {
                pdfDoc.Add(table);
            }
            else
            {
                htmlparser.Parse(sr4);
            }

            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

        }

        public void Prepare2(Control control)
        {
            string path;
            path = Server.MapPath("~/image/");
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];

                if (current is System.Web.UI.WebControls.CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as System.Web.UI.WebControls.CheckBox).Checked ? "<img  src='" + path + "checked.png' alt=''width='15' height='15' float='right' />" : "<img border='10' src='" + path + "unchecked.png' alt=''width='15' height='15' float='right' />"));
                    //current.Visible = true;
                }
            }
        }

        public class ITextEvent : PdfPageEventHelper
        {
            // This is the contentbyte object of the writer
            PdfContentByte cb;

            // we will put the final number of pages in a template
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer
            BaseFont bf = null;

            // This keeps track of the creation time
            DateTime PrintTime = DateTime.Now;


            #region Fields
            private string _header;
            #endregion

            #region Properties
            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }
            #endregion

            public override void OnOpenDocument(PdfWriter writer, iTextSharp.text.Document document)
            {
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                    //handle exception here
                }
                catch (System.IO.IOException ioe)
                {
                    //handle exception here
                }
            }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnEndPage(writer, document);

                //PdfContentByte content = writer.DirectContent;
                //Rectangle rectangle = new Rectangle(document.PageSize);
                //rectangle.Left += document.LeftMargin;
                //rectangle.Right -= document.RightMargin;
                //rectangle.Top -= 10;
                ////rectangle.Bottom += document.BottomMargin;
                //rectangle.Bottom += 10;
                //content.SetColorStroke(BaseColor.BLACK);
                //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                //content.Stroke();

                iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

                //Phrase p1Header = new Phrase("QUOTATION", baseFontNormal);

                //Create PdfTable object for header
                PdfPTable pdfTab = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings
                //Row 1
                iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/image/pmglogo1.jpg"));
                logo.ScaleAbsolute(180f, 80f);

                PdfPCell pdfCell1 = new PdfPCell();
                PdfPCell pdfCell2 = new PdfPCell();
                PdfPCell pdfCell3 = new PdfPCell(logo);
                String text = "Page " + writer.PageNumber + " of ";

                //Add paging to header
                //{
                //    cb.BeginText();
                //    cb.SetFontAndSize(bf, 12);
                //    cb.SetTextMatrix(document.PageSize.GetRight(155), document.PageSize.GetTop(45));
                //    // cb.ShowText(text);
                //    cb.EndText();
                //    //float len = bf.GetWidthPoint(text, 12);
                //    //Adds "12" in Page 1 of 12
                //    //cb.AddTemplate(headerTemplate, document.PageSize.GetRight(155), document.PageSize.GetTop(45));
                //}
                //Add paging to footer
                {
                    cb.BeginText();
                    cb.SetFontAndSize(bf, 12);
                    cb.SetTextMatrix(document.PageSize.GetRight(105), document.PageSize.GetBottom(30));
                    cb.ShowText(text);
                    cb.EndText();
                    float len = bf.GetWidthPoint(text, 12);
                    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(105) + len, document.PageSize.GetBottom(30));
                }
                //Row 2
                //PdfPCell pdfCell4 = new PdfPCell(new Phrase("PMG Stilladser A/S", baseFontNormal));
                ////Row 3

                ////PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                //PdfPCell pdfCell5 = new PdfPCell();
                //PdfPCell pdfCell6 = new PdfPCell();
                //PdfPCell pdfCell7 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                ////Row 4
                //PdfPCell pdfCell8 = new PdfPCell();
                //PdfPCell pdfCell9 = new PdfPCell();
                //PdfPCell pdfCell10 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

                ////set the alignment of all three cells and set border to 0
                pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell3.HorizontalAlignment = Element.ALIGN_LEFT;
                //pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell7.HorizontalAlignment = Element.ALIGN_LEFT;
                //pdfCell8.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell9.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell10.HorizontalAlignment = Element.ALIGN_LEFT;

                pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell3.VerticalAlignment = Element.ALIGN_LEFT;
                //pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
                //pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
                //pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
                //pdfCell7.VerticalAlignment = Element.ALIGN_LEFT;
                //pdfCell8.VerticalAlignment = Element.ALIGN_MIDDLE;
                //pdfCell9.VerticalAlignment = Element.ALIGN_MIDDLE;
                //pdfCell10.VerticalAlignment = Element.ALIGN_LEFT;

                //pdfCell4.Colspan = 3;
                //// pdfCell10.Colspan = 3;

                pdfCell1.Border = 0;
                pdfCell2.Border = 0;
                pdfCell3.Border = 0;
                //pdfCell4.Border = 0;
                //pdfCell5.Border = 0;
                //pdfCell6.Border = 0;
                //pdfCell7.Border = 0;
                //pdfCell8.Border = 0;
                //pdfCell9.Border = 0;
                //pdfCell10.Border = 0;

                ////add all three cells into PdfTable
                pdfTab.AddCell(pdfCell1);
                pdfTab.AddCell(pdfCell2);
                pdfTab.AddCell(pdfCell3);
                //pdfTab.AddCell(pdfCell4);
                //pdfTab.AddCell(pdfCell5);
                //pdfTab.AddCell(pdfCell6);
                //pdfTab.AddCell(pdfCell7);
                //pdfTab.AddCell(pdfCell8);
                //pdfTab.AddCell(pdfCell9);
                //pdfTab.AddCell(pdfCell10);

                pdfTab.TotalWidth = document.PageSize.Width - 80f;
                pdfTab.WidthPercentage = 70;

                ////call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                ////first param is start row. -1 indicates there is no end row and all the rows to be included to write
                ////Third and fourth param is x and y position to start writing
                pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                //Move the pointer and draw line to separate header section from rest of page
                //cb.MoveTo(40, document.PageSize.Height - 100);
                //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                //cb.Stroke();

                //Create PdfTable object for footer
                PdfPTable pdfTab1 = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings
                //Row 1
                PdfPCell pdfCell11 = new PdfPCell();
                PdfPCell pdfCell12 = new PdfPCell(new Phrase("PMG Stilladser A/S, Hundigevej 87, 2670 Greve", baseFontBig));
                PdfPCell pdfCell13 = new PdfPCell();

                //Row 2
                PdfPCell pdfCell14 = new PdfPCell(new Phrase("Internet:pmg.dk  Email:info@pmg.dk", baseFontBig));
                //Row 3

                //PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                PdfPCell pdfCell15 = new PdfPCell();
                PdfPCell pdfCell16 = new PdfPCell();
                PdfPCell pdfCell17 = new PdfPCell();

                //set the alignment of all three cells and set border to 0
                pdfCell11.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell12.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell13.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell14.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell15.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell16.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell17.HorizontalAlignment = Element.ALIGN_CENTER;

                pdfCell12.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell13.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell14.VerticalAlignment = Element.ALIGN_TOP;
                pdfCell15.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell16.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell17.VerticalAlignment = Element.ALIGN_MIDDLE;

                pdfCell14.Colspan = 3;
                // pdfCell10.Colspan = 3;

                pdfCell11.Border = 0;
                pdfCell12.Border = 0;
                pdfCell13.Border = 0;
                pdfCell14.Border = 0;
                pdfCell15.Border = 0;
                pdfCell16.Border = 0;
                pdfCell17.Border = 0;

                //add all three cells into PdfTable
                pdfTab1.AddCell(pdfCell11);
                pdfTab1.AddCell(pdfCell12);
                pdfTab1.AddCell(pdfCell13);
                pdfTab1.AddCell(pdfCell14);
                pdfTab1.AddCell(pdfCell15);
                pdfTab1.AddCell(pdfCell16);
                pdfTab1.AddCell(pdfCell17);

                pdfTab1.TotalWidth = document.PageSize.Width - 80f;
                pdfTab1.WidthPercentage = 70;

                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                //Third and fourth param is x and y position to start writing
                pdfTab1.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 780, writer.DirectContent);


                //Move the pointer and draw line to separate footer section from rest of page
                cb.MoveTo(40, document.PageSize.GetBottom(60));
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(60));
                cb.Stroke();
            }

            public override void OnCloseDocument(PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

                footerTemplate.BeginText();
                footerTemplate.SetFontAndSize(bf, 12);
                footerTemplate.SetTextMatrix(0, 0);
                footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                footerTemplate.EndText();
            }

            public PdfPCell logo { get; set; }
        }

        public class ITextEvent1 : PdfPageEventHelper
        {
            // This is the contentbyte object of the writer
            PdfContentByte cb;

            // we will put the final number of pages in a template
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer
            BaseFont bf = null;

            // This keeps track of the creation time
            DateTime PrintTime = DateTime.Now;


            #region Fields
            private string _header;
            #endregion

            #region Properties
            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }
            #endregion

            public override void OnOpenDocument(PdfWriter writer, iTextSharp.text.Document document)
            {
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                    //handle exception here
                }
                catch (System.IO.IOException ioe)
                {
                    //handle exception here
                }
            }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnEndPage(writer, document);

                //PdfContentByte content = writer.DirectContent;
                //Rectangle rectangle = new Rectangle(document.PageSize);
                //rectangle.Left += document.LeftMargin;
                //rectangle.Right -= document.RightMargin;
                //rectangle.Top -= 10;
                ////rectangle.Bottom += document.BottomMargin;
                //rectangle.Bottom += 10;
                //content.SetColorStroke(BaseColor.BLACK);
                //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                //content.Stroke();

                iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

                //Phrase p1Header = new Phrase("QUOTATION", baseFontNormal);

                //Create PdfTable object for header
                PdfPTable pdfTab = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings
                //Row 1
                iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/image/pmglogo1.jpg"));
                logo.ScaleAbsolute(180f, 50f);

                PdfPCell pdfCell1 = new PdfPCell(logo);
                PdfPCell pdfCell2 = new PdfPCell();
                PdfPCell pdfCell3 = new PdfPCell();
                String text = "Page " + writer.PageNumber + " of ";

                //Add paging to header
                //{
                //    cb.BeginText();
                //    cb.SetFontAndSize(bf, 12);
                //    cb.SetTextMatrix(document.PageSize.GetRight(155), document.PageSize.GetTop(45));
                //    // cb.ShowText(text);
                //    cb.EndText();
                //    //float len = bf.GetWidthPoint(text, 12);
                //    //Adds "12" in Page 1 of 12
                //    //cb.AddTemplate(headerTemplate, document.PageSize.GetRight(155), document.PageSize.GetTop(45));
                //}
                //Add paging to footer
                {
                    cb.BeginText();
                    cb.SetFontAndSize(bf, 12);
                    cb.SetTextMatrix(document.PageSize.GetRight(105), document.PageSize.GetBottom(30));
                    cb.ShowText(text);
                    cb.EndText();
                    float len = bf.GetWidthPoint(text, 12);
                    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(105) + len, document.PageSize.GetBottom(30));
                }
                //Row 2
                //PdfPCell pdfCell4 = new PdfPCell(new Phrase("PMG Stilladser A/S", baseFontNormal));
                ////Row 3

                ////PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                //PdfPCell pdfCell5 = new PdfPCell();
                //PdfPCell pdfCell6 = new PdfPCell();
                //PdfPCell pdfCell7 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                ////Row 4
                //PdfPCell pdfCell8 = new PdfPCell();
                //PdfPCell pdfCell9 = new PdfPCell();
                //PdfPCell pdfCell10 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

                ////set the alignment of all three cells and set border to 0
                pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell3.HorizontalAlignment = Element.ALIGN_LEFT;
                //pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell7.HorizontalAlignment = Element.ALIGN_LEFT;
                //pdfCell8.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell9.HorizontalAlignment = Element.ALIGN_CENTER;
                //pdfCell10.HorizontalAlignment = Element.ALIGN_LEFT;

                pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell3.VerticalAlignment = Element.ALIGN_LEFT;
                //pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
                //pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
                //pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
                //pdfCell7.VerticalAlignment = Element.ALIGN_LEFT;
                //pdfCell8.VerticalAlignment = Element.ALIGN_MIDDLE;
                //pdfCell9.VerticalAlignment = Element.ALIGN_MIDDLE;
                //pdfCell10.VerticalAlignment = Element.ALIGN_LEFT;

                //pdfCell4.Colspan = 3;
                //// pdfCell10.Colspan = 3;

                pdfCell1.Border = 0;
                pdfCell2.Border = 0;
                pdfCell3.Border = 0;
                //pdfCell4.Border = 0;
                //pdfCell5.Border = 0;
                //pdfCell6.Border = 0;
                //pdfCell7.Border = 0;
                //pdfCell8.Border = 0;
                //pdfCell9.Border = 0;
                //pdfCell10.Border = 0;

                ////add all three cells into PdfTable
                pdfTab.AddCell(pdfCell1);
                pdfTab.AddCell(pdfCell2);
                pdfTab.AddCell(pdfCell3);
                //pdfTab.AddCell(pdfCell4);
                //pdfTab.AddCell(pdfCell5);
                //pdfTab.AddCell(pdfCell6);
                //pdfTab.AddCell(pdfCell7);
                //pdfTab.AddCell(pdfCell8);
                //pdfTab.AddCell(pdfCell9);
                //pdfTab.AddCell(pdfCell10);

                pdfTab.TotalWidth = document.PageSize.Width - 80f;
                pdfTab.WidthPercentage = 70;

                ////call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                ////first param is start row. -1 indicates there is no end row and all the rows to be included to write
                ////Third and fourth param is x and y position to start writing
                pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                //Move the pointer and draw line to separate header section from rest of page
                //cb.MoveTo(40, document.PageSize.Height - 100);
                //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                //cb.Stroke();

                //Create PdfTable object for footer
                PdfPTable pdfTab1 = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings
                //Row 1
                PdfPCell pdfCell11 = new PdfPCell();
                PdfPCell pdfCell12 = new PdfPCell(new Phrase("PMG Stilladser A/S, Hundigevej 87, 2670 Greve", baseFontBig));
                PdfPCell pdfCell13 = new PdfPCell();

                //Row 2
                PdfPCell pdfCell14 = new PdfPCell(new Phrase("Internet:pmg.dk  Email:info@pmg.dk", baseFontBig));
                //Row 3

                //PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                PdfPCell pdfCell15 = new PdfPCell();
                PdfPCell pdfCell16 = new PdfPCell();
                PdfPCell pdfCell17 = new PdfPCell();

                //set the alignment of all three cells and set border to 0
                pdfCell11.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell12.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell13.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell14.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell15.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell16.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell17.HorizontalAlignment = Element.ALIGN_CENTER;

                pdfCell12.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell13.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell14.VerticalAlignment = Element.ALIGN_TOP;
                pdfCell15.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell16.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell17.VerticalAlignment = Element.ALIGN_MIDDLE;

                pdfCell14.Colspan = 3;
                // pdfCell10.Colspan = 3;

                pdfCell11.Border = 0;
                pdfCell12.Border = 0;
                pdfCell13.Border = 0;
                pdfCell14.Border = 0;
                pdfCell15.Border = 0;
                pdfCell16.Border = 0;
                pdfCell17.Border = 0;

                //add all three cells into PdfTable
                pdfTab1.AddCell(pdfCell11);
                pdfTab1.AddCell(pdfCell12);
                pdfTab1.AddCell(pdfCell13);
                pdfTab1.AddCell(pdfCell14);
                pdfTab1.AddCell(pdfCell15);
                pdfTab1.AddCell(pdfCell16);
                pdfTab1.AddCell(pdfCell17);

                pdfTab1.TotalWidth = document.PageSize.Width - 80f;
                pdfTab1.WidthPercentage = 70;

                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                //Third and fourth param is x and y position to start writing
                pdfTab1.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 780, writer.DirectContent);


                //Move the pointer and draw line to separate footer section from rest of page
                cb.MoveTo(40, document.PageSize.GetBottom(60));
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(60));
                cb.Stroke();
            }

            public override void OnCloseDocument(PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

                footerTemplate.BeginText();
                footerTemplate.SetFontAndSize(bf, 12);
                footerTemplate.SetTextMatrix(0, 0);
                footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                footerTemplate.EndText();
            }

            public PdfPCell logo { get; set; }
        }

        protected void BtnSaveAsHtml_Click(object sender, EventArgs e)
        {
            lblsagsnavn.Text = txtsagsnavn.Text;
            lbldeltagere.Text = txtdeltagere.Text;
            lbldeltagere.Visible = true;
            lblsagsnavn.Visible = true;
            txtdeltagere.Visible = false;
            txtsagsnavn.Visible = false;

            Response.Clear();
            Response.Buffer = true;
            EnableViewState = false;

            DisableCheckBox();

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "AktiveAPV.mhtml"));
            Response.Charset = "";
            Response.ContentType = "application/pdf";

            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            EnableViewState = false;


            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            DisableAjaxExtenders(this);

            kj.RenderControl(hw);
            DivHoriPDF.RenderControl(hw);

            //StringReader sr = new StringReader(sw.ToString());
            string strpath = Request.PhysicalApplicationPath + "APV" + LAPVID.Text + ".mhtml";//+ "PDFDATA" + "/"
            //string strpath = "C:\\manoj\\" + "APV" + LAPVID.Text + ".html";

            StreamWriter sw1 = new StreamWriter(strpath);
            sw1.Write(sw.ToString());
            sw1.Close();
            hw.Flush();

            //System.IO.FileStream fs = new System.IO.FileStream(@"C:\test.htm", System.IO.FileMode.Create);

            //FileStream file = new FileStream("/QuatationTemplate" + lblAktiveAPVID.Text + ".htm", FileMode.Create);

            //string s = sw.ToString();

            //byte[] b = System.Text.Encoding.UTF8.GetBytes(s);
            //fs.Write(b, 0, b.Length);
            //fs.Close();
            // Response.End();

            // System.IO.File.WriteAllText(@"C:\yoursite.htm", s);
            Response.Write(sw.ToString());
            Response.Flush();

            EnableCheckBox();
            //SimpleFileMove();

            Response.End();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);


        }

        public void SimpleFileMove()
        {
            string sourceFile = @"C:\manoj\ProjectManagement\ProjectManagement\APV.html";
            string destinationFile = @"C:\manoj\ProjectManagement\APV.html";

            // To move a file or folder to a new location:
            System.IO.File.Move(sourceFile, destinationFile);

            // To move an entire directory. To programmatically modify or combine
            // path strings, use the System.IO.Path class.
            // System.IO.Directory.Move(@"C:\manoj\", @"C:\manoj\ProjectManagement\");

        }

        public void DisableCheckBox()
        {
            //txtsagsnavn.Text = "";
            //txtdeltagere.Text = "";
            CheckBox1.Enabled = false;
            CheckBox2.Enabled = false;
            CheckBox3.Enabled = false;
            CheckBox4.Enabled = false;
            CheckBox5.Enabled = false;
            CheckBox6.Enabled = false;
            CheckBox7.Enabled = false;
            CheckBox8.Enabled = false;
            CheckBox9.Enabled = false;
            CheckBox10.Enabled = false;
            CheckBox11.Enabled = false;
            CheckBox12.Enabled = false;
            CheckBox13.Enabled = false;
            CheckBox14.Enabled = false;
            CheckBox15.Enabled = false;
            CheckBox16.Enabled = false;
            CheckBox17.Enabled = false;
            CheckBox18.Enabled = false;
            CheckBox19.Enabled = false;
            CheckBox20.Enabled = false;
            CheckBox21.Enabled = false;
            CheckBox22.Enabled = false;
            CheckBox23.Enabled = false;
            CheckBox24.Enabled = false;
            CheckBox25.Enabled = false;
            CheckBox26.Enabled = false;
            CheckBox27.Enabled = false;
            CheckBox28.Enabled = false;
            CheckBox29.Enabled = false;
            CheckBox30.Enabled = false;
            CheckBox31.Enabled = false;
            CheckBox32.Enabled = false;
            CheckBox33.Enabled = false;
            CheckBox34.Enabled = false;
            CheckBox35.Enabled = false;
            CheckBox36.Enabled = false;
            CheckBox37.Enabled = false;
            CheckBox38.Enabled = false;
            CheckBox39.Enabled = false;
            CheckBox40.Enabled = false;
            CheckBox41.Enabled = false;
            CheckBox42.Enabled = false;
            CheckBox43.Enabled = false;
            CheckBox44.Enabled = false;
            CheckBox45.Enabled = false;
            CheckBox46.Enabled = false;
            CheckBox47.Enabled = false;
            CheckBox48.Enabled = false;
        }

        public void EnableCheckBox()
        {
            //txtsagsnavn.Text = "";
            //txtdeltagere.Text = "";
            CheckBox1.Enabled = true;
            CheckBox2.Enabled = true;
            CheckBox3.Enabled = true;
            CheckBox4.Enabled = true;
            CheckBox5.Enabled = true;
            CheckBox6.Enabled = true;
            CheckBox7.Enabled = true;
            CheckBox8.Enabled = true;
            CheckBox9.Enabled = true;
            CheckBox10.Enabled = true;
            CheckBox11.Enabled = true;
            CheckBox12.Enabled = true;
            CheckBox13.Enabled = true;
            CheckBox14.Enabled = true;
            CheckBox15.Enabled = true;
            CheckBox16.Enabled = true;
            CheckBox17.Enabled = true;
            CheckBox18.Enabled = true;
            CheckBox19.Enabled = true;
            CheckBox20.Enabled = true;
            CheckBox21.Enabled = true;
            CheckBox22.Enabled = true;
            CheckBox23.Enabled = true;
            CheckBox24.Enabled = true;
            CheckBox25.Enabled = true;
            CheckBox26.Enabled = true;
            CheckBox27.Enabled = true;
            CheckBox28.Enabled = true;
            CheckBox29.Enabled = true;
            CheckBox30.Enabled = true;
            CheckBox31.Enabled = true;
            CheckBox32.Enabled = true;
            CheckBox33.Enabled = true;
            CheckBox34.Enabled = true;
            CheckBox35.Enabled = true;
            CheckBox36.Enabled = true;
            CheckBox37.Enabled = true;
            CheckBox38.Enabled = true;
            CheckBox39.Enabled = true;
            CheckBox40.Enabled = true;
            CheckBox41.Enabled = true;
            CheckBox42.Enabled = true;
            CheckBox43.Enabled = true;
            CheckBox44.Enabled = true;
            CheckBox45.Enabled = true;
            CheckBox46.Enabled = true;
            CheckBox47.Enabled = true;
            CheckBox48.Enabled = true;
        }

        protected void LBtnViewAktiveAPV_Click(object sender, EventArgs e)
        {
            string FilePath = null;

            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string AktiveID = grdrow.Cells[1].Text;

            lblAktiveAPVID.Text = AktiveID.ToString(); // FOr Updating Active APV Per Customer---

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select Distinct * from DocData where AktiveAPVID =@I_AktiveAPVID ", con);

            cmd.Parameters.Add("@I_AktiveAPVID", System.Data.SqlDbType.BigInt).Value = AktiveID;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                FilePath = dt.Tables[0].Rows[0]["FilePathPDF"].ToString();
                //string outputPath = Request.PhysicalApplicationPath + "DocFile" + "\\" + "AktiveAPV" + lblAktiveAPVID.Text + ".pdf";

                //OpenAktiveDoc(inputFile, outputPath);
                //OpenAktiveAPVPdf();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('PDF Not Created!!');", true);
                return;
            }
            try
            {
                //view document file 
                if (FilePath.Contains(".doc"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                //view pdf files 
                if (FilePath.Contains(".pdf"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                if (FilePath.Contains(".htm"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                if (FilePath.Contains(".txt"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                else
                {
                    //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('File not in Proper format');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('File not in Proper format');", true);
            }

        }

        protected void LBtViewAPV_Click(object sender, EventArgs e)
        {
            string FilePath = null;
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string APVID = grdrow.Cells[1].Text;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select Distinct * from DocData where Apvid =@I_APVID ", con);

            cmd.Parameters.Add("@I_APVID", System.Data.SqlDbType.BigInt).Value = APVID;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                FilePath = dt.Tables[0].Rows[0]["FilePathPDF"].ToString();
                //string outputPath = Request.PhysicalApplicationPath + "DocFile" + "\\" + "APV" + APVID + ".pdf";

                //OpenDoc(inputFile, outputPath);
                //OpenAPVPdf();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('PDF Not Created!!');", true);
                return;
            }
            try
            {
                //view document file 
                if (FilePath.Contains(".doc"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                //view pdf files 
                if (FilePath.Contains(".pdf"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                if (FilePath.Contains(".htm"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                if (FilePath.Contains(".txt"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                else
                {
                    //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('File not in Proper format');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('File not in Proper format');", true);
            }

        }

    }
}