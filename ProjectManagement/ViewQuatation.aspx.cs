﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
//using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;
using System.Net.Mail;
using System.Text;
using System.Net;

namespace ProjectManagement
{
    public partial class ViewQuatation : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
               // Label1.Text = Session["username"].ToString();

                string printid = Request.QueryString["Printid"];
                if (printid == "1")
                {
                    Image1.Visible = true;
                }
                BindData();
            }

        }
        private void BindData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getQuatation";

            //SqlCommand com = new SqlCommand("select * from REQUESTEDQUOTE", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataTable dt = new DataTable();

            adpt.Fill(dt);

            GridView2.DataSource = dt;

            GridView2.DataBind();
            con.Close();
        }

        protected void NewQuatationBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Quatation request form.aspx");
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {


            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[1].Text;
            //Response.Redirect("QuatationPrint.aspx?Quatdate=" + quatdate);


            string url = ("QuatationPrint.aspx?Quatdate=" + quatdate);

            string s = "window.open('" + url + "');";

            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

        }

        protected void LikButEdit_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[1].Text;
            Response.Redirect("EditQuatation.aspx?Quatdate=" + quatdate);

        }

        protected void Lnkdeleterow_Click(object sender, EventArgs e)
        {

            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[1].Text;

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();

            SqlCommand com1 = new SqlCommand("delete FROM REQUESTEDQUOTE where DateTime = @DATETIME; delete FROM HAS where DateTime = @DATETIME;", con1);

            //SqlCommand com1 = new SqlCommand();
            //com1.CommandText = "sp_deleteQuatation";
            //com1.CommandType = System.Data.CommandType.StoredProcedure;
            //com1.Connection = con1; 
            //com1.CommandType = CommandType.Text;
            com1.Parameters.Add("@DATETIME", SqlDbType.NVarChar).Value = quatdate;


            //SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
            //DataTable dt1 = new DataTable();
            //adpt1.Fill(dt1);
            //GridView2.DataSource = dt1;

            com1.ExecuteNonQuery();
            con1.Close();
            BindData();
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Quatation Deleted Successfully!!');", true);
            return;

        }

        protected void btnExtoPdf_Click(object sender, EventArgs e)
        {

            //using (StringWriter sw = new StringWriter())
            //{

            //    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
            //    {

            //        GridView2.RenderControl(hw);

            //        StringReader sr = new StringReader(sw.ToString());

            //        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

            //        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

            //        pdfDoc.Open();

            //        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);

            //        pdfDoc.Close();

            //        Response.ContentType = "application/pdf";

            //        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");

            //        Response.Cache.SetCacheability(HttpCacheability.NoCache);

            //        Response.Write(pdfDoc);

            //        Response.End();

            //    }

            //}


        }
        private void ExportGridToPDF()
        {


            //foreach (GridViewRow row in GridView2.Rows)
            //{
            //    if (row.RowType == DataControlRowType.DataRow)
            //    {
            //        CheckBox chk = (CheckBox)(row.Cells[0].FindControl("chkSelect") as CheckBox);
            //        if (chk.Checked)
            //        {
            //SaveCheckboxValues();



            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Manoj_Gawas.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            GridView2.AllowPaging = false;
            GridView2.DataBind();


            GridView2.RenderControl(hw);

            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

            pdfDoc.Open();
            //htmlparser.Parse(sr);
            // htmlparser.Parse(new StringReader("hello world"));
            //pdfDoc.Close();
            try
            {
                pdfDoc.NewPage();
                htmlparser.Parse(sr);
            }
            catch (Exception)
            {
            }
            finally
            {
                pdfDoc.Close();
            }
            Response.Write(pdfDoc);
            Response.End();
            //}

            // }
            //}
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //
        }

        //protected void LinkButton2_Click(object sender, EventArgs e)
        //{
        //    ExportGridToPDF();
        //}


        //protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{

        //    //Save checked rows before page change
        //    SaveCheckedStates();
        //    GridView2.PageIndex = e.NewPageIndex;
        //    BindData();
        //    //Populate cheked items with its checked status
        //    PopulateCheckedStates();
        //}




        //Save the state of row checkboxes
        private void SaveCheckedStates()
        {
            ArrayList objSubjectAL = new ArrayList();
            int rowIndex = -1;
            foreach (GridViewRow row in GridView2.Rows)
            {
                rowIndex = Convert.ToInt32(GridView2.DataKeys[row.RowIndex].Value);
                bool isSelected = ((CheckBox)row.FindControl("chkSelect")).Checked;
                if (ViewState["SELECTED_ROWS"] != null)
                {
                    objSubjectAL = (ArrayList)ViewState["SELECTED_ROWS"];
                }
                if (isSelected)
                {
                    if (!objSubjectAL.Contains(rowIndex))
                    {
                        objSubjectAL.Add(rowIndex);
                    }
                }
                else
                {
                    objSubjectAL.Remove(rowIndex);
                }
            }
            if (objSubjectAL != null && objSubjectAL.Count > 0)
            {
                ViewState["SELECTED_ROWS"] = objSubjectAL;
            }
        }


        //Populate the saved checked checkbox status
        private void PopulateCheckedStates()
        {
            ArrayList objSubjectAL = (ArrayList)ViewState["SELECTED_ROWS"];
            if (objSubjectAL != null && objSubjectAL.Count > 0)
            {
                foreach (GridViewRow row in GridView2.Rows)
                {
                    int rowIndex = Convert.ToInt32(GridView2.DataKeys[row.RowIndex].Value);
                    if (objSubjectAL.Contains(rowIndex))
                    {
                        CheckBox chkSelectRow = (CheckBox)row.FindControl("chkSelect");
                        chkSelectRow.Checked = true;
                    }
                }
            }
        }

     
    }
}

