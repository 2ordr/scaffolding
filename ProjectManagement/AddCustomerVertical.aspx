﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCustomerVertical.aspx.cs" Inherits="ProjectManagement.AddCustomerVertical" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <title>>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-4/styles.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <script type="text/html" src="jquery/jquery-ui-1.11.1.min.js"></script>
    <script type="text/javascript" src="jquery/FileSaver.js"></script>
    <script type="text/javascript" src="jquery/FileSaver.min.js"></script>

    <script src="jquery/jquery.wordexport.js"></script>

    <style>
        headform1 {
            color: #383838;
        }

        .bc {
            border-color: brown;
        }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .bg {
            text-align: right;
        }

        .table1 {
            border: 1px solid #ddd;
            border-collapse: separate;
            border-radius: 15px;
            border-spacing: 5px;
        }

        .simple {
            color: lightblue;
        }

            .simple:hover {
                color: blue;
            }

        .link-button {
            margin-top: 15px;
            max-width: 95px;
            /*background-color: aquamarine;
            /border-color: black;
            color: #333;*/
            display: inline-block;
            vertical-align: middle;
            text-align: center;
            text-decoration: none;
            align-items: flex-start;
            cursor: default;
            -webkit-appearence: push-button;
            border-style: solid;
            border-width: 1px;
            border-radius: 5px;
            font-size: 1em;
            font-family: inherit;
            border-color: floralwhite;
            padding-left: 5px;
            padding-right: 5px;
            width: 100%;
            min-height: 30px;
        }

            .link-button:hover {
                background-color: skyblue;
                border-color: black;
            }

        .BoxSize {
            /*box-shadow: 5px 5px 5px #888888;*/
            border: solid 0px #555;
            background-color: #F9F6F4;
            box-shadow: 0 0 5px rgba(0,0,0,0.6);
            -moz-box-shadow: 0 0 5px rgba(0,0,0,0.6);
            -webkit-box-shadow: 0 0 10px rgba(0,0,0,0.6);
            -o-box-shadow: 0 0 10px rgba(0,0,0,0.6);
        }

        .title {
            font-size: x-large;
        }

        .first-line {
            font-size: x-large;
            color: green;
        }

        .Position {
            /*Style="z-index: 103; position: absolute; top: 280px;left: 850;"*/
            position: absolute /*absolute*/;
            top: 210px;
            left: 40%;
            z-index: 99;
        }

        span:hover {
            cursor: pointer;
        }

        .parentDisable {
            z-index: 9;
            width: 100%;
            height: 100%;
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            background-color: #000;
            color: #aaa;
            opacity: .4;
            filter: alpha(opacity=50);
        }

        input.underlined {
            border: 0;
            border-bottom: solid 1px #000;
            outline: none; /* prevents textbox highlight in chrome */
        }

        input.underlined1 {
            border: 0;
            outline: none; /* prevents textbox highlight in chrome */
        }

        #Table1 td {
            border-top: 2px solid grey;
        }
    </style>

    <script type="text/javascript">

        function DisableBackButton() {                  //for disabling background browser button
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>

    <script type="text/javascript">   //for selecting only single checkbox for row
        function CheckSingleCheckbox(ob) {
            var grid = ob.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (ob.checked && inputs[i] != ob && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }
    </script>

    <script type="text/javascript">
        function SetColor(id) {
            var links = document.getElementsByTagName("<a>")
            for (var i = 0; i < links.length; i++) {
                links[i].style.color = "Black";
            }
            document.getElementById(id).style.color = "red";
        }
    </script>
    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>


    <script type="text/javascript">
        function ShowDiv2(id) {
            var e = document.getElementById(id);
            if (e.style.visibility == 'hidden')
                e.style.visibility = 'visible';

            else
                e.style.visibility = 'hidden';
            $("#pop1").css("display", "none");

            return false;
        }

    </script>

    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>

    <script type="text/javascript">
        function pop(div) {
            document.getElementById(div).style.display = 'block';
            $("#PopUpDiv").css("visibility", "visible");
            $("#PopUpDiv2").css("visibility", "visible");
            $("#PopUpDiv1").css("visibility", "visible");
            return false;
        }
        function hide(div) {
            document.getElementById(div).style.display = 'none';
            $("#PopUpDiv").css("visibility", "hidden");
            $("#PopUpDiv2").css("visibility", "hidden");
            $("#PopUpDiv1").css("visibility", "hidden");
            return false;
        }
    </script>

    <script type="text/javascript">
        function PrintGridDataAll() {

            var prtGrid = document.getElementById("divAPV1");
            var prtwin = window.open('', 'PrintDivData', 'left=100,top=10,width=730,height=1000,tollbar=0,scrollbars=1,status=0,resizable=0');
            prtwin.document.writeln();
            prtwin.document.write('<style type="text/css">.underlined {border: 0;outline: none; border-bottom: solid 1px #000;  }</style>');
            prtwin.document.write('<style type="text/css">.underlined1 {border: 0;outline: none;  }</style>');
            prtwin.document.write(prtGrid.outerHTML);
            prtwin.document.close();
            prtwin.focus();
            prtwin.print();
            prtwin.close();
        }
    </script>
    <script type="text/javascript">
        function PrintGridDataAll1() {

            var prtGrid = document.getElementById("kj");
            var prtwin = window.open('', 'PrintDivData', 'left=100,top=10,width=730,height=1000,tollbar=0,scrollbars=1,status=0,resizable=0');
            prtwin.document.writeln();
            prtwin.document.write('<style type="text/css">.underlined {border: 0;outline: none; border-bottom: solid 1px #000;  }</style>');
            prtwin.document.write('<style type="text/css">.underlined1 {border: 0;outline: none;  }</style>');
            prtwin.document.write(prtGrid.outerHTML);
            prtwin.document.close();
            prtwin.focus();
            prtwin.print();
            prtwin.close();
        }
    </script>

    <script type="text/javascript">
        function PrintGridDataAll11() {

            var prtGrid = document.getElementById("DivHoriPDF");
            var prtwin = window.open('', 'PrintDivData', 'left=100,top=10,width=730,height=1000,tollbar=0,scrollbars=1,status=0,resizable=0');
            prtwin.document.writeln();
            prtwin.document.write('<style type="text/css">.underlined {border: 0;outline: none; border-bottom: solid 1px #000;  }</style>');
            prtwin.document.write('<style type="text/css">.underlined1 {border: 0;outline: none;  }</style>');
            prtwin.document.write(prtGrid.outerHTML);
            prtwin.document.close();
            prtwin.focus();
            prtwin.print();
            prtwin.close();
        }
    </script>

</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Debug"></asp:ToolkitScriptManager>

        <div style="height: 1000px; width: 1920px">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtSearchBox" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150" OnTextChanged="txtSearchBox_TextChanged"></asp:TextBox>

                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon  app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">

                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%--<li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>--%>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div1')">Rapporter</span></span>

                                                    <div id="Div1" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div2')">Løn Modul</span></a>
                                                    <div id="Div2" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>

                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                --%>
                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUpload" />
                        <asp:PostBackTrigger ControlID="btn_SendMail" />
                        <asp:PostBackTrigger ControlID="BtnExportToDoc" />
                        <asp:PostBackTrigger ControlID="BtnPDFAPV" />
                        <asp:PostBackTrigger ControlID="btnActivepdf" />
                        <asp:PostBackTrigger ControlID="BtnPrint" />
                        <asp:PostBackTrigger ControlID="btnPrintAktiveAPV" />
                        <asp:PostBackTrigger ControlID="BtnSaveAsHtml" />
                        <asp:PostBackTrigger ControlID="btnDocAPV" />
                    </Triggers>
                    <ContentTemplate>
                        <div style="float: left; height: 1000px; width: 300px; border-color: silver; border-width: 5px">
                            <fieldset style="height: 1000px">
                                <legend style="color: black; font-weight: bold; font-size: small"></legend>
                                <div style="height: 10px;">
                                </div>
                                <table style="width: 100%; font-family: Calibri;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 50%;">
                                                <asp:Button CssClass="button" ID="BtnAddCustomer" runat="server" Text="+Tilføj Kunder" BackColor="#CC6699" Style="color: #FFFFFF; font-weight: 500; font-size: medium; background-color: #FF0000" OnClick="BtnAddCustomer_Click" />
                                            </td>
                                            <td style="width: 50%;">
                                                <table class="x-table-layout" id="ext-gen1688" role="presentation" cellspacing="0" cellpadding="0">
                                                    <tbody role="presentation">
                                                        <%-- <tr role="presentation">
                                                    <td class="x-table-layout-cell " role="presentation" rowspan="1" colspan="1"><a tabindex="0" class="x-btn x-unselectable x-sbtn-first x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon" id="button-1109" hidefocus="on" unselectable="on" data-qtip="Simple List"><span class="x-btn-wrap" id="button-1109-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1109-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1109-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-split " id="button-1109-btnIconEl" role="presentation" unselectable="on">&nbsp;</span></span></span></a></td>
                                                    <td class="x-table-layout-cell " role="presentation" rowspan="1" colspan="1"><a tabindex="0" class="x-btn x-unselectable x-sbtn-last x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon x-pressed x-btn-pressed x-btn-default-small-pressed" id="button-1110" hidefocus="on" unselectable="on" data-qtip="Advanced List"><span class="x-btn-wrap" id="button-1110-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1110-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1110-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-no-split " id="button-1110-btnIconEl" role="presentation" unselectable="on"></span></span></span></a></td>
                                                </tr>--%>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px;" colspan="3"></td>
                                        </tr>
                                        <%--<tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>--%>
                                    </tbody>
                                </table>

                                <div style="height: 850px;">
                                    <fieldset>
                                        <legend style="color: black; font-weight: bold; font-size: large">Kundenavn</legend>
                                        <div id="divOverFlowCustomer" runat="server" style="overflow: auto; height: 840px">

                                            <asp:GridView ID="GridViewCustomer" runat="server" AutoGenerateColumns="False" ShowHeader="false" RowStyle-Height="40px" RowStyle-BorderColor="White" RowStyle-BorderWidth="5px" OnRowDataBound="Bound" OnSelectedIndexChanged="GridViewCustomer_SelectedIndexChanged" OnRowCommand="GridViewCustomer_RowCommand">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="10px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged1" onclick="CheckSingleCheckbox(this)" Visible="false" />
                                                            <asp:Label ID="CustId" runat="server" Text='<%# Bind("CustId") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="CustName" runat="server" Text='<%# Bind("CustName") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10px" BorderWidth="0" BorderColor="White" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="CustId" HeaderText="CustId" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />

                                                    <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LBtnCustomerName" runat="server" Font-Underline="false" Text='<%# Bind("CustName")%>' OnClick="LBtnCustomerName_Click"> </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="250px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <span onclick="return confirm('Are you sure to Delete the Customer?')">
                                                                <asp:LinkButton CssClass="simple" ID="Lnkdeleterow" runat="server" Text="" Font-Underline="false" OnClick="Lnkdeleterow_Click" CommandArgument='<%#Eval("CustId") %>' CommandName="DeleteRow"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton></span>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </fieldset>
                                </div>
                            </fieldset>
                        </div>

                        <div id="divVertical" style="float: left; height: 1000px; width: 1420px" runat="server">
                            <div style="width: 100%; height: 90px;">

                                <div style="height: 10px"></div>
                                <div style="height: 40px">
                                    <div class="divider" style="height: 30px; width: 50px"></div>
                                    <asp:Label ID="LblCustName" runat="server" CssClass="clienthead"></asp:Label>
                                    <asp:Label ID="LblCustId" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="LblContId" runat="server" Text="" Visible="false"></asp:Label>
                                </div>

                                <div style="height: 40px">
                                    <table>
                                        <tr>
                                            <td style="height: 30px; width: 50px"></td>

                                            <td style="height: 30px; width: 100px">
                                                <%-- <asp:Button ID="Button2" runat="server" Text="Button" OnClick="BtnExport_Click" />--%>
                                                <asp:LinkButton CssClass="link-button" ID="LBtnOverview" runat="server" Font-Underline="false" OnClick="LBtnOverview_Click" ForeColor="RoyalBlue">Oversigt</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnBasicInfo" runat="server" Font-Underline="false" OnClick="LBtnBasicInfo_Click" ForeColor="RoyalBlue">Stamdata</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnContacts" runat="server" Font-Underline="false" OnClick="LBtnContacts_Click" ForeColor="RoyalBlue">Kontakter</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnDocuments" runat="server" Font-Underline="false" OnClick="LBtnDocuments_Click" ForeColor="RoyalBlue">Dokumenter</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnOrders" runat="server" Font-Underline="false" OnClick="LBtnOrders_Click" ForeColor="RoyalBlue">Ordre</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnQuotes" runat="server" Font-Underline="false" OnClick="LBtnQuotes_Click" ForeColor="RoyalBlue">Tilbud</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnProjects" runat="server" Font-Underline="false" OnClick="LBtnProjects_Click" ForeColor="RoyalBlue">Projekter</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnInvoice" runat="server" Font-Underline="false" OnClick="LBtnInvoice_Click" ForeColor="RoyalBlue">Faktura</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnSendMail" runat="server" Font-Underline="false" OnClick="LBtnSendMail_Click" ForeColor="RoyalBlue">e-mail</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnAPV" runat="server" Font-Underline="false" OnClick="LBtnAPV_Click" ForeColor="RoyalBlue">APV</asp:LinkButton></td>

                                            <td style="height: 30px; width: 100px">
                                                <asp:LinkButton CssClass="link-button" ID="LBtnActiveAPV" runat="server" Font-Underline="false" OnClick="LBtnActiveAPV_Click" ForeColor="RoyalBlue">AktiveAPV</asp:LinkButton>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="gridv" id="divOverView" runat="server" style="height: 900px;" visible="true">
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>

                                    <div style="height: 30px"></div>
                                    <div style="height: 820px; width: 100%">

                                        <div style="height: 200px; width: 100%;">
                                            <div style="height: 150px; width: 49%; float: left">
                                                <div style="height: 150px; width: 7%; float: left"></div>
                                                <div style="height: 150px; width: 93%; background-color: lightblue; float: right">
                                                    <div class="info">
                                                        <table class="client-data-info">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table class="client-info">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="width: 10px;"></td>
                                                                                    <td>
                                                                                        <div class="title">
                                                                                            <asp:Label runat="server" ID="LBVIEWCUSTNAME" CssClass="title"></asp:Label>
                                                                                        </div>
                                                                                        <div class="subtitle">
                                                                                            <asp:Label runat="server" ID="LBVIEWEMAIL"> </asp:Label>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:Label runat="server" ID="LBVIEWCITY"></asp:Label>&nbsp 
                                                                                            <asp:Label runat="server" ID="LBVIEWZIPCODE"></asp:Label>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:Label runat="server" ID="LBVIEWCOUNTRY"></asp:Label>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:Label runat="server"></asp:Label>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="height: 150px; width: 50%; float: right">
                                                <div class="BoxSize" style="height: 150px; width: 50%; background-color: lightblue; float: left">
                                                    <div class="tile-cmp">
                                                        <div class="tile-body">
                                                            <div class="title">Tilbud:</div>
                                                            <div class="first-line" id="QuoteDivNothing" runat="server" visible="false" style="text-align: center;">nothing</div>
                                                            <div class="second-line" runat="server" visible="true" style="text-align: center;">
                                                                <asp:Label runat="server" ID="QuoteCount" Height="100px" Font-Size="XX-Large"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="third-line"></div>
                                                    </div>
                                                </div>
                                                <div class="BoxSize" style="height: 150px; width: 48%; background-color: lightblue; float: right">
                                                    <div class="tile-cmp">
                                                        <div class="tile-body">
                                                            <div class="title">Projekter:</div>
                                                            <div class="first-line" id="ProjectDivNothing" runat="server" visible="false" style="text-align: center">nothing</div>
                                                            <div class="second-line" runat="server" visible="true" style="text-align: center;">
                                                                <asp:Label runat="server" ID="ProjectCount" Height="100px" Font-Size="XX-Large"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="third-line"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="height: 50px; width: 100%"></div>
                                        <div style="margin-left: 50px;">
                                            <table cellpadding="4">
                                                <tr>
                                                    <td>
                                                        <asp:Chart ID="Chart11" runat="server" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)" Palette="BrightPastel" ImageType="Png"
                                                            BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderlineWidth="2"
                                                            BackColor="#D3DFF0" BorderlineColor="26, 59, 105" Height="350px" Width="1350px">

                                                            <BorderSkin SkinStyle="Emboss" />
                                                            <Legends>
                                                                <asp:Legend Name="Legend1" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="false"></asp:Legend>
                                                            </Legends>

                                                            <Titles>
                                                                <asp:Title Alignment="TopCenter" BackColor="180, 165, 191, 228"
                                                                    BackGradientStyle="TopBottom" BackHatchStyle="None" Font="Microsoft Sans Serif, 12pt, style=Bold"
                                                                    Name="Revenue" Text="Tilbud" ToolTip="Revenue" ForeColor="26, 59, 105">
                                                                </asp:Title>

                                                                <asp:Title Alignment="TopCenter" BackColor="Transparent" Font="Microsoft Sans Serif, 10pt, style=Bold " ToolTip="Chart Type"></asp:Title>
                                                            </Titles>

                                                            <Series>
                                                                <asp:Series Name="Count" ChartArea="ChartArea1" Legend="Legend1" CustomProperties="DrawingStyle=Cylinder"
                                                                    BorderColor="64, 0, 0, 0" Color="224, 64, 10" MarkerSize="5">
                                                                </asp:Series>
                                                            </Series>

                                                            <ChartAreas>
                                                                <asp:ChartArea Name="ChartArea1" BackColor="64, 165, 191, 228" BackSecondaryColor="white"
                                                                    BorderColor="64, 64, 64, 64" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                                                    <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="false" Title="Total Cost" ArrowStyle="Triangle" Minimum="1000" Maximum="50000" Interval="5000">
                                                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                                                    </AxisY>

                                                                    <AxisX IsLabelAutoFit="false" LineColor="64, 64, 64, 64" Title="Month" ArrowStyle="Triangle" Interval="1">
                                                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" IsEndLabelVisible="false" Angle="0" />
                                                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                                                    </AxisX>
                                                                </asp:ChartArea>
                                                            </ChartAreas>
                                                        </asp:Chart>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divBasicInfo" runat="server" style="height: 900px;" visible="false">

                                <%--<div style="height: 40px">
                        </div>
                        <div style="height: 30px"></div>--%>
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <div style="height: 850px; width: 100%">

                                        <table style="width: 100%;">

                                            <tr>
                                                <td colspan="3" style="height: 80px; font-weight: bold; font-size: large">Oplysninger:</td>
                                            </tr>
                                            <tr>
                                                <td class="bg" style="width: 25%">Navn</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txtName" ValidationGroup="customer" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtName" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">E-mail</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmail" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RfwEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmail" ValidationGroup="customer" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please Enter Valid E-mail" ControlToValidate="txtEmail" CssClass="requiredFieldValidateStyle" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="customer">
                                                    </asp:RegularExpressionValidator>

                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx1" Enabled="true" TargetControlID="txtEmail" FilterType="Custom, Numbers, LowercaseLetters" ValidChars=".@-_"></asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Adresse</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtAddress" runat="server" Width="300px" Height="50px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">By</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtcity" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtcity" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Land</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtcountry" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtcountry" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Postnr</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtzip" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtzip" ValidationExpression="\d+" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Mobil:</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMobile" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Minimum 8 & Maximum 12 digit required." ControlToValidate="txtMobile" ValidationExpression="^[\s\S]{8,12}$" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx2" Enabled="true" TargetControlID="txtMobile" FilterMode="ValidChars" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Tif</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtphone" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Minimum 8 & Maximum 12 digit required." ControlToValidate="txtphone" ValidationExpression="^[\s\S]{8,12}$" ValidationGroup="customer" ForeColor="Red"></asp:RegularExpressionValidator>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx3" Enabled="true" TargetControlID="txtphone" FilterMode="ValidChars" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Fax</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtFax" runat="server" Width="300px" Height="31px"></asp:TextBox>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Website</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtWebsite" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Enter a valid URL" ControlToValidate="txtWebsite" ValidationExpression="^((http|https)://)?([\w-]+\.)+[\w]+(/[\w- ./?]*)?$" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender5" FilterMode="ValidChars" FilterType="Custom, UppercaseLetters,LowercaseLetters" ValidChars="://.@_" TargetControlID="txtWebsite"></asp:FilteredTextBoxExtender>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Område</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtProvince" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender6" FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="txtProvince"></asp:FilteredTextBoxExtender>

                                                </td>
                                            </tr>

                                             <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="bg">Discount</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDiscount" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender12" FilterType="Numbers" TargetControlID="txtDiscount"></asp:FilteredTextBoxExtender>

                                                </td>
                                            </tr>
                                            <%-- <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg">VAT </td>
                                        <td style="width: 40px"></td>
                                        <td colspan="2">
                                            <asp:DropDownList CssClass="search_categories" ID="DdlistVat" runat="server" Width="300px" Height="36px" OnSelectedIndexChanged="DdlistVat_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem>No</asp:ListItem>
                                                <asp:ListItem>Yes</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>--%>
                                            <tr>
                                                <td style="height: 20px;" colspan="3"></td>
                                            </tr>
                                            <tr id="VatNumber" runat="server">
                                                <td id="td1" runat="server" class="bg">CVR nr:</td>
                                                <td style="width: 40px"></td>
                                                <td colspan="2">
                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtVatNo" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender runat="server" ID="FEx4" Enabled="true" TargetControlID="txtVatNo" FilterMode="ValidChars" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </fieldset>
                                <div class="shadowdiv" style="width: 100%; height: 40px; text-align: center; border: solid 0px black;">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <asp:Button CssClass="buttonn" ID="BtnUpdate" runat="server" Text="OPDATER " Width="101px" Style="background-color: #009900" OnClick="BtnUpdate_Click" ValidationGroup="customer" />

                                                </td>
                                                <%--<td>
                                            <asp:Button CssClass="buttonn" ID="Btnview" runat="server" Text="VIEW" Width="120px" Style="background-color: #009933" OnClick="Btnview_Click" />

                                        </td>
                                        <td>
                                            <asp:Button CssClass="buttonn" ID="BtndeleteCustomer" runat="server" Text="DELETE" Width="120px" Style="background-color: #009933" OnClick="BtndeleteCustomer_Click" />
                                        </td>--%>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <asp:GridView CssClass="gridv" ID="GridViewBasicInfo" AutoGenerateColumns="false" Visible="false" runat="server">
                                </asp:GridView>

                            </div>
                            <div class="gridv" id="divContactInfo" runat="server" style="height: 900px;" visible="false">

                                <fieldset>
                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <div style="height: 40px">
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="Button1" runat="server" Text="Back" BackColor="#009933" Visible="false" />
                                        <asp:Button CssClass="buttonn" ID="BtnAddContact" runat="server" Text="+ Tilføj kontakter" BackColor="#009933" OnClick="BtnAddContact_Click" /><%--OnClick="OpenWindow"--%>
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="BtnRefresh" runat="server" Text="Refresh" BackColor="#009933" Visible="false" OnClick="BtnRefresh_Click" />
                                    </div>
                                    <div style="height: 30px"></div>

                                    <div style="height: 780px;">
                                        <div id="PopUpDiv" runat="server" class="Position" style="height: 650px; width: 700px; bottom: 390px; background-color: #fbfbfb; visibility: hidden">

                                            <fieldset style="height: 650px; width: 700px">

                                                <div style="height: 100%; width: 100%">
                                                    <div style="height: 50px; width: 100%; background-color: lightblue;">
                                                        <div style="height: 30px; width: 30px; display: inline-block;"></div>
                                                        <asp:Label ID="LblNoteHeading" runat="server" Text="tilføje nye kontakter" Font-Size="Large"></asp:Label>
                                                        <span class="menu-item-icon icon-times-circle-o" style="float: right" onclick="ShowDiv2('PopUpDiv')"></span>
                                                    </div>
                                                    <div style="height: 850px; width: 100%;">
                                                        <table style="width: 100%;">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="height: 20px" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="width: 25%; text-align: center;">Navn</td>
                                                                    <td style="width: 50%;">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName1" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                        <%-- <asp:RequiredFieldValidator ID="RfwName" runat="server" ErrorMessage="*" ControlToValidate="txtName1" ValidationGroup="customer" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtName" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer"></asp:RegularExpressionValidator>--%>

                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender7" Enabled="true" FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="txtName1"></asp:FilteredTextBoxExtender>

                                                                    </td>
                                                                    <td style="width: 25%;"></td>
                                                                </tr>


                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center;">e-mail</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmail1" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ErrorMessage="Please Enter Valid E-mail" Font-Size="Small" ControlToValidate="txtEmail1" CssClass="requiredFieldValidateStyle" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="customer">
                                                                        </asp:RegularExpressionValidator>
                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender1" Enabled="true" TargetControlID="txtEmail1" FilterMode="ValidChars" FilterType="Custom, Numbers, LowercaseLetters" ValidChars=".@"></asp:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center;">Adresse</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtAddress1" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center;">By</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtcity1" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ErrorMessage="Only characters allowed" Font-Size="Small" ControlToValidate="txtcity1" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender8" Enabled="true" TargetControlID="txtcity1" FilterType="UppercaseLetters, LowercaseLetters"></asp:FilteredTextBoxExtender>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center;">Land</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtCountry1" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ErrorMessage="Only characters allowed" Font-Size="Small" ControlToValidate="txtCountry1" ValidationExpression="[a-zA-Z ]*$" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender9" Enabled="true" TargetControlID="txtCountry1" FilterType="UppercaseLetters, LowercaseLetters"></asp:FilteredTextBoxExtender>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center;">Postnr</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtZipCode" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtZipCode" ValidationExpression="\d+" ValidationGroup="customer"></asp:RegularExpressionValidator>--%>
                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender4" Enabled="true" TargetControlID="txtZipCode" FilterMode="ValidChars" FilterType="Numbers"></asp:FilteredTextBoxExtender>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center;">Mobil</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMobile1" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ErrorMessage="Min 8 & Max 12 digit required." Font-Size="Small" ControlToValidate="txtMobile1" ValidationExpression="^[\s\S]{8,12}$" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender2" Enabled="true" FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txtMobile1"></asp:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center;">Tif</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtphone1" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ErrorMessage="Min 8 & Max 12 digit required." Font-Size="Small" ControlToValidate="txtphone1" ValidationExpression="^[\s\S]{8,12}$" ValidationGroup="customer"></asp:RegularExpressionValidator>
                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender3" Enabled="true" TargetControlID="txtphone1" FilterMode="ValidChars" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center;">Remark</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtRemark" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bg" style="text-align: center">Område</td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtProvince1" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender10" FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="txtProvince1" Enabled="true"></asp:FilteredTextBoxExtender>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px;" colspan="3"></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                        <div style="height: 25px;"></div>
                                                        <div style="margin-left: 30px;">
                                                            <asp:Button CssClass="twitterStyleTextbox" ID="BtnSaveContact" runat="server" Text="Gem" Height="30" Width="100" OnClick="BtnSaveContact_Click1" />
                                                            <asp:Button CssClass="twitterStyleTextbox" ID="BtnUpdate1" runat="server" Text="Opdater" Height="30" Width="100" ValidationGroup="customer" OnClick="BtnUpdate1_Click" />
                                                            <asp:Button CssClass="twitterStyleTextbox" ID="BtnCancelNote" runat="server" Text="Slet" Height="30" Width="70" OnClientClick="return hide('pop1')" />
                                                        </div>
                                                    </div>
                                                    <%--<div style="height: 50px; width: 100%;">
                                            <div style="height: 20px; width: 10px; display: inline-block"></div>
                                        </div>--%>
                                                </div>
                                            </fieldset>
                                        </div>

                                        <div id="ContactHeader" runat="server" class="allSides" style="">
                                            <table border="0" style="font-family: Calibri;">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 53px; font-size: large;"></td>
                                                    <td style="width: 199px; font-size: large;">Navn</td>
                                                    <td style="width: 199px; font-size: large;">E-mail</td>
                                                    <td style="width: 199px; font-size: large;">Adresse</td>
                                                    <td style="width: 101px; font-size: large;">By</td>
                                                    <td style="width: 100px; font-size: large;">Land</td>
                                                    <td style="width: 101px; font-size: large;">Postnr</td>
                                                    <td style="width: 99px; font-size: large;">Mobil</td>
                                                    <td style="width: 99px; font-size: large;">Tif</td>
                                                    <td style="width: 99px; font-size: large;">Bemørkninger</td>
                                                    <td style="width: 93px; font-size: large;">Område</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 745px; overflow: auto">

                                            <table style="font-family: Calibri;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GViewContact" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" RowStyle-BorderWidth="5px" BorderColor="White" BorderWidth="0px" RowStyle-BorderColor="White">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Visible="false" OnCheckedChanged="chkSelect_CheckedChanged" onclick="CheckSingleCheckbox(this)" />
                                                                        <asp:Label ID="CONTID" runat="server" Text='<%# Bind("CONTID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="50px" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LBtnName" runat="server" Font-Underline="false" Text='<%# Bind("Name")%>' OnClick="LBtnName_Click1"> </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="200px" BorderWidth="0" />
                                                                </asp:TemplateField>
                                                                <%-- <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />--%>
                                                                <asp:BoundField DataField="EmailId" HeaderText="Email-id" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="AddressLine1" HeaderText="Address" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="City" HeaderText="City" ItemStyle-Width="100px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Country" HeaderText="Country" ItemStyle-Width="100px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="ZipCode" HeaderText="Zip Code" ItemStyle-Width="100px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Mobile" HeaderText="Mobile No" ItemStyle-Width="100px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Phone" HeaderText="Phone" ItemStyle-Width="100px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Remark" HeaderText="Remark" ItemStyle-Width="100px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Province" HeaderText="Province" ItemStyle-Width="150px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divDocuments" runat="server" style="height: 900px;" visible="false">

                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <div style="height: 40px">
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="Button6" runat="server" Text="Back" BackColor="#009933" Visible="false" />

                                        <asp:Label ID="Label2" runat="server" Visible="false"></asp:Label>

                                        <asp:FileUpload ID="FileUpload2" runat="server" />
                                        <asp:Button CssClass="buttonn" ID="btnUpload" runat="server" Text="Vedhæft" OnClick="btnUpload_Click" />

                                        <div class="divider"></div>
                                    </div>
                                    <div style="height: 30px"></div>

                                    <div style="height: 780px;">
                                        <div class="allSides" style="width: 100%;">
                                            <table border="0" style="font-family: Calibri;">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 5px; font-weight: 700"></td>
                                                    <td style="width: 328px; font-weight: 700">Filnavn</td>
                                                    <td style="width: 200px; font-weight: 700">Aben</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" style="height: 7450px; overflow: auto;">
                                            <table style="font-family: Calibri;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvFileUpload" runat="server" ShowHeader="false" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px">
                                                            <Columns>

                                                                <asp:BoundField DataField="CustId" HeaderText="CustId" Visible="false" />
                                                                <asp:BoundField DataField="FileName" HeaderText="FileName" ItemStyle-Width="350px" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <%--     <asp:BoundField DataField="FilePath" HeaderText="FilePath" ItemStyle-Width="100" HeaderStyle-BackColor="#F2F2F2" HeaderStyle-Font-Size="Large" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />--%>
                                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFilePath" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="300px" BorderWidth="5" BorderColor="White" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="lbtnViewImage" runat="server" Font-Underline="false" OnClick="lbtnViewImage_Click"><span class="action-icon icon-eye"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="200px" BorderWidth="5" BorderColor="White" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="View" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="1000px" BorderWidth="5" BorderColor="White" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divOrder" runat="server" style="height: 900px;" visible="false">
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <div style="height: 40px">
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="BtnOrder" runat="server" Text="+Tilføj nye Ordre" OnClick="BtnOrder_Click" BackColor="#009933" />
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="Button3" runat="server" Text="Delete" BackColor="#009933" Visible="false" />
                                    </div>
                                    <div style="height: 30px"></div>
                                    <div style="height: 780px; width: 100%">
                                        <div id="OrderHeader" runat="server" class="allSides" style="width: 100%;">
                                            <table border="0" style="font-family: Calibri; width: 100%">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 22px; font-size: large;"></td>
                                                    <td style="width: 100px; font-size: large;">Dato</td>
                                                    <td style="width: 99px; font-size: large;">Total</td>
                                                    <td style="width: 100px; font-size: large;">Status</td>
                                                    <td style="width: 100px; font-size: large;">Vis</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div style="height: 740px; overflow: auto">
                                            <asp:GridView ID="GridViewOrder" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" OnRowDataBound="Bound" OnSelectedIndexChanged="GridViewOrder_SelectedIndexChanged">
                                                <Columns>

                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="20px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="OrderteChkbox" runat="server" AutoPostBack="true" Visible="false" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20px" BorderColor="White" BorderWidth="5" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="OrderId" HeaderText="" Visible="false" />
                                                    <asp:BoundField DataField="OrderDate" HeaderText="Order Date" HeaderStyle-Width="100px" ItemStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />
                                                    <asp:BoundField DataField="OrderTotal" HeaderText="Total Order" HeaderStyle-Width="100px" ItemStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />
                                                    <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-Width="100px" ItemStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />

                                                    <asp:TemplateField HeaderText="Edit" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton CssClass="simple" ID="LikButEdit" runat="server" Text="" CommandName="Print" CausesValidation="false" Font-Underline="false" OnClick="LikButEdit_Click"> <span class="action-icon icon-eye"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divQuate" runat="server" style="height: 900px;" visible="false">
                                <%-- <div style="height: 15px"></div>--%>
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <div style="height: 40px">
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="BtnQuotes" runat="server" Text="+Tilføj Tilbud" OnClick="BtnQuotes_Click" BackColor="#009933" />
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="Button4" runat="server" Text="Delete" BackColor="#009933" Visible="false" />
                                    </div>
                                    <div style="height: 30px"></div>
                                    <div style="height: 780px; width: 100%">
                                        <div id="QuoteHeader" runat="server" class="allSides" style="width: 100%;">
                                            <table border="0" style="font-family: Calibri; width: 100%;">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 27px; font-size: large;"></td>
                                                    <td style="width: 117px; font-size: large;">Dato</td>
                                                    <td style="width: 120px; font-size: large;">Godkendt af Admin</td>
                                                    <td style="width: 117px; font-size: large;">Bekræftelse</td>
                                                    <td style="width: 120px; font-size: large;">Total</td>
                                                    <td style="width: 60px; font-size: large;">Vis</td>
                                                    <td style="width: 60px; font-size: large;">Rediger</td>
                                                    <td style="width: 60px; font-size: large;">Udskriv</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div style="height: 740px; overflow: auto">
                                            <table style="font-family: Calibri; width: 100%;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="CusQuoteGrideView" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" RowStyle-BorderColor="White" RowStyle-BorderWidth="5px" BorderWidth="0px" OnRowDataBound="Bound">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="20px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Visible="false" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="20px" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="DATETIME" HeaderText=" Date" ItemStyle-Width="100" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="AdminApproval" HeaderText="Admin_Approval" ItemStyle-Width="100" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="RespondTime" HeaderText="Respond_Time" ItemStyle-Width="100" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="TotalCost" HeaderText="TotalCost" ItemStyle-Width="100" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />

                                                                <asp:TemplateField HeaderText="View" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LinkBtnView" runat="server" CausesValidation="false" Font-Underline="false" OnClick="LinkBtnView_Click"><span class="action-icon icon-eye"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="50px" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Edit" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LikButEdit1" runat="server" CausesValidation="false" Font-Underline="false" OnClick="LikButEdit1_Click"><span class="action-icon icon-edit-write"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="50px" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Print" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" Font-Underline="false" OnClick="LinkButton1_Click"><img src="image/PrintLogo.jpg" height="20" width="20"/></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="50px" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divProject" runat="server" style="height: 900px;" visible="false">
                                <%--  <div style="height: 15px"></div>--%>
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <div style="height: 40px">
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="Btnproject" runat="server" Text="+Tilføj Projekter" OnClick="Btnproject_Click" BackColor="#009933" />
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="Button5" runat="server" Text="Delete" BackColor="#009933" Visible="false" />
                                    </div>
                                    <div style="height: 30px"></div>

                                    <div style="height: 780px; width: 100%">
                                        <div id="ProHeader" runat="server" class="allSides" style="width: 100%;">
                                            <table style="font-family: Calibri;">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 5px;"></td>
                                                    <td style="width: 67px; font-size: large;">ID</td>
                                                    <td style="width: 333px; font-size: large">Navn</td>
                                                    <td style="width: 335px; font-size: large">Beskrivelse</td>
                                                    <td style="width: 330px; font-size: large">Dato</td>
                                                    <td style="width: 100px; font-size: large">Slet/Vis</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="stocklist" runat="server" style="overflow: auto; width: 100%; height: 740px;">

                                            <table style="width: 100%; font-family: Calibri;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridViewProjectDetails" runat="server" AutoGenerateColumns="False" ShowHeader="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" OnRowDataBound="Bound">
                                                            <Columns>
                                                                <asp:BoundField DataField="ProjectId" HeaderText="ProjectId" ItemStyle-Width="20px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="ProjectName" HeaderText="Project Name" ItemStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Description" HeaderText="Project Description" ItemStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="ProjectDate" HeaderText="Start Date" ItemStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />

                                                                <asp:TemplateField HeaderText="View/Delete/Update" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <span onclick="return confirm('Are you sure to Delete the Project?')">
                                                                            <asp:LinkButton CssClass="simple" ID="lnkDeleteProject" runat="server" Font-Underline="false" OnClick="lnkDeleteProject_Click"><span class="action-icon icon-trashcan"></asp:LinkButton></span>
                                                                        <asp:LinkButton CssClass="simple" ID="lnkUpdateProject" runat="server" Font-Underline="false" OnClick="lnkUpdateProject_Click"><span class="action-icon icon-eye"></span></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="100px" />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="divInvoice" runat="server" style="height: 900px;" visible="false">
                                <fieldset>
                                    <legend></legend>
                                    <div style="height: 40px">
                                        <div class="divider"></div>
                                        <asp:Button CssClass="buttonn" ID="BtnInvoice" runat="server" Text="+Tilføj faktura" OnClick="BtnInvoice_Click" BackColor="#009933" />
                                    </div>

                                    <div style="height: 30px"></div>
                                    <div style="height: 780px; width: 100%">
                                        <div id="InvoiceHeader" runat="server" class="allSides" style="width: 100%;" visible="false">
                                            <table border="0" style="font-family: Calibri; width: 100%;">
                                                <tr style="background-color: #F2F2F2; height: 40px">

                                                    <td style="width: 5%; font-size: large;"></td>
                                                    <td style="width: 10%; font-size: large;">Fakturanr:</td>
                                                    <td style="width: 20%; font-size: large;">Ordrenr:</td>
                                                    <td style="width: 20%; font-size: large;">Dato</td>
                                                    <td style="width: 40%; font-size: large;">Total</td>
                                                    <%-- <td style="width: 123px; font-size: large;">Print</td>--%>
                                                    <td style="width: 5%; font-size: large;">Vis</td>

                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height: 740px; width: 100%; overflow: auto">
                                            <table style="font-family: Calibri; width: 100%;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="CustInvoiceGridView" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" RowStyle-BorderColor="White" RowStyle-BorderWidth="5px" BorderWidth="0px" OnRowDataBound="Bound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="20px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="CHKselect" runat="server" AutoPostBack="true" Visible="false" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="InvoiceNo" HeaderText=" InvoiceNo" ItemStyle-Width="10%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="OrderId" HeaderText=" OrderId" ItemStyle-Width="20%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="InvoiceStartDate" HeaderText="Date" ItemStyle-Width="20%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="TotalInvoiceCost" HeaderText="Total Cost" ItemStyle-Width="40%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />

                                                                <%--<asp:TemplateField HeaderText="Print" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Print" CausesValidation="false" Font-Underline="false" OnClick="LinkButton1_Click"><img src="image/images.jpg" height="20" width="20"/></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Edit" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LnkBtnEditInvoice" runat="server" Text="" CausesValidation="false" Font-Underline="false" OnClick="LnkBtnEditInvoice_Click"><span class="action-icon icon-eye"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="" id="divSendMail" runat="server" style="height: 900px;" visible="false">
                                <div style="width: 100%; height: 800px;">

                                    <fieldset style="width: 100%; height: 860px;">

                                        <legend style="color: black; font-weight: bold; font-size: large"></legend>
                                        <table style="width: 100%;">

                                            <tbody>
                                                <tr>
                                                    <td colspan="3" style="height: 100px"></td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 20%;"></td>
                                                    <td class="divcol" style="width: 60%;">

                                                        <table class="table1" style="width: 100%;">

                                                            <tr>
                                                                <td style="height: 15px; text-align: center; font-weight: bold; font-size: large">Ny e-mail</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div>
                                                                        <div style="margin: 10px 5px; padding-top: 10px;">
                                                                            <div>
                                                                                Til :
                                                                            </div>
                                                                            <div>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 5%;"></td>
                                                                                        <td style="width: 90%;">
                                                                                            <asp:TextBox ID="txt_To" CssClass="twitterStyleTextbox" runat="server" Width="100%" Height="40"></asp:TextBox>

                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ErrorMessage="Please Enter Valid E-mail" ControlToValidate="txt_To" Font-Size="Small" CssClass="requiredFieldValidateStyle" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="e-mail">
                                                                                            </asp:RegularExpressionValidator>

                                                                                            <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender11" Enabled="true" TargetControlID="txt_To" FilterMode="ValidChars" FilterType="Custom, Numbers, LowercaseLetters" ValidChars=".@-_"></asp:FilteredTextBoxExtender>

                                                                                        </td>
                                                                                        <td style="width: 5%;"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 15px">
                                                                    <div>
                                                                        <div style="margin: 10px 5px; padding-top: 10px;">
                                                                            <div>
                                                                                Emne :
                                                                            </div>
                                                                            <div>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 5%;"></td>
                                                                                        <td style="width: 90%;">
                                                                                            <asp:TextBox ID="txt_Subject" CssClass="twitterStyleTextbox" runat="server" Width="100%" Height="40"></asp:TextBox></td>
                                                                                        <td style="width: 5%;"></td>
                                                                                    </tr>
                                                                                </table>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 15px">
                                                                    <div>
                                                                        <div style="margin: 10px 5px; padding-top: 10px;">
                                                                            <div>
                                                                                Fil:
                                                                            </div>
                                                                            <div>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 5%;"></td>
                                                                                        <td style="width: 90%;">
                                                                                            <asp:FileUpload ID="FileUpload1" runat="server" Height="40" Width="750px" />
                                                                                        </td>
                                                                                        <td style="width: 5%;"></td>
                                                                                    </tr>
                                                                                </table>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 15px">
                                                                    <div>
                                                                        <div style="margin: 10px 5px; padding-top: 10px;">
                                                                            <div>
                                                                                Besked :
                                                                            </div>
                                                                            <div>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 5%;"></td>
                                                                                        <td style="width: 90%;">
                                                                                            <asp:TextBox ID="txt_Message" CssClass="twitterStyleTextbox" runat="server" Width="100%" Height="200px" TextMode="MultiLine"></asp:TextBox></td>
                                                                                        <td style="width: 5%;"></td>
                                                                                    </tr>
                                                                                </table>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 15px;">
                                                                    <div style="text-align: center;">
                                                                        <asp:Button ID="btn_SendMail" CssClass="buttonn" runat="server" OnClick="btn_SendMai_Click" Text="Send" BackColor="#009933" />
                                                                        <asp:Button ID="btn_cancel" CssClass="buttonn" runat="server" Text="Slet" OnClick="btn_cancel_Click" BackColor="Red" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 20%;"></td>
                                                </tr>

                                                <tr>
                                                    <td colspan="3" style="height: 50px"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="height: 50px; text-align: center;">
                                                        <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="" id="divAPV" runat="server" style="height: 880px; width: 100%;" visible="false">
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>

                                    <div style="height: 10px; width: 100%;">
                                        <asp:Button CssClass="buttonn" ID="BtnAddAPV" runat="server" Text="+ADDAPV" OnClick="BtnAddAPV_Click" />
                                        <%--     <asp:Button ID="pdf" runat="server" Text="Button" OnClick="pdf_Click" />--%>
                                    </div>
                                    <div style="height: 870px; width: 100%;">
                                        <div style="height: 20px; width: 100%;"></div>
                                        <div id="DivAPV_Header" style="height: 50px; width: 100%;" runat="server" visible="false">
                                            <table border="0" style="font-family: Calibri; width: 100%;">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 3%; font-weight: 700"></td>
                                                    <td style="width: 7%; font-weight: 700">APVID:</td>
                                                    <td style="width: 10%; font-weight: 700">Sag</td>
                                                    <td style="width: 30%; font-weight: 700">Kunde</td>
                                                    <td style="width: 10%; font-weight: 700">Kontakt</td>
                                                    <td style="width: 10%; font-weight: 700">Telefon</td>
                                                    <td style="width: 15%; font-weight: 700">Indgået Dato:</td>
                                                    <td style="width: 5%; font-weight: 700">Rediger</td>
                                                    <td style="width: 5%; font-weight: 700">Vis</td>
                                                    <td style="width: 5%; font-weight: 700">Slet</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height: 800px; width: 100%; overflow: auto;">
                                            <table style="font-family: Calibri; width: 100%;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridViewAPV" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" RowStyle-BorderWidth="5px" BorderColor="White" BorderWidth="0px" RowStyle-BorderColor="White">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAPIDV" runat="server" Text='<%# Bind("APVID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="3%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="APVID" HeaderText="APVID" ItemStyle-Width="7%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="sag" HeaderText="sag" ItemStyle-Width="10%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Kunde" HeaderText="Kunde" ItemStyle-Width="30%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Kontakt" HeaderText="Kontakt" ItemStyle-Width="10%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="Telefon" HeaderText="Telefon" ItemStyle-Width="10%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="ComplationDate" HeaderText="ComplationDate" ItemStyle-Width="15%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LbtnEditAPV" runat="server" Font-Underline="false" OnClick="LbtnEditAPV_Click"><span class="menu-item-icon icon-edit-write"></span></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LBtViewAPV" runat="server" Font-Underline="false" OnClick="LBtViewAPV_Click"><span class="action-icon icon-eye"></span></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <span onclick="return confirm('Are you sure to Delete the APV?')">
                                                                            <asp:LinkButton CssClass="simple" ID="LbtnDeleteAPV" runat="server" Font-Underline="false" OnClick="LbtnDeleteAPV_Click"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton></span>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div runat="server" id="PopUpDiv1" style="height: 880px; width: 82.5%; overflow: auto; visibility: hidden; z-index: 101; background-color: white; position: absolute; left: 17.5%; top: 95px;">

                                        <div style="height: 50px; width: 100%;">
                                            <span class="menu-item-icon icon-times-circle-o" style="float: right" onclick="ShowDiv2('PopUpDiv1')"></span>
                                            <asp:Button CssClass="buttonn" ID="BtnPrint" runat="server" Text="Print" Style="float: right" OnClick="BtnPrint_Click" />
                                            <asp:Button ID="btnDocAPV" runat="server" Text="Export" CssClass="buttonn" Style="float: right" OnClick="BtnAPV_Click" Visible="false" />
                                            <asp:Button CssClass="buttonn" ID="BtnPDFAPV" runat="server" Text="PDF" Style="float: right" OnClick="BtnPDFAPV_Click" Visible="true" />
                                            <asp:Button CssClass="buttonn" ID="BtnSaveAPV" runat="server" Text="Save" Style="float: right" OnClick="BtnSaveAPV_Click" Visible="false" />
                                            <asp:Button CssClass="buttonn" ID="BtnUpdateAPV" runat="server" Text="Update" Style="float: right" OnClick="BtnUpdateAPV_Click" Visible="false" />

                                            <asp:Label ID="lblSimpleAPVID" runat="server" Text="" Visible="false"></asp:Label>
                                        </div>

                                        <div id="divAPV1" style="width: 100%;" runat="server">
                                            <div id="divAPV11" runat="server">
                                                <div style="height: 130px; width: 100%;">
                                                    <table style="height: 100%; width: 100%;">
                                                        <tr runat="server" style="width: 100%;">
                                                            <td runat="server" style="width: 60%;">
                                                                <asp:Image ID="imguser" runat="server" Height="100%" Width="60%" ImageUrl="~/image/pmglogo1.jpg" />
                                                                <%--<asp:Image ID="imguser" runat="server" Height="100%" Width="60%" ImageUrl=" C:/Users/Prathamesh/Desktop/Scaffolding/16_April/ProjectManagement/ProjectManagement/image/pmglogo1.jpg" />--%>

                                                                <%-- <img runat="server" height="130" width="200" src="~/image/pmglogo1.jpg" />--%>
                                                            </td>
                                                            <td style="width: 40%;">

                                                                <asp:Label ID="lblsagsnr" Text="Sagsnr.:" runat="server" Font-Size="20px"></asp:Label><br />

                                                                <asp:Label Text="Sags-APV" runat="server" Font-Size="40px"></asp:Label>
                                                                <asp:Label runat="server" ID="LAPVID" Visible="false"></asp:Label>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="height: 60px; width: 100%;">
                                                    <table style="height: 100%; width: 100%;" border="1">
                                                        <tr>
                                                            <td style="height: 100%; width: 50%;">

                                                                <asp:Label ID="lblsagsAPV" Text="Sag:" runat="server" Font-Bold="true"></asp:Label>
                                                                <asp:TextBox ID="txtsag" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                                <asp:Label ID="lbltxtsag" runat="server" Text="" Width="250px" Visible="false"></asp:Label>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterMode="InvalidChars" InvalidChars=" " TargetControlID="txtsag"></asp:FilteredTextBoxExtender>
                                                            </td>
                                                            <td style="height: 100%; width: 50%;">
                                                                <asp:Label ID="lblPabegy" Text="Påbegyndes/udføres:" runat="server" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="height: 60px; width: 100%;">
                                                    <table style="height: 100%; width: 100%;" border="1">
                                                        <tr>
                                                            <td style="height: 100%;" width="50%">

                                                                <asp:Label ID="lblKunde" Text="Kunde:" runat="server" Font-Bold="true"></asp:Label>
                                                                <asp:TextBox ID="txtKunde" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                                <asp:Label ID="lbltxtKunde" runat="server" Text="" Width="250px" Visible="false"></asp:Label>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterMode="InvalidChars" InvalidChars=" " TargetControlID="txtKunde"></asp:FilteredTextBoxExtender>
                                                            </td>
                                                            <td style="height: 100%;" width="25%">
                                                                <asp:Label ID="lblKondu" Text="Konduktør:" runat="server" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td style="height: 100%;" width="25%">
                                                                <asp:Label ID="lblVej" Text="Vej & Park:" runat="server" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="height: 60px; width: 100%;">
                                                    <table style="height: 100%; width: 100%;" border="1">
                                                        <tr>
                                                            <td style="height: 100%; width: 50%;">

                                                                <asp:Label ID="lblKontakt" Text="Kontakt:" runat="server" Font-Bold="true"></asp:Label>
                                                                <asp:TextBox ID="txtKontakt" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                                <asp:Label ID="lbltxtKontakt" runat="server" Text="" Width="250px" Visible="false"></asp:Label>
                                                                <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender13" Enabled="true" TargetControlID="txtKontakt" FilterType="Numbers" InvalidChars=" "></asp:FilteredTextBoxExtender>

                                                            </td>
                                                            <td style="height: 100%; width: 50%;">
                                                                <asp:Label ID="lblIndgaet" Text="Indgået dato:" runat="server" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="height: 60px; width: 100%;">
                                                    <table style="height: 100%; width: 100%;" border="1">
                                                        <tr>
                                                            <td style="height: 100%; width: 50%;">

                                                                <asp:Label ID="lbltelefon" Text="Telefon:" runat="server" Font-Bold="true"></asp:Label>
                                                                <asp:TextBox ID="txtTelefon" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                                <asp:Label ID="lbltxtTelefon" runat="server" Text="" Width="250px" Visible="false"></asp:Label>
                                                                <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender14" Enabled="true" TargetControlID="txtTelefon" FilterType="Numbers" InvalidChars=" "></asp:FilteredTextBoxExtender>

                                                            </td>
                                                            <td style="height: 100%; width: 50%;">
                                                                <asp:Label ID="lblsjak" Text="Sjak:" runat="server" Font-Bold="true"></asp:Label>
                                                                <asp:TextBox ID="txtSjak" runat="server" CssClass="underlined" Width="250px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="height: 120px; width: 100%;">
                                                    <table style="height: 100%; width: 100%;" border="1">
                                                        <tr>

                                                            <td style="height: 100%;" width="30%">
                                                                <asp:Label ID="lblstillads" Text="Stillads til:" runat="server" Font-Bold="true"></asp:Label>
                                                                <table style="height: 75%; width: 100%;" border="0">
                                                                    <tr>
                                                                        <%--<asp:Label ID="lblstillads" Text="Stillads til:" runat="server" Font-Size="24px"></asp:Label>--%>
                                                                        <td style="height: 100%; width: 55%;">

                                                                            <p>
                                                                                <asp:CheckBox ID="CheckBox49" runat="server" />Facade<br />
                                                                                <asp:CheckBox ID="CheckBox50" runat="server" />Vinduer<br />
                                                                                <asp:CheckBox ID="CheckBox51" runat="server" />Tag

                                                                            </p>

                                                                        </td>
                                                                        <td style="height: 100%; width: 45%;">

                                                                            <p>
                                                                                <asp:CheckBox ID="CheckBox52" runat="server" />Gade<br />
                                                                                <asp:CheckBox ID="CheckBox53" runat="server" />Gård<br />
                                                                                <asp:CheckBox ID="CheckBox54" runat="server" />Andet

                                                                            </p>

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>

                                                            <td style="height: 100%;" width="20%">
                                                                <asp:Label ID="Label4" Text=" " runat="server" Font-Size="20px"></asp:Label><br />
                                                                <asp:Label ID="lblAntalranker" Text="Antal ranker:" runat="server"></asp:Label><br />
                                                                <asp:Label ID="lblLangde" Text="Længde:" runat="server"></asp:Label><br />
                                                                <asp:Label ID="lblHojde" Text="Højde:" runat="server"></asp:Label>

                                                            </td>
                                                            <td style="height: 100%;" width="25%">
                                                                <asp:Label ID="lblStilladssystem" Text="Stilladssystem:" runat="server" Font-Bold="true"></asp:Label>


                                                                <p>
                                                                    <asp:CheckBox ID="CheckBox55" runat="server" />(+8)<br />
                                                                    <asp:CheckBox ID="CheckBox56" runat="server" />Haki ram<br />
                                                                    <asp:CheckBox ID="CheckBox57" runat="server" />Haki mur
                                                                </p>
                                                            </td>
                                                            <td style="height: 100%;" width="25%">
                                                                <asp:Label ID="lblLastklasse" Text="Lastklasse:" runat="server" Font-Bold="true"></asp:Label>


                                                                <p>
                                                                    <asp:CheckBox ID="CheckBox58" runat="server" />Kl.3-200Kg/m2<br />
                                                                    <asp:CheckBox ID="CheckBox59" runat="server" />Kl.5-450 Kg/m2<br />
                                                                    <asp:CheckBox ID="CheckBox60" runat="server" />_____________
                                                                </p>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="height: 255px; width: 100%;">
                                                    <table style="height: 100%; width: 100%;" border="1">
                                                        <tr>
                                                            <td style="height: 100%;" width="30%">
                                                                <asp:Label ID="lblFastgorelser" Text="Fastgørelser:" runat="server" Font-Bold="true"></asp:Label>

                                                                <p>
                                                                    <asp:CheckBox ID="CheckBox61" runat="server" />Mur<br />
                                                                    <asp:CheckBox ID="CheckBox62" runat="server" />Træ<br />
                                                                    <asp:CheckBox ID="CheckBox63" runat="server" />Beton<br />
                                                                    <asp:CheckBox ID="CheckBox64" runat="server" />Andet
                                                                </p>
                                                                <table border="0" style="height: 50%; width: 100%; border-top: 2px solid grey">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label3" Text="Strømforsyning:" runat="server" Font-Bold="true"></asp:Label>

                                                                            <p>
                                                                                <asp:CheckBox ID="CheckBox65" runat="server" />Byggestrøm<br />
                                                                                <asp:CheckBox ID="CheckBox66" runat="server" />Generator<br />
                                                                                <asp:CheckBox ID="CheckBox67" runat="server" />Andet 
                                                                            </p>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="height: 100%;" width="30%">
                                                                <asp:Label ID="lblTransportafmateriel" Text="Transport af materiel:" runat="server" Font-Bold="true"></asp:Label>

                                                                <p>
                                                                    <asp:CheckBox ID="CheckBox68" runat="server" />Lastbil<br />
                                                                    <asp:CheckBox ID="CheckBox69" runat="server" />Lille bil<br />
                                                                    <asp:CheckBox ID="CheckBox70" runat="server" />Truck<br />
                                                                    <asp:CheckBox ID="CheckBox71" runat="server" />Trækvogn<br />
                                                                    <asp:CheckBox ID="CheckBox72" runat="server" />Alurullevogne<br />
                                                                    <asp:CheckBox ID="CheckBox73" runat="server" />Andet 
                                                                              
                                                                </p>

                                                                <asp:Label ID="lblTransportantalgange" Text="Transport antal gange:" runat="server"></asp:Label>
                                                            </td>
                                                            <td style="height: 100%;" width="40%">
                                                                <asp:Label ID="lblPersonlige" Text="Personlige værnemidler:" runat="server" Font-Bold="true"></asp:Label>

                                                                <p>
                                                                    <asp:CheckBox ID="CheckBox74" runat="server" />Sele/line<br />
                                                                    <asp:CheckBox ID="CheckBox75" runat="server" />Maske<br />
                                                                    <asp:CheckBox ID="CheckBox76" runat="server" />Høreværn<br />
                                                                    <asp:CheckBox ID="CheckBox77" runat="server" />Briller<br />
                                                                    <asp:CheckBox ID="CheckBox78" runat="server" />Andet 
                                                                             
                                                                </p>
                                                                <table style="height: 40%; width: 100%;" border="0">
                                                                    <tr>
                                                                        <td style="border-top: 2px solid grey">
                                                                            <asp:Label ID="lblVelfardsforanstal" Text="Velfærdsforanstaltninger:" runat="server" Font-Bold="true"></asp:Label>

                                                                            <p>
                                                                                <asp:CheckBox ID="CheckBox79" runat="server" />Deleskur<br />
                                                                                <asp:CheckBox ID="CheckBox80" runat="server" />Mangler<br />
                                                                                <asp:CheckBox ID="CheckBox81" runat="server" />Adgang til toilet<br />
                                                                            </p>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div id="divAPV12" runat="server" style="height: 52px; width: 100%;">
                                                <table style="height: 100%; width: 100%;" border="1">
                                                    <tr>
                                                        <td style="height: 52px; width: 100%;">
                                                            <asp:Label ID="LabeldivAPV12" Text="" runat="server" Font-Size="12px"></asp:Label>
                                                            <asp:TextBox ID="TextBoxdivAPV12" runat="server" CssClass="underlined1" Width="100%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divAPV13" runat="server" style="height: 52px; width: 100%;">
                                                <table style="height: 100%; width: 100%;" border="1">
                                                    <tr>
                                                        <td style="height: 52px; width: 100%;">
                                                            <asp:Label ID="LabeldivAPV13" Text="" runat="server" Font-Size="12px"></asp:Label>
                                                            <asp:TextBox ID="TextBoxdivAPV13" runat="server" CssClass="underlined1" Width="100%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divAPV14" runat="server" style="height: 52px; width: 100%;">
                                                <table style="height: 100%; width: 100%;" border="1">
                                                    <tr>
                                                        <td style="height: 52px; width: 100%;">
                                                            <asp:Label ID="LabeldivAPV14" Text="" runat="server" Font-Size="12px"></asp:Label>
                                                            <asp:TextBox ID="TextBoxdivAPV14" runat="server" CssClass="underlined1" Width="100%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divAPV15" runat="server" style="height: 52px; width: 100%;">
                                                <table style="height: 100%; width: 100%;" border="1">
                                                    <tr>
                                                        <td style="height: 52px; width: 100%;">
                                                            <asp:Label ID="LabeldivAPV15" Text=" " runat="server" Font-Size="12px"></asp:Label>
                                                            <asp:TextBox ID="TextBoxdivAPV15" runat="server" CssClass="underlined1" Width="100%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="gridv" id="DivActiveAPV" runat="server" style="height: 880px;" visible="false">
                                <fieldset>

                                    <legend style="color: black; font-weight: bold; font-size: medium"></legend>
                                    <div style="height: 10px; width: 100%;">
                                        <asp:Button ID="BtnAddActiveAPV" CssClass="buttonn" runat="server" Text="+ADDAPV" OnClick="BtnAddActiveAPV_Click" />
                                    </div>
                                    <div style="height: 870px; width: 100%;">
                                        <div style="height: 20px; width: 100%;"></div>
                                        <div id="AktiveAPV_Header" style="height: 50px; width: 100%;" runat="server" visible="false">
                                            <table border="0" style="font-family: Calibri; width: 100%;">
                                                <tr style="background-color: #F2F2F2; height: 40px">
                                                    <td style="width: 5%; font-weight: 700"></td>
                                                    <td style="width: 10%; font-weight: 700">AktiveAPVId:</td>
                                                    <td style="width: 70%; font-weight: 700">Sagsnavn</td>
                                                    <td style="width: 5%; font-weight: 700">Rediger</td>
                                                    <td style="width: 5%; font-weight: 700">Vis</td>
                                                    <td style="width: 5%; font-weight: 700">Slet</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height: 800px; width: 100%; overflow: auto;">
                                            <table style="font-family: Calibri; width: 100%;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridActiveAPV" runat="server" AutoGenerateColumns="False" ShowHeader="false" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" RowStyle-BorderWidth="5px" BorderColor="White" BorderWidth="0px" RowStyle-BorderColor="White">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblActiveAPV" runat="server" Text='<%# Bind("AktiveAPVID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="AktiveAPVID" HeaderText="AktiveAPVID" ItemStyle-Width="10%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                <asp:BoundField DataField="sagsnavn" HeaderText="sagsnavn" ItemStyle-Width="70%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LBtnAktiveAPV" runat="server" Font-Underline="false" OnClick="LBtnAktiveAPV_Click"><span class="menu-item-icon icon-edit-write"></span></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton CssClass="simple" ID="LBtnViewAktiveAPV" runat="server" Font-Underline="false" OnClick="LBtnViewAktiveAPV_Click"><span class="action-icon icon-eye"></span></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                    <ItemTemplate>
                                                                        <span onclick="return confirm('Are you sure to Delete the AktiveAPV?')">
                                                                            <asp:LinkButton CssClass="simple" ID="LBtnDeleteAktiveAPV" runat="server" Font-Underline="false" OnClick="LBtnDeleteAktiveAPV_Click"><span class="menu-item-icon icon-trashcan"></span></asp:LinkButton></span>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="5%" BorderWidth="0" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div runat="server" id="PopUpDiv2" style="height: 880px; width: 82.5%; overflow: auto; visibility: hidden; z-index: 101; background-color: white; position: absolute; left: 17.5%; top: 95px;">
                                        <div style="height: 2650px; width: 10%; float: left;">
                                            <asp:Label ID="lblAktiveAPVID" runat="server" Text="" Visible="false"></asp:Label>
                                        </div>
                                        <div style="height: 2650px; width: 70%; float: left;">
                                            <div id="kj" runat="server" style="width: 100%;">
                                                <div runat="server" id="DivPDF0" style="width: 100%;">

                                                    <%--<div style="height: 50px; width: 100%;">
                                                        <div style="height: 50px; width: 50%; float: left">
                                                            <table style="height: 50px; width: 50%; float: left;">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/image/ActiveAPV.png" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div runat="server" id="ImageData" style="height: 50px; width: 50%; float: right">
                                                            <table style="height: 50px; width: 50%; float: right;">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/image/pmglogo1.jpg" Style="float: right" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>

                                                    </div>--%>

                                                    <div style="height: 70px; width: 100%;">
                                                        <h1 style="text-align: center; font-weight: 900; font-size: xx-large"><span style="color: blue;">AktivAPV</span> </h1>
                                                        <h5 style="text-align: center;"><span style="color: blue;">Arbejdspladsvurdering for bygge og anlæg</span> </h5>
                                                    </div>
                                                    <div style="height: 70px; width: 100%;">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label5" Text="Sagsnr./sagsnavn/arbejdsopgave:" runat="server" Font-Bold="true"></asp:Label>
                                                                    <asp:TextBox ID="txtsagsnavn" runat="server" CssClass="underlined" Width="400" MaxLength="51"> </asp:TextBox>
                                                                    <asp:Label ID="lblsagsnavn" runat="server" Font-Underline="true" Text="" Visible="false"></asp:Label>
                                                                    <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterMode="InvalidChars" InvalidChars=" " TargetControlID="txtsagsnavn">
                                                                    </asp:FilteredTextBoxExtender>--%>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label6" Text="APV-deltagere:" runat="server" Font-Bold="true"></asp:Label>
                                                                    <asp:TextBox ID="txtdeltagere" runat="server" CssClass="underlined" Width="500" MaxLength="69"></asp:TextBox>
                                                                    <asp:Label ID="lbldeltagere" runat="server" Font-Underline="true" Text="" Visible="false"></asp:Label>
                                                                    <%-- <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="InvalidChars" InvalidChars=" " TargetControlID="txtdeltagere">
                                                                    </asp:FilteredTextBoxExtender>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div runat="server" id="DivPDF1" style="height: 350px; width: 100%;">
                                                    <table style="height: 350px; width: 100%;" border="1">
                                                        <tr style="height: 30px;" bgcolor="lightblue">
                                                            <td id="td11" runat="server" colspan="3">
                                                                <h5 style="text-align: center; font-weight: 700">AktiveAPV</h5>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 70px;">
                                                            <td colspan="3">
                                                                <h5>AktivAPV er en måde at gennemføre APV på, ved en samtale mellem ledelse og ansatte. AktivAPV kan udarbejdes på et skurmøde, et sikkerhedsudvalgsmøde eller alle andre steder, hvor ledelse og medarbejdere mødes</h5>
                                                            </td>
                                                        </tr>

                                                        <tr style="height: 160px;">
                                                            <td style="height: 160px;" width="50">
                                                                <table border="1" style="width: 100%">
                                                                    <tr>
                                                                        <td>
                                                                            <h5>
                                                                                <p>AktivAPV gennemført på virksomhedens over-ordnede væsentlige problemer kan f.eks. føre til:</p>
                                                                            </h5>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <h5>
                                                                                <ul style="margin-left: 30px;">
                                                                                    <li>Nye indkøbspolitikker</li>
                                                                                    <li>Ændring af procedurer</li>
                                                                                    <li>Undersøgelse af stoffer</li>
                                                                                    <li>Ændring af instruktioner</li>
                                                                                    <li>Ændring i projektering</li>
                                                                                    <li>Øget samarbejde med bygherre om plan for sikkerhed og sundhed</li>
                                                                                </ul>
                                                                            </h5>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="height: 160px;" width="4%"></td>
                                                            <td style="height: 160px;" width="46">
                                                                <table border="1" style="width: 100%">
                                                                    <tr>
                                                                        <td>
                                                                            <h5>AktivAPV gennemført af det enkelte sjak om forhold på pladsen kan f.eks. føre til: </h5>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <h5>
                                                                                <ul style="margin-left: 30px;">
                                                                                    <li>Løsning af problemer her og nu</li>
                                                                                    <li>Bedre samarbejde på pladsen</li>
                                                                                    <li>Bedre indretning af pladsen</li>
                                                                                    <li>Ændring af rutiner</li>
                                                                                    <li>Brug af løftegrej, værnemidler mm.</li>
                                                                                    <li>Bedre kendskab til arbejdsmiljøforhold</li>
                                                                                </ul>
                                                                            </h5>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div runat="server" id="DivPDF2" style="height: 100px; width: 100%;">
                                                    <table style="height: 80px; width: 100%" border="1">
                                                        <tr style="height: 30px;" bgcolor="lightblue">
                                                            <td width="85%">
                                                                <div style="width: 100%;">
                                                                    <h5 style="font-weight: 700;">
                                                                        <p>Er alle arbejdsprocesser behandlet i BasisInstruktion(er)? </p>
                                                                    </h5>
                                                                </div>
                                                            </td>
                                                            <td width="15%">
                                                                <div style="width: 100%;">
                                                                    <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true" />
                                                                        JA
                                                            <asp:CheckBox ID="CheckBox2" runat="server" OnCheckedChanged="CheckBox2_CheckedChanged" AutoPostBack="true" />

                                                                    </h5>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5 style="font-weight: 700">De BasisInstruktioner, der behandler arbejdsprocesserne, skal vedlægges AktivAPV medmindre alt relevant er overført til AktivAPV.<br />
                                                                    De arbejdsprocesser, der ikke er behandlet i BasisInstruktion, skal behandles i AktivAPV.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div runat="server" id="DivPDF3" style="height: 350px; width: 100%;">
                                                    <table style="width: 100%;" border="1">

                                                        <tr style="height: 20px;">
                                                            <td bgcolor="lightblue">
                                                                <h5 style="text-align: center; font-weight: 700">Vejledning</h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h5>
                                                                    <p>
                                                                        Formålet med APV er at forbedre arbejdsmiljøet. Arbejdsgiveren skal sørge for, at forbedringer foregår efter de 9 forebyggelsesprincipper. Alle væsentlige problemer skal tages med, og APV’en skal være skriftlig. Sikkerhedsorganisationen eller repræsentanter for de ansatte skal deltage. Deltagernes navne skal skrives på forsiden og handlingsplanen underskrives.
                                                               <br />
                                                                        APV’en skal indeholde 4 elementer:
                                                                    <ul style="list-style-type: disc; margin-left: 40px;">
                                                                        <li>Identifikation og kortlægning af virksomhedens arbejdsmiljøforhold - hvilke problemer har vi? </li>
                                                                        <li>Beskrivelse og vurdering af virksomhedens arbejdsmiljøproblemer - hvad er årsagerne til problemerne?</li>
                                                                        <li>Prioritering og opstilling af handlingsplan – hvordan og hvornår løses problemerne?</li>
                                                                        <li>Retningslinier for opfølgning på handlingsplanen – virker det? Har tiltagene den ønskede effekt? Hvordan kommer vi videre?</li>
                                                                    </ul>
                                                                        <h5>APV’en skal være tilgængelig for de ansatte. APV’en skal fornyes, hvis der sker ændringer i arbejdsopgaver, arbejdsmetoder eller arbejdsbetingelser. Det kan således være nødvendigt at lave en APV på byggepladsen, hvis der er særlige forhold på den aktuelle plads, der ikke tidligere er kortlagt ved APV - dog skal APV’en fornyes mindst hvert 3. år. Mangelfuld APV kan udløse påbud og eventuelt rød smiley.</h5>
                                                                    </p>
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div runat="server" id="DivPDF4" style="width: 100%;">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td colspan="2">
                                                                <h4 style="font-weight: 700">APV kortlægning
                                                                </h4>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>
                                                                    <span>Svares JA på et eller flere af de følgende spørgsmål, skal problemet inddrages i handlingsplanen på bagsiden af dette skema.
                                                                    </span></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr bgcolor="lightblue">
                                                            <td width="85%">
                                                                <h5 style="font-weight: 700;">1.Er du udsat for skadelig støj? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td width="15%">
                                                                <h5 style="font-weight: 700">NEJ 
                                                                <asp:CheckBox ID="CheckBox3" runat="server" OnCheckedChanged="CheckBox3_CheckedChanged" AutoPostBack="true" Height="2px" />
                                                                    JA<asp:CheckBox ID="CheckBox4" runat="server" OnCheckedChanged="CheckBox4_CheckedChanged" AutoPostBack="true" Height="2px" Width="2px" />
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Støjniveauet er for højt, hvis man pga. støjen ikke kan føre en samtale med normal stemmeføring eller hvis støjen består af høje impulser (slag, skud og lignende). Overvej om støjen kan fjernes eller dæmpes ved f.eks. anden teknik (f.eks. præfabrikerede elementer), planlægning (f.eks. forskudt arbejdstid) maskine, værktøj eller bedre vedligeholdelse o.a.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>

                                                        <tr bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">2.Er du udsat for skadelige vibrationer? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700">NEJ 
                                                            <asp:CheckBox ID="CheckBox5" runat="server" OnCheckedChanged="CheckBox5_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox6" runat="server" OnCheckedChanged="CheckBox6_CheckedChanged" AutoPostBack="true" />
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Tænk på værktøj, der vibrerer og som på længere sigt kan give ”hvide fingre”. Tænk også på maskiner, der påvirker hele kroppen. Overvej om arbejdstiden med de pågældende maskiner bør begrænses.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">3.Er du udsat for skadeligt støv, røg eller gasser? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700">NEJ 
                                                            <asp:CheckBox ID="CheckBox7" runat="server" OnCheckedChanged="CheckBox7_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox8" runat="server" OnCheckedChanged="CheckBox8_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Støvmængden er helt sikkert for stor, hvis man umiddelbart kan se støvet i luften. Overvej om støvmængden kan undgås eller reduceres - f.eks. ved anden planlægning, arbejdsproces eller brug af sug på værktøj. Tænk desuden på isoleringsmaterialer, svejserøg, udstødningsgasser og/eller farlige luftarter - f.eks. ved arbejde i brønde.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>

                                                        <tr bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">4.Er der risiko for ulykker? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700">NEJ 
                                                            <asp:CheckBox ID="CheckBox9" runat="server" OnCheckedChanged="CheckBox9_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox10" runat="server" OnCheckedChanged="CheckBox10_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Overvej om der er risiko for ulykker - f.eks. fordi opgaven er kompliceret, uhåndterlig eller svær at overskue. Tænk f.eks. på: fald, farlige maskiner, skarpe genstande, el, elementmontage, varmt arbejde eller brand. Overvej om risikoen f.eks. kan fjernes ved anden planlægning, brug af tekniske hjælpemidler eller oprydning. </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>

                                                        <tr bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">5. Er du udsat for overbelastning/nedslidning af kroppen? NEJ JA (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox11" runat="server" OnCheckedChanged="CheckBox11_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox12" runat="server" OnCheckedChanged="CheckBox12_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Overvej om tunge og uhåndterlige løft kan undgås f.eks. ved anden planlægning eller brug af løfte- og transportgrej. Hvis der skal bæres mere end 20 kg, bør man altid finde et teknisk hjælpemiddel, der kan erstatte den manuelle transport. Tænk på om arbejdsstillinger kan varieres, så der f.eks. ikke arbejdes på knæ eller over skulderhøjde i længere tid ad gangen. Er belysningen god? Tænk desuden på om unødig kulde og træk kan undgås.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>

                                                        <tr bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">6. Er du udsat for smitterisiko? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox13" runat="server" OnCheckedChanged="CheckBox13_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox14" runat="server" OnCheckedChanged="CheckBox14_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Du kan være udsat for smitterisiko f.eks. ved kontakt med duemøg, arbejde i kloaker eller kontakt med brugte kanyler.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>

                                                        <tr bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">7. Er der problemer med trivsel og samarbejde? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox15" runat="server" OnCheckedChanged="CheckBox15_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox16" runat="server" OnCheckedChanged="CheckBox16_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Problemer med trivsel og samarbejde kan opstå pga. stress, manglende information, uklare krav eller forholdet til kolleger, mobning eller ledelse. Trivsel kan også handle om f.eks. rygepolitik.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>


                                                        <tr bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">8. Mangler du instruktion, uddannelse eller værnemidler? NEJ JA (Hvis ja, udfyld handlingsplan) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox17" runat="server" OnCheckedChanged="CheckBox17_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox18" runat="server" OnCheckedChanged="CheckBox18_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Overvej om du kender de påvirkninger, du kan blive udsat for ved arbejdet, og om du ved, hvordan du bedst undgår skader og ulykker. Er de korrekte værnemidler tilstede? Vær opmærksom på at der gælder særlige regler for unges arbejde.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>

                                                        <tr bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">9. Er velfærdsforanstaltningerne mangelfulde? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox19" runat="server" OnCheckedChanged="CheckBox19_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox20" runat="server" OnCheckedChanged="CheckBox20_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 2px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Overvej f.eks. om de fornødne faciliteter er til stede, og om de er tilstrækkeligt renholdte.</h5>
                                                                <h5>.</h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 20px;" colspan="2"></td>
                                                        </tr>

                                                    </table>

                                                    <table style="width: 100%;">

                                                        <tr>
                                                            <td colspan="2">
                                                                <span style="font-size: 14px; font-weight: 700;">APV kortlægning (forsat)
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Svares JA på et eller flere af de følgende spørgsmål, skal problemet inddrages i handlingsplanen på bagsiden af dette skema:
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>
                                                        <tr style="height: 20px;" bgcolor="lightblue">
                                                            <td width="85%">
                                                                <h5 style="font-weight: 700;">10. Er der kemiske stoffer/produkter, som giver anledning til gener/problemer?(Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td width="15%">
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox21" runat="server" OnCheckedChanged="CheckBox21_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox22" runat="server" OnCheckedChanged="CheckBox22_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Udsættes I for påvirkninger fra egne eller andre håndværkeres brug af kemiske produkter?
                                                            Overvej altid om arbejdsgangen kan ændres, så påvirkningen bliver mindre, eller om der er nogen kemiske produkter, der kan erstattes med mindre sundhedsskadelige produkter, end dem I benytter.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>

                                                        <tr style="height: 30px" bgcolor="lightblue">
                                                            <td colspan="2">
                                                                <h5 style="font-weight: 700;">11. Alle stoffer og materialer skal risikovurderes jvf. AT-bekendtgørelse nr. 292 af 26.04.01 </h5>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Dette gøres ved at udarbejde arbejdspladsbrugsanvisninger (f.eks. trukket fra Dansk Kemidatabase) på alle produkter. Arbejdspladsbrugsanvisningen udarbejdes på baggrund af Leverandør-brugsanvisningen, og skal være tilgængelig på byggepladsen/arbejdspladsen. Der skal endvidere være en liste/indholdsfortegnelse over disse produkter.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <%-- <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>--%>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Svares der JA til et af de følgende spørgsmål udfyldes handlingsplanen på bagsiden.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table border="1" style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 16%">A</td>
                                                                        <td style="width: 16%">B</td>
                                                                        <td style="width: 16%">C</td>
                                                                        <td style="width: 16%">D</td>
                                                                        <td style="width: 16%">E</td>
                                                                        <td style="width: 16%">F</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 16%">
                                                                            <h5>Mangler der arbejdsplads-brugsanvisninger på de anvendte produkter og materialer?
                                                                            </h5>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h5>Mangler der nødvendige værnemidler?
                                                                            </h5>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h5>Savnes der instruktion?
                                                                            </h5>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h5>Mangler den nødvendige uddannelse eller kurser (f.eks. epoxy, isocyanat, styren)? 
                                                                                <h5>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h5>Mangler man at tage hensyn til andre ved arbejdet med produkterne?
                                                                            </h5>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h5>Mangler man at tage højde for bortskaffelse af alt affald?
                                                                            </h5>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 16%;">
                                                                            <h6>NEJ 
                                                            <asp:CheckBox ID="CheckBox23" runat="server" OnCheckedChanged="CheckBox23_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox24" runat="server" OnCheckedChanged="CheckBox24_CheckedChanged" AutoPostBack="true" />
                                                                            </h6>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h6>NEJ 
                                                            <asp:CheckBox ID="CheckBox25" runat="server" OnCheckedChanged="CheckBox25_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox26" runat="server" OnCheckedChanged="CheckBox26_CheckedChanged" AutoPostBack="true" />
                                                                            </h6>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h6>NEJ 
                                                            <asp:CheckBox ID="CheckBox27" runat="server" OnCheckedChanged="CheckBox27_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox28" runat="server" OnCheckedChanged="CheckBox28_CheckedChanged" AutoPostBack="true" />
                                                                            </h6>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h6>NEJ 
                                                            <asp:CheckBox ID="CheckBox29" runat="server" OnCheckedChanged="CheckBox29_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox30" runat="server" OnCheckedChanged="CheckBox30_CheckedChanged" AutoPostBack="true" />
                                                                            </h6>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h6>NEJ 
                                                            <asp:CheckBox ID="CheckBox31" runat="server" OnCheckedChanged="CheckBox31_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox32" runat="server" OnCheckedChanged="CheckBox32_CheckedChanged" AutoPostBack="true" />
                                                                            </h6>
                                                                        </td>
                                                                        <td style="width: 16%">
                                                                            <h6>NEJ 
                                                            <asp:CheckBox ID="CheckBox33" runat="server" OnCheckedChanged="CheckBox33_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox34" runat="server" OnCheckedChanged="CheckBox34_CheckedChanged" AutoPostBack="true" />
                                                                            </h6>
                                                                        </td>

                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="height: 20px;" colspan="2"></td>
                                                        </tr>

                                                        <tr style="height: 20px;" bgcolor="lightblue">
                                                            <td>
                                                                <h5 style="font-weight: 700;">12. Er der maskiner, tekniske hjælpemidler og/eller løftegrej, som giver anledning til gener eller problemer?
                                                         (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td>
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox35" runat="server" OnCheckedChanged="CheckBox35_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox36" runat="server" OnCheckedChanged="CheckBox36_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Tænk f.eks. på om tekniske hjælpemidler altid er til rådighed ved behov, om der tages hensyn til arbejdsmiljø ved indkøb, og om de tekniske hjælpemidler vedligeholdes effektivt og forskriftsmæssigt.

                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Svares der JA til et af de følgende spørgsmål udfyldes handlingsplanen på bagsiden.
                                                                </h5>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <table border="1" style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 25%">A</td>
                                                                        <td style="width: 25%">B</td>
                                                                        <td style="width: 25%">C</td>
                                                                        <td style="width: 25%">D</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 25%">
                                                                            <h5>Mangler der brugsanvisninger til maskiner?
                                                                            </h5>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <h5>Mangler de nødvendige eftersyn at blive udført?
                                                                            </h5>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <h5>Mangler I at få de nødvendige uddannelser eller kurser?
                                                                            </h5>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <h5>Savner I instruktion?
                                                                            </h5>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 25%">
                                                                            <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox37" runat="server" OnCheckedChanged="CheckBox37_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox38" runat="server" OnCheckedChanged="CheckBox38_CheckedChanged" AutoPostBack="true" /></h5>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox39" runat="server" OnCheckedChanged="CheckBox39_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox40" runat="server" OnCheckedChanged="CheckBox40_CheckedChanged" AutoPostBack="true" /></h5>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox41" runat="server" OnCheckedChanged="CheckBox41_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox42" runat="server" OnCheckedChanged="CheckBox42_CheckedChanged" AutoPostBack="true" /></h5>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox43" runat="server" OnCheckedChanged="CheckBox43_CheckedChanged" AutoPostBack="true" />
                                                                                JA
                                                            <asp:CheckBox ID="CheckBox44" runat="server" OnCheckedChanged="CheckBox44_CheckedChanged" AutoPostBack="true" /></h5>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="height: 20px;" colspan="2"></td>
                                                        </tr>

                                                        <tr style="height: 20px;" bgcolor="lightblue">
                                                            <td style="width: 81%;">
                                                                <h5 style="font-weight: 700;">13. Er der arbejdsmiljørelateret sygefravær? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td style="width: 19%;">
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox45" runat="server" OnCheckedChanged="CheckBox45_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox46" runat="server" OnCheckedChanged="CheckBox46_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Manglende trivsel og dårligt samarbejde kan være årsag til stort sygefravær. Et hårdt belastende arbejdsmiljø med ulykker og nedslidning kan også være medvirkende til et stort sygefravær. Hvis sygefravær skyldes arbejdsforholdene bør det overvejes, hvordan dette fremover kan forebygges.
                                                               <h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>

                                                        <tr style="height: 20px;" bgcolor="lightblue">
                                                            <td style="width: 81%;">
                                                                <h5 style="font-weight: 700;">14. Andre forhold? (Hvis ja, udfyld handlingsplanen) </h5>
                                                            </td>
                                                            <td style="width: 19%;">
                                                                <h5 style="font-weight: 700;">NEJ 
                                                            <asp:CheckBox ID="CheckBox47" runat="server" OnCheckedChanged="CheckBox47_CheckedChanged" AutoPostBack="true" />
                                                                    JA
                                                            <asp:CheckBox ID="CheckBox48" runat="server" OnCheckedChanged="CheckBox48_CheckedChanged" AutoPostBack="true" /></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <h5>Her kan registreres andre forhold af betydning for arbejdsmiljøet, der evt. ikke er taget højde for i de forudgående punkter.
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                        <div style="height: 2650px; width: 20%; float: right;">
                                            <asp:Button ID="BtnSaveAsHtml" runat="server" Text="HTML" CssClass="buttonn" OnClick="BtnSaveAsHtml_Click" Visible="false" />
                                            <asp:Button CssClass="buttonn" ID="BtnExportToDoc" runat="server" Text="Export" OnClick="BtnExportToDoc_Click" Visible="false" />

                                            <span class="menu-item-icon icon-times-circle-o" style="float: right" onclick="ShowDiv2('PopUpDiv2')"></span>
                                            <asp:Button ID="btnPrintAktiveAPV" CssClass="buttonn" runat="server" Text="Print" Style="float: right" OnClick="btnPrintAktiveAPV_Click" />
                                            <asp:Button ID="btnActivepdf" CssClass="buttonn" runat="server" Text="PDF" Style="float: right" OnClick="btnActivepdf_Click" Visible="false" />
                                            <asp:Button CssClass="buttonn" ID="BtnUpdateActiveAPV" runat="server" Text="Update" Style="float: right" OnClick="BtnUpdateActiveAPV_Click" Visible="false" />
                                            <asp:Button CssClass="buttonn" ID="BtnSaveActiveAPV" runat="server" Text="Save" Style="float: right" OnClick="BtnSaveActiveAPV_Click" Visible="false" />
                                        </div>
                                        <div style="height: 850px; width: 100%; float: left; font-size: 14px">
                                            <div style="height: 20px; width: 100%;"></div>
                                            <div style="height: 800px; width: 5%; float: left"></div>
                                            <div runat="server" id="DivHoriPDF" style="height: 800px; width: 95%; float: left;">

                                                <div style="height: 50px; width: 100%;">
                                                    <%-- <asp:Image ID="Image3" runat="server" ImageUrl="~/image/ActiveAPV.png" />--%>
                                                </div>
                                                <div style="height: 10px; width: 100%;"></div>
                                                <div style="height: 600px; width: 100%;">
                                                    <div id="DivHoriPDF1" runat="server">
                                                        <h5 style="text-underline-position: below; font-weight: 700;">Handlingsplan </h5>
                                                        <h5>Handlingsplanen udarbejdes i samarbejde mellem ledelse og ansatte. Handlingsplanen skal være prioriteret med tidsfrist, så det fremgår, hvornår problemerne løses. Der kan f.eks. prioriteres efter problemets alvor, men også efter hvor let det løses. Vær opmærksom på at de løsninger som igangsættes, skal ske under hensyn til arbejdsmiljølovens forebyggelsesprincipper om altid at benytte bedst mulige løsning.</h5>

                                                    </div>
                                                    <div id="DivHoriPDF2" runat="server">
                                                        <table style="height: 500px; width: 100%;" border="1">
                                                            <tr style="height: 10px;">
                                                                <td style="font-size: 13px;" width="05%">
                                                                    <h5>Punkt nr.: </h5>
                                                                </td>
                                                                <td style="font-size: 13px;" width="25%">
                                                                    <h5>Beskriv problemet: </h5>
                                                                </td>
                                                                <td style="font-size: 13px;" width="40%">
                                                                    <h5>Beskriv den forebyggende handling eller problemløsning: </h5>
                                                                </td>
                                                                <td style="font-size: 13px;" width="10%">
                                                                    <h5>Iværksættes af: </h5>
                                                                </td>
                                                                <td style="font-size: 13px;" width="10%">
                                                                    <h5>Prioriteret tidsfrist for løsning: </h5>
                                                                </td>
                                                                <td style="font-size: 13px;" width="10%">
                                                                    <h5>Evaluering. Fungerer løsningen? </h5>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="05%">
                                                                    <asp:Label ID="LabelPunkt_nr" Text=" " runat="server" Font-Size="12px"></asp:Label>
                                                                    <asp:TextBox ID="TextBoxPunkt_nr" runat="server" Height="99%" TextMode="MultiLine" BorderStyle="None" CssClass="underlined1"></asp:TextBox>
                                                                </td>
                                                                <td width="25%">
                                                                    <asp:Label ID="LabelBeskriv_problemet" Text=" " runat="server" Font-Size="12px"></asp:Label>
                                                                    <asp:TextBox ID="TextBoxBeskriv_problemet" runat="server" Height="99%" TextMode="MultiLine" Width="100%" BorderStyle="None" CssClass="underlined1"></asp:TextBox>
                                                                </td>
                                                                <td width="40%">
                                                                    <asp:Label ID="LabelBeskriv_den" Text=" " runat="server" Font-Size="12px"></asp:Label>
                                                                    <asp:TextBox ID="TextBoxBeskriv_den" runat="server" Height="99%" TextMode="MultiLine" Width="100%" BorderStyle="None" CssClass="underlined1"></asp:TextBox>
                                                                </td>
                                                                <td width="10%">
                                                                    <asp:Label ID="LabelIværksættes" Text=" " runat="server" Font-Size="12px"></asp:Label>
                                                                    <asp:TextBox ID="TextBoxIværksættes" runat="server" Height="99%" TextMode="MultiLine" Width="100%" BorderStyle="None" CssClass="underlined1"></asp:TextBox>
                                                                </td>
                                                                <td width="10%">
                                                                    <asp:Label ID="LabelPrioriteret" Text=" " runat="server" Font-Size="12px"></asp:Label>
                                                                    <asp:TextBox ID="TextBoxPrioriteret" runat="server" Height="99%" TextMode="MultiLine" Width="100%" BorderStyle="None" CssClass="underlined1"></asp:TextBox>
                                                                </td>
                                                                <td width="10%">
                                                                    <asp:Label ID="LabelEvaluering" Text=" " runat="server" Font-Size="12px"></asp:Label>
                                                                    <asp:TextBox ID="TextBoxEvaluering" runat="server" Height="99%" TextMode="MultiLine" Width="100%" BorderStyle="None" CssClass="underlined1"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="DivHoriPDF3" runat="server">
                                                        <h5>Man skal ved valg af løsning bruge arbejdsmiljølovens ni forebyggelsesprincipper for at opnå det højeste (bedste) forebyggelsesniveau.
                                                        </h5>

                                                        <div style="height: 10px; width: 100%;"></div>

                                                        <table style="height: 130px; width: 100%;">
                                                            <tr>
                                                                <td style="width: 30%">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Forhindring af risici </h5>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Evaluering af risici, som ikke kan forhindres </h5>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Bekæmpelse af risici ved kilden </h5>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Tilpasning af arbejdet til mennesket </h5>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Hensyntagen til den tekniske udvikling </h5>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Udskiftning til noget mindre farligt </h5>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Helhedsplanlægning af forebyggelsen </h5>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Kollektiv frem for individuel beskyttelse </h5>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>- Hensigtsmæssig instruktion af medarbejderne </h5>
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <h5>Dato:__________________________________________ </h5>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>

                                                                            <td>
                                                                                <h5>For ledelsen:____________________________________ </h5>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>
                                                                                <h5>For medarbejderne/ Sio:___________________________ </h5>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="pop1" class="parentDisable" runat="server"></div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </form>
</body>
</html>
