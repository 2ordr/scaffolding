﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectDetails.aspx.cs" Inherits="ProjectManagement.ProjectDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>

    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style>
        .main-panel-with-title > .x-panel-header .x-panel-header-text-container-default {
            font-weight: normal;
            color: #383838;
            padding-left: 20px;
            font-size: 182%;
            line-height: inherit;
        }


        headform1 {
            color: #383838;
        }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }
    </style>


    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>
    <script type="text/javascript">   //for selecting only single checkbox for row
        function CheckSingleCheckbox(ob) {
            var grid = ob.parentNode.parentNode.parentNode;

            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (ob.checked && inputs[i] != ob && inputs[i].checked) {
                        inputs[i].checked = false;

                    }
                }
            }

        }

    </script>

     <script type="text/javascript">

         function ShowDiv(id) {
             var e = document.getElementById(id);
             if (e.style.display == 'none')
                 e.style.display = 'block';

             else
                 e.style.display = 'none';

             return false;
         }
    </script>
</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false" EnablePartialRendering="true"></asp:ToolkitScriptManager>

        <div style="height: 1000px; width: 1920px;">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 1000px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 1000px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox1" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150"></asp:TextBox>


                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 1000px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Dashboard</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Customer</span></a></li>
                                                 <%--<li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Quatation</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Order</span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-7" data-index="7"><a class="menu-link" href="View Project.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projects</span></a></li>


                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Setting</span><%--<span class="menu-toggle"></span>--%></span>
                                                   <div id="hide" runat="server" style="display: none">  <ul class="menu-group" id="ext-gen3448">

                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>
                                                    </ul></div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                 <li class="menu-item menu-app-item app-item" id="menu-item-15" data-index="135"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Salary</span></a></li>

                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">My Tasks</span></a></li>



                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>--%>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div style="height: 300px; border-color: White;">
                                    <table style="height: 300px; width: 100%">
                                        <tr style="height: 100px;">
                                            <td></td>
                                        </tr>
                                        <tr style="height: 50px;">
                                            <td>
                                                <div>&nbsp
                                                    <asp:Label ID="Label2" runat="server" Text="Timer" ForeColor="White"></asp:Label>
                                                </div>
                                                <div>

                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                                                        <ContentTemplate>&nbsp

                                                            <asp:Label ID="lbldisplayTime" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="white">00:00:00</asp:Label>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                                        </Triggers>

                                                    </asp:UpdatePanel>

                                                    <asp:Button ID="BtnStarttime" runat="server" Text="Start" OnClick="BtnStarttime_Click" CssClass="button" Width="40" Height="30" BackColor="#009900" />
                                                    <asp:Button ID="BtnStopttime" runat="server" Text="stop" OnClick="BtnStopttime_Click" CssClass="button" Width="40" Height="30" BackColor="#009900" />

                                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txtHrs" runat="server" Height="30" Width="70"></asp:TextBox>
                                                    <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick"></asp:Timer>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 100px;">
                                            <td>
                                                <ul>
                                                    <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                        <asp:Label ID="Label1" runat="server" Text="" ForeColor="White"></asp:Label>
                                                    </span></a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <div class="allSides" style="width: 1725px; float: left; height: 80px;">
                    <div style="height: 10px;">
                    </div>
                    <table style="width: 1725px; font-family: Calibri;">
                        <tbody>
                            <tr>
                                <td style="width: 1500px;">

                                    <asp:Label ID="lblProjectName" runat="server" Text="" Font-Bold="true" Font-Size="Large"></asp:Label>
                                    <asp:Label ID="Label5" runat="server" Text="Label" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label>&nbsp &nbsp &nbsp &nbsp  &nbsp  
                                                <asp:Label ID="Label6" runat="server" Text="Label" Font-Size="Large" ForeColor="black" Visible="false"></asp:Label><br />
                                    (Expected Completion Date):
                                                <asp:Label ID="lblexpectedCDate" runat="server" Text=""></asp:Label>
                                </td>
                                <%-- <td style="width: 10%;">
                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="txttransfermode" runat="server" Width="200" Height="35"></asp:TextBox>
                                </td>--%>
                                <td style="width: 350px; float: right">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:DropDownList CssClass="search_categories" ID="Ddwnlistsalarypaid" runat="server" Height="35" Width="100">
                                                    <asp:ListItem>Salary Paid</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>No</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList CssClass="search_categories" ID="Ddwnlisttransfermode" runat="server" Height="35" Width="100" OnSelectedIndexChanged="Ddwnlisttransfermode_SelectedIndexChanged">
                                                    <asp:ListItem>Transfer Mode</asp:ListItem>
                                                    <asp:ListItem>Cheque</asp:ListItem>
                                                    <asp:ListItem>Bank A/C:</asp:ListItem>
                                                    <asp:ListItem>Cash</asp:ListItem>
                                                </asp:DropDownList><br />
                                            </td>

                                            <td>
                                                <asp:Button CssClass="button" ID="btnback" runat="server" Text="Back" Width="50" Height="35" OnClick="btnback_Click" BackColor="#009900" />
                                            </td>
                                            <td>
                                                <asp:Button CssClass="button" ID="BtnSave" runat="server" Text="Save" Width="50" Height="35" OnClick="BtnSave_Click" BackColor="#009900" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style="width: 100%; float: left; height: 900px;">
                    <table style="width: 100%; font-family: Calibri;">
                        <tr>
                            <td>

                                <asp:GridView ID="grdviewprojectdetails" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" BorderWidth="0px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="10px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="20px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                                <asp:Label ID="PROJECTID" runat="server" Text='<%# Bind("ProjectId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="EmpID" runat="server" Text='<%# Bind("EmpId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="LblTaskId" runat="server" Text='<%# Bind("TaskId") %>' Visible="false"></asp:Label>
                                                  <asp:Label ID="LblSTID" runat="server" Text='<%# Bind("STID") %>' Visible="false"></asp:Label>
                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Visible="true" onclick="CheckSingleCheckbox(this)" OnCheckedChanged="chkSelect_CheckedChanged" /><%--onclick="CheckSingleCheckbox(this)"--%>
                                            </ItemTemplate>
                                            <ItemStyle Width="20px" BackColor="White" BorderWidth="0" />
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="TaskId" HeaderText="TaskId" HeaderStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />
                                        <asp:BoundField DataField="Task" HeaderText="Task" HeaderStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />
                                        <asp:BoundField DataField="AssignedDate" HeaderText="AssignedDate" HeaderStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />
                                        <asp:BoundField DataField="WagePerHr" HeaderText="WagePerHr" HeaderStyle-Width="100px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" HeaderStyle-BorderWidth="5px" />--%>

                                        <asp:TemplateField HeaderText="User Name" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                                <asp:Label ID="LblEmpName" runat="server" Text='<%# Bind("Fname") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Task" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                              <%--  <asp:Label ID="LblTask" runat="server" Text='<%# Bind("Task") %>' Visible="true"></asp:Label>--%>
                                                  <asp:Label ID="LblTask" runat="server" Text='<%# Bind("SubTask") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="AssignedDate" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                                <asp:Label ID="LblAssignedDate" runat="server" Text='<%# Bind("AssignedDate") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ExpectedHours" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                                <asp:Label ID="LblExpectedHours" runat="server" Text='<%# Bind("ExpectedHours") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="HoursWorked" HeaderStyle-Width="50px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtHoursWorked" CssClass="twitterStyleTextbox" Height="30" Width="100" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" BorderWidth="5" BorderColor="White" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="WagePerHr" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                                <asp:Label ID="LblWagePerHr" runat="server" Text='<%# Bind("WagePerHr") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                                <asp:DropDownList CssClass="search_categories" ID="DdlTaskStatus" runat="server" Width="200">
                                                    <asp:ListItem>Open</asp:ListItem>
                                                    <asp:ListItem>Working</asp:ListItem>
                                                    <asp:ListItem>On Hold</asp:ListItem>
                                                    <asp:ListItem>Waiting</asp:ListItem>
                                                    <asp:ListItem>Done</asp:ListItem>
                                                    <asp:ListItem>Closed </asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Totalsalary" HeaderStyle-Width="100px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTotalSalary" CssClass="twitterStyleTextbox" Height="30" Width="100" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" BorderWidth="5" BorderColor="White" />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
