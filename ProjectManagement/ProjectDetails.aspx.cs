﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Sql;

namespace ProjectManagement
{
    public partial class ProjectDetails : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //txttransfermode.Visible = false;
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    BtnStopttime.Visible = false;
                    Label1.Text = Session["username"].ToString();
                    BindProject();

                }
            }
        }
        public void BindProject()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_ViewProject";

            cmd.Parameters.Add("@PROJECTID", System.Data.SqlDbType.NVarChar).Value = Request.QueryString["Projectid"];
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                grdviewprojectdetails.DataSource = dt;
                grdviewprojectdetails.DataBind();

                lblProjectName.Text = dt.Tables[0].Rows[0]["ProjectName"].ToString();
                lblexpectedCDate.Text = dt.Tables[0].Rows[0]["CompletionDate"].ToString();
                //txtName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                //txtResTime.Text = dt.Tables[0].Rows[0]["RespondTime"].ToString();
                //TxtResCost.Text = dt.Tables[0].Rows[0]["TotalCost"].ToString();
                //ddlCustAck.Text = dt.Tables[0].Rows[0]["CustAck"].ToString();
                //ddlAdminApp.Text = dt.Tables[0].Rows[0]["AdminApproval"].ToString();
                //ddquotestatus.Text = dt.Tables[0].Rows[0]["Status"].ToString();
                //lblName.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
            }
            else
            {
                Response.Redirect("View Project.aspx?");
            }
            con.Close();
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("View Project.aspx");
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            lbldisplayTime.Text = DateTime.Now.ToString();
        }

        protected void BtnStarttime_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            BtnStarttime.Visible = false;
            BtnStopttime.Visible = true;
            Label5.Text = lbldisplayTime.Text;
        }

        protected void BtnStopttime_Click(object sender, EventArgs e)
        {
            //Timer1.Enabled = false;
            BtnStarttime.Visible = true;
            BtnStopttime.Visible = false;
            Label6.Text = lbldisplayTime.Text;

            DateTime inTime = Convert.ToDateTime(Label5.Text);
            DateTime outTime = Convert.ToDateTime(Label6.Text);
            TimeSpan i = outTime.Subtract(inTime);
            this.txtHrs.Text = i.ToString();


            foreach (GridViewRow item in grdviewprojectdetails.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chk = (CheckBox)(item.Cells[0].FindControl("chkSelect"));
                    if (chk.Checked)
                    {
                        TextBox txtHoursWorked = ((TextBox)item.Cells[5].FindControl("txtHoursWorked"));
                        txtHoursWorked.Text = txtHrs.Text;

                        Label LblWagePerHr = ((Label)item.Cells[6].FindControl("LblWagePerHr"));
                        decimal trr = Convert.ToDecimal(LblWagePerHr.Text);

                        TextBox txtTotalSalary = ((TextBox)item.Cells[8].FindControl("txtTotalSalary"));

                        decimal dec = Convert.ToDecimal(TimeSpan.Parse((txtHoursWorked.Text)).TotalHours);

                        // txtTotaloursWorked.Text = ((Convert.ToInt32(dec)) * (Convert.ToInt32(LblWagePerHr.Text))).ToString();
                        decimal total = ((Convert.ToDecimal(dec)) * (Convert.ToDecimal(trr)));

                        txtTotalSalary.Text = total.ToString();

                    }
                }
            }
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in grdviewprojectdetails.Rows)
            {
                CheckBox chk = (CheckBox)row.Cells[0].FindControl("chkSelect");
                if (chk.Checked)
                {
                    row.BackColor = System.Drawing.ColorTranslator.FromHtml("#7FFFD4");

                    System.Web.UI.WebControls.Label EmpId = (row.Cells[0].FindControl("EmpId") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtHoursWorked = (row.Cells[5].FindControl("txtHoursWorked") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.TextBox txtTotalSalary = (row.Cells[8].FindControl("txtTotalSalary") as System.Web.UI.WebControls.TextBox);
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();

                    SqlCommand com2 = new SqlCommand("select EMPID from EMPLOYEESALARYDETAILS where EMPID=" + EmpId.Text + "", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(com2);
                    DataTable dt = new DataTable();
                    adpt.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        com2.CommandText = "select TOTALSALARY,TOTALHOURS from EMPLOYEESALARYDETAILS where EMPID=" + EmpId.Text + "";
                        SqlDataAdapter adpt1 = new SqlDataAdapter(com2);
                        DataSet ds1 = new DataSet();
                        adpt1.Fill(ds1, "EMPLOYEESALARYDETAILS");
                        txtHoursWorked.Text = ds1.Tables["EMPLOYEESALARYDETAILS"].Rows[0]["TOTALHOURS"].ToString();
                        txtTotalSalary.Text = ds1.Tables["EMPLOYEESALARYDETAILS"].Rows[0]["TOTALSALARY"].ToString();
                    }
                }
                else
                {
                    row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F2F2");
                }
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            foreach (GridViewRow rows in grdviewprojectdetails.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chk = (CheckBox)(rows.Cells[0].FindControl("chkSelect"));
                    if (chk.Checked)
                    {
                        System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                        System.Web.UI.WebControls.Label PROJECTID = (rows.Cells[0].FindControl("ProjectId") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label EmpId = (rows.Cells[0].FindControl("EmpId") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label TaskId = (rows.Cells[0].FindControl("LblTaskId") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label LblSTID = (rows.Cells[0].FindControl("LblSTID") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label LblEmpName = (rows.Cells[1].FindControl("LblEmpName") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label Task = (rows.Cells[2].FindControl("LblTask") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label AssignedDate = (rows.Cells[3].FindControl("LblAssignedDate") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label ExpectedHours = (rows.Cells[4].FindControl("ExpectedHours") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.TextBox txtHoursWorked = (rows.Cells[5].FindControl("txtHoursWorked") as System.Web.UI.WebControls.TextBox);
                        System.Web.UI.WebControls.Label WagePerHr = (rows.Cells[6].FindControl("LblWagePerHr") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.DropDownList DdlTaskStatus = (rows.Cells[7].FindControl("DdlTaskStatus") as System.Web.UI.WebControls.DropDownList);
                        System.Web.UI.WebControls.TextBox Totalsalary = (rows.Cells[8].FindControl("txtTotalSalary") as System.Web.UI.WebControls.TextBox);

                        if (txtHoursWorked.Text != string.Empty)
                        {
                            if (Label5.Text == "Label")
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Click On Start');", true);
                                return;
                            }
                            else if (Label6.Text == "Label")
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Click On Stop');", true);
                                return;
                            }
                            else
                            {
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.CommandText = "sp_InsertHoursWorked";
                                cmd.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = PROJECTID.Text;
                                cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = EmpId.Text;
                                cmd.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = TaskId.Text;

                                cmd.Parameters.Add("@STARTDATETIME", System.Data.SqlDbType.DateTime).Value = Label5.Text;
                                cmd.Parameters.Add("@COMPLETIONDATETIME", System.Data.SqlDbType.DateTime).Value = Label6.Text;
                                cmd.Parameters.Add("@HOURSWORKED", System.Data.SqlDbType.Time).Value = txtHoursWorked.Text;
                                cmd.Parameters.Add("@TOTALWAGE", System.Data.SqlDbType.Decimal).Value = Totalsalary.Text;
                                cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.NVarChar).Value = DdlTaskStatus.SelectedValue;
                                cmd.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = LblSTID.Text;

                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();

                            }
                        }

                        if (Totalsalary.Text != string.Empty)
                        {

                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.CommandText = "sp_InsertSalaryDetails";

                            cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = EmpId.Text;
                            cmd.Parameters.Add("@TOTALSALARY", System.Data.SqlDbType.Decimal).Value = Totalsalary.Text;
                            cmd.Parameters.Add("@PAIDSALARY", System.Data.SqlDbType.VarChar).Value = Ddwnlistsalarypaid.SelectedItem.Text;
                            cmd.Parameters.Add("@SALARYPMONTH", System.Data.SqlDbType.Decimal).Value = 0;
                            cmd.Parameters.Add("@TRANSFERMODE", System.Data.SqlDbType.NVarChar).Value = Ddwnlisttransfermode.SelectedItem.Text;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Salary cannot be blank');", true);
                            return;
                        }

                        //SqlConnection co1 = new SqlConnection(cs);
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "SELECT * FROM [EMPLOYEESALARYDETAILS] WHERE ([EMPID] = @EmpId)";

                        //SqlCommand cm1 = new SqlCommand("SELECT * FROM [EMPLOYEESALARYDETAILS] WHERE ([EMPID] = @EmpId)", co1);
                        cmd.Parameters.AddWithValue("@EmpId", EmpId.Text);
                        SqlDataAdapter adpt1 = new SqlDataAdapter(cmd);
                        DataSet dt1 = new DataSet();
                        adpt1.Fill(dt1);
                        int UserExist = 0;
                        if (dt1.Tables[0].Rows.Count > 0)
                        {

                            UserExist = Convert.ToInt32(dt1.Tables[0].Rows[0]["EMPID"].ToString());
                        }
                        if (UserExist <= 0)
                        {
                            //co1.Open();
                            //cm1.Connection = co1;
                            cmd.Parameters.Clear();
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.CommandText = "sp_InsertEmployeeSalary";
                            cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = EmpId.Text;
                            cmd.Parameters.Add("@TOTALSALARY", System.Data.SqlDbType.Decimal).Value = Totalsalary.Text;
                            cmd.Parameters.Add("@TOTALHOURS", System.Data.SqlDbType.Time).Value = txtHoursWorked.Text;
                            cmd.Parameters.Add("@TOTALDEDUCTION", System.Data.SqlDbType.Decimal).Value = 0;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            //co1.Close();
                            Totalsalary.Text = "";
                            txtHoursWorked.Text = "";
                        }
                        else
                        {
                            int UserExist1 = Convert.ToInt32(dt1.Tables[0].Rows[0]["EMPID"].ToString());
                            Decimal totalsalary = Convert.ToDecimal(dt1.Tables[0].Rows[0]["TOTALSALARY"].ToString());
                            DateTime totalHours = Convert.ToDateTime(dt1.Tables[0].Rows[0]["TOTALHOURS"].ToString());
                            Decimal totalDeduction = Convert.ToDecimal(dt1.Tables[0].Rows[0]["TOTALDEDUCTION"].ToString());
                            //cmd.ExecuteScalar();

                            Decimal Sal = Convert.ToDecimal((Totalsalary.Text).ToString());
                            totalsalary = totalsalary + Sal;

                            //DateTime b = Convert.ToDateTime((txtHoursWorked.Text).ToString());
                            //totalHours = totalHours + b;


                            // co1.Open();
                            //cm1.Connection = co1;
                            cmd.Parameters.Clear();
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.CommandText = "sp_UpdateEmployeeSalary";

                            cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = EmpId.Text;
                            cmd.Parameters.Add("@TOTALSALARY", System.Data.SqlDbType.Decimal).Value = totalsalary;
                            cmd.Parameters.Add("@TOTALHOURS", System.Data.SqlDbType.Time).Value = txtHoursWorked.Text;
                            cmd.Parameters.Add("@TOTALDEDUCTION", System.Data.SqlDbType.Decimal).Value = totalDeduction;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            //cmd.Close();

                            //totalsalary = Convert.ToDecimal("");
                            Totalsalary.Text = "";
                            txtHoursWorked.Text = "";
                        }
                    }


                }

            }
            con.Close();

        }
        protected void Ddwnlisttransfermode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (Ddwnlisttransfermode.SelectedItem.Text == "Cheque")
            //{
            //    txttransfermode.Visible = true;
            //    txttransfermode.Text = "(Enter Cheque No:)";
            //}
            //else

            //if(Ddwnlisttransfermode.SelectedItem.Text == "Bank A/C:")
            //{                
            //    txttransfermode.Visible = true;
            //    txttransfermode.Text = "(Enter A/C No:)";
            //}
        }

        public void resetAll()
        {

            System.Web.UI.WebControls.TextBox txtHoursWorked = (grdviewprojectdetails.SelectedRow.Cells[5].FindControl("txtHoursWorked") as System.Web.UI.WebControls.TextBox);
            //System.Web.UI.WebControls.TextBox txtHoursWorked = (rows.Cells[5].FindControl("txtHoursWorked") as System.Web.UI.WebControls.TextBox);
            //System.Web.UI.WebControls.Label WagePerHr = (rows.Cells[6].FindControl("LblWagePerHr") as System.Web.UI.WebControls.Label);
            System.Web.UI.WebControls.Label WagePerHr = (grdviewprojectdetails.SelectedRow.Cells[6].FindControl("LblWagePerHr") as System.Web.UI.WebControls.Label);

            //System.Web.UI.WebControls.TextBox Totalsalary = (rows.Cells[8].FindControl("txtTotalSalary") as System.Web.UI.WebControls.TextBox);
            System.Web.UI.WebControls.TextBox Totalsalary = (grdviewprojectdetails.SelectedRow.Cells[8].FindControl("txtTotalSalary") as System.Web.UI.WebControls.TextBox);

        }

    }
}