﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Data;

namespace ProjectManagement
{
    public partial class Client : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {  
            if(!IsPostBack)
            {
                Label1.Text = Session["username"].ToString();

                SqlConnection con = new SqlConnection(cs);
                con.Open();


                SqlCommand com = new SqlCommand("select * from EMPLOYEE", con);

                SqlDataAdapter adpt = new SqlDataAdapter(com);

                DataTable dt = new DataTable();

                adpt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    GridView2.DataSource = dt;

                    GridView2.DataBind();
                }
                con.Close();
            }
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Employee Information.aspx?");
        }
    }
}