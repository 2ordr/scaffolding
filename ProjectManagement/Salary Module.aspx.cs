﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Net;
using System.Net.Mail;
using System.Text;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Threading;
using System.Globalization;

namespace ProjectManagement
{
    public partial class Salary_Module : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        Boolean Flag = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    LoginName.Text = Label1.Text;
                    LblUserId.Text = Session["UserId"].ToString();

                    DateTime orddate = DateTime.Now;
                    lblSystemDataTime.Text = Convert.ToString(orddate);
                    lblSystemDataTime.Text = string.Format("{0:dd-MM-yyyy hh:mm:ss}", orddate);
                    txtStartTime.Text = string.Format("{0:dd-MM-yyyy}", orddate);
                    Session["date"] = txtStartTime.Text;

                    BindProjectDeetails();
                    BindUserDetails();
                    BtnStopttime.Visible = false;
                    BtnStarttime.Enabled = false;
                    ddProjectList.Items.Insert(0, ("-Vælg-"));

                    LblTaskId.Text = "";
                    LblItemCode.Text = "";

                    SqlConnection con11 = new SqlConnection(cs);
                    SqlCommand cmd11 = new SqlCommand();
                    cmd11.Connection = con11;
                    con11.Open();
                    cmd11.CommandType = System.Data.CommandType.Text;
                    cmd11.Parameters.Clear();
                    cmd11.CommandText = "Delete FROM TempSalaryTask";
                    cmd11.ExecuteNonQuery();
                    cmd11.Parameters.Clear();
                    cmd11.CommandText = "Update SALARYREPORT SET Flag='false'";
                    cmd11.ExecuteNonQuery();
                    con11.Close();
                }
            }
            //if  (Global.TotIFees == 0)
            //{
            //    //Global.TotIFees = 0;
            //    btnSave.Visible = false;
            //    btnView.Visible = true;

            //}
            //else
            //{
            //    //Global.TotIFees = Convert.ToDecimal(Session["TotalFees"].ToString());
            //    btnSave.Visible = true;
            //    btnView.Visible = false;
            //}
        }

        public void BindProjectDeetails()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from PROJECTS", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                ddProjectList.DataSource = dt;
                ddProjectList.DataTextField = "ProjectName";
                ddProjectList.DataValueField = "ProjectId";
                ddProjectList.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Customer had Not Created any Project');", true);
                return;
            }

            con.Close();
        }

        public void BindUserDetails()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            //com.CommandText = "select * from PROJECTS where ProjectId= " + ddlCurrentDropDownList.SelectedValue;
            com.CommandText = "Select distinct * from EMPLOYEE";
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                ddlUser.DataSource = dt;
                ddlUser.DataTextField = "FName";
                ddlUser.DataValueField = "EmpId";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, ("-Vælg-"));
            }
        }

        protected void ddProjectList_SelectedIndexChanged1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            if (ddProjectList.SelectedItem.Text != "-Vælg-")
            {
                DropDownList ddlCurrentDropDownList = (DropDownList)sender;
                //com.CommandText = "select * from PROJECTS where ProjectId= " + ddlCurrentDropDownList.SelectedValue;
                com.CommandText = "Select distinct * from EMPLOYEE,ASSIGNED,PROJECTS where ASSIGNED.ProjectId=PROJECTS.ProjectId and EMPLOYEE.EmpId=ASSIGNED.EmpId and PROJECTS.ProjectId= " + ddlCurrentDropDownList.SelectedValue;
                SqlDataAdapter adpt = new SqlDataAdapter(com);
                DataSet dt = new DataSet();
                adpt.Fill(dt);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    gvPInfodetails.DataSource = dt;
                    gvPInfodetails.DataBind();

                    lblProjectId.Text = dt.Tables[0].Rows[0]["ProjectId"].ToString();
                    txtClient.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                    //txtStartTime.Text = dt.Tables[0].Rows[0]["ProjectDate"].ToString();
                    //txtCompletionDate.Text = dt.Tables[0].Rows[0]["CompletionDate"].ToString();

                    //BindTaskDetails();
                    BindStockDetails();
                    DDLSTOCKITEM();

                    DropDownUserList.DataSource = dt;
                    DropDownUserList.DataTextField = "FName";
                    DropDownUserList.DataValueField = "EmpId";
                    DropDownUserList.DataBind();
                    DropDownUserList.Items.Insert(0, ("-Vælg-"));

                    gridUsers.DataSource = dt;
                    gridUsers.DataBind();
                }
            }
            con.Close();
        }

        public void BindTaskDetails()
        {
            if (lblProjectId.Text != "" && DropDownUserList.SelectedItem.Text != "-Vælg-")
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_getTaskInfoForProject";

                cmd.Parameters.Add("@I_PRJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                cmd.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = LBLUSERID1.Text;

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    ddTaskList.DataSource = dt;
                    ddTaskList.DataTextField = "Task";
                    ddTaskList.DataValueField = "TaskId";
                    ddTaskList.DataBind();
                    ddTaskList.Items.Insert(0, ("-Vælg-"));
                }
                dt.Rows.Clear();
                cmd.Parameters.Clear();
                con.Close();
            }
            else
            {

            }
        }

        public void DDLSTOCKITEM()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_getOrderInfoForProject";

            cmd.Parameters.Add("@I_PRJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                DDLStockItem.DataSource = dt;
                DDLStockItem.DataTextField = "ItemName";
                //DDLStockItem.DataValueField = "ItemFees";
                DDLStockItem.DataValueField = "ItemCode";
                DDLStockItem.DataBind();
                DDLStockItem.Items.Insert(0, ("-Vælg-"));
            }
            con.Close();
        }

        public void BindStockDetails()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_getOrderInfoForProject";

            cmd.Parameters.Add("@I_PRJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                gvstockDetails.DataSource = dt;
                gvstockDetails.DataBind();
                HeaderStock.Visible = true;
                //LblItemCode.Text = dt.Tables[0].Rows[0]["ItemCode"].ToString();
            }
            con.Close();
        }

        protected void ddTaskList_SelectedIndexChanged1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            DropDownList ddlCurrentDropDownList = (DropDownList)sender;
            com.CommandText = "select  distinct SubTask,STID from TASK where TASKID= " + ddlCurrentDropDownList.SelectedValue;


            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                gvtaskDDetails.DataSource = dt;
                gvtaskDDetails.DataBind();

                ddlSubtask.DataSource = dt;
                ddlSubtask.DataBind();
                ddlSubtask.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Vælg-", "0"));

                //txttask_0.Text = dt.Tables[0].Rows[0]["Task"].ToString();
                //LblTaskId.Text = dt.Tables[0].Rows[0]["TaskId"].ToString();
                BtnStarttime.Enabled = true;
            }
            con.Close();
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            lbldisplayTime.Text = DateTime.Now.ToString();
        }

        protected void BtnStarttime_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            BtnStarttime.Visible = false;
            BtnStopttime.Visible = true;
            txtStartStocktime.Text = lbldisplayTime.Text;
        }

        protected void BtnStopttime_Click(object sender, EventArgs e)
        {

            BtnStarttime.Visible = true;
            BtnStopttime.Visible = false;
            txtStopStockTime.Text = lbldisplayTime.Text;

            DateTime inTime = Convert.ToDateTime(txtStartStocktime.Text);
            DateTime outTime = Convert.ToDateTime(txtStopStockTime.Text);
            TimeSpan i = outTime.Subtract(inTime);
            this.txtHrs.Text = i.ToString();
            this.txtHourseWorked.Text = i.ToString();


            //foreach (GridViewRow item in grdviewprojectdetails.Rows)
            //{
            //    if (item.RowType == DataControlRowType.DataRow)
            //    {
            //        CheckBox chk = (CheckBox)(item.Cells[0].FindControl("chkSelect"));
            //        if (chk.Checked)
            //        {
            //            TextBox txtHoursWorked = ((TextBox)item.Cells[5].FindControl("txtHoursWorked"));
            //            txtHoursWorked.Text = txtHrs.Text;

            //            Label LblWagePerHr = ((Label)item.Cells[6].FindControl("LblWagePerHr"));
            //            decimal trr = Convert.ToDecimal(LblWagePerHr.Text);

            //            TextBox txtTotalSalary = ((TextBox)item.Cells[8].FindControl("txtTotalSalary"));

            //            decimal dec = Convert.ToDecimal(TimeSpan.Parse((txtHoursWorked.Text)).TotalHours);

            //            // txtTotaloursWorked.Text = ((Convert.ToInt32(dec)) * (Convert.ToInt32(LblWagePerHr.Text))).ToString();
            //            decimal total = ((Convert.ToDecimal(dec)) * (Convert.ToDecimal(trr)));

            //            txtTotalSalary.Text = total.ToString();

            //        }
            //    }
            //}
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports.aspx");
            //if (RBtnUseQuate.Checked == true)
            //{
            //    //using (System.IO.StringWriter sw = new System.IO.StringWriter())
            //    //{
            //    //    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
            //    //    {
            //    //        //To Export all pages
            //    //        //GridView2.AllowPaging = false;
            //    //        //this.BindStock();

            //    //        pdfGeneration.RenderControl(hw);
            //    //        //GridView2.RenderControl(hw);

            //    //        // GridView2.HeaderRow.Style.Add("width", "15%");
            //    //        //GridView2.HeaderRow.Style.Add("font-size", "10px");
            //    //        // divheader.Style.Add("text-decoration", "bold");
            //    //        // divheader.Style.Add("font-family", "Arial, Helvetica, sans-serif;");
            //    //        //divheader.Style.Add("font-size", "8px");

            //    //        String attachment = "attachment;filename= StockDetails.pdf " + DateTime.Now + "";
            //    //        System.IO.StringReader sr = new System.IO.StringReader(sw.ToString());
            //    //        Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
            //    //        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //    //        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //    //        pdfDoc.Open();
            //    //        htmlparser.Parse(sr);
            //    //        pdfDoc.Close();

            //    //        Response.ContentType = "application/pdf";

            //    //        Response.AddHeader("content-disposition", attachment);
            //    //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //    //        Response.Write(pdfDoc);
            //    //        Response.End();
            //    //    }
            //    //}
            //}
            //else
            //    if (RBtnUseLight.Checked == true)
            //    {
            //        Boolean flag = true;
            //        //string url = ("PrintStockDetails.aspx?UserName=" + DropDownUserList.SelectedItem.Text);
            //        string url = ("PrintStockDetails.aspx?Flag=" + flag);
            //        string s = "window.open('" + url + "');";

            //        ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            //    }
            //    else
            //    {
            //        if (RBtnUseHeavy.Checked == true)
            //        {
            //            Boolean flag = false;
            //            //string url = ("PrintStockDetails.aspx?UserName=" + DropDownUserList.SelectedItem.Text);
            //            string url = ("PrintStockDetails.aspx?Flag=" + flag);
            //            string s = "window.open('" + url + "');";

            //            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            //        }
            //    }


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string type = "";
            Decimal TotalHOURS = 0;
            Decimal ContributionFees = 0;
            Decimal TotalWage = 0;
            Decimal Hours = 0;
            string Status = "";

            string UserType = Session["UserType"].ToString();
            Decimal TotalFees = 0;

            if (LblGrantTotal.Text != "")
            {
                TotalFees = Convert.ToDecimal(LblGrantTotal.Text);
                //Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text);
            }
            else
            {
                TotalFees = 0;
            }

            if (UserType == "Admin")
            {
                Status = "Approve";
            }
            else
            {
                Status = "Hold";
            }
            if (ddProjectList.SelectedItem.Text == "-Vælg-")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Project Name');", true);
                return;
            }
            else
            {
                float total = 0;
                foreach (GridViewRow gridViewRow in gridUsers.Rows)
                {

                    if (gridViewRow.RowType == DataControlRowType.DataRow)
                    {

                        // System.Web.UI.WebControls.TextBox thours = (gridViewRow.Cells[4].FindControl("thours") as System.Web.UI.WebControls.TextBox);

                        txtHours = (TextBox)gridViewRow.FindControl("txtHours");
                        DropDownList DDLLunch = (DropDownList)gridViewRow.FindControl("DDLLunch");
                        CheckBox chkUser = (CheckBox)gridViewRow.FindControl("chkUser");

                        if (chkUser.Checked == true)
                        {
                            float rowValue = 0;
                            if (float.TryParse(txtHours.Text.Trim(), out rowValue))
                                total += rowValue;

                            if (DDLLunch.SelectedItem.Text == "Yes")
                            {
                                total = total - 1;
                            }

                        }

                    }
                }
                lblTotalHR.Text = total.ToString();

                foreach (GridViewRow Userrows in gridUsers.Rows)
                {

                    if (Userrows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.CheckBox chkUser = (Userrows.Cells[0].FindControl("chkUser") as System.Web.UI.WebControls.CheckBox);
                        System.Web.UI.WebControls.Label lblEmpid = (Userrows.Cells[1].FindControl("lblEmpid") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.TextBox txtHours = (Userrows.Cells[2].FindControl("txtHours") as System.Web.UI.WebControls.TextBox);
                        System.Web.UI.WebControls.Label lblWage = (Userrows.Cells[2].FindControl("lblWage") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblLunchTime = (Userrows.Cells[3].FindControl("lblLunchTime") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.DropDownList DDLLunch = (Userrows.Cells[3].FindControl("DDLLunch") as System.Web.UI.WebControls.DropDownList);

                        if (chkUser.Checked == true)
                        {
                            if (txtHours.Text != "0")
                            {
                                if (DDLLunch.SelectedItem.Text == "Yes")
                                {
                                    txtHours.Text = ((Convert.ToDecimal(txtHours.Text)) - (Convert.ToDecimal(lblLunchTime.Text))).ToString();
                                }
                                if (RBtnUseQuate.Checked == true)
                                {


                                    TotalWage = ((Convert.ToDecimal(txtHours.Text)) * (Convert.ToDecimal(lblWage.Text)));
                                    ContributionFees = TotalWage;
                                    //ContributionFees = ContributionFees / 100;
                                }
                                else
                                {
                                    TotalHOURS = ((Convert.ToDecimal(txtHours.Text)) / (Convert.ToDecimal(lblTotalHR.Text))) * 100;

                                    ContributionFees = (TotalHOURS * TotalFees) / 100;
                                    //ContributionFees = ContributionFees / 100;

                                }

                                string dtEntry1 = txtStartTime.Text;
                                DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                SqlConnection co1 = new SqlConnection(cs);
                                co1.Open();

                                SqlCommand cm1 = new SqlCommand("SELECT Empid,TOTALITEMFEES,Type,Hours,ContributionFees,Status FROM [SALARYREPORT] WHERE ([EmpId] = @I_EMPID AND [EntryDate]= @DATE AND [ProjectId]= @I_PROJECTID AND [Type]=@TYPE AND [Status]=@STATUS)", co1);

                                cm1.Parameters.AddWithValue("@I_EMPID", lblEmpid.Text);
                                cm1.Parameters.AddWithValue("@DATE", dt11);
                                cm1.Parameters.AddWithValue("@I_PROJECTID", ddProjectList.SelectedValue);
                                cm1.Parameters.AddWithValue("@STATUS", Status.ToString());
                                if (RBtnUseQuate.Checked == true)
                                {
                                    type = "Hours";
                                }
                                else if (RBtnUseLight.Checked == true)
                                {
                                    type = "LightWeight";
                                }
                                else if (RBtnUseHeavy.Checked == true)
                                {
                                    type = "HeavyWeight";
                                }
                                cm1.Parameters.AddWithValue("@TYPE", type);

                                SqlDataAdapter adpt1 = new SqlDataAdapter(cm1);
                                DataSet dt1 = new DataSet();
                                adpt1.Fill(dt1);
                                int UserExist = 0;
                                double totalhours = 0;
                                double contributionfees = 0;
                                string status1;
                                if (dt1.Tables[0].Rows.Count > 0)
                                {
                                    UserExist = Convert.ToInt32(dt1.Tables[0].Rows[0]["Empid"].ToString());
                                    totalhours = Convert.ToDouble(dt1.Tables[0].Rows[0]["Hours"].ToString());
                                    contributionfees = Convert.ToDouble(dt1.Tables[0].Rows[0]["ContributionFees"].ToString());
                                    status1 = Convert.ToString(dt1.Tables[0].Rows[0]["Status"].ToString());
                                    if (status1.ToString() != Status.ToString())
                                    //if (UserExist <= 0) 
                                    {
                                        //SqlConnection con = new SqlConnection(cs);
                                        //con.Open();
                                        //SqlCommand com = new SqlCommand();
                                        //com.Connection = con;
                                        //foreach (GridViewRow rows in gridTask.Rows)
                                        //{

                                        //    if (rows.RowType == DataControlRowType.DataRow)
                                        //    {
                                        //        System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                                        //        System.Web.UI.WebControls.Label lblSTID = (rows.Cells[0].FindControl("lblSTID") as System.Web.UI.WebControls.Label);
                                        //        System.Web.UI.WebControls.Label LblItemFees = (rows.Cells[4].FindControl("LblItemFees") as System.Web.UI.WebControls.Label);
                                        //        System.Web.UI.WebControls.DropDownList DDLStockItem = (rows.Cells[4].FindControl("DDLStockItem") as System.Web.UI.WebControls.DropDownList);

                                        //        TotIFees = Convert.ToDecimal(LblItemFees.Text);

                                        cm1.Parameters.Clear();
                                        cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                        cm1.CommandText = "sp_InsertSalaryReport";
                                        cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                                        cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;     // DropDownUserList.SelectedValue;
                                        cm1.Parameters.Add("@I_TASKID", System.Data.SqlDbType.BigInt).Value = 0;        // lbltaskid.Text;
                                        cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = 0;  //DDLStockItem.SelectedValue;
                                        cm1.Parameters.Add("@TOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                        cm1.Parameters.Add("@I_STID", System.Data.SqlDbType.BigInt).Value = 0;                   // lblSTID.Text;
                                        if (txtHours.Text != "0")
                                        {
                                            Hours = Convert.ToDecimal(txtHours.Text);
                                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = Hours;
                                        }
                                        else
                                        {
                                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = 0;
                                        }

                                        //string dtEntry = txtStartTime.Text;

                                        //DateTime dt = DateTime.ParseExact(dtEntry, "dd-MM-yyyy h:mm:ss", CultureInfo.InvariantCulture);
                                        cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                        cm1.Parameters.Add("@CONTRIBUTION", System.Data.SqlDbType.Decimal).Value = ContributionFees;
                                        if (RBtnUseQuate.Checked == true)
                                        {
                                            type = "Hours";
                                        }
                                        else if (RBtnUseLight.Checked == true)
                                        {
                                            type = "LightWeight";
                                        }
                                        else if (RBtnUseHeavy.Checked == true)
                                        {
                                            type = "HeavyWeight";
                                        }

                                        cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                                        cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = Status;
                                        if (UserType == "Admin")
                                        {
                                            cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = txtHours.Text;
                                            cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = ContributionFees;
                                            cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                        }
                                        else
                                        {
                                            cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = 0;
                                            cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = 0;
                                            cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = 0;
                                        }
                                        cm1.Parameters.Add("@FLAG", System.Data.SqlDbType.VarChar).Value = "true";
                                        cm1.ExecuteNonQuery();

                                        foreach (GridViewRow Srows in GviewProjectStock.Rows)
                                        {
                                            if (Srows.RowType == DataControlRowType.DataRow)
                                            {
                                                System.Web.UI.WebControls.Label LblItemcode = (Srows.Cells[0].FindControl("LblItemcode") as System.Web.UI.WebControls.Label);
                                                System.Web.UI.WebControls.Label ItemName = (Srows.Cells[1].FindControl("ItemName") as System.Web.UI.WebControls.Label);
                                                System.Web.UI.WebControls.Label ItemFees = (Srows.Cells[2].FindControl("ItemFees") as System.Web.UI.WebControls.Label);
                                                System.Web.UI.WebControls.TextBox txtQuntity = (Srows.Cells[0].FindControl("txtQuntity") as System.Web.UI.WebControls.TextBox);
                                                //System.Web.UI.WebControls.DropDownList DDLItemName = (Srows.Cells[1].FindControl("DDLItemName") as System.Web.UI.WebControls.DropDownList);
                                                System.Web.UI.WebControls.TextBox txtItemFees = (Srows.Cells[2].FindControl("txtItemFees") as System.Web.UI.WebControls.TextBox);
                                                System.Web.UI.WebControls.Label lblQuntity = (Srows.Cells[0].FindControl("lblQuntity") as System.Web.UI.WebControls.Label);
                                                System.Web.UI.WebControls.Label LblTotAmt = (Srows.Cells[3].FindControl("LblTotAmt") as System.Web.UI.WebControls.Label);

                                                //ItemFees.Text = txtItemFees.Text;

                                                if (lblQuntity.Text != "")
                                                {
                                                    cm1.Parameters.Clear();
                                                    cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                                    cm1.CommandText = "sp_InsertSALARYSTOCKREPORT";
                                                    cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = LblItemcode.Text;
                                                    cm1.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = ItemName.Text;
                                                    cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                                                    cm1.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = ItemFees.Text;
                                                    cm1.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                                                    cm1.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                                                    cm1.Parameters.Add("@TOTAL", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                                    if (UserType == "Admin")
                                                    {
                                                        cm1.Parameters.Add("@AQUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                                                        cm1.Parameters.Add("@ASUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                                                        cm1.Parameters.Add("@ATOTAL", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                                    }
                                                    else
                                                    {
                                                        cm1.Parameters.Add("@AQUANTITY", System.Data.SqlDbType.VarChar).Value = "";
                                                        cm1.Parameters.Add("@ASUBTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                                                        cm1.Parameters.Add("@ATOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                                                    }

                                                    cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                                                    cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                                                    cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;

                                                    cm1.ExecuteNonQuery();
                                                    cm1.Parameters.Clear();

                                                    //cm1.CommandType = System.Data.CommandType.Text;
                                                    //cm1.CommandText = "Update MULTIVALUESTOCK SET ItemFees=" + ItemFees.Text + "";
                                                    //cm1.ExecuteNonQuery();
                                                    //cm1.Parameters.Clear();
                                                }
                                            }
                                        }
                                        cm1.Parameters.Clear();
                                        cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                        cm1.CommandText = "sp_getSalaryReport";
                                        cm1.Parameters.AddWithValue("@DATE", dt11);
                                        cm1.Parameters.AddWithValue("@I_PROJECTID", ddProjectList.SelectedValue);
                                        cm1.Parameters.AddWithValue("@TYPE", type);
                                        cm1.Parameters.AddWithValue("@Flag", Flag);
                                        cm1.Parameters.AddWithValue("@I_EmpId", lblEmpid.Text);

                                        adpt1.SelectCommand = cm1;
                                        DataSet dtSalary = new DataSet();
                                        adpt1.Fill(dtSalary);

                                        if (dtSalary.Tables[0].Rows.Count > 0)
                                        {
                                            int count = dtSalary.Tables[0].Rows.Count;
                                            if (count == 2)
                                            {
                                                cm1.Parameters.Clear();
                                                cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                                cm1.CommandText = "sp_InsertSalaryReportTemporary";
                                                cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[0]["EmpId"].ToString();
                                                cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["FName"].ToString();
                                                cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["Hours"].ToString();
                                                cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["ContributionFees"].ToString();
                                                cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["TYPE"].ToString();
                                                cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["STATUS"].ToString();
                                                cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                                cm1.ExecuteNonQuery();
                                                cm1.Parameters.Clear();

                                                cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[1]["EmpId"].ToString();
                                                cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["FName"].ToString();
                                                cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[1]["Hours"].ToString();
                                                cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[1]["ContributionFees"].ToString();
                                                cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["TYPE"].ToString();
                                                cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["STATUS"].ToString();
                                                cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                                cm1.ExecuteNonQuery();
                                                cm1.Parameters.Clear();
                                            }
                                            else
                                            {
                                                cm1.Parameters.Clear();
                                                cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                                cm1.CommandText = "sp_InsertSalaryReportTemporary";
                                                cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[0]["EmpId"].ToString();
                                                cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["FName"].ToString();
                                                cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["Hours"].ToString();
                                                cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["ContributionFees"].ToString();
                                                cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["TYPE"].ToString();
                                                cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["STATUS"].ToString();
                                                cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                                cm1.ExecuteNonQuery();
                                                cm1.Parameters.Clear();
                                            }

                                            //cm1.Parameters.Clear();
                                            //cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                            //cm1.CommandText = "sp_getSalaryReportTemporary";
                                            //adpt1.SelectCommand = cm1;
                                            //DataSet dtSalary1 = new DataSet();
                                            //adpt1.Fill(dtSalary1);

                                            //if (dtSalary1.Tables[0].Rows.Count > 0)
                                            //{
                                            //    GridSalary.DataSource = dtSalary;
                                            //    GridSalary.DataBind();

                                            //    divSalary.Visible = true;
                                            //}

                                            //cm1.Parameters.Clear();
                                            //cm1.CommandText = "sp_getDistinctNameSalaryReportTemporary";
                                            //adpt1.SelectCommand = cm1;
                                            //DataSet dtSalary2 = new DataSet();
                                            //adpt1.Fill(dtSalary2);
                                            //if (dtSalary2.Tables[0].Rows.Count > 0)
                                            //{
                                            //    GridUsers2.DataSource = dtSalary2;
                                            //    GridUsers2.DataBind();
                                            //}
                                            //cm1.Parameters.Clear();
                                            //cm1.CommandType = System.Data.CommandType.Text;
                                            //cm1.CommandText = "Delete from SalaryReportTemporary";
                                            //cm1.ExecuteNonQuery();
                                        }

                                        //    }
                                        //}
                                    }
                                    else
                                    {

                                        //foreach (GridViewRow rows in gridTask.Rows)
                                        //{

                                        //    if (rows.RowType == DataControlRowType.DataRow)
                                        //    {
                                        //        System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                                        //        System.Web.UI.WebControls.Label lblSTID = (rows.Cells[0].FindControl("lblSTID") as System.Web.UI.WebControls.Label);
                                        //        System.Web.UI.WebControls.Label LblItemFees = (rows.Cells[4].FindControl("LblItemFees") as System.Web.UI.WebControls.Label);
                                        //        System.Web.UI.WebControls.DropDownList DDLStockItem = (rows.Cells[4].FindControl("DDLStockItem") as System.Web.UI.WebControls.DropDownList);

                                        //TotIFees = Convert.ToDecimal(LblItemFees.Text);

                                        Decimal TotalItemFees = Convert.ToDecimal(dt1.Tables[0].Rows[0]["TOTALITEMFEES"].ToString());
                                        Decimal Sal = TotalFees;
                                        TotalItemFees = TotalItemFees + Sal;
                                        cm1.Parameters.Clear();

                                        cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                        cm1.CommandText = "sp_UpdateSalaryReports";                                                 //want  to change this procedure 
                                        cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                                        cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = UserExist;

                                        if (LblTaskId.Text != "")
                                        {
                                            cm1.Parameters.Add("@I_TASKID", System.Data.SqlDbType.BigInt).Value = 0;         //lbltaskid.Text;
                                        }
                                        else
                                        {
                                            cm1.Parameters.Add("@I_TASKID", System.Data.SqlDbType.BigInt).Value = 0;
                                        }

                                        cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = 0;        //DDLStockItem.SelectedValue;
                                        cm1.Parameters.Add("@TOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = TotalItemFees;
                                        cm1.Parameters.Add("@I_STID", System.Data.SqlDbType.BigInt).Value = 0;        //lblSTID.Text;
                                        if (txtHours.Text != "0")
                                        {

                                            Hours = Convert.ToDecimal(txtHours.Text);
                                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(txtHours.Text) + Convert.ToDecimal(totalhours);
                                        }
                                        else
                                        {
                                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = totalhours;
                                        }
                                        cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                        cm1.Parameters.Add("@CONTRIBUTION", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(ContributionFees) + Convert.ToDecimal(contributionfees);

                                        if (RBtnUseQuate.Checked == true)
                                        {
                                            type = "Hours";
                                        }
                                        else if (RBtnUseLight.Checked == true)
                                        {
                                            type = "LightWeight";
                                        }
                                        else if (RBtnUseHeavy.Checked == true)
                                        {
                                            type = "HeavyWeight";
                                        }

                                        cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                                        cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = Status;
                                        if (UserType == "Admin")
                                        {
                                            cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = txtHours.Text;
                                            cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = ContributionFees;
                                            cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = TotalItemFees;
                                        }
                                        else
                                        {
                                            cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = 0;
                                            cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = 0;
                                            cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = 0;
                                        }
                                        cm1.Parameters.Add("@FLAG", System.Data.SqlDbType.VarChar).Value = "true";
                                        cm1.ExecuteNonQuery();
                                        cm1.Parameters.Clear();

                                        foreach (GridViewRow Srows in GviewProjectStock.Rows)
                                        {
                                            if (Srows.RowType == DataControlRowType.DataRow)
                                            {
                                                System.Web.UI.WebControls.Label LblItemcode = (Srows.Cells[0].FindControl("LblItemcode") as System.Web.UI.WebControls.Label);
                                                System.Web.UI.WebControls.Label ItemName = (Srows.Cells[1].FindControl("ItemName") as System.Web.UI.WebControls.Label);
                                                System.Web.UI.WebControls.Label ItemFees = (Srows.Cells[2].FindControl("ItemFees") as System.Web.UI.WebControls.Label);
                                                //System.Web.UI.WebControls.DropDownList DDLItemName = (Srows.Cells[1].FindControl("DDLItemName") as System.Web.UI.WebControls.DropDownList);
                                                System.Web.UI.WebControls.TextBox txtQuntity = (Srows.Cells[0].FindControl("txtQuntity") as System.Web.UI.WebControls.TextBox);
                                                System.Web.UI.WebControls.Label lblQuntity = (Srows.Cells[0].FindControl("lblQuntity") as System.Web.UI.WebControls.Label);
                                                System.Web.UI.WebControls.Label LblTotAmt = (Srows.Cells[3].FindControl("LblTotAmt") as System.Web.UI.WebControls.Label);

                                                if (txtQuntity.Text != "")
                                                {
                                                    cm1.Parameters.Clear();
                                                    cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                                    cm1.CommandText = "sp_UpdateSALARYSTOCKREPORT";
                                                    cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = LblItemcode.Text;
                                                    cm1.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = ItemName.Text;
                                                    cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                                                    cm1.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = ItemFees.Text;
                                                    cm1.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                                                    cm1.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                                                    cm1.Parameters.Add("@TOTAL", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                                    if (UserType == "Admin")
                                                    {
                                                        cm1.Parameters.Add("@AQUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                                                        cm1.Parameters.Add("@ASUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                                                        cm1.Parameters.Add("@ATOTAL", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                                    }
                                                    else
                                                    {
                                                        cm1.Parameters.Add("@AQUANTITY", System.Data.SqlDbType.VarChar).Value = "";
                                                        cm1.Parameters.Add("@ASUBTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                                                        cm1.Parameters.Add("@ATOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                                                    }

                                                    cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                                                    cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                                                    cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;

                                                    cm1.ExecuteNonQuery();
                                                    cm1.Parameters.Clear();

                                                    //cm1.CommandType = System.Data.CommandType.Text;
                                                    //cm1.CommandText = "Update MULTIVALUESTOCK SET ItemFees=" + ItemFees.Text + "";
                                                    //cm1.ExecuteNonQuery();
                                                    //cm1.Parameters.Clear();
                                                }

                                            }
                                        }

                                        cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                        cm1.CommandText = "sp_getSalaryReport";
                                        cm1.Parameters.AddWithValue("@DATE", dt11);
                                        cm1.Parameters.AddWithValue("@I_PROJECTID", ddProjectList.SelectedValue);
                                        cm1.Parameters.AddWithValue("@TYPE", type);
                                        cm1.Parameters.AddWithValue("@Flag", Flag);
                                        cm1.Parameters.AddWithValue("@I_EmpId", lblEmpid.Text);

                                        adpt1.SelectCommand = cm1;
                                        DataSet dtSalary = new DataSet();
                                        adpt1.Fill(dtSalary);

                                        if (dtSalary.Tables[0].Rows.Count > 0)
                                        {
                                            int count = dtSalary.Tables[0].Rows.Count;
                                            if (count == 2)
                                            {
                                                cm1.Parameters.Clear();
                                                cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                                cm1.CommandText = "sp_InsertSalaryReportTemporary";
                                                cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[0]["EmpId"].ToString();
                                                cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["FName"].ToString();
                                                cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["Hours"].ToString();
                                                cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["ContributionFees"].ToString();
                                                cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["TYPE"].ToString();
                                                cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["STATUS"].ToString();
                                                cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                                cm1.ExecuteNonQuery();
                                                cm1.Parameters.Clear();

                                                cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[1]["EmpId"].ToString();
                                                cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["FName"].ToString();
                                                cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[1]["Hours"].ToString();
                                                cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[1]["ContributionFees"].ToString();
                                                cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["TYPE"].ToString();
                                                cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["STATUS"].ToString();
                                                cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                                cm1.ExecuteNonQuery();
                                                cm1.Parameters.Clear();
                                            }
                                            else
                                            {
                                                cm1.Parameters.Clear();
                                                cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                                cm1.CommandText = "sp_InsertSalaryReportTemporary";
                                                cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[0]["EmpId"].ToString();
                                                cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["FName"].ToString();
                                                cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["Hours"].ToString();
                                                cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["ContributionFees"].ToString();
                                                cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["TYPE"].ToString();
                                                cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["STATUS"].ToString();
                                                cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                                cm1.ExecuteNonQuery();
                                                cm1.Parameters.Clear();
                                            }
                                            //cm1.Parameters.Clear();
                                            //cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                            //cm1.CommandText = "sp_getSalaryReportTemporary";
                                            //adpt1.SelectCommand = cm1;
                                            //DataSet dtSalary1 = new DataSet();
                                            //adpt1.Fill(dtSalary1);
                                            //if (dtSalary1.Tables[0].Rows.Count > 0)
                                            //{
                                            //    GridSalary.DataSource = dtSalary1;
                                            //    GridSalary.DataBind();

                                            //    divSalary.Visible = true;
                                            //}
                                            //cm1.Parameters.Clear();
                                            //cm1.CommandText = "sp_getDistinctNameSalaryReportTemporary";
                                            //adpt1.SelectCommand = cm1;
                                            //DataSet dtSalary2 = new DataSet();
                                            //adpt1.Fill(dtSalary2);
                                            //if (dtSalary2.Tables[0].Rows.Count > 0)
                                            //{
                                            //    GridUsers2.DataSource = dtSalary2;
                                            //    GridUsers2.DataBind();
                                            //}
                                            //cm1.Parameters.Clear();
                                            //cm1.CommandType = System.Data.CommandType.Text;
                                            //cm1.CommandText = "Delete from SalaryReportTemporary";
                                            //cm1.ExecuteNonQuery();
                                        }
                                    }
                                }
                                else
                                {
                                    cm1.Parameters.Clear();
                                    cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                    cm1.CommandText = "sp_InsertSalaryReport";
                                    cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                                    cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;     // DropDownUserList.SelectedValue;
                                    cm1.Parameters.Add("@I_TASKID", System.Data.SqlDbType.BigInt).Value = 0;        // lbltaskid.Text;
                                    cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = 0;  //DDLStockItem.SelectedValue;
                                    cm1.Parameters.Add("@TOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                    cm1.Parameters.Add("@I_STID", System.Data.SqlDbType.BigInt).Value = 0;                   // lblSTID.Text;
                                    if (txtHours.Text != "0")
                                    {
                                        Hours = Convert.ToDecimal(txtHours.Text);
                                        cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = Hours;
                                    }
                                    else
                                    {
                                        cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = 0;
                                    }

                                    //string dtEntry = txtStartTime.Text;

                                    //DateTime dt = DateTime.ParseExact(dtEntry, "dd-MM-yyyy h:mm:ss", CultureInfo.InvariantCulture);
                                    cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                    cm1.Parameters.Add("@CONTRIBUTION", System.Data.SqlDbType.Decimal).Value = ContributionFees;
                                    if (RBtnUseQuate.Checked == true)
                                    {
                                        type = "Hours";
                                    }
                                    else if (RBtnUseLight.Checked == true)
                                    {
                                        type = "LightWeight";
                                    }
                                    else if (RBtnUseHeavy.Checked == true)
                                    {
                                        type = "HeavyWeight";
                                    }

                                    cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                                    cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = Status;
                                    if (UserType == "Admin")
                                    {
                                        cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = txtHours.Text;
                                        cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = ContributionFees;
                                        cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                    }
                                    else
                                    {
                                        cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = 0;
                                        cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = 0;
                                        cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = 0;
                                    }
                                    cm1.Parameters.Add("@FLAG", System.Data.SqlDbType.VarChar).Value = "true";
                                    cm1.ExecuteNonQuery();

                                    foreach (GridViewRow Srows in GviewProjectStock.Rows)
                                    {
                                        if (Srows.RowType == DataControlRowType.DataRow)
                                        {
                                            System.Web.UI.WebControls.Label LblItemcode = (Srows.Cells[0].FindControl("LblItemcode") as System.Web.UI.WebControls.Label);
                                            System.Web.UI.WebControls.Label ItemName = (Srows.Cells[1].FindControl("ItemName") as System.Web.UI.WebControls.Label);
                                            System.Web.UI.WebControls.Label ItemFees = (Srows.Cells[2].FindControl("ItemFees") as System.Web.UI.WebControls.Label);
                                            System.Web.UI.WebControls.TextBox txtQuntity = (Srows.Cells[0].FindControl("txtQuntity") as System.Web.UI.WebControls.TextBox);
                                            //System.Web.UI.WebControls.DropDownList DDLItemName = (Srows.Cells[1].FindControl("DDLItemName") as System.Web.UI.WebControls.DropDownList);
                                            System.Web.UI.WebControls.TextBox txtItemFees = (Srows.Cells[2].FindControl("txtItemFees") as System.Web.UI.WebControls.TextBox);
                                            System.Web.UI.WebControls.Label lblQuntity = (Srows.Cells[0].FindControl("lblQuntity") as System.Web.UI.WebControls.Label);
                                            System.Web.UI.WebControls.Label LblTotAmt = (Srows.Cells[3].FindControl("LblTotAmt") as System.Web.UI.WebControls.Label);

                                            //ItemFees.Text = txtItemFees.Text;

                                            if (lblQuntity.Text != "")
                                            {
                                                cm1.Parameters.Clear();
                                                cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                                cm1.CommandText = "sp_InsertSALARYSTOCKREPORT";
                                                cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = LblItemcode.Text;
                                                cm1.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = ItemName.Text;
                                                cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                                                cm1.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = ItemFees.Text;
                                                cm1.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                                                cm1.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                                                cm1.Parameters.Add("@TOTAL", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                                if (UserType == "Admin")
                                                {
                                                    cm1.Parameters.Add("@AQUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                                                    cm1.Parameters.Add("@ASUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                                                    cm1.Parameters.Add("@ATOTAL", System.Data.SqlDbType.Decimal).Value = TotalFees;
                                                }
                                                else
                                                {
                                                    cm1.Parameters.Add("@AQUANTITY", System.Data.SqlDbType.VarChar).Value = "";
                                                    cm1.Parameters.Add("@ASUBTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                                                    cm1.Parameters.Add("@ATOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                                                }

                                                cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                                                cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                                                cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;

                                                cm1.ExecuteNonQuery();
                                                cm1.Parameters.Clear();

                                                //cm1.CommandType = System.Data.CommandType.Text;
                                                //cm1.CommandText = "Update MULTIVALUESTOCK SET ItemFees=" + ItemFees.Text + "";
                                                //cm1.ExecuteNonQuery();
                                                //cm1.Parameters.Clear();
                                            }
                                        }
                                    }
                                    cm1.Parameters.Clear();
                                    cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                    cm1.CommandText = "sp_getSalaryReport";
                                    cm1.Parameters.AddWithValue("@DATE", dt11);
                                    cm1.Parameters.AddWithValue("@I_PROJECTID", ddProjectList.SelectedValue);
                                    cm1.Parameters.AddWithValue("@TYPE", type);
                                    cm1.Parameters.AddWithValue("@Flag", Flag);
                                    cm1.Parameters.AddWithValue("@I_EmpId", lblEmpid.Text);

                                    adpt1.SelectCommand = cm1;
                                    DataSet dtSalary = new DataSet();
                                    adpt1.Fill(dtSalary);

                                    if (dtSalary.Tables[0].Rows.Count > 0)
                                    {
                                        int count = dtSalary.Tables[0].Rows.Count;
                                        if (count == 2)
                                        {
                                            cm1.Parameters.Clear();
                                            cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                            cm1.CommandText = "sp_InsertSalaryReportTemporary";
                                            cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[0]["EmpId"].ToString();
                                            cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["FName"].ToString();
                                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["Hours"].ToString();
                                            cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["ContributionFees"].ToString();
                                            cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["TYPE"].ToString();
                                            cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["STATUS"].ToString();
                                            cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                            cm1.ExecuteNonQuery();
                                            cm1.Parameters.Clear();

                                            cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[1]["EmpId"].ToString();
                                            cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["FName"].ToString();
                                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[1]["Hours"].ToString();
                                            cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[1]["ContributionFees"].ToString();
                                            cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["TYPE"].ToString();
                                            cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[1]["STATUS"].ToString();
                                            cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                            cm1.ExecuteNonQuery();
                                            cm1.Parameters.Clear();
                                        }
                                        else
                                        {
                                            cm1.Parameters.Clear();
                                            cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                            cm1.CommandText = "sp_InsertSalaryReportTemporary";
                                            cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = dtSalary.Tables[0].Rows[0]["EmpId"].ToString();
                                            cm1.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["FName"].ToString();
                                            cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["Hours"].ToString();
                                            cm1.Parameters.Add("@CONTRIBUTIONFEES", System.Data.SqlDbType.Decimal).Value = dtSalary.Tables[0].Rows[0]["ContributionFees"].ToString();
                                            cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["TYPE"].ToString();
                                            cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dtSalary.Tables[0].Rows[0]["STATUS"].ToString();
                                            cm1.Parameters.Add("@DATE", System.Data.SqlDbType.DateTime).Value = dt11;
                                            cm1.ExecuteNonQuery();
                                            cm1.Parameters.Clear();
                                        }
                                        //cm1.Parameters.Clear();
                                        //cm1.CommandType = System.Data.CommandType.StoredProcedure;
                                        //cm1.CommandText = "sp_getSalaryReportTemporary";
                                        //adpt1.SelectCommand = cm1;
                                        //DataSet dtSalary1 = new DataSet();
                                        //adpt1.Fill(dtSalary1);
                                        //if (dtSalary1.Tables[0].Rows.Count > 0)
                                        //{
                                        //    GridSalary.DataSource = dtSalary1;
                                        //    GridSalary.DataBind();

                                        //    divSalary.Visible = true;
                                        //}
                                        //cm1.Parameters.Clear();
                                        //cm1.CommandText = "sp_getDistinctNameSalaryReportTemporary";
                                        //adpt1.SelectCommand = cm1;
                                        //DataSet dtSalary2 = new DataSet();
                                        //adpt1.Fill(dtSalary2);
                                        //if (dtSalary2.Tables[0].Rows.Count > 0)
                                        //{
                                        //    GridUsers2.DataSource = dtSalary2;
                                        //    GridUsers2.DataBind();
                                        //}
                                        //cm1.Parameters.Clear();
                                        //cm1.CommandType = System.Data.CommandType.Text;
                                        //cm1.CommandText = "Delete from SalaryReportTemporary";
                                        //cm1.ExecuteNonQuery();
                                    }

                                    //    }
                                    //}
                                }
                                co1.Close();
                                //ddProjectList.SelectedValue = "";
                            }
                        }

                    }
                }

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;

                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_getSalaryReportTemporary";
                SqlDataAdapter adpt2 = new SqlDataAdapter(com);
                DataSet dt2 = new DataSet();
                adpt2.Fill(dt2);
                adpt2.SelectCommand = com;

                if (dt2.Tables[0].Rows.Count > 0)
                {
                    GridSalary.DataSource = dt2;
                    GridSalary.DataBind();

                    divSalary.Visible = true;
                }
                com.Parameters.Clear();
                com.CommandText = "sp_getDistinctNameSalaryReportTemporary";
                adpt2.SelectCommand = com;
                DataSet dt3 = new DataSet();
                adpt2.Fill(dt3);
                if (dt3.Tables[0].Rows.Count > 0)
                {
                    GridUsers2.DataSource = dt3;
                    GridUsers2.DataBind();
                }
                com.Parameters.Clear();
                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "Delete from SalaryReportTemporary";
                com.ExecuteNonQuery();

                btnSave.Visible = false;
                btnView.Visible = true;
                btnPDF.Visible = true;

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Salary Submitted');", true);
                return;
            }
        }

        void DDLStockItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            //LblItemFees.Text = DDLStockItem.SelectedValue; 
            DropDownList DDLStockItem = (DropDownList)sender;
            GridViewRow grdrRow = ((GridViewRow)DDLStockItem.Parent.Parent);
            Label LblItemFees = (Label)grdrRow.FindControl("LblItemFees");
            TextBox txtSubtotal = (TextBox)grdrRow.FindControl("txtSubtotal");
            TextBox txtQuantity = (TextBox)grdrRow.FindControl("txtQuantity");

            LblItemCode.Text = DDLStockItem.SelectedValue;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            DropDownList DDLStockItem1 = (DropDownList)sender;
            com.CommandText = "select ItemFees from STOCKSITEM where ItemCode= " + DDLStockItem1.SelectedValue;
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            LblItemFees.Text = dt.Tables[0].Rows[0]["ItemFees"].ToString();
            decimal total = Convert.ToDecimal(txtSubtotal.Text);
            if (txtQuantity.Text != "0" || LblItemFees.Text != "0")
            {
                decimal qty = Convert.ToDecimal(txtQuantity.Text);
                decimal IFees = Convert.ToDecimal(LblItemFees.Text);
                decimal totFees = qty * IFees;
                txtSubtotal.Text = Convert.ToDecimal(total + totFees).ToString();
                total = 0;
            }

            con.Close();
        }

        protected void RBtnUseQuate_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseLight.Checked = false;
            DropDownUserList.Visible = false;
            divUsers.Visible = true;
            LblGrantTotal.Text = "";
            //btnSave.Visible = true;
            //btnView.Visible = false;
            finishDiv.Visible = false;
        }

        protected void RBtnUseLight_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseQuate.Checked = false;
            DropDownUserList.Visible = false;
            divUsers.Visible = true;
            //btnSave.Visible = false;
            //btnView.Visible = false;
            LblGrantTotal.Text = "";
            BindStock();
            finishDiv.Visible = true;
        }

        public void BindStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from STOCKSITEM where UseLight = 1", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GviewProjectStock.DataSource = dt;

                GviewProjectStock.DataBind();
            }
            dt.Rows.Clear();
            con.Close();
        }

        protected void RBtnUseHeavy_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseLight.Checked = false;
            RBtnUseQuate.Checked = false;
            DropDownUserList.Visible = false;
            divUsers.Visible = true;
            LblGrantTotal.Text = "";
            //btnSave.Visible = false;
            //btnView.Visible = false;
            BindHeavyStock();
            finishDiv.Visible = true;
        }

        public void BindHeavyStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from STOCKSITEM where UseHeavy = 1", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GviewProjectStock.DataSource = dt;

                GviewProjectStock.DataBind();
            }
            dt.Rows.Clear();
            con.Close();
        }

        protected void DropDownUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LBLUSERID1.Text = DropDownUserList.SelectedValue;
            Session["UserName"] = DropDownUserList.SelectedItem.Text;
            Session["ProName"] = ddProjectList.SelectedItem.Text;
            BindTaskDetails();

            ddlSubtask.Items.Clear();
            gridTask.DataSource = null;
            gridTask.DataBind();

            SqlConnection con11 = new SqlConnection(cs);
            SqlCommand cmd11 = new SqlCommand();
            cmd11.Connection = con11;
            con11.Open();
            cmd11.CommandType = System.Data.CommandType.Text;
            cmd11.Parameters.Clear();
            cmd11.CommandText = "Delete FROM TempSalaryTask";
            cmd11.ExecuteNonQuery();
            cmd11.Parameters.Clear();
            con11.Close();
        }

        protected void ddlSubtask_SelectedIndexChanged(object sender, EventArgs e)
        {

            resetTask();
        }

        public void resetTask()
        {

            int taskid;
            int subtskid;
            SqlConnection con11 = new SqlConnection(cs);
            SqlCommand cmd11 = new SqlCommand();
            cmd11.Connection = con11;
            con11.Open();

            //cmd11.CommandType = System.Data.CommandType.Text;
            //cmd11.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TEMPTASK";
            //taskid = Convert.ToInt32(cmd11.ExecuteScalar());
            //lbltaskid.Text = taskid.ToString();
            cmd11.Parameters.Clear();
            taskid = Convert.ToInt32(ddTaskList.SelectedValue);
            //cmd11.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TempSalaryTask where TaskId=" + taskid + "";
            subtskid = Convert.ToInt32(ddlSubtask.SelectedValue);

            cmd11.CommandType = System.Data.CommandType.StoredProcedure;
            cmd11.CommandText = "sp_InsertTempSalaryTask";
            cmd11.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
            cmd11.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = subtskid;
            cmd11.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = ddlSubtask.SelectedItem.Text;
            cmd11.Parameters.Add("@StockItem", System.Data.SqlDbType.VarChar).Value = "";
            cmd11.Parameters.Add("@Qty", System.Data.SqlDbType.VarChar).Value = "0";
            cmd11.Parameters.Add("@HrW", System.Data.SqlDbType.Time).Value = "00:00";
            cmd11.Parameters.Add("@Wage", System.Data.SqlDbType.Decimal).Value = 0;
            cmd11.Parameters.Add("@StartTime", System.Data.SqlDbType.VarChar).Value = "";
            cmd11.Parameters.Add("@StopTime", System.Data.SqlDbType.VarChar).Value = "";
            cmd11.Parameters.Add("@IFEES", System.Data.SqlDbType.VarChar).Value = "0";
            cmd11.Parameters.Add("@TOTAL", System.Data.SqlDbType.Decimal).Value = 0;

            cmd11.ExecuteNonQuery();
            cmd11.Parameters.Clear();

            cmd11.CommandType = System.Data.CommandType.Text;
            cmd11.CommandText = "SELECT distinct * FROM TempSalaryTask";
            SqlDataAdapter dtask = new SqlDataAdapter(cmd11);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();

            cmd11.Parameters.Clear();
            con11.Close();

            //lbltaskid.Text = Convert.ToInt32(taskid).ToString();
            //TaskId.Text = lbltaskid.Text;
        }

        protected void btnAddNewTask_Click(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand();
            com.Parameters.Clear();
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;

            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSTID = (rows.Cells[0].FindControl("lblSTID") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[0].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.TextBox txtHrW = (rows.Cells[1].FindControl("txtHrW") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.TextBox txtStartStocktime = (rows.Cells[2].FindControl("txtStartStocktime") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.TextBox txtStopStockTime = (rows.Cells[3].FindControl("txtStopStockTime") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLStockItem = (rows.Cells[4].FindControl("DDLStockItem") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.Label LblItemFees = (rows.Cells[4].FindControl("LblItemFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtQuantity = (rows.Cells[5].FindControl("txtQuantity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.TextBox txtSubtotal = (rows.Cells[6].FindControl("txtSubtotal") as System.Web.UI.WebControls.TextBox);

                    //if (txtTask.Text == "")
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                    //    return;
                    //}

                    //else
                    //{
                    decimal total = Convert.ToDecimal(txtSubtotal.Text);

                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_UpdateTempSalaryTask";
                    com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                    com.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = lblSTID.Text;
                    com.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                    com.Parameters.Add("@StockItem", System.Data.SqlDbType.VarChar).Value = DDLStockItem.SelectedItem.Text;
                    com.Parameters.Add("@Qty", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                    if (txtHrW.Text != "")
                    {
                        com.Parameters.Add("@HrW", System.Data.SqlDbType.Time).Value = txtHrW.Text;
                    }
                    else
                    {
                        com.Parameters.Add("@HrW", System.Data.SqlDbType.Time).Value = "00:00";
                    }
                    com.Parameters.Add("@Wage", System.Data.SqlDbType.Decimal).Value = 0;
                    com.Parameters.Add("@StartTime", System.Data.SqlDbType.VarChar).Value = txtStartStocktime.Text;
                    com.Parameters.Add("@StopTime", System.Data.SqlDbType.VarChar).Value = txtStopStockTime.Text;
                    com.Parameters.Add("@IFEES", System.Data.SqlDbType.VarChar).Value = LblItemFees.Text;
                    com.Parameters.Add("@TOTAL", System.Data.SqlDbType.Decimal).Value = total;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    //com.CommandType = System.Data.CommandType.Text;
                    //com.CommandText = "SELECT * FROM TEMPTASK";
                    //SqlDataAdapter dtask = new SqlDataAdapter(com);
                    //DataTable dttask = new DataTable();
                    //dtask.Fill(dttask);
                    //gridTask.DataSource = dttask;
                    //gridTask.DataBind();


                    // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details Added SuccessFully');", true);

                    //resetall();
                    //}
                }
            }

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT distinct * FROM TempSalaryTask";
            SqlDataAdapter dtask = new SqlDataAdapter(com);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();

            com.Parameters.Clear();
            con.Close();
            //resetTask();
        }

        protected void gridTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList DDLStockItem = (DropDownList)e.Row.FindControl("DDLStockItem");
                Label lbltaskid = (Label)e.Row.FindControl("lbltaskid");
                Label lblSTID = (Label)e.Row.FindControl("lblSTID");
                TextBox txtHoursWorked = (TextBox)e.Row.FindControl("txtHrW");
                TextBox txtSubtotal = (TextBox)e.Row.FindControl("txtSubtotal");


                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_getOrderInfoForProject";

                com.Parameters.Add("@I_PRJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                SqlDataAdapter adp1 = new SqlDataAdapter(com);
                DataSet dtt1 = new DataSet();
                adp1.Fill(dtt1);
                DDLStockItem.DataSource = dtt1;
                DDLStockItem.DataTextField = "ItemName";
                DDLStockItem.DataValueField = "ItemCode";
                DDLStockItem.DataBind();
                DDLStockItem.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Vælg-", "0"));
                com.Parameters.Clear();

                dtt1.Tables[0].Rows.Clear();
                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "SELECT StockItem,TaskName FROM TempSalaryTask WHERE TASKID=" + lbltaskid.Text + "AND STID=" + lblSTID.Text + "";
                adp1.SelectCommand = com;
                adp1.Fill(dtt1);
                if (dtt1.Tables[0].Rows.Count > 0)
                {
                    if (dtt1.Tables[0].Rows[0]["TaskName"] != "")
                    {
                        DDLStockItem.SelectedItem.Text = dtt1.Tables[0].Rows[0]["StockItem"].ToString();

                    }
                    else
                    {
                        DDLStockItem.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Vælg-", "0"));
                    }


                }

                com.Parameters.Clear();

                com.CommandText = "select DISTINCT E.TOTALSALARY,E.TOTALHOURS from EMPLOYEESALARYDETAILS E ,HoursWorked H where E.EMPID=H.EMPID AND E.EMPID=" + DropDownUserList.SelectedValue + " AND H.TASKID=" + lbltaskid.Text + " AND H.STID=" + lblSTID.Text + "";
                SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                DataSet ds1 = new DataSet();
                adpt1.Fill(ds1, "EMPLOYEESALARYDETAILS");
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    if (txtSubtotal.Text == "0.00")
                    {
                        txtSubtotal.Text = ds1.Tables["EMPLOYEESALARYDETAILS"].Rows[0]["TOTALSALARY"].ToString();
                    }
                    txtHoursWorked.Text = ds1.Tables["EMPLOYEESALARYDETAILS"].Rows[0]["TOTALHOURS"].ToString();
                }
                //com.Parameters.Clear();

                //com.CommandText = "select TOTALSALARY,TOTALHOURS from EMPLOYEESALARYDETAILS where EMPID=" + DropDownUserList.SelectedValue + "";
                //SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                //DataSet ds1 = new DataSet();
                //adpt1.Fill(ds1, "EMPLOYEESALARYDETAILS");
                //txtHoursWorked.Text = ds1.Tables["EMPLOYEESALARYDETAILS"].Rows[0]["TOTALHOURS"].ToString();
                //txtSubtotal.Text = ds1.Tables["EMPLOYEESALARYDETAILS"].Rows[0]["TOTALSALARY"].ToString();

                //txtTotalSalary.Text = ds1.Tables["EMPLOYEESALARYDETAILS"].Rows[0]["TOTALSALARY"].ToString();

                com.Parameters.Clear();

                conn.Close();
            }
        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            TextBox txtQuantity = (TextBox)sender;
            GridViewRow grdrRow = ((GridViewRow)txtQuantity.Parent.Parent);
            Label LblItemFees = (Label)grdrRow.FindControl("LblItemFees");
            TextBox txtSubtotal = (TextBox)grdrRow.FindControl("txtSubtotal");
            decimal total = Convert.ToDecimal(txtSubtotal.Text);

            if (txtQuantity.Text != "0" || LblItemFees.Text != "0")
            {
                decimal qty = Convert.ToDecimal(txtQuantity.Text);
                decimal IFees = Convert.ToDecimal(LblItemFees.Text);
                decimal totFees = qty * IFees;
                txtSubtotal.Text = Convert.ToDecimal(total + totFees).ToString();
                total = 0;
            }

        }

        protected void txtHours_TextChanged(object sender, EventArgs e)
        {


            //float total = 0;
            //foreach (GridViewRow gridViewRow in gridUsers.Rows)
            //{

            //    if (gridViewRow.RowType == DataControlRowType.DataRow)
            //    {

            //        // System.Web.UI.WebControls.TextBox thours = (gridViewRow.Cells[4].FindControl("thours") as System.Web.UI.WebControls.TextBox);
            //        txtHours = (TextBox)gridViewRow.FindControl("txtHours");

            //        float rowValue = 0;
            //        if (float.TryParse(txtHours.Text.Trim(), out rowValue))
            //            total += rowValue;

            //    }
            //}
            //lblTotalHR.Text = total.ToString();


        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            if (ddProjectList.SelectedItem.Text != "-Vælg-")
            {
                if (ddlUser.SelectedItem.Text != "-Vælg-")
                {


                    string dtEntry1 = txtStartTime.Text;
                    DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "DELETE FROM ASSIGNED WHERE ProjectId=@PROJECTID AND EMPID=@EMPID";
                    com.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = ddProjectList.SelectedValue;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = ddlUser.SelectedValue;
                    com.ExecuteNonQuery();

                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_UpdateAssigned";
                    com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = 0;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = ddlUser.SelectedValue;
                    com.Parameters.Add("@ASSIGN_PROJECTID  ", System.Data.SqlDbType.BigInt).Value = ddProjectList.SelectedValue;
                    com.Parameters.Add("@ASSIGNEDDATE", System.Data.SqlDbType.DateTime).Value = dt11;
                    //com.Parameters.Add("@ASSIGNEDDATE", System.Data.SqlDbType.DateTime).Value = dtp1.CalendarDateString;          
                    com.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = 0;
                    com.Parameters.Add("@ESTIMATEDHOURS", System.Data.SqlDbType.Decimal).Value = 0;
                    com.Parameters.Add("@LABORCHARGES", System.Data.SqlDbType.Decimal).Value = 0;
                    com.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = 0;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "Select distinct * from EMPLOYEE,ASSIGNED,PROJECTS where ASSIGNED.ProjectId=PROJECTS.ProjectId and EMPLOYEE.EmpId=ASSIGNED.EmpId and PROJECTS.ProjectId= " + ddProjectList.SelectedValue;
                    SqlDataAdapter adpt = new SqlDataAdapter(com);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        gridUsers.DataSource = dt;
                        gridUsers.DataBind();
                    }
                    com.Parameters.Clear();
                    dt.Tables[0].Rows.Clear();
                }
            }
            con.Close();
        }

        protected void txtQuntity_TextChanged(object sender, EventArgs e)
        {
            //LblGrantTotal.Text = "";

            TextBox txtQuntity = (TextBox)sender;
            GridViewRow grdrRow = ((GridViewRow)txtQuntity.Parent.Parent);
            Label ItemFees = (Label)grdrRow.FindControl("ItemFees");
            Label LblTotAmt = (Label)grdrRow.FindControl("LblTotAmt");
            Label lblQuntity = (Label)grdrRow.FindControl("lblQuntity");
            //TextBox txtSubtotal = (TextBox)grdrRow.FindControl("txtSubtotal");
            if (txtQuntity.Text != "")
            {
                if (ItemFees.Text != "")
                {
                    decimal Price = Convert.ToDecimal(ItemFees.Text);
                    long qty = Convert.ToInt64(txtQuntity.Text);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    //pricqty = pricqty / 100;
                    total = total + pricqty;
                    LblTotAmt.Text = total.ToString();

                    if (LblGrantTotal.Text != "")
                    {
                        LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(LblTotAmt.Text)).ToString();
                        //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                    }
                    else
                    {
                        LblGrantTotal.Text = LblTotAmt.Text;
                        //txttot.Text = lblTot.Text;
                    }
                    lblQuntity.Text = txtQuntity.Text;
                    lblQuntity.Visible = true;
                    txtQuntity.Visible = false;

                }
                else
                {
                    txtQuntity.Text = "";
                }

                if (LblGrantTotal.Text != "")
                {
                    Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text);
                    //Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text) / 100;
                }
                else
                {
                    Global.TotIFees = 0;
                }
            }


            //foreach (GridViewRow rows in GviewProjectStock.Rows)
            //{

            //    if (rows.RowType == DataControlRowType.DataRow)
            //    {
            //        //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("stkChkbox") as System.Web.UI.WebControls.CheckBox);
            //        System.Web.UI.WebControls.TextBox txtQuntity = (rows.Cells[1].FindControl("txtQuntity") as System.Web.UI.WebControls.TextBox);
            //        System.Web.UI.WebControls.Label ItemFees = (rows.Cells[2].FindControl("ItemFees") as System.Web.UI.WebControls.Label);
            //        System.Web.UI.WebControls.Label LblTotAmt = (rows.Cells[3].FindControl("LblTotAmt") as System.Web.UI.WebControls.Label);
            //        System.Web.UI.WebControls.Label lblQuntity = (rows.Cells[1].FindControl("lblQuntity") as System.Web.UI.WebControls.Label);
            //        //System.Web.UI.WebControls.DropDownList LblTotAmt1 = (rows.Cells[4].FindControl("LblTotAmt1") as System.Web.UI.WebControls.DropDownList);


            //        if (txtQuntity.Text != "")
            //        {

            //            if (ItemFees.Text != "")
            //            {

            //                decimal Price = Convert.ToDecimal(ItemFees.Text);
            //                long qty = Convert.ToInt64(txtQuntity.Text);
            //                decimal total = 0;
            //                decimal pricqty = 0;
            //                pricqty = Price * qty;
            //                pricqty = pricqty / 100;
            //                total = total + pricqty;
            //                LblTotAmt.Text = total.ToString();

            //                //if (ViewState["TotalPrice"] == null)
            //                //{
            //                //    ViewState["TotalPrice"] = lblTot.Text;
            //                //}
            //                //if (rows.RowType == DataControlRowType.Footer)
            //                //{
            //                //    System.Web.UI.WebControls.TextBox txttot = (rows.Cells[4].FindControl("txttot") as System.Web.UI.WebControls.TextBox);
            //                if (LblGrantTotal.Text != "")
            //                {
            //                    LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(LblTotAmt.Text)).ToString();
            //                    //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

            //                }
            //                else
            //                {
            //                    LblGrantTotal.Text = LblTotAmt.Text;
            //                    //txttot.Text = lblTot.Text;
            //                }
            //                lblQuntity.Text = txtQuntity.Text;
            //                lblQuntity.Visible = true;
            //                txtQuntity.Visible = false;
            //            }
            //            else
            //            {
            //                txtQuntity.Text = "";
            //            }

            //        }
            //    }
            //}
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            //if (LblGrantTotal.Text != "")
            //{
            //    Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text);
            //    //Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text) / 100;
            //}
            //else
            //{
            //    Global.TotIFees = 0;
            //}
        }

        protected void GviewProjectStock_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            string type = "";
            foreach (GridViewRow Srows in GviewProjectStock.Rows)
            {
                if (Srows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label LblItemcode = (Srows.Cells[0].FindControl("LblItemcode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label ItemName = (Srows.Cells[1].FindControl("ItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.DropDownList DDLItemName = (Srows.Cells[1].FindControl("DDLItemName") as System.Web.UI.WebControls.DropDownList);

                    if (RBtnUseLight.Checked == true)
                    {
                        type = "LightWeight";
                    }
                    else if (RBtnUseHeavy.Checked == true)
                    {
                        type = "HeavyWeight";
                    }

                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "SELECT M.ItemCode,M.ItemName,M.ItemFees,M.ICODE FROM MULTIVALUESTOCK M,STOCKSITEM S WHERE M.ItemCode=S.ItemCode AND M.ITEMCODE=@ITEMCODE AND M.StockType=@STOCKTYPE";
                    com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = LblItemcode.Text;
                    com.Parameters.Add("@STOCKTYPE", System.Data.SqlDbType.VarChar).Value = type;

                    SqlDataAdapter adpt = new SqlDataAdapter(com);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        DDLItemName.DataSource = dt;
                        DDLItemName.DataTextField = "ItemName";
                        DDLItemName.DataValueField = "ICODE";
                        DDLItemName.DataBind();
                        ItemName.Visible = false;
                        DDLItemName.Visible = true;
                    }

                }
            }

            con.Close();
        }

        protected void DDLItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            string type = "";

            if (RBtnUseLight.Checked == true)
            {
                type = "LightWeight";
            }
            else if (RBtnUseHeavy.Checked == true)
            {
                type = "HeavyWeight";
            }

            DropDownList DDLItemName = (DropDownList)sender;
            GridViewRow grdrRow = ((GridViewRow)DDLItemName.Parent.Parent);
            Label ItemFees = (Label)grdrRow.FindControl("ItemFees");
            Label ItemName = (Label)grdrRow.FindControl("ItemName");
            TextBox txtItemFees = (TextBox)grdrRow.FindControl("txtItemFees");
            ItemName.Text = DDLItemName.SelectedItem.Text;

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT M.ItemCode,M.ItemName,M.ItemFees FROM MULTIVALUESTOCK M,STOCKSITEM S WHERE M.ItemCode=S.ItemCode AND M.ICODE=@ICODE AND M.StockType=@STOCKTYPE";
            com.Parameters.Add("@ICODE", System.Data.SqlDbType.BigInt).Value = DDLItemName.SelectedValue;
            com.Parameters.Add("@STOCKTYPE", System.Data.SqlDbType.VarChar).Value = type;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                ItemFees.Text = dt.Tables[0].Rows[0]["ItemFees"].ToString();
                if (ItemFees.Text != "")
                {
                    ItemFees.Visible = true;
                    txtItemFees.Visible = false;
                }
                else
                {
                    ItemFees.Visible = false;
                    txtItemFees.Visible = true;
                }


            }
            con.Close();
        }

        protected void DDLStockItem_SelectedIndexChanged1(object sender, EventArgs e)
        {
            DropDownList DDLStockItem = (DropDownList)sender;
            GridViewRow grdrRow = ((GridViewRow)DDLStockItem.Parent.Parent);
            Label LblItemFees = (Label)grdrRow.FindControl("LblItemFees");
            TextBox txtSubtotal = (TextBox)grdrRow.FindControl("txtSubtotal");
            TextBox txtQuantity = (TextBox)grdrRow.FindControl("txtQuantity");

            LblItemCode.Text = DDLStockItem.SelectedValue;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            DropDownList DDLStockItem1 = (DropDownList)sender;
            com.CommandText = "select ItemFees from STOCKSITEM where ItemCode= " + DDLStockItem1.SelectedValue;
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);
            LblItemFees.Text = dt.Tables[0].Rows[0]["ItemFees"].ToString();
            decimal total = Convert.ToDecimal(txtSubtotal.Text);
            if (txtQuantity.Text != "0" || LblItemFees.Text != "0")
            {
                decimal qty = Convert.ToDecimal(txtQuantity.Text);
                decimal IFees = Convert.ToDecimal(LblItemFees.Text);
                decimal totFees = qty * IFees;
                txtSubtotal.Text = Convert.ToDecimal(total + totFees).ToString();
                total = 0;
            }

            con.Close();
        }

        protected void DDLLunch_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList DDllunch = (DropDownList)sender;
            GridViewRow Row = (GridViewRow)DDllunch.NamingContainer;
            CheckBox chkUser = (CheckBox)Row.FindControl("chkUser");
            Label lblpopup = (Label)Row.FindControl("LblPopup_image");

            if (chkUser.Checked == true)
            {
                if (DDllunch.SelectedItem.Text == "No")
                {
                    Row.BackColor = System.Drawing.Color.Pink;
                    lblpopup.Visible = true;
                }
                else
                {
                    Row.BackColor = System.Drawing.Color.White;
                    lblpopup.Visible = false;
                }
            }
            else
            {
                Row.BackColor = System.Drawing.Color.White;
                lblpopup.Visible = false;
            }
        }

        protected void chkUser_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            GridViewRow row = (GridViewRow)cb.NamingContainer;
            DropDownList DDllunch = (DropDownList)row.FindControl("DDLLunch");
            Label lblpopup = (Label)row.FindControl("LblPopup_image");

            if (cb.Checked == true)
            {
                if (DDllunch.SelectedItem.Text == "No")
                {
                    row.BackColor = System.Drawing.Color.Pink;
                    lblpopup.Visible = true;
                }
                else
                {
                    row.BackColor = System.Drawing.Color.White;
                    lblpopup.Visible = false;
                }
            }
            else
            {
                row.BackColor = System.Drawing.Color.White;
                lblpopup.Visible = false;
            }
        }

        protected void btnPDF_Click(object sender, EventArgs e)
        {

            lblUser.Text = ddlUser.SelectedItem.Text;
            lblProjectList1.Text = ddProjectList.SelectedItem.Text;

            if (RBtnUseQuate.Checked)
            {
                lblRadioButton.Text = "HourBased";
            }
            else if (RBtnUseLight.Checked)
            {
                lblRadioButton.Text = "Lite Scaffold";
            }
            else
            {
                lblRadioButton.Text = "Heavy Scaffold";
            }
            lblTime.Text = txtStartTime.Text;
            ddlUser.Visible = false;
            ddProjectList.Visible = false;
            txtStartTime.Visible = false;
            RBtnUseQuate.Visible = false;
            RBtnUseLight.Visible = false;
            RBtnUseHeavy.Visible = false;
            lblRadioButton.Visible = true;
            lblUser.Visible = true;
            lblTime.Visible = true;
            lblProjectList1.Visible = true;
            Div1.Visible = false;
            Div2.Visible = false;
            Div4.Visible = false;
            Div5.Visible = false;
            PDFDIV1.Visible = false;
            Div7.Visible = true;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.Parameters.Clear();

            string path = Server.MapPath("~/SalaryModulePDF/");
            //string imagepath = Server.MapPath("~/image/");
            //string imagepath = Server.MapPath("\\image\\pmglogo1.jpg");

            string attachment = "attachment;filename=SalaryPDF" + lblProjectId.Text + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", attachment);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringWriter sw = new StringWriter();
            StringWriter sw1 = new StringWriter();
            StringWriter sw2 = new StringWriter();
            StringWriter sw3 = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            HtmlTextWriter hw2 = new HtmlTextWriter(sw2);
            HtmlTextWriter hw3 = new HtmlTextWriter(sw3);

            //DisableAjaxExtenders(this);

            //PDFDIV1.RenderControl(hw);
            Div7.RenderControl(hw);
            PDFDIV2.RenderControl(hw1);
            divUsers.RenderControl(hw2);
            divSalary.RenderControl(hw3);
            //Div4.RenderControl(hw1);

            //DivFooter.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            StringReader sr1 = new StringReader(sw1.ToString());
            StringReader sr2 = new StringReader(sw2.ToString());
            StringReader sr3 = new StringReader(sw3.ToString());

            //Document pdfDoc = new Document(PageSize.A4, 40f, 40f, 105f, 60f);
            Document pdfDoc = new Document(PageSize.A4, 40f, 40f, 55f, 60f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            FileStream file = new FileStream(path + "/SalaryPDF" + lblProjectId.Text + ".pdf", FileMode.Create);

            PdfWriter writer1 = PdfWriter.GetInstance(pdfDoc, file);
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //PdfWriter writer1 = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "delete from SalaryModulePDF where ProjectID='" + lblProjectId.Text + "'";
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "insert into SalaryModulePDF (ProjectID,FIleName,FilePath) values(@PROJECTID,@NAME,@PATH)";
            com.Parameters.AddWithValue("@PROJECTID", lblProjectId.Text);
            com.Parameters.AddWithValue("@NAME", System.Data.SqlDbType.NVarChar).Value = "SalaryPDF" + lblProjectId.Text + ".pdf";
            com.Parameters.AddWithValue("@PATH", System.Data.SqlDbType.NVarChar).Value = (path + "/SalaryPDF" + lblProjectId.Text + ".pdf");
            com.ExecuteNonQuery();
            con.Close();

            if (GridUsers2.Rows.Count > 0)
            {
                PdfPTable table = new PdfPTable(GridUsers2.Columns.Count);
                table.WidthPercentage = 30;

                Font NFont = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
                Font NFont1 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10);
                //table.Cellpadding = 5;
                //Set the column widths
                int[] widths = new int[GridUsers2.Columns.Count];
                for (int x = 0; x < GridUsers2.Columns.Count; x++)
                {
                    //Phrase phrs = new Phrase();
                    widths[x] = (int)GridUsers2.Columns[x].ItemStyle.Width.Value;
                    string cellText = Server.HtmlDecode(GridUsers2.HeaderRow.Cells[x].Text);
                    //phrs.Add(new Chunk(cellText));
                    PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont));
                    //cell.BorderColor = BaseColor.WHITE;
                    cell.Border = 0;
                    cell.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderColorLeft = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderColorRight = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderWidthTop = 1f;
                    cell.BorderWidthBottom = 1f;
                    cell.BorderWidthRight = 0.5f;
                    cell.BorderWidthLeft = 0.5f;

                    cell.PaddingBottom = 3;
                    //cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    table.AddCell(cell);
                }

                table.SetWidths(widths);
                //Transfer rows from GridView to table

                for (int i = 0; i < GridUsers2.Rows.Count; i++)
                {
                    if (GridUsers2.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        for (int j = 0; j < GridUsers2.Columns.Count; j++)
                        {
                            //Phrase phrs = new Phrase();
                            string cellText = Server.HtmlDecode(GridUsers2.Rows[i].Cells[j].Text);

                            //phrs.Add(new Chunk(cellText));
                            PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont1));
                            cell.BorderColor = BaseColor.BLACK;
                            //Set Color of Alternating row
                            //if (i % 2 != 0)
                            //{
                            //    cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#C2D69B"));
                            //}

                            table.AddCell(cell);
                        }
                    }
                }

                if (GridSalary.Rows.Count > 0)
                {
                    PdfPTable table2 = new PdfPTable(GridSalary.Columns.Count);
                    table2.WidthPercentage = 100;

                    Font NFont4 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
                    Font NFont5 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10);
                    //Set the column widths
                    int[] widths2 = new int[GridSalary.Columns.Count];
                    for (int x = 0; x < GridSalary.Columns.Count; x++)
                    {
                        //Phrase phrs = new Phrase();
                        widths2[x] = (int)GridSalary.Columns[x].ItemStyle.Width.Value;
                        string cellText = Server.HtmlDecode(GridSalary.HeaderRow.Cells[x].Text);
                        //phrs.Add(new Chunk(cellText));
                        PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont4));
                        cell.BorderColor = BaseColor.BLACK;
                        cell.Border = 0;
                        cell.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                        cell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        cell.BorderColorLeft = new BaseColor(System.Drawing.Color.Black);
                        cell.BorderColorRight = new BaseColor(System.Drawing.Color.Black);
                        cell.BorderWidthTop = 1f;
                        cell.BorderWidthBottom = 1f;
                        cell.BorderWidthRight = 0.5f;
                        cell.BorderWidthLeft = 0.5f;
                        cell.PaddingBottom = 3;
                        //cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        table2.AddCell(cell);
                    }

                    table2.SetWidths(widths2);
                    //Transfer rows from GridView to table

                    for (int i = 0; i < GridSalary.Rows.Count; i++)
                    {
                        if (GridSalary.Rows[i].RowType == DataControlRowType.DataRow)
                        {
                            for (int j = 0; j < GridSalary.Columns.Count; j++)
                            {
                                //Phrase phrs = new Phrase();
                                string cellText = Server.HtmlDecode(GridSalary.Rows[i].Cells[j].Text);

                                //phrs.Add(new Chunk(cellText));
                                PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont5));

                                cell.BorderColor = BaseColor.BLACK;
                                //Set Color of Alternating row
                                //if (i % 2 != 0)
                                //{
                                //    cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#C2D69B"));
                                //}

                                table2.AddCell(cell);
                            }
                        }
                    }
                    pdfDoc.Open();
                    Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                    Phrase ph1 = new Phrase(Environment.NewLine);

                    htmlparser.Parse(sr);
                    pdfDoc.Add(p);
                    htmlparser.Parse(sr2);
                    pdfDoc.Add(new Paragraph("\n"));
                    pdfDoc.Add(table);
                    pdfDoc.Add(p);
                    //pdfDoc.Add(ph1);
                    htmlparser.Parse(sr3);
                    pdfDoc.Add(new Paragraph("\n"));
                    pdfDoc.Add(table2);
                    pdfDoc.Close();
                    Response.Write(pdfDoc);
                    OpenPDF();
                    lblRadioButton.Visible = false;
                    lblUser.Visible = false;
                    lblTime.Visible = false;
                    lblProjectList1.Visible = false;
                    ddlUser.Visible = true;
                    ddProjectList.Visible = true;
                    txtStartTime.Visible = true;
                    RBtnUseQuate.Visible = true;
                    RBtnUseLight.Visible = true;
                    RBtnUseHeavy.Visible = true;
                    Div1.Visible = true;
                    Div2.Visible = true;
                    Div4.Visible = true;
                    Div5.Visible = true;
                    Div7.Visible = false;
                    PDFDIV1.Visible = true;
                    btnPDF.Visible = false;
                    //btnView.Enabled = true;
                    //Response.End();
                }

            }

        }

        private void DisableAjaxExtenders(Control parent)
        {
            for (int i = 0; i < parent.Controls.Count; i++)
            {
                if (parent.Controls[i].GetType().ToString().IndexOf("Extender") != -1 && parent.Controls[i].ID != null)
                {
                    parent.Controls.RemoveAt(i);
                    parent.Controls[i].Dispose();
                }
                if (parent.Controls[i].Controls.Count > 0)
                {
                    DisableAjaxExtenders(parent.Controls[i]);
                }
            }
        }

        public void OpenPDF()
        {
            string FilePath = null;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;

            com.CommandText = "select FilePath from SalaryModulePDF where ProjectID ='" + lblProjectId.Text + "'";
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                FilePath = dt.Rows[0][0].ToString();
            }
            else
            {

            }

            con.Close();
            try
            {
                //view document file 
                if (FilePath.Contains(".doc"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                //view pdf files 
                if (FilePath.Contains(".pdf"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                if (FilePath.Contains(".htm"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                if (FilePath.Contains(".txt"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                else
                {
                    //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('File not in Proper format');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('File not in Proper format');", true);
            }
        }

        protected void btnQuantity_Click(object sender, EventArgs e)
        {
            Button btnQuantity = (Button)sender;
            GridViewRow grdrRow = ((GridViewRow)btnQuantity.NamingContainer);

            TextBox txtQuntity = (TextBox)grdrRow.FindControl("txtQuntity");
            Label ItemFees = (Label)grdrRow.FindControl("ItemFees");
            Label LblTotAmt = (Label)grdrRow.FindControl("LblTotAmt");
            Label lblQuntity = (Label)grdrRow.FindControl("lblQuntity");
            //TextBox txtSubtotal = (TextBox)grdrRow.FindControl("txtSubtotal");
            if (txtQuntity.Text != "")
            {
                if (ItemFees.Text != "")
                {
                    decimal Price = Convert.ToDecimal(ItemFees.Text);
                    long qty = Convert.ToInt64(txtQuntity.Text);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    //pricqty = pricqty / 100;
                    total = total + pricqty;
                    LblTotAmt.Text = total.ToString();

                    if (LblGrantTotal.Text != "")
                    {
                        LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(LblTotAmt.Text)).ToString();
                        //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                    }
                    else
                    {
                        LblGrantTotal.Text = LblTotAmt.Text;
                        //txttot.Text = lblTot.Text;
                    }
                    lblQuntity.Text = txtQuntity.Text;
                    lblQuntity.Visible = true;
                    txtQuntity.Visible = false;

                }
                else
                {
                    txtQuntity.Text = "";
                }

                if (LblGrantTotal.Text != "")
                {
                    Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text);
                    //Global.TotIFees = Convert.ToDecimal(LblGrantTotal.Text) / 100;
                }
                else
                {
                    Global.TotIFees = 0;
                }
            }
        }

        //protected void DDLItemName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    SqlConnection con = new SqlConnection(cs);
        //    con.Open();
        //    SqlCommand com = new SqlCommand();
        //    com.Connection = con;
        //    string type = "";

        //    if (RBtnUseLight.Checked == true)
        //    {
        //        type = "LightWeight";
        //    }
        //    else if (RBtnUseHeavy.Checked == true)
        //    {
        //        type = "HeavyWeight";
        //    }

        //    DropDownList DDLItemName = (DropDownList)sender;
        //    GridViewRow grdrRow = ((GridViewRow)DDLItemName.Parent.Parent);
        //    Label ItemFees = (Label)grdrRow.FindControl("ItemFees");
        //    Label ItemName = (Label)grdrRow.FindControl("ItemName");
        //    TextBox txtItemFees = (TextBox)grdrRow.FindControl("txtItemFees");
        //    ItemName.Text = DDLItemName.SelectedItem.Text;

        //    com.Parameters.Clear();
        //    com.CommandType = System.Data.CommandType.Text;
        //    com.CommandText = "SELECT M.ItemCode,M.ItemName,M.ItemFees FROM MULTIVALUESTOCK M,STOCKSITEM S WHERE M.ItemCode=S.ItemCode AND M.ICODE=@ICODE AND M.StockType=@STOCKTYPE";
        //    com.Parameters.Add("@ICODE", System.Data.SqlDbType.BigInt).Value = DDLItemName.SelectedValue;
        //    com.Parameters.Add("@STOCKTYPE", System.Data.SqlDbType.VarChar).Value = type;

        //    SqlDataAdapter adpt = new SqlDataAdapter(com);
        //    DataSet dt = new DataSet();
        //    adpt.Fill(dt);
        //    if (dt.Tables[0].Rows.Count > 0)
        //    {
        //        ItemFees.Text = dt.Tables[0].Rows[0]["ItemFees"].ToString();
        //        if(ItemFees.Text !="")
        //        {
        //            ItemFees.Visible = true;
        //            txtItemFees.Visible = false;
        //        }
        //        else
        //        {
        //            ItemFees.Visible = false;
        //            txtItemFees.Visible = true;
        //        }


        //    }
        //    con.Close();


        //}
    }
}