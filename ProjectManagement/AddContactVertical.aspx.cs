﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Data;
using System.Collections;
using System.Net;
using System.Net.Mail;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Web.Services;
using System.Web.UI.WebControls.WebParts;

namespace ProjectManagement
{
    public partial class AddContactVertical : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Chart();
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    txtSearchBox.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    BindGridview();
                    txtempName.Enabled = false;
                }

                LblUseType.Text = Session["UserType"].ToString();
                if (LblUseType.Text == "Staff")
                {
                    BtnUserNote.Visible = false;
                    grdviewNote.Columns[4].Visible = false;
                    TdDelete.Visible = false;
                }
                else
                {
                    BtnUserNote.Visible = true;
                    grdviewNote.Columns[4].Visible = true;
                    TdDelete.Visible = true;
                }
            }
        }

        public void BindGridview()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand("select * from EMPLOYEE", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridViewEmployee.DataSource = dt;
                GridViewEmployee.DataBind();
            }
            else
            {
                GridViewEmployee.DataSource = dt;
                GridViewEmployee.DataBind();
            }
            con.Close();
        }

        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection(cs);
            con.Open();

            String sql = "Select * from EMPLOYEE where FName Like @FNAME";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {

                cmd.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar, 100).Value = "%" + txtSearchBox.Text + "%";

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                DataSet dt1 = new DataSet();

                adpt.Fill(dt1);
                if (dt1.Tables[0].Rows.Count > 0)
                {
                    GridViewEmployee.DataSource = dt1;
                    GridViewEmployee.DataBind();
                }
                else
                {
                    GridViewEmployee.DataSource = dt1;
                    GridViewEmployee.DataBind();
                }
            }
            con.Close();
        }


        //[WebMethod]
        //public static List<string> GetEmployeeName(string empName)
        //{
        //    List<string> empResult = new List<string>();

        //    using (SqlConnection con = new SqlConnection(@"Data Source=PC02\SQLEXPRESS;Initial Catalog=ScaffOldingManagementSystem;user id=sa;password=Certigoa2015!;Integrated Security=True"))
        //    {

        //        using (SqlCommand cmd = new SqlCommand())
        //        {
        //            cmd.Connection = con;
        //            con.Open();
        //            cmd.CommandType = System.Data.CommandType.Text;
        //            cmd.CommandText = "select Top 2 FName from EMPLOYEE where FName LIKE ''+@SearchEmpName+'%'";

        //            cmd.Parameters.AddWithValue("@SearchEmpName", empName);
        //            SqlDataReader dr = cmd.ExecuteReader();
        //            while (dr.Read())
        //            {
        //                empResult.Add(dr["FName"].ToString());
        //            }
        //            con.Close();
        //            return empResult;
        //        }
        //    }
        //}


        protected void BtnAddEmployee_Click(object sender, EventArgs e)
        {
            Response.Redirect("Employee Information.aspx");
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            ArrayList selectedValues = new ArrayList();
            foreach (GridViewRow item in GridViewEmployee.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chk = (CheckBox)(item.Cells[0].FindControl("chkSelect"));
                    if (chk.Checked)
                    {
                        Label lbEmptId = ((Label)item.Cells[0].FindControl("EmpId"));
                        Label lbEmpName = ((Label)item.Cells[1].FindControl("FName"));

                        LblEmpId.Text = lbEmptId.Text;
                        LblEmpName.Text = lbEmpName.Text;

                        //Response.Redirect("customer overview.aspx?CustId=" + CustId.Text);

                    }

                }

            }
        }

        protected void LBtnBasicInfo_Click(object sender, EventArgs e)
        {
            if (LblEmpId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt bruger');", true);
                return;
            }
            else
            {
                LBtnBasicInfo.BackColor = System.Drawing.Color.SkyBlue;
                LBtnBasicInfo.BorderColor = System.Drawing.Color.Black;
                lbtnSendMail.BackColor = LBtnOverview.BackColor = LBtnNotes.BackColor = System.Drawing.Color.Empty;
                lbtnSendMail.BorderColor = LBtnOverview.BorderColor = LBtnNotes.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.Red;
                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                lbtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnNotes.ForeColor = System.Drawing.Color.RoyalBlue;

                DivBasicEmpInfo.Visible = true;
                divOverView.Visible = false;
                divSendMail.Visible = false;
                DivNotes.Visible = false;
                BasicInfoData();
            }

        }

        public void BasicInfoData()
        {
            if (LblEmpId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper Employee Name');", true);
                return;
            }

            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();

                SqlCommand com1 = new SqlCommand("select * from EMPLOYEE where EmpId =" + LblEmpId.Text);

                com1.Connection = con;

                SqlDataAdapter adpt1 = new SqlDataAdapter(com1);

                DataSet dt2 = new DataSet();

                adpt1.Fill(dt2);
                if (dt2.Tables[0].Rows.Count > 0)
                {
                    GridViewBasicInfo.DataSource = dt2;
                    GridViewBasicInfo.DataBind();

                    txtempName.Text = dt2.Tables[0].Rows[0]["FName"].ToString();
                    txtlname.Text = dt2.Tables[0].Rows[0]["LName"].ToString();
                    txtWage.Text = dt2.Tables[0].Rows[0]["WagePerHr"].ToString();
                    txtEmail.Text = dt2.Tables[0].Rows[0]["EmailId"].ToString();
                    txtAddress.Text = dt2.Tables[0].Rows[0]["AddressLine1"].ToString();
                    txtCity.Text = dt2.Tables[0].Rows[0]["City"].ToString();
                    //txtcountry.Text = dt2.Tables[0].Rows[0]["Country"].ToString();
                    txtZipcode.Text = dt2.Tables[0].Rows[0]["ZipCode"].ToString();
                    txtMobile.Text = dt2.Tables[0].Rows[0]["Mobile"].ToString();
                    txtphone.Text = dt2.Tables[0].Rows[0]["Phone2"].ToString();
                    //txtFax.Text = dt2.Tables[0].Rows[0]["Fax"].ToString();
                    txtOccupation.Text = dt2.Tables[0].Rows[0]["Occupation"].ToString();
                    txtProvince.Text = dt2.Tables[0].Rows[0]["Province"].ToString();
                    txtSkillset.Text = dt2.Tables[0].Rows[0]["SkillSets"].ToString();
                    //txtVatNo.Text = dt2.Tables[0].Rows[0]["VATNumber"].ToString();
                    //txtWebsite.Text = dt2.Tables[0].Rows[0]["Website"].ToString();
                }
                con.Close();
            }
        }

        protected void LBtnOverview_Click(object sender, EventArgs e)
        {
            if (LblEmpId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt bruger');", true);
                return;
            }

            else
            {
                LBtnOverview.BackColor = System.Drawing.Color.SkyBlue;
                LBtnOverview.BorderColor = System.Drawing.Color.Black;
                LBtnBasicInfo.BackColor = lbtnSendMail.BackColor = LBtnNotes.BackColor = System.Drawing.Color.Empty;
                LBtnBasicInfo.BorderColor = lbtnSendMail.BorderColor = LBtnNotes.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOverview.ForeColor = System.Drawing.Color.Red;
                lbtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnNotes.ForeColor = System.Drawing.Color.RoyalBlue;

                divOverView.Visible = true;
                DivBasicEmpInfo.Visible = false;
                divSendMail.Visible = false;
                DivNotes.Visible = false;
            }
        }

        protected void lbtnSendMail_Click(object sender, EventArgs e)
        {
            if (LblEmpId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt bruger');", true);
                return;
            }

            else
            {
                lbtnSendMail.BackColor = System.Drawing.Color.SkyBlue;
                lbtnSendMail.BorderColor = System.Drawing.Color.Black;
                LBtnBasicInfo.BackColor = LBtnOverview.BackColor = LBtnNotes.BackColor = System.Drawing.Color.Empty;
                LBtnBasicInfo.BorderColor = LBtnOverview.BorderColor = LBtnNotes.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                lbtnSendMail.ForeColor = System.Drawing.Color.Red;
                LBtnNotes.ForeColor = System.Drawing.Color.RoyalBlue;

                divSendMail.Visible = true;
                divOverView.Visible = false;
                DivBasicEmpInfo.Visible = false;
                DivNotes.Visible = false;
                txt_To.Text = txtEmail.Text;
            }
        }

        protected void btn_SendMail_Click(object sender, EventArgs e)
        {
            string to = txt_To.Text;
            string from = "manojgawas49@gmail.com"; //info@certigoa.com
            string subject = txt_Subject.Text;
            string body = txt_Message.Text;

            try
            {
                using (MailMessage mm = new MailMessage(from, to))
                {

                    mm.Subject = subject;
                    mm.Body = body;
                    if (FileUpload1.HasFile)
                    {
                        string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                        mm.Attachments.Add(new Attachment(FileUpload1.PostedFile.InputStream, FileName));
                    }

                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();


                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("manojgawas49@gmail.com", "papa2119");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;


                    smtp.Send(mm);
                }
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Mail Send Successfully')", true);
            }


            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + ex.Message + "')", true);

            }
        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            Clearall();
        }

        protected void Clearall()
        {
            txt_Message.Text = string.Empty;
            txt_Subject.Text = string.Empty;
            txt_To.Text = string.Empty;

        }

        protected void BtndeleteEmployee_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvrow in GridViewEmployee.Rows)
            {

                CheckBox chkdelete = (CheckBox)gvrow.FindControl("chkSelect");

                if (chkdelete.Checked)
                {
                    int EmpID = Convert.ToInt32(GridViewEmployee.DataKeys[gvrow.RowIndex].Value);

                    using (SqlConnection con = new SqlConnection(cs))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM EMPLOYEE where EmpId=" + EmpID, con);
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            BindGridview();
            resetall();
        }

        public void resetall()
        {
            txtempName.Text = "";
            txtlname.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";

            txtZipcode.Text = "";
            txtMobile.Text = "";
            txtphone.Text = "";

            txtOccupation.Text = "";
            txtProvince.Text = "";
            txtSkillset.Text = "";
            LblEmpName.Text = "";
            LblEmpId.Text = "";
        }

        protected void BtnUpdate_Click1(object sender, EventArgs e)
        {
            if (txtempName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('User Frist Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con1 = new SqlConnection(cs);
                con1.Open();
                com.Connection = con1;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_UpdateEmployeeDetails";
                com.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = LblEmpId.Text;
                com.Parameters.Add("@FNAME", System.Data.SqlDbType.VarChar).Value = txtempName.Text;
                com.Parameters.Add("@LNAME", System.Data.SqlDbType.VarChar).Value = txtlname.Text;
                com.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = txtWage.Text;
                com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
                com.Parameters.Add("@PHONE2", System.Data.SqlDbType.VarChar).Value = txtphone.Text;
                com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
                com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress.Text;
                com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtCity.Text;
                com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince.Text;
                com.Parameters.Add("@SKILLSETS", System.Data.SqlDbType.VarChar).Value = txtSkillset.Text;
                com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.VarChar).Value = txtZipcode.Text;
                com.Parameters.Add("@OCCUPATION", System.Data.SqlDbType.VarChar).Value = txtOccupation.Text;

                com.ExecuteNonQuery();
                BindGridview();
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Data Updated SuccessFully');", true);
                com.Parameters.Clear();
                con1.Close();
            }
        }

        public void CallJavaScript(object sender)
        {
            LinkButton lnk = (LinkButton)sender;
            //Response.Write("link button:" + lnk.Text + " is clicked");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Ming1", "SetColor('" + lnk.ClientID + "')", true);
        }

        //protected void Chart()
        //{
        //    SqlConnection con = new SqlConnection(cs);
        //    con.Open();

        //    DataSet ds2 = new DataSet();
        //    string query2 = "select DATEPART(dd,ExpectedTime) AS Days,TotalCost from REQUESTEDQUOTE";
        //    SqlCommand cmd2 = new SqlCommand(query2, con);

        //    cmd2.ExecuteNonQuery();
        //    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);

        //    da2.Fill(ds2, "PremiumData");

        //    if (ds2.Tables["PremiumData"].Rows.Count > 0)
        //    {
        //        Chart3.DataSource = ds2.Tables["PremiumData"];
        //        // Set series members names for the X and Y values

        //        Chart3.Series["Count"].XValueMember = "Days";

        //        Chart3.Series["Count"].YValueMembers = "TotalCost";

        //        Chart3.Series["Count"].IsValueShownAsLabel = true;
        //        Chart3.Series["Count"].ChartType = SeriesChartType.Line;

        //        Chart3.Titles[1].Text = "Line Chart";
        //        Chart3.DataBind();
        //    }
        //    con.Close();
        //}

        protected void LBtnEmployeeName_Click(object sender, EventArgs e)
        {
            LBtnOverview.BackColor = System.Drawing.Color.SkyBlue;
            LBtnOverview.BorderColor = System.Drawing.Color.Black;
            lbtnSendMail.BackColor = LBtnBasicInfo.BackColor = LBtnNotes.BackColor = System.Drawing.Color.Empty;
            lbtnSendMail.BorderColor = LBtnBasicInfo.BorderColor = LBtnNotes.BorderColor = System.Drawing.Color.Empty;

            LBtnOverview.ForeColor = System.Drawing.Color.Red;
            LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            lbtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnNotes.ForeColor = System.Drawing.Color.RoyalBlue;

            DivBasicEmpInfo.Visible = false;
            divOverView.Visible = true;
            divSendMail.Visible = false;
            DivNotes.Visible = false;
            DivNotes.Visible = false;

            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            Label quatdate = ((Label)grdrow.Cells[0].FindControl("EmpId"));
            Label lbEmpName = ((Label)grdrow.Cells[1].FindControl("FName"));
            LblEmpId.Text = quatdate.Text;
            LblEmpName.Text = lbEmpName.Text;

            foreach (GridViewRow rows in GridViewEmployee.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.LinkButton LBtnEmployeeName = (rows.Cells[1].FindControl("LBtnEmployeeName") as System.Web.UI.WebControls.LinkButton);

                    if (lbEmpName.Text == LBtnEmployeeName.Text)
                    {
                        LBtnEmployeeName.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        LBtnEmployeeName.ForeColor = System.Drawing.Color.RoyalBlue;

                    }
                }
            }

            BasicInfoData();
            //CallJavaScript(sender);

        }

        protected void GridViewEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("DELETE FROM EMPLOYEE where EmpId=" + id, con);

                cmd.ExecuteNonQuery();
                con.Close();
                BindGridview();
                resetall();
            }
        }

        protected void Bound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "style.backgroundColor='LightBlue'");
            e.Row.Attributes.Add("onmouseout", "style.backgroundColor='#fbfbfb'");
        }

        protected void LBtnNotes_Click(object sender, EventArgs e)
        {
            if (LblEmpId.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Vælg venligst Korrekt bruger');", true);
                return;
            }
            else
            {
                LBtnNotes.BackColor = System.Drawing.Color.SkyBlue;
                LBtnNotes.BorderColor = System.Drawing.Color.Black;
                LBtnBasicInfo.BackColor = lbtnSendMail.BackColor = LBtnOverview.BackColor = System.Drawing.Color.Empty;
                LBtnBasicInfo.BorderColor = lbtnSendMail.BorderColor = LBtnOverview.BorderColor = System.Drawing.Color.Empty;

                LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnOverview.ForeColor = System.Drawing.Color.RoyalBlue;
                lbtnSendMail.ForeColor = System.Drawing.Color.RoyalBlue;
                LBtnNotes.ForeColor = System.Drawing.Color.Red;

                divOverView.Visible = false;
                DivBasicEmpInfo.Visible = false;
                divSendMail.Visible = false;
                DivNotes.Visible = true;

                BindNote();
            }
        }

        protected void BtnSaveNote_Click(object sender, EventArgs e)
        {
            int id = 0;

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();
            com.Connection = con1;

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT (ISNULL(MAX(NOTEID),0) + 1) as OrderId FROM USERNOTE";
            id = Convert.ToInt32(com.ExecuteScalar().ToString());
            com.Parameters.Clear();

            if (string.IsNullOrWhiteSpace(txtNoteName.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Name cannot be blank');", true);
                return;
            }
            else
            {
                com.Parameters.Clear();
                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "Insert into USERNOTE (NOTEID, UID, NoteName, Description) Values (@NOTEID, @UID, @NOTENAME, @DESCRIPTION)";

                com.Parameters.Add("@NOTEID", System.Data.SqlDbType.BigInt).Value = id;
                com.Parameters.Add("@UID", System.Data.SqlDbType.BigInt).Value = LblEmpId.Text;
                com.Parameters.Add("@NOTENAME", System.Data.SqlDbType.NVarChar).Value = txtNoteName.Text;
                com.Parameters.Add("@DESCRIPTION", System.Data.SqlDbType.NVarChar).Value = txtNoteDescr.InnerText;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con1.Close();

                BindNote();

                txtNoteName.Text = "";
                txtNoteDescr.InnerText = "";
            }
        }

        public void BindNote()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com1 = new SqlCommand("select Distinct * from USERNOTE where UID =" + LblEmpId.Text);

            com1.Connection = con;

            SqlDataAdapter adpt1 = new SqlDataAdapter(com1);

            DataSet dt2 = new DataSet();

            adpt1.Fill(dt2);
            if (dt2.Tables[0].Rows.Count > 0)
            {
                NoteHeader.Visible = true;
                grdviewNote.DataSource = dt2;
                grdviewNote.DataBind();
            }
            else
            {
                NoteHeader.Visible = false;
                grdviewNote.DataSource = dt2;
                grdviewNote.DataBind();
            }
            con.Close();
        }

        protected void grdviewNote_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            GridViewRow GridViewRo = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int RowIndex = GridViewRo.RowIndex;

            string Notenamee = grdviewNote.Rows[RowIndex].Cells[1].Text;
            string NoteDescriptionn = grdviewNote.Rows[RowIndex].Cells[2].Text;

            int id = Convert.ToInt32(e.CommandArgument);
            LabelNoteId.Text = id.ToString();

            if (e.CommandName == "DeleteRow")
            {
                cmd.Parameters.Clear();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "delete from USERNOTE where NOTEID='" + id + "'AND NoteName='" + Notenamee.ToString() + "' AND UID='" + LblEmpId.Text + "'";
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                BindNote();
            }
            if (e.CommandName == "ViewNote")
            {
                cmd.Parameters.Clear();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select Distinct * from USERNOTE where NOTEID='" + id + "'AND NoteName='" + Notenamee.ToString() + "'AND UID='" + LblEmpId.Text + "'";
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet dSet = new DataSet();

                adp.Fill(dSet);

                if (dSet.Tables[0].Rows.Count > 0)
                {


                    txtNoteName.Text = dSet.Tables[0].Rows[0]["NoteName"].ToString();
                    txtNoteDescr.InnerText = dSet.Tables[0].Rows[0]["Description"].ToString();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1')", true);

                    BtnSaveNote.Visible = false;

                    if (LblUseType.Text == "Staff")
                    {
                        LblNoteHeading.Text = "Info:";
                        BtnUpdateNote.Visible = false;
                    }
                    else
                    {
                        LblNoteHeading.Text = "Opdater Noter";
                        BtnUpdateNote.Visible = true;
                    }
                }
                cmd.Parameters.Clear();
            }
            con.Close();
        }

        protected void BtnUpdateNote_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlCommand cmdd = new SqlCommand();
            cmdd.Connection = conn;

            if (string.IsNullOrWhiteSpace(txtNoteName.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Name cannot be blank');", true);
                return;
            }
            else
            {

                cmdd.Parameters.Clear();
                cmdd.CommandType = System.Data.CommandType.Text;
                cmdd.CommandText = " Update USERNOTE SET  NoteName=@NOTENAME,Description=@DESCRIPTION Where NOTEID='" + LabelNoteId.Text + "' and UID='" + LblEmpId.Text + "'";

                cmdd.Parameters.Add("@NOTENAME", System.Data.SqlDbType.NVarChar).Value = txtNoteName.Text;
                cmdd.Parameters.Add("@DESCRIPTION", System.Data.SqlDbType.NVarChar).Value = txtNoteDescr.InnerText;

                cmdd.ExecuteNonQuery();
                cmdd.Parameters.Clear();
                conn.Close();

                BindNote();
            }
        }

        protected void BtnUserNote_Click(object sender, EventArgs e)
        {
            BtnSaveNote.Visible = true;
            BtnUpdateNote.Visible = false;

            LblNoteHeading.Text = "Tilføj noter";

            txtNoteDescr.InnerText = "";
            txtNoteName.Text = "";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "pop('pop1')", true);
        }

    }
}