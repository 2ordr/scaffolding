﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;

namespace ProjectManagement
{
    public partial class HomePage : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";
                    Label1.Text = Session["username"].ToString();
                    //VatNumber.Visible = false;
                    resetall();
                    int CUSTID;

                    SqlConnection con1 = new SqlConnection(cs);
                    con1.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con1;

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT  ISNULL(MAX(CustId),0) FROM CUSTOMER";
                    CUSTID = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
                    lblCustid.Text = Convert.ToInt32(CUSTID).ToString(); ;

                    cmd.Parameters.Clear();
                    con1.Close();

                    txtName.Focus();
                    Btnview.Visible = false;
                }
            }
        }
        public void resetall()
        {
            txtName.Text = "";
            txtMobile.Text = "";
            txtphone.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtcity.Text = "";
            txtProvince.Text = "";
            txtzip.Text = "";
            txtVatNo.Text = "";
            txtFax.Text = "";
            txtcountry.Text = "";
            //DdlistVat.SelectedValue = "";
        }

        protected void Btnview_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddCustomerVertical.aspx?");
        }

        protected void Btnadd_Click(object sender, EventArgs e)
        {

            if (txtName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertCustomerDetails";
                com.Parameters.Add("@CUSTNAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;
                com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
                com.Parameters.Add("@PHONE2", System.Data.SqlDbType.VarChar).Value = txtphone.Text;
                com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
                com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress.Text;
                com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtcity.Text;
                com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince.Text;
                com.Parameters.Add("@REMARK", System.Data.SqlDbType.NVarChar).Value = "";
                com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.NVarChar).Value = txtzip.Text;
                com.Parameters.Add("@VATNUMBER", System.Data.SqlDbType.NVarChar).Value = txtVatNo.Text;
                com.Parameters.Add("@FAX", System.Data.SqlDbType.VarChar).Value = txtFax.Text;
                com.Parameters.Add("@COUNTRY", System.Data.SqlDbType.VarChar).Value = txtcountry.Text;
                //com.Parameters.Add("@VAT", System.Data.SqlDbType.VarChar).Value = DdlistVat.SelectedValue;
                com.Parameters.Add("@WEBSITE", System.Data.SqlDbType.NVarChar).Value = txtWebsite.Text;
                com.Parameters.Add("@DISCOUNT", System.Data.SqlDbType.Decimal).Value = txtDiscount.Text;

                //com.Parameters.Add("@DELETESTATUS", System.Data.SqlDbType.BigInt).Value =false;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                resetall();
                con.Close();

            }
            Btnview.Visible = true;
            Btnadd.Visible = false;
        }

        protected void DdlistVat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DdlistVat.Text == "Yes")
            {
                //VatNumber.Visible = true;
                txtVatNo.Visible = true;
                td1.Visible = true;
            }
            else
            {
                //    VatNumber.Visible = false;
                txtVatNo.Visible = false;
                td1.Visible = false;
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
            if (filename == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select File To Upload');", true);
                return;
            }
            else
            {
                string filePath = @"~\CustomerData\" + filename.Trim();
                string newfile = Server.MapPath(filePath);
                FileUpload1.SaveAs(newfile);
                //Stream str = FileUpload1.PostedFile.InputStream;

                //BinaryReader br = new BinaryReader(str);

                //Byte[] size = br.ReadBytes((int)str.Length);

                using (SqlConnection con = new SqlConnection(cs))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "insert into CUSTOMERFILEDATA (CustId,FileName,FilePath) values(@id,@Name,@FilePath)";

                        cmd.Parameters.AddWithValue("@id", System.Data.SqlDbType.BigInt).Value = lblCustid.Text;

                        cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarChar).Value = filename;

                        cmd.Parameters.AddWithValue("@FilePath", System.Data.SqlDbType.VarChar).Value = newfile;

                        cmd.Connection = con;

                        con.Open();

                        cmd.ExecuteNonQuery();

                        con.Close();
                    }

                }
                //gvFileUpload1();
            }
        }


        //protected void DdlistVat_TextChanged(object sender, EventArgs e)
        //{

        //    //string value = DdlistVat.SelectedValue;

        //    //if (value == "Yes")
        //    //{
        //    //    VatNumber.Visible = true;

        //    //}
        //    //else
        //    //{
        //    //    VatNumber.Visible = false;
        //    //}
        //}


    }
}