﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditQuatation.aspx.cs" Inherits="ProjectManagement.EditQuatation" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>New - Projects - Scaffolding</title>

    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style>
        headform1 {
            color: #383838;
        }

        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .auto-style2 {
            height: 15px;
        }

        .Itemalign {
            text-align: center;
        }
    </style>

    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>
    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>

    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>

</head>

<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">
        <div style="height: 1000px; width: 1920px;">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">
                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtSearchBox" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150"></asp:TextBox>

                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon  app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item  x-item-selected active" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">

                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%--  <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>
                                                --%>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('DivA')">Rapporter</span></span>

                                                    <div id="DivA" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div2')">Løn Modul</span></a>
                                                    <div id="Div2" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>

                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                --%>
                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <div class="allSides" style="width: 100%; height: 80px;">
                    <div class="divider"></div>
                    <asp:Label ID="Label10" runat="server" CssClass="clienthead" Text=" Details Quatation"></asp:Label>
                </div>
                <div style="width: 1%; float: left; height: 800px; border: solid 0px black;">
                </div>
                <div style="height: 50px">
                    <%-- <asp:Image ID="Image2" runat="server" ImageUrl="~/image/Step1.jpg" Height="50" Width="100" />--%>
                </div>
                <div style="width: 82%; float: left; height: 820px; font-family: Calibri;">

                    <div style="width: 1700px; float: left; height: 800px;">
                        <fieldset>

                            <legend style="color: black; font-weight: bold; font-size: large">Stamdata</legend>
                            <table style="width: 1700px;">

                                <tbody>
                                    <tr>
                                        <td style="width: 40%;">

                                            <table style="width: 90%;">
                                                <tr>
                                                    <td style="width: 30%; text-align: right;">Dato&nbsp;&nbsp;&nbsp;                                            
                                                    </td>
                                                    <td style="width: 25%;">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtDate" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 30%; text-align: right;">Kundernavn&nbsp;&nbsp;&nbsp;                                            
                                                    </td>
                                                    <td style="width: 25%;">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName" runat="server" Width="300px" Height="31px" AutoPostBack="true" Enabled="false"></asp:TextBox>
                                                        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtName" ValidationExpression="[a-zA-Z ]*$"></asp:RegularExpressionValidator>--%>
                                                        <asp:Label ID="lblName" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="height: 20px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Bekræftelse&nbsp;&nbsp;&nbsp;</td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtResTime" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Total&nbsp;&nbsp;&nbsp;</td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtResCost" runat="server" Width="300px" Height="31px" TextMode="Number" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Customer Ack&nbsp;&nbsp;&nbsp;</td>
                                                    <td colspan="2">
                                                        <asp:DropDownList CssClass="search_categories" ID="ddlCustAck" runat="server" Width="300px" Height="36px" Enabled="false">
                                                            <asp:ListItem>Yes</asp:ListItem>
                                                            <asp:ListItem>No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Godkendt af Admin&nbsp;&nbsp;&nbsp;</td>
                                                    <td colspan="2">

                                                        <asp:DropDownList CssClass="search_categories" ID="ddlAdminApp" runat="server" Width="300px" Height="36px" Enabled="false">
                                                            <asp:ListItem>Yes</asp:ListItem>
                                                            <asp:ListItem>No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Status&nbsp;&nbsp;&nbsp;</td>
                                                    <td colspan="2">

                                                        <asp:DropDownList CssClass="search_categories" ID="ddquotestatus" runat="server" Width="300px" Height="36px" OnSelectedIndexChanged="ddquotestatus_SelectedIndexChanged">
                                                            <asp:ListItem>Draft</asp:ListItem>
                                                            <asp:ListItem>Send</asp:ListItem>
                                                            <asp:ListItem>Accepted</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Possibility&nbsp;&nbsp;&nbsp;</td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuoteposs" runat="server" Width="300px" Height="31px" TextMode="Number" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right;">Total Earn&nbsp;&nbsp;&nbsp;</td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEarning" runat="server" Width="300px" Height="31px" TextMode="Number" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                        <td style="width: 60%;">
                                            <table style="width: 100%; font-family: Calibri; margin-top: 1px;">
                                                <tbody>

                                                    <tr>
                                                        <td>
                                                            <div id="div1" runat="server" visible="false">
                                                                <table style="margin-left: 4px;">
                                                                    <tr style="background-color: #F2F2F2; border-width: 5px;">
                                                                        <td style="border-color: white">Customer Name</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div id="divCustlist" runat="server" style="overflow: auto; height: 200px" visible="false">

                                                                <table style="width: 100%; font-family: Calibri; margin-top: 2px;">
                                                                    <tbody>

                                                                        <tr>
                                                                            <td style="width: 100%;" colspan="3">
                                                                                <asp:GridView ID="GridView1" runat="server" ShowHeader="false" DataKeyNames="CustId" AutoGenerateColumns="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="CustName" HeaderText="Customer Name" HeaderStyle-Width="300px" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                                                        <asp:CommandField SelectText="Select" ShowSelectButton="true" Visible="false" />
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px;" colspan="3"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="height: 20px;"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>
                        </fieldset>
                        <div style="height: 20px;">
                        </div>
                        <fieldset>

                            <legend style="color: black; font-weight: 700; font-size: large">Item Details:</legend>

                            <div id="headerdiv" runat="server" style="width: 100%; height: 20px">
                                <table border="0" id="" style="width: 100%; border-width: 5px; border-color: white">
                                    <tr style="background-color: #F2F2F2;">

                                        <td style="width: 12px; text-align: left; border-width: 5px; border-color: white"></td>

                                        <td style="width: 32px; text-align: left; border-width: 5px; border-color: white">Item Code</td>

                                        <td style="width: 207px; text-align: left; border-width: 5px; border-color: white">Service/Item Name</td>

                                        <td style="width: 72px; text-align: left; border-width: 5px; border-color: white">Fees/Price</td>

                                        <td style="width: 143px; text-align: left; border-width: 5px; border-color: white">Quantity</td>

                                        <td style="width: 173px; text-align: left; border-width: 5px; border-color: white">Amount</td>

                                    </tr>
                                </table>
                            </div>
                            <div id="divstk" runat="server" style="width: 1700px; height: 300px; overflow: auto">
                                <table style="width: 1700px;">

                                    <tr>
                                        <td style="width: 100%;">
                                            <asp:GridView ID="GridViewEditQuatation" runat="server" ShowHeader="false" ShowFooter="true" AutoGenerateColumns="false" Width="100%" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White" Style="font-size: medium" OnSelectedIndexChanged="GridViewEditQuatation_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                        <ItemStyle Width="14px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Item Code" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="Itemalign" />
                                                        <ItemStyle Width="42px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ItemName" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemName" runat="server" Text='<%# Bind("ItemName") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="Itemalign" />
                                                        <ItemStyle Width="270px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Price" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemPrice" runat="server" Text='<%# Bind("Price") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="Itemalign" />
                                                        <ItemStyle Width="91px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Quantity" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtQuantity" runat="server" Text='<%# Bind("RespondQuantity") %>' Visible="true" Height="31" Enabled="false"></asp:TextBox>

                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="Itemalign" />
                                                        <ItemStyle Width="164px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Amount" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                                        <ItemTemplate>
                                                            <%-- <asp:TextBox CssClass="twitterStyleTextbox" ID="txtAmount" runat="server" Height="31" Text='<%# Convert.ToInt32(Eval("RespondQuantity"))*Convert.ToInt32(Eval("Price")) %>' Visible="true" Enabled="false"></asp:TextBox>--%>
                                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtAmount" runat="server" Height="31" Text='<%# Bind("TotalPrice") %>' Visible="true" Enabled="false"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="Itemalign" />
                                                        <ItemStyle Width="230px" />
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblTot" runat="server" Text="" Visible="true"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </fieldset>
                    </div>

                </div>
                <div runat="server" class="shadowdiv" style="width: 100%; float: left; height: 50px; text-align: center; border: solid 0px black;">
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="ButUpdate" runat="server" Text="Opdater " Width="101px" Style="background-color: #009900;" OnClick="ButUpdate_Click" Visible="false" />

                                </td>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="ButView" runat="server" Text="Vis" Width="120px" Style="background-color: #009933" OnClick="ButView_Click" />

                                </td>
                                <%--<td>
                                    <asp:Button CssClass="buttonn" ID="ButDelete" runat="server" Text="DELETE" Width="120px" Style="background-color: #009933" />
                                </td>--%>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>



            </div>

        </div>
    </form>
</body>
</html>
