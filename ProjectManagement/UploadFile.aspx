﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="ProjectManagement.UploadFile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table style="width:100px;" border="0" cellpadding="6" cellspacing="0" bgcolor="#e1ecf2" summary="This table is for layout">
                <tr>
                    <td style="vertical-align:top;">
                        <input class="blueFormText" id="txtFileToUpload" title="File to Upload" tabindex="200" type="file" size="30" name="filMyFile" runat="server" style="width: 275px; height: 22px"/>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
