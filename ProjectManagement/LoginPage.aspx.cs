﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Net;

namespace ProjectManagement
{
    public partial class LoginPage : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Session["UserType"] = "";

                if (Request.Cookies["username"] != null)

                    txt_UserName.Text = Request.Cookies["username"].Value;

                if (Request.Cookies["pwd"] != null)

                    txt_Password.Attributes.Add("value", Request.Cookies["pwd"].Value);

                if (Request.Cookies["username"] != null && Request.Cookies["pwd"] != null)
                    ChkRememberMe.Checked = true;



            }
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection(cs);

            con.Open();

            SqlCommand com = new SqlCommand("select * from EMPLOYEE where UserName =@USERNAME and Password =@PASSWORD", con);

            com.Parameters.AddWithValue("@USERNAME", txt_UserName.Text);

            com.Parameters.AddWithValue("@PASSWORD", txt_Password.Text);

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataTable dt = new DataTable();

            adpt.Fill(dt);

            con.Close();

            //com.ExecuteNonQuery();

            //if (dt.Rows.Count > 0)
            //{
            //    Response.Redirect("Dashboard.aspx");
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'> alert('Invalid Username and Password')</script>");
            //}
            if (dt.Rows.Count > 0)
            {
                int EmployeeId = Convert.ToInt32(dt.Rows[0]["EmpId"].ToString());

                Session["UserId"] = EmployeeId;
                Session["username"] = txt_UserName.Text;
                Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                if (ChkRememberMe.Checked == true)
                {
                    Response.Cookies["username"].Value = txt_UserName.Text;
                    Response.Cookies["pwd"].Value = txt_Password.Text;
                    Response.Cookies["username"].Expires = DateTime.Now.AddDays(15);
                    Response.Cookies["pwd"].Expires = DateTime.Now.AddDays(15);
                }

                else
                {
                    Response.Cookies["username"].Expires = DateTime.Now.AddDays(-1);

                    Response.Cookies["pwd"].Expires = DateTime.Now.AddDays(-1);
                }

                Response.Redirect("Dashboard.aspx");
                //Response.Redirect("AddCustomerVertical.aspx");
            }

            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'> alert('Please Enter Proper Username and Password ')</script>");
            }
        }
    }
}