﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;

namespace ProjectManagement
{
    public partial class View_Order : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["username"] as string))
            {
                Response.Redirect("LoginPage.aspx");
            }
            else
            {
                TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                Label1.Text = Session["username"].ToString();
                BindOrder();
            }
        }

        public void BindOrder()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from ORDERS", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataTable dt = new DataTable();
            dt.Clear();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridView2.DataSource = dt;

                GridView2.DataBind();
            }
            con.Close();
        }

        protected void BtnAddOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("Wizardss.aspx");
        }

        protected void lkbtnEditOrder_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[2].Text;
            Response.Redirect("EditOrder.aspx?Quatdate=" + quatdate);
        }

        protected void lnkDeleteOrder_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string Date = grdrow.Cells[2].Text;

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();

            SqlCommand com1 = new SqlCommand("delete FROM ORDERS where OrderDate = @ORDERDATE ; delete FROM HAS  where DateTime = @ORDERDATE;", con1);
            com1.Parameters.Add("@ORDERDATE", SqlDbType.NVarChar).Value = Date;

            com1.ExecuteNonQuery();
            con1.Close();
            BindOrder();
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Order Deleted Successfully!!');", true);
            return;
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                int Orderid = Convert.ToInt32(e.CommandArgument);
                SqlConnection con = new SqlConnection(cs);

                //SqlCommand cmd = new SqlCommand("delete from STOCKSITEM where ItemCode=" + Orderid, con);
                //SqlCommand com = new SqlCommand("delete FROM ORDERS where OrderId =" + Orderid , "delete FROM HAS where OrderId =" + Orderid ; , con);
                SqlCommand com = new SqlCommand("Delete FROM ORDERS where OrderId =" + Orderid, con);
                com.Connection = con;
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
                BindOrder();
                con.Close();
            }
            else
                if (e.CommandName == "InvoiceGen")
                {
                    int OrderID = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect("New Invoice1.aspx?OrderId=" + OrderID);
                }
        }

        protected void Bound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "style.backgroundColor='LightBlue'");
            e.Row.Attributes.Add("onmouseout", "style.backgroundColor='#fbfbfb'");
        }
    }
}