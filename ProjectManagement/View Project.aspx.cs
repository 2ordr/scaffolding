﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace ProjectManagement
{
    public partial class View_Project : System.Web.UI.Page
    {

        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    btnClose.Visible = false;
                    Label1.Text = Session["username"].ToString();
                    BindData();
                }
            }
        }

        public void BindData()
        {

            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com1 = new SqlCommand("select * from PROJECTS ORDER BY ProjectId", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
            con.Close();
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProjectAssignment.aspx?");
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            //SqlConnection con = new SqlConnection(cs);
            ////SqlCommand com1 = new SqlCommand();

            //if (TextBox1.Text != "")
            //{


            //    con.Open();


            //    SqlCommand com1 = new SqlCommand("SearchAllTables", con);
            //    com1.CommandType = CommandType.StoredProcedure;
            //    //com.Parameters.Add("@SearchStr", SqlDbType.NVarChar, 60).Value = "%" + txtName.Text + "%";
            //    //com.CommandType = System.Data.CommandType.StoredProcedure;
            //    //com.CommandText = "SearchAllTables";
            //    com1.Parameters.Add("@SearchStr", System.Data.SqlDbType.NVarChar).Value = "%" + TextBox1.Text + "%";

            //    SqlDataAdapter adpt = new SqlDataAdapter(com1);
            //    DataTable dt = new DataTable();
            //    adpt.Fill(dt);

            //    GridView1.DataSource = dt;
            //    GridView1.DataBind();

            //}
        }

        protected void lnkViewProject_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string Proid = grdrow.Cells[0].Text;
            Response.Redirect("ProjectDetails.aspx?Projectid=" + Proid);
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            btnStart.Visible = false;
            btnClose.Visible = true;
            Label5.Text = lbltimedisplay.Text;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            btnStart.Visible = true;
            btnClose.Visible = false;

            Label6.Text = lbltimedisplay.Text;

            DateTime inTime = Convert.ToDateTime(Label5.Text);
            DateTime outTime = Convert.ToDateTime(Label6.Text);
            TimeSpan i = outTime.Subtract(inTime);
            this.txtHrs.Text = i.ToString();
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            lbltimedisplay.Text = DateTime.Now.ToString();
        }

        protected void lnkDeleteProject_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string ProId = grdrow.Cells[0].Text;

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();

            SqlCommand com1 = new SqlCommand("delete FROM PROJECTS where ProjectId = @PROJECTID ; delete FROM ASSIGNED  where ProjectId = @PROJECTID;", con1);

            //SqlCommand com1 = new SqlCommand();
            //com1.CommandText = "sp_deleteQuatation";
            //com1.CommandType = System.Data.CommandType.StoredProcedure;
            //com1.Connection = con1; 
            //com1.CommandType = CommandType.Text;
            com1.Parameters.Add("@PROJECTID", SqlDbType.BigInt).Value = ProId;


            //SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
            //DataTable dt1 = new DataTable();
            //adpt1.Fill(dt1);
            //GridView2.DataSource = dt1;

            com1.ExecuteNonQuery();
            con1.Close();
            BindData();
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Project Deleted Successfully!!');", true);
            return;

        }

        protected void lnkUpdateProject_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string Proid = grdrow.Cells[0].Text;
            Response.Redirect("ProjectAssignment.aspx?Projectid=" + Proid);
        }
    }
}