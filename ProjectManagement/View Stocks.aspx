﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="View Stocks.aspx.cs" Inherits="ProjectManagement.View_Stocks" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>

    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style>
        .main-panel-with-title > .x-panel-header .x-panel-header-text-container-default {
            font-weight: normal;
            color: #383838;
            padding-left: 20px;
            font-size: 182%;
            line-height: inherit;
        }

        headform1 {
            color: #383838;
        }

        .simple {
            color: lightblue;
        }

            .simple:hover {
                color: blue;
            }

        .modal {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }

        .center {
            z-index: 1000;
            margin: 300px auto;
            padding: 10px;
            width: 250px;
            /*background-color: White;*/
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>
    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>
    <script type="text/javascript">
        function clearTextBox(textBoxID) {
            document.getElementById(textBoxID).value = "";
        }
    </script>

</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Debug"></asp:ToolkitScriptManager>

        <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
            <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                    <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                        <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox1" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%--<div class="x-panel-body x-panel-body-default x-box-layout-ct x-panel-body-default x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="searchBox-body" role="presentation" style="left: 0px; top: 0px; width: 177px; height: 28px;">
                                    <div class="x-box-inner " id="searchBox-innerCt" role="presentation" style="width: 177px; height: 28px;">
                                        <div class="x-box-target" id="searchBox-targetEl" role="presentation" style="width: 177px;">
                                            <table class="x-field x-table-plain x-form-item x-form-type-text x-box-item x-field-default x-hbox-form-item" id="combobox-1022" role="presentation" style="margin: 0px; left: 0px; top: 1px; width: 139px; right: auto; table-layout: fixed;" cellpadding="0">
                                                <tbody>
                                                    <tr class="x-form-item-input-row" id="combobox-1022-inputRow" role="presentation">
                                                        <td width="105" class="x-field-label-cell" id="combobox-1022-labelCell" role="presentation" valign="top" style="display: none;" halign="left">
                                                            <label class="x-form-item-label x-unselectable x-form-item-label-left" id="combobox-1022-labelEl" style="width: 100px; margin-right: 5px;" for="combobox-1022-inputEl" unselectable="on"></label>
                                                        </td>
                                                        <td class="x-form-item-body  " id="combobox-1022-bodyEl" role="presentation" style="width: 100%;" colspan="3">
                                                            <table class="x-form-trigger-wrap" id="combobox-1022-triggerWrap" role="presentation" style="width: 100%; table-layout: fixed;" cellspacing="0" cellpadding="0">
                                                                <tbody role="presentation">
                                                                    <tr role="presentation">
                                                                        <td class="x-form-trigger-input-cell" id="combobox-1022-inputCell" role="presentation" style="width: 100%;">
                                                                            <div class="x-hide-display x-form-data-hidden" id="ext-gen1091" role="presentation"></div>
                                                                            <input name="combobox-1022-inputEl" class="x-field-without-trigger x-form-empty-field x-form-text " id="combobox-1022-inputEl" style="width: 100%;" type="text" placeholder="Search (Ctrl+/)" autocomplete="off" /><span class="icon-search"></span></td>
                                                                        <td class=" x-trigger-cell x-unselectable" id="ext-gen1090" role="presentation" valign="top" style="width: 23px; display: none;">
                                                                            <div class="x-trigger-index-0 x-form-trigger x-form-arrow-trigger x-form-trigger-first" id="ext-gen1089" role="presentation"></div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="x-form-invalid-under" id="combobox-1022-errorEl" role="alert" aria-live="polite" style="display: none;" colspan="2"></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="x-splitter x-box-item x-splitter-default x-splitter-vertical x-unselectable" id="splitter-1023" style="margin: 0px; left: 134px; top: 0px; width: 5px; height: 28px; right: auto; display: none;"></div>
                                            <a tabindex="0" class="x-btn btn-collapse x-unselectable x-box-item x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon btn-expanded" id="button-1024" style="border-width: 0px; margin: 0px; left: 149px; top: 2px; right: auto;" hidefocus="on" unselectable="on">
                                                <span class="x-btn-wrap" id="button-1024-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1024-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1024-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-collapse " id="button-1024-btnIconEl" role="presentation" unselectable="on">&nbsp;</span></span></span></a>
                                        </div>
                                    </div>
                                </div>--%>
                        </div>
                        <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                            <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                            </div>
                            <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                    <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                        <ul class="menu">
                                            <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                            <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                            <%--<li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                            <%-- <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-4" data-index="4"><a class="menu-link" href=" View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                            <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                            <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                            <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="ProjectAssignmentNew.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>


                                            <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                <div id="hide" runat="server">
                                                    <ul class="menu-group" id="ext-gen3448">

                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                        <li class="menu-item menu-app-item app-item item-child x-item-selected active" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <%-- <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a>
                                            --%>
                                            <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div1')">Rapporter</span></span>

                                                <div id="Div1" runat="server" style="display: none">
                                                    <ul class="menu-group" id="ext-gen34481">
                                                        <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div2')">Løn Modul</span></a>
                                                <div id="Div2" runat="server" style="display: none">
                                                    <ul class="menu-group" id="ext-gen3450">
                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>



                                            <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447">
                                                    <span class="group-item-text menu-link">
                                                        <span class="menu-item-icon app-billing"></span>
                                                        <span class="menu-item-text">Invoicing</span>
                                                        <span class="menu-toggle"></span></span>
                                                    <ul class="menu-group" id="ext-gen3448">
                                                        <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                        <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>--%>
                                            <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                            <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>

                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">
            <div style="width: 1725px; float: left; height: 1000px;">
                <div style="height: 10px;">
                </div>
                <table style="width: 100%; font-family: Calibri;">
                    <tbody>
                        <tr>

                            <td style="width: 95%;">
                                <asp:Button CssClass="button" ID="Button1" runat="server" Text="+ Tilføj vare/service" BackColor="#CC6699" Style="color: #FFFFFF; font-weight: 700; font-size: medium; background-color: #FF0000" OnClick="Button1_Click" />
                            </td>

                            <td style="width: 5%;">
                                <asp:Button runat="server" CssClass="buttonn" ID="PrintButton" BackColor="Green" Text="SaveAsPdf" Visible="true" OnClick="PrintButton_Click" /></td>
                        </tr>
                        <tr>
                            <td style="height: 20px;" colspan="3"></td>
                        </tr>

                    </tbody>
                </table>
                <div class="allSides" id="divheader" runat="server" style="width: 100%;">
                    <table style="font-family: Calibri; width: 100%;">
                        <tr style="background-color: #F2F2F2; height: 40px">

                            <td style="width: 2.4%; font-size: large;"></td>
                            <td style="width: 13.6%; font-size: large;">Varenr</td>
                            <td style="width: 13.8%; font-size: large;">Kategori</td>
                            <td style="width: 19.6%; font-size: large;">Varebeskrivelse</td>
                            <td style="width: 11.8%; font-size: large;">Pris</td>
                            <td style="width: 12%; font-size: large;">Enhed</td>
                            <td style="width: 13.6%; font-size: large;">Specifikation</td>
                            <td style="width: 12.2%; font-size: large;">Vis</td>

                        </tr>
                    </table>
                </div>
                <div id="stocklist" runat="server" style="overflow: auto; width: 100%; height: 850px;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table style="width: 100%; font-family: Calibri;">
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" ShowHeader="False" HeaderStyle-BackColor="#F2F2F2" Width="100%" HeaderStyle-Font-Size="Large" RowStyle-Height="30px" BorderColor="White" OnRowCommand="GridView2_RowCommand" OnRowDataBound="Bound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="stkChkbox" runat="server" Visible="false" />
                                                        <asp:Label ID="ItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="2%" BorderColor="White" BorderWidth="5" />
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="ItemCode" HeaderText="ItemCode" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />
                                                <asp:BoundField DataField="StockNumber" HeaderText="Stock No" ItemStyle-Width="14%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                <asp:BoundField DataField="Type" HeaderText="Stock Type" ItemStyle-Width="14%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                <asp:BoundField DataField="ItemName" HeaderText=" Item Name" ItemStyle-Width="20%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                <asp:BoundField DataField="Price" HeaderText="Price" ItemStyle-Width="12%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                <asp:BoundField DataField="Quantity" HeaderText="Quantity" ItemStyle-Width="12%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />
                                                <asp:BoundField DataField="ItemProperty1" HeaderText="Property" ItemStyle-Width="14%" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />

                                                <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px">
                                                    <ItemTemplate>
                                                        <%--   <asp:LinkButton ID="lnkViewStock" runat="server" Font-Underline="false" OnClick="lnkViewStock_Click"><span class="action-icon icon-eye"></asp:LinkButton>--%>
                                                        <asp:LinkButton CssClass="simple" ID="lnkUpdateStock" runat="server" Font-Underline="false" OnClick="lnkUpdateStock_Click" CommandArgument='<%#Eval("ItemCode") %>' CommandName="UpdateRow"><span class="action-icon icon-eye"></span></asp:LinkButton>
                                                        <asp:LinkButton CssClass="simple" ID="lnkDeleteStock" runat="server" Font-Underline="false" OnClick="lnkDeleteStock_Click" CommandArgument='<%#Eval("ItemCode") %>' CommandName="DeleteRow" Visible="false"><span class="action-icon icon-trashcan"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="12%" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="TextBox1" EventName="TextChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0">
                        <ProgressTemplate>
                            <div class="modal">
                                <div class="center">
                                    <img src="image/22.gif" />
                                </div>

                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
