﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;

namespace ProjectManagement
{
    public partial class PrintStockDetails : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (string.IsNullOrEmpty(Session["username"] as string))
                //{
                //    //Response.Redirect("LoginPage.aspx");
                //}
                //else
                //{
                //    //Label1.Text = Session["username"].ToString();

                //}
                Session["REPORTTYPE"] = "";
                String flag = Request.QueryString["flag"];
                if (flag == "True")
                {
                    lblType.Text = flag;

                    BindStock();
                }
                else if (flag == "False")
                {
                    lblType.Text = flag;
                    BindHeavyStock();
                }


            }
        }

        public void BindStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from STOCKSITEM where UseLight = 1", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GviewProjectStock.DataSource = dt;

                GviewProjectStock.DataBind();
            }
            con.Close();
        }

        public void BindHeavyStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from STOCKSITEM where UseHeavy = 1", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GviewProjectStock.DataSource = dt;

                GviewProjectStock.DataBind();
            }
            con.Close();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

            /* Verifies that the control is rendered */

        }
        protected void GenerateLightStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "UPDATE LEFTTEMPSTOCK SET Quantity='',SubTotal=0,GRANDTOTAL=0";
            com.ExecuteNonQuery();
            com.Parameters.Clear();
            com.CommandText = "UPDATE RIGHTTEMPSTOCK SET Quantity='',TOTAL=0,GRANDTOTAL=0";
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            foreach (GridViewRow rows in GviewProjectStock.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label LblItemcode = (rows.Cells[0].FindControl("LblItemcode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblType = (rows.Cells[0].FindControl("lblType") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtQuntity = (rows.Cells[2].FindControl("txtQuntity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label ItemFees = (rows.Cells[3].FindControl("ItemFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label LblTotAmt = (rows.Cells[4].FindControl("LblTotAmt") as System.Web.UI.WebControls.Label);
                    int itemcode = Convert.ToInt32(LblItemcode.Text);

                    if (itemcode < 52)
                    {
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_UpdateLEFTTEMPSTOCK";
                        com.Parameters.Add("@IITEMCODE", System.Data.SqlDbType.BigInt).Value = itemcode;
                        com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.NVarChar).Value = ItemFees.Text;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                        if (LblTotAmt.Text != "")
                        {
                            com.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                        }
                        else
                        {
                            com.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                        }
                        if (LblGrantTotal.Text != "")
                        {
                            com.Parameters.Add("@GRANDTOTAL", System.Data.SqlDbType.Decimal).Value = LblGrantTotal.Text;
                        }
                        else
                        {
                            com.Parameters.Add("@GRANDTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                        }
                        com.ExecuteNonQuery();
                        com.Parameters.Clear();

                    }
                    else
                    {
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_UpdateRIGHTTEMPSTOCK";
                        com.Parameters.Add("@IITEMCODE", System.Data.SqlDbType.BigInt).Value = itemcode;
                        com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.NVarChar).Value = ItemFees.Text;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                        if (LblTotAmt.Text != "")
                        {
                            com.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                        }
                        else
                        {
                            com.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                        }
                        if (LblGrantTotal.Text != "")
                        {
                            com.Parameters.Add("@GRANDTOTAL", System.Data.SqlDbType.Decimal).Value = LblGrantTotal.Text;
                        }
                        else
                        {
                            com.Parameters.Add("@GRANDTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                        }

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                    }



                }
            }
            Session["REPORTTYPE"] = 1;

            string url = "ViewReport.aspx";
            string s = "window.open('" + url + "');";

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
            return;
        }
        protected void GenerateHeavyStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "UPDATE HEAVYLEFTTEMPSTOCK SET Quantity='',SubTotal=0,GRANDTOTAL=0";
            com.ExecuteNonQuery();
            com.Parameters.Clear();
            com.CommandText = "UPDATE HEAVYRIGHTTEMPSTOCK SET Quantity='',SubTotal=0,GRANDTOTAL=0";
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            foreach (GridViewRow rows in GviewProjectStock.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label LblItemcode = (rows.Cells[0].FindControl("LblItemcode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblType = (rows.Cells[0].FindControl("lblType") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtQuntity = (rows.Cells[2].FindControl("txtQuntity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label ItemFees = (rows.Cells[3].FindControl("ItemFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label LblTotAmt = (rows.Cells[4].FindControl("LblTotAmt") as System.Web.UI.WebControls.Label);
                    int itemcode = Convert.ToInt32(LblItemcode.Text);

                    if (itemcode < 162)
                    {
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_UpdateHEAVYLEFTTEMPSTOCK";
                        com.Parameters.Add("@IITEMCODE", System.Data.SqlDbType.BigInt).Value = itemcode;
                        com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.NVarChar).Value = ItemFees.Text;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                        if (LblTotAmt.Text != "")
                        {
                            com.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                        }
                        else
                        {
                            com.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                        }
                        if (LblGrantTotal.Text != "")
                        {
                            com.Parameters.Add("@GRANDTOTAL", System.Data.SqlDbType.Decimal).Value = LblGrantTotal.Text;
                        }
                        else
                        {
                            com.Parameters.Add("@GRANDTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                        }
                        com.ExecuteNonQuery();
                        com.Parameters.Clear();

                    }
                    else
                    {
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_UpdateHEAVYRIGHTTEMPSTOCK";
                        com.Parameters.Add("@IITEMCODE", System.Data.SqlDbType.BigInt).Value = itemcode;
                        com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.NVarChar).Value = ItemFees.Text;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuntity.Text;
                        if (LblTotAmt.Text != "")
                        {
                            com.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = LblTotAmt.Text;
                        }
                        else
                        {
                            com.Parameters.Add("@SUBTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                        }
                        if (LblGrantTotal.Text != "")
                        {
                            com.Parameters.Add("@GRANDTOTAL", System.Data.SqlDbType.Decimal).Value = LblGrantTotal.Text;
                        }
                        else
                        {
                            com.Parameters.Add("@GRANDTOTAL", System.Data.SqlDbType.Decimal).Value = 0;
                        }

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                    }



                }
            }

            Session["REPORTTYPE"] = 2;

            string url = "ViewReport.aspx";
            string s = "window.open('" + url + "');";

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
            return;

        }
        protected void BtnPDFGene_Click(object sender, EventArgs e)
        {

            if (lblType.Text == "True")
            {
                GenerateLightStock();
            }
            else if (lblType.Text == "False")
            {
                GenerateHeavyStock();
            }



        }
        //protected void BtnPDFGene_Click(object sender, EventArgs e)
        //{
        //    using (System.IO.StringWriter sw = new System.IO.StringWriter())
        //    {
        //        using (HtmlTextWriter hw = new HtmlTextWriter(sw))
        //        {
        //            //To Export all pages
        //            GviewProjectStock.AllowPaging = false;
        //            //this.BindStock();

        //            TopDiv.RenderControl(hw);
        //            divheader.RenderControl(hw);
        //            //TopMainDiv.RenderControl(hw);
        //            GviewProjectStock.RenderControl(hw);
        //            DivFooter.RenderControl(hw);
        //            //GviewProjectStock.HeaderRow.Style.Add("width", "15%");
        //            //GviewProjectStock.HeaderRow.Style.Add("font-size", "10px");

        //            //TopMainDiv.Style.Add("text-decoration", "bold");
        //            //TopMainDiv.Style.Add("font-family", "Arial, Helvetica, sans-serif;");
        //            //TopMainDiv.Style.Add("font-size", "25px");

        //            String attachment = "attachment;filename= StockDetails.pdf " + DateTime.Now + "";
        //            System.IO.StringReader sr = new System.IO.StringReader(sw.ToString());
        //            Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
        //            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //            pdfDoc.Open();
        //            htmlparser.Parse(sr);
        //            pdfDoc.Close();

        //            Response.ContentType = "application/pdf";

        //            Response.AddHeader("content-disposition", attachment);
        //            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //            Response.Write(pdfDoc);
        //            Response.End();
        //        }
        //    }



        //}

        protected void txtQuntity_TextChanged(object sender, EventArgs e)
        {



        }

        protected void txtQuntity_TextChanged1(object sender, EventArgs e)
        {
            foreach (GridViewRow rows in GviewProjectStock.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("stkChkbox") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.TextBox txtQuntity = (rows.Cells[1].FindControl("txtQuntity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label ItemFees = (rows.Cells[2].FindControl("ItemFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label LblTotAmt = (rows.Cells[3].FindControl("LblTotAmt") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblQuntity = (rows.Cells[1].FindControl("lblQuntity") as System.Web.UI.WebControls.Label);
                    //System.Web.UI.WebControls.DropDownList LblTotAmt1 = (rows.Cells[4].FindControl("LblTotAmt1") as System.Web.UI.WebControls.DropDownList);


                    if (txtQuntity.Text != "")
                    {

                        if (ItemFees.Text != "")
                        {

                            decimal Price = Convert.ToDecimal(ItemFees.Text);
                            long qty = Convert.ToInt64(txtQuntity.Text);
                            decimal total = 0;
                            decimal pricqty = 0;
                            pricqty = Price * qty;
                            total = total + pricqty;
                            LblTotAmt.Text = total.ToString();

                            //if (ViewState["TotalPrice"] == null)
                            //{
                            //    ViewState["TotalPrice"] = lblTot.Text;
                            //}
                            //if (rows.RowType == DataControlRowType.Footer)
                            //{
                            //    System.Web.UI.WebControls.TextBox txttot = (rows.Cells[4].FindControl("txttot") as System.Web.UI.WebControls.TextBox);
                            if (LblGrantTotal.Text != "")
                            {
                                LblGrantTotal.Text = (Convert.ToDecimal(LblGrantTotal.Text) + Convert.ToDecimal(LblTotAmt.Text)).ToString();
                                //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                            }
                            else
                            {
                                LblGrantTotal.Text = LblTotAmt.Text;
                                //txttot.Text = lblTot.Text;
                            }
                            lblQuntity.Text = txtQuntity.Text;
                            lblQuntity.Visible = true;
                            txtQuntity.Visible = false;
                        }
                        else
                        {
                            txtQuntity.Text = "";
                        }
                      
                    }
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (LblGrantTotal.Text != "")
            {          
            Global.TotIFees =Convert.ToDecimal(LblGrantTotal.Text);
            Session["TotalFees"] = LblGrantTotal.Text;
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "ClosePage", "window.close();", true);
            }
            else
            {
                Global.TotIFees = 0;
                Session["TotalFees"] = "";
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "ClosePage", "window.close();", true);
            }
            //Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
            //this.Dispose();
            //Response.Redirect("Salary Module.aspx");

        }


    }
}