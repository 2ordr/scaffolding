﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectManagement
{
    public partial class ViewFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string filepath = Session["FILEPATH"].ToString();

            if (filepath.Contains(".pdf"))
            {
                Response.ContentType = "Application/pdf";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".doc"))
            {
                Response.ContentType = "Application/msword";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".rtf"))
            {
                Response.ContentType = "application/rtf";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".txt"))
            {
                Response.ContentType = "text/plain";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".htm") || filepath.Contains(".html") || filepath.Contains(".log"))
            {
                Response.ContentType = "text/HTML";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".jpg") || filepath.Contains(".jpeg"))
            {
                Response.ContentType = "image/jpeg";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".png"))
            {
                Response.ContentType = "image/png";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".tif") || filepath.Contains(".tiff"))
            {
                Response.ContentType = "image/tiff";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".gif"))
            {
                Response.ContentType = "image/gif";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".bmp"))
            {
                Response.ContentType = "image/bmp";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".asf"))
            {
                Response.ContentType = "video/x-ms-asf";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".avi"))
            {
                Response.ContentType = "video/avi";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".zip"))
            {
                Response.ContentType = "application/zip";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".xls") || filepath.Contains(".csv"))
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".wav"))
            {
                Response.ContentType = "audio/wav";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".mp3"))
            {
                Response.ContentType = "audio/mpeg3";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".mpg") || filepath.Contains(".mpeg"))
            {
                Response.ContentType = "video/mpeg";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".fdf"))
            {
                Response.ContentType = "application/vnd.fdf";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".ppt"))
            {
                Response.ContentType = "application/mspowerpoint";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".dwg"))
            {
                Response.ContentType = "image/vnd.dwg";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".msg"))
            {
                Response.ContentType = "application/msoutlook";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".xml") || filepath.Contains(".sdxl"))
            {
                Response.ContentType = "application/xml";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".xdp"))
            {
                Response.ContentType = "application/vnd.adobe.xdp+xml";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            if (filepath.Contains(".jar"))
            {
                Response.ContentType = "application/java-archive";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
            Session["FILEPATH"] = null;
        }
    }
}