﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;
using System.Collections;



namespace ProjectManagement
{
    public partial class Order_Form : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    Label1.Text = Session["username"].ToString();
                    BtnViewOrder.Visible = false;
                    //DateTime orddate = DateTime.Today.Date;
                    //txtorderdt.Text = Convert.ToString(orddate);

                    txtorderdt.Text = Request.QueryString["DATETIME"];

                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    com.Connection = con;
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_getQuatationByDate";
                    com.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = Request.QueryString["DATETIME"];

                    SqlDataAdapter adpt = new SqlDataAdapter(com);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        GridViewOrder.DataSource = dt;

                        GridViewOrder.DataBind();

                        txtName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                        TxtTotal.Text = dt.Tables[0].Rows[0]["TotalCost"].ToString();
                        lblName.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                        txtOrderStatus.Text = dt.Tables[0].Rows[0]["Status"].ToString();
                        Lbldate.Text = dt.Tables[0].Rows[0]["DateTime"].ToString();
                        txtTotalEarning.Text = dt.Tables[0].Rows[0]["QuoteEarn"].ToString();
                    }
                    con.Close();
                }
            }
        }

        public void resetall()
        {
            //TxtCustId.Text = "";
            txtName.Text = "";
            txtorderdt.Text = "";
            Txttax.Text = "";
            TxtTotal.Text = "";
            txtPenalty.Text = "0";
            TxtIncentive.Text = "0";
            txtOrderStatus.Text = "";
        }

        protected void BtnViewOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("View Order.aspx?");
        }

        protected void BtnAddOrder_Click(object sender, EventArgs e)
        {
            DateTime orddate = DateTime.Today.Date;
            int OrderID = 0;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            cmd.CommandText = "SELECT (ISNULL(MAX(OrderId),0) + 1) as OrderId FROM ORDERS";
            OrderID = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select * from ORDERS where OrderDate='" + txtorderdt.Text + "'";
          
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count <= 0)
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_InsertOrder";

                cmd.Parameters.Add("@I_ORDID", System.Data.SqlDbType.BigInt).Value = OrderID;
                cmd.Parameters.Add("@ORDERDATE", System.Data.SqlDbType.NVarChar).Value = txtorderdt.Text;
                cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = Txttax.Text;
                cmd.Parameters.Add("@ORDERTOTAL", System.Data.SqlDbType.Decimal).Value = TxtTotal.Text;
                cmd.Parameters.Add("@PENALTY", System.Data.SqlDbType.VarChar).Value = txtPenalty.Text;
                cmd.Parameters.Add("@INCENTIVE", System.Data.SqlDbType.VarChar).Value = TxtIncentive.Text;
                cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtOrderStatus.Text;
                cmd.Parameters.Add("@ORDERNAME", System.Data.SqlDbType.NVarChar).Value = OrderID + " - " + txtName.Text;

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Order Generated Successfully!!');", true);
                con.Close();
                resetall();
                BtnAddOrder.Visible = false;
                BtnViewOrder.Visible = true;
            }

            else
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_UpdateOrder";

                if (Lbldate.Text == txtorderdt.Text)
                {
                    cmd.Parameters.Add("@ORDERDATE", System.Data.SqlDbType.NVarChar).Value = txtorderdt.Text;
                    cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = Txttax.Text;
                    cmd.Parameters.Add("@ORDERTOTAL", System.Data.SqlDbType.Decimal).Value = TxtTotal.Text;
                    cmd.Parameters.Add("@PENALTY", System.Data.SqlDbType.VarChar).Value = txtPenalty.Text;
                    cmd.Parameters.Add("@INCENTIVE", System.Data.SqlDbType.VarChar).Value = TxtIncentive.Text;
                    cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtOrderStatus.Text;

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    con.Close();

                    BtnAddOrder.Visible = false;
                    BtnViewOrder.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('Order Update Successfully');", true);
                    return;
                }
            }
            //int id = 0;
            //cmd.CommandText = "SELECT (ISNULL(MAX(OrderId),0) + 1) as OrderId FROM ORDERS";
            //id = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            //cmd.Parameters.Clear();

            //foreach (GridViewRow rows in GridViewOrder.Rows)
            //{

            //    if (rows.RowType == DataControlRowType.DataRow)
            //    {
            //        System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
            //        System.Web.UI.WebControls.Label ItemName = (rows.Cells[1].FindControl("ItemName") as System.Web.UI.WebControls.Label);
            //        System.Web.UI.WebControls.Label lblItemPrice = (rows.Cells[2].FindControl("lblItemPrice") as System.Web.UI.WebControls.Label);
            //        System.Web.UI.WebControls.Label lblItemQua = (rows.Cells[3].FindControl("lblItemQua") as System.Web.UI.WebControls.Label);
            //        System.Web.UI.WebControls.Label lblItemtotal = (rows.Cells[4].FindControl("lblItemtotal") as System.Web.UI.WebControls.Label);
            //        System.Web.UI.WebControls.Label lblTot = (rows.Cells[5].FindControl("lblTot") as System.Web.UI.WebControls.Label);

            //        //cmd.Connection = con;
            //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //        cmd.CommandText = "sp_UpdateHAS";
            //        cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblName.Text;
            //        cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
            //        cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = Lbldate.Text;
            //        cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
            //        cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = lblItemQua.Text;
            //        cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtOrderStatus.Text;
            //        cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";

            //        cmd.ExecuteNonQuery();
            //        cmd.Parameters.Clear();
            //    }

            //}

        }

    }
}