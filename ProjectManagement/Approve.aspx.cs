﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;

namespace ProjectManagement
{
    public partial class Approve : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    string UserType = Session["UserType"].ToString();
                    Session["Projectid"] = "";
                    Session["EntryDate"] = "";
                    Session["Type"] = "";
                    //if (UserType == "Admin")
                    //{
                    //    btnAdminApp.Visible = true;

                    //}
                    //else
                    //{
                    //    btnAdminApp.Visible = false;
                    //}
                    BindSalaryReport();
                }
            }

        }

        public void BindSalaryReport()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            //string TYPE = "";
            //if (RBtnUseQuate.Checked == true)
            //{
            //    TYPE = "Hours";
            //}
            //else if (RBtnUseLight.Checked == true)
            //{
            //    TYPE = "LightWeight";
            //}
            //else if (RBtnUseHeavy.Checked == true)
            //{
            //    TYPE = "HeavyWeight";
            //}
            SqlCommand com1 = new SqlCommand("select distinct E.EmpId,E.FName,S.Hours,S.ContributionFees,S.TYPE, CONVERT(VARCHAR, CONVERT(DATETIME,S.EntryDate,106),103) AS EntryDate,S.STATUS,S.ProjectId,S.TotalItemFees from EMPLOYEE E, SALARYREPORT S where E.EmpId = S.EMPID AND S.STATUS <> 'Approve' ", con);  //AND S.TYPE='" + TYPE + "'
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridSalary.DataSource = dt;
                GridSalary.DataBind();
                divSalary.Visible = true;
            }
            else
            {
                GridSalary.DataSource = null;
                GridSalary.DataBind();
                divSalary.Visible = false;

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No User Salary For Approvement');", true);
                return;
            }


            com1.Parameters.Clear();
            dt.Rows.Clear();
            con.Close();
        }

        public void BindAllSalaryReport()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            string TYPE = "";
            if (RBtnUseQuate.Checked == true)
            {
                TYPE = "Hours";
            }
            else if (RBtnUseLight.Checked == true)
            {
                TYPE = "LightWeight";
            }
            else if (RBtnUseHeavy.Checked == true)
            {
                TYPE = "HeavyWeight";
            }
            SqlCommand com1 = new SqlCommand("select distinct E.EmpId,E.FName,S.Hours,S.ContributionFees,S.TYPE, CONVERT(VARCHAR, CONVERT(DATETIME,S.EntryDate,106),103) AS EntryDate,S.STATUS,S.ProjectId,S.TotalItemFees from EMPLOYEE E, SALARYREPORT S where E.EmpId = S.EMPID AND S.STATUS <> 'Approve' AND S.TYPE='" + TYPE + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridSalary.DataSource = dt;
                GridSalary.DataBind();
                divSalary.Visible = true;
            }
            else
            {
                GridSalary.DataSource = null;
                GridSalary.DataBind();
                divSalary.Visible = false;

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No User Salary For Approvement');", true);
                return;
            }


            com1.Parameters.Clear();
            dt.Rows.Clear();
            con.Close();
        }

        protected void RBtnUseLight_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseQuate.Checked = false;
            RadioButtonAll.Checked = false;
            BindAllSalaryReport();
        }

        protected void RBtnUseHeavy_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseLight.Checked = false;
            RBtnUseQuate.Checked = false;
            RadioButtonAll.Checked = false;
            BindAllSalaryReport();
        }

        protected void RBtnUseQuate_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseLight.Checked = false;
            RadioButtonAll.Checked = false;
            BindAllSalaryReport();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string type = "";

            foreach (GridViewRow rows in GridSalary.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblProjectId = (rows.Cells[0].FindControl("lblProjectId") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblEDate = (rows.Cells[1].FindControl("lblEDate") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblEmpid = (rows.Cells[2].FindControl("lblEmpid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblHours = (rows.Cells[3].FindControl("lblHours") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblTotalFees = (rows.Cells[4].FindControl("lblTotalFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblContributionFees = (rows.Cells[4].FindControl("lblContributionFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label LType = (rows.Cells[5].FindControl("LType") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.DropDownList ddlStatus = (rows.Cells[6].FindControl("ddlStatus") as System.Web.UI.WebControls.DropDownList);

                    System.Web.UI.WebControls.Label LblStatus = (rows.Cells[6].FindControl("LblStatus") as System.Web.UI.WebControls.Label);


                    string dtEntry1 = lblEDate.Text;
                    DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                    if (RBtnUseQuate.Checked == true)
                    {
                        type = "Hours";
                    }
                    else if (RBtnUseLight.Checked == true)
                    {
                        type = "LightWeight";
                    }
                    else if (RBtnUseHeavy.Checked == true)
                    {
                        type = "HeavyWeight";
                    }
                    else if (RadioButtonAll.Checked == true)
                    {
                        type = LType.Text;
                    }

                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand cm1 = new SqlCommand();
                    cm1.Connection = con;
                    cm1.CommandType = System.Data.CommandType.StoredProcedure;
                    cm1.CommandText = "sp_UpdateSalaryReportsAdminNew";
                    cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                    cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                    cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = LType.Text;
                    cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = LblStatus.Text;        //DDLStockItem.SelectedValue;
                    cm1.Parameters.Add("@STATUS1", System.Data.SqlDbType.VarChar).Value = ddlStatus.SelectedItem.Text;
                    cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = lblHours.Text;
                    cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = lblContributionFees.Text;        //DDLStockItem.SelectedValue;
                    cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = lblTotalFees.Text;
                    cm1.ExecuteNonQuery();
                    cm1.Parameters.Clear();

                    cm1 = new SqlCommand("SELECT SUM(Hours),SUM(ContributionFees),SUM(AHours),SUM(AContributionFees) from SALARYREPORT where EmpId=@I_EMPID  AND ProjectId=@I_PROJECTID and Type=@TYPE and Status=@STATUS1 ", con);
                    cm1.Parameters.AddWithValue("@I_EMPID", lblEmpid.Text);
                    cm1.Parameters.AddWithValue("@TYPE", LType.Text);
                    cm1.Parameters.AddWithValue("@I_PROJECTID", lblProjectId.Text);
                    cm1.Parameters.AddWithValue("@STATUS1", ddlStatus.SelectedItem.Text);
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cm1);
                    DataSet dt1 = new DataSet();
                    adpt1.Fill(dt1);
                    double totalhours = 0;
                    double contributionfees = 0;
                    double totalAhours = 0;
                    double Acontributionfees = 0;
                    totalhours = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column1"].ToString());
                    contributionfees = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column2"].ToString());
                    totalAhours = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column3"].ToString());
                    Acontributionfees = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column4"].ToString());

                    cm1.ExecuteNonQuery();
                    cm1.Parameters.Clear();
                    cm1 = new SqlCommand("DELETE from SALARYREPORT where EmpId=@I_EMPID  AND ProjectId=@I_PROJECTID and Type=@TYPE and Status=@STATUS1 ", con);
                    cm1.Parameters.AddWithValue("@I_EMPID", lblEmpid.Text);
                    cm1.Parameters.AddWithValue("@TYPE", LType.Text);
                    cm1.Parameters.AddWithValue("@I_PROJECTID", lblProjectId.Text);
                    cm1.Parameters.AddWithValue("@STATUS1", ddlStatus.SelectedItem.Text);

                    cm1.ExecuteNonQuery();
                    cm1.Parameters.Clear();

                    cm1.CommandType = System.Data.CommandType.StoredProcedure;
                    cm1.CommandText = "sp_UpdateSalaryReportsNew";
                    cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                    cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                    cm1.Parameters.Add("@I_TASKID", System.Data.SqlDbType.BigInt).Value = 0;
                    cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = 0;
                    cm1.Parameters.Add("@TOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = lblTotalFees.Text;
                    cm1.Parameters.Add("@I_STID", System.Data.SqlDbType.BigInt).Value = 0;
                    cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = totalhours;
                    cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;
                    cm1.Parameters.Add("@CONTRIBUTION", System.Data.SqlDbType.Decimal).Value = contributionfees;
                    cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                    cm1.Parameters.Add("@STATUS1", System.Data.SqlDbType.VarChar).Value = ddlStatus.SelectedItem.Text;
                    cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = totalAhours;
                    cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = Acontributionfees;
                    cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = lblTotalFees.Text;

                    cm1.ExecuteNonQuery();
                    cm1.Parameters.Clear();
                    con.Close();


                }

            }
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Salary Updated');", true);
            return;
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports.aspx");
        }

        protected void GridSalary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList ddlStatus = (DropDownList)e.Row.FindControl("ddlStatus");
                Label LblStatus = (Label)e.Row.FindControl("LblStatus");
                Label lblEDate = (Label)e.Row.FindControl("lblEDate");

                ddlStatus.SelectedItem.Text = LblStatus.Text;
            }

        }

        protected void lnkbtnView_Click(object sender, EventArgs e)
        {
            LinkButton lnkbtnView = (LinkButton)sender;
            GridViewRow grdrRow = ((GridViewRow)lnkbtnView.Parent.Parent);
            Label lblProjectId = (Label)grdrRow.FindControl("lblProjectId");
            Label lblEDate = (Label)grdrRow.FindControl("lblEDate");
            Label LType = (Label)grdrRow.FindControl("LType");
            Label lblEmpid = (Label)grdrRow.FindControl("lblEmpid");

            Session["Projectid"] = lblProjectId.Text;
            Session["EntryDate"] = lblEDate.Text;
            Session["Type"] = LType.Text;
            Session["Empid"] = lblEmpid.Text;

            //DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            Response.Redirect("EditSalary.aspx");
        }

        protected void RadioButtonAll_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseLight.Checked = false;
            RBtnUseQuate.Checked = false;
            BindSalaryReport();
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            string TYPE = "";
            if (RBtnUseQuate.Checked == true)
            {
                TYPE = "Hours";
            }
            else if (RBtnUseLight.Checked == true)
            {
                TYPE = "LightWeight";
            }
            else if (RBtnUseHeavy.Checked == true)
            {
                TYPE = "HeavyWeight";
            }

            if (TYPE.ToString() != "")
            {
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "select distinct E.EmpId,E.FName,S.Hours,S.ContributionFees,S.TYPE, CONVERT(VARCHAR, CONVERT(DATETIME,S.EntryDate,106),103) AS EntryDate,S.STATUS,S.ProjectId,S.TotalItemFees from EMPLOYEE E, SALARYREPORT S where  E.FName Like @USERNAME AND E.EmpId = S.EMPID AND S.STATUS <> 'Approve' AND S.TYPE='" + TYPE + "'";
                cmd.Parameters.Add("@USERNAME", System.Data.SqlDbType.VarChar).Value = "%" + TextBox1.Text + "%";
            }
            else
            {
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "select distinct E.EmpId,E.FName,S.Hours,S.ContributionFees,S.TYPE, CONVERT(VARCHAR, CONVERT(DATETIME,S.EntryDate,106),103) AS EntryDate,S.STATUS,S.ProjectId,S.TotalItemFees from EMPLOYEE E, SALARYREPORT S where  E.FName Like @USERNAME AND  E.EmpId = S.EMPID AND S.STATUS <> 'Approve'";
                cmd.Parameters.Add("@USERNAME", System.Data.SqlDbType.VarChar).Value = "%" + TextBox1.Text + "%";
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridSalary.DataSource = dt;
                GridSalary.DataBind();
                divSalary.Visible = true;
            }
            else
            {
                GridSalary.DataSource = null;
                GridSalary.DataBind();
                divSalary.Visible = false;

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No User Salary For Approvement');", true);
                return;
            }


            cmd.Parameters.Clear();
            dt.Rows.Clear();
            con.Close();
        }
    }
}