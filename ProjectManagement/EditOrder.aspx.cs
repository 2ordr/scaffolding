﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Sql;

namespace ProjectManagement
{
    public partial class EditOrder : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    BtnViewOrder.Visible = true;
                    BtnUpdateOrder.Visible = false;
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;


                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_getOrderByDate";

                    String var = Request.QueryString["Quatdate"];

                    //DateTime dt2 = DateTime.ParseExact(var);
                    //DateTime dt2 = Convert.ToDateTime(var);

                    cmd.Parameters.Add("@ORDERDATE", System.Data.SqlDbType.NVarChar).Value = var;

                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        GridViewEditOrder.DataSource = dt;
                        GridViewEditOrder.DataBind();

                        txtorderdt.Text = dt.Tables[0].Rows[0]["OrderDate"].ToString();
                        txtName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                        TxtTotal.Text = dt.Tables[0].Rows[0]["OrderTotal"].ToString();
                        ddquotestatus.Text = dt.Tables[0].Rows[0]["Status"].ToString();
                        TxtIncentive.Text = dt.Tables[0].Rows[0]["Incentive"].ToString();
                        Txttax.Text = dt.Tables[0].Rows[0]["Taxes"].ToString();
                        txtPenalty.Text = dt.Tables[0].Rows[0]["Penalty"].ToString();
                        lblCustId.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                        lblorderId.Text = dt.Tables[0].Rows[0]["OrderId"].ToString();

                        txtTotalEarning.Text = dt.Tables[0].Rows[0]["QuoteEarn"].ToString();

                        Lbldate.Text = (txtorderdt.Text);
                    }
                    con.Close();
                }
            }
        }

        protected void BtnUpdateOrder_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            //int id = 0;
            //cmd.CommandText = "SELECT (ISNULL(MAX(OrderId),0)) as OrderId FROM ORDERS";
            //id = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            //cmd.Parameters.Clear();

            foreach (GridViewRow rows in GridViewEditOrder.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[0].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemName = (rows.Cells[1].FindControl("ItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemPrice = (rows.Cells[2].FindControl("lblItemPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemQuantity = (rows.Cells[3].FindControl("lblItemQuantity") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemtotal = (rows.Cells[4].FindControl("lblItemtotal") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblTot = (rows.Cells[5].FindControl("lblTot") as System.Web.UI.WebControls.Label);

                    //cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_UpdateHAS";
                    cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                    cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                    cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = txtorderdt.Text;
                    cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = lblorderId.Text;
                    cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = lblItemQuantity.Text;
                    cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedItem.Text;
                    cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                    cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = txtorderdt.Text;
                    cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = txtTotalEarning.Text;

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }

            }

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_UpdateOrder";

            if (Lbldate.Text == txtorderdt.Text)
            {
                cmd.Parameters.Add("@ORDERDATE", System.Data.SqlDbType.NVarChar).Value = txtorderdt.Text;
                cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = Txttax.Text;
                cmd.Parameters.Add("@ORDERTOTAL", System.Data.SqlDbType.Decimal).Value = TxtTotal.Text;
                cmd.Parameters.Add("@PENALTY", System.Data.SqlDbType.VarChar).Value = txtPenalty.Text;
                cmd.Parameters.Add("@INCENTIVE", System.Data.SqlDbType.VarChar).Value = TxtIncentive.Text;
                cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedItem.Text;

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                con.Close();

                BtnUpdateOrder.Visible = false;
                BtnViewOrder.Visible = true;

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('Order Update Successfully');", true);
                return;
            }
            else
            {
                txtorderdt.Text = Lbldate.Text;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('Order Not Update Successfully');", true);
                return;
            }

        }

        protected void BtnViewOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("View Order.aspx");
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //27 June ------Prathamesh
            //Added this code for generation of print for order.
            string script = "<script>PrintGridDataAll();</script>";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), script, false);
        }
    }
}