﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewUserVertical.aspx.cs" Inherits="ProjectManagement.ViewUserVertical" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
     <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style>
        headform1 {
            color: #383838;
        }

        .bc {
            border-color: brown;
        }


        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        .bg {
            text-align: right;
        }
    </style>

    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>

    <script type="text/javascript">
        function CheckSingleCheckbox(ob) {
            var grid = ob.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (ob.checked && inputs[i] != ob && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }
    </script>

     <script type="text/javascript">
         function Confirmationbox() {
             var result = confirm('Are you sure you want to delete selected User(s)?');
             if (result) {
                 return true;
             }
             else {
                 return false;
             }
         }
    </script> 

</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">

        <div style="height: 1000px; width: 1920px">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">
                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txtSearchBox" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150" OnTextChanged="txtSearchBox_TextChanged"></asp:TextBox>

                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Dashboard</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Customer</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon  app-clients"></span><span class="menu-item-text">Contacts</span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Quatation</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Order</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="View Project.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projects</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Setting</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <ul class="menu-group" id="ext-gen3448">

                                                        <li class="menu-item menu-app-item app-item item-child x-item-selected active" id="menu-item-8" data-index="8"><a class="menu-link" href="ViewUserVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">My Tasks</span></a></li>

                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                --%>
                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-15" data-index="15"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <div style="float: left; height: 1000px; width: 300px; border-color: silver; border-width: 5px">
                    <fieldset style="height: 1000px">
                        <legend style="color: black; font-weight: bold; font-size: small"></legend>
                        <div style="height: 10px;">
                        </div>
                        <table style="width: 100%; font-family: Calibri;">
                            <tbody>
                                <tr>
                                    <td style="width: 50%;">
                                        <asp:Button CssClass="button" ID="BtnAddUsers" runat="server" Text="+Add Users" BackColor="#CC6699" Style="color: #FFFFFF; font-weight: 500; font-size: medium; background-color: #FF0000" OnClick="BtnAddUsers_Click" />
                                    </td>
                                    <td style="width: 50%;">
                                        <table class="x-table-layout" id="ext-gen1688" role="presentation" cellspacing="0" cellpadding="0">
                                            <tbody role="presentation">
                                                <tr role="presentation">
                                                    <td class="x-table-layout-cell " role="presentation" rowspan="1" colspan="1">
                                                        <a tabindex="0" class="x-btn x-unselectable x-sbtn-first x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon" id="button-1109" href="#" hidefocus="on" unselectable="on" data-qtip="Simple List">
                                                            <span class="x-btn-wrap" id="button-1109-btnWrap" role="presentation" unselectable="on">
                                                                <span class="x-btn-button" id="button-1109-btnEl" role="presentation">
                                                                    <span class="x-btn-inner x-btn-inner-center" id="button-1109-btnInnerEl" unselectable="on">&nbsp;</span>
                                                                    <span class="x-btn-icon-el icon-split " id="button-1109-btnIconEl" role="presentation" unselectable="on">&nbsp;</span>
                                                                </span>
                                                            </span>
                                                        </a>
                                                    </td>
                                                    <td class="x-table-layout-cell " role="presentation" rowspan="1" colspan="1"><a tabindex="0" href="#" class="x-btn x-unselectable x-sbtn-last x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon x-pressed x-btn-pressed x-btn-default-small-pressed" id="button-1110" hidefocus="on" unselectable="on" data-qtip="Advanced List"><span class="x-btn-wrap" id="button-1110-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1110-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1110-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-no-split " id="button-1110-btnIconEl" role="presentation" unselectable="on"></span></span></span></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>
                                <%--<tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>--%>
                            </tbody>
                        </table>

                        <div style="height: 850px">
                            <fieldset>
                                <legend style="color: black; font-weight: bold; font-size: large">User Name</legend>
                                <div id="div1" runat="server" style="height: 840px">
                                    <asp:GridView ID="GridViewUser" runat="server" AutoGenerateColumns="False" ShowHeader="false" DataKeyNames="USERID" HeaderStyle-BackColor="#F2F2F2" Width="270px" HeaderStyle-Font-Size="Large" RowStyle-Height="40px" RowStyle-BorderWidth="5px" BorderColor="White" BorderWidth="0px" RowStyle-BorderColor="White">

                                        <Columns>
                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="10px" HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="5px">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" onclick="CheckSingleCheckbox(this)" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("Name") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblUSERID" runat="server" Text='<%# Bind("USERID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="10px" BackColor="White" BorderWidth="0" />
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="USERID" HeaderText="USERID" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="false" />
                                            <asp:BoundField DataField="Name" HeaderText="User Name" ItemStyle-Width="200px" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" />

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </fieldset>
                        </div>
                    </fieldset>
                </div>

                <div id="divVertical" style="float: left; height: 1000px; width: 1420px" runat="server">
                    <div style="width: 100%; height: 90px;">

                        <div style="height: 10px"></div>
                        <div style="height: 40px">
                            <div class="divider" style="height: 30px; width: 50px"></div>
                            <asp:Label ID="LblUserName" runat="server" CssClass="clienthead"></asp:Label>
                            <asp:Label ID="LblUserId" runat="server" Text="" Visible="false"></asp:Label>
                        </div>
                        <div style="height: 40px">
                            <table>
                                <tr>
                                    <td style="height: 30px; width: 50px"></td>
                                    <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnOverview" runat="server" Font-Underline="false" OnClick="LBtnOverview_Click">Overview</asp:LinkButton></td>
                                    <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnBasicInfo" runat="server" Font-Underline="false" OnClick="LBtnBasicInfo_Click">Basic Info</asp:LinkButton></td>


                                    <%-- <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnContacts" runat="server" Font-Underline="false" OnClick="LBtnContacts_Click">Contacts</asp:LinkButton></td>
                                    <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnOrders" runat="server" Font-Underline="false" OnClick="LBtnOrders_Click">Orders</asp:LinkButton></td>
                                    <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnQuotes" runat="server" Font-Underline="false" OnClick="LBtnQuotes_Click">Quotes</asp:LinkButton></td>
                                    <td style="height: 30px; width: 130px">
                                        <asp:LinkButton ID="LBtnProjects" runat="server" Font-Underline="false" OnClick="LBtnProjects_Click">Projects</asp:LinkButton></td>--%>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="gridv" id="divOverView" runat="server" style="height: 900px;" visible="true">
                        <fieldset>

                            <legend style="color: black; font-weight: bold; font-size: medium"></legend>

                            <div style="height: 30px"></div>
                            <div style="height: 820px; width: 100%">
                            </div>

                        </fieldset>
                    </div>

                    <div id="divBasicInfo" runat="server" style="height: 900px;" visible="false">
                        <fieldset>

                            <legend style="color: black; font-weight: bold; font-size: x-large"></legend>

                            <div style="height: 850px; width: 100%">

                                <table style="width: 100%;">

                                    <tr>
                                        <td colspan="3" style="height: 80px; font-weight: bold; font-size: large">Customer Basic Info:</td>
                                    </tr>
                                    <tr>
                                        <td class="bg" style="width: 25%">Name:</td>
                                        <td style="width: 40px"></td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg">UserName:</td>
                                        <td style="width: 40px"></td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtUName" runat="server" Width="300px" Height="31px"></asp:TextBox>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg">Password:</td>
                                        <td style="width: 40px"></td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPassword" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg">Mobile No:</td>
                                        <td style="width: 40px"></td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMobile" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg">Email:</td>
                                        <td style="width: 40px"></td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmail" runat="server" Width="300px" Height="50px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg">Wage Per Hour:</td>
                                        <td style="width: 40px"></td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtWage" runat="server" Width="300px" Height="31px">0:00</asp:TextBox>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td class="bg">User Type:</td>
                                        <td style="width: 40px"></td>
                                        <td colspan="2">
                                            <asp:TextBox CssClass="twitterStyleTextbox" ID="txtUserType" runat="server" Width="300px" Height="31px"></asp:TextBox>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="height: 20px;" colspan="3"></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="shadowdiv" style="width: 100%; height: 100px; text-align: center; border: solid 0px black;">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Button CssClass="buttonn" ID="BtnUpdate" runat="server" Text="Update " Width="101px" Style="background-color: #009900" OnClick="BtnUpdate_Click"/>
                                        </td>                                       
                                        <td>
                                            <asp:Button CssClass="buttonn" ID="BtndeleteCustomer" runat="server" Text="Delete" Width="120px" Style="background-color: #009933" OnClick="BtndeleteCustomer_Click" OnClientClick="javascript:return Confirmationbox();"/>
                                        </td>                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        </fieldset>

                        <asp:GridView CssClass="gridv" ID="GridViewBasicInfo" AutoGenerateColumns="false" Visible="false" runat="server">
                        </asp:GridView>
                    </div>


                </div>

            </div>
        </div>

    </form>
</body>
</html>
