﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="ProjectManagement.CreateUser" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

   <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <title>New - Projects - Scaffolding</title>
    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />
      <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style>
        headform1 {
            color: #383838;
        }


        .bc {
            border-color: brown;
        }


        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }
    </style>

    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>

</head>
<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">
        <div style="height: 1000px; width: 1920px;">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox1" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150"></asp:TextBox>

                                <%--<div class="x-panel-body x-panel-body-default x-box-layout-ct x-panel-body-default x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="searchBox-body" role="presentation" style="left: 0px; top: 0px; width: 177px; height: 28px;">
                                    <div class="x-box-inner " id="searchBox-innerCt" role="presentation" style="width: 177px; height: 28px;">
                                        <div class="x-box-target" id="searchBox-targetEl" role="presentation" style="width: 177px;">

                                            <table class="x-field x-table-plain x-form-item x-form-type-text x-box-item x-field-default x-hbox-form-item" id="combobox-1022" role="presentation" style="margin: 0px; left: 0px; top: 1px; width: 139px; right: auto; table-layout: fixed;" cellpadding="0">
                                                <tbody>
                                                    <tr class="x-form-item-input-row" id="combobox-1022-inputRow" role="presentation">
                                                        <td width="105" class="x-field-label-cell" id="combobox-1022-labelCell" role="presentation" valign="top" style="display: none;" halign="left">
                                                            <label class="x-form-item-label x-unselectable x-form-item-label-left" id="combobox-1022-labelEl" style="width: 100px; margin-right: 5px;" for="combobox-1022-inputEl" unselectable="on"></label>
                                                        </td>
                                                        <td class="x-form-item-body  " id="combobox-1022-bodyEl" role="presentation" style="width: 100%;" colspan="3">
                                                            <table class="x-form-trigger-wrap" id="combobox-1022-triggerWrap" role="presentation" style="width: 100%; table-layout: fixed;" cellspacing="0" cellpadding="0">
                                                                <tbody role="presentation">
                                                                    <tr role="presentation">
                                                                        <td class="x-form-trigger-input-cell" id="combobox-1022-inputCell" role="presentation" style="width: 100%;">
                                                                            <div class="x-hide-display x-form-data-hidden" id="ext-gen1091" role="presentation"></div>
                                                                            <input name="combobox-1022-inputEl" class="x-field-without-trigger x-form-empty-field x-form-text " id="combobox-1022-inputEl" style="width: 100%;" type="text" placeholder="Search (Ctrl+/)" autocomplete="off" /><span class="icon-search"></span></td>
                                                                        <td class=" x-trigger-cell x-unselectable" id="ext-gen1090" role="presentation" valign="top" style="width: 23px; display: none;">
                                                                            <div class="x-trigger-index-0 x-form-trigger x-form-arrow-trigger x-form-trigger-first" id="ext-gen1089" role="presentation"></div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="x-form-invalid-under" id="combobox-1022-errorEl" role="alert" aria-live="polite" style="display: none;" colspan="2"></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="x-splitter x-box-item x-splitter-default x-splitter-vertical x-unselectable" id="splitter-1023" style="margin: 0px; left: 134px; top: 0px; width: 5px; height: 28px; right: auto; display: none;"></div>
                                            <a tabindex="0" class="x-btn btn-collapse x-unselectable x-box-item x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon btn-expanded" id="button-1024" style="border-width: 0px; margin: 0px; left: 149px; top: 2px; right: auto;" hidefocus="on" unselectable="on"><span class="x-btn-wrap" id="button-1024-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1024-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1024-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-collapse " id="button-1024-btnIconEl" role="presentation" unselectable="on">&nbsp;</span></span></span></a>
                                        </div>
                                    </div>
                                </div>--%>
                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Dashboard</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Customer</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon  app-clients"></span><span class="menu-item-text">Contacts</span></a></li>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Quatation</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Order</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="View Project.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projects</span></a></li>

                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Setting</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <ul class="menu-group" id="ext-gen3448">

                                                        <li class="menu-item menu-app-item app-item item-child x-item-selected active" id="menu-item-8" data-index="8"><a class="menu-link" href="ViewUserVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                        <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                <li class="menu-item menu-app-item app-item " id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">My Tasks</span></a></li>

                                                <%--                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                --%>

                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon icon-power-off"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 100%; right: 0px;">

                <div class="allSides" style="width: 100%; height: 80px;">
                    <div class="divider"></div>
                    <asp:Label ID="Label3" runat="server" CssClass="clienthead" Text="Add User"></asp:Label>
                </div>
                <div style="width: 1%; float: left; height: 800px; border: solid 0px black;">
                </div>
                <div style="height: 50px"></div>

                <div style="width: 90%; float: left; height: 820px; font-family: Calibri;">

                    <div style="width: 70%; float: left; height: 800px;">

                        <fieldset>

                            <legend style="color: black; font-weight: bold; font-size: large">User Basic Info</legend>
                            <table style="width: 100%;">
                                <tbody>
                                    <%-- <tr>
                                    <td style="height: 20px;" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td style="width: 60%;">
                                        <div class="divider"></div>
                                        <asp:Label ID="Label2" runat="server" CssClass="clienthead" Text="User Basic Info"></asp:Label>

                                    </td>
                                    <td style="width: 40%;">
                                        <asp:Label ID="Label1" runat="server" CssClass="clienthead" Text="Client Logo"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 35px;" colspan="2"></td>
                                </tr>--%>

                                    <tr>
                                        <td style="width: 60%;">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td colspan="3" style="height: 20px"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Name</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtName" ValidationExpression="[a-zA-Z ]*$"></asp:RegularExpressionValidator>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">UserName</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtUName" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only 15 characters allowed" ControlToValidate="txtUName" ValidationExpression="^[\s\S]{0,15}$"></asp:RegularExpressionValidator>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Password</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPassword" runat="server" Width="300px" Height="31px" TextMode="Password"></asp:TextBox> 
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Minimum 8 characters required" ControlToValidate="txtPassword" ValidationExpression="^[\s\S]{8,50}$"></asp:RegularExpressionValidator>                                                       
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Mobile No.</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtMobile" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Only numbers allowed" ControlToValidate="txtMobile" ValidationExpression="\d+"></asp:RegularExpressionValidator>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Email</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtEmail" runat="server" Width="300px" Height="31px"></asp:TextBox>
                                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please Enter Valid Email ID" ControlToValidate="txtEmail" CssClass="requiredFieldValidateStyle" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">Wage Per Hour</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtWage" runat="server" Width="300px" Height="31px"></asp:TextBox>(Wage Must In Numeric...)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">User Type</td>
                                                    <td style="width: 5px"></td>
                                                    <td colspan="2">
                                                        <asp:DropDownList CssClass="search_categories" ID="ddlUserType" runat="server" Width="300px" Height="36px" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged">
                                                            <asp:ListItem>Admin</asp:ListItem>
                                                            <asp:ListItem>Staff</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="ddlUserType" ValidationGroup="User"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>

                                            </table>
                                        </td>
                                        <%--<td style="width: 40%;">
                                            <table style="width: 100%; font-family: Calibri; margin-top: 2px;">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 25%;"></td>
                                                    <td style="width: 25%;"></td>
                                                    <td style="width: 50%;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%;" colspan="3">
                                                        <asp:Image ID="imgProfileImage" runat="server" Width="140px" Height="100px" ImageUrl="~/image/Untitled.png" /><br />
                                                        Snap Uploaded                                     
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="height: 10px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%;" colspan="3">
                                                        <asp:Button CssClass="button" ID="btnChange" runat="server" Text="Change Picture" Style="background-color: #009900" />
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="height: 80px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 160px;" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 50px;" colspan="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>--%>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <div class="shadowdiv" style="width: 100%; float: left; height: 100px; text-align: center; border: solid 0px black;">
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="Btnadd" runat="server" Text="Save " Width="101px" Style="background-color: #009900" OnClick="Btnadd_Click" ValidationGroup="User" />

                                </td>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="Btnview" runat="server" Text="View" Width="120px" Style="background-color: #009933" OnClick="Btnview_Click" />

                                </td>
                               <%-- <td>
                                    <asp:Button CssClass="buttonn" ID="Btndelete" runat="server" Text="DELETE" Width="120px" Style="background-color: #009933" />
                                </td>--%>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </form>
</body>
</html>
