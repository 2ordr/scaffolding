﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace ProjectManagement
{
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    public class ImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

            try
            {
                string roll_no = context.Request.QueryString["roll_no"].ToString();

                string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

                SqlConnection objconn = new SqlConnection(cs);

                objconn.Open();

                SqlCommand cmd = new SqlCommand("select imagedata from imagedetails where imageid= " + roll_no, objconn);

                SqlDataReader dr = cmd.ExecuteReader();

                dr.Read();

                context.Response.ContentType = "image/jpg";
                context.Response.BinaryWrite((byte[])dr[0]);

                objconn.Close();

            }
            catch (Exception ex)
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}