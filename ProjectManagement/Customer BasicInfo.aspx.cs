﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace ProjectManagement
{
    public partial class Customer_BasicInfo : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            //LblCustName.Text = Session["labeltext"].ToString();
            LblCustId.Text = Session["labelid"].ToString();

            if (!IsPostBack)
            {

                SqlConnection con = new SqlConnection(cs);
                con.Open();


                SqlCommand com = new SqlCommand("select * from CUSTOMER where CustId =" + LblCustId.Text);

                com.Connection = con;

                SqlDataAdapter adpt = new SqlDataAdapter(com);

                DataSet dt = new DataSet();

                adpt.Fill(dt);

                GridViewBasicInfo.DataSource = dt;

                GridViewBasicInfo.DataBind();
                LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                txtName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                txtEmail.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                txtAddress.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                txtcity.Text = dt.Tables[0].Rows[0]["City"].ToString();
                txtcountry.Text = dt.Tables[0].Rows[0]["Country"].ToString();
                txtzip.Text = dt.Tables[0].Rows[0]["ZipCode"].ToString();
                txtMobile.Text = dt.Tables[0].Rows[0]["Mobile"].ToString();
                txtphone.Text = dt.Tables[0].Rows[0]["Phone2"].ToString();
                txtFax.Text = dt.Tables[0].Rows[0]["Fax"].ToString();
                txtProvince.Text = dt.Tables[0].Rows[0]["Province"].ToString();
                txtVatNo.Text = dt.Tables[0].Rows[0]["VATNumber"].ToString();
                txtWebsite.Text = dt.Tables[0].Rows[0]["Website"].ToString();



            }
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Customer.aspx");
        }

        protected void BtnAddCustomer_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddCustomer.aspx");
        }

        protected void DdlistVat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void BtnUpdate_Click(object sender, EventArgs e)
        {

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();
            com.Connection = con1;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_UpdateCustomerDetails";
            com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = LblCustId.Text;
            com.Parameters.Add("@CUSTNAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;
            com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
            com.Parameters.Add("@PHONE2", System.Data.SqlDbType.VarChar).Value = txtphone.Text;
            com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
            com.Parameters.Add("@ADDRESS1", System.Data.SqlDbType.VarChar).Value = txtAddress.Text;
            com.Parameters.Add("@ADDRESS2", System.Data.SqlDbType.VarChar).Value = "";
            com.Parameters.Add("@CITY", System.Data.SqlDbType.VarChar).Value = txtcity.Text;
            com.Parameters.Add("@PROVINCE", System.Data.SqlDbType.VarChar).Value = txtProvince.Text;
            com.Parameters.Add("@REMARK", System.Data.SqlDbType.VarChar).Value = "";
            com.Parameters.Add("@ZIPCODE", System.Data.SqlDbType.VarChar).Value = txtzip.Text;
            com.Parameters.Add("@VATNUMBER", System.Data.SqlDbType.VarChar).Value = txtVatNo.Text;
            com.Parameters.Add("@FAX", System.Data.SqlDbType.VarChar).Value = txtFax.Text;
            com.Parameters.Add("@COUNTRY", System.Data.SqlDbType.VarChar).Value = txtcountry.Text;
            com.Parameters.Add("@WEBSITE", System.Data.SqlDbType.NVarChar).Value = txtWebsite.Text;

            //com.Parameters.Add("@DELETESTATUS", System.Data.SqlDbType.BigInt).Value =false;



            com.ExecuteNonQuery();
            com.Parameters.Clear();
            //con1.Close();
        }

        protected void Btnview_Click(object sender, EventArgs e)
        {
            Response.Redirect("Customer.aspx");
        }
    }
}