﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;

namespace ProjectManagement
{
    public partial class Reports : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        string UserType;
        protected void Page_Load(object sender, EventArgs e)
        {
            UserType = Session["UserType"].ToString();

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    txtboxSearch.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    Session["Projectid"] = "";
                    Session["EntryDate"] = "";
                    Session["Type"] = "";

                    if (UserType == "Admin")
                    {
                        btnAdminApp.Visible = true;
                        ButtonTd.Style.Add(HtmlTextWriterStyle.Width, "8%");
                    }
                    else
                    {
                        btnAdminApp.Visible = false;
                        ButtonTd.Style.Add(HtmlTextWriterStyle.Width, "8%");
                    }
                    DivHeader.Visible = false;
                    DivSalaryInfo.Visible = false;
                    BindSalaryReport();
                }
            }
        }

        public void BindSalaryDetails()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com1 = new SqlCommand("select * from SALARYDETAILS S, EMPLOYEE E where E.EmpId = S.EMPID ORDER BY S.EMPID", con);
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
            else
            {
                GridView2.DataSource = null;
                GridView2.DataBind();
            }
            con.Close();
        }

        public void BindSalaryReport()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            //string TYPE = "";

            //if (RBtnUseQuate.Checked == true)
            //{
            //    TYPE = "Hours";
            //}
            //else if (RBtnUseLight.Checked == true)
            //{
            //    TYPE = "LightWeight";
            //}
            //else if (RBtnUseHeavy.Checked == true)
            //{
            //    TYPE = "HeavyWeight";
            //}
            SqlCommand com1 = new SqlCommand("Select Distinct E.EmpId,E.FName,S.Hours,S.ContributionFees,S.TYPE,CONVERT(VARCHAR, CONVERT(DATETIME,S.EntryDate,106),103) AS EntryDate,S.TotalItemFees,S.STATUS,S.ProjectId, P.ProjectName from EMPLOYEE E, SALARYREPORT S, PROJECTS P where E.EmpId = S.EMPID AND S.ProjectId=P.ProjectId ", con);//AND S.TYPE='" + TYPE + "'
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridSalary.DataSource = dt;
                GridSalary.DataBind();
                divSalary.Visible = true;
            }
            else
            {
                GridSalary.DataSource = null;
                GridSalary.DataBind();
                divSalary.Visible = false;

                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No Salary Details');", true);
                //return;
            }
            DivHeader.Visible = false;
            DivSalaryInfo.Visible = false;
            com1.Parameters.Clear();
            dt.Rows.Clear();
            con.Close();
        }

        public void BindSalaryOnRadioButton()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            string TYPE = "";

            if (RBtnUseQuate.Checked == true)
            {
                TYPE = "Hours";
            }
            else if (RBtnUseLight.Checked == true)
            {
                TYPE = "LightWeight";
            }
            else if (RBtnUseHeavy.Checked == true)
            {
                TYPE = "HeavyWeight";
            }
            SqlCommand com1 = new SqlCommand("Select Distinct E.EmpId,E.FName,S.Hours,S.ContributionFees,S.TYPE, CONVERT(VARCHAR, CONVERT(DATETIME,S.EntryDate,106),103) AS EntryDate,S.TotalItemFees,S.ProjectId, S.STATUS, P.ProjectName from EMPLOYEE E, SALARYREPORT S, PROJECTS P where E.EmpId = S.EMPID AND S.ProjectId=P.ProjectId AND S.TYPE='" + TYPE + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridSalary.DataSource = dt;
                GridSalary.DataBind();
                divSalary.Visible = true;
                CheckVisibility();
            }
            else
            {
                GridSalary.DataSource = null;
                GridSalary.DataBind();
                divSalary.Visible = false;

                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No Salary Details');", true);
               // return;
            }
            DivHeader.Visible = false;
            DivSalaryInfo.Visible = false;
            com1.Parameters.Clear();
            dt.Rows.Clear();
            con.Close();
        }

        protected void BtnAddReports_Click(object sender, EventArgs e)
        {
            // Response.Redirect("AddReports.aspx");
        }

        //protected void DDReportMode_SelectedIndexChanged(object sender, EventArgs e) 
        //{
        //    if (DDReportMode.Text == "User Salary Module")
        //    {
        //        DivHeader.Visible = true;
        //        DivSalaryInfo.Visible = true;
        //        divSalary.Visible = false;

        //        RBtnUseQuate.Visible = false;
        //        RBtnUseLight.Visible = false;
        //        RBtnUseHeavy.Visible = false;
        //        RadioButtonAll.Visible = false;

        //        BindSalaryDetails();
        //    }
        //    else
        //        if (DDReportMode.Text == "Project Salary Module")
        //        {
        //            RBtnUseQuate.Visible = true;
        //            RBtnUseLight.Visible = true;
        //            RBtnUseHeavy.Visible = true;
        //            RadioButtonAll.Visible = true;
        //            BindSalaryReport();

        //        }
        //}

        protected void lnkUserSalary_Click(object sender, EventArgs e)
        {
            //GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //string Salaryid = grdrow.Cells[0].Text;
            //Response.Redirect("ProjectDetails.aspx?SalaryId=" + Salaryid);
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewSalary")
            {
                int Salaryid = Convert.ToInt32(e.CommandArgument);
                //Response.Redirect("SalaryDetails.aspx?SalaryId=" + Salaryid);


                string url = ("SalaryDetails.aspx?SalaryId=" + Salaryid);

                string s = "window.open('" + url + "');";

                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
        }

        protected void RBtnUseLight_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseQuate.Checked = false;
            RadioButtonAll.Checked = false;
            BindSalaryOnRadioButton();
        }

        protected void RBtnUseHeavy_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseLight.Checked = false;
            RBtnUseQuate.Checked = false;
            RadioButtonAll.Checked = false;
            BindSalaryOnRadioButton();
        }

        protected void RBtnUseQuate_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseLight.Checked = false;
            RadioButtonAll.Checked = false;
            BindSalaryOnRadioButton();
        }

        protected void btnAdminApp_Click(object sender, EventArgs e)
        {
            Response.Redirect("Approve.aspx");
        }

        protected void BtnAddSalary_Click(object sender, EventArgs e)
        {
            Response.Redirect("Salary Module.aspx");
        }

        protected void txtboxSearch_TextChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            string TYPE1 = "";

            if (RBtnUseQuate.Checked == true)
            {
                TYPE1 = "Hours";
            }
            else if (RBtnUseLight.Checked == true)
            {
                TYPE1 = "LightWeight";
            }
            else if (RBtnUseHeavy.Checked == true)
            {
                TYPE1 = "HeavyWeight";
            }

            if (TYPE1.ToString() != "")
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_SearchSalaryReport";

                cmd.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = TYPE1.ToString();
                cmd.Parameters.Add("@USERNAME", System.Data.SqlDbType.VarChar).Value = "%" + txtboxSearch.Text + "%";
            }
            else
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[sp_SearchALLSalaryReport]";

                cmd.Parameters.Add("@USERNAME", System.Data.SqlDbType.VarChar).Value = "%" + txtboxSearch.Text + "%";
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt1 = new DataSet();

            adpt.Fill(dt1);
            if (dt1.Tables[0].Rows.Count > 0)
            {
                GridSalary.DataSource = dt1;
                GridSalary.DataBind();
                divSalary.Visible = true;
            }
            else
            {
                //GridSalary.DataSource = null;
                //GridSalary.DataBind();
                //divSalary.Visible = false;

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No User Of Such Name');", true);
                return;
            }
            con.Close();
        }

        protected void RadioButtonAll_CheckedChanged(object sender, EventArgs e)
        {
            RBtnUseHeavy.Checked = false;
            RBtnUseLight.Checked = false;
            RBtnUseQuate.Checked = false;
            BindSalaryReport();
        }

        protected void GridSalary_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox ApproveCheckBox = (CheckBox)e.Row.FindControl("ApproveCheckBox");
                LinkButton lnkbtnView = (LinkButton)e.Row.FindControl("lnkbtnView");
                Label ddlStatus = (Label)e.Row.FindControl("LblStatus");

                if (UserType == "Admin")
                {
                    if (ddlStatus.Text == "Approve")
                    {
                        ApproveCheckBox.Visible = false;
                        lnkbtnView.Visible = false;
                    }
                    else
                    {
                        ApproveCheckBox.Visible = true;
                        lnkbtnView.Visible = true;
                    }

                }
            }
            else
            {

            }
        }

        public void CheckVisibility()
        {
            if (UserType == "Admin")
            {
                foreach (GridViewRow rows in GridSalary.Rows)
                {
                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.CheckBox ApproveCheckBox = (rows.Cells[6].FindControl("ApproveCheckBox") as System.Web.UI.WebControls.CheckBox);
                        System.Web.UI.WebControls.Label LblStatus = (rows.Cells[7].FindControl("LblStatus") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.LinkButton lnkbtnView = (rows.Cells[8].FindControl("lnkbtnView") as System.Web.UI.WebControls.LinkButton);


                        if (LblStatus.Text == "Approve")
                        {
                            ApproveCheckBox.Visible = false;
                            lnkbtnView.Visible = false;
                        }
                        else
                        {
                            ApproveCheckBox.Visible = true;
                            lnkbtnView.Visible = true;
                        }
                    }

                }
            }
            else
            {

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string type = "";

            foreach (GridViewRow rows in GridSalary.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblProjectId = (rows.Cells[0].FindControl("lblProjectId") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblEDate = (rows.Cells[1].FindControl("lblEDate") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblEmpid = (rows.Cells[2].FindControl("lblEmpid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblHours = (rows.Cells[4].FindControl("lblHours") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblTotalFees = (rows.Cells[5].FindControl("lblTotalFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblContributionFees = (rows.Cells[5].FindControl("lblContributionFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label LType = (rows.Cells[6].FindControl("LType") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.CheckBox ApproveCheckBox = (rows.Cells[7].FindControl("ApproveCheckBox") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label LblStatus = (rows.Cells[8].FindControl("LblStatus") as System.Web.UI.WebControls.Label);


                    string dtEntry1 = lblEDate.Text;
                    DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (ApproveCheckBox.Checked == true)
                    {

                        if (RBtnUseQuate.Checked == true)
                        {
                            type = "Hours";
                        }
                        else if (RBtnUseLight.Checked == true)
                        {
                            type = "LightWeight";
                        }
                        else if (RBtnUseHeavy.Checked == true)
                        {
                            type = "HeavyWeight";
                        }
                        else if (RadioButtonAll.Checked == true)
                        {
                            type = LType.Text;
                        }

                        SqlConnection con = new SqlConnection(cs);
                        con.Open();
                        SqlCommand cm1 = new SqlCommand();
                        cm1.Connection = con;
                        cm1.CommandType = System.Data.CommandType.StoredProcedure;
                        cm1.CommandText = "sp_UpdateSalaryReportsAdminNew";
                        cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                        cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                        cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = LType.Text;
                        cm1.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = LblStatus.Text;        //DDLStockItem.SelectedValue;
                        cm1.Parameters.Add("@STATUS1", System.Data.SqlDbType.VarChar).Value = "Approve";                     //ddlStatus.SelectedItem.Text;
                        cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = lblHours.Text;
                        cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = lblContributionFees.Text;        //DDLStockItem.SelectedValue;
                        cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = lblTotalFees.Text;
                        cm1.ExecuteNonQuery();
                        cm1.Parameters.Clear();

                        cm1 = new SqlCommand("SELECT SUM(Hours),SUM(ContributionFees),SUM(AHours),SUM(AContributionFees) from SALARYREPORT where EmpId=@I_EMPID  AND ProjectId=@I_PROJECTID and Type=@TYPE and Status=@STATUS1 ", con);
                        cm1.Parameters.AddWithValue("@I_EMPID", lblEmpid.Text);
                        cm1.Parameters.AddWithValue("@TYPE", LType.Text);
                        cm1.Parameters.AddWithValue("@I_PROJECTID", lblProjectId.Text);
                        cm1.Parameters.AddWithValue("@STATUS1", "Approve");
                        SqlDataAdapter adpt1 = new SqlDataAdapter(cm1);
                        DataSet dt1 = new DataSet();
                        adpt1.Fill(dt1);
                        double totalhours = 0;
                        double contributionfees = 0;
                        double totalAhours = 0;
                        double Acontributionfees = 0;
                        totalhours = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column1"].ToString());
                        contributionfees = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column2"].ToString());
                        totalAhours = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column3"].ToString());
                        Acontributionfees = Convert.ToDouble(dt1.Tables[0].Rows[0]["Column4"].ToString());

                        cm1.ExecuteNonQuery();
                        cm1.Parameters.Clear();
                        cm1 = new SqlCommand("DELETE from SALARYREPORT where EmpId=@I_EMPID  AND ProjectId=@I_PROJECTID and Type=@TYPE and Status=@STATUS1 ", con);
                        cm1.Parameters.AddWithValue("@I_EMPID", lblEmpid.Text);
                        cm1.Parameters.AddWithValue("@TYPE", LType.Text);
                        cm1.Parameters.AddWithValue("@I_PROJECTID", lblProjectId.Text);
                        cm1.Parameters.AddWithValue("@STATUS1", "Approve");

                        cm1.ExecuteNonQuery();
                        cm1.Parameters.Clear();

                        cm1.CommandType = System.Data.CommandType.StoredProcedure;
                        cm1.CommandText = "sp_UpdateSalaryReportsNew";
                        cm1.Parameters.Add("@I_PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProjectId.Text;
                        cm1.Parameters.Add("@I_EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpid.Text;
                        cm1.Parameters.Add("@I_TASKID", System.Data.SqlDbType.BigInt).Value = 0;
                        cm1.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = 0;
                        cm1.Parameters.Add("@TOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = lblTotalFees.Text;
                        cm1.Parameters.Add("@I_STID", System.Data.SqlDbType.BigInt).Value = 0;
                        cm1.Parameters.Add("@HOURS", System.Data.SqlDbType.Decimal).Value = totalhours;
                        cm1.Parameters.Add("@ENTRYDATE", System.Data.SqlDbType.DateTime).Value = dt11;
                        cm1.Parameters.Add("@CONTRIBUTION", System.Data.SqlDbType.Decimal).Value = contributionfees;
                        cm1.Parameters.Add("@TYPE", System.Data.SqlDbType.VarChar).Value = type;
                        cm1.Parameters.Add("@STATUS1", System.Data.SqlDbType.VarChar).Value = "Approve";
                        cm1.Parameters.Add("@AHOURS", System.Data.SqlDbType.Decimal).Value = totalAhours;
                        cm1.Parameters.Add("@ACONTRIBUTION", System.Data.SqlDbType.Decimal).Value = Acontributionfees;
                        cm1.Parameters.Add("@ATOTALITEMFEES", System.Data.SqlDbType.Decimal).Value = lblTotalFees.Text;

                        cm1.ExecuteNonQuery();
                        cm1.Parameters.Clear();
                        con.Close();

                        BindSalaryReport();
                        CheckVisibility();

                    }
                }

            }
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Salary Updated');", true);
            return;
        }

        protected void lnkbtnView_Click(object sender, EventArgs e)
        {
            LinkButton lnkbtnView = (LinkButton)sender;
            GridViewRow grdrRow = ((GridViewRow)lnkbtnView.Parent.Parent);
            Label lblProjectId = (Label)grdrRow.FindControl("lblProjectId");
            Label lblEDate = (Label)grdrRow.FindControl("lblEDate");
            Label LType = (Label)grdrRow.FindControl("LType");
            Label lblEmpid = (Label)grdrRow.FindControl("lblEmpid");

            Session["Projectid"] = lblProjectId.Text;
            Session["EntryDate"] = lblEDate.Text;
            Session["Type"] = LType.Text;
            Session["Empid"] = lblEmpid.Text;

            //DateTime dt11 = DateTime.ParseExact(dtEntry1, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            Response.Redirect("EditSalary.aspx");
        }

    }
}