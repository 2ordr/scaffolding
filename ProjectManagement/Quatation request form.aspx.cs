﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;
using System.Collections;
using System.Threading;


namespace ProjectManagement
{
    public partial class Quatation_request_form : System.Web.UI.Page
    {

        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
     
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand();
            DataTable dts = new DataTable();

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand();



            if (!IsPostBack)
            {
                //Label1.Text = Session["username"].ToString();

                resetall();
                DateTime orddate = DateTime.Now;

                txtResTime.Text = Convert.ToString(orddate);

                txtDate.Text = string.Format("{0:dd/MM/yyyy hh:mm:ss}", orddate);
                con.Open();

                cmd1.Connection = con;

                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "select * from Stocksitem";
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                GridView2.DataSource = dt;
                GridView2.DataBind();
                dt.Rows.Clear();


                Global.dtStock.Clear();
                dt.Rows.Clear();

                cmd1.CommandType = System.Data.CommandType.Text;
                cmd1.CommandText = "select ItemCode,itemname,Quantity,Price from Stocksitem order by Itemname";
                SqlDataAdapter adpt1 = new SqlDataAdapter(cmd1);

                adpt1.Fill(dts);

                foreach (DataRow rows in dts.Rows)
                {


                    Global.dtStock.Rows.Add(rows["Itemcode"].ToString(), rows["ItemName"].ToString(), rows["Quantity"].ToString(), rows["Price"].ToString());
                }

                ddlItemname1.DataSource = Global.dtStock;
                ddlItemname1.DataBind();
                ddlItemname2.DataSource = Global.dtStock;
                ddlItemname2.DataBind();
                ddlItemname3.DataSource = Global.dtStock;
                ddlItemname3.DataBind();
                ddlItemname4.DataSource = Global.dtStock;
                ddlItemname4.DataBind();
                ddlItemname5.DataSource = Global.dtStock;
                ddlItemname5.DataBind();
                ddlItemname6.DataSource = Global.dtStock;
                ddlItemname6.DataBind();
                ddlItemname7.DataSource = Global.dtStock;
                ddlItemname7.DataBind();
                ddlItemname8.DataSource = Global.dtStock;
                ddlItemname8.DataBind();
                ddlItemname9.DataSource = Global.dtStock;
                ddlItemname9.DataBind();
                ddlItemname10.DataSource = Global.dtStock;
                ddlItemname10.DataBind();
                ddlItemname11.DataSource = Global.dtStock;
                ddlItemname11.DataBind();
                ddlItemname12.DataSource = Global.dtStock;
                ddlItemname12.DataBind();
                ddlItemname13.DataSource = Global.dtStock;
                ddlItemname13.DataBind();
                ddlItemname14.DataSource = Global.dtStock;
                ddlItemname14.DataBind();
                ddlItemname15.DataSource = Global.dtStock;
                ddlItemname15.DataBind();
            }


        }
      
        protected void ddlItemName1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            DropDownList ddlCurrentDropDownList = (DropDownList)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)ddlCurrentDropDownList.Parent.Parent);

            Label lblprice = (Label)grdrDropDownRow.FindControl("lblPrice");
            Label lblquantity = (Label)grdrDropDownRow.FindControl("lblQuantity");
            if (lblprice != null || lblquantity != null)


                com.CommandText = "select * from Stocksitem where ItemCode= " + ddlCurrentDropDownList.SelectedValue;


            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);

            lblprice.Text = dt.Rows[0]["Price"].ToString();
            lblquantity.Text = dt.Rows[0]["Quantity"].ToString();
        }


        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            //txtDate.Text = Convert.ToDateTime(Calendar1.SelectedDate).ToString("MM/dd/yyyy"); 
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            //if (Calendar1.Visible == false)
            //{
            //    Calendar1.Visible = true;
            //}
            //else
            //{
            //    Calendar1.Visible = false;
            //}
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            DateTime orddate = DateTime.Today.Date;
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand();

            foreach (GridViewRow rows in GridView2.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.TextBox txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[6].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                    //System.Web.UI.WebControls.Label lblPrice = (rows.Cells[2].FindControl("lblPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblTot = (rows.Cells[5].FindControl("lblTot") as System.Web.UI.WebControls.Label);

                    if (txtQuantity.Text != "")
                    {


                        con.Open();
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_InsertHAS";
                        cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblName.Text;
                        cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                        cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                        cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = 0;
                        cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                        cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                        cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        con.Close();

                    }
                }

            }

            SqlConnection con1 = new SqlConnection(cs);

            con1.Open();
            com.Connection = con1;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_InsertRequestQuote";
            com.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
            com.Parameters.Add("@CUSTACK", System.Data.SqlDbType.VarChar).Value = ddlCustAck.SelectedValue;
            com.Parameters.Add("@ADMINAPPR", System.Data.SqlDbType.VarChar).Value = ddlAdminApp.SelectedValue;
            com.Parameters.Add("@EXPECTTIME", System.Data.SqlDbType.DateTime).Value = orddate;


            com.Parameters.Add("@RESPTIME", System.Data.SqlDbType.DateTime).Value = orddate;
            com.Parameters.Add("@TOTALCOST", System.Data.SqlDbType.NVarChar).Value = TxtResCost.Text;


            com.ExecuteNonQuery();

            if (ddquotestatus.SelectedValue == "Accepted")
            {
                Response.Redirect("Order Form.aspx?DATETIME=" + txtDate.Text);

            }
            com.Parameters.Clear();


            Thread.Sleep(2000);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<scripts>alert('!! Data submited !! ');</scripts>");

        }
        public void resetall()
        {

            txtDate.Text = "";
            ddlCustAck.Text = "0";
            ddlAdminApp.Text = "0";
            //TxtExTime.Text = "";
            //TxtReqQty.Text = "";
            //TxtReqCost.Text = "";
            txtResTime.Text = "";
            //TxtRespondQty.Text = "";
            TxtResCost.Text = "";
            txtName.Text = "";
            divStockdv1.Visible = false;
            divstk.Visible = false;
            headerdiv.Visible = false;



        }
        protected void CustAck_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ViewButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewQuatation.aspx?Printid=" + 1);
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("ondblclick", "__doPostBack('GridView1','Select$" + e.Row.RowIndex + "');");
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CustomerID = GridView1.SelectedDataKey.Value.ToString();
            string CustomerName = GridView1.SelectedRow.Cells[0].Text;
            lblName.Text = CustomerID;
            txtName.Text = CustomerName;
            divCustlist.Visible = false;
            div1.Visible = false;
        }

        protected void txtName_TextChanged(object sender, EventArgs e)
        {
            if (txtName.Text != "")
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();


                SqlCommand com = new SqlCommand("select * from CUSTOMER where CustName like @SearchStr ", con);

                com.Parameters.Add("@SearchStr", SqlDbType.NVarChar, 60).Value = "%" + txtName.Text + "%";

                SqlDataAdapter adpt = new SqlDataAdapter(com);
                DataTable dt = new DataTable();

                adpt.Fill(dt);
                GridView1.DataSource = dt;
                GridView1.DataBind();
                divCustlist.Visible = true;
                divStockdv1.Visible = false;
                divstk.Visible = true;
                headerdiv.Visible = true;
                div1.Visible = true;
                dt.Rows.Clear();
            }
        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            TxtResCost.Text = "";
            foreach (GridViewRow rows in GridView2.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lblTot = (rows.Cells[5].FindControl("lblTot") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblPrice = (rows.Cells[2].FindControl("lblPrice") as System.Web.UI.WebControls.Label);
                    if (txtQuantity.Text != "")
                    {

                        decimal Price = Convert.ToDecimal(lblPrice.Text);
                        long qty = Convert.ToInt64(txtQuantity.Text);
                        decimal total = 0;
                        decimal pricqty = 0;
                        pricqty = Price * qty;
                        total = total + pricqty;
                        lblTot.Text = total.ToString();
                        if (TxtResCost.Text != "")
                        {
                            TxtResCost.Text = (Convert.ToDecimal(TxtResCost.Text) + Convert.ToDecimal(lblTot.Text)).ToString();
                        }
                        else
                        {
                            TxtResCost.Text = lblTot.Text;
                        }

                    }

                }
            }
        }


        protected void btnAddmore_Click(object sender, EventArgs e)
        {
            divStockdv2.Visible = true;
        }

        protected void ddlItemname1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname1.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname1.SelectedValue == itemcode)
                    {
                        txtQuantity1.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice1.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice1_TextChanged(object sender, EventArgs e)
        {
            if (txtPrice1.Text != "0" || txtQQty1.Text != "0")
            {
                decimal Price = Convert.ToDecimal(txtPrice1.Text);
                long qty = Convert.ToInt64(txtQQty1.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                txtQPrice1.Text = pricqty.ToString();
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {

                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }

            }
        }
        protected void ddlItemname2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname2.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname2.SelectedValue == itemcode)
                    {
                        txtQuantity2.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice2.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice2_TextChanged(object sender, EventArgs e)
        {
            if (txtPrice2.Text != "" || txtQQty2.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtPrice2.Text);
                long qty = Convert.ToInt64(txtQQty2.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                txtQPrice2.Text = pricqty.ToString();

                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {

                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }

            }
        }
        protected void ddlItemname3_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname3.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname3.SelectedValue == itemcode)
                    {
                        txtQuantity3.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice3.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice3_TextChanged(object sender, EventArgs e)
        {
            if (txtPrice3.Text != "" || txtQQty3.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtPrice3.Text);
                long qty = Convert.ToInt64(txtQQty3.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                txtQPrice3.Text = pricqty.ToString();

                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname4_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname4.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname4.SelectedValue == itemcode)
                    {
                        txtQuantity4.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice4.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice4_TextChanged(object sender, EventArgs e)
        {
            if (txtPrice4.Text != "" || txtQQty4.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtPrice3.Text);
                long qty = Convert.ToInt64(txtQQty4.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                txtQPrice4.Text = pricqty.ToString();

                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }

        protected void ddlItemname5_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname5.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname5.SelectedValue == itemcode)
                    {
                        txtQuantity5.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice5.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice5_TextChanged(object sender, EventArgs e)
        {
            if (txtPrice5.Text != "" || txtQQty5.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtPrice5.Text);
                long qty = Convert.ToInt64(txtQQty5.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                txtPrice5.Text = pricqty.ToString();
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }


        protected void ddlItemname6_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname6.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname6.SelectedValue == itemcode)
                    {
                        txtQuantity6.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice6.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice6_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice6.Text != "" || txtQQty6.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice6.Text);
                long qty = Convert.ToInt64(txtQQty6.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname7_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname7.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname7.SelectedValue == itemcode)
                    {
                        txtQuantity7.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice7.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice7_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice7.Text != "" || txtQQty7.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice7.Text);
                long qty = Convert.ToInt64(txtQQty7.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname8_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname8.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname8.SelectedValue == itemcode)
                    {
                        txtQuantity8.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice8.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice8_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice8.Text != "" || txtQQty8.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice8.Text);
                long qty = Convert.ToInt64(txtQQty8.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname9_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname9.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname9.SelectedValue == itemcode)
                    {
                        txtQuantity9.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice9.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice9_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice9.Text != "" || txtQQty9.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice9.Text);
                long qty = Convert.ToInt64(txtQQty9.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname10_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname10.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname10.SelectedValue == itemcode)
                    {
                        txtQuantity10.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice10.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice10_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice10.Text != "" || txtQQty10.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice10.Text);
                long qty = Convert.ToInt64(txtQQty10.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname11_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname11.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname11.SelectedValue == itemcode)
                    {
                        txtQuantity11.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice11.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice11_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice11.Text != "" || txtQQty11.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice11.Text);
                long qty = Convert.ToInt64(txtQQty11.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname12_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname12.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname12.SelectedValue == itemcode)
                    {
                        txtQuantity12.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice12.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice12_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice12.Text != "" || txtQQty12.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice12.Text);
                long qty = Convert.ToInt64(txtQQty12.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname13_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname13.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname13.SelectedValue == itemcode)
                    {
                        txtQuantity13.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice13.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice13_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice13.Text != "" || txtQQty13.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice13.Text);
                long qty = Convert.ToInt64(txtQQty13.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname14_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname14.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname14.SelectedValue == itemcode)
                    {
                        txtQuantity14.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice14.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice14_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice14.Text != "" || txtQQty14.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice14.Text);
                long qty = Convert.ToInt64(txtQQty14.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }
        protected void ddlItemname15_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            if (ddlItemname15.SelectedIndex > -1)
            {
                for (i = 0; i < Global.dtStock.Rows.Count; i++)
                {
                    string itemcode = Global.dtStock.Rows[i]["Itemcode"].ToString();

                    if (ddlItemname15.SelectedValue == itemcode)
                    {
                        txtQuantity15.Text = Global.dtStock.Rows[i]["Quantity"].ToString();
                        txtPrice15.Text = Global.dtStock.Rows[i]["Price"].ToString();
                    }
                }

            }

        }
        protected void txtQPrice15_TextChanged(object sender, EventArgs e)
        {
            if (txtQPrice15.Text != "" || txtQQty15.Text != "")
            {
                decimal Price = Convert.ToDecimal(txtQPrice15.Text);
                long qty = Convert.ToInt64(txtQQty15.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                if (TxtResCost.Text == "")
                {
                    TxtResCost.Text = (total + pricqty).ToString();
                }
                else
                {
                    total = Convert.ToDecimal(TxtResCost.Text);
                    TxtResCost.Text = (total + pricqty).ToString();
                }
            }
        }

        protected void GridView2_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand("select * from Stocksitem", con);


            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);


            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList ddlDropDownList = (DropDownList)e.Row.FindControl("ddlItemName1");

                if (ddlDropDownList != null)
                {

                    ddlDropDownList.DataSource = dt;
                    ddlDropDownList.DataTextField = "ItemName";
                    ddlDropDownList.DataValueField = "ItemCode";
                    ddlDropDownList.DataBind();


                }
                ddlDropDownList.Items.Insert(0, new ListItem("-Select Items-", "0"));
            }
        }

        //protected void TextBox1_TextChanged(object sender, EventArgs e)
        //{

        //    SqlConnection con = new SqlConnection(cs);
        //    //SqlCommand com1 = new SqlCommand();


        //    if (TextBox1.Text != "")
        //    {


        //        con.Open();


        //        SqlCommand com1 = new SqlCommand("SearchAllTables", con);
        //        com1.CommandType = CommandType.StoredProcedure;
        //        //com.Parameters.Add("@SearchStr", SqlDbType.NVarChar, 60).Value = "%" + txtName.Text + "%";
        //        //com.CommandType = System.Data.CommandType.StoredProcedure;
        //        //com.CommandText = "SearchAllTables";
        //        com1.Parameters.Add("@SearchStr", System.Data.SqlDbType.NVarChar).Value = "%" + TextBox1.Text + "%";

        //        SqlDataAdapter adpt = new SqlDataAdapter(com1);
        //        DataTable dt = new DataTable();
        //        adpt.Fill(dt);

        //        GridViewSearch.DataSource = dt;
        //        GridViewSearch.DataBind();


        //    }




        //}
    }

}