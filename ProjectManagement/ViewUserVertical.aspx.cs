﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
//using System.Text;

namespace ProjectManagement
{
    public partial class ViewUserVertical : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label1.Text = Session["username"].ToString();
                BindGridview();
            }
        }

        public void BindGridview()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();


            SqlCommand com = new SqlCommand("select * from USERS ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataTable dt = new DataTable();

            adpt.Fill(dt);

            GridViewUser.DataSource = dt;

            GridViewUser.DataBind();
            con.Close();
        }

        protected void BtnAddUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateUser.aspx");
        }

        protected void chkSelect_CheckedChanged(object sender, System.EventArgs e)
        {
            //ArrayList selectedValues = new ArrayList();
            foreach (GridViewRow item in GridViewUser.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chk = (CheckBox)(item.Cells[0].FindControl("chkSelect"));
                    if (chk.Checked)
                    {
                        Label lblUserName = ((Label)item.Cells[0].FindControl("lblUserName"));
                        Label lblUSERID = ((Label)item.Cells[1].FindControl("lblUSERID"));

                        LblUserName.Text = lblUserName.Text;
                        LblUserId.Text = lblUSERID.Text;

                        //Response.Redirect("customer overview.aspx?CustId=" + CustId.Text);

                    }

                }

            }
        }

        protected void txtSearchBox_TextChanged(object sender, System.EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            String sql = "Select * from USERS where Name Like @NAME";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {

                cmd.Parameters.Add("@NAME", System.Data.SqlDbType.VarChar, 100).Value = "%" + txtSearchBox.Text + "%";

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                DataSet dt1 = new DataSet();

                adpt.Fill(dt1);

                GridViewUser.DataSource = dt1;

                BindGridview();
                con.Close();
            }
        }

        protected void LBtnOverview_Click(object sender, System.EventArgs e)
        {
            divOverView.Visible = true;
            divBasicInfo.Visible = false;
        }

        protected void LBtnBasicInfo_Click(object sender, System.EventArgs e)
        {
            divOverView.Visible = false;
            divBasicInfo.Visible = true;


            if (LblUserName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select Proper User Name');", true);
                return;
            }

            else
            {

                SqlConnection con = new SqlConnection(cs);
                con.Open();


                SqlCommand com1 = new SqlCommand("select * from USERS where USERID =" + LblUserId.Text);

                com1.Connection = con;

                SqlDataAdapter adpt1 = new SqlDataAdapter(com1);

                DataSet dt2 = new DataSet();

                adpt1.Fill(dt2);

                GridViewBasicInfo.DataSource = dt2;

                GridViewBasicInfo.DataBind();

                txtName.Text = dt2.Tables[0].Rows[0]["Name"].ToString();
                txtUName.Text = dt2.Tables[0].Rows[0]["UserName"].ToString();
                txtPassword.Text = dt2.Tables[0].Rows[0]["Password"].ToString();
                txtMobile.Text = dt2.Tables[0].Rows[0]["Mobile"].ToString();
                txtEmail.Text = dt2.Tables[0].Rows[0]["EmailId"].ToString();
                txtWage.Text = dt2.Tables[0].Rows[0]["WagePerHr"].ToString();
                txtUserType.Text = dt2.Tables[0].Rows[0]["UserType"].ToString();

            }
        }

        protected void BtnUpdate_Click(object sender, System.EventArgs e)
        {
           
            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();
            com.Connection = con1;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_UpdateUserDetails";
            com.Parameters.Add("@USERID", System.Data.SqlDbType.BigInt).Value = LblUserId.Text;
            com.Parameters.Add("@NAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;
            com.Parameters.Add("@USERNAME", System.Data.SqlDbType.VarChar).Value = txtUName.Text;
            com.Parameters.Add("@PASSWORD", System.Data.SqlDbType.VarChar).Value = txtPassword.Text;
            com.Parameters.Add("@MOBILE", System.Data.SqlDbType.VarChar).Value = txtMobile.Text;
            com.Parameters.Add("@EMAILID", System.Data.SqlDbType.VarChar).Value = txtEmail.Text;
            com.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = txtWage.Text;
            com.Parameters.Add("@USERTYPE", System.Data.SqlDbType.VarChar).Value = txtUserType.Text;
            com.ExecuteNonQuery();
            com.Parameters.Clear();
            con1.Close();

            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('User Data Updated');", true);
        }

        protected void BtndeleteCustomer_Click(object sender, System.EventArgs e)
        {

            foreach (GridViewRow gvrow in GridViewUser.Rows)
            {

                CheckBox chkdelete = (CheckBox)gvrow.FindControl("chkSelect");

                if (chkdelete.Checked)
                {
                    int UserID = Convert.ToInt32(GridViewUser.DataKeys[gvrow.RowIndex].Value);

                    using (SqlConnection con = new SqlConnection(cs))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM USERS where USERID=" + UserID, con);
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            BindGridview();
            resetall();

        }
        public void resetall()
        {

            txtName.Text = "";
            txtUName.Text = "";
            txtPassword.Text = "";
            txtMobile.Text = "";
            txtEmail.Text = "";
            txtWage.Text = "";
            txtUserType.Text = "";
            LblUserId.Text = "";
            LblUserName.Text = "";
        }

    }
}