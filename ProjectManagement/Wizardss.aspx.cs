﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Threading;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.html;
using iTextSharp.tool.xml;

namespace ProjectManagement
{
    public partial class Wizardss : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        // int total1 = 0;
        decimal FinalEarn = 0;
        Boolean Flag = false;
        string QuoteDate = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Wizard1.PreRender += new EventHandler(Wizard1_PreRender);
            imguser.ImageUrl = "~/image/pmglogo1.jpg";
            imguser1.ImageUrl = "~/image/pmglogo1.jpg";
            imguser2.ImageUrl = "~/image/pmglogo1.jpg";

            if (!Page.IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    Global.FlagVis = false;
                    BtnSaveASTemplate.Visible = true;
                    FileUploadDiv11.Visible = false;
                    lblDateTime.Text = null;

                    txtSearchBox.Attributes["onclick"] = "clearTextBox(this.id)";
                    DateTime orddate = DateTime.Now;

                    txtCompareDate.Text = string.Format("{0:dd/MM/yyyy }", orddate);

                    txtDate.Text = string.Format("{0:dd/MM/yyyy hh:mm:ss}", orddate);
                    txtResTime.Text = string.Format("{0:dd/MM/yyyy }", orddate);
                    //date2.Text = txtDate.Text;

                    SqlConnection conD = new SqlConnection(cs);
                    SqlCommand cmdD = new SqlCommand();
                    conD.Open();
                    cmdD.Connection = conD;
                    cmdD.CommandText = "Delete From TemplateData";
                    cmdD.ExecuteNonQuery();
                    cmdD.Parameters.Clear();

                    cmdD.CommandText = "Delete From HAS where QuoteEarn='" + null + "'";
                    cmdD.ExecuteNonQuery();
                    cmdD.Parameters.Clear();
                    conD.Close();


                    if (string.IsNullOrEmpty(Session["quatdate"] as string))
                    {

                        SqlConnection con = new SqlConnection(cs);
                        SqlCommand cmd = new SqlCommand();
                        con.Open();
                        cmd.Connection = con;

                        BtnStopttime.Visible = false;
                        //fieldsetcustomer.Visible = false;
                        //fieldsetDetails.Visible = false;
                        Label1.Text = Session["username"].ToString();

                        //cmd.CommandType = System.Data.CommandType.Text;
                        //cmd.CommandText = "select * from Stocksitem";
                        //SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        //DataTable dt = new DataTable();
                        //adpt.Fill(dt);
                        //if (dt.Rows.Count > 0)
                        //{
                        //    GridView2.DataSource = dt;
                        //    GridView2.DataBind();
                        //    dt.Rows.Clear();
                        //}
                        cmd.Parameters.Clear();

                        BindPreDifinedText();//----------------Not In USe-------------------------
                        BindListItem();
                        BindData();
                        BindUserList();
                        GETQuoteID();
                        BindTemplateDropDown();
                        datetime1.Text = string.Format("{0:dd/MM/yyyy hh:mm:ss}", orddate);

                        //SqlConnection conn = new SqlConnection(cs);
                        //conn.Open();
                        //SqlCommand cmdd = new SqlCommand();
                        //cmdd.Connection = conn;
                        //cmd.CommandType = System.Data.CommandType.Text;
                        //cmd.CommandText = "delete  from TemplateData";
                        //cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "delete  from TERMSAndCONDITION";
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    else
                    {
                        // Wizard1.ActiveStepIndex = Wizard1.WizardSteps.IndexOf(w1);
                        Global.FlagVis = true;

                        BindUserList();
                        BindPreDifinedText();//----------------Not In USe-------------------------
                        BindListItem();
                        BindGridView();
                        BindTemplateDropDown();
                        Session["quatdate"] = null;
                    }
                }
            }
            this.RegisterPostBackControl();


            //if (IsPostBack && FileUpload01.PostedFile != null)
            //{
            //    if (FileUpload01.PostedFile.FileName.Length > 0)
            //    {
            //        String ext = System.IO.Path.GetExtension(FileUpload01.FileName);

            //        if (ext != ".jpg" && ext != ".png" && ext != ".gif" && ext != ".jpeg" && ext != ".jpe" && ext != ".jfif" && ext != ".gif" && ext != ".bmp" && ext != ".tit" && ext != ".jpeg2000" && ext != ".exif" && ext != ".gif" && ext != ".ppm" && ext != ".pgm" && ext != ".pbm" && ext != ".pnm" && ext != ".bpg" && ext != ".psd" && ext != ".tif" && ext != ".pspimage" && ext != ".yuv" && ext != ".thm" && ext != ".ai" && ext != ".drw" && ext != ".eps" && ext != ".ps" && ext != ".svg")
            //        {
            //            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Only Images Allowed.');", true);
            //            return;
            //        }
            //        else
            //        {
            //            string filename = Path.GetFileName(FileUpload01.PostedFile.FileName);

            //            int length = FileUpload01.PostedFile.ContentLength;

            //            byte[] imgbyte = new byte[length];

            //            HttpPostedFile img = FileUpload01.PostedFile;

            //            img.InputStream.Read(imgbyte, 0, length);

            //            DeleteImageData();

            //            FileUpload01.SaveAs(Server.MapPath("~/image/") + Path.GetFileName(FileUpload01.FileName));

            //            imguser.ImageUrl = "~/image/" + Path.GetFileName(FileUpload01.FileName);

            //            using (SqlConnection con = new SqlConnection(cs))
            //            {
            //                using (SqlCommand cmd = new SqlCommand())
            //                {
            //                    cmd.CommandText = "insert into QUOTEImageDetails (QUOTEID,ImageName,ImageData) values(@QUOTEID,@Name,@Data)";

            //                    cmd.Parameters.AddWithValue("@QUOTEID", LBLQUoteIdForImage.Text);

            //                    cmd.Parameters.AddWithValue("@Name", filename);

            //                    cmd.Parameters.AddWithValue("@Data", System.Data.SqlDbType.VarBinary).Value = imgbyte;


            //                    cmd.Connection = con;

            //                    con.Open();

            //                    cmd.ExecuteNonQuery();

            //                    con.Close();
            //                }
            //            }
            //            imguser.ImageUrl = "~/QupteImageHandler.ashx?QuoteID=" + LBLQUoteIdForImage.Text;
            //            imguser1.ImageUrl = "~/QupteImageHandler.ashx?QuoteID=" + LBLQUoteIdForImage.Text;
            //        }
            //    }
            //}
        }

        private void DeleteImageData()//--Not Use Now-----//
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Delete from QUOTEImageDetails Where QUOTEID= '" + LBLQUoteIdForImage.Text + "'";
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            con.Close();
        }

        public void BindPreDifinedText()//----------------Not In USe-------------------------
        {
            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();
            SqlCommand cmd1 = new SqlCommand();
            cmd1.Connection = con1;
            cmd1.CommandType = System.Data.CommandType.Text;
            cmd1.CommandText = "select * from [Predifined-Text]";
            SqlDataAdapter adpt1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            adpt1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                GridVConDition.DataSource = dt1;
                GridVConDition.DataBind();
            }
            else
            {
                GridVConDition.DataSource = dt1;
                GridVConDition.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any Predifined Text !! ');", true);
            }
            con1.Close();

        }

        public void BindListItem()
        {
            SqlConnection con2 = new SqlConnection(cs);
            con2.Open();
            SqlCommand cmd2 = new SqlCommand("select * from [Predifined-Text]", con2);
            SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            adpt2.Fill(dt2);
            if (dt2.Rows.Count > 0)
            {
                ddlistCondition.DataSource = dt2;
                ddlistCondition.DataTextField = "KortNavn";
                ddlistCondition.DataValueField = "StantekstID";
                ddlistCondition.DataBind();
                ddlistCondition.Items.Insert(0, ("-Vælg-"));

                ddlistCondition1.DataSource = dt2;
                ddlistCondition1.DataTextField = "KortNavn";
                ddlistCondition1.DataValueField = "StantekstID";
                ddlistCondition1.DataBind();
                ddlistCondition1.Items.Insert(0, ("-Vælg-"));

                cmd2.Parameters.Clear();
                dt2.Rows.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any Predifined Text !! ');", true);
            }

            cmd2.CommandText = "select * from Stocksitem where UseQuate='True'";
            adpt2.SelectCommand = cmd2;
            adpt2.Fill(dt2);
            if (dt2.Rows.Count > 0)
            {
                ddlItemName1.DataSource = dt2;
                ddlItemName1.DataTextField = "ItemName";
                ddlItemName1.DataValueField = "ItemCode";
                ddlItemName1.DataBind();
                ddlItemName1.Items.Insert(0, ("-Vælg-"));

                GridView2.DataSource = dt2;             //------Not Used
                GridView2.DataBind();                   //------Not Used

                cmd2.Parameters.Clear();
                dt2.Rows.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Empty Stocksitem  !! ');", true);
            }

            cmd2.CommandText = "select * from Stocksitem where Type='Service'";
            adpt2.SelectCommand = cmd2;
            adpt2.Fill(dt2);
            if (dt2.Rows.Count > 0)
            {
                DropDownListServiceItem.DataSource = dt2;
                DropDownListServiceItem.DataTextField = "ItemName";
                DropDownListServiceItem.DataValueField = "ItemCode";
                DropDownListServiceItem.DataBind();
                DropDownListServiceItem.Items.Insert(0, ("-Vælg-"));

                cmd2.Parameters.Clear();
                dt2.Rows.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Empty Stocksitem  !! ');", true);
            }

            con2.Close();
        }

        protected void Wizard1_PreRender(object sender, EventArgs e)
        {
            Repeater SideBarList = Wizard1.FindControl("HeaderContainer").FindControl("SideBarList") as Repeater;
            SideBarList.DataSource = Wizard1.WizardSteps;
            SideBarList.DataBind();
        }

        protected string GetClassForWizardStep(object wizardStep)
        {
            WizardStep step = wizardStep as WizardStep;

            if (step == null)
            {
                return "";
            }
            int stepIndex = Wizard1.WizardSteps.IndexOf(step);

            if (stepIndex < Wizard1.ActiveStepIndex)
            {
                return "prevStep";
            }
            else if (stepIndex > Wizard1.ActiveStepIndex)
            {
                return "nextStep";
            }
            else
            {
                return "currentStep";
            }
        }

        private void BindData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getQuatation";
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                HeaderDiv11.Visible = true;
                gridviewquatationView.DataSource = dt;
                gridviewquatationView.DataBind();
            }
            else
            {
                HeaderDiv11.Visible = false;
                gridviewquatationView.DataSource = null;
                gridviewquatationView.DataBind();
                // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any  Quatation  !! ');", true);
                return;
            }
            con.Close();
        }

        protected void GridView3_SelectedIndexChanged(object sender, EventArgs e)//----------------Not In USe-------------------------
        {

        }

        protected void LikButEdit_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[2].Text;
            Response.Redirect("EditQuatation.aspx?Quatdate=" + quatdate);
        }

        protected void Lnkdeleterow_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[2].Text;

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();

            SqlCommand com1 = new SqlCommand("delete FROM REQUESTEDQUOTE where DateTime = @DATETIME;", con1);  //delete FROM HAS where DateTime = @DATETIME;

            //SqlCommand com1 = new SqlCommand();
            //com1.CommandText = "sp_deleteQuatation";
            //com1.CommandType = System.Data.CommandType.StoredProcedure;
            //com1.Connection = con1; 
            //com1.CommandType = CommandType.Text;
            com1.Parameters.Add("@DATETIME", SqlDbType.NVarChar).Value = quatdate;


            //SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
            //DataTable dt1 = new DataTable();
            //adpt1.Fill(dt1);
            //GridView2.DataSource = dt1;

            com1.ExecuteNonQuery();
            con1.Close();
            BindData();
            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Quatation Deleted Successfully!!');", true);
            return;
        }

        protected void txtName_TextChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            if (txtName.Text == "")
            {

            }
            if (txtName.Text != "")
            {
                SqlCommand com = new SqlCommand("select * from CUSTOMER where CustName like @SearchStr ", con);

                com.Parameters.Add("@SearchStr", SqlDbType.NVarChar, 60).Value = "%" + txtName.Text + "%";

                SqlDataAdapter adpt = new SqlDataAdapter(com);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    // GridViewCustDetails.DataSource = dt;
                    //GridViewCustDetails.DataBind();

                    divCustlist.Visible = true;
                    //fieldsetcustomer.Visible = true;
                    divstk.Visible = false;
                    headerdiv.Visible = true;
                    //div1.Visible = true;
                    ItemInfoFieldSet.Visible = true;
                    dt.Rows.Clear();
                }
            }

            con.Close();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[2].Text;
            //Response.Redirect("QuatationPrint.aspx?Quatdate=" + quatdate);

            string url = ("QuatationPrint.aspx?Quatdate=" + quatdate);

            string s = "window.open('" + url + "');";

            // ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
            return;
        }

        protected void GridViewCustDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("ondblclick", "__doPostBack('GridViewCustDetails','Select$" + e.Row.RowIndex + "');");
            }
        }

        //protected void GridViewCustDetails_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    string CustomerID = GridViewCustDetails.SelectedDataKey.Value.ToString();
        //    string CustomerName = GridViewCustDetails.SelectedRow.Cells[0].Text;
        //    lblCustId.Text = CustomerID;
        //    txtName.Text = CustomerName;
        //    System.Web.UI.WebControls.Label lblemaill = (GridViewCustDetails.SelectedRow.Cells[0].FindControl("lblemaill") as System.Web.UI.WebControls.Label);
        //    System.Web.UI.WebControls.Label lblAddressLine1 = (GridViewCustDetails.SelectedRow.Cells[0].FindControl("lblAddressLine1") as System.Web.UI.WebControls.Label);
        //    System.Web.UI.WebControls.Label lblCity = (GridViewCustDetails.SelectedRow.Cells[0].FindControl("lblCity") as System.Web.UI.WebControls.Label);
        //    divCustlist.Visible = false;
        //    div1.Visible = false;
        //    //fieldsetcustomer.Visible = false;
        //    fieldsetDetails.Visible = true;
        //    LblCustName.Text = CustomerName;
        //    LblCustAddress.Text = lblAddressLine1.Text;
        //    LblCity.Text = lblCity.Text;
        //    LblEmail.Text = lblemaill.Text;

        //}

        protected void ddlItemName1_SelectedIndexChanged1(object sender, EventArgs e) //----------------Not In USe-------------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            DropDownList ddlCurrentDropDownList = (DropDownList)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)ddlCurrentDropDownList.Parent.Parent);

            Label lblprice = (Label)grdrDropDownRow.FindControl("lblPrice");
            Label lblquantity = (Label)grdrDropDownRow.FindControl("lblQuantity");

            if (lblprice != null || lblquantity != null)

                com.CommandText = "select * from Stocksitem where ItemCode= " + ddlCurrentDropDownList.SelectedValue;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                lblprice.Text = dt.Rows[0]["Price"].ToString();
                lblquantity.Text = dt.Rows[0]["Quantity"].ToString();
            }
            con.Close();
        }

        protected void GridView2_RowDataBound1(object sender, GridViewRowEventArgs e) //----------------Not In USe-------------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand("select * from Stocksitem where UseQuate='True'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    DropDownList ddlDropDownList = (DropDownList)e.Row.FindControl("ddlItemName1");
                    Label lblTot = (Label)e.Row.FindControl("lblTot");

                    if (ddlDropDownList != null)
                    {

                        ddlDropDownList.DataSource = dt;
                        ddlDropDownList.DataTextField = "ItemName";
                        ddlDropDownList.DataValueField = "ItemCode";
                        ddlDropDownList.DataBind();
                    }
                    ScriptManager.GetCurrent(this.Page).RegisterPostBackControl((TextBox)e.Row.FindControl("txtQuantity"));
                    //ddlDropDownList.Items.Insert(0, new ListItem("-Select Items-", "0"));
                    if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        System.Web.UI.WebControls.TextBox txttot = (TextBox)e.Row.FindControl("txttot");
                        if (txttot.Text != "")
                        {
                            //TxtResCost.Text = (Convert.ToDecimal(TxtResCost.Text) + Convert.ToDecimal(lblTot.Text)).ToString();
                            txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                        }
                        else
                        {
                            //TxtResCost.Text = lblTot.Text;
                            txttot.Text = lblTot.Text;
                        }
                    }
                }
            }
            con.Close();
        }

        private void RegisterPostBackControl()
        {
            foreach (GridViewRow row in GridView2.Rows)
            {
                //22.09.15
                //Button lnkFull = row.FindControl("btnCopy") as Button;
                TextBox lnkFull = row.FindControl("txtQuantity") as TextBox;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkFull);
            }
        }

        protected void GetTotal() //----------------Not In USe-------------------------
        {
            int count = 1;
            decimal total = 0;
            foreach (GridViewRow rows in GridView2.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {

                    System.Web.UI.WebControls.Label lblTot = (rows.Cells[5].FindControl("lblTot") as System.Web.UI.WebControls.Label);
                    count += 1;
                    total = GridView2.Rows.Count;

                    //total = Convert.ToInt64(lblTot.Text);

                    if (rows.RowType == DataControlRowType.Footer)
                    {

                        System.Web.UI.WebControls.TextBox txttot = (rows.Cells[4].FindControl("txttot") as System.Web.UI.WebControls.TextBox);

                        if (txttot.Text != "")
                        {
                            //TxtResCost.Text = (Convert.ToDecimal(TxtResCost.Text) + Convert.ToDecimal(lblTot.Text)).ToString();
                            txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                        }
                        else
                        {
                            //TxtResCost.Text = lblTot.Text;
                            txttot.Text = lblTot.Text;
                        }

                    }
                }
            }
            GridView2.FooterRow.Cells[4].Text = "Total = " + total;

        }

        protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {

            //if (LBLResCost.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Total Cost Must FulFill');", true);

            //    if (IsValid)
            //    {
            //        e.Cancel = true;
            //    }
            //    return;
            //}

        }

        protected void Wizard1_FinishButtonClick1(object sender, WizardNavigationEventArgs e) //----------------Not In USe-------------------------
        {
            if (txtDate.Text != "")
            {
                DateTime orddate = DateTime.Today.Date;
                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand();

                foreach (GridViewRow rows in GridView2.Rows)
                {

                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                        System.Web.UI.WebControls.TextBox txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.TextBox);
                        System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[6].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                        //System.Web.UI.WebControls.Label lblPrice = (rows.Cells[2].FindControl("lblPrice") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblTot = (rows.Cells[5].FindControl("lblTot") as System.Web.UI.WebControls.Label);

                        if (txtQuantity.Text != "")
                        {
                            if (datetime1.Text == txtDate.Text)
                            {
                                con.Open();
                                cmd.Connection = con;
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.CommandText = "sp_InsertHAS";
                                cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                                cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                                cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                                cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = 0;
                                cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                                cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                                cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                                cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;

                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                con.Close();
                            }
                            else
                            {
                                con.Open();
                                cmd.Connection = con;
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.CommandText = "sp_UpdateHAS";
                                cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                                cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                                cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                                cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = lbllDatetime.Text;
                                cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                                cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                                cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                                cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;

                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                con.Close();
                            }
                        }
                    }
                }

                SqlConnection con1 = new SqlConnection(cs);
                SqlCommand com = new SqlCommand();
                con1.Open();
                com.Connection = con1;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertRequestQuote";
                com.Parameters.Add("@DATETIME", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                com.Parameters.Add("@CUSTACK", System.Data.SqlDbType.VarChar).Value = ddlCustAck.SelectedValue;
                com.Parameters.Add("@ADMINAPPR", System.Data.SqlDbType.VarChar).Value = ddlAdminApp.SelectedValue;
                com.Parameters.Add("@EXPECTTIME", System.Data.SqlDbType.DateTime).Value = txtResTime.Text; ;
                com.Parameters.Add("@RESPTIME", System.Data.SqlDbType.DateTime).Value = txtResTime.Text;
                com.Parameters.Add("@TOTALCOST", System.Data.SqlDbType.Decimal).Value = LBLResCost.Text;
                com.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = LblTotalEarning.Text;


                com.ExecuteNonQuery();
                com.Parameters.Clear();
                con1.Close();
                Thread.Sleep(222000);
                if (txtDate.Text == String.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Data Not Submited');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Data submited !! ');", true);

                    Response.Redirect("Order Form.aspx?DATETIME=" + txtDate.Text);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Data Not Submited');", true);
                return;
            }
        }

        protected void BtnStarttime_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            BtnStarttime.Visible = false;
            BtnStopttime.Visible = true;
            Label5.Text = lbldisplayTime.Text;
        }

        protected void BtnStopttime_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            BtnStarttime.Visible = true;
            BtnStopttime.Visible = false;
            Label6.Text = lbldisplayTime.Text;

            DateTime inTime = Convert.ToDateTime(Label5.Text);
            DateTime outTime = Convert.ToDateTime(Label6.Text);
            TimeSpan i = outTime.Subtract(inTime);
            this.txtHrs.Text = i.ToString();
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            lbldisplayTime.Text = DateTime.Now.ToString();
        }

        protected void txtQuantity_TextChanged1(object sender, EventArgs e)//----------------Not In USe---------------------------------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            //int count = 1;
            //decimal totalamt = 0;
            //TxtResCost.Text = "";
            foreach (GridViewRow rows in GridView2.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lblTot = (rows.Cells[5].FindControl("lblTot") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtQuantity = (rows.Cells[4].FindControl("txtQuantity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblPrice = (rows.Cells[2].FindControl("lblPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[6].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.DropDownList ddlItemName1 = (rows.Cells[1].FindControl("ddlItemName1") as System.Web.UI.WebControls.DropDownList);
                    //System.Web.UI.WebControls.TextBox txttot = (rows.Cells[6].FindControl("txttot") as System.Web.UI.WebControls.TextBox);

                    if (txtQuantity.Text != "")
                    {

                        decimal Price = Convert.ToDecimal(lblPrice.Text);
                        long qty = Convert.ToInt64(txtQuantity.Text);
                        decimal total = 0;
                        decimal pricqty = 0;
                        pricqty = Price * qty;
                        total = total + pricqty;
                        lblTot.Text = total.ToString();

                        //if (ViewState["TotalPrice"] == null)
                        //{
                        //    ViewState["TotalPrice"] = lblTot.Text;
                        //}
                        //if (rows.RowType == DataControlRowType.Footer)
                        //{
                        //    System.Web.UI.WebControls.TextBox txttot = (rows.Cells[4].FindControl("txttot") as System.Web.UI.WebControls.TextBox);
                        if (TxtResCost.Text != "")
                        {
                            TxtResCost.Text = (Convert.ToDecimal(TxtResCost.Text) + Convert.ToDecimal(lblTot.Text)).ToString();
                            //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                        }
                        else
                        {
                            TxtResCost.Text = lblTot.Text;
                            //txttot.Text = lblTot.Text;
                        }


                        //if (lblTotalCost1.Text != "")
                        //{
                        //    //TxtResCost.Text = (Convert.ToDecimal(TxtResCost.Text) + Convert.ToDecimal(lblTot.Text)).ToString();
                        //    totalamt = Convert.ToDecimal(lblTot.Text);
                        //    lblTotalCost1.Text = (Convert.ToDecimal(lblTotalCost1.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                        //}
                        //else
                        //{
                        //    //TxtResCost.Text = lblTot.Text;
                        //    totalamt = Convert.ToDecimal(lblTot.Text);
                        //    lblTotalCost1.Text = totalamt.ToString();
                        //    count += 1;

                        //}

                        SqlCommand com = new SqlCommand();
                        com.Connection = con;
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertTemplateData";
                        com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                        com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = ddlItemName1.SelectedItem.Text;
                        com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = lblPrice.Text;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                        com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblTot.Text;

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();

                        //GetTotal();
                    }
                    SqlCommand com1 = new SqlCommand();
                    com1.Connection = con;
                    com1.CommandType = System.Data.CommandType.Text;
                    com1.CommandText = "select distinct * from TemplateData";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
                    DataTable dt1 = new DataTable();
                    adpt1.Fill(dt1);
                    if (dt1.Rows.Count > 0)
                    {
                        GrdViewPrint.Visible = true;
                        GrdViewPrint.DataSource = dt1;
                        GrdViewPrint.DataBind();
                    }

                }
            }

            // GridView2.FooterRow.Cells[1].Text = "Total = " + totalamt;
            //if (lblTotalCost1.Text != "")
            //{
            //    //TxtResCost.Text = (Convert.ToDecimal(TxtResCost.Text) + Convert.ToDecimal(lblTot.Text)).ToString();
            //    lblTotalCost1.Text = (Convert.ToDecimal(lblTotalCost1.Text) + Convert.ToDecimal(totalamt)).ToString();

            //}
            //else
            //{
            //    //TxtResCost.Text = lblTot.Text;
            //    lblTotalCost1.Text = totalamt.ToString();
            //}
            //lblTotalCost1.Text = totalamt.ToString();
            //TxtResCost.Text = totalamt.ToString();
            con.Close();
        }

        protected void StepNextButton_Click(object sender, EventArgs e)
        {

            if (lblCustId.Text == "")
            {
                Wizard1.ActiveStepIndex = Wizard1.WizardSteps.IndexOf(w1);

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert(' For Selecting Proper Name ');", true);
                return;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridviewquatationView.PageIndex = e.NewPageIndex;
            this.BindData();
        }

        protected void btnExtoPdf_Click(object sender, EventArgs e)//----Not Use Now-----//
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select ImageName from QUOTEImageDetails where QUOTEID ='" + LBLQUoteIdForImage.Text + "'";
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            string imagename = dt.Rows[0][0].ToString();
            com.Parameters.Clear();

            GrdViewPrint.AllowPaging = false;
            GrdViewPrint.Columns[6].Visible = false;
            GrdViewPrint.Columns[0].Visible = false;
            DleteTd.Visible = false;
            FirstId.Visible = false;
            HeaderTable.Border = 1;
            HeaderTable.BgColor = "LightBlue";
            //abc.Style.Add(HtmlTextWriterStyle.BackgroundColor, "Red");

            //HeaderTemplatee.Visible = true;
            FileUploadDiv.Visible = false;
            Label4.Visible = false;

            string path = Server.MapPath("~/PDFDATA/");
            string imagepath = Server.MapPath("~/image/");

            string attachment = "attachment;filename=QuatationTemplate" + LBLQUoteIdForImage.Text + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", attachment);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            divtemplate.RenderControl(hw);

            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 40f, 40f, 105f, 50f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

            FileStream file = new FileStream(path + "/QuatationTemplate" + LBLQUoteIdForImage.Text + ".pdf", FileMode.Create);
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, file);

            PdfWriter writer1 = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(imagepath + "/" + imagename.ToString());
            gif.ScaleAbsolute(300f, 80f);

            writer.PageEvent = new ITextEvent();
            writer1.PageEvent = new itextEvent1();
            gif.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "delete from PDFDATA where QUOTEID='" + LBLQUoteIdForImage.Text + "'AND QuoteDate='" + datetime1.Text + "'";
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "insert into PDFDATA (QUOTEID,QuoteDate,FIleName,FilePath) values(@QUOTEID,@QDATE,@NAME,@PATH)";
            com.Parameters.AddWithValue("@QUOTEID", LBLQUoteIdForImage.Text);
            com.Parameters.AddWithValue("@QDATE", datetime1.Text);
            com.Parameters.AddWithValue("@NAME", System.Data.SqlDbType.NVarChar).Value = "QuatationTemplate" + LBLQUoteIdForImage.Text + ".pdf";
            com.Parameters.AddWithValue("@PATH", System.Data.SqlDbType.NVarChar).Value = (path + "/QuatationTemplate" + LBLQUoteIdForImage.Text + ".pdf");
            com.ExecuteNonQuery();
            con.Close();

            pdfDoc.Open();
            pdfDoc.Add(gif);
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);

            FileUploadDiv.Visible = true;
            OpenPDF();
            //Response.End();
        }

        public void OpenPDF()
        {
            string FilePath = null;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;

            com.CommandText = "select FilePath from PDFDATA where QUOTEID ='" + LBLQUoteIdForImage.Text + "' and QuoteDate='" + datetime1.Text + "'";
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                FilePath = dt.Rows[0][0].ToString();
            }
            else
            {

            }
            GrdViewPrint.Columns[6].Visible = true;
            GrdViewPrint.Columns[0].Visible = true;
            DleteTd.Visible = true;
            FirstId.Visible = true;
            imguser.Visible = true;
            HeaderTable.Border = 0;
            BindTemplateData();
            con.Close();
            try
            {
                //view document file 
                if (FilePath.Contains(".doc"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                //view pdf files 
                if (FilePath.Contains(".pdf"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                if (FilePath.Contains(".htm"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                if (FilePath.Contains(".txt"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
                else
                {
                    //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('File not in Proper format');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alert", "alert('File not in Proper format');", true);
            }

        }

        public class ITextEvent : PdfPageEventHelper
        {
            // This is the contentbyte object of the writer
            PdfContentByte cb;

            // we will put the final number of pages in a template
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer
            BaseFont bf = null;

            // This keeps track of the creation time
            DateTime PrintTime = DateTime.Now;


            #region Fields
            private string _header;
            #endregion

            #region Properties
            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }
            #endregion

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                    //handle exception here
                }
                catch (System.IO.IOException ioe)
                {
                    //handle exception here
                }
            }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnEndPage(writer, document);

                //PdfContentByte content = writer.DirectContent;
                //Rectangle rectangle = new Rectangle(document.PageSize);
                //rectangle.Left += document.LeftMargin;
                //rectangle.Right -= document.RightMargin;
                //rectangle.Top -= 10;
                ////rectangle.Bottom += document.BottomMargin;
                //rectangle.Bottom += 10;
                //content.SetColorStroke(BaseColor.BLACK);
                //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                //content.Stroke();

                iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

                //Phrase p1Header = new Phrase("QUOTATION", baseFontNormal);

                //Create PdfTable object for header
                PdfPTable pdfTab = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings
                //Row 1
                iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/image/pmglogo1.jpg"));
                logo.ScaleAbsolute(180f, 50f);

                PdfPCell pdfCell1 = new PdfPCell();
                PdfPCell pdfCell2 = new PdfPCell();
                PdfPCell pdfCell3 = new PdfPCell(logo);
                String text = "Page " + writer.PageNumber + " of ";

                //Add paging to header
                //{
                //    cb.BeginText();
                //    cb.SetFontAndSize(bf, 12);
                //    cb.SetTextMatrix(document.PageSize.GetRight(155), document.PageSize.GetTop(45));
                //    // cb.ShowText(text);
                //    cb.EndText();
                //    //float len = bf.GetWidthPoint(text, 12);
                //    //Adds "12" in Page 1 of 12
                //    //cb.AddTemplate(headerTemplate, document.PageSize.GetRight(155), document.PageSize.GetTop(45));
                //}
                //Add paging to footer
                {
                    cb.BeginText();
                    cb.SetFontAndSize(bf, 12);
                    cb.SetTextMatrix(document.PageSize.GetRight(105), document.PageSize.GetBottom(30));
                    cb.ShowText(text);
                    cb.EndText();
                    float len = bf.GetWidthPoint(text, 12);
                    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(105) + len, document.PageSize.GetBottom(30));
                }
                //Row 2
                //PdfPCell pdfCell4 = new PdfPCell(new Phrase("PMG Stilladser A/S", baseFontNormal));
                ////Row 3

                ////PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                PdfPCell pdfCell5 = new PdfPCell();
                PdfPCell pdfCell6 = new PdfPCell();
                PdfPCell pdfCell7 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                ////Row 4
                PdfPCell pdfCell8 = new PdfPCell();
                PdfPCell pdfCell9 = new PdfPCell();
                PdfPCell pdfCell10 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

                ////set the alignment of all three cells and set border to 0
                pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell3.HorizontalAlignment = Element.ALIGN_LEFT;
                //pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell7.HorizontalAlignment = Element.ALIGN_LEFT;
                pdfCell8.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell9.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell10.HorizontalAlignment = Element.ALIGN_LEFT;

                pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell3.VerticalAlignment = Element.ALIGN_LEFT;
                //pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
                pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell7.VerticalAlignment = Element.ALIGN_LEFT;
                pdfCell8.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell9.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell10.VerticalAlignment = Element.ALIGN_LEFT;

                //pdfCell4.Colspan = 3;
                //// pdfCell10.Colspan = 3;

                pdfCell1.Border = 0;
                pdfCell2.Border = 0;
                pdfCell3.Border = 0;
                //pdfCell4.Border = 0;
                pdfCell5.Border = 0;
                pdfCell6.Border = 0;
                pdfCell7.Border = 0;
                pdfCell8.Border = 0;
                pdfCell9.Border = 0;
                pdfCell10.Border = 0;

                ////add all three cells into PdfTable
                pdfTab.AddCell(pdfCell1);
                pdfTab.AddCell(pdfCell2);
                pdfTab.AddCell(pdfCell3);
                //pdfTab.AddCell(pdfCell4);
                pdfTab.AddCell(pdfCell5);
                pdfTab.AddCell(pdfCell6);
                pdfTab.AddCell(pdfCell7);
                pdfTab.AddCell(pdfCell8);
                pdfTab.AddCell(pdfCell9);
                pdfTab.AddCell(pdfCell10);

                pdfTab.TotalWidth = document.PageSize.Width - 80f;
                pdfTab.WidthPercentage = 70;

                ////call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                ////first param is start row. -1 indicates there is no end row and all the rows to be included to write
                ////Third and fourth param is x and y position to start writing
                pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 10, writer.DirectContent);

                //Move the pointer and draw line to separate header section from rest of page
                //cb.MoveTo(40, document.PageSize.Height - 100);
                //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                //cb.Stroke();

                //Create PdfTable object for footer
                PdfPTable pdfTab1 = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings
                //Row 1
                PdfPCell pdfCell11 = new PdfPCell();
                PdfPCell pdfCell12 = new PdfPCell(new Phrase("PMG Stilladser A/S, Hundigevej 87, 2670 Greve", baseFontBig));
                PdfPCell pdfCell13 = new PdfPCell();

                //Row 2
                PdfPCell pdfCell14 = new PdfPCell(new Phrase("Internet:pmg.dk  Email:info@pmg.dk", baseFontBig));
                //Row 3

                //PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                PdfPCell pdfCell15 = new PdfPCell();
                PdfPCell pdfCell16 = new PdfPCell();
                PdfPCell pdfCell17 = new PdfPCell();

                //set the alignment of all three cells and set border to 0
                pdfCell11.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell12.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell13.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell14.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell15.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell16.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell17.HorizontalAlignment = Element.ALIGN_CENTER;

                pdfCell12.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell13.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell14.VerticalAlignment = Element.ALIGN_TOP;
                pdfCell15.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell16.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell17.VerticalAlignment = Element.ALIGN_MIDDLE;

                pdfCell14.Colspan = 3;
                // pdfCell10.Colspan = 3;

                pdfCell11.Border = 0;
                pdfCell12.Border = 0;
                pdfCell13.Border = 0;
                pdfCell14.Border = 0;
                pdfCell15.Border = 0;
                pdfCell16.Border = 0;
                pdfCell17.Border = 0;

                //add all three cells into PdfTable
                pdfTab1.AddCell(pdfCell11);
                pdfTab1.AddCell(pdfCell12);
                pdfTab1.AddCell(pdfCell13);
                pdfTab1.AddCell(pdfCell14);
                pdfTab1.AddCell(pdfCell15);
                pdfTab1.AddCell(pdfCell16);
                pdfTab1.AddCell(pdfCell17);

                pdfTab1.TotalWidth = document.PageSize.Width - 80f;
                pdfTab1.WidthPercentage = 70;

                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                //Third and fourth param is x and y position to start writing
                pdfTab1.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 780, writer.DirectContent);


                //Move the pointer and draw line to separate footer section from rest of page
                cb.MoveTo(40, document.PageSize.GetBottom(60));
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(60));
                cb.Stroke();
            }

            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

                footerTemplate.BeginText();
                footerTemplate.SetFontAndSize(bf, 12);
                footerTemplate.SetTextMatrix(0, 0);
                footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                footerTemplate.EndText();
            }

            public PdfPCell logo { get; set; }
        }

        public class itextEvent1 : PdfPageEventHelper
        {
            // This is the contentbyte object of the writer
            PdfContentByte cb;

            // we will put the final number of pages in a template
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer
            BaseFont bf = null;

            // This keeps track of the creation time
            DateTime PrintTime = DateTime.Now;


            #region Fields
            private string _header;
            #endregion

            #region Properties
            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }
            #endregion

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                // base.OnOpenDocument(writer, document);
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                    //handle exception here
                }
                catch (System.IO.IOException ioe)
                {
                    //handle exception here
                }
            }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnEndPage(writer, document);

                iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

                Phrase p1Header = new Phrase("QUOTATION", baseFontNormal);

                //Create PdfTable object
                PdfPTable pdfTab = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings
                //Row 1
                PdfPCell pdfCell1 = new PdfPCell();
                PdfPCell pdfCell2 = new PdfPCell(p1Header);
                PdfPCell pdfCell3 = new PdfPCell();
                String text = "Page " + writer.PageNumber + " of ";

                //Add paging to header
                {
                    cb.BeginText();
                    cb.SetFontAndSize(bf, 12);
                    cb.SetTextMatrix(document.PageSize.GetRight(155), document.PageSize.GetTop(45));
                    cb.ShowText(text);
                    cb.EndText();
                    float len = bf.GetWidthPoint(text, 12);
                    //Adds "12" in Page 1 of 12
                    cb.AddTemplate(headerTemplate, document.PageSize.GetRight(155) + len, document.PageSize.GetTop(45));
                }
                //Add paging to footer
                {
                    cb.BeginText();
                    cb.SetFontAndSize(bf, 12);
                    cb.SetTextMatrix(document.PageSize.GetRight(180), document.PageSize.GetBottom(30));
                    cb.ShowText(text);
                    cb.EndText();
                    float len = bf.GetWidthPoint(text, 12);
                    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(180) + len, document.PageSize.GetBottom(30));
                }
                //Row 2
                PdfPCell pdfCell4 = new PdfPCell(new Phrase("CERTIGOA ENTERPRISES", baseFontNormal));
                //Row 3

                PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
                PdfPCell pdfCell6 = new PdfPCell();
                PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

                //set the alignment of all three cells and set border to 0
                pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;

                pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
                pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;

                pdfCell4.Colspan = 3;

                pdfCell1.Border = 0;
                pdfCell2.Border = 0;
                pdfCell3.Border = 0;
                pdfCell4.Border = 0;
                pdfCell5.Border = 0;
                pdfCell6.Border = 0;
                pdfCell7.Border = 0;

                //add all three cells into PdfTable
                pdfTab.AddCell(pdfCell1);
                pdfTab.AddCell(pdfCell2);
                pdfTab.AddCell(pdfCell3);
                pdfTab.AddCell(pdfCell4);
                pdfTab.AddCell(pdfCell5);
                pdfTab.AddCell(pdfCell6);
                pdfTab.AddCell(pdfCell7);

                pdfTab.TotalWidth = document.PageSize.Width - 80f;
                pdfTab.WidthPercentage = 70;



                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                //Third and fourth param is x and y position to start writing
                pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                //Move the pointer and draw line to separate header section from rest of page
                cb.MoveTo(40, document.PageSize.Height - 100);
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                cb.Stroke();

                //Move the pointer and draw line to separate footer section from rest of page
                cb.MoveTo(40, document.PageSize.GetBottom(50));
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
                cb.Stroke();
            }

            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

                footerTemplate.BeginText();
                footerTemplate.SetFontAndSize(bf, 12);
                footerTemplate.SetTextMatrix(0, 0);
                footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                footerTemplate.EndText();
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)//----------------Not In USe---------------------------------------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            //if (txtName.Text == "")
            //{
            //SqlCommand com = new SqlCommand("select * from CUSTOMER ", con);
            SqlCommand com = new SqlCommand("select * from CUSTOMER where CustName like @SearchStr ", con);

            com.Parameters.Add("@SearchStr", SqlDbType.NVarChar, 60).Value = "%" + txtName.Text + "%";

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                //GridViewCustDetails.DataSource = dt;
                //GridViewCustDetails.DataBind();

                divCustlist.Visible = true;
                //fieldsetcustomer.Visible = true;
                divstk.Visible = false;
                headerdiv.Visible = true;
                //div1.Visible = true;
                ItemInfoFieldSet.Visible = true;
                dt.Rows.Clear();

                //divCustlist.Visible = true;
                // fieldsetcustomer.Visible = true;
                //divstk.Visible = false;
                //headerdiv.Visible = true;
                // div1.Visible = true;
            }
            else
            {
                //GridViewCustDetails.DataSource = dt;
                //GridViewCustDetails.DataBind();

                divCustlist.Visible = true;
                //fieldsetcustomer.Visible = true;
                divstk.Visible = false;
                headerdiv.Visible = true;
                //div1.Visible = true;
                ItemInfoFieldSet.Visible = true;
                dt.Rows.Clear();
            }
            //}
            con.Close();
        }

        protected void GridVConDition_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)//----------------Not In USe----------------------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            DropDownList ddlCurrentDropDownList = (DropDownList)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)ddlCurrentDropDownList.Parent.Parent);
            Label lblCondition = (Label)grdrDropDownRow.FindControl("lblCondition");
            if (lblCondition != null)

                com.CommandText = "select * from [Predifined-Text] where StantekstID= " + ddlCurrentDropDownList.SelectedValue;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {

                lblCondition.Text = dt.Rows[0]["Standardtekst"].ToString();
            }
            con.Close();
        }

        protected void GridVConDition_RowDataBound(object sender, GridViewRowEventArgs e)//----------------Not In USe----------------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand("select * from [Predifined-Text]", con);


            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlDropDownList = (DropDownList)e.Row.FindControl("DropDownList2");
                    if (ddlDropDownList != null)
                    {
                        ddlDropDownList.DataSource = dt;
                        ddlDropDownList.DataTextField = "KortNavn";
                        ddlDropDownList.DataValueField = "StantekstID";
                        ddlDropDownList.DataBind();
                    }
                    //ddlDropDownList.Items.Insert(0, new ListItem("-Select Items-", "0"));
                }
            }
            con.Close();
        }

        protected void btnSave_Click(object sender, EventArgs e)//----------------Not In USe-----------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            btnExtoPdf.Visible = true;

            //foreach (GridViewRow rows in GridVConDition.Rows)
            //{
            //    if (rows.RowType == DataControlRowType.DataRow)
            //    {
            //        System.Web.UI.WebControls.DropDownList ddlpre = (rows.Cells[0].FindControl("DropDownList2") as System.Web.UI.WebControls.DropDownList);//int total = 0;
            //        System.Web.UI.WebControls.Label lblCondition = (rows.Cells[0].FindControl("lblCondition") as System.Web.UI.WebControls.Label);


            //        if (lblCondition.Text != "")
            //        {
            //            com.CommandType = System.Data.CommandType.StoredProcedure;
            //            com.CommandText = "sp_InsertShow";
            //            com.Parameters.Add("@DATETIME", SqlDbType.NVarChar, 50).Value = datetime1.Text;//Datetime.Text;
            //            com.Parameters.Add("@StanTekstId", SqlDbType.NVarChar, 50).Value = ddlpre.SelectedValue;
            //            com.ExecuteNonQuery();
            //            com.Parameters.Clear();
            //        }
            //    }
            //}

            foreach (GridViewRow rows in GrdViewPrint.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblQuatationId = (rows.Cells[0].FindControl("lblQuatationId") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[1].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);

                    System.Web.UI.WebControls.TextBox txtEmptyBox = (rows.Cells[2].FindControl("txtEmptyBox") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblItemName = (rows.Cells[2].FindControl("lblItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemQua = (rows.Cells[3].FindControl("lblItemQua") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemPrice = (rows.Cells[4].FindControl("lblItemPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtItemprice = (rows.Cells[4].FindControl("txtItemprice") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblItemgtotal = (rows.Cells[5].FindControl("lblItemgtotal") as System.Web.UI.WebControls.Label);

                    if (lblQuatationId.Text != "")
                    {
                        //com.CommandType = System.Data.CommandType.StoredProcedure;
                        //com.CommandText = "sp_InsertTemplateData";
                        //com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = lblItemCode;
                        //com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblItemName;
                        //com.Parameters.Add("@PRICE", System.Data.SqlDbType.NVarChar).Value = lblItemQua;
                        //com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.NVarChar).Value = lblItemPrice;
                        //com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.NVarChar).Value = lblItemgtotal;
                        //com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        //com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;

                        //com.ExecuteNonQuery();
                        //com.Parameters.Clear();
                        lblItemPrice.Text = txtItemprice.Text;
                        txtItemprice.Visible = false;
                        lblItemPrice.Visible = true;

                    }
                }
            }
            con.Close();
        }

        protected void ddlistCondition_SelectedIndexChanged1(object sender, EventArgs e)//----------------Not In USe-----------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            DropDownList ddlCurrentDropDownList = (DropDownList)sender;

            Label lblCondition = (Label)GridVConDition.FindControl("lblCondition");
            if (lblCondition == null)

                com.CommandText = "select * from [Predifined-Text] where StantekstID= " + ddlCurrentDropDownList.SelectedValue;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
                //com.ExecuteNonQuery();

                //lblCondition.Text = dt.Rows[0]["Standardtekst"].ToString();
            }
            con.Close();
        }

        protected void ddlItemName1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            if (lblPrice1 != null || lblQuantity1 != null)
            {
                if (ddlItemName1.SelectedValue == "-Vælg-")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Select Proper Name!! ');", true);
                    return;
                }
                else
                {
                    com.CommandText = "select * from Stocksitem where ItemCode= " + ddlItemName1.SelectedValue;
                    SqlDataAdapter adpt = new SqlDataAdapter(com);
                    DataTable dt = new DataTable();

                    adpt.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {

                        lblPrice1.Text = dt.Rows[0]["Price"].ToString();
                        lblQuantity1.Text = dt.Rows[0]["Quantity"].ToString();
                        LblItemFees.Text = dt.Rows[0]["ItemFees"].ToString();

                        txtQuantity1.Focus();
                    }
                }
            }
            con.Close();
        }

        protected void txtQuantity1_TextChanged(object sender, EventArgs e)//----------------Not In USe------------------------
        {

            SqlConnection con = new SqlConnection(cs);
            con.Open();

            if (txtQuantity1.Text != "")
            {
                decimal Price = Convert.ToDecimal(lblPrice1.Text);
                long qty = Convert.ToInt64(txtQuantity1.Text);
                decimal total = 0;
                decimal pricqty = 0;
                pricqty = Price * qty;
                total = total + pricqty;
                lblTot1.Text = total.ToString();

                if (LBLResCost.Text != "")
                {
                    //TxtResCost.Text = lblTot.Text;
                    LBLResCost.Text = (Convert.ToDecimal(LBLResCost.Text) + Convert.ToDecimal(lblTot1.Text)).ToString();
                    //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();

                }
                else
                {
                    LBLResCost.Text = lblTot1.Text;
                    //txttot.Text = lblTot.Text;
                }

                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertTemplateData";
                com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.BigInt).Value = ddlItemName1.SelectedValue;
                com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = ddlItemName1.SelectedItem.Text;
                com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = lblPrice1.Text;
                com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity1.Text;
                com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblTot1.Text;
                com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;

                com.ExecuteNonQuery();
                com.Parameters.Clear();

                SqlCommand com1 = new SqlCommand();
                com1.Connection = con;
                com1.CommandType = System.Data.CommandType.Text;
                com1.CommandText = "select distinct * from TemplateData";
                SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
                DataTable dt1 = new DataTable();
                adpt1.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    HeaderTemplatee.Visible = true;
                    GrdViewPrint.Visible = true;
                    GrdViewPrint.DataSource = dt1;
                    GrdViewPrint.DataBind();
                    lblTotalCost1.Text = LBLResCost.Text;
                    lblTotalCost3.Text = lblTotalCost1.Text;
                    lblTotalCost3.Text = LBLResCost.Text;
                    lblTotalCost22.Text = lblTotalCost1.Text;
                    //TxtResCost.Text = lblTotalCost1.Text;
                    com1.Parameters.Clear();
                }

            }
            con.Close();
        }
        //MG
        public void ontextChange()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            con.Open();

            if (lblCustId.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Select Proper Customer!! ');", true);
                return;
            }
            else
            {
                int id = 0;
                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "SELECT (ISNULL(MAX(OrderId),0) + 1) as OrderId FROM ORDERS";
                id = Convert.ToInt32(com.ExecuteScalar().ToString());
                com.Parameters.Clear();

                if (txtQuantity1.Text != "")
                {
                    decimal Price = Convert.ToDecimal(lblPrice1.Text);
                    long qty = Convert.ToInt64(txtQuantity1.Text);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    total = total + pricqty;
                    lblTot1.Text = total.ToString();


                    if (LblTotalEarning.Text == "")
                    {
                        if (txtDiscount.Text != "")
                        {
                            FinalEarn = (Convert.ToDecimal(((Convert.ToDecimal(lblPrice1.Text)) - (Convert.ToDecimal(LblItemFees.Text))) * Convert.ToDecimal(txtQuantity1.Text)));
                            FinalEarn = FinalEarn - ((Convert.ToDecimal(txtDiscount.Text) * total) / 100);
                            LblTotalEarning.Text = Convert.ToString(Convert.ToDecimal(FinalEarn));
                        }
                        else
                        {
                            FinalEarn = (Convert.ToDecimal(((Convert.ToDecimal(lblPrice1.Text)) - (Convert.ToDecimal(LblItemFees.Text))) * Convert.ToDecimal(txtQuantity1.Text)));
                            LblTotalEarning.Text = Convert.ToString(Convert.ToDecimal(FinalEarn));
                        }
                    }
                    else
                    {
                        if (txtDiscount.Text != "")
                        {
                            FinalEarn = (Convert.ToDecimal(((Convert.ToDecimal(lblPrice1.Text)) - (Convert.ToDecimal(LblItemFees.Text))) * Convert.ToDecimal(txtQuantity1.Text)));
                            FinalEarn = FinalEarn - ((Convert.ToDecimal(txtDiscount.Text) * total) / 100);
                            LblTotalEarning.Text = Convert.ToString((Convert.ToDecimal(LblTotalEarning.Text)) + (Convert.ToDecimal(FinalEarn)));
                        }
                        else
                        {
                            FinalEarn = (Convert.ToDecimal(((Convert.ToDecimal(lblPrice1.Text)) - (Convert.ToDecimal(LblItemFees.Text))) * Convert.ToDecimal(txtQuantity1.Text)));

                            LblTotalEarning.Text = Convert.ToString((Convert.ToDecimal(LblTotalEarning.Text)) + (Convert.ToDecimal(FinalEarn)));
                        }
                    }

                    if (txtDiscount.Text != "")
                    {
                        total = total - ((Convert.ToDecimal(txtDiscount.Text) * total) / 100);
                        lblTot1.Text = total.ToString();
                    }
                    else
                    {
                        lblTot1.Text = total.ToString();
                    }

                    if (LBLResCost.Text != "")
                    {
                        //TxtResCost.Text = lblTot.Text;
                        LBLResCost.Text = (Convert.ToDecimal(LBLResCost.Text) + Convert.ToDecimal(lblTot1.Text)).ToString();
                        //txttot.Text = (Convert.ToDecimal(txttot.Text) + Convert.ToDecimal(lblTot.Text)).ToString();
                    }
                    else
                    {
                        LBLResCost.Text = lblTot1.Text;
                        //txttot.Text = lblTot.Text;
                    }
                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_InsertTemplateData";
                    com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = ddlItemName1.SelectedValue;
                    com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = ddlItemName1.SelectedItem.Text;
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = lblPrice1.Text;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = txtQuantity1.Text;
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblTot1.Text;
                    com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = LblItemFees.Text;
                    com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = FinalEarn;
                    com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;
                    com.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 0;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    if (datetime1.Text == txtDate.Text)
                    {
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertHAS";
                        com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                        com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = ddlItemName1.SelectedValue;
                        com.Parameters.Add("@DATETIME", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                        com.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity1.Text;
                        com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                        com.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                        com.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                        com.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = "";

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                    }
                    else
                    {
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertHAS";
                        com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                        com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = ddlItemName1.SelectedValue;
                        com.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        com.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity1.Text;
                        com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                        com.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                        com.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        com.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = "";

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                    }

                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "select Distinct * from TemplateData where TempDate='" + datetime1.Text + "' AND TCType=0 ORDER BY TEMPID";//AND QuaDate='" + txtDate.Text + "'OR QuaDate='" + txtDate.Text + "'

                    SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                    DataTable dt1 = new DataTable();
                    adpt1.Fill(dt1);
                    if (dt1.Rows.Count > 0)
                    {
                        HeaderTemplatee.Visible = true;
                        GrdViewPrint.Visible = true;
                        GrdViewPrint.DataSource = dt1;
                        GrdViewPrint.DataBind();

                        GrdViewPrint1.DataSource = dt1;
                        GrdViewPrint1.DataBind();

                        HeaderTemplatee1.Visible = true;
                        GrdViewPrint2.DataSource = dt1;
                        GrdViewPrint2.DataBind();

                        GridViewPreText.DataSource = dt1;
                        GridViewPreText.DataBind();

                        lblTotalCost1.Text = LBLResCost.Text;
                        lblTotalCost3.Text = LBLResCost.Text;
                        TxtResCost.Text = lblTotalCost1.Text;

                        lblTotalCost22.Text = LBLResCost.Text;
                        com.Parameters.Clear();
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Enter Proper Quantity!! ');", true);
                    return;
                }
            }
            con.Close();
        }

        protected void ddlistCondition_SelectedIndexChanged(object sender, EventArgs e)//-------------Not In Use---------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            com.CommandText = "select * from [Predifined-Text] where StantekstID= " + ddlistCondition.SelectedValue;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                //lblID1.Text = dt.Rows[0]["ID"].ToString();
                lblTermsAndCondition1.Text = dt.Rows[0]["Standardtekst"].ToString();
                //Label lblCondition = (Label)GridVConDition.FindControl("lblCondition");
                //if (lblCondition == null)
                //dt.Rows.Clear();

                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertTermsAndCondition";
                com.Parameters.Add("@TERMSANDCONDITION", System.Data.SqlDbType.VarChar).Value = lblTermsAndCondition1.Text;
                //com1.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = lblPrice.Text;
                //com1.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                //com1.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblTot.Text;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
            }
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select distinct * from TERMSAndCONDITION";
            adpt.SelectCommand = com;

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
                //com.ExecuteNonQuery();

                //lblCondition.Text = dt.Rows[0]["Standardtekst"].ToString();
            }
            con.Close();
        }

        protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            // Session["quatdate"] = null;

            if (lblCustId.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Select Proper Customer!! ');", true);
                return;
            }
            else
            {
                if (string.IsNullOrEmpty(LBLResCost.Text))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Empty Quotation ! Select Proper Products!! ');", true);
                    return;
                }
                else
                {
                    DateTime orddate = DateTime.Today.Date;

                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    int id = 0;
                    int QuoteId = 0;
                    int QuoteDataID = 0;
                    cmd.CommandText = "SELECT (ISNULL(MAX(OrderId),0) + 1) as OrderId FROM ORDERS";
                    id = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    cmd.Parameters.Clear();

                    cmd.CommandText = "SELECT (ISNULL(MAX(QUOTID),0) + 1) as QUOTID FROM REQUESTEDQUOTE";
                    QuoteId = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    cmd.Parameters.Clear();

                    cmd.Parameters.Clear();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "Delete From QUOTATIONDATA Where QuaDate=@I_QUATDATE AND TEMPType='" + 0 + "'";
                    cmd.Parameters.Add("@I_QUATDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandText = "SELECT (ISNULL(MAX(QuoteID),0) + 1) as QuoteID FROM QUOTATIONDATA";
                    QuoteDataID = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    cmd.Parameters.Clear();

                    foreach (GridViewRow rows in GrdViewPrint.Rows)
                    {
                        if (rows.RowType == DataControlRowType.DataRow)
                        {
                            System.Web.UI.WebControls.Label lblQuatationId = (rows.Cells[0].FindControl("lblQuatationId") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[1].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblItemName = (rows.Cells[2].FindControl("lblItemName") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblItemQua = (rows.Cells[3].FindControl("lblItemQua") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label LabelItemFees = (rows.Cells[4].FindControl("LabelItemFees") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblItemPrice = (rows.Cells[4].FindControl("lblItemPrice") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblEarning = (rows.Cells[5].FindControl("lblEarning") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblItemgtotal = (rows.Cells[5].FindControl("lblItemgtotal") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblServiceType = (rows.Cells[5].FindControl("lblServiceType") as System.Web.UI.WebControls.Label);


                            if (lblItemQua.Text != "")
                            {
                                if (datetime1.Text == txtDate.Text)
                                {
                                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                    cmd.CommandText = "sp_UpdateHAS";
                                    cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                                    cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                                    cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                                    cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                                    cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = lblItemQua.Text;
                                    cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                                    cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                                    cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                                    cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = LblTotalEarning.Text;

                                    cmd.ExecuteNonQuery();
                                    cmd.Parameters.Clear();
                                }
                                else
                                {
                                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                    cmd.CommandText = "sp_UpdateHAS";
                                    cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                                    cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                                    cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                                    cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = lbllDatetime.Text;
                                    cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = lblItemQua.Text;
                                    cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                                    cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                                    cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                                    cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = LblTotalEarning.Text;

                                    cmd.ExecuteNonQuery();
                                    cmd.Parameters.Clear();
                                }
                            }

                            cmd.Parameters.Clear();
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.CommandText = "sp_InsertQuotationData";

                            cmd.Parameters.Add("@I_QUOTEID", System.Data.SqlDbType.BigInt).Value = QuoteDataID;
                            // cmd.Parameters.Add("@I_TEMPID", System.Data.SqlDbType.BigInt).Value = lblQuatationId.Text;
                            cmd.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = lblItemCode.Text;
                            cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblItemName.Text;

                            if (string.IsNullOrEmpty(lblItemPrice.Text))
                            {
                                cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = lblItemPrice.Text;
                            }
                            if (string.IsNullOrEmpty(lblItemQua.Text))
                            {
                                cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = lblItemQua.Text;
                            }
                            if (string.IsNullOrEmpty(lblItemgtotal.Text))
                            {
                                cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblItemgtotal.Text;
                            }

                            cmd.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                            cmd.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;

                            if (string.IsNullOrEmpty(LabelItemFees.Text))
                            {
                                cmd.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = LabelItemFees.Text;
                            }
                            if (string.IsNullOrEmpty(lblEarning.Text))
                            {
                                cmd.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = lblEarning.Text;
                            }

                            cmd.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;
                            cmd.Parameters.Add("@TEMPTYPE", System.Data.SqlDbType.Bit).Value = 0;
                            cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = lblServiceType.Text;

                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                    }

                    foreach (GridViewRow rows in GridViewTC.Rows)
                    {
                        if (rows.RowType == DataControlRowType.DataRow)
                        {
                            System.Web.UI.WebControls.Label lblTermNcondition = (rows.Cells[0].FindControl("lblTermNcondition") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblQuatationId = (rows.Cells[0].FindControl("lblQuatationId") as System.Web.UI.WebControls.Label);

                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.CommandText = "sp_InsertQuotationData";
                            cmd.Parameters.Add("@I_QUOTEID", System.Data.SqlDbType.BigInt).Value = QuoteDataID;
                            // cmd.Parameters.Add("@I_TEMPID", System.Data.SqlDbType.BigInt).Value = lblQuatationId.Text;
                            cmd.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = "";
                            cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblTermNcondition.Text;
                            cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            cmd.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                            cmd.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                            cmd.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            cmd.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                            cmd.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 1;
                            cmd.Parameters.Add("@TEMPTYPE", System.Data.SqlDbType.Bit).Value = 0;
                            cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 0;

                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                    }

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "Delete From TemplateData";
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    if (datetime1.Text == txtDate.Text)
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_InsertRequestQuote";

                        cmd.Parameters.Add("@I_QUOTID", System.Data.SqlDbType.BigInt).Value = QuoteId;
                        cmd.Parameters.Add("@DATETIME", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                        cmd.Parameters.Add("@CUSTACK", System.Data.SqlDbType.VarChar).Value = ddlCustAck.SelectedValue;
                        cmd.Parameters.Add("@ADMINAPPR", System.Data.SqlDbType.VarChar).Value = ddlAdminApp.SelectedValue;
                        cmd.Parameters.Add("@EXPECTTIME", System.Data.SqlDbType.DateTime).Value = Convert.ToDateTime((txtResTime.Text).ToString());
                        cmd.Parameters.Add("@RESPTIME", System.Data.SqlDbType.DateTime).Value = Convert.ToDateTime((txtResTime.Text).ToString());
                        cmd.Parameters.Add("@TOTALCOST", System.Data.SqlDbType.Decimal).Value = LBLResCost.Text;

                        cmd.Parameters.Add("@QUOTENAME", System.Data.SqlDbType.NVarChar).Value = QuoteId.ToString() + " - " + DropDownUserList.SelectedItem;

                        //cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = LblTotalEarning.Text;

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        Response.Redirect("Order Form.aspx?DATETIME=" + txtDate.Text);
                    }
                    else
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_UpdateRequestQuote";
                        cmd.Parameters.Add("@DATETIME", System.Data.SqlDbType.NVarChar).Value = datetime1.Text; ;
                        cmd.Parameters.Add("@CUSTACK", System.Data.SqlDbType.VarChar).Value = ddlCustAck.SelectedItem.Text;
                        cmd.Parameters.Add("@ADMINAPPR", System.Data.SqlDbType.VarChar).Value = ddlAdminApp.SelectedItem.Text;
                        cmd.Parameters.Add("@EXPECTTIME", System.Data.SqlDbType.DateTime).Value = txtResTime.Text;
                        cmd.Parameters.Add("@RESPTIME", System.Data.SqlDbType.DateTime).Value = txtResTime.Text;
                        cmd.Parameters.Add("@TOTALCOST", System.Data.SqlDbType.Decimal).Value = TxtResCost.Text;
                        //cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = LblTotalEarning.Text;

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        Response.Redirect("Order Form.aspx?DATETIME=" + datetime1.Text);
                    }

                    Thread.Sleep(2000);

                    //lblfinish.Visible = true;
                    con.Close();
                }
            }
        }

        protected void btnQuantity_Click(object sender, EventArgs e)
        {
            if (ddlItemName1.Text == "-Vælg-")
            {
                txtQuantity1.Text = "";
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Select Proper Product!! ');", true);
                return;
            }
            else
            {
                ontextChange();
                txtQuantity1.Text = "";
            }
        }

        protected void btnAddTD_Click(object sender, EventArgs e)
        {
            if (Global.fflag == 0)
            {
                EmptyLine();
            }

            if (ddlistCondition.SelectedIndex > 0)
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;

                com.CommandText = "select * from [Predifined-Text] where StantekstID= " + ddlistCondition.SelectedValue;

                SqlDataAdapter adpt = new SqlDataAdapter(com);
                DataTable dt = new DataTable();

                adpt.Fill(dt);

                com.Parameters.Clear();

                if (dt.Rows.Count > 0)
                {
                    lblID1.Text = dt.Rows[0]["StantekstID"].ToString();
                    lblTermsAndCondition1.Text = dt.Rows[0]["Standardtekst"].ToString();
                    //(System.Data.SqlDbType.BigInt) sasa = dt.Rows[0]["StantekstID"].ToString();

                    //Label lblCondition = (Label)GridVConDition.FindControl("lblCondition");
                    //if (lblCondition == null)
                    //dt.Rows.Clear();

                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    //com.CommandText = "sp_InsertTempTemplateData";
                    com.CommandText = "sp_InsertTemplateData";
                    //com.CommandText = "sp_InsertTermsAndCondition";
                    //com.Parameters.Add("@TERMSANDCONDITION", System.Data.SqlDbType.VarChar).Value = lblTermsAndCondition1.Text;
                    com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = "";
                    com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblTermsAndCondition1.Text;
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;
                    com.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 0;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    //BindTemplateData();
                    //QuotationTemplateData();
                }

                com.Parameters.Clear();
                dt.Rows.Clear();

                com.CommandType = System.Data.CommandType.Text;

                com.CommandText = "select Distinct * from TemplateData where TempDate = '" + datetime1.Text + "' AND TCType=0 ORDER BY TEMPID";
                SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                DataTable dt1 = new DataTable();
                adpt1.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    GrdViewPrint.Visible = true;
                    GrdViewPrint.DataSource = dt1;
                    GrdViewPrint.DataBind();

                    GrdViewPrint1.DataSource = dt1;
                    GrdViewPrint1.DataBind();

                    GridViewPreText.DataSource = dt1;
                    GridViewPreText.DataBind();

                    GrdViewPrint2.Visible = true;
                    GrdViewPrint2.DataSource = dt1;
                    GrdViewPrint2.DataBind();
                }
                com.Parameters.Clear();
                dt.Rows.Clear();

                con.Close();
            }
        }

        protected void DropDownListSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListSelect.Text == "Stock Item(Product)")
            {
                Global.fflag = 0;
                ddlistCondition.Visible = false;
                btnAddTD.Visible = false;
                TrIdtblTND.Visible = false;
                headerdiv1.Visible = true;
                TaskIdtblTND.Visible = true;
                ddlItemName1.Visible = true;
                txtQuantity1.Visible = true;

                TxtTermsAnCondition.Visible = false;
                TrServiceItem.Visible = false;
                DropDownListServiceItem.Visible = false;
                BtnAddItemService.Visible = false;

                txtCText1.Visible = false;
                txtCText2.Visible = false;
                txtCText3.Visible = false;
                btnADDC.Visible = false;

                TrTemplate.Visible = false;
                txtTemplate.Visible = false;
                ddlistTemplate.Visible = false;
                BtnADDTemplate.Visible = false;
            }
            else
                if (DropDownListSelect.Text == "Predifined Text")
                {
                    Global.fflag = 0;
                    ddlItemName1.Visible = false;
                    txtQuantity1.Visible = false;
                    TaskIdtblTND.Visible = false;
                    headerdiv1.Visible = true;
                    TrIdtblTND.Visible = true;
                    ddlistCondition.Visible = true;
                    btnAddTD.Visible = true;

                    TxtTermsAnCondition.Visible = true;
                    TrServiceItem.Visible = false;
                    DropDownListServiceItem.Visible = false;
                    BtnAddItemService.Visible = false;

                    txtCText1.Visible = false;
                    txtCText2.Visible = false;
                    txtCText3.Visible = false;
                    btnADDC.Visible = false;

                    TrTemplate.Visible = false;
                    txtTemplate.Visible = false;
                    ddlistTemplate.Visible = false;
                    BtnADDTemplate.Visible = false;

                    TxtTermsAnCondition.Focus();
                }
                else
                    if (DropDownListSelect.Text == "Empty Line")
                    {
                        ddlistCondition.Visible = false;
                        btnAddTD.Visible = false;
                        TrIdtblTND.Visible = false;
                        headerdiv1.Visible = true;
                        ddlItemName1.Visible = false;
                        txtQuantity1.Visible = false;
                        TaskIdtblTND.Visible = false;

                        TxtTermsAnCondition.Visible = false;
                        TrServiceItem.Visible = false;
                        DropDownListServiceItem.Visible = false;
                        BtnAddItemService.Visible = false;

                        txtCText1.Visible = false;
                        txtCText2.Visible = false;
                        txtCText3.Visible = false;
                        btnADDC.Visible = false;

                        TrTemplate.Visible = false;
                        txtTemplate.Visible = false;
                        ddlistTemplate.Visible = false;
                        BtnADDTemplate.Visible = false;

                        if (Global.fflag == 0)
                        {
                            EmptyLine();
                        }
                    }
                    else
                        if (DropDownListSelect.Text == "Stock Item(Service)")
                        {
                            Global.fflag = 0;
                            ddlistCondition.Visible = false;
                            btnAddTD.Visible = false;
                            TrIdtblTND.Visible = false;
                            headerdiv1.Visible = true;
                            ddlItemName1.Visible = false;
                            txtQuantity1.Visible = false;
                            TaskIdtblTND.Visible = false;

                            TxtTermsAnCondition.Visible = false;
                            TrServiceItem.Visible = true;
                            DropDownListServiceItem.Visible = true;
                            BtnAddItemService.Visible = true;

                            txtCText1.Visible = false;
                            txtCText2.Visible = false;
                            txtCText3.Visible = false;
                            btnADDC.Visible = false;

                            TrTemplate.Visible = false;
                            txtTemplate.Visible = false;
                            ddlistTemplate.Visible = false;
                            BtnADDTemplate.Visible = false;
                        }
                        else if (DropDownListSelect.Text == "Salutation Text1")
                        {
                            Global.fflag = 0;
                            ddlItemName1.Visible = false;
                            txtQuantity1.Visible = false;
                            TaskIdtblTND.Visible = false;
                            headerdiv1.Visible = false;
                            TrIdtblTND.Visible = false;
                            ddlistCondition.Visible = false;
                            btnAddTD.Visible = false;

                            TxtTermsAnCondition.Visible = false;
                            TrServiceItem.Visible = false;
                            DropDownListServiceItem.Visible = false;
                            BtnAddItemService.Visible = false;

                            txtCText1.Visible = true;
                            txtCText2.Visible = false;
                            txtCText3.Visible = false;
                            btnADDC.Visible = true;

                            TrTemplate.Visible = false;
                            txtTemplate.Visible = false;
                            ddlistTemplate.Visible = false;
                            BtnADDTemplate.Visible = false;

                            txtCText1.Focus();
                            //txtCText1.Font.Bold = true;
                        }
                        else if (DropDownListSelect.Text == "Salutation Text2")
                        {
                            Global.fflag = 0;
                            ddlItemName1.Visible = false;
                            txtQuantity1.Visible = false;
                            TaskIdtblTND.Visible = false;
                            headerdiv1.Visible = false;
                            TrIdtblTND.Visible = false;
                            ddlistCondition.Visible = false;
                            btnAddTD.Visible = false;

                            TxtTermsAnCondition.Visible = false;
                            TrServiceItem.Visible = false;
                            DropDownListServiceItem.Visible = false;
                            BtnAddItemService.Visible = false;

                            txtCText1.Visible = false;
                            txtCText2.Visible = true;
                            txtCText3.Visible = false;
                            btnADDC.Visible = true;

                            TrTemplate.Visible = false;
                            txtTemplate.Visible = false;
                            ddlistTemplate.Visible = false;
                            BtnADDTemplate.Visible = false;

                            txtCText2.Focus();
                            txtCText2.Text = "Idet vi henviser til Deres forespørgsel har vi hermed formøjelsen at tilbyde Dem leje og montage af let stålstillads til ovennævnte arbejdplads, som følger:";
                        }
                        else if (DropDownListSelect.Text == "Salutation Text3")
                        {
                            Global.fflag = 0;
                            ddlItemName1.Visible = false;
                            txtQuantity1.Visible = false;
                            TaskIdtblTND.Visible = false;
                            headerdiv1.Visible = false;
                            TrIdtblTND.Visible = false;
                            ddlistCondition.Visible = false;
                            btnAddTD.Visible = false;

                            TxtTermsAnCondition.Visible = false;
                            TrServiceItem.Visible = false;
                            DropDownListServiceItem.Visible = false;
                            BtnAddItemService.Visible = false;

                            txtCText1.Visible = false;
                            txtCText2.Visible = false;
                            txtCText3.Visible = true;
                            btnADDC.Visible = true;

                            TrTemplate.Visible = false;
                            txtTemplate.Visible = false;
                            ddlistTemplate.Visible = false;
                            BtnADDTemplate.Visible = false;

                            txtCText3.Focus();
                            //txtCText3.Font.Bold = true;
                            txtCText3.Text = "Stillads opstilles til tagarbejde,forsynet med bredt arbejdsdæk,kasseskærm og finerafdækining ved tagfod.";
                        }
                        else if (DropDownListSelect.Text == "Template")
                        {
                            Global.fflag = 0;
                            ddlItemName1.Visible = false;
                            txtQuantity1.Visible = false;
                            TaskIdtblTND.Visible = false;
                            headerdiv1.Visible = true;
                            TrIdtblTND.Visible = false;
                            ddlistCondition.Visible = false;
                            btnAddTD.Visible = false;

                            TxtTermsAnCondition.Visible = false;
                            TrServiceItem.Visible = false;
                            DropDownListServiceItem.Visible = false;
                            BtnAddItemService.Visible = false;

                            txtCText1.Visible = false;
                            txtCText2.Visible = false;
                            txtCText3.Visible = false;
                            btnADDC.Visible = false;

                            if (Global.FlagVis == true)
                            {
                                TrTemplate.Visible = false;
                                txtTemplate.Visible = false;
                                ddlistTemplate.Visible = false;
                                BtnADDTemplate.Visible = false;
                            }
                            else
                            {
                                TrTemplate.Visible = true;
                                txtTemplate.Visible = true;
                                ddlistTemplate.Visible = true;
                                BtnADDTemplate.Visible = true;
                                txtTemplate.Focus();
                            }
                        }
        }

        public void EmptyLine()
        {
            if (Global.fflag == 0)
            {
                if (lblLineEmptyTotal.Text == "")
                {
                    lblLineEmptyTotal.Text = lblTotalCost1.Text;
                    lblSubTotal.Text = lblTotalCost1.Text;
                }
                else
                {
                    lblLineEmptyTotal.Text = Convert.ToString((Convert.ToDecimal(lblTotalCost1.Text)) - (Convert.ToDecimal(lblSubTotal.Text)));
                    lblSubTotal.Text = lblTotalCost1.Text;
                }
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;


                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertTemplateData";

                com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = "";
                com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = "";
                com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                if (string.IsNullOrEmpty(lblLineEmptyTotal.Text))
                {
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                }
                else
                {
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblLineEmptyTotal.Text;
                }
                com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;
                com.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 0;

                com.ExecuteNonQuery();
                com.Parameters.Clear();

                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "select Distinct * from TemplateData where TempDate='" + datetime1.Text + "' AND TCType=0 ORDER BY TEMPID";
                SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                DataTable dt1 = new DataTable();
                adpt1.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    HeaderTemplatee.Visible = true;
                    GrdViewPrint.Visible = true;

                    GrdViewPrint.DataSource = dt1;
                    GrdViewPrint.DataBind();

                    GrdViewPrint1.DataSource = dt1;
                    GrdViewPrint1.DataBind();

                    HeaderTemplatee1.Visible = true;
                    GrdViewPrint2.DataSource = dt1;
                    GrdViewPrint2.DataBind();

                    GridViewPreText.DataSource = dt1;
                    GridViewPreText.DataBind();
                }
                com.Parameters.Clear();
                con.Close();

                Global.fflag = 1;
            }
        }

        protected void GrdViewPrint_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (DropDownListSelect.Text == "Empty Line")
            //{

            foreach (GridViewRow rows in GrdViewPrint.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.TextBox txtEmptyBox = (rows.Cells[1].FindControl("txtEmptyBox") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblItemName = (rows.Cells[1].FindControl("lblItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemQua = (rows.Cells[3].FindControl("lblItemQua") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtItemprice = (rows.Cells[4].FindControl("txtItemprice") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblItemgtotal = (rows.Cells[4].FindControl("lblItemgtotal") as System.Web.UI.WebControls.Label);

                    if (lblItemName.Text == "")
                    {
                        txtEmptyBox.Visible = true;
                        lblItemName.Visible = false;
                    }
                    else
                    {
                        txtEmptyBox.Visible = false;
                        lblItemName.Visible = true;
                    }
                    //if (txtItemprice.Text == "")
                    //{
                    //    txtItemprice.Visible = false;
                    //}
                    //else
                    //{
                    //    txtItemprice.Visible = true;
                    //}
                }
            }
        }

        protected void GrdViewPrint_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Global.subValues = 0;

            Global.fflag = 0;

            if (e.CommandName == "DeleteRow")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                lblDeletedId.Text = id.ToString();

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "select Distinct * from TemplateData where TEMPID='" + id + "' ORDER BY TEMPID";
                SqlDataAdapter adpt11 = new SqlDataAdapter(cmd);
                DataSet dt11 = new DataSet();
                adpt11.Fill(dt11);

                if (dt11.Tables[0].Rows.Count > 0)
                {
                    string itemcode = dt11.Tables[0].Rows[0]["ItemCode"].ToString();
                    string itemname = dt11.Tables[0].Rows[0]["ItemName"].ToString();
                    string price = dt11.Tables[0].Rows[0]["TotalPrice"].ToString();
                    string totalEarn = dt11.Tables[0].Rows[0]["TotalEarn"].ToString();

                    cmd.Parameters.Clear();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "delete from HAS where ItemCode='" + itemcode + "'AND DateTime='" + datetime1.Text + "'";
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    if (price != string.Empty && itemcode != string.Empty)
                    {
                        lblTotalCost1.Text = ((Convert.ToDecimal(lblTotalCost1.Text)) - (Convert.ToDecimal(price))).ToString();
                        LblTotalEarning.Text = ((Convert.ToDecimal(LblTotalEarning.Text)) - (Convert.ToDecimal(totalEarn))).ToString();

                        lblTotalCost3.Text = lblTotalCost1.Text;
                        lblTotalCost22.Text = lblTotalCost1.Text;
                        TxtResCost.Text = lblTotalCost1.Text;
                        LBLResCost.Text = lblTotalCost1.Text;
                        lblSubTotal.Text = lblTotalCost1.Text;

                        foreach (GridViewRow rows in GrdViewPrint.Rows)
                        {
                            if (Global.subValues == 0)
                            {
                                if (rows.RowType == DataControlRowType.DataRow)
                                {
                                    System.Web.UI.WebControls.Label lblQuatationId11 = (rows.Cells[0].FindControl("lblQuatationId") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemCode11 = (rows.Cells[1].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.TextBox txtEmptyBox11 = (rows.Cells[2].FindControl("txtEmptyBox") as System.Web.UI.WebControls.TextBox);
                                    System.Web.UI.WebControls.Label lblItemName11 = (rows.Cells[2].FindControl("lblItemName") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemQua11 = (rows.Cells[3].FindControl("lblItemQua") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemPrice = (rows.Cells[4].FindControl("lblItemPrice") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.TextBox txtItemprice11 = (rows.Cells[4].FindControl("txtItemprice") as System.Web.UI.WebControls.TextBox);
                                    System.Web.UI.WebControls.Label lblItemgtotal11 = (rows.Cells[5].FindControl("lblItemgtotal") as System.Web.UI.WebControls.Label);

                                    if ((Convert.ToDecimal(id)) <= (Convert.ToDecimal(lblQuatationId11.Text)))
                                    {
                                        if (txtEmptyBox11.Text == "")
                                        {
                                            lblItemgtotal11.Text = ((Convert.ToDecimal(lblItemgtotal11.Text)) - (Convert.ToDecimal(price))).ToString();

                                            cmd.Parameters.Clear();
                                            cmd.CommandType = System.Data.CommandType.Text;
                                            cmd.CommandText = " Update TemplateData SET  Price=@PRICE,TotalPrice=@TOTALPRICE Where TEMPID='" + lblQuatationId11.Text + "' and QuaDate='" + datetime1.Text + "'";
                                            if (string.IsNullOrEmpty(txtItemprice11.Text))
                                            {
                                                cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                                            }
                                            else
                                            {
                                                cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtItemprice11.Text;
                                            }

                                            cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblItemgtotal11.Text;
                                            cmd.ExecuteNonQuery();
                                            cmd.Parameters.Clear();

                                            Global.subValues = 1;
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        TxtResCost.Text = lblTotalCost1.Text;
                        LBLResCost.Text = lblTotalCost1.Text;
                    }
                }

                cmd.Parameters.Clear();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "delete from TemplateData where TEMPID='" + id + "'AND TempDate='" + datetime1.Text + "'";
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                GrdViewPrint.Visible = true;

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "select Distinct * from TemplateData where TempDate='" + datetime1.Text + "' AND TCType=0 ORDER BY TEMPID";
                SqlDataAdapter adpt1 = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                adpt1.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    HeaderTemplatee.Visible = true;
                    GrdViewPrint.Visible = true;
                    GrdViewPrint.DataSource = dt1;
                    GrdViewPrint.DataBind();

                    GrdViewPrint1.DataSource = dt1;
                    GrdViewPrint1.DataBind();

                    GrdViewPrint2.DataSource = dt1;
                    GrdViewPrint2.DataBind();

                    cmd.Parameters.Clear();
                }
                else
                {
                    GrdViewPrint.DataSource = dt1;
                    GrdViewPrint.DataBind();

                    GrdViewPrint1.DataSource = dt1;
                    GrdViewPrint1.DataBind();

                    GrdViewPrint2.DataSource = dt1;
                    GrdViewPrint2.DataBind();
                }
                con.Close();
            }
        }

        public void BindGridView()
        {
            String TempDatee = Session["quatdate"].ToString();

            GetFilesForQuote();
            BindTCGrid(TempDatee);

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getQuotationData";

            com.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = TempDatee;
            com.Parameters.Add("@Flag", System.Data.SqlDbType.Bit).Value = Flag;
            com.ExecuteNonQuery();

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getTemplateData";
            com.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = TempDatee;
            SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            DataSet dt1 = new DataSet();
            adpt1.Fill(dt1);

            if (dt1.Tables[0].Rows.Count > 0)
            {
                HeaderTemplatee.Visible = true;
                GrdViewPrint.DataSource = dt1;
                GrdViewPrint.DataBind();

                GrdViewPrint1.DataSource = dt1;
                GrdViewPrint1.DataBind();

                GrdViewPrint2.DataSource = dt1;
                GrdViewPrint2.DataBind();

                GridViewPreText.DataSource = dt1;
                GridViewPreText.DataBind();

                Wizard1.ActiveStepIndex = Wizard1.WizardSteps.IndexOf(w2);

                LblCustName.Text = dt1.Tables[0].Rows[0]["CustName"].ToString();
                LblCustAddress.Text = dt1.Tables[0].Rows[0]["AddressLine1"].ToString();
                LblCity.Text = dt1.Tables[0].Rows[0]["City"].ToString();
                LblEmail.Text = dt1.Tables[0].Rows[0]["EmailId"].ToString();
                lblTotalCost1.Text = dt1.Tables[0].Rows[0]["TotalCost"].ToString();
                txtName.Text = dt1.Tables[0].Rows[0]["CustName"].ToString();

                LblCustName1.Text = dt1.Tables[0].Rows[0]["CustName"].ToString();
                LblCustAddress1.Text = dt1.Tables[0].Rows[0]["AddressLine1"].ToString();
                LblCity1.Text = dt1.Tables[0].Rows[0]["City"].ToString();
                LblEmail1.Text = dt1.Tables[0].Rows[0]["EmailId"].ToString();
                lblTotalCost3.Text = dt1.Tables[0].Rows[0]["TotalCost"].ToString();

                LblCustName2.Text = dt1.Tables[0].Rows[0]["CustName"].ToString();
                LblCustAddress2.Text = dt1.Tables[0].Rows[0]["AddressLine1"].ToString();
                LblCity2.Text = dt1.Tables[0].Rows[0]["City"].ToString();
                LblEmail2.Text = dt1.Tables[0].Rows[0]["EmailId"].ToString();
                lblTotalCost22.Text = dt1.Tables[0].Rows[0]["TotalCost"].ToString();

                //DropDownUserList.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                //DropDownUserList.Items.Insert(0, ("-Select-"));
                DropDownUserList.SelectedItem.Text = dt1.Tables[0].Rows[0]["CustName"].ToString();
                txtResTime.Text = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(dt1.Tables[0].Rows[0]["RespondTime"].ToString()));
                ddlCustAck.Text = dt1.Tables[0].Rows[0]["CustAck"].ToString();
                ddlAdminApp.Text = dt1.Tables[0].Rows[0]["AdminApproval"].ToString();
                TxtResCost.Text = dt1.Tables[0].Rows[0]["TotalCost"].ToString();
                LBLResCost.Text = dt1.Tables[0].Rows[0]["TotalCost"].ToString();
                ddquotestatus.Text = dt1.Tables[0].Rows[0]["Status"].ToString();
                lblCustId.Text = dt1.Tables[0].Rows[0]["CustId"].ToString();
                LBLQUoteIdForImage.Text = dt1.Tables[0].Rows[0]["QUOTID"].ToString();

                LblTotalEarning.Text = dt1.Tables[0].Rows[0]["QuoteEarn"].ToString();
                lbllDatetime.Text = dt1.Tables[0].Rows[0]["OrderId"].ToString();
                datetime1.Text = TempDatee;

                DisplayDivBasicInfo();
                // //txtDate.Text = TempDatee;

                //imguser.ImageUrl = "~/QupteImageHandler.ashx?QuoteID=" + LBLQUoteIdForImage.Text;
                //imguser1.ImageUrl = "~/QupteImageHandler.ashx?QuoteID=" + LBLQUoteIdForImage.Text;
            }
            else
            {
                HeaderTemplatee.Visible = true;
                GrdViewPrint.DataSource = dt1;
                GrdViewPrint.DataBind();

                Wizard1.ActiveStepIndex = Wizard1.WizardSteps.IndexOf(w1);

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any  Template Created  !! ');", true);
                return;
            }
            con.Close();

            Session["quatdate"] = null;
        }

        public void DisplayDivBasicInfo()
        {
            divBasicInfo2.Visible = true;

            //if (DropDownUserList.Text != "-Vælg-")
            //{
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand("select * from CUSTOMER where CustId =" + lblCustId.Text);
            com.Connection = con;
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                GridViewBasicInfo.DataSource = dt;
                GridViewBasicInfo.DataBind();

                //LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                txtName1.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                txtEmail.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                txtAddress.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                txtcity.Text = dt.Tables[0].Rows[0]["City"].ToString();
                txtcountry.Text = dt.Tables[0].Rows[0]["Country"].ToString();
                txtzip.Text = dt.Tables[0].Rows[0]["ZipCode"].ToString();
                txtMobile.Text = dt.Tables[0].Rows[0]["Mobile"].ToString();
                txtphone.Text = dt.Tables[0].Rows[0]["Phone2"].ToString();
                txtFax.Text = dt.Tables[0].Rows[0]["Fax"].ToString();
                txtProvince.Text = dt.Tables[0].Rows[0]["Province"].ToString();
                txtVatNo.Text = dt.Tables[0].Rows[0]["VATNumber"].ToString();
                txtWebsite.Text = dt.Tables[0].Rows[0]["Website"].ToString();
                lblCustId.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                txtDiscount.Text = dt.Tables[0].Rows[0]["Discount"].ToString();
                LblCustName.Text = txtName1.Text;
                LblCustAddress.Text = txtAddress.Text;
                LblCity.Text = txtcity.Text;
                LblEmail.Text = txtEmail.Text;

                LblCustName1.Text = txtName1.Text; ;
                LblCustAddress1.Text = txtAddress.Text;
                LblCity1.Text = txtcity.Text;
                LblEmail1.Text = txtEmail.Text;

                LblCustName2.Text = txtName1.Text; ;
                LblCustAddress2.Text = txtAddress.Text;
                LblCity2.Text = txtcity.Text;
                LblEmail2.Text = txtEmail.Text;
            }
            con.Close();


        }
        protected void LinkButtonTemplate_Click(object sender, EventArgs e)
        {
            Global.FlagVis = true;

            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[2].Text;

            Session["quatdate"] = quatdate;
            BindGridView();
        }

        protected void DropDownUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            divBasicInfo2.Visible = true;

            if (DropDownUserList.Text != "-Vælg-")
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();

                SqlCommand com = new SqlCommand("select * from CUSTOMER where CustId =" + DropDownUserList.SelectedValue);
                com.Connection = con;
                SqlDataAdapter adpt = new SqlDataAdapter(com);
                DataSet dt = new DataSet();
                adpt.Fill(dt);

                if (dt.Tables[0].Rows.Count > 0)
                {
                    GridViewBasicInfo.DataSource = dt;
                    GridViewBasicInfo.DataBind();

                    //LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                    txtName1.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                    txtEmail.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();
                    txtAddress.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                    txtcity.Text = dt.Tables[0].Rows[0]["City"].ToString();
                    txtcountry.Text = dt.Tables[0].Rows[0]["Country"].ToString();
                    txtzip.Text = dt.Tables[0].Rows[0]["ZipCode"].ToString();
                    txtMobile.Text = dt.Tables[0].Rows[0]["Mobile"].ToString();
                    txtphone.Text = dt.Tables[0].Rows[0]["Phone2"].ToString();
                    txtFax.Text = dt.Tables[0].Rows[0]["Fax"].ToString();
                    txtProvince.Text = dt.Tables[0].Rows[0]["Province"].ToString();
                    txtVatNo.Text = dt.Tables[0].Rows[0]["VATNumber"].ToString();
                    txtWebsite.Text = dt.Tables[0].Rows[0]["Website"].ToString();
                    lblCustId.Text = dt.Tables[0].Rows[0]["CustId"].ToString();
                    txtDiscount.Text = dt.Tables[0].Rows[0]["Discount"].ToString();//Changes for Discount.//

                    LblCustName.Text = txtName1.Text;
                    LblCustAddress.Text = txtAddress.Text;
                    LblCity.Text = txtcity.Text;
                    LblEmail.Text = txtEmail.Text;

                    LblCustName1.Text = txtName1.Text; ;
                    LblCustAddress1.Text = txtAddress.Text;
                    LblCity1.Text = txtcity.Text;
                    LblEmail1.Text = txtEmail.Text;

                    LblCustName2.Text = txtName1.Text; ;
                    LblCustAddress2.Text = txtAddress.Text;
                    LblCity2.Text = txtcity.Text;
                    LblEmail2.Text = txtEmail.Text;
                }
                con.Close();
            }
            else
            {

            }
        }

        public void BindUserList()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from CUSTOMER ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {

                //GridViewCustDetails.DataSource = dt;
                //GridViewCustDetails.DataBind();

                DropDownUserList.DataSource = dt;
                DropDownUserList.DataTextField = "CustName";
                DropDownUserList.DataValueField = "CustId";
                DropDownUserList.DataBind();
                DropDownUserList.Items.Insert(0, ("-Vælg-"));

                cmd.Parameters.Clear();
                dt.Rows.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any Predifined Text !! ');", true);
            }
        }

        public void GetPredifinedText()//-------------Not In Use-------------------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select distinct * from TERMSAndCONDITION";

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GridViewPreText.DataSource = dt;
                GridViewPreText.DataBind();

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                dt.Rows.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any Predifined Text !! ');", true);
            }

            con.Close();

        }

        protected void ddlistCondition_SelectedIndexChanged2(object sender, EventArgs e)
        {
            // Global.fflag = 0;

            //if (ddlistCondition.SelectedValue != "")
            //{
            //    DivConditionHead.Visible = true;
            //    // ddlistCondition.Visible = true;

            //    SqlConnection con = new SqlConnection(cs);
            //    con.Open();
            //    SqlCommand com = new SqlCommand();
            //    com.Connection = con;

            //    com.CommandText = "select * from [Predifined-Text] where StantekstID= " + ddlistCondition.SelectedValue;
            //    SqlDataAdapter adpt = new SqlDataAdapter(com);
            //    DataTable dt = new DataTable();

            //    adpt.Fill(dt);

            //    if (dt.Rows.Count > 0)
            //    {
            //        lblID1.Text = dt.Rows[0]["StantekstID"].ToString();
            //        lblTermsAndCondition1.Text = dt.Rows[0]["Standardtekst"].ToString();

            //        com.CommandType = System.Data.CommandType.StoredProcedure;
            //        com.CommandText = "sp_InsertTempTemplateData";



            //        //com.Parameters.Add("@TERMSID", System.Data.SqlDbType.NVarChar).Value = lblID1.Text;
            //        // com.Parameters.Add("@TERMSANDCONDITION", System.Data.SqlDbType.NVarChar).Value = lblTermsAndCondition1.Text;

            //        com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = "";
            //        com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblTermsAndCondition1.Text;
            //        com.Parameters.Add("@PRICE", System.Data.SqlDbType.NVarChar).Value = "";
            //        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.NVarChar).Value = "";
            //        com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.NVarChar).Value = "";

            //        //com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
            //        //com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;

            //        com.ExecuteNonQuery();

            //        //GetPredifinedText();
            //        QuotationTemplateData();
            //    }
            //    com.Parameters.Clear();
            //    con.Close();

            //}
        }

        protected void ChkSelect_CheckedChanged(object sender, EventArgs e)//-------------Not In Use-------------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            foreach (GridViewRow item in GridViewPreText.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chk = (CheckBox)(item.Cells[0].FindControl("ChkSelect"));
                    if (chk.Checked)
                    {
                        Label lbTermsId = ((Label)item.Cells[1].FindControl("lblTermsId"));
                        Label lbConditionName = ((Label)item.Cells[2].FindControl("lblCondition"));

                        //lblTermsAndCondition1.Text = lbTermsId.ToString();

                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertTemplateData";
                        //com.CommandText = "sp_InsertTermsAndCondition";
                        //com.Parameters.Add("@TERMSANDCONDITION", System.Data.SqlDbType.VarChar).Value = lblTermsAndCondition1.Text;
                        com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = "";
                        com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lbConditionName.Text;
                        com.Parameters.Add("@PRICE", System.Data.SqlDbType.NVarChar).Value = "";
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.NVarChar).Value = "";
                        com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.NVarChar).Value = "";
                        com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                    }
                }
            }


            //dt.Rows.Clear();

            com.CommandType = System.Data.CommandType.Text;

            com.CommandText = "select Distinct * from TemplateData where TempDate = '" + datetime1.Text + "' ORDER BY TEMPID";
            SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            DataTable dt1 = new DataTable();
            adpt1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                HeaderTemplatee.Visible = true;
                GrdViewPrint.Visible = true;
                GrdViewPrint.DataSource = dt1;
                GrdViewPrint.DataBind();

                com.Parameters.Clear();
            }
            com.Parameters.Clear();
            //dt.Rows.Clear();

            con.Close();
        }

        protected void AddTerms_Click(object sender, EventArgs e)//-------------Not In Use------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            Button btn = (Button)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;


            //Label lbTermsId = ((Label)gvr.Cells[1].FindControl("lblTermsId"));
            //Label lbConditionName = ((Label)gvr.Cells[2].FindControl("lblCondition"));
            //TextBox txtCondition = ((TextBox)gvr.Cells[2].FindControl("txtCondition"));

            Label LBLID = ((Label)gvr.Cells[0].FindControl("lblID"));
            Label lblQuatationId = ((Label)gvr.Cells[0].FindControl("lblQuatationId"));
            Label lblItemCode = ((Label)gvr.Cells[1].FindControl("lblItemCode"));
            TextBox txtEmptyBox = ((TextBox)gvr.Cells[2].FindControl("txtEmptyBox"));
            Label lblItemName = ((Label)gvr.Cells[2].FindControl("lblItemName"));
            Label lblItemQua = ((Label)gvr.Cells[3].FindControl("lblItemQua"));
            TextBox txtItemprice = ((TextBox)gvr.Cells[4].FindControl("txtItemprice"));
            Label lblItemgtotal = ((Label)gvr.Cells[5].FindControl("lblItemgtotal"));


            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_InsertTemplateData";
            com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = lblItemCode.Text;
            //com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lbConditionName.Text;
            if (txtEmptyBox.Visible == false)
            {
                com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblItemName.Text;
            }
            else
            {
                com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = txtEmptyBox.Text;
            }
            com.Parameters.Add("@PRICE", System.Data.SqlDbType.NVarChar).Value = txtItemprice.Text;
            com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.NVarChar).Value = lblItemQua.Text;
            com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.NVarChar).Value = lblItemgtotal.Text;
            com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
            com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;

            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select Distinct * from TemplateData where TempDate = '" + datetime1.Text + "' ORDER BY TEMPID";
            SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            DataTable dt1 = new DataTable();
            adpt1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                GrdViewPrint.Visible = true;
                GrdViewPrint.DataSource = dt1;
                GrdViewPrint.DataBind();

                GrdViewPrint1.DataSource = dt1;
                GrdViewPrint1.DataBind();

                com.Parameters.Clear();
            }

            if (lblItemQua.Text != "" && lblTotalCost1.Text != "")
            {
                lblTotalCost1.Text = Convert.ToString((Convert.ToDecimal(lblTotalCost1.Text)) + (Convert.ToDecimal(lblItemgtotal.Text)));
                lblTotalCost3.Text = lblTotalCost1.Text;
                lblTotalCost22.Text = lblTotalCost1.Text;
            }
            else
            {
                lblTotalCost1.Text = lblItemgtotal.Text;
                lblTotalCost3.Text = lblTotalCost1.Text;
                lblTotalCost22.Text = lblTotalCost1.Text;
            }
            com.Parameters.Clear();
            con.Close();
        }

        protected void txtItemprice_TextChanged(object sender, EventArgs e)
        {

        }

        public void QuotationTemplateData()//-------------Not In Use-----------------------
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;

            com.CommandText = "select distinct * from TEMPTEMPLATEDATA  ORDER BY TEMPID";
            SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            DataTable dt1 = new DataTable();
            adpt1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                //HeaderTemplatee.Visible = true;
                GrdViewPrint.Visible = true;

                GridViewPreText.DataSource = dt1;
                GridViewPreText.DataBind();

                com.Parameters.Clear();
            }
            con.Close();
        }

        protected void BtnTxtItemPrice_Click(object sender, EventArgs e)//-------------Not In Use----------------
        {

        }

        protected void LinkEditRow_Click(object sender, EventArgs e)//-------------Not In Use----------------
        {

        }

        protected void SideBarList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Wizard1.ActiveStepIndex = e.Item.ItemIndex;
        }

        protected void BtnAddItemService_Click(object sender, EventArgs e)
        {
            if (lblCustId.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Select Proper Customer!! ');", true);
                return;
            }
            else
            {
                Global.fflag = 0;
                string ITEMQuantity = "0";

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;

                int id = 0;
                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "SELECT (ISNULL(MAX(OrderId),0) + 1) as OrderId FROM ORDERS";
                id = Convert.ToInt32(com.ExecuteScalar().ToString());
                com.Parameters.Clear();

                if (lblServiceItemId.Text != null || lblServiceItemPrice.Text != null)
                {
                    if (DropDownListServiceItem.SelectedValue == "-Vælg-")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Select Proper Service Item!! ');", true);
                        return;
                    }
                    else
                    {
                        com.CommandText = "select * from Stocksitem where ItemCode= " + DropDownListServiceItem.SelectedValue;
                        SqlDataAdapter adpt = new SqlDataAdapter(com);
                        DataTable dt = new DataTable();

                        adpt.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            lblServiceItemId.Text = dt.Rows[0]["ItemCode"].ToString();
                            lblServiceItemPrice.Text = dt.Rows[0]["Price"].ToString();
                            LblItemFees.Text = dt.Rows[0]["ItemFees"].ToString();
                            ITEMQuantity = dt.Rows[0]["Quantity"].ToString();
                        }
                        if (string.IsNullOrEmpty(ITEMQuantity))
                        {
                            ITEMQuantity = "0";
                        }
                        if (string.IsNullOrEmpty(lblServiceItemPrice.Text))
                        {
                            lblServiceItemPrice.Text = "0";
                        }
                        if (string.IsNullOrEmpty(LblItemFees.Text))
                        {
                            LblItemFees.Text = "0";
                        }

                        decimal Price = Convert.ToDecimal(lblServiceItemPrice.Text);
                        long qty = Convert.ToInt64(ITEMQuantity);
                        decimal total = 0;
                        decimal pricqty = 0;
                        pricqty = Price * qty;
                        total = total + pricqty;
                        lblTot1.Text = total.ToString();


                        if (LblTotalEarning.Text == "")
                        {
                            if (txtDiscount.Text != "")
                            {
                                FinalEarn = (Convert.ToDecimal(((Convert.ToDecimal(lblServiceItemPrice.Text)) - (Convert.ToDecimal(LblItemFees.Text))) * Convert.ToDecimal(ITEMQuantity)));
                                FinalEarn = FinalEarn - ((Convert.ToDecimal(txtDiscount.Text) * total) / 100);
                                LblTotalEarning.Text = Convert.ToString(Convert.ToDecimal(FinalEarn));
                            }
                            else
                            {
                                FinalEarn = (Convert.ToDecimal(((Convert.ToDecimal(lblServiceItemPrice.Text)) - (Convert.ToDecimal(LblItemFees.Text))) * Convert.ToDecimal(ITEMQuantity)));
                                LblTotalEarning.Text = Convert.ToString(Convert.ToDecimal(FinalEarn));
                            }
                        }
                        else
                        {
                            if (txtDiscount.Text != "")
                            {
                                FinalEarn = (Convert.ToDecimal(((Convert.ToDecimal(lblServiceItemPrice.Text)) - (Convert.ToDecimal(LblItemFees.Text))) * Convert.ToDecimal(ITEMQuantity)));
                                FinalEarn = FinalEarn - ((Convert.ToDecimal(txtDiscount.Text) * total) / 100);
                                LblTotalEarning.Text = Convert.ToString((Convert.ToDecimal(LblTotalEarning.Text)) + (Convert.ToDecimal(FinalEarn)));
                            }
                            else
                            {
                                FinalEarn = (Convert.ToDecimal(((Convert.ToDecimal(lblServiceItemPrice.Text)) - (Convert.ToDecimal(LblItemFees.Text))) * Convert.ToDecimal(ITEMQuantity)));
                                LblTotalEarning.Text = Convert.ToString((Convert.ToDecimal(LblTotalEarning.Text)) + (Convert.ToDecimal(FinalEarn)));
                            }
                        }

                        if (txtDiscount.Text != "")
                        {
                            total = total - ((Convert.ToDecimal(txtDiscount.Text) * total) / 100);
                            lblTot1.Text = total.ToString();
                        }
                        else
                        {
                            lblTot1.Text = total.ToString();
                        }

                        if (LBLResCost.Text == "")
                        {
                            LBLResCost.Text = lblTot1.Text;
                        }
                        else
                        {
                            LBLResCost.Text = (Convert.ToDecimal(LBLResCost.Text) + Convert.ToDecimal(lblTot1.Text)).ToString();
                        }
                        dt.Rows.Clear();
                        com.Parameters.Clear();
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertTemplateData";
                        com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = lblServiceItemId.Text;
                        com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = DropDownListServiceItem.SelectedItem.Text;
                        if (string.IsNullOrEmpty(lblServiceItemPrice.Text))
                        {
                            com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = lblServiceItemPrice.Text;
                        }
                        if (string.IsNullOrEmpty(ITEMQuantity.ToString()))
                        {
                            com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = ITEMQuantity.ToString();
                        }
                        com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblTot1.Text;
                        com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;

                        if (string.IsNullOrEmpty(LblItemFees.Text))
                        {
                            com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = LblItemFees.Text;
                        }
                        if (string.IsNullOrEmpty(lblServiceItemPrice.Text) || string.IsNullOrEmpty(ITEMQuantity.ToString()))
                        {
                            com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = FinalEarn.ToString();
                        }
                        com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;

                        com.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 1;

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                    }
                }
                if (datetime1.Text == txtDate.Text)
                {
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_InsertHAS";
                    com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                    com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblServiceItemId.Text;
                    com.Parameters.Add("@DATETIME", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                    com.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = ITEMQuantity;
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                    com.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                    com.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                    com.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = "";

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();
                }
                else
                {
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_InsertHAS";
                    com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                    com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblServiceItemId.Text;
                    com.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = "";
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                    com.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                    com.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = "";

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                }
                SqlCommand com1 = new SqlCommand();
                com1.Connection = con;
                com1.CommandType = System.Data.CommandType.Text;
                com1.CommandText = "select Distinct * from TemplateData where TempDate='" + datetime1.Text + "' AND TCType=0 ORDER BY TEMPID";

                SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
                DataTable dt1 = new DataTable();
                adpt1.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    HeaderTemplatee.Visible = true;
                    GrdViewPrint.Visible = true;
                    GrdViewPrint.DataSource = dt1;
                    GrdViewPrint.DataBind();

                    GrdViewPrint1.DataSource = dt1;
                    GrdViewPrint1.DataBind();

                    HeaderTemplatee1.Visible = true;

                    GridViewPreText.DataSource = dt1;
                    GridViewPreText.DataBind();

                    GrdViewPrint2.DataSource = dt1;
                    GrdViewPrint2.DataBind();

                    lblTotalCost1.Text = LBLResCost.Text;
                    TxtResCost.Text = lblTotalCost1.Text;
                    lblTotalCost3.Text = lblTotalCost1.Text;
                    lblTotalCost22.Text = lblTotalCost1.Text;

                    com1.Parameters.Clear();
                }
                con.Close();
            }
        }

        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {
            if (txtSearchBox.Text != "")
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();

                // String sql = "Select * from EMPLOYEE where FName Like @FNAME";

                //using (SqlCommand cmd = new SqlCommand(sql, con))
                //{
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.CommandText = "sp_SearchQuatation";

                //cmd.Parameters.Add("@CUSTNAME", System.Data.SqlDbType.VarChar, 255).Value = "%" + txtSearchBox.Text + "%";
                cmd.CommandText = "sp_SearchQuatationWithID";

                cmd.Parameters.Add("@QUOTID", System.Data.SqlDbType.BigInt).Value = txtSearchBox.Text;
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                DataSet dt1 = new DataSet();

                adpt.Fill(dt1);
                if (dt1.Tables[0].Rows.Count > 0)
                {
                    gridviewquatationView.DataSource = dt1;
                    gridviewquatationView.DataBind();
                }
                else
                {
                    gridviewquatationView.DataSource = dt1;
                    gridviewquatationView.DataBind();
                }
                //}
                //}
            }
            else
            {

            }
        }

        protected void GridViewPreText_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            float total1 = 0;
            float total11 = 0;

            foreach (GridViewRow rows in GridViewPreText.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.TextBox txtEmptyBox1 = (rows.Cells[2].FindControl("txtEmptyBox1") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblItemName1 = (rows.Cells[2].FindControl("lblItemName1") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemQua1 = (rows.Cells[3].FindControl("lblItemQua1") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtItemQuantity = (rows.Cells[3].FindControl("txtItemQuantity") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.TextBox txtItemprice1 = (rows.Cells[4].FindControl("txtItemprice1") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.Label lblItemgtotal1 = (rows.Cells[5].FindControl("lblItemgtotal1") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemFees1 = (rows.Cells[6].FindControl("lblItemFees1") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblTotalEarn1 = (rows.Cells[7].FindControl("lblTotalEarn1") as System.Web.UI.WebControls.Label);

                    //if (txtItemQuantity.Text == "0.00" || txtItemQuantity.Text == "0,00" || txtItemQuantity.Text == "")
                    if (txtItemQuantity.Text == "")
                    {
                        txtItemQuantity.Visible = false;
                        lblItemQua1.Visible = true;
                    }
                    else
                    {
                        txtItemQuantity.Visible = true;
                        lblItemQua1.Visible = false;
                    }
                    if (txtItemprice1.Text == "")
                    {
                        txtItemprice1.Visible = false;
                        txtEmptyBox1.Visible = true;
                        lblItemName1.Visible = false;
                    }
                    else
                    {
                        txtItemprice1.Visible = true;
                        txtEmptyBox1.Visible = false;
                        lblItemName1.Visible = true;
                    }
                    if (Flag == true)
                    {
                        if (lblItemFees1.Text != "" || lblTotalEarn1.Text != "")
                        {
                            //rows.BackColor = System.Drawing.Color.LightGoldenrodYellow;

                            float rowValue = 0;
                            if (float.TryParse(lblTotalEarn1.Text.Trim(), out rowValue))
                                total1 += rowValue;
                            LblTotalEarning.Text = total1.ToString();

                            float Rowvalue = 0;
                            if (float.TryParse(lblItemgtotal1.Text.Trim(), out Rowvalue))
                                total11 += Rowvalue;

                            lblTotalCost1.Text = total11.ToString();
                            lblTotalCost3.Text = lblTotalCost1.Text;
                            TxtResCost.Text = lblTotalCost1.Text;

                            lblTotalCost22.Text = lblTotalCost1.Text;
                            LBLResCost.Text = lblTotalCost1.Text;
                        }

                    }
                }
            }
        }

        protected void txtItemprice1_TextChanged(object sender, EventArgs e)
        {

        }

        public void BindTemplateData()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand com1 = new SqlCommand();
            com1.Connection = con;
            com1.CommandType = System.Data.CommandType.Text;
            com1.CommandText = "select Distinct * from TemplateData where TempDate='" + datetime1.Text + "' AND TCType=0 ORDER BY TEMPID";//AND QuaDate='" + txtDate.Text + "'OR QuaDate='" + txtDate.Text + "'

            SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
            DataTable dt1 = new DataTable();
            adpt1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                HeaderTemplatee.Visible = true;
                GrdViewPrint.Visible = true;
                GrdViewPrint.DataSource = dt1;
                GrdViewPrint.DataBind();

                GrdViewPrint1.DataSource = dt1;
                GrdViewPrint1.DataBind();

                GrdViewPrint2.DataSource = dt1;
                GrdViewPrint2.DataBind();

                //lblTotalCost1.Text = LBLResCost.Text;
                //TxtResCost.Text = lblTotalCost1.Text;

                com1.Parameters.Clear();
            }
            else
            {

            }
        }

        protected void txtEmptyBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void BtnForTextChange_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            con.Open();

            Global.subValues = 0;
            // decimal FinalEarn = 0;
            decimal TOTAL = 0;
            int id = 0;
            decimal TOTAL1 = 0;

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT (ISNULL(MAX(OrderId),0) + 1) as OrderId FROM ORDERS";
            id = Convert.ToInt32(com.ExecuteScalar().ToString());
            com.Parameters.Clear();

            Button btn = (Button)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            Label LBLID1 = ((Label)gvr.Cells[0].FindControl("LBLID1"));
            Label lblQuatationId1 = ((Label)gvr.Cells[0].FindControl("lblQuatationId1"));
            Label lblItemCode1 = ((Label)gvr.Cells[1].FindControl("lblItemCode1"));
            TextBox txtEmptyBox1 = ((TextBox)gvr.Cells[2].FindControl("txtEmptyBox1"));
            Label lblItemName1 = ((Label)gvr.Cells[2].FindControl("lblItemName1"));
            Label lblItemQua1 = ((Label)gvr.Cells[3].FindControl("lblItemQua1"));
            TextBox txtItemQuantity = ((TextBox)gvr.Cells[3].FindControl("txtItemQuantity"));
            TextBox txtItemprice1 = ((TextBox)gvr.Cells[4].FindControl("txtItemprice1"));
            Label lblItemgtotal1 = ((Label)gvr.Cells[5].FindControl("lblItemgtotal1"));
            Label lblItemFees1 = ((Label)gvr.Cells[6].FindControl("lblItemFees1"));
            Label lblTotalEarn1 = ((Label)gvr.Cells[7].FindControl("lblTotalEarn1"));

            lblItemQua1.Text = txtItemQuantity.Text;
            LblSubid.Text = lblQuatationId1.Text;
            LblSubQuantity.Text = txtItemQuantity.Text;
            LblSubPrice.Text = lblItemgtotal1.Text;

            if (txtItemprice1.Text != "")
            {
                TOTAL = Convert.ToDecimal(lblItemgtotal1.Text);
                TOTAL1 = Convert.ToDecimal(lblItemgtotal1.Text);
                //id = Convert.ToInt32(LBLID1.Text);
                decimal Price = Convert.ToDecimal(txtItemprice1.Text);

                if (lblItemQua1.Text == "0.00")
                {
                    decimal qty = 0;
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    total = total + pricqty;
                    //lblTot1.Text = total.ToString();
                    lblItemgtotal1.Text = total.ToString("N2");
                }
                else
                {
                    if (lblItemQua1.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Enter Proper Values!! ');", true);
                        return;
                    }
                    decimal qty = Convert.ToDecimal(lblItemQua1.Text);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    total = total + pricqty;
                    if (txtDiscount.Text != "")
                    {
                        lblDiscount1.Text = (Convert.ToString(((Convert.ToDecimal(txtDiscount.Text)) * total) / 100));
                        total = total - ((Convert.ToDecimal(txtDiscount.Text)) * total) / 100;
                        //lblTot1.Text = total.ToString();
                        lblItemgtotal1.Text = total.ToString("N2");
                    }
                    else
                    {
                        lblItemgtotal1.Text = total.ToString("N2");
                    }
                }

                if (lblTotalCost1.Text != "")
                {
                    if (lblDeletedId.Text != lblQuatationId1.Text)
                    {
                        lblTotalCost1.Text = Convert.ToString(((Convert.ToDecimal(lblTotalCost1.Text)) - TOTAL) + (Convert.ToDecimal(lblItemgtotal1.Text)));

                        if (LblTotalEarning.Text == "")
                        {
                            lblTotalEarn1.Text = (Convert.ToString(((Convert.ToDecimal(txtItemprice1.Text)) - (Convert.ToDecimal(lblItemFees1.Text))) * Convert.ToDecimal(txtItemQuantity.Text)));
                            LblTotalEarning.Text = lblTotalEarn1.Text;
                        }
                        else
                        {
                            FinalEarn = ((Convert.ToDecimal((((Convert.ToDecimal(txtItemprice1.Text)) - (Convert.ToDecimal(lblItemFees1.Text))) * Convert.ToDecimal(txtItemQuantity.Text)))));
                            LblTotalEarning.Text = (Convert.ToString(((Convert.ToDecimal(LblTotalEarning.Text)) - (Convert.ToDecimal(lblTotalEarn1.Text))) + (Convert.ToDecimal(FinalEarn.ToString("N2")))));
                            lblTotalEarn1.Text = FinalEarn.ToString("N2");
                            if (txtDiscount.Text != "")
                            {
                                lblTotalEarn1.Text = Convert.ToString((Convert.ToDecimal(lblTotalEarn1.Text)) - (Convert.ToDecimal(lblDiscount1.Text)));
                                LblTotalEarning.Text = lblTotalEarn1.Text;
                            }
                            else
                            {

                            }
                        }

                        LBLResCost.Text = lblTotalCost1.Text;
                        TxtResCost.Text = lblTotalCost1.Text;
                        lblTotalCost3.Text = lblTotalCost1.Text;
                        lblTotalCost22.Text = lblTotalCost1.Text;

                        if (TOTAL.ToString() == TOTAL1.ToString())
                        {
                            if (lblLineEmptyTotal.Text != "")
                            {
                                //lblSubTotal.Text = lblLineEmptyTotal.Text;
                            }
                            else
                            {
                                lblSubTotal.Text = lblTotalCost1.Text;
                            }
                        }
                        else
                        {
                            lblSubTotal.Text = lblTotalCost1.Text;
                        }
                        foreach (GridViewRow rows in GridViewPreText.Rows)
                        {
                            if (Global.subValues == 0)
                            {
                                if (rows.RowType == DataControlRowType.DataRow)
                                {
                                    System.Web.UI.WebControls.Label lblQuatationId11 = (rows.Cells[0].FindControl("lblQuatationId1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemCode11 = (rows.Cells[1].FindControl("lblItemCode1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.TextBox txtEmptyBox11 = (rows.Cells[2].FindControl("txtEmptyBox1") as System.Web.UI.WebControls.TextBox);
                                    System.Web.UI.WebControls.Label lblItemName11 = (rows.Cells[2].FindControl("lblItemName1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemQua11 = (rows.Cells[3].FindControl("lblItemQua1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.TextBox txtItemQuantity1 = (rows.Cells[3].FindControl("txtItemQuantity") as System.Web.UI.WebControls.TextBox);
                                    System.Web.UI.WebControls.TextBox txtItemprice11 = (rows.Cells[4].FindControl("txtItemprice1") as System.Web.UI.WebControls.TextBox);
                                    System.Web.UI.WebControls.Label lblItemgtotal11 = (rows.Cells[5].FindControl("lblItemgtotal1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemFees11 = (rows.Cells[6].FindControl("lblItemFees1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblTotalEarn11 = (rows.Cells[5].FindControl("lblTotalEarn1") as System.Web.UI.WebControls.Label);

                                    if ((Convert.ToDecimal(LblSubid.Text)) <= (Convert.ToDecimal(lblQuatationId11.Text)))
                                    {
                                        if (txtEmptyBox11.Text == "")
                                        {
                                            lblItemgtotal11.Text = Convert.ToString((Convert.ToDecimal(lblItemgtotal11.Text)) - (Convert.ToDecimal(LblSubPrice.Text)) + (Convert.ToDecimal(lblItemgtotal1.Text)));

                                            com.Parameters.Clear();
                                            com.CommandType = System.Data.CommandType.Text;
                                            com.CommandText = " Update TemplateData SET  Price=@PRICE,TotalPrice=@TOTALPRICE Where TEMPID='" + lblQuatationId11.Text + "' and QuaDate='" + datetime1.Text + "'";
                                            if (string.IsNullOrEmpty(txtItemprice11.Text))
                                            {
                                                com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                                            }
                                            else
                                            {
                                                com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtItemprice11.Text;
                                            }

                                            com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblItemgtotal11.Text;
                                            com.ExecuteNonQuery();
                                            com.Parameters.Clear();

                                            lblSubTotal.Text = lblItemgtotal11.Text;

                                            Global.subValues = 1;
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (txtDiscount.Text != "")
                        {
                            lblTotalCost1.Text = Convert.ToString((Convert.ToDecimal(lblTotalCost1.Text)) + (Convert.ToDecimal(lblItemgtotal1.Text)));

                            FinalEarn = ((Convert.ToDecimal((((Convert.ToDecimal(txtItemprice1.Text)) - (Convert.ToDecimal(lblItemFees1.Text))) * Convert.ToDecimal(txtItemQuantity.Text)))));
                            FinalEarn = FinalEarn - (Convert.ToDecimal(lblDiscount1.Text));
                            LblTotalEarning.Text = Convert.ToString((Convert.ToDecimal(LblTotalEarning.Text)) + (Convert.ToDecimal(FinalEarn.ToString("N2"))));
                            LblTotalEarning.Text = Convert.ToString((Convert.ToDecimal(LblTotalEarning.Text)) - (Convert.ToDecimal(lblDiscount1.Text)));
                            lblTotalEarn1.Text = FinalEarn.ToString("N2");
                        }
                        else
                        {
                            lblTotalCost1.Text = Convert.ToString((Convert.ToDecimal(lblTotalCost1.Text)) + (Convert.ToDecimal(lblItemgtotal1.Text)));
                            FinalEarn = ((Convert.ToDecimal((((Convert.ToDecimal(txtItemprice1.Text)) - (Convert.ToDecimal(lblItemFees1.Text))) * Convert.ToDecimal(txtItemQuantity.Text)))));
                            LblTotalEarning.Text = Convert.ToString((Convert.ToDecimal(LblTotalEarning.Text)) + (Convert.ToDecimal(FinalEarn.ToString("N2"))));
                            lblTotalEarn1.Text = FinalEarn.ToString("N2");
                        }

                        LBLResCost.Text = lblTotalCost1.Text;
                        TxtResCost.Text = lblTotalCost1.Text;
                        lblTotalCost3.Text = lblTotalCost1.Text;
                        lblTotalCost22.Text = lblTotalCost1.Text;
                        //lblSubTotal.Text = lblTotalCost1.Text;
                    }
                }
                else
                {
                    lblTotalCost1.Text = lblItemgtotal1.Text;
                    lblTotalCost3.Text = lblTotalCost1.Text;
                    lblTotalCost22.Text = lblTotalCost1.Text;
                    LBLResCost.Text = lblTotalCost1.Text;
                    TxtResCost.Text = lblTotalCost1.Text;
                    lblSubTotal.Text = lblTotalCost1.Text;
                }
                if (lblDeletedId.Text != lblQuatationId1.Text)
                {
                    com.Parameters.Clear();

                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = " Update TemplateData SET Quantity=@QUANTITY, Price=@PRICE, TotalPrice=@TOTALPRICE, TotalEarn=@TOTALEARN Where ItemCode='" + lblItemCode1.Text + "' and QuaDate='" + datetime1.Text + "'";
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(txtItemQuantity.Text);
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(txtItemprice1.Text);
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(lblItemgtotal1.Text);
                    com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(lblTotalEarn1.Text);
                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = " Update HAS SET RespondQuantity=@QUANTITY  Where ItemCode='" + lblItemCode1.Text + "' and DateTime='" + datetime1.Text + "'";
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = Convert.ToDecimal(txtItemQuantity.Text);
                    com.ExecuteNonQuery();
                    com.Parameters.Clear();
                }
                else
                {
                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_InsertTemplateData";
                    com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = lblItemCode1.Text;
                    com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblItemName1.Text;
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtItemprice1.Text;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(lblItemQua1.Text);
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblItemgtotal1.Text;
                    com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = lblItemFees1.Text;
                    com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = lblTotalEarn1.Text;
                    com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_InsertHAS";
                    com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                    com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode1.Text;
                    com.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = Convert.ToDecimal(txtItemQuantity.Text);
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                    com.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                    com.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = "";

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    lblDeletedId.Text = "";
                }
                BindTemplateData();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Enter Proper Values!! ');", true);
                return;
            }
            con.Close();
        }

        protected void BtnQuantityChange_Click(object sender, EventArgs e)
        {
            Global.subValues = 0;
            decimal TOTAL = 0;
            int id = 0;
            decimal TOTAL1 = 0;
            // decimal FinalEarn = 0;

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT (ISNULL(MAX(OrderId),0) + 1) as OrderId FROM ORDERS";
            id = Convert.ToInt32(com.ExecuteScalar().ToString());
            com.Parameters.Clear();

            Button btn = (Button)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            Label LBLID1 = ((Label)gvr.Cells[0].FindControl("LBLID1"));
            Label lblQuatationId1 = ((Label)gvr.Cells[0].FindControl("lblQuatationId1"));
            Label lblItemCode1 = ((Label)gvr.Cells[1].FindControl("lblItemCode1"));
            TextBox txtEmptyBox1 = ((TextBox)gvr.Cells[2].FindControl("txtEmptyBox1"));
            Label lblItemName1 = ((Label)gvr.Cells[2].FindControl("lblItemName1"));
            Label lblItemQua1 = ((Label)gvr.Cells[3].FindControl("lblItemQua1"));
            TextBox txtItemQuantity = ((TextBox)gvr.Cells[4].FindControl("txtItemQuantity"));
            TextBox txtItemprice1 = ((TextBox)gvr.Cells[4].FindControl("txtItemprice1"));
            Label lblItemgtotal1 = ((Label)gvr.Cells[5].FindControl("lblItemgtotal1"));
            Label lblItemFees1 = ((Label)gvr.Cells[6].FindControl("lblItemFees1"));
            Label lblTotalEarn1 = ((Label)gvr.Cells[7].FindControl("lblTotalEarn1"));

            lblItemQua1.Text = txtItemQuantity.Text;
            LblSubid.Text = lblQuatationId1.Text;
            LblSubQuantity.Text = txtItemQuantity.Text;
            LblSubPrice.Text = lblItemgtotal1.Text;

            if (txtItemprice1.Text != "")
            {
                TOTAL = Convert.ToDecimal(lblItemgtotal1.Text);
                TOTAL1 = Convert.ToDecimal(lblItemgtotal1.Text);
                //id = Convert.ToInt32(LBLID1.Text);
                decimal Price = Convert.ToDecimal(txtItemprice1.Text);

                if (txtItemQuantity.Text == "0.00" || txtItemQuantity.Text == "0,00")
                {
                    decimal qty = 0;
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    total = total + pricqty;
                    //lblTot1.Text = total.ToString();
                    lblItemgtotal1.Text = total.ToString("N2");
                }
                else
                {
                    if (txtItemQuantity.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Enter Proper Values!! ');", true);
                        return;
                    }
                    decimal qty = Convert.ToDecimal(txtItemQuantity.Text);
                    decimal total = 0;
                    decimal pricqty = 0;
                    pricqty = Price * qty;
                    total = total + pricqty;

                    if (txtDiscount.Text != "")
                    {
                        lblDiscount.Text = (Convert.ToString(((Convert.ToDecimal(txtDiscount.Text)) * total) / 100));
                        total = total - ((Convert.ToDecimal(txtDiscount.Text)) * total) / 100;
                        lblItemgtotal1.Text = total.ToString("N2");
                    }
                    else
                    {
                        lblItemgtotal1.Text = total.ToString("N2");
                    }
                    //lblTot1.Text = total.ToString();

                }

                if (lblTotalCost1.Text != "")
                {
                    if (lblDeletedId.Text != lblQuatationId1.Text)
                    {
                        lblTotalCost1.Text = Convert.ToString(((Convert.ToDecimal(lblTotalCost1.Text)) - TOTAL) + (Convert.ToDecimal(lblItemgtotal1.Text)));

                        if (LblTotalEarning.Text == "")
                        {
                            lblTotalEarn1.Text = (Convert.ToString(((Convert.ToDecimal(txtItemprice1.Text)) - (Convert.ToDecimal(lblItemFees1.Text))) * Convert.ToDecimal(txtItemQuantity.Text)));
                            LblTotalEarning.Text = lblTotalEarn1.Text;
                        }
                        else
                        {
                            FinalEarn = ((Convert.ToDecimal((((Convert.ToDecimal(txtItemprice1.Text)) - (Convert.ToDecimal(lblItemFees1.Text))) * Convert.ToDecimal(txtItemQuantity.Text)))));
                            LblTotalEarning.Text = (Convert.ToString(((Convert.ToDecimal(LblTotalEarning.Text)) - (Convert.ToDecimal(lblTotalEarn1.Text))) + (Convert.ToDecimal(FinalEarn.ToString("N2")))));
                            lblTotalEarn1.Text = FinalEarn.ToString("N2");

                            if (txtDiscount.Text != "")
                            {
                                lblTotalEarn1.Text = Convert.ToString((Convert.ToDecimal(lblTotalEarn1.Text)) - (Convert.ToDecimal(lblDiscount.Text)));
                                LblTotalEarning.Text = lblTotalEarn1.Text;
                            }
                            else
                            {

                            }
                        }

                        lblTotalCost3.Text = lblTotalCost1.Text;
                        lblTotalCost22.Text = lblTotalCost1.Text;
                        LBLResCost.Text = lblTotalCost1.Text;
                        TxtResCost.Text = lblTotalCost1.Text;


                        foreach (GridViewRow rows in GridViewPreText.Rows)
                        {
                            if (Global.subValues == 0)
                            {
                                if (rows.RowType == DataControlRowType.DataRow)
                                {
                                    System.Web.UI.WebControls.Label lblQuatationId11 = (rows.Cells[0].FindControl("lblQuatationId1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemCode11 = (rows.Cells[1].FindControl("lblItemCode1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.TextBox txtEmptyBox11 = (rows.Cells[2].FindControl("txtEmptyBox1") as System.Web.UI.WebControls.TextBox);
                                    System.Web.UI.WebControls.Label lblItemName11 = (rows.Cells[2].FindControl("lblItemName1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemQua11 = (rows.Cells[3].FindControl("lblItemQua1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.TextBox txtItemQuantity1 = (rows.Cells[3].FindControl("txtItemQuantity") as System.Web.UI.WebControls.TextBox);
                                    System.Web.UI.WebControls.TextBox txtItemprice11 = (rows.Cells[4].FindControl("txtItemprice1") as System.Web.UI.WebControls.TextBox);
                                    System.Web.UI.WebControls.Label lblItemgtotal11 = (rows.Cells[5].FindControl("lblItemgtotal1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblItemFees11 = (rows.Cells[6].FindControl("lblItemFees1") as System.Web.UI.WebControls.Label);
                                    System.Web.UI.WebControls.Label lblTotalEarn11 = (rows.Cells[5].FindControl("lblTotalEarn1") as System.Web.UI.WebControls.Label);

                                    if ((Convert.ToDecimal(LblSubid.Text)) <= (Convert.ToDecimal(lblQuatationId11.Text)))
                                    {
                                        if (txtEmptyBox11.Text == "")
                                        {
                                            lblItemgtotal11.Text = Convert.ToString((Convert.ToDecimal(lblItemgtotal11.Text)) - (Convert.ToDecimal(LblSubPrice.Text)) + (Convert.ToDecimal(lblItemgtotal1.Text)));

                                            com.Parameters.Clear();
                                            com.CommandType = System.Data.CommandType.Text;
                                            com.CommandText = " Update TemplateData SET  Price=@PRICE,TotalPrice=@TOTALPRICE Where TEMPID='" + lblQuatationId11.Text + "' and QuaDate='" + datetime1.Text + "'";
                                            if (string.IsNullOrEmpty(txtItemprice11.Text))
                                            {
                                                com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                                            }
                                            else
                                            {
                                                com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtItemprice11.Text;
                                            }
                                            com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblItemgtotal11.Text;
                                            com.ExecuteNonQuery();
                                            com.Parameters.Clear();

                                            lblSubTotal.Text = lblItemgtotal11.Text;

                                            Global.subValues = 1;
                                        }
                                    }
                                    else
                                    {

                                    }

                                }
                            }
                        }
                        if (TOTAL.ToString() == TOTAL1.ToString())
                        {
                            if (lblLineEmptyTotal.Text != "")
                            {
                                //lblSubTotal.Text = lblLineEmptyTotal.Text;
                            }
                            else
                            {
                                lblSubTotal.Text = lblTotalCost1.Text;
                            }
                        }
                        else
                        {
                            lblSubTotal.Text = lblTotalCost1.Text;
                        }
                    }
                    else
                    {
                        lblTotalCost1.Text = Convert.ToString((Convert.ToDecimal(lblTotalCost1.Text)) + (Convert.ToDecimal(lblItemgtotal1.Text)));

                        FinalEarn = ((Convert.ToDecimal((((Convert.ToDecimal(txtItemprice1.Text)) - (Convert.ToDecimal(lblItemFees1.Text))) * Convert.ToDecimal(txtItemQuantity.Text)))));
                        LblTotalEarning.Text = Convert.ToString((Convert.ToDecimal(LblTotalEarning.Text)) + (Convert.ToDecimal(FinalEarn.ToString("N2"))));
                        lblTotalEarn1.Text = FinalEarn.ToString("N2");

                        lblTotalCost3.Text = lblTotalCost1.Text;
                        lblTotalCost22.Text = lblTotalCost1.Text;
                        LBLResCost.Text = lblTotalCost1.Text;
                        TxtResCost.Text = lblTotalCost1.Text;
                        //lblSubTotal.Text = lblTotalCost1.Text;
                    }
                }
                else
                {
                    lblTotalCost1.Text = lblItemgtotal1.Text;

                    lblTotalCost3.Text = lblTotalCost1.Text;
                    lblTotalCost22.Text = lblTotalCost1.Text;
                    LBLResCost.Text = lblTotalCost1.Text;
                    TxtResCost.Text = lblTotalCost1.Text;
                }

                if (lblDeletedId.Text != lblQuatationId1.Text)
                {
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = " Update TemplateData SET Quantity=@QUANTITY, Price=@PRICE, TotalPrice=@TOTALPRICE, TotalEarn=@TOTALEARN Where ItemCode='" + lblItemCode1.Text + "' and QuaDate='" + datetime1.Text + "'";
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(txtItemQuantity.Text);
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(txtItemprice1.Text);
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(lblItemgtotal1.Text);
                    com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(lblTotalEarn1.Text);
                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = " Update HAS SET RespondQuantity=@QUANTITY  Where ItemCode='" + lblItemCode1.Text + "' and DateTime='" + datetime1.Text + "'";
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = Convert.ToDecimal(txtItemQuantity.Text);
                    com.ExecuteNonQuery();
                    com.Parameters.Clear();
                }
                else
                {
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_InsertTemplateData";
                    com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = lblItemCode1.Text;
                    com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblItemName1.Text;
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = txtItemprice1.Text;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(txtItemQuantity.Text);
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblItemgtotal1.Text;
                    com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = lblItemFees1.Text;
                    com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = lblTotalEarn1.Text;
                    com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_InsertHAS";
                    com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                    com.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode1.Text;
                    com.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = Convert.ToDecimal(txtItemQuantity.Text);
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                    com.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                    com.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = "";

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    lblDeletedId.Text = "";
                }
                BindTemplateData();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Enter Proper Values!! ');", true);
                return;
            }
            con.Close();
        }

        protected void BtnEmptyTextboxChange_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            Button btn = (Button)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            Label LBLID1 = ((Label)gvr.Cells[0].FindControl("LBLID1"));
            Label lblQuatationId1 = ((Label)gvr.Cells[0].FindControl("lblQuatationId1"));
            Label lblItemCode1 = ((Label)gvr.Cells[1].FindControl("lblItemCode1"));
            TextBox txtEmptyBox1 = ((TextBox)gvr.Cells[2].FindControl("txtEmptyBox1"));
            Label lblItemName1 = ((Label)gvr.Cells[2].FindControl("lblItemName1"));
            Label lblItemQua1 = ((Label)gvr.Cells[3].FindControl("lblItemQua1"));
            TextBox txtItemprice1 = ((TextBox)gvr.Cells[4].FindControl("txtItemprice1"));
            Label lblItemgtotal1 = ((Label)gvr.Cells[5].FindControl("lblItemgtotal1"));

            //if (lblDeletedId.Text != lblQuatationId1.Text)
            //{
            SqlCommand com = new SqlCommand("Update TemplateData SET ItemName=@ITEMNAME Where TEMPID='" + lblQuatationId1.Text + "'", con);
            com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = txtEmptyBox1.Text;
            com.ExecuteNonQuery();
            com.Parameters.Clear();
            // }
            //else
            //{
            //    SqlCommand com = new SqlCommand();
            //    com.Connection = con;
            //    com.CommandType = System.Data.CommandType.StoredProcedure;
            //    com.CommandText = "sp_InsertTemplateData";
            //    com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = lblItemCode1.Text;
            //    com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = txtEmptyBox1.Text;
            //    com.Parameters.Add("@PRICE", System.Data.SqlDbType.NVarChar).Value = txtItemprice1.Text;
            //    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.NVarChar).Value = lblItemQua1.Text;
            //    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.NVarChar).Value = lblItemgtotal1.Text;
            //    com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
            //    com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
            //    com.ExecuteNonQuery();
            //    com.Parameters.Clear();
            //}
            BindTemplateData();

            con.Close();
        }

        public void GETQuoteID()
        {
            int QUOTEid = 0;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT (ISNULL(MAX(QUOTID),0) + 1) as QUOTID FROM REQUESTEDQUOTE";
            QUOTEid = Convert.ToInt32(com.ExecuteScalar().ToString());

            LBLQUoteIdForImage.Text = QUOTEid.ToString();

            com.Parameters.Clear();
        }

        protected void BtnTermsAnCondition_Click1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lblCustId.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Please Select Customer !! ');", true);
                return;
            }
            else
            {
                if (Global.fflag == 0)
                {
                    EmptyLine();
                }

                if (TxtTermsAnCondition.Text != "")
                {
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand com = new SqlCommand();
                    com.Connection = con;

                    com.CommandText = "select * from [Predifined-Text] where TextID= " + TxtTermsAnCondition.Text;

                    SqlDataAdapter adpt = new SqlDataAdapter(com);
                    DataTable dt = new DataTable();

                    adpt.Fill(dt);

                    com.Parameters.Clear();

                    if (dt.Rows.Count > 0)
                    {
                        lblID1.Text = dt.Rows[0]["StantekstID"].ToString();
                        lblTermsAndCondition1.Text = dt.Rows[0]["Standardtekst"].ToString();
                        ddlistCondition.SelectedValue = dt.Rows[0]["StantekstID"].ToString();

                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertTemplateData";
                        com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = "";
                        com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblTermsAndCondition1.Text;
                        com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;
                        com.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 0;

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Insert Proper Code!! ');", true);
                        return;
                    }
                    com.Parameters.Clear();
                    dt.Rows.Clear();

                    com.CommandType = System.Data.CommandType.Text;

                    com.CommandText = "select Distinct * from TemplateData where TempDate = '" + datetime1.Text + "' AND TCType=0 ORDER BY TEMPID";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                    DataTable dt1 = new DataTable();
                    adpt1.Fill(dt1);
                    if (dt1.Rows.Count > 0)
                    {
                        GrdViewPrint.Visible = true;
                        GrdViewPrint.DataSource = dt1;
                        GrdViewPrint.DataBind();

                        GrdViewPrint1.DataSource = dt1;
                        GrdViewPrint1.DataBind();

                        HeaderTemplatee1.Visible = true;
                        GrdViewPrint2.DataSource = dt1;
                        GrdViewPrint2.DataBind();

                        GridViewPreText.DataSource = dt1;
                        GridViewPreText.DataBind();
                    }
                    com.Parameters.Clear();
                    dt.Rows.Clear();

                    con.Close();
                }

            }
        }

        protected void BtnPrint_Click1(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>PrintGridDataAll();</script>");

            string script = "<script>PrintGridDataAll();</script>";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), script, false);
        }

        protected void Bound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "style.backgroundColor='LightBlue'");
            e.Row.Attributes.Add("onmouseout", "style.backgroundColor='#fbfbfb'");
        }

        protected void btnExtoPdf_Click1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            //com.CommandType = System.Data.CommandType.Text;
            //com.CommandText = "select ImageName from QUOTEImageDetails where QUOTEID ='" + LBLQUoteIdForImage.Text + "'";
            //SqlDataAdapter adpt = new SqlDataAdapter(com);
            //DataTable dt = new DataTable();
            //adpt.Fill(dt);

            //string imagename = dt.Rows[0][0].ToString();
            com.Parameters.Clear();

            string path = Server.MapPath("~/PDFDATA/");
            //string imagepath = Server.MapPath("~/image/");
            //string imagepath = Server.MapPath("\\image\\pmglogo1.jpg");

            string attachment = "attachment;filename=QuatationTemplate" + LBLQUoteIdForImage.Text + ".pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", attachment);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringWriter sw = new StringWriter();
            StringWriter sw1 = new StringWriter();
            StringWriter sw2 = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            HtmlTextWriter hw2 = new HtmlTextWriter(sw2);
            // GrdViewPrint1.ShowHeader = true;
            imguser.Visible = false;

            DivTot.RenderControl(hw);
            DivQuotation.RenderControl(hw1);
            DivQuotationpdf.RenderControl(hw2);

            StringReader sr = new StringReader(sw.ToString());
            StringReader sr1 = new StringReader(sw1.ToString());
            StringReader sr2 = new StringReader(sw2.ToString());
            Document pdfDoc = new Document(PageSize.A4, 40f, 40f, 105f, 60f);

            //Document pdfDoc = new Document(new RectangleReadOnly(862, 615), 0f, 0f, 1f, 1f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            FileStream file = new FileStream(path + "/QuatationTemplate" + LBLQUoteIdForImage.Text + ".pdf", FileMode.Create);

            //PdfWriter writer1 = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            PdfWriter writer1 = PdfWriter.GetInstance(pdfDoc, file);
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(imagepath);
            //gif.ScaleAbsolute(200f, 80f);

            writer1.PageEvent = new ITextEvent();
            writer.PageEvent = new ITextEvent();
            //gif.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;

            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "delete from PDFDATA where QUOTEID='" + LBLQUoteIdForImage.Text + "'AND QuoteDate='" + datetime1.Text + "'";
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "insert into PDFDATA (QUOTEID,QuoteDate,FIleName,FilePath) values(@QUOTEID,@QDATE,@NAME,@PATH)";
            com.Parameters.AddWithValue("@QUOTEID", LBLQUoteIdForImage.Text);
            com.Parameters.AddWithValue("@QDATE", datetime1.Text);
            com.Parameters.AddWithValue("@NAME", System.Data.SqlDbType.NVarChar).Value = "QuatationTemplate" + LBLQUoteIdForImage.Text + ".pdf";
            com.Parameters.AddWithValue("@PATH", System.Data.SqlDbType.NVarChar).Value = (path + "/QuatationTemplate" + LBLQUoteIdForImage.Text + ".pdf");
            com.ExecuteNonQuery();
            con.Close();

            //Create a table
            if (GrdViewPrint1.Rows.Count > 0)
            {
                PdfPTable table = new PdfPTable(GrdViewPrint1.Columns.Count);
                table.WidthPercentage = 100;

                Font NFont = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
                Font NFont1 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10);
                //table.Cellpadding = 5;
                //Set the column widths
                int[] widths = new int[GrdViewPrint1.Columns.Count];
                for (int x = 0; x < GrdViewPrint1.Columns.Count; x++)
                {
                    //Phrase phrs = new Phrase();
                    widths[x] = (int)GrdViewPrint1.Columns[x].ItemStyle.Width.Value;
                    string cellText = Server.HtmlDecode(GrdViewPrint1.HeaderRow.Cells[x].Text);
                    //phrs.Add(new Chunk(cellText));
                    PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont));
                    //cell.BorderColor = BaseColor.WHITE;
                    cell.Border = 0;
                    cell.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderWidthTop = 1f;
                    cell.BorderWidthBottom = 1f;
                    cell.PaddingBottom = 3;
                    //cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    table.AddCell(cell);
                }

                table.SetWidths(widths);
                //Transfer rows from GridView to table

                for (int i = 0; i < GrdViewPrint1.Rows.Count; i++)
                {
                    if (GrdViewPrint1.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        for (int j = 0; j < GrdViewPrint1.Columns.Count; j++)
                        {
                            //Phrase phrs = new Phrase();
                            string cellText = Server.HtmlDecode(GrdViewPrint1.Rows[i].Cells[j].Text);

                            //phrs.Add(new Chunk(cellText));
                            PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont1));
                            cell.BorderColor = BaseColor.WHITE;
                            //Set Color of Alternating row
                            //if (i % 2 != 0)
                            //{
                            //    cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#C2D69B"));
                            //}

                            table.AddCell(cell);
                        }
                    }
                }

                if (GridViewTC1.Rows.Count > 0)
                {
                    PdfPTable table1 = new PdfPTable(GridViewTC1.Columns.Count);
                    table1.WidthPercentage = 100;

                    Font NFont2 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
                    Font NFont3 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10);
                    //Set the column widths
                    int[] widths1 = new int[GridViewTC1.Columns.Count];
                    for (int x = 0; x < GridViewTC1.Columns.Count; x++)
                    {
                        //Phrase phrs = new Phrase();
                        widths1[x] = (int)GridViewTC1.Columns[x].ItemStyle.Width.Value;
                        string cellText = Server.HtmlDecode(GridViewTC1.HeaderRow.Cells[x].Text);
                        //phrs.Add(new Chunk(cellText));
                        PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont2));
                        //cell.BorderColor = BaseColor.WHITE;
                        cell.Border = 0;
                        cell.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                        cell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        cell.BorderWidthTop = 1f;
                        cell.BorderWidthBottom = 1f;
                        cell.PaddingBottom = 3;
                        //cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        //table1.AddCell(cell);
                    }

                    table1.SetWidths(widths1);
                    //Transfer rows from GridView to table

                    for (int i = 0; i < GridViewTC1.Rows.Count; i++)
                    {
                        if (GridViewTC1.Rows[i].RowType == DataControlRowType.DataRow)
                        {
                            for (int j = 0; j < GridViewTC1.Columns.Count; j++)
                            {
                                //Phrase phrs = new Phrase();
                                string cellText = Server.HtmlDecode(GridViewTC1.Rows[i].Cells[j].Text);

                                //phrs.Add(new Chunk(cellText));
                                PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont3));

                                cell.BorderColor = BaseColor.WHITE;
                                //Set Color of Alternating row
                                //if (i % 2 != 0)
                                //{
                                //    cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#C2D69B"));
                                //}

                                table1.AddCell(cell);
                            }
                        }
                    }

                    pdfDoc.Open();
                    // pdfDoc.Add(gif);
                    Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                    htmlparser.Parse(sr1);
                    pdfDoc.Add(p);
                    htmlparser.Parse(sr2);
                    //XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr2);
                    //pdfDoc.Add(new Phrase("TILBUD"));
                    Phrase ph1 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(ph1);
                    pdfDoc.Add(table);
                    pdfDoc.Add(p);
                    htmlparser.Parse(sr);
                    pdfDoc.Add(ph1);
                    pdfDoc.Add(table1);
                    pdfDoc.Close();
                    Response.Write(pdfDoc);
                    OpenPDF();
                    //Response.End();
                    GrdViewPrint1.AllowPaging = false;
                    //GridViewCust.DataBind();  
                }
                else
                {
                    pdfDoc.Open();
                    // pdfDoc.Add(gif);
                    Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                    htmlparser.Parse(sr1);
                    pdfDoc.Add(p);
                    htmlparser.Parse(sr2);
                    //pdfDoc.Add(new Phrase("TILBUD"));
                    Phrase ph1 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(ph1);
                    pdfDoc.Add(table);
                    pdfDoc.Add(p);
                    htmlparser.Parse(sr);
                    pdfDoc.Close();
                    Response.Write(pdfDoc);
                    OpenPDF();
                    //Response.End();
                    GrdViewPrint1.AllowPaging = false;
                    //GridViewCust.DataBind(); 
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('No data found!');", true);
            }


        }

        protected void btnADDC_Click(object sender, EventArgs e)
        {
            if (txtCText1.Text != "")
            {
                Label10.Text = txtCText1.Text;
                Label10.Visible = true;
                Label10.Font.Bold = true;
                Label14.Text = txtCText1.Text;
                Label14.Visible = true;
                Label14.Font.Bold = true;
                Label17.Text = txtCText1.Text;
                Label17.Visible = true;
                Label17.Font.Bold = true;

                // DivQuotationpdf.Visible = true;
            }
            if (txtCText2.Text != "")
            {
                Label11.Text = txtCText2.Text;
                Label11.Visible = true;
                Label15.Text = txtCText2.Text;
                Label15.Visible = true;
                Label18.Text = txtCText2.Text;
                Label18.Visible = true;

                //DivQuotationpdf.Visible = true;
            }
            if (txtCText3.Text != "")
            {
                Label13.Text = txtCText3.Text;
                Label13.Visible = true;
                Label13.Font.Bold = true;
                Label16.Text = txtCText3.Text;
                Label16.Visible = true;
                Label16.Font.Bold = true;
                Label19.Text = txtCText3.Text;
                Label19.Visible = true;
                Label19.Font.Bold = true;

                //DivQuotationpdf.Visible = true;
            }
        }

        protected void btnAddTD1_Click(object sender, EventArgs e)
        {
            //if (Global.fflag == 0)
            //{
            //    EmptyLine();
            //}

            if (ddlistCondition1.SelectedIndex > 0)
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;

                com.CommandText = "select * from [Predifined-Text] where StantekstID= " + ddlistCondition1.SelectedValue;

                SqlDataAdapter adpt = new SqlDataAdapter(com);
                DataTable dt = new DataTable();

                adpt.Fill(dt);

                com.Parameters.Clear();

                if (dt.Rows.Count > 0)
                {
                    lblID11.Text = dt.Rows[0]["StantekstID"].ToString();
                    lblTermsAndCondition11.Text = dt.Rows[0]["Standardtekst"].ToString();
                    //(System.Data.SqlDbType.BigInt) sasa = dt.Rows[0]["StantekstID"].ToString();

                    //Label lblCondition = (Label)GridVConDition.FindControl("lblCondition");
                    //if (lblCondition == null)
                    //dt.Rows.Clear();

                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    //com.CommandText = "sp_InsertTempTemplateData";
                    com.CommandText = "sp_InsertTemplateData";
                    //com.CommandText = "sp_InsertTermsAndCondition";
                    //com.Parameters.Add("@TERMSANDCONDITION", System.Data.SqlDbType.VarChar).Value = lblTermsAndCondition1.Text;
                    com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = "";
                    com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblTermsAndCondition11.Text;
                    com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                    com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 1;
                    com.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 0;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    //BindTemplateData();
                    //QuotationTemplateData();
                }

                com.Parameters.Clear();
                dt.Rows.Clear();

                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "select Distinct * from TemplateData where TCTYPE=1 AND TempDate = '" + datetime1.Text + "' ORDER BY TEMPID";
                SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                DataTable dt1 = new DataTable();
                adpt1.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    //GrdViewPrint2.Visible = true;
                    //GrdViewPrint2.DataSource = dt1;
                    //GrdViewPrint2.DataBind();


                    //GrdViewPrint.DataSource = dt1;
                    //GrdViewPrint.DataBind();

                    //GrdViewPrint1.DataSource = dt1;
                    //GrdViewPrint1.DataBind();

                    //GridViewPreText.DataSource = dt1;
                    //GridViewPreText.DataBind();

                    GridViewTC.DataSource = dt1;
                    GridViewTC.DataBind();

                    GridViewTC1.DataSource = dt1;
                    GridViewTC1.DataBind();
                }
                com.Parameters.Clear();
                dt.Rows.Clear();

                con.Close();
            }
        }

        public void BindTCGrid(string TempDatee)
        {
            //SqlConnection con = new SqlConnection(cs);
            //con.Open();
            //SqlCommand com = new SqlCommand();
            //com.Connection = con;

            //com.CommandType = System.Data.CommandType.StoredProcedure;
            //// com.CommandText = "sp_getTCTemplateData";
            //com.CommandText = "sp_getTCTQuotationData";
            //com.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = TempDatee;//07/04/2016 10:04:04
            //com.ExecuteNonQuery();

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getTCTQuotationData";
            //com.CommandText = "sp_getTemplateData";
            com.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = TempDatee;//.Text;
            com.Parameters.Add("@Flag", System.Data.SqlDbType.Bit).Value = Flag;
            com.ExecuteNonQuery();

            //SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            //DataSet dt1 = new DataSet();
            //adpt1.Fill(dt1);

            //if (dt1.Tables[0].Rows.Count > 0)
            //{
            //    com.Parameters.Clear();
            //    com.CommandType = System.Data.CommandType.StoredProcedure;

            //    com.CommandText = "sp_InsertTemplateData";
            //    com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["ItemCode"].ToString();
            //    com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["ItemName"].ToString();
            //    com.Parameters.Add("@PRICE", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["Price"].ToString();
            //    com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["Quantity"].ToString();
            //    com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["TotalPrice"].ToString();
            //    com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["TempDate"].ToString();
            //    com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["QuaDate"].ToString();
            //    com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["ItemFees"].ToString();
            //    com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.NVarChar).Value = dt1.Tables[0].Rows[0]["TotalEarn"].ToString();
            //    com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 1;

            //    com.ExecuteNonQuery();
            //    com.Parameters.Clear();
            //}


            com.Parameters.Clear();
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getTCTemplateData";
            com.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = TempDatee;//.Text;
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                HeaderTemplatee1.Visible = true;
                GridViewTC.DataSource = dt;
                GridViewTC.DataBind();

                GridViewTC1.DataSource = dt;
                GridViewTC1.DataBind();

            }
            com.Parameters.Clear();
            dt.Rows.Clear();

            con.Close();
        }

        protected void BtnTermsAnCondition1_Click(object sender, EventArgs e)
        {
            //if (Global.fflag == 0)
            //{
            //    EmptyLine();
            //}
            if (string.IsNullOrEmpty(lblCustId.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Please Select Customer !! ');", true);
                return;
            }
            else
            {

                if (TxtTermsAnCondition1.Text != "")
                {
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand com = new SqlCommand();
                    com.Connection = con;

                    com.CommandText = "select * from [Predifined-Text] where TextID= " + TxtTermsAnCondition1.Text;

                    SqlDataAdapter adpt = new SqlDataAdapter(com);
                    DataTable dt = new DataTable();

                    adpt.Fill(dt);

                    com.Parameters.Clear();

                    if (dt.Rows.Count > 0)
                    {
                        lblID1.Text = dt.Rows[0]["StantekstID"].ToString();
                        lblTermsAndCondition1.Text = dt.Rows[0]["Standardtekst"].ToString();
                        ddlistCondition1.SelectedValue = dt.Rows[0]["StantekstID"].ToString();

                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertTemplateData";
                        com.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = DBNull.Value;
                        com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblTermsAndCondition1.Text;
                        com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        com.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        com.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        com.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 1;
                        com.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 0;

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Insert Proper Code!! ');", true);
                        return;
                    }
                    com.Parameters.Clear();
                    dt.Rows.Clear();

                    com.CommandType = System.Data.CommandType.Text;

                    com.CommandText = "select Distinct * from TemplateData where TempDate = '" + datetime1.Text + "' AND TCType=1 ORDER BY TEMPID";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                    DataTable dt1 = new DataTable();
                    adpt1.Fill(dt1);
                    if (dt1.Rows.Count > 0)
                    {
                        //GrdViewPrint.Visible = true;
                        //GrdViewPrint.DataSource = dt1;
                        //GrdViewPrint.DataBind();

                        //GrdViewPrint1.DataSource = dt1;
                        //GrdViewPrint1.DataBind();

                        //HeaderTemplatee1.Visible = true;
                        //GrdViewPrint2.DataSource = dt1;
                        //GrdViewPrint2.DataBind();

                        //GridViewPreText.DataSource = dt1;
                        //GridViewPreText.DataBind();

                        GridViewTC.DataSource = dt1;
                        GridViewTC.DataBind();

                        GridViewTC1.DataSource = dt1;
                        GridViewTC1.DataBind();
                    }
                    com.Parameters.Clear();
                    dt.Rows.Clear();

                    con.Close();
                }
            }
        }

        protected void BindTemplateDropDown()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("Select Distinct QuoteID from QUOTATIONDATA Where  TEMPType='" + 1 + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                ddlistTemplate.DataSource = dt;
                ddlistTemplate.DataTextField = "QuoteID";
                ddlistTemplate.DataValueField = "QuoteID";
                ddlistTemplate.DataBind();
                ddlistTemplate.Items.Insert(0, ("-Vælg-"));

                cmd.Parameters.Clear();
                dt.Rows.Clear();
            }
            else
            {
                ddlistTemplate.Items.Insert(0, ("-Vælg-"));
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any Template !! ');", true);
            }
        }

        protected void BtnTemplate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lblCustId.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Please Select Customer !! ');", true);
                return;
            }
            else
            {

                Flag = true;

                if (Global.fflag == 0)
                {
                    EmptyLine();
                }

                if (txtTemplate.Text != "")
                {
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand com = new SqlCommand();
                    com.Connection = con;

                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "Select Distinct QuaDate from QUOTATIONDATA Where QuoteID=@I_QuoteID AND TEMPType='" + 1 + "'";
                    com.Parameters.Add("@I_QuoteID", System.Data.SqlDbType.BigInt).Value = txtTemplate.Text;

                    SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                    DataTable dt1 = new DataTable();
                    adpt1.Fill(dt1);
                    if (dt1.Rows.Count > 0)
                    {
                        QuoteDate = dt1.Rows[0]["QuaDate"].ToString();

                        com.Parameters.Clear();
                        com.CommandType = System.Data.CommandType.Text;
                        com.CommandText = "Delete from TemplateData";
                        com.ExecuteNonQuery();
                        com.Parameters.Clear();

                        //Use For Deleting Unwanted Entry Record From HAS Table-----------

                        com.CommandType = System.Data.CommandType.Text;
                        com.CommandText = "Delete from HAS Where DateTime='" + datetime1.Text + "'";
                        com.ExecuteNonQuery();
                        com.Parameters.Clear();

                        BindTemGrid();
                        BindTCTTempGrid();
                        UpdateTemplateData();
                        TemplateDataBoundForHas();
                    }
                    else
                    {
                        headerdiv1.Visible = false;
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any Template on This Code !! ');", true);
                    }
                    com.Parameters.Clear();
                    con.Close();

                }
                Flag = false;
            }
        }

        public void BindTemGrid()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getQuotationData";

            com.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = QuoteDate;//.Text;
            com.Parameters.Add("@Flag", System.Data.SqlDbType.Bit).Value = Flag;

            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select Distinct * from TemplateData where TempDate = '" + QuoteDate + "' AND TCType=0 ORDER BY TEMPID";

            SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            DataTable dt1 = new DataTable();
            adpt1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                GrdViewPrint.Visible = true;
                GrdViewPrint.DataSource = dt1;
                GrdViewPrint.DataBind();

                headerdiv1.Visible = true;
                GrdViewPrint1.DataSource = dt1;
                GrdViewPrint1.DataBind();

                HeaderTemplatee1.Visible = true;
                GrdViewPrint2.DataSource = dt1;
                GrdViewPrint2.DataBind();

                GridViewPreText.DataSource = dt1;
                GridViewPreText.DataBind();
            }
            com.Parameters.Clear();
            dt1.Rows.Clear();

            con.Close();

        }

        public void BindTCTTempGrid()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_getTCTQuotationData";

            com.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = QuoteDate;//.Text;
            com.Parameters.Add("@Flag", System.Data.SqlDbType.Bit).Value = Flag;

            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select Distinct * from TemplateData where TempDate = '" + QuoteDate + "' AND TCType=1 ORDER BY TEMPID";

            SqlDataAdapter adpt1 = new SqlDataAdapter(com);
            DataTable dt1 = new DataTable();
            adpt1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                HeaderTemplatee1.Visible = true;

                GridViewTC.DataSource = dt1;
                GridViewTC.DataBind();

                GridViewTC1.DataSource = dt1;
                GridViewTC1.DataBind();
            }
            com.Parameters.Clear();
            dt1.Rows.Clear();

            con.Close();
        }

        public void UpdateTemplateData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = " Update TemplateData SET TempDate=@I_TempDate, QuaDate=@I_QuaDate Where QuaDate='" + QuoteDate + "'";

            com.Parameters.Add("@I_TempDate", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
            com.Parameters.Add("@I_QuaDate", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;

            com.ExecuteNonQuery();
            com.Parameters.Clear();
            con.Close();
        }

        public void TemplateDataBoundForHas()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            int id = 0;

            cmd.CommandText = "SELECT (ISNULL(MAX(OrderId),0) + 1) as OrderId FROM ORDERS";
            id = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            cmd.Parameters.Clear();

            foreach (GridViewRow rows in GrdViewPrint.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblQuatationId = (rows.Cells[0].FindControl("lblQuatationId") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[1].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemName = (rows.Cells[2].FindControl("lblItemName") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemQua = (rows.Cells[3].FindControl("lblItemQua") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label LabelItemFees = (rows.Cells[4].FindControl("LabelItemFees") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemPrice = (rows.Cells[4].FindControl("lblItemPrice") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblEarning = (rows.Cells[5].FindControl("lblEarning") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblItemgtotal = (rows.Cells[5].FindControl("lblItemgtotal") as System.Web.UI.WebControls.Label);

                    if (lblItemCode.Text != "")
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_InsertHAS";
                        cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                        cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                        cmd.Parameters.Add("@DATETIME", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                        cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;
                        cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = lblItemQua.Text;
                        cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                        cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                        cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                        cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = "";

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
            }
            con.Close();
        }

        protected void BtnSaveASTemplate_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            //cmd.Parameters.Clear();
            //cmd.CommandType = System.Data.CommandType.Text;
            //cmd.CommandText = "Delete From QUOTATIONDATA Where QuaDate=@I_QUATDATE AND TEMPType='" + 1 + "'";
            //cmd.Parameters.Add("@I_QUATDATE", System.Data.SqlDbType.NVarChar).Value = QuoteDate;
            //cmd.ExecuteNonQuery();
            //cmd.Parameters.Clear();
            if (string.IsNullOrEmpty(lblCustId.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Select Proper Customer!! ');", true);
                return;
            }
            else
            {
                int QuoteDataID = 0;
                cmd.CommandText = "SELECT (ISNULL(MAX(QuoteID),0) + 1) as QuoteID FROM QUOTATIONDATA";
                QuoteDataID = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                cmd.Parameters.Clear();

                foreach (GridViewRow rows in GrdViewPrint.Rows)
                {
                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.Label lblQuatationId = (rows.Cells[0].FindControl("lblQuatationId") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblItemCode = (rows.Cells[1].FindControl("lblItemCode") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblItemName = (rows.Cells[2].FindControl("lblItemName") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblItemQua = (rows.Cells[3].FindControl("lblItemQua") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label LabelItemFees = (rows.Cells[4].FindControl("LabelItemFees") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblItemPrice = (rows.Cells[4].FindControl("lblItemPrice") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblEarning = (rows.Cells[5].FindControl("lblEarning") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblItemgtotal = (rows.Cells[5].FindControl("lblItemgtotal") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblServiceType = (rows.Cells[5].FindControl("lblServiceType") as System.Web.UI.WebControls.Label);

                        //if (lblItemQua.Text != "")
                        //{
                        //    if (datetime1.Text == txtDate.Text)
                        //    {
                        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        //        cmd.CommandText = "sp_UpdateHAS";
                        //        cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                        //        cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                        //        cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = txtDate.Text;
                        //        cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = id;//lbllDatetime.Text;
                        //        cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = lblItemQua.Text;
                        //        cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                        //        cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                        //        cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        //        cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = LblTotalEarning.Text;

                        //        cmd.ExecuteNonQuery();
                        //        cmd.Parameters.Clear();
                        //    }
                        //    else
                        //    {
                        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        //        cmd.CommandText = "sp_UpdateHAS";
                        //        cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustId.Text;
                        //        cmd.Parameters.Add("@ITEMCODE", System.Data.SqlDbType.BigInt).Value = lblItemCode.Text;
                        //        cmd.Parameters.Add("@DATETIME ", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        //        cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = lbllDatetime.Text;
                        //        cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = lblItemQua.Text;
                        //        cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = ddquotestatus.SelectedValue;
                        //        cmd.Parameters.Add("@FOLLOWUPDATE", System.Data.SqlDbType.NVarChar).Value = "";
                        //        cmd.Parameters.Add("@TEMDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        //        cmd.Parameters.Add("@QUOTEEARN", System.Data.SqlDbType.NVarChar).Value = LblTotalEarning.Text;

                        //        cmd.ExecuteNonQuery();
                        //        cmd.Parameters.Clear();
                        //    }
                        //}

                        cmd.Parameters.Clear();
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_InsertQuotationData";

                        cmd.Parameters.Add("@I_QUOTEID", System.Data.SqlDbType.BigInt).Value = QuoteDataID;
                        // cmd.Parameters.Add("@I_TEMPID", System.Data.SqlDbType.BigInt).Value = lblQuatationId.Text;
                        if (string.IsNullOrEmpty(lblItemCode.Text))
                        {
                            cmd.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = lblItemCode.Text;
                        }
                        cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblItemName.Text;

                        if (string.IsNullOrEmpty(lblItemPrice.Text))
                        {
                            cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = lblItemPrice.Text;
                        }
                        //cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = lblItemPrice.Text;

                        if (string.IsNullOrEmpty(lblItemQua.Text))
                        {
                            cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = lblItemQua.Text;
                        }
                        //cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = lblItemQua.Text;
                        if (string.IsNullOrEmpty(lblItemgtotal.Text))
                        {
                            cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblItemgtotal.Text;
                        }
                        //cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = lblItemgtotal.Text;
                        cmd.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        cmd.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        if (string.IsNullOrEmpty(LabelItemFees.Text))
                        {
                            cmd.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = LabelItemFees.Text;
                        }
                        //cmd.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = LabelItemFees.Text;
                        if (string.IsNullOrEmpty(lblEarning.Text))
                        {
                            cmd.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = lblEarning.Text;
                        }
                        //cmd.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = lblEarning.Text;
                        cmd.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 0;
                        cmd.Parameters.Add("@TEMPTYPE", System.Data.SqlDbType.Bit).Value = 1;
                        cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = lblServiceType.Text;


                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                    }
                }

                foreach (GridViewRow rows in GridViewTC.Rows)
                {
                    if (rows.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.Label lblTermNcondition = (rows.Cells[0].FindControl("lblTermNcondition") as System.Web.UI.WebControls.Label);
                        System.Web.UI.WebControls.Label lblQuatationId = (rows.Cells[0].FindControl("lblQuatationId") as System.Web.UI.WebControls.Label);

                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_InsertQuotationData";
                        cmd.Parameters.Add("@I_QUOTEID", System.Data.SqlDbType.BigInt).Value = QuoteDataID;
                        // cmd.Parameters.Add("@I_TEMPID", System.Data.SqlDbType.BigInt).Value = lblQuatationId.Text;
                        cmd.Parameters.Add("@I_ITEMCODE", System.Data.SqlDbType.NVarChar).Value = DBNull.Value;
                        cmd.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.NVarChar).Value = lblTermNcondition.Text;
                        cmd.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        cmd.Parameters.Add("@QUANTITY", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        cmd.Parameters.Add("@TOTALPRICE", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        cmd.Parameters.Add("@TEMPDATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        cmd.Parameters.Add("@QUODATE", System.Data.SqlDbType.NVarChar).Value = datetime1.Text;
                        cmd.Parameters.Add("@ITEMFEES", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        cmd.Parameters.Add("@TOTALEARN", System.Data.SqlDbType.Decimal).Value = DBNull.Value;
                        cmd.Parameters.Add("@TCTYPE", System.Data.SqlDbType.Bit).Value = 1;
                        cmd.Parameters.Add("@TEMPTYPE", System.Data.SqlDbType.Bit).Value = 1;
                        cmd.Parameters.Add("@SERVICETYPE", System.Data.SqlDbType.Bit).Value = 0;

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
                BtnSaveASTemplate.Visible = false;
                con.Close();
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Template Created !! ');", true);
                return;
            }
        }

        protected void BtnADDTemplate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lblCustId.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Please Select Customer !! ');", true);
                return;
            }
            else
            {
                Flag = true;

                if (Global.fflag == 0)
                {
                    EmptyLine();
                }

                if (ddlistTemplate.SelectedIndex > 0)
                {
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    SqlCommand com = new SqlCommand();
                    com.Connection = con;

                    com.Parameters.Clear();
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "Select Distinct QuaDate from QUOTATIONDATA Where QuoteID=@I_QuoteID AND TEMPType='" + 1 + "'";
                    com.Parameters.Add("@I_QuoteID", System.Data.SqlDbType.BigInt).Value = ddlistTemplate.SelectedValue;

                    SqlDataAdapter adpt1 = new SqlDataAdapter(com);
                    DataTable dt1 = new DataTable();
                    adpt1.Fill(dt1);
                    if (dt1.Rows.Count > 0)
                    {
                        QuoteDate = dt1.Rows[0]["QuaDate"].ToString();

                        com.Parameters.Clear();
                        com.CommandType = System.Data.CommandType.Text;
                        com.CommandText = "Delete from TemplateData";
                        com.ExecuteNonQuery();
                        com.Parameters.Clear();

                        //Use For Deleting Unwanted Entry Record From HAS Table-----------

                        com.CommandType = System.Data.CommandType.Text;
                        com.CommandText = "Delete from HAS Where DateTime='" + datetime1.Text + "'";
                        com.ExecuteNonQuery();
                        com.Parameters.Clear();

                        BindTemGrid();
                        BindTCTTempGrid();
                        UpdateTemplateData();
                        TemplateDataBoundForHas();
                    }
                    else
                    {
                        headerdiv1.Visible = false;
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! No any Template on This Code !! ');", true);
                    }
                    com.Parameters.Clear();
                    con.Close();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!! Invalid Selection!! ');", true);
                    return;
                }
                Flag = false;
            }
        }


        //23_June----Prathamesh
        //Added Code for File Upload in Quatation.
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string filename;
            if (FileUpload2.HasFile)
            {
                FileUploadDiv11.Visible = true;
                filename = FileUpload2.FileName;

                string filePath = @"~\Quotation_Files\" + filename.Trim();
                string newfile = Server.MapPath(filePath);
                FileUpload2.SaveAs(newfile);

                string date;
                date = txtDate.Text;
                //DateTime date;
                //date = Convert.ToDateTime(txtDate.Text);
                date = date.Replace(":", "");
                date = date.Replace("/", "");
                date = date.Replace(" ", "");
                date = date.Replace("-", "");

                if (lblDateTime.Text != "")
                {
                    using (SqlConnection con = new SqlConnection(cs))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {

                            cmd.CommandText = "insert into QuotationFiles (Date,FileName,FilePath) values(@date,@Name,@FilePath)";

                            //cmd.Parameters.AddWithValue("@date", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal((lblDateTime.Text).ToString());
                            cmd.Parameters.AddWithValue("@date", System.Data.SqlDbType.NVarChar).Value = (lblDateTime.Text).ToString();

                            cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarChar).Value = filename;

                            cmd.Parameters.AddWithValue("@FilePath", System.Data.SqlDbType.VarChar).Value = newfile;

                            cmd.Connection = con;

                            con.Open();

                            cmd.ExecuteNonQuery();

                            con.Close();
                        }
                    }
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(cs))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandText = "insert into QuotationFiles (Date,FileName,FilePath) values(@date,@Name,@FilePath)";

                            //cmd.Parameters.AddWithValue("@date", System.Data.SqlDbType.Decimal).Value = Convert.ToDecimal(date.ToString());
                            cmd.Parameters.AddWithValue("@date", System.Data.SqlDbType.NVarChar).Value = date.ToString();

                            cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarChar).Value = filename;

                            cmd.Parameters.AddWithValue("@FilePath", System.Data.SqlDbType.VarChar).Value = newfile;

                            cmd.Connection = con;

                            con.Open();

                            cmd.ExecuteNonQuery();

                            con.Close();
                        }
                    }
                }
                gvFileUpload1();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select File To Upload');", true);
                return;
            }
        }

        private void gvFileUpload1()
        {
            string date;
            date = txtDate.Text;
            //DateTime date;
            //date = Convert.ToDateTime(txtDate.Text);
            date = date.Replace(":", "");
            date = date.Replace("/", "");
            date = date.Replace(" ", "");
            date = date.Replace("-", "");
            if (lblDateTime.Text != "")
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.CommandText = "select * from QuotationFiles where Date=" + lblDateTime.Text;

                        cmd.Connection = con;

                        con.Open();

                        gvFileUpload.DataSource = cmd.ExecuteReader();

                        gvFileUpload.DataBind();

                        con.Close();
                    }
                }
            }
            else
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.CommandText = "select * from QuotationFiles where Date=" + date;

                        cmd.Connection = con;

                        con.Open();

                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        adpt.Fill(dt);

                        gvFileUpload.DataSource = cmd.ExecuteReader();

                        gvFileUpload.DataBind();

                        con.Close();
                    }
                }
            }

        }

        protected void lbtnViewImage_Click(object sender, EventArgs e)
        {

            {
                GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;

                Label lblFilePath = (Label)grdrow.Cells[2].FindControl("lblFilePath");
                string FilePath = lblFilePath.Text;

                if (FilePath.Contains(".doc"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;


                }

                if (FilePath.Contains(".pdf"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }


                if (FilePath.Contains(".htm"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;

                }

                if (FilePath.Contains(".txt"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;

                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;


                }

                if (FilePath.Contains(".doc") || FilePath.Contains(".rtf") || FilePath.Contains(".html") || FilePath.Contains(".log") || FilePath.Contains(".jpeg") || FilePath.Contains(".tif") || FilePath.Contains(".tiff") || FilePath.Contains(".gif") || FilePath.Contains(".bmp") || FilePath.Contains(".asf"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }

                //view pdf files 
                if (FilePath.Contains(".pdf") || FilePath.Contains(".avi") || FilePath.Contains(".zip") || FilePath.Contains(".xls") || FilePath.Contains(".csv") || FilePath.Contains(".wav") || FilePath.Contains(".mp3"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }


                if (FilePath.Contains(".htm") || FilePath.Contains(".mpg") || FilePath.Contains(".mpeg") || FilePath.Contains(".fdf") || FilePath.Contains(".ppt") || FilePath.Contains(".dwg") || FilePath.Contains(".msg"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;
                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;

                }

                if (FilePath.Contains(".txt") || FilePath.Contains(".xml") || FilePath.Contains(".sdxl") || FilePath.Contains(".xdp") || FilePath.Contains(".jar"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Session["FILEPATH"] = FilePath;

                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }

                if (FilePath.Contains(".jpg"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Session["FILEPATH"] = FilePath;

                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }

                if (FilePath.Contains(".png"))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Session["FILEPATH"] = FilePath;

                    string url = "ViewFile.aspx";
                    string s = "window.open('" + url + "');";

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                    return;
                }
            }
        }

        //23_June----Prathamesh
        //Added new function GetFilesForQuote() to view files attached to quatation at the time of edit..
        //Function call in BindGridView() function.
        public void GetFilesForQuote()
        {

            String TempDatee = Session["quatdate"].ToString();

            TempDatee = TempDatee.Replace("/", "");
            TempDatee = TempDatee.Replace(":", "");
            TempDatee = TempDatee.Replace(" ", "");
            TempDatee = TempDatee.Replace("-", "");

            lblDateTime.Text = TempDatee;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "select * from QuotationFiles where Date=" + TempDatee;

                    cmd.Connection = con;

                    con.Open();

                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adpt.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        FileUploadDiv11.Visible = true;
                    }
                    else
                    {
                        FileUploadDiv11.Visible = false;
                    }

                    gvFileUpload.DataSource = cmd.ExecuteReader();

                    gvFileUpload.DataBind();

                    con.Close();
                }
            }
        }
    }
}



