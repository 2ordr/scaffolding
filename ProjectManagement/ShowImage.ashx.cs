﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;


namespace ProjectManagement
{
    /// <summary>
    /// Summary description for ShowImage
    /// </summary>
    public class ShowImage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

            Int32 Invoiceno;

            if (context.Request.QueryString["id"] != null)
                Invoiceno = Convert.ToInt32(context.Request.QueryString["id"]);

            else
                throw new ArgumentException("No Parameter Specified");

            context.Response.ContentType = "image/jpeg";
            Stream strm = ShowInvoiceImage(Invoiceno);
            byte[] buffer = new byte[4096];
            int byteSeq = strm.Read(buffer, 0, 4096);

            while (byteSeq > 0)
            {
                context.Response.OutputStream.Write(buffer, 0, byteSeq);
                byteSeq = strm.Read(buffer, 0, 4096);
            }

        }

        public Stream ShowInvoiceImage(int Invoiceno)
        {
            string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
            SqlConnection connection = new SqlConnection(cs);
            string sql = "select * from INVOICELOGO where InvoiceNo= @ID";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ID", Invoiceno);
            connection.Open();
            object img = cmd.ExecuteScalar();
            try
            {
                return new MemoryStream((byte[])img);
            }
            catch
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}