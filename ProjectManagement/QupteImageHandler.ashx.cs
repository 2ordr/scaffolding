﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace ProjectManagement
{
    /// <summary>
    /// Summary description for QupteImageHandler
    /// </summary>
    public class QupteImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

            try
            {
                string QuoteID = context.Request.QueryString["QuoteID"].ToString();

                string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

                SqlConnection objconn = new SqlConnection(cs);

                objconn.Open();

                SqlCommand cmd = new SqlCommand("Select ImageData from QUOTEImageDetails where QUOTEID= " + QuoteID, objconn);

                SqlDataReader dr = cmd.ExecuteReader();

                dr.Read();

                context.Response.ContentType = "image/jpg";
                context.Response.BinaryWrite((byte[])dr[0]);

                objconn.Close();

            }
            catch (Exception ex)
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}