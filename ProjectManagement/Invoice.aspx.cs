﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace ProjectManagement
{
    public partial class Invoice : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();

                    BindInvoiceData();
                }
            }
        }

        protected void BtnAddInvoice_Click(object sender, EventArgs e)
        {
            Response.Redirect("New Invoice1.aspx");
        }

        public void BindInvoiceData()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com1 = new SqlCommand("select distinct * from INVOICEDATA order by InvoiceNo ");
            com1.Connection = con;
            SqlDataAdapter adpt = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                InvoiceHeader.Visible = true;
                CustInvoiceGridView.DataSource = dt;
                CustInvoiceGridView.DataBind();
            }
            else
            {
                CustInvoiceGridView.DataSource = null;
                CustInvoiceGridView.DataBind();
                InvoiceHeader.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Not Generated any Invoice');", true);
                //return;
            }
            con.Close();
        }

        //protected void LikButEditInvoice_Click(object sender, EventArgs e)
        //{
        //    GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
        //    string InvoiceNo = grdrow.Cells[1].Text;

        //  Response.Redirect("New Invoice1.aspx?InvoiceNO=" + InvoiceNo);
        //}

        protected void LnkDelInvoice_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string invoiceno = grdrow.Cells[1].Text;

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();

            SqlCommand com1 = new SqlCommand();
            com1.Connection = con1;
            com1.CommandType = System.Data.CommandType.Text;
            com1.CommandText = " Delete FROM INVOICEDATA where InvoiceNo ='" + invoiceno + "';";
            com1.ExecuteNonQuery();
            com1.Parameters.Clear();

            com1.CommandType = System.Data.CommandType.Text;
            com1.CommandText = " Delete FROM INVOICE  where InvoiceNo ='" + invoiceno + "';";
            com1.ExecuteNonQuery();

            con1.Close();

            BindInvoiceData();

            ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Invoice Deleted Successfully!!');", true);
            return;
        }

        protected void LikButEditInvoice_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string InvoiceNo = grdrow.Cells[1].Text;
            Response.Redirect("New Invoice1.aspx?InvoiceNo=" + InvoiceNo);
        }

        protected void Bound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "style.backgroundColor='LightBlue'");
            e.Row.Attributes.Add("onmouseout", "style.backgroundColor='#fbfbfb'");
        }
    }
}