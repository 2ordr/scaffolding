﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Adapters;
using System.IO;

namespace ProjectManagement
{
    public partial class FileUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //FileUpload1.SaveAs(Server.MapPath("~/") + FileUpload1.PostedFile.FileName);

            //lblMessage.Text = FileUpload1.PostedFile.FileName + " uploaded successfully !";            


            //check to make sure a file is selected
            if (FileUpload1.HasFile)
            {
                //create the path to save the file to
                string fileName = Path.Combine(Server.MapPath("~/Files"), FileUpload1.FileName);

                if (!Directory.Exists(@"C:\manoj\ProjectManagement\ProjectManagement\Files\"))
                {
                    Directory.CreateDirectory(@"C:\manoj\ProjectManagement\ProjectManagement\Files\");
                }
                //save the file to our local path
                FileUpload1.SaveAs(fileName);
                //Session["filename"] = fileName ;
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('FileUpload Successfully');", true);

        }

       
    }
}