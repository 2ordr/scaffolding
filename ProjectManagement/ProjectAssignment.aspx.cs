﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;
using System.Collections;
using System.Threading;


namespace ProjectManagement
{
    public partial class ProjectAssignment : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand();

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    LBtnBasicInfo.BackColor = System.Drawing.Color.SkyBlue;
                    LBtnBasicInfo.BorderColor = System.Drawing.Color.Black;

                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                    divBasicInfo.Visible = true;
                    divUserList.Visible = false;
                    divOrderList.Visible = false;
                    divTaskList.Visible = false;
                    PopulateHours();
                    PopulateMinutes();
                    PopulateSeconds();
                    LBtnBasicInfo.ForeColor = System.Drawing.Color.Red;
                    Global.Editflag = false;

                    DateTime orddate = DateTime.Now;
                    //string stdt = dtp1.CalendarDateString;

                    //txtStartdt.Text = stdt;
                    // txtStartdt.Text = string.Format("{0:dd-MM-yyyy hh:mm:ss}", stdt);

                    // dtp1.CalendarDate = orddate;
                    // dtp2.CalendarDate = orddate;
                    txtCompareDate.Text = string.Format("{0:dd-MM-yyyy}", orddate);
                    //txtStartdt.Text = Convert.ToString(orddate);
                    txtStartdt.Text = string.Format("{0:dd-MM-yyyy}", orddate);
                    //txtCompletionDate.Text = Convert.ToString(orddate);
                    txtCompletionDate.Text = string.Format("{0:dd-MM-yyyy}", orddate);

                    lblProid.Text = Request.QueryString["Projectid"];

                    Button3.Visible = false;
                    Label1.Text = Session["username"].ToString();

                    BindOrderList();

                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "select * from CUSTOMER";
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dt = new DataSet();
                    adpt.Fill(dt);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        ddlCustName.DataSource = dt;
                        ddlCustName.DataTextField = "CustName";
                        ddlCustName.DataValueField = "CustId";
                        ddlCustName.DataBind();
                        ddlCustName.Items.Insert(0, new ListItem("-Vælg Kunder-", "0"));
                    }

                    SqlCommand com = new SqlCommand("select * from EMPLOYEE ", con);
                    SqlDataAdapter adp = new SqlDataAdapter(com);
                    DataTable dtt = new DataTable();
                    adp.Fill(dtt);
                    if (dtt.Rows.Count > 0)
                    {
                        GridViewEmpname.DataSource = dtt;
                        GridViewEmpname.DataBind();
                    }

                    SqlConnection con1 = new SqlConnection(cs);
                    con1.Open();
                    com.Connection = con1;
                    com.CommandType = System.Data.CommandType.Text;
                    com.CommandText = "Select Distinct EmpId,SkillSets from EMPLOYEE Where SkillSets!='" + null + "'"; // Change Code//
                    SqlDataAdapter adp1 = new SqlDataAdapter(com);
                    DataSet dtt1 = new DataSet();
                    adp1.Fill(dtt1);
                    DDLSkillSet.DataSource = dtt1;
                    DDLSkillSet.DataTextField = "SkillSets";
                    DDLSkillSet.DataValueField = "EmpId";
                    DDLSkillSet.DataBind();
                    DDLSkillSet.Items.Insert(0, new ListItem("-Vælg-", "0"));
                    con1.Close();

                    int taskid;
                    int subtskid;

                    SqlConnection con11 = new SqlConnection(cs);
                    SqlCommand cmd11 = new SqlCommand();
                    cmd11.Connection = con11;
                    con11.Open();
                    cmd11.CommandType = System.Data.CommandType.Text;
                    cmd11.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
                    taskid = Convert.ToInt32(cmd11.ExecuteScalar()) + 1;
                    cmd11.Parameters.Clear();
                    cmd11.CommandText = "Delete FROM TEMPTASK";
                    cmd11.ExecuteNonQuery();

                    cmd11.Parameters.Clear();
                    cmd11.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
                    subtskid = Convert.ToInt32(cmd11.ExecuteScalar());
                    cmd11.Parameters.Clear();

                    cmd11.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd11.CommandText = "sp_InsertTempTask";
                    cmd11.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
                    cmd11.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
                    cmd11.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
                    cmd11.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
                    cmd11.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
                    cmd11.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

                    cmd11.ExecuteNonQuery();
                    cmd11.Parameters.Clear();

                    cmd11.CommandType = System.Data.CommandType.Text;
                    cmd11.CommandText = "SELECT * FROM TEMPTASK";
                    SqlDataAdapter dtask = new SqlDataAdapter(cmd11);
                    DataTable dttask = new DataTable();
                    dtask.Fill(dttask);
                    gridTask.DataSource = dttask;
                    gridTask.DataBind();

                    cmd11.Parameters.Clear();
                    cmd11.CommandType = System.Data.CommandType.Text;
                    cmd11.CommandText = "DELETE  FROM TEMPORDER";
                    cmd11.ExecuteNonQuery();
                    cmd11.Parameters.Clear();

                    con11.Close();

                    lbltaskid.Text = Convert.ToInt32(taskid).ToString();
                    TaskId.Text = lbltaskid.Text;


                    if (lblProid.Text != string.Empty)//--------------Not Use For This Page
                    {
                        BtnUpdate.Visible = true;
                        BtnAdd.Visible = false;
                        SqlConnection con2 = new SqlConnection(cs);
                        con2.Open();
                        SqlCommand cmd2 = new SqlCommand();
                        cmd2.Connection = con2;

                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd2.CommandText = "sp_ViewProject";

                        cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.NVarChar).Value = lblProid.Text; //Request.QueryString["Projectid"];
                        SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
                        DataSet dt2 = new DataSet();
                        adpt2.Fill(dt2);

                        if (dt2.Tables[0].Rows.Count > 0)
                        {
                            gvUpdateProject.DataSource = dt2;
                            gvUpdateProject.DataBind();

                            lblCustid.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                            txtProjectName.Text = dt2.Tables[0].Rows[0]["ProjectName"].ToString();
                            txtDescript.Text = dt2.Tables[0].Rows[0]["Description"].ToString();
                            txtStartdt.Text = dt2.Tables[0].Rows[0]["ProjectDate"].ToString();
                            txtCompletionDate.Text = dt2.Tables[0].Rows[0]["CompletionDate"].ToString();
                            ddlCustName.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                            ddOrderList.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();
                            LblOrder.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();

                            dtp1.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["ProjectDate"].ToString());
                            dtp2.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["CompletionDate"].ToString());
                            Global.Editflag = true;

                            GridViewSelEmployee.DataSource = dt2;
                            GridViewSelEmployee.DataBind();

                            int count = dt2.Tables[0].Rows.Count - 1;

                            foreach (GridViewRow rw in GridViewSelEmployee.Rows)
                            {
                                if (rw.RowType == DataControlRowType.DataRow)
                                {
                                    foreach (GridViewRow rows in GridViewEmpname.Rows)
                                    {
                                        if (rows.RowType == DataControlRowType.DataRow)
                                        {
                                            System.Web.UI.WebControls.Label lbllEmpid = (rows.Cells[0].FindControl("lbllEmpid") as System.Web.UI.WebControls.Label);
                                            CheckBox chkselect = (rows.Cells[0].FindControl("chkSelect") as CheckBox);
                                            Label lblEmpID = (rw.Cells[1].FindControl("lblEmpID") as Label);
                                            DropDownList ddlTaskList = (rw.Cells[4].FindControl("ddlTaskList") as DropDownList);

                                            if (lbllEmpid.Text == lblEmpID.Text)
                                            {
                                                chkselect.Checked = true;
                                            }
                                        }
                                    }
                                }
                            }
                            BOrderList();
                        }
                        else//---------------Not Use For This Page
                        {
                            cmd2.Parameters.Clear();
                            dt2.Tables[0].Rows.Clear();
                            cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd2.CommandText = "sp_GetProjectDetails";
                            cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.NVarChar).Value = lblProid.Text; //Request.QueryString["Projectid"];
                            adpt2.SelectCommand = cmd2;
                            adpt2.Fill(dt2);
                            if (dt2.Tables[0].Rows.Count > 0)
                            {
                                gvUpdateProject.DataSource = dt2;
                                gvUpdateProject.DataBind();

                                lblCustid.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                                txtProjectName.Text = dt2.Tables[0].Rows[0]["ProjectName"].ToString();
                                txtDescript.Text = dt2.Tables[0].Rows[0]["Description"].ToString();
                                txtStartdt.Text = dt2.Tables[0].Rows[0]["ProjectDate"].ToString();
                                txtCompletionDate.Text = dt2.Tables[0].Rows[0]["CompletionDate"].ToString();
                                ddlCustName.Text = dt2.Tables[0].Rows[0]["CustId"].ToString();
                                ddOrderList.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();
                                LblOrder.Text = dt2.Tables[0].Rows[0]["OrderId"].ToString();

                                dtp1.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["ProjectDate"].ToString());
                                dtp2.CalendarDate = Convert.ToDateTime(dt2.Tables[0].Rows[0]["CompletionDate"].ToString());
                                Global.Editflag = false;
                            }
                        }
                        con2.Close();
                        BOrderList();
                    }

                }
            }
        }

        private void PopulateHours()
        {
            for (int i = 0; i <= 24; i++)
            {
                ddlHours.Items.Add(i.ToString("D2"));
            }
        }

        private void PopulateMinutes()
        {
            for (int i = 0; i < 60; i++)
            {
                ddlMinutes.Items.Add(i.ToString("D2"));
            }
        }

        private void PopulateSeconds()
        {
            for (int i = 0; i < 60; i++)
            {
                ddlSeconds.Items.Add(i.ToString("D2"));
            }
        }

        protected void ddlCustName_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCustid.Text = ddlCustName.SelectedValue;

        }

        //protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtEmployeeName.Text != "")
        //    {
        //        SqlConnection con = new SqlConnection(cs);
        //        con.Open();


        //        SqlCommand com = new SqlCommand("select * from EMPLOYEE where FName like @SearchStr ", con);

        //        com.Parameters.Add("@SearchStr", SqlDbType.NVarChar, 100).Value = "%" + txtEmployeeName.Text + "%";

        //        SqlDataAdapter adpt = new SqlDataAdapter(com);
        //        DataTable dt = new DataTable();

        //        adpt.Fill(dt);
        //        GridViewEmpname.DataSource = dt;
        //        GridViewEmpname.DataBind();
        //        dt.Rows.Clear();
        //    }
        //}

        protected void GridViewEmpname_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //        e.Row.Attributes.Add("ondblclick", "__doPostBack('GridViewEmpname','Select$" + e.Row.RowIndex + "');");
            //    }
        }

        protected void GridViewEmpname_SelectedIndexChanged(object sender, EventArgs e)
        {
            //    string EmployeeID = GridViewEmpname.SelectedDataKey.Value.ToString();
            //    string EmployeeName = GridViewEmpname.SelectedRow.Cells[0].Text;
            //    lblEmpid.Text = EmployeeID;
            //   txtEmployeeName.Text = EmployeeName;
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            AssignedUserFieldSet.Visible = true;

            DataTable dt1 = new DataTable();

            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("EmpId") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("FName") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("WagePerHr") });
            //dt1.Columns.AddRange(new DataColumn[] { new DataColumn("TaskID") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("EstimatedHours") });
            foreach (GridViewRow gvrows in GridViewEmpname.Rows)
            {

                if (gvrows.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkselect = (gvrows.Cells[0].FindControl("chkSelect") as CheckBox);
                    Label lbllEmpid = (gvrows.Cells[0].FindControl("lbllEmpid") as Label);
                    //int i = 0;
                    if (chkselect.Checked)
                    {
                        SqlConnection con2 = new SqlConnection(cs);
                        con2.Open();
                        SqlCommand cmd2 = new SqlCommand();
                        cmd2.Connection = con2;

                        if (lblProid.Text != "")
                        {
                            cmd2.Parameters.Clear();
                            cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd2.CommandText = "sp_GetPAssign";
                            cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProid.Text; //Request.QueryString["Projectid"];
                            cmd2.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = lbllEmpid.Text;
                            SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
                            DataTable dt2 = new DataTable();
                            adpt2.Fill(dt2);
                            DataRow dr = dt1.NewRow();

                            if (dt2.Rows.Count > 0)
                            {
                                dr["EmpId"] = dt2.Rows[0]["empid"];
                                dr["FName"] = dt2.Rows[0]["fname"];
                                dr["WagePerHr"] = dt2.Rows[0]["wageperhr"];
                                //dr["Taskid"] = dt2.Rows[0]["TaskId"]; 
                                dr["EstimatedHours"] = dt2.Rows[0]["EstimatedHours"];
                                dt1.Rows.Add(dr);
                                //i = i + 1;
                            }
                            //if (lblEmpID.Text == gvrows.Cells[1].Text)
                            //{

                            //    dr["EmpId"] = gvrows.Cells[1].Text;
                            //    dr["FName"] = gvrows.Cells[2].Text;
                            //    dr["WagePerHr"] = txtWagePerHr.Text;
                            //    //dr["Task"] = ddlTaskList.SelectedItem.Text;
                            //    dr["EstimatedHours"] = txtEstiHrs.Text;
                            //    dt1.Rows.Add(dr);
                            //    break;

                            //}
                            else
                            {
                                //DataRow dr = dt1.NewRow();
                                dr["EmpId"] = gvrows.Cells[1].Text;
                                dr["FName"] = gvrows.Cells[2].Text;
                                dr["WagePerHr"] = 0;
                                //dr["Taskid"] = ""; 
                                dr["EstimatedHours"] = 0;
                                dt1.Rows.Add(dr);
                                //break;
                            }

                            //String EmpId = gvrows.Cells[1].Text;
                            //dt1.Rows.Add(EmpId);

                            //String EmployeeName = gvrows.Cells[2].Text;
                            //dt1.Rows.Add(EmployeeName);

                            GridViewSelEmployee.DataSource = dt1;
                            GridViewSelEmployee.DataBind();
                        }
                        else
                        {
                            DataRow dr = dt1.NewRow();
                            //DataRow dr = dt1.NewRow();
                            dr["EmpId"] = gvrows.Cells[1].Text;
                            dr["FName"] = gvrows.Cells[2].Text;
                            dr["WagePerHr"] = 0;
                            //dr["Taskid"] = ""; 
                            dr["EstimatedHours"] = 0;
                            dt1.Rows.Add(dr);
                            //break;
                        }

                        //String EmpId = gvrows.Cells[1].Text;
                        //dt1.Rows.Add(EmpId);

                        //String EmployeeName = gvrows.Cells[2].Text;
                        //dt1.Rows.Add(EmployeeName);

                        GridViewSelEmployee.DataSource = dt1;
                        GridViewSelEmployee.DataBind();
                    }
                    else
                    {
                        GridViewSelEmployee.DataSource = dt1;
                        GridViewSelEmployee.DataBind();

                        //cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        //cmd2.CommandText = "sp_GetPAssign";
                        //cmd2.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProid.Text; //Request.QueryString["Projectid"];
                        //cmd2.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = lbllEmpid.Text;
                        //SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
                        //DataTable dt2 = new DataTable();
                        //adpt2.Fill(dt2);
                    }
                }
            }


        }

        //protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        //{
        //    DataTable dt1 = new DataTable();

        //    dt1.Columns.AddRange(new DataColumn[] { new DataColumn("EmpId") });
        //    dt1.Columns.AddRange(new DataColumn[] { new DataColumn("FName") });
        //    dt1.Columns.AddRange(new DataColumn[] { new DataColumn("WagePerHr") });
        //    //dt1.Columns.AddRange(new DataColumn[] { new DataColumn("Task") });
        //    dt1.Columns.AddRange(new DataColumn[] { new DataColumn("EstimatedHours") });
        //    foreach (GridViewRow gvrows in GridViewEmpname.Rows)
        //    {

        //        if (gvrows.RowType == DataControlRowType.DataRow)
        //        {
        //            CheckBox chkselect = (gvrows.Cells[0].FindControl("chkSelect") as CheckBox);

        //            if (chkselect.Checked)
        //            {

        //                if (GridViewSelEmployee.Rows.Count > 0)
        //                {                      
        //                foreach (GridViewRow rows in GridViewSelEmployee.Rows)
        //                {

        //                    if (rows.RowType == DataControlRowType.DataRow)
        //                    {
        //                        System.Web.UI.WebControls.Label lblEmpID = (rows.Cells[1].FindControl("lblEmpID") as System.Web.UI.WebControls.Label);
        //                        System.Web.UI.WebControls.TextBox txtWagePerHr = (rows.Cells[3].FindControl("txtWagePerHr") as System.Web.UI.WebControls.TextBox);
        //                        System.Web.UI.WebControls.DropDownList ddlTaskList = (rows.Cells[5].FindControl("ddlTaskList") as System.Web.UI.WebControls.DropDownList);
        //                        System.Web.UI.WebControls.TextBox txtEstiHrs = (rows.Cells[4].FindControl("txtEstiHrs") as System.Web.UI.WebControls.TextBox);

        //                        if (lblEmpID.Text == gvrows.Cells[1].Text)
        //                        {
        //                            DataRow dr = dt1.NewRow();
        //                            dr["EmpId"] = gvrows.Cells[1].Text;
        //                            dr["FName"] = gvrows.Cells[2].Text;
        //                            dr["WagePerHr"] = txtWagePerHr.Text;
        //                            //dr["Task"] = ddlTaskList.SelectedItem.Text;
        //                            dr["EstimatedHours"] = txtEstiHrs.Text; 
        //                            dt1.Rows.Add(dr);
        //                            break;

        //                        }
        //                        else
        //                        {
        //                            DataRow dr = dt1.NewRow();
        //                            dr["EmpId"] = gvrows.Cells[1].Text;
        //                            dr["FName"] = gvrows.Cells[2].Text;
        //                            dr["WagePerHr"] = "";
        //                            //dr["Task"] = "";
        //                            dr["EstimatedHours"] = "";
        //                            dt1.Rows.Add(dr);
        //                            break;
        //                        }
        //                    }
        //                }

        //                }
        //                else
        //                {
        //                    DataRow dr = dt1.NewRow();
        //                    dr["EmpId"] = gvrows.Cells[1].Text;
        //                    dr["FName"] = gvrows.Cells[2].Text;
        //                    dr["WagePerHr"] = "";
        //                    //dr["Task"] = "";
        //                    dr["EstimatedHours"] = "";
        //                    dt1.Rows.Add(dr);

        //                }

        //                //String EmpId = gvrows.Cells[1].Text;
        //                //dt1.Rows.Add(EmpId);

        //                //String EmployeeName = gvrows.Cells[2].Text;
        //                //dt1.Rows.Add(EmployeeName);
        //            }
        //        }
        //    }

        //    GridViewSelEmployee.DataSource = dt1;
        //    GridViewSelEmployee.DataBind();
        //}

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            DataTable dt1 = new DataTable();

            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("EmpId") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("FName") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("WagePerHr") });
            //dt1.Columns.AddRange(new DataColumn[] { new DataColumn("Taskid") });
            dt1.Columns.AddRange(new DataColumn[] { new DataColumn("EstimatedHours") });
            foreach (GridViewRow gvrows in GridViewEmpname.Rows)
            {

                if (gvrows.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkselect = (gvrows.Cells[0].FindControl("chkSelect") as CheckBox);

                    if (chkselect.Checked)
                    {
                        DataRow dr = dt1.NewRow();
                        dr["EmpId"] = gvrows.Cells[1].Text;
                        dr["FName"] = gvrows.Cells[2].Text;
                        dr["WagePerHr"] = "";
                        //dr["Taskid"] = ""; 
                        dr["EstimatedHours"] = "";
                        dt1.Rows.Add(dr);
                        //String EmpId = gvrows.Cells[1].Text;
                        //dt1.Rows.Add(EmpId);

                        //String EmployeeName = gvrows.Cells[2].Text;
                        //dt1.Rows.Add(EmployeeName);
                    }
                }
            }

            GridViewSelEmployee.DataSource = dt1;
            GridViewSelEmployee.DataBind();

        }

        protected void GridViewSelEmployee_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();

            SqlCommand comm = new SqlCommand("select distinct Task,TaskId from TASK", con1);


            SqlDataAdapter adpt12 = new SqlDataAdapter(comm);
            DataTable dt12 = new DataTable();

            adpt12.Fill(dt12);
            if (dt12.Rows.Count > 0)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Global.Editflag == false)
                    {
                        //DropDownList ddlDropDownList = (DropDownList)e.Row.FindControl("ddlSubTaskList");
                        DropDownList ddlMainTask = (DropDownList)e.Row.FindControl("ddlMainTask");

                        if (ddlMainTask != null)
                        {

                            ddlMainTask.DataSource = dt12;
                            ddlMainTask.DataTextField = "Task";
                            ddlMainTask.DataValueField = "TaskId";
                            ddlMainTask.DataBind();
                        }
                        ddlMainTask.Items.Insert(0, new ListItem("-Vælg Opgave-", "0"));
                    }
                    else
                    {
                        comm.Parameters.Clear();

                        //if (ddlMainTask != null)
                        //{

                        //    ddlMainTask.DataSource = dt12;
                        //    ddlMainTask.DataTextField = "Task";
                        //    ddlMainTask.DataValueField = "TaskId";
                        //    ddlMainTask.DataBind();
                        //}

                        //foreach (GridViewRow rw in GridViewSelEmployee.Rows)
                        //{
                        //    if (rw.RowType == DataControlRowType.DataRow)
                        //    {
                        Label lblEmpID = (Label)e.Row.FindControl("lblEmpID");
                        lblTaskEmpId.Text = lblEmpID.Text;
                        //DropDownList ddlTaskList = (rw.Cells[4].FindControl("ddlTaskList") as DropDownList);
                        DropDownList ddlMainTask = (DropDownList)e.Row.FindControl("ddlMainTask");
                        DropDownList ddlSubTaskList = (DropDownList)e.Row.FindControl("ddlSubTaskList");

                        comm.CommandText = "select distinct Task.Task,Task.TaskId,Task.SubTask,Task.STID from TASK,ASSIGNED where ASSIGNED.STID=TASK.STID AND Task.TaskId=ASSIGNED.TaskId AND ASSIGNED.Empid=" + lblEmpID.Text + " and ASSIGNED.ProjectId=" + lblProid.Text + "";
                        adpt12 = new SqlDataAdapter(comm);
                        DataTable dttask = new DataTable();

                        adpt12.Fill(dttask);

                        if (dttask.Rows.Count > 0)
                        {
                            //

                            if (ddlMainTask != null)
                            {

                                ddlMainTask.DataSource = dt12;
                                ddlMainTask.DataTextField = "Task";
                                ddlMainTask.DataValueField = "TaskId";
                                ddlMainTask.DataBind();

                                ddlSubTaskList.DataSource = dttask;
                                ddlSubTaskList.DataTextField = "SubTask";
                                ddlSubTaskList.DataValueField = "STID";
                                ddlSubTaskList.DataBind();

                                ddlMainTask.SelectedItem.Text = dttask.Rows[0]["Task"].ToString();
                                ddlMainTask.SelectedValue = dttask.Rows[0]["TaskId"].ToString();

                                ddlSubTaskList.Visible = true;

                                ddlSubTaskList.SelectedItem.Text = dttask.Rows[0]["SubTask"].ToString();
                                ddlSubTaskList.SelectedValue = dttask.Rows[0]["STID"].ToString();
                                dttask.Rows.Clear();
                                comm.Parameters.Clear();
                            }
                        }
                        else
                        {
                            if (ddlMainTask != null)
                            {

                                ddlMainTask.DataSource = dt12;
                                ddlMainTask.DataTextField = "Task";
                                ddlMainTask.DataValueField = "TaskId";
                                ddlMainTask.DataBind();
                            }
                            ddlMainTask.Items.Insert(0, new ListItem("-Vælg Opgave-", "0"));
                        }
                        //    }
                        //}
                    }


                }
            }
            con1.Close();
        }

        protected void BtnAssign_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            if (ddlCustName.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!Select Customer Name!!');", true);
                return;
            }
            else
            {
                DateTime inTime = Convert.ToDateTime(txtStartdt.Text);
                DateTime outTime = Convert.ToDateTime(txtCompletionDate.Text);
                if (inTime > outTime)
                {
                    //BtnUpdate.Enabled = true;
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper date Which is Greater Than Start Date');", true);
                    return;
                }
                else
                {

                    int projectid;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT  ISNULL(MAX(Projectid),0) FROM PROJECTS";
                    projectid = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
                    cmd.Parameters.Clear();

                    foreach (GridViewRow rows in GridViewSelEmployee.Rows)
                    {
                        if (rows.RowType == DataControlRowType.DataRow)
                        {
                            System.Web.UI.WebControls.Label lblEmpID = (rows.Cells[1].FindControl("lblEmpID") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblEmpNAME = (rows.Cells[2].FindControl("lblEmpNAME") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.TextBox txtWagePerHr = (rows.Cells[3].FindControl("txtWagePerHr") as System.Web.UI.WebControls.TextBox);
                            System.Web.UI.WebControls.DropDownList ddlMainTask = (rows.Cells[4].FindControl("ddlMainTask") as System.Web.UI.WebControls.DropDownList);
                            System.Web.UI.WebControls.DropDownList ddlSubTaskList = (rows.Cells[5].FindControl("ddlSubTaskList") as System.Web.UI.WebControls.DropDownList);
                            System.Web.UI.WebControls.TextBox txtEstiHrs = (rows.Cells[6].FindControl("txtEstiHrs") as System.Web.UI.WebControls.TextBox);

                            if (lblEmpID.Text != "")
                            {
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.CommandText = "sp_InsertAssigned";
                                cmd.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = ddlMainTask.SelectedValue;
                                cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpID.Text;
                                cmd.Parameters.Add("@PROJECTID  ", System.Data.SqlDbType.BigInt).Value = projectid;
                                cmd.Parameters.Add("@ASSIGNEDDATE", System.Data.SqlDbType.DateTime).Value = txtStartdt.Text;

                                if (txtWagePerHr.Text != "")
                                {
                                    cmd.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = txtWagePerHr.Text;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = 0;
                                }
                                if (txtEstiHrs.Text != "")
                                {
                                    cmd.Parameters.Add("@ESTIMATEDHOURS", System.Data.SqlDbType.Decimal).Value = txtEstiHrs.Text;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@ESTIMATEDHOURS", System.Data.SqlDbType.Decimal).Value = 0;
                                }

                                cmd.Parameters.Add("@LABORCHARGES", System.Data.SqlDbType.Decimal).Value = 0;

                                if (ddlSubTaskList.Text != "-Vælg Opgave-")
                                {
                                    cmd.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = ddlSubTaskList.SelectedValue;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = 0;
                                }
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                            }
                        }
                    }

                    foreach (GridViewRow rows in GrdViewOrderDetails.Rows)
                    {
                        if (rows.RowType == DataControlRowType.DataRow)
                        {
                            System.Web.UI.WebControls.Label lblOrderID = (rows.Cells[0].FindControl("lblOrderID") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.LinkButton LnkBtnOrderId = (rows.Cells[0].FindControl("LnkBtnOrderId") as System.Web.UI.WebControls.LinkButton);
                            System.Web.UI.WebControls.Label lblOrderDate = (rows.Cells[1].FindControl("lblOrderDate") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblTotalOrder = (rows.Cells[2].FindControl("lblTotalOrder") as System.Web.UI.WebControls.Label);
                            System.Web.UI.WebControls.Label lblStatus = (rows.Cells[3].FindControl("lblStatus") as System.Web.UI.WebControls.Label);

                            if (lblOrderID.Text != "")
                            {
                                cmd.CommandType = System.Data.CommandType.Text;
                                cmd.CommandText = "INSERT INTO PROJECTORDER (ProjectId,OrderId) Values (@PRO_JECTID,@ORDERID)";

                                cmd.Parameters.Add("@PRO_JECTID  ", System.Data.SqlDbType.BigInt).Value = projectid;
                                cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = lblOrderID.Text;
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                            }
                        }
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_InsertProjects";
                    cmd.Parameters.Add("@I_PRJECTID", System.Data.SqlDbType.BigInt).Value = projectid;
                    if (lblCustid.Text != "")
                    {
                        cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustid.Text;
                    }
                    else
                    {
                        cmd.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = 0;
                    }
                    //cmd.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = ddOrderList.SelectedValue;
                    cmd.Parameters.Add("@PROJECTNAME", System.Data.SqlDbType.NVarChar).Value = txtProjectName.Text;
                    if (txtDescript.Text != "")
                    {
                        cmd.Parameters.Add("@DESCRIPTION", System.Data.SqlDbType.NVarChar).Value = txtDescript.Text;
                    }
                    else
                    {
                        cmd.Parameters.Add("@DESCRIPTION", System.Data.SqlDbType.NVarChar).Value = 0;
                    }
                    cmd.Parameters.Add("@PROJECTDATE", System.Data.SqlDbType.DateTime).Value = txtStartdt.Text;
                    cmd.Parameters.Add("@COMPDATE", System.Data.SqlDbType.DateTime).Value = txtCompletionDate.Text;
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "DELETE  FROM TEMPORDER";
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    Thread.Sleep(2000);
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('!!New Project Created Successfully!!!!!!!!!!!');", true);

                    BtnView.Visible = true;
                    BtnAdd.Visible = false;
                    clearall();

                    con.Close();
                }
            }
        }

        public void clearall()
        {
            txtProjectName.Text = "";
            txtDescript.Text = "";
            txtStartdt.Text = "";
            txtCompletionDate.Text = "";
            GridViewSelEmployee.Visible = false;
        }

        protected void BtnView_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProjectAssignmentNew.aspx");
        }

        //protected void UpdateTimer_Tick(object sender, EventArgs e)
        //{

        //    DateStampLabel.Text = DateTime.Now.ToString();
        //}

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            Label4.Text = DateTime.Now.ToString();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            Button2.Visible = false;
            Button3.Visible = true;
            Label5.Text = Label4.Text;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            Button2.Visible = true;
            Button3.Visible = false;
            Label6.Text = Label4.Text;

            DateTime inTime = Convert.ToDateTime(Label5.Text);
            DateTime outTime = Convert.ToDateTime(Label6.Text);
            TimeSpan i = outTime.Subtract(inTime);
            this.txtHrs.Text = i.ToString();

            // txtHrs.Text = ((Convert.ToInt32(Label6.Text)) - (Convert.ToInt32(Label5.Text))).ToString();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
            if (filename == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Please Select FFile To Upload');", true);
                return;
            }
            else
            {
                //int taskid;
                //SqlConnection con1 = new SqlConnection(cs);
                //con1.Open();
                //SqlCommand cmd1 = new SqlCommand();C:\Tools\ProjectManagement\ProjectManagement\Order Form.aspx
                //cmd1.Connection = con1;

                //cmd1.CommandType = System.Data.CommandType.Text;
                //cmd1.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
                //taskid = Convert.ToInt32(cmd1.ExecuteScalar()) + 1;
                //con1.Close();
                //lbltaskid.Text = taskid.ToString();

                string filePath = @"~\Uploads\" + filename.Trim();
                string newfile = Server.MapPath(filePath);
                FileUpload1.SaveAs(newfile);
                //Stream str = FileUpload1.PostedFile.InputStream;

                //BinaryReader br = new BinaryReader(str);

                //Byte[] size = br.ReadBytes((int)str.Length);

                using (SqlConnection con = new SqlConnection(cs))
                {

                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.CommandText = "insert into TaskFIleData(TaskId,FileName,FilePath) values(@id,@Name,@FilePath)";

                        cmd.Parameters.AddWithValue("@id", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;

                        cmd.Parameters.AddWithValue("@Name", System.Data.SqlDbType.VarChar).Value = filename;

                        cmd.Parameters.AddWithValue("@FilePath", System.Data.SqlDbType.VarChar).Value = newfile;

                        cmd.Connection = con;

                        con.Open();

                        cmd.ExecuteNonQuery();

                        con.Close();



                    }

                } gvFileUpload1();

            }
        }

        private void gvFileUpload1()
        {

            using (SqlConnection con = new SqlConnection(cs))
            {

                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "select * from TaskFIleData where TaskId=" + lbltaskid.Text;

                    cmd.Connection = con;

                    con.Open();

                    gvFileUpload.DataSource = cmd.ExecuteReader();

                    gvFileUpload.DataBind();

                    con.Close();

                }

            }
            Idfiledetails.Visible = true;

        }

        protected void SaveTask_Click(object sender, EventArgs e)
        {
            string hours = "00:00:00";

            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubtask = (rows.Cells[0].FindControl("lblSubtask") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[1].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLSkillSet = (rows.Cells[3].FindControl("DDLSkillSet") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtStatus = (rows.Cells[4].FindControl("txtStatus") as System.Web.UI.WebControls.TextBox);

                    if (txtTask.Text == string.Empty || txtMTask.Text == string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                        return;
                    }

                    else
                    {
                        hours = ddlHours.Text + ":" + ddlMinutes.Text + ":" + ddlSeconds.Text;

                        SqlConnection con = new SqlConnection(cs);
                        con.Open();
                        com.Connection = con;
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertTaskDetails";
                        com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                        com.Parameters.Add("@STATUS ", System.Data.SqlDbType.VarChar).Value = txtStatus.Text;
                        com.Parameters.Add("@ENDDATE", System.Data.SqlDbType.VarChar).Value = "";
                        com.Parameters.Add("@TASKS ", System.Data.SqlDbType.VarChar).Value = txtMTask.Text;
                        com.Parameters.Add("@FILEPATH", System.Data.SqlDbType.VarChar).Value = FileUpload1.FileName;
                        com.Parameters.Add("@EXPECTEDHOURS", System.Data.SqlDbType.Time).Value = hours;
                        com.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = DDLSkillSet.SelectedItem.Text;
                        com.Parameters.Add("@SUBTASK", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                        com.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = lblSubtask.Text;

                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                        con.Close();
                        ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details Added SuccessFully');", true);

                    }
                }
            }
            resetall();
            ViewTaskList();
            BindTaskForEmployee();
        }

        public void BindTaskForEmployee()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            com.Connection = con;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = " select distinct Task,TaskId from TASK";

            SqlDataAdapter adp = new SqlDataAdapter(com);
            DataTable dtb = new DataTable();

            adp.Fill(dtb);

            System.Web.UI.WebControls.DropDownList ddlMainTask = (GridViewSelEmployee.Rows[0].FindControl("ddlMainTask") as System.Web.UI.WebControls.DropDownList);

            if (dtb.Rows.Count > 0)
            {
                ddlMainTask.DataSource = dtb;
                ddlMainTask.DataTextField = "Task";
                ddlMainTask.DataValueField = "TaskId";
                ddlMainTask.DataBind();
            }
            con.Close();
        }

        protected void BtnUpdate_Click(object sender, EventArgs e)//------------------------Not IN USE------------
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand();
            int projid = 0;

            con.Open();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT ISNULL((Projectid),0) FROM ASSIGNED where ProjectId=@PROJECTID";
            cmd.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = lblProid.Text;
            projid = Convert.ToInt32(cmd.ExecuteScalar());
            cmd.Parameters.Clear();
            if (projid > 0)
            {
                cmd.Parameters.Clear();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "DELETE FROM ASSIGNED WHERE ProjectId=@PROJECTID";
                cmd.Parameters.Add("@PROJECTID", System.Data.SqlDbType.BigInt).Value = projid;
                cmd.ExecuteNonQuery();
            }
            foreach (GridViewRow rows in GridViewSelEmployee.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblEmpID = (rows.Cells[1].FindControl("lblEmpID") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblEmpNAME = (rows.Cells[2].FindControl("lblEmpNAME") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtWagePerHr = (rows.Cells[3].FindControl("txtWagePerHr") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList ddlMainTask = (rows.Cells[4].FindControl("ddlMainTask") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.DropDownList ddlSubTaskList = (rows.Cells[5].FindControl("ddlSubTaskList") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtEstiHrs = (rows.Cells[6].FindControl("txtEstiHrs") as System.Web.UI.WebControls.TextBox);

                    if (lblEmpID.Text != "")
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_UpdateAssigned";
                        cmd.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = ddlMainTask.SelectedValue;
                        cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = lblEmpID.Text;
                        cmd.Parameters.Add("@PROJECTID  ", System.Data.SqlDbType.BigInt).Value = lblProid.Text;
                        cmd.Parameters.Add("@ASSIGNEDDATE", System.Data.SqlDbType.DateTime).Value = txtStartdt.Text;

                        if (txtWagePerHr.Text != "")
                        {
                            cmd.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = txtWagePerHr.Text;
                        }
                        else
                        {
                            cmd.Parameters.Add("@WAGEPERHR", System.Data.SqlDbType.Decimal).Value = 0;
                        }
                        if (txtEstiHrs.Text != "")
                        {
                            cmd.Parameters.Add("@ESTIMATEDHOURS", System.Data.SqlDbType.Decimal).Value = txtEstiHrs.Text;
                        }
                        else
                        {
                            cmd.Parameters.Add("@ESTIMATEDHOURS", System.Data.SqlDbType.Decimal).Value = 0;
                        }
                        cmd.Parameters.Add("@LABORCHARGES", System.Data.SqlDbType.Decimal).Value = 0;

                        if (ddlSubTaskList.Visible != false)
                        {
                            if (ddlSubTaskList.Text != "-Vælg Opgave-")
                            {
                                cmd.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = ddlSubTaskList.SelectedValue;
                            }
                            else
                            {
                                cmd.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = 0;
                            }
                        }
                        else
                        {
                            cmd.Parameters.Add("@STID", System.Data.SqlDbType.BigInt).Value = 0;
                        }
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }

            }
            con.Close();

            SqlConnection con1 = new SqlConnection(cs);

            con1.Open();
            com.Connection = con1;
            com.CommandType = System.Data.CommandType.StoredProcedure;
            com.CommandText = "sp_UpdateProject";

            com.Parameters.Add("@I_PRJECTID", System.Data.SqlDbType.BigInt).Value = lblProid.Text;
            com.Parameters.Add("@CUSTID", System.Data.SqlDbType.BigInt).Value = lblCustid.Text;
            com.Parameters.Add("@ORDERID", System.Data.SqlDbType.BigInt).Value = LblOrder.Text;
            com.Parameters.Add("@PROJECTNAME", System.Data.SqlDbType.NVarChar).Value = txtProjectName.Text;
            com.Parameters.Add("@DESCRIPTION", System.Data.SqlDbType.NVarChar).Value = txtDescript.Text;
            com.Parameters.Add("@PROJECTDATE", System.Data.SqlDbType.DateTime).Value = txtStartdt.Text;
            com.Parameters.Add("@COMPDATE", System.Data.SqlDbType.DateTime).Value = txtCompletionDate.Text;

            com.ExecuteNonQuery();
            com.Parameters.Clear();
            con1.Close();

            Thread.Sleep(2000);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<scripts>alert('!! Data submited !! ');</scripts>");
            BtnUpdate.Visible = false;
            BtnView.Visible = true;
        }

        public void resetall()
        {
            txttask_0.Text = "";
            //txtExpectedHrs.Text = "00:00";
            txtMTask.Text = "";
            int taskid, subtskid;
            SqlConnection con11 = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con11;
            con11.Open();

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TASK";
            taskid = Convert.ToInt32(cmd.ExecuteScalar()) + 1;
            lbltaskid.Text = taskid.ToString();

            cmd.Parameters.Clear();
            cmd.CommandText = "Delete FROM TEMPTASK";
            cmd.ExecuteNonQuery();

            cmd.Parameters.Clear();
            cmd.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
            subtskid = Convert.ToInt32(cmd.ExecuteScalar());
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_InsertTempTask";
            cmd.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
            cmd.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
            cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
            cmd.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(cmd);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();

            cmd.Parameters.Clear();

            con11.Close();

            lbltaskid.Text = Convert.ToInt32(taskid).ToString();
            TaskId.Text = lbltaskid.Text;
        }

        public void resetTask()
        {

            int taskid;
            int subtskid;
            SqlConnection con11 = new SqlConnection(cs);
            SqlCommand cmd11 = new SqlCommand();
            cmd11.Connection = con11;
            con11.Open();

            cmd11.CommandType = System.Data.CommandType.Text;
            cmd11.CommandText = "SELECT  ISNULL(MAX(TaskId),0) FROM TEMPTASK";
            taskid = Convert.ToInt32(cmd11.ExecuteScalar());
            lbltaskid.Text = taskid.ToString();
            cmd11.Parameters.Clear();

            cmd11.CommandText = "SELECT ISNULL(MAX(SubTask),0) + 1 FROM TEMPTASK where TaskId=" + taskid + "";
            subtskid = Convert.ToInt32(cmd11.ExecuteScalar());

            cmd11.CommandType = System.Data.CommandType.StoredProcedure;
            cmd11.CommandText = "sp_InsertTempTask";
            cmd11.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = taskid;
            cmd11.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = "";
            cmd11.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = "";
            cmd11.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = "";
            cmd11.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = subtskid;
            cmd11.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = 0;

            cmd11.ExecuteNonQuery();
            cmd11.Parameters.Clear();

            cmd11.CommandType = System.Data.CommandType.Text;
            cmd11.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(cmd11);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();

            cmd11.Parameters.Clear();
            con11.Close();

            lbltaskid.Text = Convert.ToInt32(taskid).ToString();
            TaskId.Text = lbltaskid.Text;
        }

        protected void btnAddNewTask_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubtask = (rows.Cells[0].FindControl("lblSubtask") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[1].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLSkillSet = (rows.Cells[3].FindControl("DDLSkillSet") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtStatus = (rows.Cells[4].FindControl("txtStatus") as System.Web.UI.WebControls.TextBox);

                    //if (txtTask.Text == "")
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                    //    return;
                    //}

                    //else
                    //{
                    com.Parameters.Clear();
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    com.Connection = con;
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_UpdateTempTask";
                    com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                    com.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                    com.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = DDLSkillSet.SelectedItem.Text;
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtStatus.Text;
                    com.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = lblSubtask.Text;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = DDLSkillSet.SelectedValue;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    //com.CommandType = System.Data.CommandType.Text;
                    //com.CommandText = "SELECT * FROM TEMPTASK";
                    //SqlDataAdapter dtask = new SqlDataAdapter(com);
                    //DataTable dttask = new DataTable();
                    //dtask.Fill(dttask);
                    //gridTask.DataSource = dttask;
                    //gridTask.DataBind();

                    con.Close();
                    // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details Added SuccessFully');", true);

                    //resetall();
                    //}
                }
            }
            resetTask();
        }

        protected void gridTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            com.Connection = conn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList DDLSkillSet = (DropDownList)e.Row.FindControl("DDLSkillSet");
                Label lbltaskid = (Label)e.Row.FindControl("lbltaskid");
                Label lblSubtask = (Label)e.Row.FindControl("lblSubtask");

                com.CommandType = System.Data.CommandType.Text;
                com.CommandText = "Select Distinct EmpId,SkillSets from EMPLOYEE Where SkillSets!='" + null + "'"; // Change Code//
                SqlDataAdapter adp1 = new SqlDataAdapter(com);
                DataSet dtt1 = new DataSet();
                adp1.Fill(dtt1);
                DDLSkillSet.DataSource = dtt1;
                DDLSkillSet.DataTextField = "SkillSets";
                DDLSkillSet.DataValueField = "EmpId";
                DDLSkillSet.DataBind();
                DDLSkillSet.Items.Insert(0, new ListItem("-Vælg-", "0"));
                com.Parameters.Clear();

                dtt1.Tables[0].Rows.Clear();
                com.CommandText = "SELECT SkillSet,Empid,TaskName FROM TEMPTASK WHERE TASKID=" + lbltaskid.Text + "AND SubTask=" + lblSubtask.Text + "";
                adp1.SelectCommand = com;
                adp1.Fill(dtt1);
                if (dtt1.Tables[0].Rows.Count > 0)
                {
                    if (dtt1.Tables[0].Rows[0]["TaskName"] != "")
                    {
                        DDLSkillSet.SelectedItem.Text = dtt1.Tables[0].Rows[0]["Skillset"].ToString();
                        DDLSkillSet.SelectedValue = dtt1.Tables[0].Rows[0]["Empid"].ToString();
                    }
                    else
                    {
                        DDLSkillSet.Items.Insert(0, new ListItem("-Vælg-", "0"));
                    }


                }
                com.Parameters.Clear();
                conn.Close();
            }

        }

        protected void lnkDeletetask_Click(object sender, EventArgs e)
        {
            LinkButton lnkDeletetask = (LinkButton)sender;
            GridViewRow grdRow = ((GridViewRow)lnkDeletetask.Parent.Parent);
            Label lbltaskid = (Label)grdRow.FindControl("lbltaskid");
            Label lblSubtask = (Label)grdRow.FindControl("lblSubtask");

            com.Parameters.Clear();
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            com.Connection = conn;
            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "Delete from TempTask where TASKID=" + lbltaskid.Text + "AND SubTask=" + lblSubtask.Text + "";
            com.ExecuteNonQuery();
            com.Parameters.Clear();

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "SELECT * FROM TEMPTASK";
            SqlDataAdapter dtask = new SqlDataAdapter(com);
            DataTable dttask = new DataTable();
            dtask.Fill(dttask);
            gridTask.DataSource = dttask;
            gridTask.DataBind();
            com.Parameters.Clear();

            conn.Close();
        }

        protected void lnkbtnView_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //string FilePath = grdrow.Cells[2].Text;
            Label lblFilePath = (Label)grdrow.Cells[2].FindControl("lblFilePath");
            string FilePath = lblFilePath.Text;

            if (FilePath.Contains(".doc") || FilePath.Contains(".rtf") || FilePath.Contains(".html") || FilePath.Contains(".log") || FilePath.Contains(".jpeg") || FilePath.Contains(".tif") || FilePath.Contains(".tiff") || FilePath.Contains(".gif") || FilePath.Contains(".bmp") || FilePath.Contains(".asf"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            //view pdf files 
            if (FilePath.Contains(".pdf") || FilePath.Contains(".avi") || FilePath.Contains(".zip") || FilePath.Contains(".xls") || FilePath.Contains(".csv") || FilePath.Contains(".wav") || FilePath.Contains(".mp3"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }


            if (FilePath.Contains(".htm") || FilePath.Contains(".mpg") || FilePath.Contains(".mpeg") || FilePath.Contains(".fdf") || FilePath.Contains(".ppt") || FilePath.Contains(".dwg") || FilePath.Contains(".msg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;

            }

            if (FilePath.Contains(".txt") || FilePath.Contains(".xml") || FilePath.Contains(".sdxl") || FilePath.Contains(".xdp") || FilePath.Contains(".jar"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".jpg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".png"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }
        }

        //protected void ddlMainTask_DataBound(object sender, EventArgs e)
        //{

        //}

        protected void ddlMainTask_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void ddlMainTask_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            DropDownList ddlMainTask = (DropDownList)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)ddlMainTask.Parent.Parent);
            //DropDownList ddlMainTask = (DropDownList)grdrDropDownRow.FindControl("ddlTaskList");

            DropDownList ddlSubTaskList = (DropDownList)grdrDropDownRow.FindControl("ddlSubTaskList");
            com.CommandText = "select  distinct SubTask,STID from TASK where TASKID= " + ddlMainTask.SelectedValue;

            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                if (ddlSubTaskList != null)
                {

                    ddlSubTaskList.DataSource = dt;
                    ddlSubTaskList.DataTextField = "SubTask";
                    ddlSubTaskList.DataValueField = "STID";
                    ddlSubTaskList.DataBind();
                }
                ddlSubTaskList.Items.Insert(0, new ListItem("-Vælg Opgave-", "0"));
                ddlSubTaskList.Visible = true;
            }
            con.Close();
        }

        protected void ddOrderList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddOrderList.SelectedValue != "-Vælg Ordre-")
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand();

                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select * From ORDERS Where OrderId= '" + ddOrderList.SelectedValue + "'";
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                adpt.Fill(dt);

                if (dt.Tables[0].Rows.Count > 0)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_InsertTempOrder";

                    cmd.Parameters.Add("@TEMPORDERID", System.Data.SqlDbType.BigInt).Value = dt.Tables[0].Rows[0]["OrderId"].ToString();
                    cmd.Parameters.Add("@ORDERDATE", System.Data.SqlDbType.NVarChar).Value = dt.Tables[0].Rows[0]["OrderDate"].ToString();
                    cmd.Parameters.Add("@TAX", System.Data.SqlDbType.Decimal).Value = dt.Tables[0].Rows[0]["Taxes"].ToString();
                    cmd.Parameters.Add("@ORDERTOTAL", System.Data.SqlDbType.Decimal).Value = dt.Tables[0].Rows[0]["OrderTotal"].ToString();
                    cmd.Parameters.Add("@PENALTY", System.Data.SqlDbType.VarChar).Value = dt.Tables[0].Rows[0]["Penalty"].ToString();
                    cmd.Parameters.Add("@INCENTIVE", System.Data.SqlDbType.VarChar).Value = dt.Tables[0].Rows[0]["Incentive"].ToString();
                    cmd.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = dt.Tables[0].Rows[0]["Status"].ToString();

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Order Not Available');", true);
                    return;
                }

                cmd.Parameters.Clear();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select distinct * From TEMPORDER";
                SqlDataAdapter adpt1 = new SqlDataAdapter(cmd);
                DataSet dt1 = new DataSet();
                adpt1.Fill(dt1);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    GrdViewOrderDetails.DataSource = dt1;
                    GrdViewOrderDetails.DataBind();
                }
                else
                {
                    GrdViewOrderDetails.DataSource = dt1;
                    GrdViewOrderDetails.DataBind();
                }

                con.Close();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Select Proper Order');", true);
                return;
            }
        }

        public void BindOrderList()
        {
            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();
            com.Connection = con1;

            com.CommandType = System.Data.CommandType.Text;
            com.CommandText = "select * from ORDERS";
            SqlDataAdapter adp1 = new SqlDataAdapter(com);
            DataSet dtt1 = new DataSet();
            adp1.Fill(dtt1);
            ddOrderList.DataSource = dtt1;
            ddOrderList.DataTextField = "OrderName";
            ddOrderList.DataValueField = "OrderId";
            ddOrderList.DataBind();
            ddOrderList.Items.Insert(0, new ListItem("-Vælg Ordre-", "0"));
            con1.Close();
        }

        public void BOrderList()
        {
            SqlConnection con12 = new SqlConnection(cs);
            con12.Open();
            SqlCommand cmd12 = new SqlCommand();

            cmd12.Connection = con12;
            cmd12.CommandType = System.Data.CommandType.Text;
            cmd12.CommandText = "select * from HAS,ORDERS,STOCKSITEM where ORDERS.OrderId=HAS.OrderId and HAS.ItemCode=STOCKSITEM.ItemCode and HAS.DateTime=ORDERS.OrderDate and ORDERS.OrderId= '" + LblOrder.Text + "'";
            SqlDataAdapter adpt12 = new SqlDataAdapter(cmd12);
            DataSet dt12 = new DataSet();
            adpt12.Fill(dt12);
            if (dt12.Tables[0].Rows.Count > 0)
            {
                GridViewEditOrder.DataSource = dt12;
                GridViewEditOrder.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Order Not Available');", true);
                return;
            }

            con12.Close();
        }

        protected void LBtnBasicInfo_Click(object sender, EventArgs e)
        {
            LBtnBasicInfo.BackColor = System.Drawing.Color.SkyBlue;
            LBtnBasicInfo.BorderColor = System.Drawing.Color.Black;
            LBtnUserList.BackColor = LBtnOrderList.BackColor = LBtnNewTask.BackColor = System.Drawing.Color.Empty;
            LBtnUserList.BorderColor = LBtnOrderList.BorderColor = LBtnNewTask.BorderColor = System.Drawing.Color.Empty;

            LBtnBasicInfo.ForeColor = System.Drawing.Color.Red;
            LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;
            divBasicInfo.Visible = true;

            divBasicInfo.Visible = true;
            divUserList.Visible = false;
            divOrderList.Visible = false;
            divTaskList.Visible = false;
        }

        protected void LBtnUserList_Click(object sender, EventArgs e)
        {
            LBtnUserList.BackColor = System.Drawing.Color.SkyBlue;
            LBtnUserList.BorderColor = System.Drawing.Color.Black;
            LBtnBasicInfo.BackColor = LBtnOrderList.BackColor = LBtnNewTask.BackColor = System.Drawing.Color.Empty;
            LBtnBasicInfo.BorderColor = LBtnOrderList.BorderColor = LBtnNewTask.BorderColor = System.Drawing.Color.Empty;

            LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnUserList.ForeColor = System.Drawing.Color.Red;

            divUserList.Visible = true;
            divTaskList.Visible = false;
            divBasicInfo.Visible = false;
            divOrderList.Visible = false;

            FieldSetUserList.Visible = true;
        }

        protected void LBtnOrderList_Click(object sender, EventArgs e)
        {
            LBtnOrderList.BackColor = System.Drawing.Color.SkyBlue;
            LBtnOrderList.BorderColor = System.Drawing.Color.Black;
            LBtnUserList.BackColor = LBtnBasicInfo.BackColor = LBtnNewTask.BackColor = System.Drawing.Color.Empty;
            LBtnUserList.BorderColor = LBtnBasicInfo.BorderColor = LBtnNewTask.BorderColor = System.Drawing.Color.Empty;

            LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnNewTask.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnOrderList.ForeColor = System.Drawing.Color.Red;
            LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;

            divUserList.Visible = false;
            divTaskList.Visible = false;
            divBasicInfo.Visible = false;
            divOrderList.Visible = true;
        }

        protected void LBtnNewTask_Click(object sender, EventArgs e)
        {
            LBtnNewTask.BackColor = System.Drawing.Color.SkyBlue;
            LBtnNewTask.BorderColor = System.Drawing.Color.Black;
            LBtnUserList.BackColor = LBtnOrderList.BackColor = LBtnBasicInfo.BackColor = System.Drawing.Color.Empty;
            LBtnUserList.BorderColor = LBtnOrderList.BorderColor = LBtnBasicInfo.BorderColor = System.Drawing.Color.Empty;

            LBtnBasicInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnNewTask.ForeColor = System.Drawing.Color.Red;
            LBtnOrderList.ForeColor = System.Drawing.Color.RoyalBlue;
            LBtnUserList.ForeColor = System.Drawing.Color.RoyalBlue;

            divBasicInfo.Visible = false;
            divUserList.Visible = false;
            divOrderList.Visible = false;
            divTaskList.Visible = true;
        }

        protected void AddTask_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow rows in gridTask.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    //System.Web.UI.WebControls.CheckBox chkItem = (rows.Cells[0].FindControl("chkItem") as System.Web.UI.WebControls.CheckBox);
                    System.Web.UI.WebControls.Label lbltaskid = (rows.Cells[0].FindControl("lbltaskid") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.Label lblSubtask = (rows.Cells[0].FindControl("lblSubtask") as System.Web.UI.WebControls.Label);
                    System.Web.UI.WebControls.TextBox txtTask = (rows.Cells[1].FindControl("txtTask") as System.Web.UI.WebControls.TextBox);
                    System.Web.UI.WebControls.DropDownList DDLSkillSet = (rows.Cells[2].FindControl("DDLSkillSet") as System.Web.UI.WebControls.DropDownList);
                    System.Web.UI.WebControls.TextBox txtStatus = (rows.Cells[4].FindControl("txtStatus") as System.Web.UI.WebControls.TextBox);

                    //if (txtTask.Text == "")
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details cannot be blank');", true);
                    //    return;
                    //}

                    //else
                    //{
                    com.Parameters.Clear();
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();
                    com.Connection = con;
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandText = "sp_UpdateTempTask";
                    com.Parameters.Add("@TASKID", System.Data.SqlDbType.BigInt).Value = lbltaskid.Text;
                    com.Parameters.Add("@TASKS", System.Data.SqlDbType.VarChar).Value = txtTask.Text;
                    com.Parameters.Add("@SKILLSET", System.Data.SqlDbType.NVarChar).Value = DDLSkillSet.SelectedItem.Text;
                    com.Parameters.Add("@STATUS", System.Data.SqlDbType.VarChar).Value = txtStatus.Text;
                    com.Parameters.Add("@SUBTASK", System.Data.SqlDbType.BigInt).Value = lblSubtask.Text;
                    com.Parameters.Add("@EMPID", System.Data.SqlDbType.BigInt).Value = DDLSkillSet.SelectedValue;

                    com.ExecuteNonQuery();
                    com.Parameters.Clear();

                    //com.CommandType = System.Data.CommandType.Text;
                    //com.CommandText = "SELECT * FROM TEMPTASK";
                    //SqlDataAdapter dtask = new SqlDataAdapter(com);
                    //DataTable dttask = new DataTable();
                    //dtask.Fill(dttask);
                    //gridTask.DataSource = dttask;
                    //gridTask.DataBind();

                    con.Close();
                    // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Task Details Added SuccessFully');", true);

                    //resetall();
                    //}
                }
            }
            resetTask();
        }

        protected void btnVIewTask_Click(object sender, EventArgs e)
        {
            ViewTaskList();
        }

        public void ViewTaskList()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand amdd = new SqlCommand();
            con.Open();
            amdd.Connection = con;
            amdd.CommandType = System.Data.CommandType.StoredProcedure;
            amdd.CommandText = "sp_getTaskDetails";
            SqlDataAdapter adp1 = new SqlDataAdapter(amdd);
            DataSet dtt1 = new DataSet();
            adp1.Fill(dtt1);
            if (dtt1.Tables[0].Rows.Count > 0)
            {
                gvTaskView.DataSource = dtt1;
                gvTaskView.DataBind();
                IdTaskDetails.Visible = true;
            }
            else
            {
                gvTaskView.DataSource = dtt1;
                gvTaskView.DataBind();
            }
        }

        protected void lbtnViewImage_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //string FilePath = grdrow.Cells[2].Text;
            Label lblFilePath = (Label)grdrow.Cells[2].FindControl("lblFilePath");
            string FilePath = lblFilePath.Text;

            if (FilePath.Contains(".doc") || FilePath.Contains(".rtf") || FilePath.Contains(".html") || FilePath.Contains(".log") || FilePath.Contains(".jpeg") || FilePath.Contains(".tif") || FilePath.Contains(".tiff") || FilePath.Contains(".gif") || FilePath.Contains(".bmp") || FilePath.Contains(".asf"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            //view pdf files 
            if (FilePath.Contains(".pdf") || FilePath.Contains(".avi") || FilePath.Contains(".zip") || FilePath.Contains(".xls") || FilePath.Contains(".csv") || FilePath.Contains(".wav") || FilePath.Contains(".mp3"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }


            if (FilePath.Contains(".htm") || FilePath.Contains(".mpg") || FilePath.Contains(".mpeg") || FilePath.Contains(".fdf") || FilePath.Contains(".ppt") || FilePath.Contains(".dwg") || FilePath.Contains(".msg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                Session["FILEPATH"] = FilePath;
                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;

            }

            if (FilePath.Contains(".txt") || FilePath.Contains(".xml") || FilePath.Contains(".sdxl") || FilePath.Contains(".xdp") || FilePath.Contains(".jar"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".jpg"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }

            if (FilePath.Contains(".png"))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();


                Session["FILEPATH"] = FilePath;
                //Response.ContentType = "text/html";
                //Response.WriteFile(FilePath);
                //Response.Flush();
                //Response.End();

                string url = "ViewFile.aspx";
                string s = "window.open('" + url + "');";

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "newpage", s, true);
                return;
            }
        }

        protected void LnkBtnOrderId_Click(object sender, EventArgs e)
        {
            LinkButton ID = (LinkButton)sender;

            if (ID != null)
            {
                LabelID.Text = ID.Text;
            }

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand();

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select * from HAS,ORDERS,QUOTATIONDATA where ORDERS.OrderId=HAS.OrderId and HAS.ItemCode=QUOTATIONDATA.ItemCode and HAS.TemDate=QUOTATIONDATA.TempDate and HAS.DateTime=ORDERS.OrderDate and QUOTATIONDATA.TEMPType='false' and ORDERS.OrderId= '" + LabelID.Text + "'";
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.Fill(dt);

            if (dt.Tables[0].Rows.Count > 0)
            {
                GridViewEditOrder.DataSource = dt;
                GridViewEditOrder.DataBind();
            }
            else
            {
                GridViewEditOrder.DataSource = dt;
                GridViewEditOrder.DataBind();

                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Order Not Available');", true);
                return;
            }
            con.Close();

            foreach (GridViewRow rows in GrdViewOrderDetails.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.LinkButton LnkBtnOrderId = (rows.Cells[0].FindControl("LnkBtnOrderId") as System.Web.UI.WebControls.LinkButton);

                    if (ID.Text == LnkBtnOrderId.Text)
                    {
                        rows.BackColor = System.Drawing.Color.LightBlue;
                    }
                    else
                    {
                        rows.BackColor = System.Drawing.Color.Empty;
                    }
                }
            }
        }

        protected void GrdViewOrderDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                int ORDERId = Convert.ToInt32(e.CommandArgument);

                SqlConnection con1 = new SqlConnection(cs);
                con1.Open();

                SqlCommand com1 = new SqlCommand("delete FROM TEMPORDER where OrderId = @ORDER_ID  ", con1);

                com1.Parameters.Add("@ORDER_ID", SqlDbType.BigInt).Value = ORDERId;

                com1.ExecuteNonQuery();

                com1.Parameters.Clear();

                com1.CommandType = System.Data.CommandType.Text;
                com1.CommandText = "Select distinct * From TEMPORDER";
                SqlDataAdapter adpt1 = new SqlDataAdapter(com1);
                DataSet dt1 = new DataSet();
                adpt1.Fill(dt1);
                if (dt1.Tables[0].Rows.Count > 0)
                {
                    GrdViewOrderDetails.DataSource = dt1;
                    GrdViewOrderDetails.DataBind();

                    if (ORDERId.ToString() == LabelID.Text)
                    {
                        GridViewEditOrder.DataSource = null;
                        GridViewEditOrder.DataBind();
                    }
                    else
                    {

                    }
                }
                else
                {
                    GrdViewOrderDetails.DataSource = dt1;
                    GrdViewOrderDetails.DataBind();

                    GridViewEditOrder.DataSource = null;
                    GridViewEditOrder.DataBind();
                }

                con1.Close();
                // ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('ORDER Deleted Successfully!!');", true);
                return;
            }
        }

        protected void Bound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "style.backgroundColor='LightBlue'");
            e.Row.Attributes.Add("onmouseout", "style.backgroundColor='#fbfbfb'");
        }
    }

}
