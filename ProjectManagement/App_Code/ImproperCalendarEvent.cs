﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DHTMLX.Scheduler;

namespace ProjectManagement.App_Code
{
    public class ImproperCalendarEvent
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }

        [DHXJson(Ignore = true)]
        public string start { get; set; }
        public string end { get; set; }
        public bool allDay { get; set; }
    }
}