﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Sql;
using System.Net;
using System.Net.Mail;
using System.Text;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;


namespace ProjectManagement
{
    public partial class QuatationPrint : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd1 = new SqlCommand();
                SqlCommand cmd2 = new SqlCommand();
                SqlCommand cmd3 = new SqlCommand();
                cmd1.Connection = con;
                cmd2.Connection = con;
                cmd3.Connection = con;

                cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                cmd1.CommandText = "sp_getQuatationByDate";
                cmd1.Parameters.Add("@QUATDATE", System.Data.SqlDbType.NVarChar).Value = Request.QueryString["Quatdate"];
                SqlDataAdapter adpt = new SqlDataAdapter(cmd1);
                DataSet dt = new DataSet();
                adpt.Fill(dt);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    GrdViewPrint.DataSource = dt;
                    GrdViewPrint.DataBind();
                }
                cmd2.CommandType = System.Data.CommandType.Text;
                cmd2.CommandText = "select * from [Predifined-Text]";
                SqlDataAdapter adpt1 = new SqlDataAdapter(cmd2);
                DataTable dt1 = new DataTable();
                adpt1.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {

                    GridView3.DataSource = dt1;
                    GridView3.DataBind();
                }
                LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                LblCustAddress.Text = dt.Tables[0].Rows[0]["AddressLine1"].ToString();
                LblCity.Text = dt.Tables[0].Rows[0]["City"].ToString();
                LblEmail.Text = dt.Tables[0].Rows[0]["EmailId"].ToString();


                Datetime.Text = dt.Tables[0].Rows[0]["DateTime"].ToString();
                //CompanyName.Text=Convert.ToString(dt.[""]);
                datetime1.Text = dt.Tables[0].Rows[0]["DateTime"].ToString();
                lblTotalCost1.Text = dt.Tables[0].Rows[0]["TotalCost"].ToString();

                cmd3.CommandText = "SELECT count(*) as Rcnt FROM SHOWS where datetime='" + Datetime.Text + "'";
                SqlDataAdapter adpt2 = new SqlDataAdapter(cmd3);
                DataTable dt2 = new DataTable();
                adpt2.Fill(dt2);
                if (dt2.Rows.Count > 0)
                {
                    string rowcont = dt2.Rows[0]["Rcnt"].ToString();

                    if (rowcont != "0")
                    {
                        btnSave.Enabled = false;

                        int rowcnt = 0;
                        cmd3.CommandText = "select distinct * from [Predifined-Text] p, REQUESTEDQUOTE r, shows s where s.datetime = r.datetime and p.StanTekstId = s.StanTekstId and s.datetime='" + Datetime.Text + "'";
                        SqlDataAdapter adpt3 = new SqlDataAdapter(cmd3);
                        DataTable dt3 = new DataTable();
                        adpt3.Fill(dt3);

                        for (int i = 0; i < dt3.Rows.Count; i++)
                        {


                            foreach (GridViewRow rows in GridView3.Rows)
                            {
                                if (rowcnt <= i)
                                {
                                    if (rows.RowType == DataControlRowType.DataRow)
                                    {
                                        System.Web.UI.WebControls.DropDownList ddlpre = (rows.Cells[0].FindControl("DropDownList2") as System.Web.UI.WebControls.DropDownList);//int total = 0;
                                        System.Web.UI.WebControls.Label lblCondition = (rows.Cells[0].FindControl("lblCondition") as System.Web.UI.WebControls.Label);

                                        ddlpre.SelectedItem.Text = dt3.Rows[i]["KortNavn"].ToString();

                                        lblCondition.Text = dt3.Rows[i]["Standardtekst"].ToString();


                                        ddlpre.Enabled = false;
                                        ddlpre.Visible = false;


                                        rowcnt++;
                                    }
                                }
                                //if (rowcnt > i)
                                //{
                                //    if (rows.RowType == DataControlRowType.DataRow)
                                //    {
                                //        System.Web.UI.WebControls.DropDownList ddlpre = (rows.Cells[0].FindControl("DropDownList2") as System.Web.UI.WebControls.DropDownList);//int total = 0;
                                //        System.Web.UI.WebControls.Label lblCondition = (rows.Cells[0].FindControl("lblCondition") as System.Web.UI.WebControls.Label);


                                //        ddlpre.Enabled = false;

                                //        ddlpre.Visible = false;


                                //    }
                                //}




                            }
                        }
                        ResetTerms();
                        //** To Do As-  select distinct * from [Predifined-Text] p, REQUESTEDQUOTE r, shows s where s.datetime = r.datetime and p.StanTekstId = s.StanTekstId 
                        //** Now To Pass p.KortNavn & p.Standardtekst  to the grid  rows.

                    }

                    else
                    {
                        btnSave.Enabled = true;
                    }
                }

                cmd1.Parameters.Clear();

            }

        }

        //protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //        foreach (GridViewRow rows in GridView3.Rows)
        //    {
        //        if (rows.RowType == DataControlRowType.DataRow)
        //        {
        //            System.Web.UI.WebControls.DropDownList ddlpre = (rows.Cells[0].FindControl("DropDownList2") as System.Web.UI.WebControls.DropDownList);
        //            System.Web.UI.WebControls.Label lblCondition = (rows.Cells[0].FindControl("lblCondition") as System.Web.UI.WebControls.Label);
        //            SqlConnection con = new SqlConnection(cs);
        //            con.Open();


        //    SqlCommand com = new SqlCommand("select * from [Predifined-Text] where StantekstID = @SearchStr ", con);

        //    com.Parameters.Add("@SearchStr", SqlDbType.NVarChar, 50).Value = ddlpre.SelectedValue;

        //    SqlDataAdapter adpt = new SqlDataAdapter(com);
        //    DataTable dt = new DataTable();

        //    adpt.Fill(dt);

        //    lblCondition.Text = dt.Rows[0]["Standardtekst"].ToString();
        //        }
        //        }
        //}

        protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand("select * from [Predifined-Text]", con);
            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlDropDownList = (DropDownList)e.Row.FindControl("DropDownList2");
                    if (ddlDropDownList != null)
                    {
                        ddlDropDownList.DataSource = dt;
                        ddlDropDownList.DataTextField = "KortNavn";
                        ddlDropDownList.DataValueField = "StantekstID";
                        ddlDropDownList.DataBind();
                    }
                    //ddlDropDownList.Items.Insert(0, new ListItem("-Select Items-", "0"));
                }
            }
            con.Close();
        }
        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;

            DropDownList ddlCurrentDropDownList = (DropDownList)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)ddlCurrentDropDownList.Parent.Parent);
            Label lblCondition = (Label)grdrDropDownRow.FindControl("lblCondition");
            if (lblCondition != null)


                com.CommandText = "select * from [Predifined-Text] where StantekstID= " + ddlCurrentDropDownList.SelectedValue;


            SqlDataAdapter adpt = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                lblCondition.Text = dt.Rows[0]["Standardtekst"].ToString();
            }
            con.Close();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            btnSave.Visible = false;
            ExportTopdf.Visible = true;
            foreach (GridViewRow rows in GridView3.Rows)
            {
                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.DropDownList ddlpre = (rows.Cells[0].FindControl("DropDownList2") as System.Web.UI.WebControls.DropDownList);//int total = 0;
                    System.Web.UI.WebControls.Label lblCondition = (rows.Cells[0].FindControl("lblCondition") as System.Web.UI.WebControls.Label);
                    SqlConnection con = new SqlConnection(cs);
                    con.Open();

                    SqlCommand com = new SqlCommand();
                    com.Connection = con;
                    if (lblCondition.Text != "")
                    {
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.CommandText = "sp_InsertShow";
                        com.Parameters.Add("@DATETIME", SqlDbType.NVarChar, 50).Value = Datetime.Text;
                        com.Parameters.Add("@StanTekstId", SqlDbType.NVarChar, 50).Value = ddlpre.SelectedValue;
                        com.ExecuteNonQuery();
                        com.Parameters.Clear();
                        ResetTerms();
                    }
                    con.Close();
                }
            }
        }

        protected void ResetTerms()
        {
            foreach (GridViewRow rows in GridView3.Rows)
            {

                if (rows.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.DropDownList ddlpre = (rows.Cells[0].FindControl("DropDownList2") as System.Web.UI.WebControls.DropDownList);//int total = 0;
                    System.Web.UI.WebControls.Label lblCondition = (rows.Cells[0].FindControl("lblCondition") as System.Web.UI.WebControls.Label);

                    if (lblCondition.Text != "")
                    {
                        ddlpre.Enabled = false;

                    }
                    else
                    {
                        ddlpre.Visible = false;
                    }

                }
            }

        }

        protected void GridView3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public override void VerifyRenderingInServerForm(Control control)
        {

            /* Verifies that the control is rendered */

        }
        protected void ExportTopdf_Click(object sender, EventArgs e)
        {
            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    //To Export all pages
                    GridView3.AllowPaging = false;
                    //this.BindStock();
                    GridView3.Columns[0].Visible = false;
                    QuationMainDiv.RenderControl(hw);
                    //GridView3.RenderControl(hw);

                    // GridView2.HeaderRow.Style.Add("width", "15%");
                    //GridView2.HeaderRow.Style.Add("font-size", "10px");
                    // divheader.Style.Add("text-decoration", "bold");
                    // divheader.Style.Add("font-family", "Arial, Helvetica, sans-serif;");
                    //divheader.Style.Add("font-size", "8px");

                    String attachment = "attachment;filename= QuatationPrint.pdf " + DateTime.Now + "";
                    System.IO.StringReader sr = new System.IO.StringReader(sw.ToString());
                    Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    htmlparser.Parse(sr);
                    pdfDoc.Close();

                    Response.ContentType = "application/pdf";

                    Response.AddHeader("content-disposition", attachment);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();
                }
            }
        }
    }
}