﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order Form.aspx.cs" Inherits="ProjectManagement.Order_Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>New - Projects - Scaffolding</title>

    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
    <%--<link rel="stylesheet" href="fonts/untitle-font-2/styles12.css" type="text/css" />--%>

    <link rel="stylesheet" href="fonts/untitle-font-4/styles.css" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>

    <style type="text/css">
        .clienthead {
            font-size: 20px;
            font-weight: bold;
        }

        *.MyCalendar1 .ajax__calendar_container {
            border: 1px solid #646464;
            background-color: #3A4F63;
            color: white;
            position: absolute;
            left: 1px;
            top: 3px;
        }

        .MyCalendar1 .ajax__calendar_footer {
            font-size: 0px;
            height: 0px;
        }

        .MyCalendar1 .ajax__calendar_other .ajax__calendar_day, .MyCalendar1 .ajax__calendar_other .ajax__calendar_year {
            color: black;
        }

        .MyCalendar1 .ajax__calendar_hover .ajax__calendar_day, .MyCalendar1 .ajax__calendar_hover .ajax__calendar_month, .MyCalendar1 .ajax__calendar_hover .ajax__calendar_year {
            color: black;
        }

        .MyCalendar1 .ajax__calendar_active .ajax__calendar_day, .MyCalendar1 .ajax__calendar_active .ajax__calendar_month, .MyCalendar1 .ajax__calendar_active .ajax__calendar_year {
            color: black;
            font-weight: bold;
        }
    </style>

    <script type="text/javascript" language="javascript">

        function DisableBackButton() {
            window.history.forward(0)
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
    </script>
    <script type="text/javascript">

        function ShowDiv(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'none')
                e.style.display = 'block';

            else
                e.style.display = 'none';

            return false;
        }
    </script>
</head>

<body class="dark x-body x-win x-border-layout-ct x-border-box x-container x-container-default" id="ext-gen1022" scroll="no" style="border-width: 0px;">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>

        <div style="height: 1000px; width: 1920px;">

            <div class="x-panel x-border-item x-box-item x-panel-main-menu expanded" id="main-menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 1000px; right: auto;">
                <div class="x-panel-body x-panel-body-main-menu x-box-layout-ct x-panel-body-main-menu x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="main-menu-body" role="presentation" style="left: 0px; top: 0px; width: 195px; height: 809px;">
                    <div class="x-box-inner " id="main-menu-innerCt" role="presentation" style="width: 195px; height: 809px;">
                        <div class="x-box-target" id="main-menu-targetEl" role="presentation" style="width: 195px;">
                            <div class="x-panel search x-box-item x-panel-default" id="searchBox" style="margin: 0px; left: 0px; top: 0px; width: 195px; height: 70px; right: auto;">

                                <asp:TextBox CssClass="twitterStyleTextbox" ID="TextBox1" AutoPostBack="true" runat="server" Text="Search(Ctrl+/)" Height="31" Width="150"></asp:TextBox>

                                <%--<div class="x-panel-body x-panel-body-default x-box-layout-ct x-panel-body-default x-docked-noborder-top x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left" id="searchBox-body" role="presentation" style="left: 0px; top: 0px; width: 177px; height: 28px;">
                                    <div class="x-box-inner " id="searchBox-innerCt" role="presentation" style="width: 177px; height: 28px;">
                                        <div class="x-box-target" id="searchBox-targetEl" role="presentation" style="width: 177px;">
                                            <table class="x-field x-table-plain x-form-item x-form-type-text x-box-item x-field-default x-hbox-form-item" id="combobox-1022" role="presentation" style="margin: 0px; left: 0px; top: 1px; width: 139px; right: auto; table-layout: fixed;" cellpadding="0">
                                                <tbody>
                                                    <tr class="x-form-item-input-row" id="combobox-1022-inputRow" role="presentation">
                                                        <td width="105" class="x-field-label-cell" id="combobox-1022-labelCell" role="presentation" valign="top" style="display: none;" halign="left">
                                                            <label class="x-form-item-label x-unselectable x-form-item-label-left" id="combobox-1022-labelEl" style="width: 100px; margin-right: 5px;" for="combobox-1022-inputEl" unselectable="on"></label>
                                                        </td>
                                                        <td class="x-form-item-body  " id="combobox-1022-bodyEl" role="presentation" style="width: 100%;" colspan="3">
                                                            <table class="x-form-trigger-wrap" id="combobox-1022-triggerWrap" role="presentation" style="width: 100%; table-layout: fixed;" cellspacing="0" cellpadding="0">
                                                                <tbody role="presentation">
                                                                    <tr role="presentation">
                                                                        <td class="x-form-trigger-input-cell" id="combobox-1022-inputCell" role="presentation" style="width: 100%;">
                                                                            <div class="x-hide-display x-form-data-hidden" id="ext-gen1091" role="presentation"></div>
                                                                            <input name="combobox-1022-inputEl" class="x-field-without-trigger x-form-empty-field x-form-text " id="combobox-1022-inputEl" style="width: 100%;" type="text" placeholder="Search (Ctrl+/)" autocomplete="off" /><span class="icon-search"></span></td>
                                                                        <td class=" x-trigger-cell x-unselectable" id="ext-gen1090" role="presentation" valign="top" style="width: 23px; display: none;">
                                                                            <div class="x-trigger-index-0 x-form-trigger x-form-arrow-trigger x-form-trigger-first" id="ext-gen1089" role="presentation"></div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="x-form-invalid-under" id="combobox-1022-errorEl" role="alert" aria-live="polite" style="display: none;" colspan="2"></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="x-splitter x-box-item x-splitter-default x-splitter-vertical x-unselectable" id="splitter-1023" style="margin: 0px; left: 134px; top: 0px; width: 5px; height: 28px; right: auto; display: none;"></div>
                                            <a tabindex="0" class="x-btn btn-collapse x-unselectable x-box-item x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon btn-expanded" id="button-1024" style="border-width: 0px; margin: 0px; left: 149px; top: 2px; right: auto;" hidefocus="on" unselectable="on"><span class="x-btn-wrap" id="button-1024-btnWrap" role="presentation" unselectable="on"><span class="x-btn-button" id="button-1024-btnEl" role="presentation"><span class="x-btn-inner x-btn-inner-center" id="button-1024-btnInnerEl" unselectable="on">&nbsp;</span><span class="x-btn-icon-el icon-collapse " id="button-1024-btnIconEl" role="presentation" unselectable="on">&nbsp;</span></span></span></a>
                                        </div>
                                    </div>
                                </div>--%>
                            </div>
                            <div class="x-container x-box-item x-container-apps-menu x-box-layout-ct" id="container-1025" style="margin: 0px; left: 0px; top: 70px; width: 195px; height: 543px; right: auto;">
                                <div class="x-box-inner x-box-scroller-top" id="ext-gen1545" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-top x-unselectable x-box-scroller-disabled x-container-scroll-top-disabled" id="container-1025-before-scroller" role="presentation" style="display: none;"></div>
                                </div>
                                <div class="x-box-inner x-vertical-box-overflow-body" id="container-1025-innerCt" role="presentation" style="width: 195px; height: 543px;">
                                    <div class="x-box-target" id="container-1025-targetEl" role="presentation" style="width: 195px;">
                                        <div tabindex="-1" class="x-component x-box-item x-component-default" id="applications_menu" style="margin: 0px; left: 0px; top: 0px; width: 195px; right: auto;">
                                            <ul class="menu">
                                                <li class="menu-item menu-app-item app-item" id="menu-item-1" data-index="1"><a class="menu-link" href="Dashboard.aspx"><span class="menu-item-icon app-dashboard"></span><span class="menu-item-text">Oversigt</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-2" data-index="2"><a class="menu-link" href="AddCustomerVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Kunder</span></a></li>
                                                <%-- <li class="menu-item menu-app-item app-item" id="menu-item-3" data-index="3"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-clients"></span><span class="menu-item-text">Contacts</span></a></li>--%>
                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Stocks</span></a></li>--%>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-5" data-index="5"><a class="menu-link" href="Wizardss.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Tilbud</span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active" id="menu-item-6" data-index="6"><a class="menu-link" href="View Order.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Ordre</span></a></li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-7" data-index="7"><a class="menu-link" href="View Project.aspx"><span class="menu-item-icon app-projects"></span><span class="menu-item-text">Projekter</span></a></li>

                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon icon-tools"></span><span class="menu-item-text" onclick="ShowDiv('hide')">Indstillinger</span><%--<span class="menu-toggle"></span>--%></span>
                                                    <div id="hide" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3448">

                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-8" data-index="8"><a class="menu-link" href="AddContactVertical.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Bruger</span></a></li>
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-4" data-index="4"><a class="menu-link" href="View Stocks.aspx"><span class="menu-item-icon app-timesheets"></span><span class="menu-item-text">Lager</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <%-- <li class="menu-item menu-app-item app-item " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>

                                                <li class="menu-item menu-app-item app-item " id="menu-item-13" data-index="13"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>
                                                --%>

                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-13" data-index="13"><span class="group-item-text menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text" onclick="ShowDiv('Div1')">Rapporter</span></span>

                                                    <div id="Div1" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen34481">
                                                            <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link" href="Invoice.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Faktura</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item group-item expanded" id="menu-item-15" data-index="15"><a class="menu-link" href="Salary Module.aspx"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text" onclick="ShowDiv('Div3')">Løn Modul</span></a>
                                                    <div id="Div3" runat="server" style="display: none">
                                                        <ul class="menu-group" id="ext-gen3450">
                                                            <li class="menu-item menu-app-item app-item item-child" id="menu-item-10" data-index="16"><a class="menu-link" href="Reports.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Admin</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="menu-item menu-app-item app-item" id="menu-item-14" data-index="14"><a class="menu-link" href="MyTask.aspx"><span class="menu-item-icon app-mytasks"></span><span class="menu-item-text">Opgaver</span></a></li>



                                                <%--<li class="menu-item menu-app-item app-item" id="menu-item-8" data-index="8"><a class="menu-link" href="CreateUser.aspx"><span class="menu-item-icon app-users"></span><span class="menu-item-text">Users</span></a></li>
                                                <li class="menu-item menu-app-item group-item expanded" id="ext-gen3447"><span class="group-item-text menu-link"><span class="menu-item-icon app-billing"></span><span class="menu-item-text">Invoicing</span><span class="menu-toggle"></span></span><ul class="menu-group" id="ext-gen3448">
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-9" data-index="9"><a class="menu-link"><span class="menu-item-icon app-invoicing"></span><span class="menu-item-text">Invoices</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child " id="menu-item-10" data-index="10"><a class="menu-link"><span class="menu-item-icon app-estimates"></span><span class="menu-item-text">Estimates</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-11" data-index="11"><a class="menu-link"><span class="menu-item-icon app-recurring-profiles"></span><span class="menu-item-text">Recurring</span></a></li>
                                                    <li class="menu-item menu-app-item app-item item-child" id="menu-item-12" data-index="12"><a class="menu-link"><span class="menu-item-icon app-expenses"></span><span class="menu-item-text">Expenses</span></a></li>
                                                </ul>
                                                </li>
                                                <li class="menu-item menu-app-item app-item  " id="menu-item-13" data-index="13"><a class="menu-link"><span class="menu-item-icon app-reports"></span><span class="menu-item-text">Reports</span></a></li>--%>

                                                <li class="menu-item menu-app-item app-item"><a class="menu-link"><span class="menu-item-text"></span></a></li>
                                                <li class="menu-item menu-app-item app-item x-item-selected active"><span class="menu-item-icon app-recurring-profiles"></span><a class="menu-link" href="LoginPage.aspx"><span class="menu-item-text">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></span></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="x-box-inner x-box-scroller-bottom" id="ext-gen1546" role="presentation">
                                    <div class="x-box-scroller x-container-scroll-bottom x-unselectable" id="container-1025-after-scroller" role="presentation" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-container app-container x-border-item x-box-item x-container-default x-layout-fit" id="container-1041" style="border-width: 0px; margin: 0px; left: 195px; top: 0px; width: 1725px; height: 1000px; right: auto;">

                <div class="allSides" style="width: 100%; height: 80px;">
                    <div class="divider"></div>
                    <asp:Label ID="Label2" runat="server" CssClass="clienthead" Text="Ny Ordre"></asp:Label>
                </div>
                <div style="width: 1%; float: left; height: 800px; border: solid 0px black;">
                </div>
                <div style="height: 50px"></div>
                <div style="width: 82%; float: left; height: 820px; font-family: Calibri;">

                    <div style="width: 100%; float: left; height: 800px;">
                        <fieldset style="width: 1680px">

                            <legend style="color: black; font-weight: bold; font-size: large">Kundeoplysninger:</legend>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: right;">Kundenavn                                            
                                    </td>
                                    <td style="width: 5px"></td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtName" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqftxtName" runat="server" ErrorMessage="*" ControlToValidate="txtName" ValidationGroup="Order" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only characters allowed" ControlToValidate="txtName" ValidationExpression="[a-zA-Z ]*$"></asp:RegularExpressionValidator>--%>
                                        <asp:Label ID="lblName" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">Dato
                                    </td>
                                    <td style="width: 5px"></td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtorderdt" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                        <asp:Label ID="Lbldate" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtorderdt"></asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">Status
                                    </td>
                                    <td style="width: 5px"></td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtOrderStatus" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Moms</td>
                                    <td style="width: 5px"></td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="Txttax" runat="server" Width="300px" Height="31px" Text="0" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Total</td>
                                    <td style="width: 5px"></td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtTotal" runat="server" Width="300px" Height="31px" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Field1</td>
                                    <td style="width: 5px"></td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtPenalty" runat="server" Width="300px" Height="31px" Text="0" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Field2
                                    </td>
                                    <td style="width: 5px"></td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="TxtIncentive" runat="server" Width="300px" Height="31px" Text="0" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Total Earning
                                    </td>
                                    <td style="width: 5px"></td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="twitterStyleTextbox" ID="txtTotalEarning" runat="server" Width="300px" Height="31px" Text="" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;" colspan="3"></td>
                                </tr>

                            </table>
                        </fieldset>
                        <fieldset style="width: 1680px">

                            <legend style="color: black; font-size: large; font-weight: 700;">Varebeskrivelse:</legend>
                            <div id="Order" runat="server" style="height: 400px; overflow: auto">
                                <asp:GridView ID="GridViewOrder" runat="server" ShowFooter="true" AutoGenerateColumns="false" Width="100%" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White" Style="font-size: medium">
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White"></asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item Code" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblItemCode" runat="server" Text='<%# Bind("ItemCode") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle CssClass="Itemalign" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Varenavn" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblItemName" runat="server" Text='<%# Bind("ItemName") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Itemalign" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Pris" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblItemPrice" runat="server" Text='<%# Bind("Price") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Itemalign" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Antal" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblItemQua" runat="server" Text='<%# Bind("RespondQuantity") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Itemalign" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Total" HeaderStyle-BorderWidth="5px" HeaderStyle-BorderColor="white" ItemStyle-BorderColor="White" ItemStyle-BorderWidth="5px" Visible="true" FooterStyle-BorderWidth="5px" FooterStyle-BorderColor="White">
                                            <ItemTemplate>
                                                <%--  <asp:Label ID="lblItemtotal" runat="server" Text='<%# Convert.ToInt32(Eval("RespondQuantity"))*Convert.ToInt32(Eval("Price")) %>' Visible="true"></asp:Label>--%>
                                                <asp:Label ID="lblItemtotal" runat="server" Text='<%# Bind("TotalPrice") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Itemalign" />
                                            <FooterTemplate>
                                                <asp:Label ID="lblTot" runat="server" Text="" Visible="true"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="shadowdiv" style="width: 1725px; float: left; height: 50px; text-align: center; border: solid 0px black;">
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="BtnAddOrder" runat="server" Text="Tilføj" Width="101px" Style="background-color: #009900" OnClick="BtnAddOrder_Click" ValidationGroup="Order" />
                                </td>
                                <td>
                                    <asp:Button CssClass="buttonn" ID="BtnViewOrder" runat="server" Text="Vis" Width="120px" Style="background-color: #009933" OnClick="BtnViewOrder_Click" />
                                </td>
                                <%-- <td>
                                    <asp:Button CssClass="buttonn" ID="BtnDeleteOrder" runat="server" Text="DELETE" Width="120px" Style="background-color: #009933" />
                                </td>
                                <td></td>--%>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </form>
</body>
</html>
