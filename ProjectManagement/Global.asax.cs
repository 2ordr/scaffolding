﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Data;
using System.Collections;

namespace ProjectManagement
{
    public class Global : HttpApplication
    {
        public static DataTable dtStock = new DataTable();
        public static int flag = 0;
        public static Boolean Editflag = false;
        public static Boolean Flag = true;
        public static int fflag = 0;
        public static Decimal TotIFees = 0;
        public static int FLAGG = 0;
        public static int Updateflag = 0;
        public static int ChkFlagg = 0;
        public static int subValues = 0;
        public static int OrderValues = 0;
        public static int chkUser = 0;
        public static int DeleteProOrder = 0;
        public static Boolean FlagVis = false;
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            dtStock.Columns.AddRange(new DataColumn[4] { new DataColumn("Itemcode"), new DataColumn("ItemName"), new DataColumn("Quantity"), new DataColumn("Price") });
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }
    }
}