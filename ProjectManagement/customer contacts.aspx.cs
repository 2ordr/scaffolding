﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace ProjectManagement
{
    public partial class customer_contacts : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            //LblCustName.Text = Session["labeltext"].ToString();
            LblCustId.Text = Session["labelid"].ToString();


            if (!IsPostBack)
            {



                SqlConnection con = new SqlConnection(cs);
                con.Open();


                //SqlCommand com = new SqlCommand("select * from CONTACTS ", con);

                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_ContactByName";
                com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.NVarChar).Value = LblCustId.Text;

                SqlDataAdapter adpt = new SqlDataAdapter(com);

                DataSet dt = new DataSet();

                adpt.Fill(dt);

                GViewContact.DataSource = dt;

                GViewContact.DataBind();
                LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();
                LblContId.Text = dt.Tables[0].Rows[0]["CONTID"].ToString();

            }
        }





        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            //divContact.Visible = false;
        }

        protected void BtnSaveContact_Click(object sender, EventArgs e)
        {

        }

        //protected void BtnContact_Click(object sender, EventArgs e)
        //{
        //    divContact.Visible = true;
        //}

        protected void OpenWindow(object sender, EventArgs e)
        {
            Global.flag = 0;
            string url = ("Popup.aspx?CustId=" + LblCustId.Text);

            string s = "window.open('" + url + "', 'popup_window', 'width=1000, height=800, left=200, top=200, resizable=no');";

            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {

        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            Global.flag = 1;

            ArrayList selectedValues = new ArrayList();
            foreach (GridViewRow item in GViewContact.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chk = (CheckBox)(item.Cells[0].FindControl("chkSelect"));
                    if (chk.Checked)
                    {
                        Label ContId = ((Label)item.Cells[0].FindControl("CONTID"));


                        string url = ("Popup.aspx?ContId=" + ContId.Text);

                        string s = "window.open('" + url + "', 'popup_window', 'width=1000, height=800, left=200, top=200, resizable=no');";

                        ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                        //Response.Redirect("Popup.aspx?ContId=" + LblContId.Text);

                    }

                }
                //Session["SELECTEDVALUES"] = selectedValues;
                //Response.Redirect("customer overview.aspx");

            }
        }
    }
}