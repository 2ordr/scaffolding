﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectManagement
{
    public partial class New_Invoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";
                    Label2.Text = Session["username"].ToString();
                }

            }
        }
    }
}