﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data.OleDb;
using System.Data;

namespace ProjectManagement
{
    public partial class New_Project : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label1.Text = Session["username"].ToString();
                resetall();
            }
        }
        public void resetall()
        {

            txtName.Text = "";
            DropDownList2.SelectedValue = "";
            TxtDescript.Text = "";
            TxtStartdt.Text = "";
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("View Project.aspx");
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            if (txtName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('First Name cannot be blank');", true);
                return;
            }
            else
            {
                SqlConnection con = new SqlConnection(cs);
                con.Open();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_InsertProjects";
                com.Parameters.Add("@ITEMNAME", System.Data.SqlDbType.VarChar).Value = txtName.Text;
                com.Parameters.Add("@CLient", System.Data.SqlDbType.VarChar).Value = DropDownList2.SelectedValue;
                com.Parameters.Add("@ITEMPROPERTY1", System.Data.SqlDbType.VarChar).Value = TxtDescript.Text;
                com.Parameters.Add("@StartDate", System.Data.SqlDbType.DateTime).Value = TxtStartdt.Text;

                //com.Parameters.Add("@ITEMPROPERTY2", System.Data.SqlDbType.VarChar).Value = "";
                //com.Parameters.Add("@ITEMPROPERTY3", System.Data.SqlDbType.VarChar).Value = "";
                //com.Parameters.Add("@PRICE", System.Data.SqlDbType.Decimal).Value = Txtprice.Text;
                //com.Parameters.Add("@QUANTITY", System.Data.SqlDbType.VarChar).Value = txtQuantity.Text;
                //com.Parameters.Add("@FLAG", System.Data.SqlDbType.Bit).Value = 0;
                //com.Parameters.Add("@UNIT", System.Data.SqlDbType.VarChar).Value = TxtUnit.Text;

                //com.Parameters.Add("@DELETESTATUS", System.Data.SqlDbType.BigInt).Value =false;

                com.ExecuteNonQuery();
                com.Parameters.Clear();
                resetall();

            }
        }
    }
}