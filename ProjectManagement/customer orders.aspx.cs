﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace ProjectManagement
{

    public partial class customer_orders : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
       
        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            //LblCustName.Text = Session["labeltext"].ToString();
            LblCustId.Text = Session["labelid"].ToString();
            if (!IsPostBack)
            {

                SqlConnection con = new SqlConnection(cs);
                con.Open();


                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.CommandText = "sp_OrderByName";
                com.Parameters.Add("@I_CUSTID", System.Data.SqlDbType.NVarChar).Value = LblCustId.Text;


                SqlDataAdapter adpt = new SqlDataAdapter(com);

                DataSet dt = new DataSet();

                //dt.Rows.Clear();

                adpt.Fill(dt);

                GridViewOrder.DataSource = dt;

                GridViewOrder.DataBind();

                LblCustName.Text = dt.Tables[0].Rows[0]["CustName"].ToString();

            }
        }

        protected void BtnOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("Order Form.aspx");
        }

        protected void GridViewOrder_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void LikButEdit_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string quatdate = grdrow.Cells[2].Text;
            Response.Redirect("EditOrder.aspx?Quatdate=" + quatdate);

        }

    }
}