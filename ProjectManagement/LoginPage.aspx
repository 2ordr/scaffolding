﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="ProjectManagement.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

   <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>New - Projects - Scaffolding</title>

    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="fonts/untitled-font-1/styles.css" type="text/css" />
    <link rel="stylesheet" href="Styles/defaultcss.css" type="text/css" />
     <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>


    <style>
        .divframe {
            background-image:;
        }

        .col_cs {
            width: 25%;
            height: 20px;
        }
    </style>

    <style type="text/css">
        .styled-button-7 {
            -webkit-box-shadow: rgba(0,0,0,0.2) 0 1px 0 0;
            -moz-box-shadow: rgba(0,0,0,0.2) 0 1px 0 0;
            box-shadow: rgba(0,0,0,0.2) 0 1px 0 0;
            border-bottom-color: #333;
            border: 1px solid #61c4ea;
            background-color: #7cceee;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            color: #333;
            font-family: 'Verdana',Arial,sans-serif;
            font-size: 14px;
            text-shadow: #b2e2f5 0 1px 0;
            padding: 5px;
        }

        .fieldset {
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
        }
    </style>


</head>
<body style="background-image: url('image/Loginback.png')">
    <form id="form1" runat="server">
        <div style="height: 800px; width: 100%;">

            <div style="height: 10%; width: 100%;"></div>

            <div style="height: 90%; width: 100%;">

                <div style="height: 50%; width: 100%;">
                    <table style="height: 100%; width: 100%;">
                        <tr>
                            <td style="height: 100%; width: 30%"></td>
                            <td style="height: 100%; width: 40%;">
                                <fieldset class="fieldset">

                                    <legend style="color: white; font-weight: bold; font-size: x-large; font-family: Calibri; font-style: italic;">Scaffolding Login</legend>
                                    <table style="height: 80%; width: 100%;">

                                        <tr class="col_cs">
                                            <td colspan="3"></td>

                                        </tr>
                                        <tr>
                                            <td class="col_cs"></td>
                                            <td style="width: 50%">
                                                <asp:Label ID="LblUserName" runat="server" ForeColor="White" Text="UserName:"></asp:Label></td>
                                            <td class="col_cs"></td>
                                        </tr>
                                        <tr class="col_cs">
                                            <td></td>
                                            <td>
                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txt_UserName" runat="server"></asp:TextBox></td>
                                            <td>
                                                <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ForeColor="White" ControlToValidate="txt_UserName"
                                                    ErrorMessage="User Name is required."
                                                    ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>--%></td>
                                        </tr>

                                        <tr class="col_cs">
                                            <td class="col_cs" colspan="3"></td>
                                        </tr>

                                        <tr class="col_cs">
                                            <td></td>
                                            <td>
                                                <asp:Label ID="LblPassword" runat="server" ForeColor="White" Text="Password:"></asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr class="col_cs">
                                            <td></td>
                                            <td>
                                                <asp:TextBox CssClass="twitterStyleTextbox" ID="txt_Password" runat="server" TextMode="Password"></asp:TextBox></td>
                                            <td>
                                               <%-- <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ForeColor="White" ControlToValidate="txt_Password"
                                                    ErrorMessage="Password is required." ToolTip="Password is required."
                                                    ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>--%></td>
                                        </tr>

                                        <tr class="col_cs">
                                            <td class="col_cs" colspan="3"></td>
                                        </tr>
                                        <tr class="col_cs">
                                            <td></td>
                                            <td>
                                                <asp:CheckBox ID="ChkRememberMe" runat="server" Text="" />
                                                <asp:Label ID="Label1" runat="server" AssociatedControlID="ChkRememberMe" ForeColor="White" Text="Keep me logged in"></asp:Label></td>
                                            <td></td>
                                        </tr>

                                        <tr class="col_cs">
                                            <td></td>
                                            <td>
                                                <asp:Button CssClass="styled-button-7" ID="BtnLogin" runat="server" Text="Login" Height="31" OnClick="BtnLogin_Click" /><div class="divider"></div>
                                                <asp:Button CssClass="styled-button-7" ID="BtnSignIn" runat="server" Text="SignUp" Height="31" /></td>
                                            <td></td>
                                        </tr>
                                        <tr class="col_cs">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr class="col_cs">
                                            <td></td>
                                            <td colspan="2">
                                                <p style="color: white">
                                                    Please enter your username and password.
                                                </p>
                                            </td>

                                        </tr>
                                        <tr class="col_cs">
                                            <td colspan="3"></td>
                                        </tr>

                                    </table>
                                </fieldset>
                            </td>
                            <td style="height: 100%; width: 30%;"></td>
                        </tr>


                    </table>

                </div>
                <div style="height: 40%; width: 100%;"></div>
            </div>

        </div>
    </form>
</body>
</html>
