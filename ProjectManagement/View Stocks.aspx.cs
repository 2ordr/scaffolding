﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Threading;

namespace ProjectManagement
{
    public partial class View_Stocks : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;

        SqlCommand com = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Session["username"] as string))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    TextBox1.Attributes["onclick"] = "clearTextBox(this.id)";

                    Label1.Text = Session["username"].ToString();
                    BindStock();
                }
            }

        }

        public void BindStock()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            SqlCommand com = new SqlCommand("select * from STOCKSITEM", con);

            SqlDataAdapter adpt = new SqlDataAdapter(com);

            DataTable dt = new DataTable();

            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {

                GridView2.DataSource = dt;

                GridView2.DataBind();
            }
            con.Close();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Stocks.aspx");
        }

        //protected void lnkViewStock_Click(object sender, EventArgs e)
        //{

        //}

        protected void lnkDeleteStock_Click(object sender, EventArgs e)
        {
            //    GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //    string StockId = grdrow.Cells[1].Text;

            //    SqlConnection con1 = new SqlConnection(cs);
            //    con1.Open();

            //    SqlCommand com1 = new SqlCommand("delete FROM STOCKSITEM where ItemCode = @I_ITEMCODE;", con1); //delete FROM HAS  where ItemCode = @I_ITEMCODE;

            //    com1.Parameters.Add("@I_ITEMCODE", SqlDbType.BigInt).Value = StockId;

            //    com1.ExecuteNonQuery();
            //    con1.Close();
            //    BindStock();
            //    ScriptManager.RegisterStartupScript(this, GetType(), "AlertMsg", "alert('Stock Deleted Successfully!!');", true);
            //    return;
        }

        protected void lnkUpdateStock_Click(object sender, EventArgs e)
        {
            //GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            //string Itemcode = grdrow.Cells[0].Text;
            //Response.Redirect("ProjectAssignment.aspx?ItemCode=" + Itemcode);
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("delete from STOCKSITEM where ItemCode=" + id, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                BindStock();
            }
            if (e.CommandName == "UpdateRow")
            {
                int Itemcode = Convert.ToInt32(e.CommandArgument);
                Response.Redirect("Stocks.aspx?ItemCode=" + Itemcode);
            }
        }

        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView2.PageIndex = e.NewPageIndex;
            this.BindStock();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void PrintButton_Click(object sender, EventArgs e)
        {

            if (GridView2.Rows.Count > 0)
            {
                //To Export all pages
                GridView2.AllowPaging = false;
                this.BindStock();

                PdfPTable table = new PdfPTable(GridView2.Columns.Count);
                table.WidthPercentage = 100;

                Font NFont = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
                Font NFont1 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10);
                //table.Cellpadding = 5;
                //Set the column widths
                int[] widths = new int[GridView2.Columns.Count];
                for (int x = 0; x < GridView2.Columns.Count; x++)
                {
                    //Phrase phrs = new Phrase();
                    widths[x] = (int)GridView2.Columns[x].ItemStyle.Width.Value;
                    string cellText = Server.HtmlDecode(GridView2.HeaderRow.Cells[x].Text);
                    //phrs.Add(new Chunk(cellText));
                    PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont));
                    cell.BorderColor = BaseColor.BLACK;
                    cell.Border = 1;
                    cell.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderColorLeft = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderColorRight = new BaseColor(System.Drawing.Color.Black);
                    cell.BorderWidthTop = 1f;
                    cell.BorderWidthBottom = 1f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.BorderWidthRight = 0.5f;

                    cell.PaddingBottom = 3;
                    //cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    table.AddCell(cell);
                }

                table.SetWidths(widths);
                //Transfer rows from GridView to table

                for (int i = 0; i < GridView2.Rows.Count; i++)
                {
                    if (GridView2.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        for (int j = 0; j < GridView2.Columns.Count; j++)
                        {
                            //Phrase phrs = new Phrase();
                            string cellText = Server.HtmlDecode(GridView2.Rows[i].Cells[j].Text);

                            //phrs.Add(new Chunk(cellText));
                            PdfPCell cell = new PdfPCell(new Phrase(cellText, NFont1));


                            //Set Color of Alternating row
                            //if (i % 2 != 0)
                            //{
                            //    cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#C2D69B"));
                            //}

                            table.AddCell(cell);
                        }
                    }
                }
                String attachment = "attachment;filename= StockDetails.pdf " + DateTime.Now + "";

                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();

                pdfDoc.Add(table);
                pdfDoc.Close();

                Response.ContentType = "application/pdf";

                Response.AddHeader("content-disposition", attachment);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();
            }
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(50);

            SqlConnection con = new SqlConnection(cs);
            con.Open();

            String sql = "Select * from STOCKSITEM where ItemName Like @ItemName";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {

                cmd.Parameters.Add("@ItemName", System.Data.SqlDbType.VarChar, 50).Value = "%" + TextBox1.Text + "%";

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                DataSet dt1 = new DataSet();

                adpt.Fill(dt1);

                if (dt1.Tables[0].Rows.Count > 0)
                {

                    GridView2.DataSource = dt1;

                    GridView2.DataBind();
                }
            }
            con.Close();
        }

        protected void Bound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "style.backgroundColor='LightBlue'");
            e.Row.Attributes.Add("onmouseout", "style.backgroundColor='#fbfbfb'");
        }

    }
}